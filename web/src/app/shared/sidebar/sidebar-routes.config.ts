import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [

	{
		path: '/dashboard', title: 'Tableau de bord', icon: 'fa fa-tachometer', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},

	//agendaglobal
	{
		path: '/agendaglobal', title: 'Agenda global', icon: 'fa fa-calendar', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},

	{
		path: '/tache', title: 'Tâches', icon: 'fa fa-calendar', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	// --------------Section----------------
	{
		isTitleSection: true, path: '', title: 'Contacts', icon: 'fa fa-id-card', class: 'menu-item-section', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},

	{
		path: '/utilisateurs', title: 'Utilisateurs', icon: 'fa fa-user', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/clients', title: 'Clients', icon: 'fa fa-users', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	// {
	// 	path: '/groupes', title: 'Groupes', icon: 'fa fa-users', class: 'nav-item', badge: '', badgeClass: '',
	// 	isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	// },

	{
		path: '/fournisseurs', title: 'Fournisseurs', icon: 'fa fa-handshake-o', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	// --------------Section----------------
	{
		isTitleSection: true, path: '', title: 'Gestion Chantiers', icon: 'fa fa-wrench', class: 'menu-item-section', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/chantiers', title: 'Chantiers', icon: 'fa fa-university', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/ficheintervention', title: 'Fiche Intervention', icon: 'fa fa-clipboard', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	// --------------Section----------------
	{
		isTitleSection: true, path: '', title: 'Gestion maintenance', icon: 'fa fa-stethoscope',
		class: 'menu-item-section', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/gammemaintenanceequipements', title: 'GME', icon: 'fa fa-shopping-cart', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/contratentretiens', title: 'Contrat d\'entretien', icon: 'fa fa-files-o', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/visitesMaintenance', title: 'Visites de maintenance', icon: 'fa fa-file-o', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/ficheinterventionmaintenance', title: 'Fiches IM', icon: 'fa fa-clipboard', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/planification', title: 'Planification', icon: 'fa fa-calendar', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},


	// --------------Section----------------
	{
		isTitleSection: true, path: '', title: 'Facturation', icon: 'fa fa-credit-card-alt',
		class: 'menu-item-section', badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/devis', title: 'Devis', icon: 'fa fa-file-text-o', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/factures', title: 'Factures', icon: 'fa fa-newspaper-o', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/factureReccurente', title: 'Programmation', icon: 'fa fa-file-archive-o', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/avoirs', title: 'Avoirs', icon: 'fa fa-file-o', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/bonCommandeFournisseur', title: 'Bon de commande Frs', icon: 'fa fa-list-alt ', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/depense', title: 'Dépenses', icon: 'fa fa-wpforms', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/paiements', title: 'Paiements', icon: 'ft-credit-card', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	// --------------Section----------------
	{
		isTitleSection: true, path: '', title: 'Bibliothèque', icon: 'fa fa-cubes', class: 'menu-item-section', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/produits', title: 'Articles', icon: 'fa fa-shopping-cart', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/lots', title: 'Lots', icon: 'fa fa-shopping-bag', class: 'nav-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},

	// --------------Section----------------
	{
		isTitleSection: true, path: '', title: 'Comptabilité', icon: 'fa fa-list-ol', class: 'menu-item-section',
		badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/comptabilite', title: 'Journaux comptables', icon: 'fa fa-newspaper-o', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		isTitleSection: true, path: '', title: 'Administration', icon: 'fa fa-sliders', class: 'menu-item-section',
		badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	// {
	//     path: '', title: 'parameteres', icon: 'fa fa-wrench', class: 'menu-item has-sub', badge: '', badgeClass: '',
	// 	isExternalLink: false, isNavHeader: false, actif: true } ,
	// submenu: [
	{
		path: '/parameteres/numerotationPrefixe', title: 'Numérotations et préfixes', icon: '', class: 'menu-item',
		badge: '', badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/agenda', title: 'Agenda', icon: 'fa fa-newspaper-o', class: 'nav-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/parametrageDocument', title: 'Paramétrage des documents', icon: '', class: 'menu-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/parametragePdf', title: 'Paramétrage PDF', icon: '', class: 'menu-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/modelregelement', title: 'Modes de réglement', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/prix', title: 'TVA et Coûts par défaut', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/horairetravail', title: 'Horaires de travail', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/objectifsca', title: 'Objectifs CA', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},

	{
		path: '/parameteres/labels', title: 'Labels articles', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/typedocument', title: 'Types de document', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/comptabilite', title: 'Comptabilité', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/editique', title: 'Module éditique', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/messagerie', title: 'Messagerie', icon: '', class: 'menu-item', badge: '', badgeClass: '',
		isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	{
		path: '/parameteres/synagenda', title: 'Synchronisation agenda', icon: '', class: 'menu-item', badge: '',
		badgeClass: '', isExternalLink: false, isNavHeader: false, submenu: [], actif: true
	},
	// 	]
	// }
];
