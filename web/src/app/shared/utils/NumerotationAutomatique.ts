import { Numerotation, FormatDate } from 'app/Models/Entities/Parametres/Numerotation';
export class NumerotationAutomatique {

	public constructor() { }

	public static composeNumbering(documentNumberingInformations: Numerotation, incrementationNumber?: number): string {
		incrementationNumber = incrementationNumber === undefined ? 0 : incrementationNumber;
		// tslint:disable-next-line: radix
		const compteur: string = (documentNumberingInformations.counter + incrementationNumber).toString();
		const longeurCompteur = documentNumberingInformations.counterLength.toString();
		const racine: string = documentNumberingInformations.prefix;
		const formatDate: string = this.getFormatDate(documentNumberingInformations.dateFormat);
		const numberZeros: string = this.calculateNumberOfComplementaryZeros(longeurCompteur, compteur);
		return racine + formatDate + numberZeros + compteur;
	}

	public static getFormatDate(formatDate): string {
		let formatDateLabel: string = '';
		if (formatDate !== undefined) {
			const date = new Date();
			const cYear = date.getFullYear();
			const DateMonth = (date.getMonth()) + 1;
			const cMonth = DateMonth < 10 ? `0${DateMonth}` : DateMonth;
			switch (formatDate) {
				case FormatDate.Annee:
					formatDateLabel = cYear.toString();
					break;
				case FormatDate.AnneeMois:
					formatDateLabel = cYear.toString() + '' + cMonth.toString();
					break;
				case FormatDate.None:
					formatDateLabel = '';
					break;
			}
		}
		return formatDateLabel;
	}

	public static calculateNumberOfComplementaryZeros(longeurCompteur: string, compteur: string): string {
		// tslint:disable-next-line: radix
		const count: number = compteur == null ? 0 : parseInt(longeurCompteur) - compteur.toString().length;
		let numberZeros: string = '';
		if (count > 0) {
			for (let index = 0; index < count; index++) {
				numberZeros = numberZeros + '0';
			}
		}
		return numberZeros;
	}
}
