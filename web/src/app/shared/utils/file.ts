export class FileUtils {


    /**
     * Download file from base64
     * @param data base64
     * @param fileName name of file
     * @param fileType type of file
     * @param extension extension of file
     */
	static downloadBase64(data: any, fileName: string, fileType: string, extension: string) {
		let buffer: any, blob: any, url: any, a: any;

		if (fileType.includes('application/octet-stream')) {
			fileType = 'application/' + extension;
		}

		const binaryString = window.atob(data.split(',')[1]);
		const len = binaryString.length;
		const bytes = new Uint8Array(len);
		for (let i = 0; i < len; i++) {
			bytes[i] = binaryString.charCodeAt(i);
		}
		buffer = bytes.buffer;

		blob = new Blob([buffer], { type: fileType });

		// Set view.
		if (blob) {
			// Read blob.
			url = window.URL.createObjectURL(blob);

			// Create link.
			a = document.createElement('a');
			// Set link on DOM.
			document.body.appendChild(a);
			// Set link's visibility.
			a.style = 'display: none';
			// Set href on link.
			a.href = url;
			// Set file name on link.
			a.download = fileName;

			// Trigger click of link.
			a.click();

			// Clear.
			window.URL.revokeObjectURL(url);
		}
	}

    /**
     * download blob data
     * @param data the data format blob
     */
	static downloadBlob(data: Blob, filename: string) {
		const downloadedFile = new Blob([data], { type: data.type });
		const a = document.createElement('a');
		a.setAttribute('style', 'display:none;');
		document.body.appendChild(a);
		a.download = filename;
		a.href = URL.createObjectURL(downloadedFile);
		a.target = '_blank';
		a.click();
		document.body.removeChild(a);
	}

    /**
     * transfer array bytes to Uint8Array
     * @param byteArray the array of bytes
     */
	static bytesToArrayBuffer(byteArray): ArrayBuffer {
		const binary_string = window.atob(byteArray);
		const len = binary_string.length;
		const bytes = new Uint8Array(len);
		for (let i = 0; i < len; i++) {
			bytes[i] = binary_string.charCodeAt(i);
		}
		return bytes.buffer;
	}

    /**
     * generate object from Array Buffer
     * @param data the array ArrayBuffer
     * @param fileName the file name
     * @param fileType the type of file
     * @param extension the extension
     */
	static generateObjectFromArrayBuffer(data, fileName: string, fileType: string, extension: string): any {

		// Get time stamp for fileName.
		const stamp = new Date().getTime();

		// Set MIME type and encoding.
		fileType = (fileType || 'application/pdf');

		// Set file name.
		fileName = (fileName || stamp + '.' + extension);

		// Set data on blob.
		const blob = new Blob([data], { type: fileType });

		// Set view.
		return window.URL.createObjectURL(blob);
	}

    /**
     * download excel file
     * @param base64 the file format base64
     * @param fileName the name of excel file
     */
	static downloadExcelFile(base64: string, fileName: string) {
		// Get time stamp for fileName.
		const stamp = new Date().getTime();

		// file type
		const fileType = 'application/vnd.ms-excel';

		// file extension
		const extension = 'xlsx';

		FileUtils.downloadBase64(',' + base64, `${fileName}-${stamp}.${extension}`, fileType, extension);
	}

}
