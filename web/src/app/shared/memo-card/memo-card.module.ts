import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MemoCardComponent } from './memo-card.component';
import { MatIconModule, MatMenuModule, MatTooltipModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MemoCardComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatMenuModule,
    MatTooltipModule,
    FormsModule
  ],
  exports: [MemoCardComponent]
})
export class MemoCardModule { }
