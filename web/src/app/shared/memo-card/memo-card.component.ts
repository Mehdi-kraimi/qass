import { AfterViewInit, Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { VisualiserPdfComponent } from 'app/common/visualiser-pdf/visualiser-pdf.component';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { DialogHelper } from 'app/libraries/dialog';
import { PieceJoin } from 'app/Models/Entities/Commun/Commun';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { IGenericRepository } from '../repository/igeneric-repository';
import { ShowImageComponent } from '../show-image/show-image.component';
import { Constants } from '../utils/constants';
declare var toastr: any;
declare var jQuery: any;

@Component({
	selector: 'memo-card',
	templateUrl: './memo-card.component.html',
	styleUrls: ['./memo-card.component.scss']
})
export class MemoCardComponent implements OnInit, AfterViewInit {

	//#region Properties

	memo: Memo = new Memo();
	commentaire = '';
	files = null;

	readonly = true;
	selectedAttachment: any = {};
	@Input('modificationPermission') modificationPermission = false;

	@Input() data: any = [];

	@Output() delete = new EventEmitter<void>();
	@Output() edit = new EventEmitter<any>();
	@Output() download = new EventEmitter<number>();

	@ViewChild('previewContainer') previewContainer: ElementRef;
	isAdd = false;

	//#endregion

	//#region Lifecycle

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private sanitizer: DomSanitizer,
		private dialog: MatDialog,
		private translate: TranslateService,
	) {
		this.translate.setDefaultLang('fr');
		this.translate.use(AppSettings.lang);
	}

	ngOnInit(): void {
	}

	ngAfterViewInit(): void {
		if (this.previewContainer && this.previewContainer.nativeElement && this.data && this.data.attachments && this.data.attachments.length > 0) {
			const start = this.previewContainer.nativeElement.children.item(0).getClientRects().item(0).x;
			const last = this.previewContainer.nativeElement.children.item(this.previewContainer.nativeElement.children.length - 1).getClientRects().item(0).x - 35;
			const width = Math.abs(last - start);
			this.previewContainer.nativeElement.style.setProperty('width', `${width}px`, 'important');
			this.selectedAttachment = { ...this.data.attachments.slice(0).reverse()[0] };
		}
	}

	//#endregion

	//#region Events

	onValidate(): void {
		if (this.isUpdate === true) {


			this.data.comment = this.commentaire;
			this.data.attachments = this.memo.attachments;
			this.edit.emit(this.data);
			this.isUpdate = false;
		} else {
			this.edit.emit(this.data);

		}
		this.readonly = true;
		// if (this.data) {
		//   this.isUpdate = true;
		//   this.memo = { ...this.data };
		//   this.commentaire = this.data.comment;
		//   this.files = this.data.comment.attachments || [];
		// }

	}

	onUpload(event: FileList): void {
		const file = event.item(0)
		const reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
			const pieceJoin = new PieceJoin()
			// pieceJoin.name = AppSettings.guid()
			pieceJoin.fileType = Constants.getContentType(file.name.substring(file.name.lastIndexOf('.') + 1)).Mime;
			pieceJoin.fileName = file.name
			pieceJoin.content = reader.result.toString()
			this.memo.attachments.unshift(pieceJoin)
			this.files = null;
		}
	}

	onInit(): void {
		this.commentaire = '';
		this.memo = new Memo();
	}

	onDownload(index: any): void {
		this.download.emit(index);
	}


	// tslint:disable-next-line: member-ordering
	isUpdate = false;
	onEdit(): void {
		if (this.data) {
			this.isUpdate = true;
			this.memo = { ...this.data };
			this.commentaire = this.data.comment;
			this.files = this.data.comment.attachments || [];
		}

		this.readonly = false;
	}

	isShow(item) {
		if (this.isAttachmentImage(item)) {
			return true;
		}
		if (this.isAttachmentPDF(item)) {
			return true;
		}
		else {
			return false;
		}
	}


	showDoc(attachment) {
		this.service.getById(ApiUrl.File, attachment['fileId']).subscribe(
			value => {
				const image = value.value;
				if (this.isAttachmentPDF(attachment)) {
					const base64 = image.substring(image.lastIndexOf(';base64,') + 8);

					this.genererPDF(base64)
				} else {
					const img = this.sanitizer.bypassSecurityTrustUrl(image);
					this.showImg(img)
				}

			});
	}

	showImg(image) {
		const dialogShowUmageConfig = new MatDialogConfig();
		dialogShowUmageConfig.width = '700px';
		dialogShowUmageConfig.height = '500px';
		dialogShowUmageConfig.data = image;
		const dialogRef = this.dialog.open(ShowImageComponent, dialogShowUmageConfig);
		dialogRef.afterClosed().subscribe((data) => {
		});
	}


	onRemove(i: number) {
		this.memo.attachments.splice(i, 1)
	}

	isAttachmentPDF = (attachment: any): boolean => attachment && attachment.fileType && attachment.fileType.includes('pdf');

	onDelete(): void {
		this.delete.emit(this.elementSelected);

	}
	elementSelected = null;
	onMenuOpened(element: any): void {
		this.elementSelected = element;
	}

	onAttachmentHover(attachement: any): void {
		this.selectedAttachment = attachement;
	}

	//#endregion

	//#region Methods

	isAttachmentImage = (attachment: any): boolean => attachment && attachment.fileType && attachment.fileType.includes('image');

	getAttachmentIcon(attachment: any): string {
		const type = this.isAttachmentImage(attachment) ? 'image' : 'pdf';
		return `./assets/app/images/Icons/file-type-${type}.svg`;
	}

	getAttachmentViewIcon(attachment: any): string {
		const type = this.isAttachmentImage(attachment) ? 'green' : 'bleu';
		return `./assets/app/images/Icons/eye-${type}.svg`;
	}

	genererPDF(base64) {
		DialogHelper.openDialog(
			this.dialog,
			VisualiserPdfComponent,
			DialogHelper.SIZE_MEDIUM,
			{ base64: base64, docType: '', doc: '' }
		).subscribe(async response => {
		});
	}

	//#endregion
}
