import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FournisseurCardComponent } from './fournisseur-card.component';

describe('FournisseurCardComponent', () => {
  let component: FournisseurCardComponent;
  let fixture: ComponentFixture<FournisseurCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FournisseurCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FournisseurCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
