import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ToggleFullscreenDirective } from './directives/toggle-fullscreen.directive';
import { TranslateModule } from '@ngx-translate/core';
// import { changePasswordModule } from 'app/common/change-password/change-password.module';
import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { NotificationsComponent } from './notifications/notifications.component';
import { AgendaFormComponent } from './agenda-form/agenda-form/agenda-form.component';
import { MatIconModule, MatDatepickerModule, MatFormField, MatLabel, MatFormFieldModule, MatCheckboxModule, MatSelectModule, MatInputModule, MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FournisseurCardModule } from './fournisseur-card/fournisseur-card.module';

import { HttpClient } from '@angular/common/http';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { AgendaoptionComponent } from './agenda-form/agenda-option/agenda-option.component';
import { ListIndexModule } from 'app/components/form-data/index-list/index.module';
import { ShowImageComponent } from './show-image/show-image.component';
import { CommonModules } from 'app/common/common.module';


export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/user/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },

	]);
}
@NgModule({
	exports: [
		CommonModule,
		FooterComponent,
		NavbarComponent,
		SidebarComponent,
		MenuComponent,
		HeaderComponent,
		ToggleFullscreenDirective,
		AgendaFormComponent,
		NgbModule,
		AgendaoptionComponent,
		ShowImageComponent
	],
	imports: [
		CommonModules,
		NgxMatSelectSearchModule,
		RouterModule,
		CommonModule,
		MatMenuModule,
		InputTextareaModule,
		MatIconModule,
		MatDatepickerModule,
		MatFormFieldModule,
		MatInputModule,
		MatRippleModule,
		MatCheckboxModule,
		MatSelectModule,
		ReactiveFormsModule,
		OverlayModule,
		FormsModule,
		TranslateModule,
		NgbModule,
		FournisseurCardModule,
		MatDialogModule,
		// changePasswordModule
	],
	declarations: [
		FooterComponent,
		NavbarComponent,
		SidebarComponent,
		ToggleFullscreenDirective,
		MenuComponent,
		HeaderComponent,
		NotificationsComponent,
		AgendaFormComponent,
		AgendaoptionComponent,
		ShowImageComponent
	],
	entryComponents: [AgendaFormComponent, AgendaoptionComponent, ShowImageComponent]
})
export class SharedModule { }
