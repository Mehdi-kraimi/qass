import { Injectable } from '@angular/core';
import { IGenericRepository } from './igeneric-repository';
import { RequestType } from 'app/Enums/Configuration/request-type.enum';
import { AppSettings } from 'app/app-settings/app-settings';
import { SendMailParams } from 'app/Models/Entities/Commun/SendMailParams';
import { GlobalInstances } from '../../app.module';

@Injectable()
export class GenericRepository implements IGenericRepository<Object> {

	constructor() {
	}

	delete(url: string, idDoc: any) {
		return GlobalInstances.httpClient.delete(`${AppSettings.API_ENDPOINT}${url}/${idDoc}/Delete`,
			AppSettings.RequestOptions(RequestType.JSON));
	}
	deleteAll(url: string) {
		return GlobalInstances.httpClient.delete(`${AppSettings.API_ENDPOINT}${url}`, AppSettings.RequestOptions(RequestType.JSON));
	}
	getAll(url: string) {
		return GlobalInstances.httpClient.get<Object>(`${AppSettings.API_ENDPOINT}${url}`, AppSettings.RequestOptions(RequestType.JSON));
	}

	// getAllPagination(url: string, filterOption: IFilterOption) {
	getAllPagination(url: string, filterOption: any) {
		return GlobalInstances.httpClient.post<Object>(`${AppSettings.API_ENDPOINT}${url}`,
			filterOption, AppSettings.RequestOptions(RequestType.JSON));
	}

	getById(url: string, id: any) {
		const rout = (id === undefined) ? `${AppSettings.API_ENDPOINT}${url}` : `${AppSettings.API_ENDPOINT}${url}/${id}`;
		return GlobalInstances.httpClient.get(rout, AppSettings.RequestOptions(RequestType.JSON));
	}

	update<Object>(url: string, body, idDoc: any) {
		const request = (typeof body === 'object') ? RequestType.JSON : RequestType.TEXT;
		const res = `${AppSettings.API_ENDPOINT}${url}/${idDoc}/Update`;
		return GlobalInstances.httpClient.put<any>(res, body, AppSettings.RequestOptions(request));
	}
	updatePublishing<Object>(url: string, body, idDoc: any) {
		const request = (typeof body === 'object') ? RequestType.JSON : RequestType.TEXT;
		const res = `${AppSettings.API_ENDPOINT}${url}`;
		return GlobalInstances.httpClient.put<any>(res, body, AppSettings.RequestOptions(request));
	}

	updateAll(url: string, body) {
		const request = (typeof body === 'object') ? RequestType.JSON : RequestType.TEXT;
		return GlobalInstances.httpClient.put<any>(`${AppSettings.API_ENDPOINT}${url}`, body, AppSettings.RequestOptions(request));
	}

	create<Object>(url: string, body) {
		const request = (typeof body === 'object') ? RequestType.JSON : RequestType.TEXT;
		return GlobalInstances.httpClient.post<any>(`${AppSettings.API_ENDPOINT}${url}`, body, AppSettings.RequestOptions(request));
	}

	sendMail(url: string, id: string, emailOptions: SendMailParams) {
		return GlobalInstances.httpClient.post<any>(`${AppSettings.API_ENDPOINT}${url}/${id}/Email`,
			emailOptions, AppSettings.RequestOptions(RequestType.JSON));
	}

	visualPdf(url: string, id: string) {
		return GlobalInstances.httpClient.post<any>(`${AppSettings.API_ENDPOINT}${url}/${id}/Export`,
			{
				'exportType': 'pdf'
			}, AppSettings.RequestOptions(RequestType.JSON));
	}

}
