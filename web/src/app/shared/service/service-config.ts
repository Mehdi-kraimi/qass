import { GenericRepository } from '../repository/generic-repository';

export const repositoryConfig = { provide: 'IGenericRepository', useClass: GenericRepository };
