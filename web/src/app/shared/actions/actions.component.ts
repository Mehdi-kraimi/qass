import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';

@Component({
	selector: 'app-actions',
	templateUrl: './actions.component.html',
	styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit {

	//#region Properties

	@Input() opened = true;
	@Input() actions: { label: string, icon: string, color: string, action: () => {}, appear: () => {} }[] = [];
	@Input() document = null;
	@Input() title = 'Actions';
	@Output() toggeled = new EventEmitter<boolean>();
	@Input() toggle = true;

	//#endregion

	//#region Lifecycle

	constructor(
		public translate: TranslateService,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}
	ngOnInit(): void {
		this.toggeled.emit(this.opened);
	}

	//#endregion

	//#region Events

	onToggle(): void {
		this.opened = !this.opened;
		this.toggeled.emit(this.opened);
	}

	//#endregion

	//#region Methods

	//getIcon = (icon: string) => `./assets/app/images/Icons/actions/${icon}.svg`;

	//#endregion
}
