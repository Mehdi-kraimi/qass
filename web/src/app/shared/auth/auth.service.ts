import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
	token: string;

	constructor() { }

	signupUser() {	}

	signinUser() {
	}

	logout() {
		this.token = null;
	}

	getToken() {
		return this.token;
	}

	isAuthenticated() {
		return true;
	}
}
