import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
	selector: 'show-image',
	templateUrl: './show-image.component.html',
	styleUrls: ['./show-image.component.scss']
})
export class ShowImageComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<ShowImageComponent>,
		@Inject(MAT_DIALOG_DATA) public data,

	) { }

	ngOnInit() {
	}

	close(): void {
		this.dialogRef.close();
	}


}
