import { trigger, state, transition, style, animate } from '@angular/animations';

export const textareaAnimations = [
	trigger('toggle', [
		state('sm-hidden', style({
			height: 0,
			margin: 0,
			padding: 0,
			opacity: 0,
			overflow: 'hidden'
		})),
		state('lg-hidden', style({
			height: 0,
			margin: 0,
			padding: 0,
			opacity: 0,
			overflow: 'hidden'
		})),
		state('sm-visible', style({
			height: '30px',
			margin: '10px 0',
			opacity: 1
		})),
		state('lg-visible', style({
			height: '55px',
			margin: '10px 0',
			opacity: 1
		})),
		transition('sm-hidden <=> sm-visible', [
			animate('0.3s')
		]),
		transition('lg-hidden <=> lg-visible', [
			animate('0.3s')
		])
	]),
	trigger('flip', [
		state('down', style({
			transform: 'rotateZ(0deg)'
		})),
		state('up', style({
			transform: 'rotateZ(180deg)'
		})),
		transition('down <=> up', [
			animate('0.2s')
		])
	])
];
