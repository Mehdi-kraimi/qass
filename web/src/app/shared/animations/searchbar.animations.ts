import { trigger, state, transition, style, animate } from '@angular/animations';

export const searchbarAnimations = [
	trigger('fadeIn', [
		state('hidden', style({
			opacity: 0
		})),
		state('visible', style({
			opacity: 1
		})),
		transition('hidden <=> visible', [
			animate('0.4s ease-in-out')
		])
	])
];
