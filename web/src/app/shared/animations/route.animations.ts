import { trigger, transition, style, animate, query, group } from '@angular/animations';

export const routeAnimations = trigger('routeAnimations', [
	transition('* <=> visuel', [group([
		query(
			':enter',
			[
				style({
					transform: 'rotateY(180deg)',
					'backface-visibility': 'hidden'
				}),
				animate(
					'0.5s ease-in-out',
					style({
						transform: 'rotateY(0deg)',
						'backface-visibility': 'hidden'
					})
				)
			],
			{
				optional: true
			}
		),
		query(
			':leave',
			[
				style({
					transform: 'rotateY(0deg)'
				}),
				animate(
					'0.5s ease-in-out',
					style({
						transform: 'rotateY(180deg)'
					})
				)
			],
			{
				optional: true
			}
		)
	])
	])
	// transition('* <=> *', [group([
	// 	query(
	// 		':enter',
	// 		[
	// 			style({
	// 				transform: 'translateX(100%)'
	// 			}),
	// 			animate(
	// 				'0.3s ease-in-out',
	// 				style({
	// 					transform: 'translate(0%)'
	// 				})
	// 			)
	// 		],
	// 		{
	// 			optional: true
	// 		}
	// 	),
	// 	query(
	// 		':leave',
	// 		[
	// 			style({
	// 				transform: 'translateX(0%)'
	// 			}),
	// 			animate(
	// 				'0.3s ease-in-out',
	// 				style({
	// 					transform: 'translate(-100%)'
	// 				})
	// 			)
	// 		],
	// 		{
	// 			optional: true
	// 		}
	// 	)
	// ])
	// ])
]);
