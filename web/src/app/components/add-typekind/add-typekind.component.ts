import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { ACTION_API, ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { AppSettings } from 'app/app-settings/app-settings';
import { FormBuilder } from '@angular/forms';

export declare var swal: any;
declare var jQuery: any;

@Component({
	selector: 'app-add-add-typekind',
	templateUrl: './add-typekind.component.html',
	styleUrls: ['./add-typekind.component.scss']
})
export class AddTypekindComponent implements OnInit {

	docType = '';
	processing = false;
	missionKind;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	document;
	observation = '';
	form;
	show;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<AddTypekindComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) {
		this.missionKind = this.data.missionKind;
		this.show = this.data.show;
		this.createForm();
	}

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.createForm();
	}

	close() { this.dialogRef.close() }

	async submit() {
		const res = {
			'id': this.data.selected && this.data.selected.id ? this.data.selected.id : Math.floor((1 + Math.random()) * 0x10000),
			'value': this.form.value.value,
			'missionKind': this.missionKind
		}
		await this.service.updateAll(ApiUrl.typesMission, res).toPromise();
		this.dialogRef.close(this.form.value.value);
	}

	createForm(): void {
		this.form = this.fb.group({
			value: [this.data.selected && this.data.selected.value ? this.data.selected.value : '']
		});
	}

	get f() { return this.form.controls; }

}
