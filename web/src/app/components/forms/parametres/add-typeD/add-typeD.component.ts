import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';

declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'add-typeD',
	templateUrl: './add-typeD.component.html',
	styleUrls: ['./add-typeD.component.scss']
})
export class AddtypeDComponent implements OnInit {

	title: string;
	form: FormGroup;
	documents = [];

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<AddtypeDComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any

	) { }

	async ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.createForm();
		await this.getAll();
		if (this.data.selected) {
			this.setData();
			this.title = 'Modifier';
		} else {
			this.title = 'Ajouter'
		}
	}

	close() { this.dialogRef.close() }

	async getAll() {
		this.documents = (await this.service.getAll(ApiUrl.configDocumentType).toPromise()).value;
	}


	submit() {
		if (this.form.valid) {
			if (this.documents.find(m => m.value === this.form.value.nom) && !this.data.show) {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.modeRgelementExiste, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				})
				return;
			}

			let res = {};
			if (this.data.selected) {
				this.data.selected.value = this.form.value.nom;
				res = this.data.selected;
			} else {
				res = {
					'value': this.form.value.nom,
					'isDefault': false
				}
			}

			this.service.updateAll(ApiUrl.configDocumentType, res).subscribe(res => {
				if (res) {
					toastr.success(this.translate.instant(this.data.selected ? 'toast.update-sucsess' : 'toast.add-sucsess'), '',
						{ positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					this.dialogRef.close(this.form.value);
				}
			});

		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

	createForm(): void {
		this.form = this.fb.group({
			nom: ['', [Validators.required]]
		});
	}

	setData() {
		this.form.patchValue({
			nom: this.data.selected.value,
		});
	}

	get f() { return this.form.controls; }
}
