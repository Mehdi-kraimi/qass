import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreplanifierFormComponent } from './areplanifier-form.component';

describe('AreplanifierFormComponent', () => {
  let component: AreplanifierFormComponent;
  let fixture: ComponentFixture<AreplanifierFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreplanifierFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreplanifierFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
