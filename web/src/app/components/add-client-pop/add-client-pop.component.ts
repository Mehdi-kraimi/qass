import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Contact } from 'app/Models/Entities/Commun/Contact';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { AppSettings } from 'app/app-settings/app-settings';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'app/Models/Entities/User';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { ClientType } from 'app/Enums/TypeClient.enum';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

declare var toastr: any;
declare var jQuery: any;

@Component({
	selector: 'app-add-client-pop',
	templateUrl: './add-client-pop.component.html',
	styleUrls: ['./add-client-pop.component.scss']
})
export class AddClientPopComponent implements OnInit {

	// tslint:disable-next-line:no-output-rename
	@Output('retrunClient') retrunNewClient = new EventEmitter();
	form;
	contacts: Contact[] = [];
	adresses: Adresse[] = [];
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	agents: User[] = [];
	groupes: Groupe[] = [];
	initialisation = {
		groupe: false,
		agent: false,
		code: false
	}
	clientType: typeof ClientType = ClientType;
	CodeComptable
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		public dialogRef: MatDialogRef<AddClientPopComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { docType: string },
		private translate: TranslateService) {

		this.form = this.fb.group({
			firstName: [null, [Validators.minLength(2), Validators.required]],
			lastName: [''],
			reference: [null, [Validators.required], this.CheckUniquecode.bind(this)],
			phoneNumber: [null, [Validators.minLength(10), Validators.pattern(AppSettings.regexPhone)]],
			fax: [null, [Validators.pattern(AppSettings.regexPhone)]],
			email: [null, [Validators.pattern(AppSettings.regexEmail)]],
			website: [null, [Validators.pattern(AppSettings.regexURL)]],
			siret: [null],
			intraCommunityVAT: [null],
			accountingCode: [null],
			groupeId: [null],
			type: [this.clientType.Particular, [Validators.required]],
			street: [null],
			complement: [null],
			city: [null],
			postalCode: [null],
			department: [null],
			countryCode: [null],
		});

		if (this.isFournisseur()) {
			this.form.get('street').setValidators([Validators.required]);
			this.form.get('city').setValidators([Validators.required]);
			this.form.get('postalCode').setValidators([Validators.required]);
		}

	}

	async ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.CodeComptable = await this.GetCodeComptable();

		this.generatecode();
	}


	CheckUniquecode(control: FormControl) {
		if (control.value !== '') {
			const type = this.isClient() ? ApiUrl.Client : ApiUrl.Fournisseur;
			const promise = new Promise((resolve, reject) => {
				this.service
					.getById(type + ApiUrl.CheckReference, control.value)
					.subscribe(res => {
						if (!res) {
							resolve({ CheckUniquecode: true });

						} else {
							resolve(null);
						}
					});
			});
			return promise;
		}
	}

	removeContact(event) {
		this.contacts.splice(event.contactIndex, 1);
	}

	removeAdresse(i) {
		this.adresses.splice(i, 1);
	}

	submit() {

		if (this.isClient() && this.adresses.length === 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.adresseRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			return
		}

		if (this.isClient() && this.adresses.filter(x => x.isDefault).length === 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.adresseDefaultRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
			return
		}
		if (this.form.valid) {

			this.translate.get('add').subscribe(text => {
				let values = this.form.value;
				values = AppSettings.ConvertEmptyValueToNull(values)
				values['contactInformations'] = this.contacts;
				if (this.isFournisseur()) { values['address'] = this.setAdresse(); }
				if (this.isClient()) { values['addresses'] = this.adresses; }
				const type = this.isClient() ? ApiUrl.Client : ApiUrl.Fournisseur;
				this.service.create(type + ACTION_API.create, values).subscribe(res => {
					if (res) {
						this.dialogRef.close(res.value);
						this.form.reset();
						this.contacts = [];
						this.adresses = [];
						toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					}
				});
			})
		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

	setAdresse() {
		return {
			'designation': '',
			'department': this.form.controls['department'].value,
			'street': this.form.controls['street'].value,
			'complement': this.form.controls['complement'].value,
			'city': this.form.controls['city'].value,
			'postalCode': this.form.controls['postalCode'].value,
			'countryCode': this.form.controls['countryCode'].value,
			'isDefault': true
		}
	}



	get f() { return this.form.controls; }

	generatecode() {
		if (!this.initialisation.code) {
			const type = this.isClient() ? this.typeNumerotation.Client : this.typeNumerotation.fournisseur
			this.service.getAll(ApiUrl.configReference + type + '/reference')
				.subscribe(res => {
					this.form.controls['reference'].setValue(res);
					this.initialisation.code = true;
				});
		}
	}
	GetCodeComptable(): Promise<number> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.configCategory).subscribe(
				res => {
					resolve(res.value.find(x => + x.id === 11)['code']);
				},
				err => {
					reject(err);
				}
			);
		});
	}
	isClient() { return this.data.docType === ApiUrl.Client }
	isFournisseur() { return this.data.docType === ApiUrl.Fournisseur }

	getListContacts(contacts) {
		this.contacts = contacts;
	}

	getListAdressesAAA(adresses) {
		this.adresses = adresses;
	}

	generateCodeComptable() {
		this.form.controls['accountingCode'].setValue(this.CodeComptable + this.f.firstName.value.replace(/ /g, ''));
	}

	getGroupes() {
		this.service.getAll(ApiUrl.Groupe).subscribe(res => {
			this.groupes = res.value;
			this.initialisation.groupe = true;
		}, err => { console.log(err); })
	}


	close() { this.dialogRef.close(); }
}
