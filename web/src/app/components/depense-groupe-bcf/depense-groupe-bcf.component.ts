import { Component, OnInit, Input, Inject } from '@angular/core';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { Supplier } from 'app/Models/Entities/Contacts/Supplier';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { StatutBonCommandeFournisseur } from 'app/Enums/Statut/StatutBonCommandeFournisseur.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { LocalElements } from 'app/shared/utils/local-elements';
declare var jQuery: any;
declare var toastr: any;
@Component({
	selector: 'app-depense-groupe-bcf',
	templateUrl: './depense-groupe-bcf.component.html',
	styleUrls: ['./depense-groupe-bcf.component.scss']
})
export class DepenseGroupeBCFComponent implements OnInit {

	bonCommandeFournisseurUnSelected = [];
	bonCommandeFournisseur = [];
	idChantier;
	idFournisseur;
	finished = true;
	search = "";
	page = 1;
	totalPage = 0;
	chantier: Workshop[] = []
	chantierSelected: Workshop[] = []
	@Input("fournisseurs") fournisseurs: Supplier[] = []
	fournisseurSelected: Supplier[] = []
	@Input("chantiers") chantiers: Workshop[] = [];

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private router: Router,
		private translate: TranslateService,
	) { }
	ngOnInit() {
	}
	/**
	 * Choisir Chantier
	 */
	getBCFByChantier(info: { idChantier: number, idFournisseur: number }): void {
		this.idChantier = info.idChantier;
		this.idFournisseur = info.idFournisseur;
		this.bonCommandeFournisseurUnSelected = []
		this.bonCommandeFournisseur = []
		this.bonCommandeFournisseurList();
		jQuery("#depenseGroup").modal("show");
	}
	/**
	 * Récupérer la liste des bon des commandes Fournisseur
	 */
	bonCommandeFournisseurList() {
		this.service
			.getAllPagination(ApiUrl.BonCommandeF, this.getFilters())
			.subscribe((res) => {
				var listBonCommandeFournisseurs = res.value.filter(value =>
					this.bonCommandeFournisseurUnSelected.filter(x => x.id == value.id).length == 0);
				this.totalPage = res.rowsCount;
				listBonCommandeFournisseurs.forEach(BCFournisseur => {
					this.bonCommandeFournisseurUnSelected.push(BCFournisseur)
				})
			});
	}

	getFilters() {
		return {
			'externalPartnerId': this.idFournisseur,
			'workshopId': this.idChantier,
			'status': [StatutBonCommandeFournisseur.Encours],
			'dateStart': null,
			'dateEnd': null,
			'searchQuery': this.search,
			'page': this.page,
			'pageSize': 5,
			'sortDirection': 'Ascending',
			'orderBy': 'reference'
		};
	}

	/**
	 * On cas de scroll dans popup de BCFournisseur
	 */
	onScroll() {
		this.page++;
		this.bonCommandeFournisseurList();
		if (this.totalPage = this.page) {
			this.finished = true;
		}
	}

	/**
	 * Recherche des BCFournisseur dans popup
	 */
	searchBCF() {
		this.page = 1;
		this.bonCommandeFournisseurUnSelected = [];
		this.bonCommandeFournisseurList();
	}
	/**
	  * Selectioné bon commande Fournisseur
	  */
	addBonCommandeFournisseur(index) {
		let bonCommadeFournisseur = this.bonCommandeFournisseurUnSelected[index]
		this.bonCommandeFournisseur.push(bonCommadeFournisseur)
		this.bonCommandeFournisseurUnSelected.splice(index, 1)
	}
	/**
	 * Déselectioné bon commande Fournisseur
	 */
	removeBonCommandeFournisseur(index) {
		let bonCommadeFournisseur = this.bonCommandeFournisseur[index]
		this.bonCommandeFournisseurUnSelected.push(bonCommadeFournisseur)
		this.bonCommandeFournisseur.splice(index, 1)
	}
	/**
	 * save Bon Commande Fournisseur
	 */
	saveBonCommandeFournisseur() {
		if (this.bonCommandeFournisseur.length == 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.SelectionneBonCommandeRequired, '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			})
		}
		if (this.bonCommandeFournisseur.length > 0) {
			let depense = {}
			let articles = [];
			let prestations: any
			let idsBonCommandeFournisseur = [];
			this.bonCommandeFournisseur.forEach(bonCommandeFournisseur => {
				idsBonCommandeFournisseur.push(bonCommandeFournisseur.id);
				prestations = bonCommandeFournisseur.orderDetails.lineItems;
				prestations.forEach(element => articles.push(element));
				depense['workshopId'] = bonCommandeFournisseur.workshop !== null ? bonCommandeFournisseur.workshop.id : null;
				depense['workshop'] = bonCommandeFournisseur.workshop;
				depense['supplierId'] = bonCommandeFournisseur.supplier.id;
				depense['supplier'] = bonCommandeFournisseur.supplier
			})
			depense['orderDetails'] = {
				'lineItems': articles
			}

			const res = {
				depense,
				idsBonCommandeFournisseur
			}

			localStorage.setItem(LocalElements.docType, ApiUrl.Depense);
			localStorage.setItem(LocalElements.docTypeGenerer, 'DepenseGroupe');
			localStorage.setItem('DepenseGroupe', JSON.stringify(res));
			this.router.navigate(['/depense/generer-create/', '']);
			jQuery("#depenseGroup").modal("hide");
		}
	}

}
