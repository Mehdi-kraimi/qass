import { Component, OnInit, Inject } from '@angular/core';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { StatutAvoir } from 'app/Enums/Statut/StatutAvoir.Enum';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { Client, ClientMinimalInfo } from 'app/Models/Entities/Contacts/Client';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { OrderProductsDetailsType } from 'app/Enums/productsDetailsType.enum';
import { OrderDetails } from 'app/Models/Entities/Commun/OrderDetails';
import { TypeValue } from 'app/Enums/typeValue.Enums';
declare var toastr: any;
@Component({
	selector: 'app-add-facture-cloture',
	templateUrl: './add-facture-cloture.component.html',
	styleUrls: ['./add-facture-cloture.component.scss']
})
export class AddFactureClotureComponent implements OnInit {

	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	statutAvoir: typeof StatutAvoir = StatutAvoir;
	statutFacture: typeof StatutFacture = StatutFacture;
	prestations: any;
	tva: any;
	editorConfig: AngularEditorConfig = { editable: true, spellcheck: true, height: '8rem', translate: 'yes', };
	chantierPreDefini = null;
	dateLang: any;
	emitter: any = {};
	remise = 0;
	typeRemise = TypeValue.Amount;
	puc = 0;
	prorata = 0;
	retenueGarantie = 0;
	delaiGarantie = 0;
	idicheIntervention
	idFichesTravail;
	loading;
	reference;
	datafacture;
	idDevis = null;
	processing = false;
	creationForm: any = null;
	devisInfos;
	adresseFacturation: /*Adresse*/any;
	idChantier: number = null
	pourcentageCloture = 100;
	articles = [];
	listFactureSituation: Invoice[] = [];
	listFactureAcompte: Invoice[] = [];
	documentConfiguration;
	totalTTcSotuation = 0;
	totalHTSituation = 0;
	totalTTcAcompte = 0;
	totalHTAcompte = 0;
	pourcentageIn = 0;
	totalAvancementSituation = 0;
	detailLot = false;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang); this.translate.setDefaultLang(AppSettings.lang);
	}

	/*---------------------------------------------------------------*/
	/**
	 * initialisations of the component
	 */
	/*---------------------------------------------------------------*/
	async ngOnInit() {

		this.processing = true;
		this.translate.get('datePicker').subscribe(text => this.dateLang = text);
		this.buildCreationForm();
		await this.getDocumentConfiguration();
		await this.initializeCreationForm();
		this.getDevisById(this.route.snapshot.params.id).then(async (res) => {
			this.devisInfos = res.value;
			this.detailLot = this.devisInfos.lotDetails === true ? true : false;

			this.articles = this.devisInfos.orderDetails.lineItems;
			//this.getPrestationAcompteSituation(this.devisInfos.situations);
			//this.pourcentageIn = this.devisInfos.situations.reduce((x, y) => x + y.situation, 0);
			// tslint:disable-next-line: max-line-length
			this.pourcentageIn = this.devisInfos.situations.filter(e => e.status != this.statutFacture.Annule && (e.typeInvoice === +TypeFacture.Situation || e.typeInvoice === +TypeFacture.Acompte)).reduce((x, y) => x + y.situation, 0);

			this.totalAvancementSituation = this.devisInfos.orderDetails.totalTTC * (this.pourcentageIn / 100);
			await this.setDefaultValueFacture();

			this.setAdresse();
			this.processing = false;
		});
		this.idChantier = await this.getParamsFromRoute('idChantier') as number;

	}

	click(e) {
		this.detailLot = e;
	}

	/**
	 * return the type of the document
	 */
	getParamsFromRoute(paramName: string): Promise<string | number> {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => resolve(params[paramName]))
		});
	}
	setAdresse() {
		this.adresseFacturation = this.devisInfos.addressIntervention as Adresse;
	}
	getAdresseDesignation(data): string {
		try {
			const adresse = data.filter(x => x.isDefault);
			return adresse.length !== 0 ? adresse[0].designation : '';
		} catch (err) {
			return '';
		}
	}
	/**
	* @description build the creation Form
	*/
	buildCreationForm(): void {
		this.creationForm = this.formBuilder.group({
			reference: [null, [Validators.required]],
			dateCreation: [new Date(), [Validators.required]],
			dateEcheance: [null, [Validators.required]],
			object: [],
			note: [],
			conditionRegelement: [],
			client: [null],
			pourcentage: [null, [Validators.required, Validators.min(1), Validators.max(100)]],
			detailLot: []
		});
	}



	/**
	* @description initialize the creation Form
	*/
	async initializeCreationForm(): Promise<void> {
		// set the defaults values from the configuration
		this.creationForm.controls['reference'].setValue(await this.generateReference());
		this.creationForm.controls['dateCreation'].setValue(new Date);
		if (this.documentConfiguration) {
			const dateToday = new Date();
			//  tslint:disable-next-line:radix
			dateToday.setDate(new Date().getDate() + parseInt(this.documentConfiguration['validity'].toString()));
			this.creationForm.controls['dateEcheance'].setValue(dateToday)
		} else {
			this.creationForm.controls['dateEcheance'].setValue(new Date);
		}
	}

	/**
	 * @description return a reference for the new insertion
	 */
	generateReference(): Promise<string> {
		return new Promise((reslove, reject) => {
			this.service.getAll(ApiUrl.configReference + 'invoice' + '/reference').subscribe(
				res => {
					reslove(res as string);
				},
				err => {
					console.error(err);
				}
			);
		});
	}

	/**
	 * @description return the defaults values from the configuration
	 */
	async setDefaultValueFacture() {
		this.creationForm.controls['reference'].setValue(await this.generateReference());
		//   set the defaults values from the configuration
		this.creationForm.controls['note'].setValue(this.devisInfos.client['note']);
		this.creationForm.controls['conditionRegelement'].setValue(this.devisInfos.client['paymentCondition']);
		this.creationForm.controls['object'].setValue(this.devisInfos['purpose']);
		//   set default date validite
		if (this.documentConfiguration.validity) {
			const dateToday = new Date();
			//  tslint:disable-next-line:radix
			dateToday.setDate(dateToday.getDate() + parseInt(this.documentConfiguration['validity'].toString()));
			this.creationForm.controls['dateEcheance'].setValue(dateToday);
		}
	}

	async getDocumentConfiguration() {
		this.service.getAll(ApiUrl.configDocument).subscribe(res => {
			if (res) {
				this.documentConfiguration = JSON.parse(res).find(element => element.documentType === TypeNumerotation.facture);
			}
		}, reject => console.error(reject));
	}

	checkFormIsValid(statut: StatutFacture): boolean {
		let valid = true;
		for (const key in this.creationForm.controls) {
			if (
				this.creationForm.controls[key].errors !== null
				&&
				(statut !== this.statutFacture.Cloture || (key !== 'pourcentage' && statut === this.statutFacture.Cloture))
			) {
				valid = false;
			}
		}
		return valid;
	}


	calcultHt() {

		this.totalHTSituation = this.totalHTSituation === undefined ? 0 : this.totalHTSituation;
		this.totalHTAcompte = this.totalHTAcompte === undefined ? 0 : this.totalHTAcompte;
		return (this.devisInfos.totalHt - (this.totalHTSituation + this.totalHTAcompte))
	}
	calculTTC() {
		this.totalTTcSotuation = this.totalTTcSotuation === undefined ? 0 : this.totalTTcSotuation;
		this.totalTTcAcompte = this.totalTTcAcompte === undefined ? 0 : this.totalTTcAcompte;
		return (this.devisInfos.total - (this.totalTTcSotuation + this.totalTTcAcompte))
	}

	getListFacture(facture: Invoice[]) {
		if (facture.length > 0) {
			this.listFactureSituation = facture.filter(x => x.typeInvoice === TypeFacture.Situation)
			this.listFactureAcompte = facture.filter(x => x.typeInvoice === TypeFacture.Acompte)
			this.listFactureSituation.forEach(situation => {
				this.totalTTcSotuation += situation.orderDetails.totalTTC,
					this.totalHTSituation += situation.orderDetails.totalHT
			});
			this.getPrestationAcompteSituation(this.listFactureSituation);
			this.getPrestationAcompteSituation(this.listFactureAcompte);

			this.listFactureAcompte.forEach(acompte => {
				this.totalTTcAcompte += acompte.orderDetails.totalTTC,
					this.totalHTAcompte += acompte.orderDetails.totalHT
			});
		}

	}

	calcTotalHtToSave(status: StatutFacture, prestationTotalHt: number): number {
		if (status === this.statutFacture.Cloture) {
			const situation = JSON.parse(this.devisInfos.situation) as { idFacture: number, pourcentage: number, resteAPayer: number }[];
			return (situation.length === 0) ? this.devisInfos.totalHt : (situation[situation.length - 1].resteAPayer);
		} else {
			return prestationTotalHt;
		}
	}

	calcTotalTtcToSave(status: StatutFacture, prestationTotalTtc: number, totalHt: number, tva: number) {
		if (status === this.statutFacture.Cloture) {
			return totalHt * ((tva / 100) + 1)
		} else {
			return prestationTotalTtc;
		}
	}
	getPrestationAcompteSituation(listFacture) {
		if (listFacture.length > 0) {

			listFacture.forEach(element => {
				this.articles.push({
					'product': {
						discount: {
							'value': 0,
							'type': TypeValue.Amount,
						},
						'description': '',
						'designation':
							element.purpose,
						'lotProduits': null,
						'name': element.purpose,
						'totalHT': element.totalHT,
						'cout_vente': 1,
						'materialCost': -element.totalHT,
						'hourlyCost': 1,
						'totalHours': 0,
						'prixHt': element.totalHT,
						'vat': element.situation,
						'totalTTC': element.totalTTC,
						'categorieId': 10,
						'quantity': 1,
						'qte': 1
					},
					'quantity': 1,
					'type': 1,
					'remise': 0
				});
			});
		}
	}


	async getDevisById(idDevis: string) {
		return await this.service.getById(ApiUrl.Devis, idDevis).toPromise();
	}

	/*---------------------------------------------------------------*/
	/**
	 * @section  Logique de sauvegarde des données
	 */
	/*---------------------------------------------------------------*/

	/**
	 * @description la fonction principale pour sauvegarder la nouvelle facture
	 * @param statut statut du facture qui on veut sauvegarder (cloture/brouillon/en cours)
	 */
	async add(statut: StatutFacture) {
		// afficher l'animation de chargement des données
		this.processing = true;
		// vérifier si tous les champs obligatoir est remplie
		if (!this.checkFormIsValid(statut)) {
			// récupère la traduction des messages d'erreurs
			const translatation = await this.getTranslateByKey('errors');
			// afficher un message d'erreur
			toastr.warning(translatation.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
			// cacher l'animation de chargement des données
			this.processing = false;
			// sortir de la fonction
			return;
		}

		// création le corp de la requête du creation
		this.prestations = await this.getDataFromArticlesComponet();
		if (+this.prestations.totalHt <= 0) {
			const translatation = await this.getTranslateByKey('errors');
			toastr.warning(translatation.factureSituationTotalHt, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
			this.processing = false;
			return;
		}
		const facturecreationBody = await this.createBodyRequest(statut);
		// send request to the server
		this.service.create(ApiUrl.Invoice + ACTION_API.create, facturecreationBody.facture).subscribe(
			async res => {
				if (res && res.isSuccess) {
					//  récupère la traduction des messages d'ajout
					const translatation = await this.getTranslateByKey('addFacture');
					//  affcihe un message du succès
					toastr.success(translatation.msg, translatation.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					//  redirigé au detail de la nouvelle insertion
					this.navigateToDevisDetail();

				} else {
					// récupère la traduction des messages d'erreurs
					const translatation = await this.getTranslateByKey('errors');
					toastr.warning(translatation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
				}
			},
			async err => {
				// récupère la traduction des messages d'erreurs
				const translatation = await this.getTranslateByKey('errors');
				toastr.warning(translatation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
			}, () => {
				// cacher l'animation de chargement des données
				this.processing = false;
			});
	}

	navigateToDevisDetail() {
		if (this.idChantier === null || this.idChantier === undefined) {
			this.router.navigate([`/devis/detail/${this.devisInfos.id}`],
				{
					queryParams: {
						'section': 'factureSituation',
					}
				});
		} else {
			this.router.navigate([`/chantiers/${this.idChantier}/documents/3/detail/${this.devisInfos.id}`],
				{
					queryParams: {
						'section': 'factureSituation',
					}
				});
		}
	}
	getDataFromArticlesComponet(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.emitter.getDateToSave((res) => {
				resolve(res);
			})
		})
	}
	async createBodyRequest(status: StatutFacture): Promise<any> {

		const formValue = this.creationForm.value;
		const facture: Invoice = new Invoice();
		facture.reference = formValue.reference;
		//	const pourcentageFactureSituation = this.devisInfos.situations.filter(e => e.typeInvoice === TypeFacture.Situation)
		//.reduce((x, y) => x + y.situation, 0);
		facture.situation = 100 - this.pourcentageIn;
		facture.workshopId = this.devisInfos.workshop !== null ? this.devisInfos.workshop.id : null;
		facture.creationDate = AppSettings.formaterDatetime(formValue.dateCreation);
		facture.dueDate = AppSettings.formaterDatetime(formValue.dateEcheance);

		facture.status = StatutFacture.Encours;
		facture.typeInvoice = TypeFacture.Cloturer;
		facture.purpose = formValue.object;
		facture.note = formValue.note;
		const client: Client = this.devisInfos.client;
		facture.clientId = client.id;
		facture.client = client;
		facture.addressIntervention = client['addresses'].find(x => x.isDefault === true);
		facture.paymentCondition = formValue.conditionRegelement;
		//facture.lotDetails = formValue.detailLot === '1' ? true : false
		facture.lotDetails = this.detailLot;
		const orderDetails: OrderDetails = {
			'globalDiscount': {
				'value': this.prestations.remise,
				'type': this.prestations.typeRemise
			},
			'holdbackDetails': {
				'warrantyPeriod': this.prestations.delaiGarantie,
				'holdback': this.prestations.retenueGarantie,
				'warrantyExpirationDate': new Date(),
			},
			'globalVAT_Value': this.prestations.tvaGlobal,
			'puc': this.prestations.puc,
			'proportion': this.prestations.prorata,
			'productsDetailsType': OrderProductsDetailsType.List,
			'lineItems': this.prestations.prestation,
			'productsPricingDetails': {
				'totalHours': 0,
				'salesPrice': 0
			},
			'totalHT': this.prestations.totalHt,
			'totalTTC': this.prestations.totalTtc
		};

		facture.orderDetails = orderDetails;
		facture.quoteId = this.devisInfos.id;
		return {
			idDevis: this.devisInfos.id,
			//  situations: this.createFacturesSituationInfos(status, pourcentage),//  TODO Facture
			// situations: this.createFacturesSituationInfos(status, 100),
			facture: facture
		}
	}

	/*---------------------------------------------------------------*/
	/**
	 * @section  fonctions d'aide
	 */
	/*---------------------------------------------------------------*/
	getTranslateByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translatation => {
				resolve(translatation);
			});
		});
	}

	navigateToDevisList() {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => {
				const idChantier: number = params['idChantier'];
				const idDevis: number = params['id'];
				const url = idChantier === undefined ? `/devis` : `/chantiers/${idChantier}/documents/3/detail/${idDevis}`;
				this.router.navigate([url]);
			});
		});
	}


	get f() {
		return this.creationForm.controls;
	}

}
