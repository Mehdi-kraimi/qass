import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'add-ligne',
	templateUrl: './add-ligne.component.html',
	styleUrls: ['./add-ligne.component.scss']
})
export class AdddLigneComponent implements OnInit {

	title: string;
	public form: FormGroup;
	show = false;

	constructor(
		private fb: FormBuilder,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<AdddLigneComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.createForm();
	}

	close() { this.dialogRef.close() }

	submit() {
		this.dialogRef.close(this.form.value)
	}

	createForm(): void {
		this.form = this.fb.group({
			description: [''],
			designation: [''],
		});
	}

	get f() { return this.form.controls; }
}
