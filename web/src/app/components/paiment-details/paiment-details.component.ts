import { Component, EventEmitter, Inject, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Calcule } from 'app/calcule/Calcule';
import { ICalcule } from 'app/calcule/ICalcule';
import { ACTION_API, ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { TypeCompte } from 'app/Enums/Parameters/TypeCompte.enum';
import { StaticModeReglement } from 'app/Enums/StaticModeReglement.Enum';
import { StatutChantier } from 'app/Enums/Statut/StatutChantier.Enum';
import { StatutDepense } from 'app/Enums/Statut/StatutDepense.Enum';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { StatutComptabilise } from 'app/Enums/StatutComptabilise.enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { Credit } from 'app/Models/Entities/Documents/Credit';
import { Expense } from 'app/Models/Entities/Documents/Expense';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { Modereglement } from 'app/Models/Entities/Parametres/Modereglement';
import { ParametrageCompte } from 'app/Models/Entities/Parametres/ParametrageCompte';
import { PaymentAdd } from 'app/Models/Model/PaymentAdd';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { LocalElements } from 'app/shared/utils/local-elements';
declare const toastr: any;
declare const jQuery: any;
declare const swal: any;

export enum Typepaiement {

	/// <summary>
	/// a payment for an expense
	/// </summary>
	Expense,
	/// <summary>
	/// a payment for an invoice
	/// </summary>
	Invoice,
	/// <summary>
	/// the payment is a Transfer of funds
	/// </summary>
	Transfer
}
@Component({
	selector: 'paiment-details',
	templateUrl: './paiment-details.component.html',
	styleUrls: ['./paiment-details.component.scss']
})
export class PaimentDetailsComponent implements OnInit, OnChanges {
	@Output() refresh = new EventEmitter();
	@Input() document: any;
	form;
	dateLang; //  translate datepicker
	modesRegelement: Modereglement[] = null;
	comptes: ParametrageCompte[] = null;
	loading = false;
	indexModified;
	statutFacture: typeof StatutFacture = StatutFacture;
	statutDepense: typeof StatutDepense = StatutDepense;
	statutComptabilise: typeof StatutComptabilise = StatutComptabilise
	typePaiement: typeof TypePaiement = TypePaiement;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	staticModeReglement: typeof StaticModeReglement = StaticModeReglement;
	ApiUrl: typeof ApiUrl = ApiUrl;
	calcule: ICalcule = new Calcule();
	reference;
	initAvoir = false;
	restPaye = 0;
	MontantRest;
	docType = '';
	idclient = null
	idchantier = null

	TotalTtcFacture = 0;
	restApayeNotRoundi = 0;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb?: FormBuilder,
		private translate?: TranslateService,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.createForm();
	}

	ngOnChanges() {
	}

	ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType)
		ManageDataTable.docType = this.docType;
	}
	//#region Form
	createForm() {
		this.form = this.fb.group({
			'montant': [null, [Validators.required], this.CheckValidPrice.bind(this)],
			'idCaisse': [null, [Validators.required]],
			'datePaiement': [null, [Validators.required]],
			'idModePaiement': [null, [Validators.required]],
			'description': [null],
			'createAvoir': [false]
		})
	}

	//  Vérifier si la valeur de montant saisi est valide
	CheckValidPrice(control: FormControl) {
		const montant = parseFloat(control.value)
		if (montant > 0) {
			const promise = new Promise((resolve, reject) => {
				const montantPaiementModifier = this.indexModified !== null ? this.document.payments.find(pai => pai.id = this.indexModified).amount : 0
				// tslint:disable-next-line: max-line-length
				const resPayer = montantPaiementModifier + this.document.orderDetails.totalTTC - this.document.payments.reduce((x, y) => x + y.amount, 0);
				if (AppSettings.formaterNumber(montant) <= AppSettings.formaterNumber(resPayer)) {
					resolve(null)
				} else {
					resolve({ CheckValidPrice: true })
				}
			});
			return promise;
		} else {
			const promise = new Promise((resolve, reject) => { resolve({ CheckValidPrice: true }) });
			return promise;
		}
	}

	//  Charger & clear form ajouter
	chargeLists() {
		this.GetCompte('');
		this.GetModeRegelement('');
		this.form.reset()
		this.indexModified = null;
		const text = this.docType === ApiUrl.Depense ? 'Paiement dépense N° ' : 'Paiement facture N° ';
		this.form.controls['description'].setValue(text + this.document.reference)
		this.form.controls['montant'].setValue(AppSettings.formaterNumber(this.getResterPayer()))
		this.form.controls['datePaiement'].setValue(new Date());
	}
	get f() { return this.form.controls; }

	//#endregion
	//#region fct server
	GetModeRegelement(search) {
		if (this.modesRegelement === null) {
			this.service.getAll(ApiUrl.configModeRegelemnt)
				.subscribe(
					res => {
						this.modesRegelement = res.value
					}
				)
		}
	}
	GetCompte(search) {
		if (this.comptes === null) {
			this.service.getAll(ApiUrl.configCompte)
				.subscribe(res => {
					this.comptes = res.value
				})
		}
	}
	//#endregion

	//#region Calcul

	getResterPayer() {
		if (this.document && this.document.orderDetails !== undefined) {
			this.restApayeNotRoundi = this.document.orderDetails.totalTTC - this.document.payments.reduce((x, y) => x + y.amount, 0);
			this.restPaye = this.document.orderDetails.totalTTC - this.document.payments.reduce((x, y) => x + y.amount, 0)
			if (this.restPaye < 0) {
				return 0;
			}
			return this.restPaye;
		} else {
			return 0;
		}
	}
	CompareMontantInDb(montant: number) {

		const reste = this.restApayeNotRoundi - montant;
		if ((reste > 0 && reste < 0.01) || reste < 0) {
			return this.restApayeNotRoundi;
		} else {
			return montant
		}
	}
	createPaiementAvoir(avoir: Credit) {
		this.loading = true;
		const paiement: PaymentAdd = new PaymentAdd();
		paiement.amount = avoir['totalTTC'] * (-1);
		paiement.datePayment = new Date(AppSettings.formaterDatetime(new Date().toString()));
		paiement.creditNoteId = avoir.id;
		paiement.operation = TypePaiement.CreditNotePayment;
		paiement.paymentMethodId = 1;
		if (this.docType === ApiUrl.Invoice) {
			paiement.type = Typepaiement.Invoice;
			paiement.description = 'Paiement facture N° ' + this.document.reference;

			paiement.invoices = [{
				amount: avoir['totalTTC'] * (-1),
				documentId: this.document.id
			}];
		}
		if (this.docType === ApiUrl.Depense) {
			paiement.description = 'Paiement depense N° ' + this.document.reference;
			paiement.type = Typepaiement.Expense;
			paiement.expenses = [{
				amount: avoir['totalTTC'] * (-1),
				documentId: this.document.id
			}];
		}


		paiement.operation = TypePaiement.CreditNotePayment;
		paiement.paymentMethodId = 1;

		this.service.create(ApiUrl.Payment + ACTION_API.create, paiement).subscribe(value => {
			this.loading = false;
			if (value) {
				this.refresh.emit('')
				this.successfulAdd(value.value)
			} else {
				this.errorMontantIncorrect()
			}
		}, err => {
			console.log(err);
			this.loading = false;
			this.errorServer()
		});

	}
	chargerModifierPaiement(index) {
		this.TotalTtcFacture = this.document.orderDetails.totalTTC;
		this.GetCompte('');
		this.GetModeRegelement('');
		this.indexModified = this.document.payments[index].id;
		const paiement = this.document.payments[index]
		this.form.controls['description'].setValue(paiement.description)
		this.form.controls['montant'].setValue(AppSettings.formaterNumber(paiement.amount))
		this.form.controls['idCaisse'].setValue(paiement.account.id.toString())
		this.form.controls['idModePaiement'].setValue(paiement.paymentMethod.id.toString())
		const datePaiement = new Date(paiement.datePayment.toString())
		this.form.controls['datePaiement'].setValue(datePaiement)
		jQuery('#ajouterPaiement').modal('show');
	}
	async savePaiement(indexModified) {
		if (this.form.valid) {
			this.loading = true;
			const values = this.form.value;
			const paiement: PaymentAdd = new PaymentAdd();
			paiement.description = values.description;
			paiement.operation = TypePaiement.Payment;
			paiement.amount = this.CompareMontantInDb(values['montant']);
			paiement.datePayment = values.datePaiement;
			paiement.paymentMethodId = +values['idModePaiement'];
			paiement.accountId = +values.idCaisse;
			if (this.docType === ApiUrl.Invoice) {
				paiement.creditNoteId = null;

				paiement.type = Typepaiement.Invoice;
				paiement.invoices = [{
					amount: this.CompareMontantInDb(values['montant']),
					documentId: this.document.id
				}];
			}
			if (this.docType === ApiUrl.Depense) {
				paiement.invoices = [];
				paiement.expenses = [{
					'documentId': this.document.id,
					'amount': this.CompareMontantInDb(values['montant']),
				}]
			}
			if (indexModified) {
				this.updatePaiment(paiement, indexModified)
			} else {
				this.createPaiement(paiement);
			}
		}
	}
	updatePaiment(paiement, indexModified) {
		this.service.update(ApiUrl.Payment, paiement, indexModified)
			.subscribe(arg => {
				if (arg.isSuccess) {
					this.successUpdate();
				} else {
					this.errorMontantIncorrect()
				}
				this.loading = false;
			}, err => {
				console.log(err);
				this.loading = false;
				this.errorServer()
			});
	}

	createPaiement(paiement) {
		this.service.create(ApiUrl.Payment + ACTION_API.create, paiement).subscribe(res => {
			this.loading = false;
			if (res) {
				this.successfulAdd(res.value)
			} else {
				this.errorMontantIncorrect()
			}
		}, err => {
			console.log(err);
			this.loading = false;
			this.errorServer()
		})
	}
	//#endregion

	//#region supprimer
	removePaiement(index) {
		const paiement = this.document.payments[index];
		this.translate.get('paiement.delete').subscribe(text => {
			swal({
				title: text.title,
				text: (paiement.operation === TypePaiement.GroupedPayment ? text.questionGroupe :
					(paiement.operation === TypePaiement.CreditNotePayment ? text.questionAvoirDeconste : text.question)),
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {

					this.loading = true;
					this.service.delete(ApiUrl.Payment, paiement.id).subscribe(res => {
						if (res) {
							this.loading = false;
							swal(text.success, '', 'success');

							// tslint:disable-next-line: max-line-length
							if (this.document['workshop'] != undefined && this.docType === ApiUrl.Invoice && this.document.status === this.statutFacture.Cloture && this.document['workshop'].status === StatutChantier.Termine) {
								this.retourStatutChantier(this.document)
							} else {
								this.refresh.emit('')

							}

						}
					}, err => {
						this.loading = false;
						swal(text.failed, '', 'warning');
					})
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});

	}
	//#endregion


	//#region msg
	successfulAdd(paiement?) {
		this.refresh.emit('')
		this.translate.get('paiement').subscribe(text => {
			jQuery('#ajouterPaiement').modal('hide');
			toastr.success(text.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			if (paiement !== null && paiement.operation === TypePaiement.CreditNotePayment) {
				toastr.success(text.avoirCreated + paiement.reference, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}
		})
	}

	successUpdate() {
		this.refresh.emit('')
		this.translate.get('paiement').subscribe(text => {
			jQuery('#ajouterPaiement').modal('hide');
			toastr.success(text.modifierSuccess, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		})
	}
	//  Montant incorrect
	errorMontantIncorrect() {
		this.translate.get('errors').subscribe(text => {
			toastr.warning(text.invalid, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			setTimeout(() => { location.reload(); }, 1000)
		})
	}
	//  Error server
	errorServer() {
		this.translate.get('errors').subscribe(text => {
			toastr.warning(text.errorServer, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		})
	}
	//#endregion

	retourStatutChantier(facture: any) {

		// this.chantierService.changeStatut({idChantier : facture.chantier.id,statutChantier : StatutChantier.Accepte  })
		this.translate.get('paiement.retourChantier').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {

				if (isConfirm) {

					this.loading = true;
					//  this.chantierService.changeStatut({ idChantier: facture.chantier.id, statutChantier: StatutChantier.Accepte }).subscribe(res => {
					//    if (res) {
					//      this.loading = false;
					//      swal(text.success, '', 'success');
					//      this.refresh.emit('')
					//    }
					//  }, err => {
					//    this.loading = false;
					//    swal(text.failed, '', 'warning');
					//  })
				} else {
					this.refresh.emit('')
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	onChangeMoyenPaiement(value) {
		if (value === StaticModeReglement.Avoir) {
			this.form.controls['idCaisse'].setValue(null)
			this.form.controls['idCaisse'].setValidators([])
			this.form.controls['idCaisse'].updateValueAndValidity()
		} else if (value === StaticModeReglement.Espece) {
			const caisse = this.comptes.filter(x => x.type === TypeCompte.caisse)[0];
			if (caisse !== null) {
				this.form.controls['idCaisse'].setValue(caisse.id.toString())
			}
		} else {
			this.form.controls['idCaisse'].setValidators([Validators.required])
			this.form.controls['idCaisse'].updateValueAndValidity()
		}
	}
}
