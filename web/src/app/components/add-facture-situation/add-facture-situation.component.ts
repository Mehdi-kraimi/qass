import { Component, OnInit, Inject } from '@angular/core';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { StatutAvoir } from 'app/Enums/Statut/StatutAvoir.Enum';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { Client, ClientMinimalInfo } from 'app/Models/Entities/Contacts/Client';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { OrderProductsDetailsType } from 'app/Enums/productsDetailsType.enum';
import { OrderDetails } from 'app/Models/Entities/Commun/OrderDetails';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { HeaderService } from 'app/services/header/header.service';
import { LocalElements } from 'app/shared/utils/local-elements';
declare var toastr: any;

@Component({
	selector: 'app-add',
	templateUrl: './add-facture-situation.component.html',
	styleUrls: ['./add-facture-situation.component.scss']
})

export class AddFactureSituationComponent implements OnInit {


	get f() {
		return this.creationForm.controls;
	}
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	statutAvoir: typeof StatutAvoir = StatutAvoir;
	statutFacture: typeof StatutFacture = StatutFacture;
	prestations: any;
	tva: any;
	editorConfig: AngularEditorConfig = { editable: true, spellcheck: true, height: '8rem', translate: 'yes', };
	chantierPreDefini = null;
	dateLang: any;
	emitter: any = {};
	remise = 0;
	typeRemise = TypeValue.Amount;
	puc = 0;
	prorata = 0;
	retenueGarantie = 0;
	delaiGarantie = 0;
	idicheIntervention
	idFichesTravail;
	loading;
	reference;
	datafacture;
	idDevis = null;
	processing = false;
	creationForm: any = null;
	devisInfos;
	adresseFacturation: /*Adresse*/any;
	idChantier: number = null
	pourcentageIn = 0;
	pourcentageInDbAcompte = 0;
	totalAvancementAcompte = 0;
	totalAvancementSituation = 0;
	totalAvancement = 0;
	nouveauTotalAvancement = 0;
	newPourcentage = 0;
	documentConfiguration;
	docType = ''

	factureSituationCount
	nbrSituation = 0;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private header: HeaderService,

	) { }
	async ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType);
		this.pourcentageIn = 0;
		this.processing = true;
		this.buildCreationForm();
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => this.dateLang = text);
		await this.getDocumentConfiguration();
		await this.initializeCreationForm();
		this.getDevisById(this.route.snapshot.params.id).then(async (result) => {
			this.devisInfos = result.value;
			// récuperer le nomber des facture situation
			this.factureSituationCount = !this.devisInfos.situations ? 0 :
				this.devisInfos.situations.filter(x => x.typeInvoice === TypeFacture.Situation).length;
			this.getPourcentage();
			this.setAdresse();
			await this.setDefaultValueFacture();
			this.idChantier = await this.getParamsFromRoute('idChantier') as number;

			this.processing = false;
		}, error => {
			console.error(error);
			this.processing = false;
		});
		// this.route.params.subscribe(async params => {
		// 	this.devisInfos = await this.getDevisById(params['id']);

		// });

	}


	calculTotalAvancement() {
		this.totalAvancementAcompte = this.devisInfos.orderDetails.totalTTC * (this.pourcentageInDbAcompte / 100);
		this.totalAvancementSituation = this.totalAvancementAcompte + this.devisInfos.orderDetails.totalTTC * (this.pourcentageIn / 100);
	}
	getPourcentage() {
		// this.pourcentageInDbAcompte = this.devisInfos.situations.filter(e => e.typeInvoice === TypeFacture.Acompte)
		// 	.reduce((x, y) => x + y.situation, 0);
		// tslint:disable-next-line: max-line-length
		this.pourcentageIn = this.devisInfos.situations.filter(e => e.status != this.statutFacture.Annule && (e.typeInvoice === +TypeFacture.Situation || e.typeInvoice === TypeFacture.Acompte)).reduce((x, y) => x + y.situation, 0);
		// if (this.factureSituationCount === 0) {
		// 	this.pourcentageInDbAcompte = this.devisInfos.situations.filter(e => e.status != this.statutFacture.Annule && e.typeInvoice === TypeFacture.Acompte)
		// 		.reduce((x, y) => x + y.situation, 0);
		// 	this.pourcentageIn = this.pourcentageInDbAcompte;
		// 	this.newPourcentage = 0;

		// }
		// else {
		// 	this.newPourcentage = this.pourcentageIn;
		// }
		this.totalAvancement = this.devisInfos.orderDetails.totalTTC * (this.newPourcentage / 100);
		this.calculTotalAvancement();
	}

	changePourcentage(pourcentage) {
		// if (this.factureSituationCount === 0) {
		// 	this.newPourcentage = pourcentage;
		// } else {
		this.newPourcentage = pourcentage + this.pourcentageIn;
		//	}
		this.nouveauTotalAvancement = this.devisInfos.orderDetails.totalTTC * (pourcentage / 100);
		this.totalAvancement = this.devisInfos.orderDetails.totalTTC * (this.newPourcentage / 100);
	}
	getAdresseDesignation(data): string {
		try {
			const adresse = data.filter(x => x.isDefault);
			return adresse.length !== 0 ? adresse[0].designation : '';
		} catch (err) {
			return '';
		}
	}

	/**
	 * return the type of the document
	 */
	getParamsFromRoute(paramName: string): Promise<string | number> {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => resolve(params[paramName]))
		});
	}
	setAdresse() {
		this.adresseFacturation = this.devisInfos.addressIntervention as Adresse;
	}
	/**
	* @description build the creation Form
	*/
	buildCreationForm(): void {

		this.creationForm = this.formBuilder.group({
			reference: [null, [Validators.required]],
			dateCreation: [new Date(), [Validators.required]],
			dateEcheance: [null, [Validators.required]],
			object: [],
			note: [],
			conditionRegelement: [],
			client: [null],
			pourcentage: [null, [Validators.required, Validators.min(1)]]
		});
	}



	/**
	* @description initialize the creation Form
	*/
	async initializeCreationForm(): Promise<void> {
		// set the defaults values from the configuration
		this.creationForm.controls['reference'].setValue(await this.generateReference());
		this.creationForm.controls['dateCreation'].setValue(new Date);
		if (this.documentConfiguration) {
			const dateToday = new Date();
			// tslint:disable-next-line:radix
			dateToday.setDate(new Date().getDate() + parseInt(this.documentConfiguration['validity'].toString()));
			this.creationForm.controls['dateEcheance'].setValue(dateToday)
		} else {
			this.creationForm.controls['dateEcheance'].setValue(new Date);
		}
	}

	/**
	 * @description return a reference for the new insertion
	 */
	generateReference(): Promise<string> {
		return new Promise((reslove, reject) => {
			this.service.getAll(ApiUrl.configReference + 'invoice' + '/reference').subscribe(
				res => {
					reslove(res as string);
				},
				err => {
					console.error(err);
				}
			);
		});
	}

	/**
	 * @description return the defaults values from the configuration
	 */
	async setDefaultValueFacture() {
		this.creationForm.controls['reference'].setValue(await this.generateReference());
		//  set the defaults values from the configuration
		this.creationForm.controls['note'].setValue(this.devisInfos.client['note']);
		this.creationForm.controls['conditionRegelement'].setValue(this.devisInfos.client['paymentCondition']);
		//this.creationForm.controls['object'].setValue(this.documentConfiguration['object']);
		//  set default date validite
		if (this.devisInfos.situations != null && this.devisInfos.situations.length > 0) {
			this.nbrSituation = this.devisInfos.situations.filter(e => e.typeInvoice === TypeFacture.Situation).length;
		}
		this.nbrSituation += 1;
		const text = 'Situation N° ' + this.nbrSituation + ' pour devis ' + this.devisInfos.reference + ' au ' + AppSettings.formaterNotTime(AppSettings.formatDate(new Date()));

		this.creationForm.controls['object'].setValue(text);
		if (this.documentConfiguration.validity) {
			const dateToday = new Date();
			// tslint:disable-next-line:radix
			dateToday.setDate(dateToday.getDate() + parseInt(this.documentConfiguration['validity'].toString()));
			this.creationForm.controls['dateEcheance'].setValue(dateToday);
		}
	}


	async getDocumentConfiguration() {
		this.service.getAll(ApiUrl.configDocument).subscribe(res => {
			if (res) {
				this.documentConfiguration = JSON.parse(res).find(element => element.documentType === TypeNumerotation.facture);
			}
		}, reject => console.error(reject));
	}

	checkFormIsValid(statut: StatutFacture): boolean {
		let valid = true;
		for (const key in this.creationForm.controls) {
			if (
				this.creationForm.controls[key].errors !== null
				&&
				(statut !== this.statutFacture.Cloture || (key !== 'pourcentage' && statut === this.statutFacture.Cloture))
			) {
				valid = false;
			}
		}
		return valid;

	}

	getStatutTosave(status: StatutFacture, resteAPayer): StatutFacture {
		//  Brouillon Cloture Encours
		if (status === this.statutFacture.Encours) {
			if (resteAPayer === 0) {
				return this.statutFacture.Cloture;
			} else {
				return this.statutFacture.Encours;
			}
		} else if (status === this.statutFacture.Cloture || status === this.statutFacture.Brouillon) {
			return status;
		}
	}

	calcTotalHtToSave(status: StatutFacture, prestationTotalHt: number): number {
		if (status === this.statutFacture.Cloture) {
			const situation = JSON.parse(this.devisInfos.situation) as { idFacture: number, pourcentage: number, resteAPayer: number }[];
			return (situation.length === 0) ? this.devisInfos.totalHt : (situation[situation.length - 1].resteAPayer);
		} else {
			return prestationTotalHt;
		}
	}

	calcTotalTtcToSave(status: StatutFacture, prestationTotalTtc: number, totalHt: number, tva: number) {
		if (status === this.statutFacture.Cloture) {
			return totalHt * ((tva / 100) + 1)
		} else {
			return prestationTotalTtc;
		}
	}



	async getDevisById(idDevis: string) {
		return await this.service.getById(ApiUrl.Devis, idDevis).toPromise();
	}

	/*---------------------------------------------------------------*/
	/**
	 * @section  Logique de sauvegarde des données
	 */
	/*---------------------------------------------------------------*/

	/**
	 * @description la fonction principale pour sauvegarder la nouvelle facture
	 * @param statut statut du facture qui on veut sauvegarder (cloture/brouillon/en cours)
	 */
	async add(statut: StatutFacture) {
		// afficher l'animation de chargement des données
		this.processing = true;

		// vérifier si tous les champs obligatoir est remplie
		if (!this.checkFormIsValid(statut)) {
			// récupère la traduction des messages d'erreurs
			const translatation = await this.getTranslateByKey('errors');
			// afficher un message d'erreur
			toastr.warning(translatation.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
			// cacher l'animation de chargement des données
			this.processing = false;
			// sortir de la fonction
			return;
		}

		const pourcentage = this.creationForm.value.pourcentage;

		const purcountageExiste = pourcentage + this.pourcentageIn;


		// if (this.factureSituationCount === 0 && this.pourcentageInDbAcompte > pourcentage) {
		// 	// récupère la traduction des messages d'erreurs

		// 	const text = this.translate.instant('errors.totalpourcentageSituationMin')
		// 	toastr.warning(text + ' ' + this.pourcentageInDbAcompte, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
		// 	// cacher l'animation de chargement des données
		// 	this.processing = false;
		// 	// sortir de la fonction
		// 	return;
		// }

		if (purcountageExiste > 90) {
			// récupère la traduction des messages d'erreurs
			const translatation = await this.getTranslateByKey('errors');
			// afficher un message d'erreur

			toastr.warning(translatation.pourcentageExiste, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
			// cacher l'animation de chargement des données
			this.processing = false;
			// sortir de la fonction
			return;
		}


		const facturecreationBody = await this.createBodyRequest(statut);
		// send request to the server
		this.service.create(ApiUrl.Invoice + ACTION_API.create, facturecreationBody.facture).subscribe(
			async res => {
				if (res) {
					// récupère la traduction des messages d'ajout
					const translatation = await this.getTranslateByKey('addFacture');
					// affcihe un message du succès
					toastr.success(translatation.msg, translatation.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					// redirigé au detail de la nouvelle insertion
					this.navigateToDevisDetail();

				} else {
					// récupère la traduction des messages d'erreurs
					const translatation = await this.getTranslateByKey('errors');
					toastr.warning(translatation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
				}
			},
			async err => {
				// récupère la traduction des messages d'erreurs
				const translatation = await this.getTranslateByKey('errors');
				toastr.warning(translatation.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center', });
			}, () => {
				// cacher l'animation de chargement des données
				this.processing = false;
			});
	}

	navigateToDevisDetail() {
		if (!this.idChantier) {
			this.router.navigate([`/devis/detail/${this.devisInfos.id}`],
				{
					queryParams: {
						'section': 'factureSituation',
					}
				});
		} else {
			this.router.navigate([`/chantiers/${this.idChantier}/documents/3/detail/${this.devisInfos.id}`],
				{
					queryParams: {
						'section': 'factureSituation',
					}
				});
		}
	}
	getDataFromArticlesComponet(status: StatutFacture): Promise<any> {
		return new Promise((resolve, reject) => {
			this.emitter.getDateToSave(status, (res) => {
				resolve(res);
			});
		});
	}
	pourcentage(pourcentageForm, pourcentageInDb) {
		let pourcentagereturn = 0;
		const pourcentage = (pourcentageForm + pourcentageInDb)
		if (pourcentage === null) {
			pourcentagereturn = 0;
		} else {
			if (pourcentage > 90) {
				pourcentagereturn = 90 - pourcentageInDb
			}
			if (pourcentage === 90) {
				pourcentagereturn = 90 - pourcentageInDb;
			}
			if (pourcentage < 90) {
				pourcentagereturn = pourcentageForm
			}
		}
		return pourcentagereturn;
	}
	async createBodyRequest(status: StatutFacture): Promise<any> {
		this.prestations = await this.getDataFromArticlesComponet(status);
		const formValue = this.creationForm.value;
		const facture: Invoice = new Invoice();
		facture.reference = formValue.reference;
		const pourcentage = formValue.pourcentage;
		facture.situation = pourcentage;
		facture.workshopId = this.devisInfos.workshop != null ? this.devisInfos.workshop.id : null;
		facture.creationDate = AppSettings.formaterDatetime(formValue.dateCreation);
		facture.dueDate = AppSettings.formaterDatetime(formValue.dateEcheance);
		facture.status = status;//this.getStatutTosave(status, this.prestations.resteAPayerTTC);
		facture.typeInvoice = TypeFacture.Situation;
		facture.emails = '[]';
		facture.memos = '[]';
		facture.purpose = formValue.object;
		facture.note = formValue.note;
		// region : ajout des informations du client
		const client: Client = this.devisInfos.client;
		facture.clientId = client.id;
		facture.client = client;
		facture.addressIntervention = client['addresses'].find(x => x.isDefault === true);
		facture.paymentCondition = formValue.conditionRegelement;
		const orderDetails: OrderDetails = {
			'globalDiscount': {
				'value': this.prestations.remise,
				'type': this.prestations.typeRemise
			},
			'holdbackDetails': {
				'warrantyPeriod': this.prestations.delaiGarantie,
				'holdback': this.prestations.retenueGarantie,
				'warrantyExpirationDate': new Date(),
			},
			'globalVAT_Value': this.prestations.tvaGlobal,
			'puc': this.prestations.puc,
			'proportion': this.prestations.prorata,
			'productsDetailsType': OrderProductsDetailsType.List,
			'lineItems': this.prestations.prestations,
			'productsPricingDetails': {
				'totalHours': 0,
				'salesPrice': 0
			},
			'totalHT': this.prestations.totalHt,
			'totalTTC': this.prestations.totalTtc
		};
		facture.orderDetails = orderDetails;
		facture.quoteId = this.devisInfos.id.toString();

		return {
			idDevis: this.devisInfos.id,
			// situations: this.createFacturesSituationInfos(status, pourcentage),
			facture: facture
		}
	}

	// createFacturesSituationInfos(status, pourcentage) {
	// 	const resteAayer = (): number => {
	// 		if (status !== this.statutFacture.Cloture) {
	// 			return parseFloat((((this.prestations.resteAPayerTTC === undefined ? 0 : this.prestations.resteAPayerTTC) as number).toFixed(2)))
	// 		} else {
	// 			return 0;
	// 		}
	// 	}
	// 	return {
	// 		pourcentage: pourcentage,
	// 		resteAPayer: resteAayer(),
	// 		resteAPayerHT: this.prestations.resteAPayerHT,
	// 		situationCumulleeTTC: this.prestations.situationCumulleeTTC,
	// 		situationCumulleeHT: this.prestations.situationCumulleeHT,
	// 		acomptesCumulleeHT: this.prestations.acomptesCumulleeHT,
	// 		acomptesCumulleeTTC: this.prestations.acomptesCumulleeTTC
	// 	};
	// }

	IncremenetRefernce() {
		// this.paramteresService.Increment(this.typeNumerotation.facture as number).subscribe(res => { });
	}

	/*---------------------------------------------------------------*/
	/**
	 * @section  fonctions d'aide
	 */
	/*---------------------------------------------------------------*/
	getTranslateByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translatation => {
				resolve(translatation);
			});
		});
	}

	navigateToDevisList() {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => {
				const idChantier: number = params['idChantier'];
				const idDevis: number = params['id'];
				const url = idChantier === undefined ? `/devis` : `/chantiers/${idChantier}/documents/3/detail/${idDevis}`;

				//	const url = idChantier !== null ? `/chantiers/${idChantier}/documents/3/detail/${idDevis}` : `/devis`;
				this.router.navigate([url]);
			});
		});
	}
}
