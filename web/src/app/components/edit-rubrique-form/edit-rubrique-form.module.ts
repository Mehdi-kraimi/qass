import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule, MatDatepickerModule, MatCheckboxModule, MatAutocompleteModule, MatSelectModule, MatDialogModule } from '@angular/material';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DataTablesModule } from 'angular-datatables';
import { EditRubriqueFormComponent } from './edit-rubrique-form.component';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/chantier/', '.json');
}

@NgModule({
	imports: [
		CommonModule,
		NgSelectModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatCheckboxModule,
		MatAutocompleteModule,
		MatSelectModule,
		ReactiveFormsModule,
		DataTablesModule,
		NgbTooltipModule,
		MatDialogModule,

	],
	exports: [EditRubriqueFormComponent],
	declarations: [EditRubriqueFormComponent],
	entryComponents: [EditRubriqueFormComponent]
})
export class EditRubriqueFormModule { }
