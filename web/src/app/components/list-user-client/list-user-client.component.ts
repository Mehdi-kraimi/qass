import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ClientAuthComponent } from '../clientAuth/clientAuth.component';
import { UserListModel } from 'app/Models/Entities/UserListModel';
import { UsersProfil } from 'app/Enums/UserProfil.Enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';

declare var swal: any;
declare var toastr: any;
declare var jQuery: any;

@Component({
	selector: 'app-list-user-client',
	templateUrl: './list-user-client.component.html',
	styleUrls: ['./list-user-client.component.scss']
})
export class ListUserClientComponent implements OnInit {
	// tslint:disable-next-line: no-input-rename
	@Input('client') public client: Client;
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	Users: UserListModel;
	userTableColumns: any = [];
	dtOptions: DataTables.Settings = {};
	dtTrigger: Subject<any> = new Subject();
	dataTablesLang = {};
	i = 0;
	total;
	checkedColumns: any = [];
	pageLength = AppSettings.SIZE_PAGE;
	processing = false;
	initPage = 0;
	profils = [];
	profilUser
	IdUser
	usersProfil: typeof UsersProfil = UsersProfil
	idClient

	changePasswordForm = this.formBuilder.group({
		password: ['', [Validators.required, Validators.minLength(5)]],
		confirmPassword: ['']
	}, { validator: this.checkPasswords })


	iduser: any;

	/**
   * double clique pour passer au details de chantier
   */
	preventSingleClick = false;
	timer: any;
	delay: Number;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		// private utilisateurService: UtilisateurServi
		private translate: TranslateService,
		private formBuilder: FormBuilder,
		// private loginService: LoginService,
		private previousRouteService: PreviousRouteService,
		private router: Router,
		private route: ActivatedRoute,
		private dialog: MatDialog,


	) { }
	ngOnInit() {
		this.route.params.subscribe(params => {
			this.idClient = params['id'];
		});
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('dataTables').subscribe((text: string) => {

			this.dataTablesLang = text;
			this.InitDataTable();
		});
		this.getDataTablesTrans();
		this.translate.get('labels').subscribe(labels => {
			// tslint:disable-next-line: max-line-length
			this.userTableColumns = [labels.nom, labels.prenom, labels.Actif, labels.dernierConnect, labels.JointDate, labels.Username, labels.action]
		});
		this.getProfils();
		// this.IdUser = this.loginService.getUser().id;

	}

	GetAll(search, pNumber, pSize, cSort, sDir) {
		// this.service
		// 	.getAll(search, pNumber, pSize, cSort, sDir)
		// 	.subscribe(Response => {
		// 		this.Users = Response;

		// 	});
	}

	InitDataTable() {
		this.processing = true;
		// recupérer old paramétere
		const oldChoices = JSON.parse(localStorage.getItem('module_user_info'));
		// tslint:disable-next-line: radix
		this.pageLength = !oldChoices || oldChoices.page_length == null ? AppSettings.SIZE_PAGE : parseInt(oldChoices.page_length);
		// tslint:disable-next-line: radix
		const order = !oldChoices || oldChoices.sort == null ? [[0, 'asc']] : [[parseInt(oldChoices.sort), oldChoices.dir]];

		// Garder pagination
		// tslint:disable-next-line: prefer-const
		let previousUrl = this.previousRouteService.getPreviousUrl();

		if (previousUrl.includes('/utilisateurs/detail') || previousUrl.includes('/utilisateurs/modifier')) {
			if (oldChoices && oldChoices.start != null) { this.initPage = oldChoices.start; }
		}

		this.dtOptions = {
			pagingType: 'full_numbers',
			pageLength: this.pageLength,
			displayStart: this.initPage,
			serverSide: true,
			processing: false,
			search: { search: oldChoices && oldChoices.search != null ? oldChoices.search : '' },
			data: [],
			order: order,
			language: this.dataTablesLang,
			columnDefs: [{ orderable: false, targets: 6 }],
			ajax: (dataTablesParameters: any, callback) => {
				this.saveChoices(dataTablesParameters.length, dataTablesParameters.search.value,
					dataTablesParameters.order[0].column, dataTablesParameters.order[0].dir,
					dataTablesParameters.start);
				// this.utilisateurService
				// 	.GetAll(
				// 		dataTablesParameters.search.value,
				// 		dataTablesParameters.start / dataTablesParameters.length + 1,
				// 		dataTablesParameters.length,
				// 		dataTablesParameters.columns[dataTablesParameters.order[0].column].data,
				// 		dataTablesParameters.order[0].dir,
				// 		this.profilUser != null ? [this.profilUser] : [],
				// 		this.idClient
				// 		//this.profilUser
				// 	)
				// 	.subscribe(data => {
				// 		this.Users = data;
				// 		this.GetInitParameters();
				// 		if (this.i == 0) {
				// 			this.total = data.totalItems;
				// 			this.i++;
				// 		}
				// 		callback({
				// 			recordsTotal: this.total,
				// 			recordsFiltered: data.totalItems,
				// 			data: []
				// 		});
				// 		// this.processing = false;
				// 	}, err => this.translate.get('errors').subscribe(errors => {
				// 		if (this.processing) {
				// 			this.processing = false;
				// 			toastr.warning(errors.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				// 		}
				// 	}));
			},
			columns: [
				{ data: 'Nom' },
				{ data: 'Prenom' },
				{ data: 'Actif' },
				{ data: 'Dernierecon' },
				{ data: 'JoinDate' },
				{ data: 'Username' },
				{ data: 'action' }
			]
		};
	}

	delete(id, i) {
		this.translate.get('list.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					/* this.utilisateurService.Delete(id).subscribe(res => {
						if (res) {
							swal(text.success, '', 'success');
							// this.rerender();
							this.Users.list.splice(i, 1);
						} else {
							//swal(text.error, '', 'error');
							swal(text.ImpossibleDeSuppression, '', 'error');
						}
					}); */
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}

	ngAfterViewInit(): void {
		setTimeout(() => {
			this.GetInitParameters()
			this.InitDataTable();
			this.dtTrigger.next();
			this.rerender();
		}, 500);
		setTimeout(() => {
			this.processing = false;
		}, 1000);
	}

	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

	rerender(): void {
		this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		});
	}

	getDataTablesTrans() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('dataTables').subscribe((text: string) => {
			this.dataTablesLang = text;
		});
		this.InitDataTable();
	}

	SetCheckedColmuns(arr) { /* arr : is array of colmun checked */
		localStorage.setItem('module_user_colmuns', JSON.stringify(arr))
	}

	GetInitParameters() {
		const data = JSON.parse(localStorage.getItem('module_user_colmuns'));
		this.checkedColumns = (data == null ? [] : data)
	}

	saveChoices(length, search, sort, dir, start) {
		localStorage.setItem('module_user_info', JSON.stringify({ page_length: length, search: search, sort: sort, dir: dir, start: start }));
	}

	checkPasswords(group: FormGroup) {

		const password = group.controls.password.value;
		const confirmPassword = group.controls.confirmPassword.value;

		return password === confirmPassword ? null : { notSame: true }
	}
	changePassword() {
		/* this.loginService.changePassword(this.iduser, this.changePasswordForm.value.password).subscribe(res => {
			this.translate.get('changePassword').subscribe(text => {
				toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			jQuery('#changePassword').modal('hide');
		}); */
	}

	getProfils() {
		this.translate.get('labels').subscribe(labels => {
			this.profils.push({ id: UserProfile.admin, libelle: labels.admin });
			this.profils.push({ id: UserProfile.manager, libelle: labels.manager });
			this.profils.push({ id: UserProfile.technicien, libelle: labels.technicien });
			this.profils.push({ id: UserProfile.technicienChantier, libelle: labels.technicienChantier });
			this.profils.push({ id: UserProfile.technicienMaintenance, libelle: labels.technicienmaintenace });


		});
	}

	setUserIdInlocalStorage(id: number) {
		localStorage.setItem('SBP_changePassword_UserId', id.toString());
	}

	LoadAddClientUser() {

		const dialogLotConfig = new MatDialogConfig();
		// dialogLotConfig.data = null;
		dialogLotConfig.width = '650px';
		dialogLotConfig.height = '550px';
		dialogLotConfig.data = this.client;
		const dialogRef = this.dialog.open(ClientAuthComponent, dialogLotConfig);

		dialogRef.afterClosed().subscribe((data) => {
			this.rerender();
		});
	}

	doubleClick(idUser) {
		this.preventSingleClick = true;
		clearTimeout(this.timer);
		this.router.navigate(['/utilisateurs/detail', idUser]);
	}
}
