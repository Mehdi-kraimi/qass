import { Component, EventEmitter, OnInit, Inject, Input } from '@angular/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { IFilterOption } from 'app/Models/Model/filter-option';
import { MatDialog } from '@angular/material';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ManageDataTable, DataTableRowActions } from 'app/libraries/manage-data-table';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ActivatedRoute } from '@angular/router';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { DocumentHelper } from 'app/libraries/document-helper';
import { JournalType } from 'app/Enums/typeJournal.enum';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';
import { MissionType } from 'app/pages/parameteres/agenda/agenda.component';

@Component({
	selector: 'list-global-index',
	templateUrl: './index.component.html'
})
export class IndexListGlobalComponent implements OnInit {

	/** event export list clients */
	@Input() doctype = '';
	@Input() idChantier = null;
	@Input() typeJournal = '';
	@Input() typeKinds = MissionType.technician_task_types;

	processing = true;
	exportEvent = new EventEmitter();
	helper = ManageDataTable;
	loading = true;
	actions: DataTableRowActions[];
	status = [];
	totalPeriode = 0;
	formConfig: any;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private dialog: MatDialog,
		public activatedRoute: ActivatedRoute,
		private header: HeaderService
	) {
		// this.translate.setDefaultLang(AppSettings.lang);
		// this.translate.use(AppSettings.lang);
		TranslateConfiguration.setCurrentLanguage(this.translate);

	}

	ngOnInit() {
		this.loading = true;
		this.translate.get('...').toPromise().then(() => {
			ManageDataTable.listTechnicien = [];
			ManageDataTable.listClient = [];
			ManageDataTable.listMois = [];
			ManageDataTable.comptes = [];
			ManageDataTable.listSupplier = [];
			ManageDataTable.docType = '';
			ManageDataTable.idChantier = null;
			ManageDataTable.service = this.service;
			ManageDataTable.dialog = this.dialog;
			ManageDataTable.periodes = [];
			ManageDataTable.typeJournal = '';
			ManageDataTable.profils = [];
			ManageDataTable.listChantiers = [];
			if (this.doctype && this.doctype !== '') {
				ManageDataTable.docType = this.doctype;
			} else {
				ManageDataTable.docType = this.activatedRoute.snapshot.data.docType;
			}

			if (localStorage.getItem(LocalElements.docType) !== ManageDataTable.docType) {
				localStorage.removeItem(`item_column`);
				localStorage.removeItem(`state`);
			}
			localStorage.setItem(LocalElements.docType, ManageDataTable.docType);

			ManageDataTable.initFilters();
			if (this.idChantier && this.idChantier !== null) {
				ManageDataTable.idChantier = this.idChantier;
				ManageDataTable.filterOptions['workshopId'] = this.idChantier;
			}
			if (ManageDataTable.docType === ApiUrl.journal) {
				ManageDataTable.getPeriodes();
				ManageDataTable.typeJournal = this.typeJournal;
				if (this.typeJournal === 'JournalVente') { ManageDataTable.filterOptions['journalType'] = JournalType.Sales; };
				if (this.typeJournal === 'JournalAchat') { ManageDataTable.filterOptions['journalType'] = JournalType.Expense; };
				if (this.typeJournal === 'journal_banque') { ManageDataTable.filterOptions['journalType'] = JournalType.Bank; };
				if (this.typeJournal === 'journal_caisse') { ManageDataTable.filterOptions['journalType'] = JournalType.Cash; };
			}
			if (ManageDataTable.docType === ApiUrl.typesMission) {
				ManageDataTable.typeKinds = this.typeKinds;
				ManageDataTable.filterOptions['missionKinds'] = [this.typeKinds];
			}
			if (ManageDataTable.docType === ApiUrl.Mission) {
				ManageDataTable.filterOptions['OrderBy'] = 'startingDate';
			}
			if (ManageDataTable.docType === ApiUrl.User) {
				ManageDataTable.getRole();
			}
			if (this.IsChantier() && this.idChantier === null) {
				ManageDataTable.getChantiers();
			}
			if (this.IsClient()) {
				ManageDataTable.getClients();
			}
			if (this.IsSupplier()) {
				ManageDataTable.getSupplier();
			}
			if (ApiUrl.Payment === ManageDataTable.docType) {
				ManageDataTable.GetCompte();
				this.getSolde();
			}
			if (this.IsTechnicien()) {
				ManageDataTable.getTechniciens();
			}
			ManageDataTable.header = ManageDataTable.getHeader();
			ManageDataTable.header = ManageDataTable.header.filter(x => x['appear'] === true);
			this.actions = ManageDataTable.getActions(false);
			if (this.IsStatut()) {
				this.status = Object.entries(DocumentHelper.getStatus(ManageDataTable.docType)).map(doc => ({
					label:
						this.translate.instant(this.isProgress() && doc[1] === 'in_progress' ? 'encours' : doc[1]), value: doc[1]
				}));
			}
			if (this.isVisitM()) {
				ManageDataTable.getlistMois();
			}
			ManageDataTable.getData()
				.then((e) => {
					this.loading = false;
					// Loader.hide();
				}).catch(() => console.log("Loader.hide()"));

			this.prepareBreadcrumb();
		});

	}

	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			if (this.idChantier == null) {
				this.header.breadcrumb = [
					{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() }
				];
			}
			if (this.idChantier != null) {
				const chantierName = localStorage.getItem('nomChantier');
				this.header.breadcrumb = [
					{ label: 'Chantiers', route: `/chantiers` },
					{ label: chantierName, route: `/chantiers/detail/${this.idChantier}` },
					{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: this.isRouteRebrique() }

				];
			}
		});
	}


	isRouteRebrique() {
		const moduleName = localStorage.getItem('moduleName');
		let moduleRebrique = 0;
		if (ManageDataTable.docType === ApiUrl.Devis) {
			moduleRebrique = 1;
		}
		if (ManageDataTable.docType === ApiUrl.Invoice) {
			moduleRebrique = 3;
		}
		if (ManageDataTable.docType === ApiUrl.BonCommandeF) {
			moduleRebrique = 2;
		}
		return `/chantiers/${this.idChantier}/documents/${moduleName}/${moduleRebrique}`
	}
	ngOnDestroy(): void {

	}


	Isfilter() {
		return ManageDataTable.docType !== ApiUrl.Client && ManageDataTable.docType !== ApiUrl.Fournisseur
			&& ManageDataTable.docType !== ApiUrl.journal && ManageDataTable.docType !== ApiUrl.Groupe
			&& ManageDataTable.docType !== ApiUrl.GME && ManageDataTable.docType !== ApiUrl.Lot
			&& ManageDataTable.docType !== ApiUrl.configModeRegelemnt && ManageDataTable.docType !== ApiUrl.configurationLabel
			&& ManageDataTable.docType !== ApiUrl.configDocumentType && ManageDataTable.docType !== ApiUrl.typesMission
			;
	}


	IsStatut() {
		return ManageDataTable.docType === ApiUrl.Devis || ManageDataTable.docType === ApiUrl.Invoice ||
			ManageDataTable.docType === ApiUrl.Depense || ManageDataTable.docType === ApiUrl.MaintenanceContrat ||
			ManageDataTable.docType === ApiUrl.avoir || ManageDataTable.docType === ApiUrl.BonCommandeF ||
			ManageDataTable.docType === ApiUrl.MaintenanceOperationSheet || ManageDataTable.docType === ApiUrl.Chantier
			|| ManageDataTable.docType === ApiUrl.VisitMaintenance || ManageDataTable.docType === ApiUrl.FicheIntervention;
	}


	IsChantier() {
		return ManageDataTable.docType === ApiUrl.Devis || ManageDataTable.docType === ApiUrl.Invoice
			|| ManageDataTable.docType === ApiUrl.Depense
			|| ManageDataTable.docType === ApiUrl.avoir || ManageDataTable.docType === ApiUrl.BonCommandeF
			|| ManageDataTable.docType === ApiUrl.FicheIntervention;
	}

	IsClient() {
		return ManageDataTable.docType === ApiUrl.Invoice || ManageDataTable.docType === ApiUrl.avoir
			|| ManageDataTable.docType === ApiUrl.MaintenanceOperationSheet || ManageDataTable.docType === ApiUrl.Chantier
			|| ManageDataTable.docType === ApiUrl.MaintenanceContrat || ManageDataTable.docType === ApiUrl.VisitMaintenance ||
			ManageDataTable.docType === ApiUrl.FicheIntervention;
	}

	IsSupplier() {
		return ManageDataTable.docType === ApiUrl.BonCommandeF || ManageDataTable.docType === ApiUrl.Depense;
	}

	IsTechnicien() {
		return ManageDataTable.docType === ApiUrl.MaintenanceOperationSheet || ManageDataTable.docType === ApiUrl.FicheIntervention;
	}

	filterDate() {
		return ManageDataTable.docType === ApiUrl.Invoice || ManageDataTable.docType === ApiUrl.avoir
			|| ManageDataTable.docType === ApiUrl.MaintenanceOperationSheet || ManageDataTable.docType === ApiUrl.journal
			|| ManageDataTable.docType === ApiUrl.Payment || ManageDataTable.docType === ApiUrl.MaintenanceContrat ||
			ManageDataTable.docType === ApiUrl.FicheIntervention;
	}

	isProgress() {
		return ManageDataTable.docType === ApiUrl.Invoice || ManageDataTable.docType === ApiUrl.avoir
			|| ManageDataTable.docType === ApiUrl.Depense || ManageDataTable.docType === ApiUrl.MaintenanceContrat
			|| ManageDataTable.docType === ApiUrl.BonCommandeF;
	}

	depenseGroupe() { return ManageDataTable.docType === ApiUrl.Depense }


	//#region actions

	/** export click */
	exportClick() {
		this.exportEvent.emit();
	}

	rerender() {
		this.ngOnInit();
	}

	isAdd() {
		return ManageDataTable.docType !== ApiUrl.journal &&
			ManageDataTable.docType !== ApiUrl.VisitMaintenance;
	}

	isPaiement() {
		return ManageDataTable.docType === ApiUrl.Payment
	}

	isVisitM() { return ApiUrl.VisitMaintenance === ManageDataTable.docType }
	isMission() { return ApiUrl.Mission === ManageDataTable.docType }
	/**
	 * change dataTables
	 */
	changeFiltersEvent(dataTableOutput: IFilterOption) {
		ManageDataTable.filterOptions = { ...ManageDataTable.filterOptions, ...dataTableOutput };
		ManageDataTable.getData()
			.then(() => {
				this.loading = false;
			});
	}


	exportFactures(body) {

		const result = {
			'idClient': body.params.IdClient,
			'startDate': body.params.DateDebut,
			'endDate': body.params.DateFin,
			'type': +body.type,
			'exportFcature': body.params.ExporterFacturesChantier
		}

		this.service.create(ApiUrl.Invoice + '/ExportReleveByPeriod', result).subscribe((res: any) => {

			//  Get time stamp for fileName.
			const stamp = new Date().getTime();

			//  file type
			const fileType = 'application/pdf';

			//  file extension
			const extension = 'pdf';

			res.value.invoiceCredit.forEach((element, index) => {
				AppSettings.downloadBase64(
					',' + element,
					'FA_' + index + stamp + '.' + extension,
					fileType,
					extension
				);
			});

			res.value.documents.forEach((element, index) => {
				AppSettings.downloadBase64(
					',' + element,
					'Doc_' + index + stamp + '.' + extension,
					fileType,
					extension
				);
			});
		});

	}

	getSolde() {
		this.service.getAll(ApiUrl.Payment + '/Balance').subscribe(res => {
			if (res.isSuccess) {
				this.totalPeriode = res.value.totalPaymentInvoice - res.value.totalPaymentExpense;
			}
		})
	}

	setformConfig(defaultData, type) {
		this.formConfig.defaultData = defaultData;
		this.formConfig.type = type;
	}


	//#endregion

}
