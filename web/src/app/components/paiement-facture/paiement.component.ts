import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Modereglement } from 'app/Models/Entities/Parametres/Modereglement';
import { ParametrageCompte } from 'app/Models/Entities/Parametres/ParametrageCompte';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { Credit } from 'app/Models/Entities/Documents/Credit';
import { Calcule } from 'app/calcule/Calcule';
import { ICalcule } from 'app/calcule/ICalcule';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';
import { StaticModeReglement } from 'app/Enums/StaticModeReglement.Enum';
import { TypeCompte } from 'app/Enums/Parameters/TypeCompte.enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { StatutComptabilise } from 'app/Enums/StatutComptabilise.enum';
import { StatutChantier } from 'app/Enums/Statut/StatutChantier.Enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { PaymentAdd } from 'app/Models/Model/PaymentAdd';

declare const toastr: any;
declare const jQuery: any;
declare const swal: any;

export enum Typepaiement {

	/// <summary>
	/// a payment for an expense
	/// </summary>
	Expense,
	/// <summary>
	/// a payment for an invoice
	/// </summary>
	Invoice,
	/// <summary>
	/// the payment is a Transfer of funds
	/// </summary>
	Transfer
}

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'paiement-facture',
	templateUrl: './paiement.component.html',
	styleUrls: ['./paiement.component.scss']
})
export class PaiementComponent implements OnInit {
	@Output() refresh = new EventEmitter();
	@Input() facture: Invoice;
	form;
	dateLang; //  translate datepicker
	modesRegelement: Modereglement[] = null;
	comptes: ParametrageCompte[] = null;
	loading = false;
	indexModified;
	statutFacture: typeof StatutFacture = StatutFacture;
	statutComptabilise: typeof StatutComptabilise = StatutComptabilise

	typePaiement: typeof TypePaiement = TypePaiement;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	staticModeReglement: typeof StaticModeReglement = StaticModeReglement;
	calcule: ICalcule = new Calcule();
	reference;
	initAvoir = false;
	restPaye = 0;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb?: FormBuilder,
		private translate?: TranslateService
	) {
		this.form = this.fb.group({
			'montant': [null, [Validators.required], this.CheckValidPrice.bind(this)],
			'idCaisse': [null, [Validators.required]],
			'datePaiement': [null, [Validators.required]],
			'idModePaiement': [null, [Validators.required]],
			'description': [null],
			'createAvoir': [false]
		})
	}
	ngOnInit(): void {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
		//  this.GetCompte('');
		//  this.GetModeRegelement('');
	}
	//  Récuperer la liste des modes régelements
	GetModeRegelement(search) {
		if (this.modesRegelement === null) {
			this.service.getAll(ApiUrl.configModeRegelemnt)
				.subscribe(
					res => {
						this.modesRegelement = res.value
					}
				)
		}
	}

	//  Récuperer la liste des comptes
	GetCompte(search) {
		if (this.comptes === null) {
			this.service.getAll(ApiUrl.configCompte)
				.subscribe(res => {
					this.comptes = res.value
				})
		}
	}
	//  Charger & clear form ajouter
	chargeLists() {
		this.GetCompte('');
		this.GetModeRegelement('');
		this.form.reset()
		this.indexModified = null;
		this.form.controls['description'].setValue('Paiement facture N° ' + this.facture.reference)
		this.form.controls['montant'].setValue(AppSettings.formaterNumber(this.getResterPayer()))
		this.form.controls['datePaiement'].setValue(new Date());
	}

	//  Get information de form
	get f() { return this.form.controls; }


	//  Calculate reste à payer
	getResterPayer() {
		if (this.facture && this.facture.orderDetails !== undefined) {
			this.restPaye = this.facture.orderDetails.totalTTC - this.facture.payments.reduce((x, y) => x + y.amount, 0)
			return this.restPaye;
		} else {
			return 0;
		}
	}
	CompareMontantInDb(montant: number, montantInDB: number) {

		const reste = montant - montantInDB;
		return (reste > 0 && reste < 0.01) ? montantInDB : montant
	}


	createPaiementAvoir(avoir: Credit) {
		this.loading = true;
		const paiement: PaymentAdd = new PaymentAdd();
		paiement.amount = avoir['totalTTC'] * (-1);
		paiement.datePayment = new Date(AppSettings.formaterDatetime(new Date().toString()));
		paiement.description = 'Paiement facture N° ' + this.facture.reference;
		paiement.type = Typepaiement.Invoice;
		paiement.creditNoteId = avoir.id;
		paiement.invoices = [{
			amount: avoir['totalTTC'] * (-1),
			documentId: this.facture.id
		}];

		paiement.operation = TypePaiement.CreditNotePayment;
		paiement.paymentMethodId = 1;

		this.service.create(ApiUrl.Payment + ACTION_API.create, paiement).subscribe(value => {
			this.loading = false;
			if (value) {
				this.refresh.emit('')
				this.successfulAdd(value.value)
			} else {
				this.errorMontantIncorrect()
			}
		}, err => {
			console.log(err);
			this.loading = false;
			this.errorServer()
		});

	}


	//  Ajouter payement
	async savePaiement(indexModified?) {
		if (this.form.valid) {
			this.loading = true;
			const values = this.form.value;
			const paiement: PaymentAdd = new PaymentAdd();
			paiement.description = values.description;
			paiement.operation = TypePaiement.Payment;
			paiement.amount = values.montant;
			paiement.datePayment = values.datePaiement;
			paiement.type = Typepaiement.Invoice;
			paiement.creditNoteId = '';
			paiement.paymentMethodId = +values['idModePaiement'];
			paiement.accountId = +values['idCaisse'];
			paiement.invoices = [{
				amount: values.montant,
				documentId: this.facture.id
			}];
			if (indexModified) {
				this.service.update(ApiUrl.Payment, paiement, indexModified)
					.subscribe(arg => {
						if (arg.isSuccess) {
							this.successUpdate();
						} else {
							this.errorMontantIncorrect()
						}
						this.loading = false;
					}, err => {
						console.log(err);
						this.loading = false;
						this.errorServer()
					});

			} else {
				this.service.create(ApiUrl.Payment + ACTION_API.create, paiement).subscribe(res => {
					this.loading = false;
					if (res) {
						this.successfulAdd(res.value)
					} else {
						this.errorMontantIncorrect()
					}
				}, err => {
					console.log(err);
					this.loading = false;
					this.errorServer()
				})
			}
		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAllPaiement, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}
	//  supprimer paiement
	removePaiement(index) {
		const paiement = this.facture.payments[index];
		this.translate.get('paiement.delete').subscribe(text => {
			swal({
				title: text.title,
				text: (paiement.operation === TypePaiement.GroupedPayment ? text.questionGroupe :
					(paiement.operation === TypePaiement.CreditNotePayment ? text.questionAvoirDeconste : text.question)),
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {

					this.loading = true;
					this.service.delete(ApiUrl.Payment, paiement.id).subscribe(res => {
						if (res) {
							this.loading = false;
							swal(text.success, '', 'success');
							if (this.facture.status === this.statutFacture.Cloture && this.facture.workshop.status === StatutChantier.Termine) {
								this.retourStatutChantier(this.facture)
							} else {
								this.refresh.emit('')
							}
						}
					}, err => {
						this.loading = false;
						swal(text.failed, '', 'warning');
					})
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	retourStatutChantier(facture: Invoice) {

		// this.chantierService.changeStatut({idChantier : facture.chantier.id,statutChantier : StatutChantier.Accepte  })
		this.translate.get('paiement.retourChantier').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {

				if (isConfirm) {

					this.loading = true;
					//  this.chantierService.changeStatut({ idChantier: facture.chantier.id, statutChantier: StatutChantier.Accepte }).subscribe(res => {
					//    if (res) {
					//      this.loading = false;
					//      swal(text.success, '', 'success');
					//      this.refresh.emit('')
					//    }
					//  }, err => {
					//    this.loading = false;
					//    swal(text.failed, '', 'warning');
					//  })
				} else {
					this.refresh.emit('')
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}


	//  Charger les données dans la form de modification
	chargerModifierPaiement(index) {

		this.GetCompte('');
		this.GetModeRegelement('');
		this.indexModified = this.facture.payments[index].id;
		const paiement = this.facture.payments[index]
		this.form.controls['description'].setValue(paiement.description)
		this.form.controls['montant'].setValue(AppSettings.formaterNumber(paiement.amount))
		this.form.controls['idCaisse'].setValue(paiement.account.id.toString())
		this.form.controls['idModePaiement'].setValue(paiement.paymentMethod.id.toString())
		const datePaiement = new Date(paiement.datePayment.toString())
		this.form.controls['datePaiement'].setValue(datePaiement)
		jQuery('#ajouterPaiement').modal('show');
	}


	//  Vérifier si la valeur de montant saisi est valide
	CheckValidPrice(control: FormControl) {
		const montant = parseFloat(control.value)
		if (montant > 0) {
			const promise = new Promise((resolve, reject) => {
				const montantPaiementModifier = this.indexModified !== null ? this.facture.payments.find(pai => pai.id = this.indexModified).amount : 0
				const resPayer = montantPaiementModifier + this.facture.orderDetails.totalTTC - this.facture.payments.reduce((x, y) => x + y.amount, 0);
				if (AppSettings.formaterNumber(montant) <= AppSettings.formaterNumber(resPayer)) {
					resolve(null)
				} else {
					resolve({ CheckValidPrice: true })
				}
			});
			return promise;
		} else {
			const promise = new Promise((resolve, reject) => { resolve({ CheckValidPrice: true }) });
			return promise;
		}
	}

	//  Successful ajoute
	successfulAdd(paiement?) {
		this.refresh.emit('')
		this.translate.get('paiement').subscribe(text => {
			jQuery('#ajouterPaiement').modal('hide');
			toastr.success(text.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			if (paiement !== null && paiement.operation === TypePaiement.CreditNotePayment) {
				toastr.success(text.avoirCreated + paiement.reference, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}
		})
	}

	successUpdate() {
		this.refresh.emit('')
		this.translate.get('paiement').subscribe(text => {
			jQuery('#ajouterPaiement').modal('hide');
			toastr.success(text.modifierSuccess, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		})
	}

	//  Error server
	errorServer() {
		this.translate.get('errors').subscribe(text => {
			toastr.warning(text.errorServer, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		})
	}

	//  Montant incorrect
	errorMontantIncorrect() {
		this.translate.get('errors').subscribe(text => {
			toastr.warning(text.invalid, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			setTimeout(() => { location.reload(); }, 1000)
		})
	}

	onChangeMoyenPaiement(value) {
		if (value === StaticModeReglement.Avoir) {
			this.form.controls['idCaisse'].setValue(null)
			this.form.controls['idCaisse'].setValidators([])
			this.form.controls['idCaisse'].updateValueAndValidity()
		} else if (value === StaticModeReglement.Espece) {
			const caisse = this.comptes.filter(x => x.type === TypeCompte.caisse)[0];
			if (caisse !== null) {
				this.form.controls['idCaisse'].setValue(caisse.id.toString())
			}
		} else {
			this.form.controls['idCaisse'].setValidators([Validators.required])
			this.form.controls['idCaisse'].updateValueAndValidity()
		}
	}


}
