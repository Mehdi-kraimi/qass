import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardLayoutComponent } from './layouts/dashboard/dashboard.component';
import { loginLayoutComponent } from './layouts/login/login.component';
// import { FIXED_NAVBAR_FOOTER_ROUTES } from './shared/routes/dashboard.routes';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { ApiUrl } from './Enums/Configuration/api-url.enum';

const FIXED_NAVBAR_FOOTER_ROUTES: Routes = [

	// {
	// 	path: 'dashboard',
	// 	loadChildren: './pages/dashboard/dashboard-page.module#DashboardPagesModule'
	// },

	{
		path: 'dashboard',
		loadChildren: './pages/dashboardaxiobat/dashboardaxio-page.module#DashboardAxioPagesModule'
	},
	{
		path: 'utilisateurs',
		loadChildren: './pages/users/users.module#UtilisateurModule',
		data: {
			docType: ApiUrl.User
		}
	},
	{
		path: 'clients',
		loadChildren: './pages/contacts/contacts.module#ContactsModule',
		data: {
			docType: ApiUrl.Client
		}
	},
	{
		path: 'fournisseurs',
		loadChildren: './pages/contacts/contacts.module#ContactsModule',
		data: {
			docType: ApiUrl.Fournisseur
		}
	},
	{
		path: 'devis',
		loadChildren: './pages/documents/documents.module#DocumentsModule',
		data: {
			docType: ApiUrl.Devis
		}
	},
	{
		path: 'produits',
		loadChildren: './pages/Produits/produit.module#ProduitModule',
		data: {
			docType: ApiUrl.Produit
		}
	},
	{
		path: 'chantiers',
		loadChildren: './pages/chantier/chantier.module#ChantiersModule',
		data: {
			docType: ApiUrl.Chantier
		}
	},
	{
		path: 'ficheintervention',
		loadChildren: './pages/ficheInterv/ficheIntervention.module#FicheInterventionModule',
		data: {
			docType: ApiUrl.FicheIntervention
		}
	},
	{
		path: 'factures',
		loadChildren: './pages/documents/documents.module#DocumentsModule',
		data: {
			docType: ApiUrl.Invoice
		}
	},
	{
		path: 'paiements',
		loadChildren: './pages/paiements/paiements.module#PaiementsModule',
		data: {
			docType: ApiUrl.Payment
		}
	},
	{
		path: 'avoirs',
		loadChildren: './pages/documents/documents.module#DocumentsModule',
		data: {
			docType: ApiUrl.avoir
		}
	},
	{
		path: 'parameteres',
		loadChildren: './pages/parameteres/parameteres.module#ParameteresModule'
	},
	{
		path: 'lots',
		loadChildren: './pages/lots/lots.module#LotsModule',
		data: {
			docType: ApiUrl.Lot
		}
	},
	{
		path: 'groupes',
		loadChildren: './pages/Groupe/Groupe.module#GroupesModule',
		data: {
			docType: ApiUrl.Groupe
		}
	},
	{
		path: 'bonCommandeFournisseur',
		loadChildren: './pages/documents/documents.module#DocumentsModule',
		data: {
			docType: ApiUrl.BonCommandeF
		}
	},
	{
		path: 'depense',
		loadChildren: './pages/documents/documents.module#DocumentsModule',
		data: {
			docType: ApiUrl.Depense
		}
	},
	{
		path: 'comptabilite',
		loadChildren: './pages/comptabilite/comptabilite.module#ComptabiliteModule'
	},
	{
		path: 'gammemaintenanceequipements',
		loadChildren: './pages/gamme-maintenance-equipement/gamme-maintenance-equipement.module#GammeMaintenanceEquipementModule',
		data: {
			docType: ApiUrl.GME
		}

	},
	{
		path: 'contratentretiens',
		loadChildren: './pages/contartEntretien/contratEntretien.module#ContratEntretienModule',
		data: {
			docType: ApiUrl.MaintenanceContrat
		}

	},
	{
		path: 'agendaglobal',
		loadChildren: './pages/agenda-global/agenda-global.module#AgendaGlobalModule'

	},
	{
		//C:\Users\Admin\Documents\Projet Axiobat\webadmin-axiobat-1\src\app\pages\tache\tache.module.ts

		path: 'tache',
		loadChildren: './pages/tache/tache.module#TacheModule',
		data: {
			docType: ApiUrl.Mission
		}

	},
	{
		path: 'visitesMaintenance',
		loadChildren: './pages/visiteMaintenence/visiteMaintenance.module#VisiteMaintenanceModule',
		data: {
			docType: ApiUrl.VisitMaintenance
		}
	},

	{
		path: 'planification',
		loadChildren: './pages/planification/planification.module#PlanificationModule'

	},
	{
		path: 'ficheinterventionmaintenance',
		loadChildren: './pages/ficheInterv/ficheIntervention.module#FicheInterventionModule',
		data: {
			docType: ApiUrl.MaintenanceOperationSheet
		}
	},
	{
		path: 'factureReccurente',
		loadChildren: './pages/factureReccurente/facture-reccurente.module#FactureReccurenteModule',
		data: {
			docType: ApiUrl.Recurring
		}
	}
];

const appRoutes: Routes = [
	{
		path: '',
		redirectTo: '/dashboard',
		pathMatch: 'full',
		canActivate: [AuthGuard]
	},
	{
		path: '',
		component: DashboardLayoutComponent,
		data: { title: '' },
		children: FIXED_NAVBAR_FOOTER_ROUTES,
		canActivate: [AuthGuard]
	}, {
		path: '',
		component: loginLayoutComponent,
		children: [{
			path: 'login',
			loadChildren: './pages/login/login.module#LoginModule'
		}],
		data: { title: '' }
	}
];

@NgModule({
	imports: [RouterModule.forRoot(appRoutes , { onSameUrlNavigation: 'reload' })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
