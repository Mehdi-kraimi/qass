import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from './app-settings/app-settings';
import { TokenHelper } from './shared/utils/token-helper';
import { Router } from '@angular/router';
import { MenuService } from './services/menu/menu.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	//#region lifecycle

	constructor(
		private translate: TranslateService,
		private route: Router,
		public menu: MenuService
	) {
		this.translate.setDefaultLang('fr');
		this.translate.use(AppSettings.lang)
		this.checkToken();
	}

	//#endregion

	//#region Methods

	checkToken() {
		if (!TokenHelper.hasValidToken() && TokenHelper.isExpired() && !window.location.href.includes('change-password')) {
			localStorage.clear();
			this.route.navigateByUrl('/login');
		}
	}

	//#endregion
}
