import { Adresse } from '../Commun/Adresse';

import { Historique } from '../Commun/Historique';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { ClientMinimalInfo, Client } from '../Contacts/Client';
import { OrderDetails } from '../Commun/OrderDetails';
import { Quote } from './Quote';
import { Workshop } from './Workshop';

export class Invoice {
	id: string;
	reference: string;
	status: StatutFacture;
	note: string;
	purpose: string;
	paymentCondition: string;
	orderDetails: OrderDetails;
	workshopId: string;
	addressIntervention: Adresse;
	emails: any;
	changesHistory: Historique[];
	dueDate: Date | string;
	billingAdress: Adresse;
	quoteId: string;
	client: ClientMinimalInfo | Client;
	clientId: string;
	memos: any;
	creationDate: Date | string;
	typeInvoice: TypeFacture;
	lotDetails: boolean;
	operationSheetsIds: string[];
	situation?: number;
	workshop?: Workshop;
	devis?: Quote;
	payments: any;
	associatedDocuments?: any;
	operationSheetMaintenance: any;
	contract: any;
	OperationSheetMaintenanceId: string;
	ContractId: string;
	totalTTC?: string;
	totalHT?: string;
	canUpdate: any;
	canDelete: any;
	canCancel: any;
}

export class FactureTransmise {
	idFacture: number;
	transmise: number;
}
