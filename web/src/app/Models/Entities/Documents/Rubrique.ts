import { Workshop } from "./Workshop";

export class Rubrique {
  id: string;
  value: string;
  type: any;
  totalDocuments?: number;
  workshopId: any;
  workshop: Workshop;
  documents: any;
}