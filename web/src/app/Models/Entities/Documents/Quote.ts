
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { Client } from '../Contacts/Client';
import { Adresse } from '../Commun/Adresse';
import { AssociatedDoc, Email } from '../Commun/Commun';
import { Historique } from '../Commun/Historique';
import { OrderDetails } from '../Commun/OrderDetails';
import { Workshop } from './Workshop';
import { Memo } from '../Commun/Memo';

export class Quote {
	lotDetails: boolean;
	addressIntervention: Adresse;
	orderDetails: OrderDetails;
	clientId: string;
	client: Client;
	emails: Email[];
	situations: QuoteSituations[];
	id: string;
	reference: string;
	status: string;
	note: string;
	purpose: string;
	paymentCondition: string;
	workshop: Workshop;
	changesHistory: Historique[];
	associatedDocuments: AssociatedDoc[];
	creationDate: Date;
	dueDate: Date;
	totalTTC?: number;
	totalHT?: number;
	memos: Memo[];

}

export class QuoteSituations {
	invoiceId: string;
	reference: string;
	typeInvoice: TypeFacture
	situation: number;
	status: StatutFacture;
	totalHT: number;
	totalTTC: number;
	dueDate: Date;
	creationDate: Date;
	restePayer: number;
}










