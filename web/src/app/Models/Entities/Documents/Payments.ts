import { AssociatedDoc } from '../Commun/Commun';
import { Historique } from '../Commun/Historique';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';

export class Payment {
	id: string;
	description: string;
	operation: TypePaiement;
	type: TypePaiement;
	datePayment: Date;
	amount: 0;
	creditNote: CreditNote;
	account: Account;
	paymentMethod: PaymentMethod;
	changesHistory: Historique[];
	associatedDocuments: AssociatedDoc[]
}

export class CreditNote {
	documentType: undefined;
	id: string;
	reference: string;
	status: string;
	totalHT: 0;
	totalTTC: 0
}

export class Account {
	id: number;
	type: any;
	label: string;
	code: string;
	isDefault: boolean;
}

export class PaymentMethod {
	id: number;
	value: string;
	isDefault: boolean;
}


