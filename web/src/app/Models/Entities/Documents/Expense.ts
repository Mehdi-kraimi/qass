import { Workshop } from './Workshop';
import { Supplier } from '../Contacts/Supplier';
import { OrderDetails } from '../Commun/OrderDetails';
import { Attachement } from '../Commun/DocumentAttacher';
import { AssociatedDoc } from '../Commun/Commun';
import { Historique } from '../Commun/Historique';
import { Memo } from '../Commun/Memo';
import { Payment } from './Payments';
import { TypePaiement } from 'app/Enums/TypePaiement.Enum';


export class Expense {
	dueDate: Date;
	creationDate: Date;
	cancellationDocument: CancelDoc;
	orderDetails: OrderDetails;
	expenseCategory: CategoryExpense;
	supplier: Supplier;
	memos: Memo[];
	payments: Payment[];
	id: string;
	reference: string;
	status: string;
	note: string;
	purpose: string;
	paymentCondition: string;
	workshop: Workshop;
	changesHistory: Historique[];
	associatedDocuments: AssociatedDoc[];

}

export class CancelDoc {
	id: string;
	comment: string;
	createdOn: Date;
	user: {
		id: string;
		userName: string
	};
	attachments: Attachement[]
}

export class CategoryExpense {
	id: string;
	type: string;
	categoryType: string;
	label: string;
	code: string;
	description: string;
	vatValue: number;
	parentId: string;
	subCategories: [];
}
