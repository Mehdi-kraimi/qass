export class OptionSpDF {
	header: Header;
	footer: Footer;
	images: Images;

}
export class Header {
	companyName: string
	phoneNumber: string
	email: string
	address: adresseCompany
}
// tslint:disable-next-line: class-name
export class adresseCompany {
	street: string;
	city: string;
	postalCode: string;
	countryCode: string
}
export class Footer {
	line_1: string;
	line_2: string;
	line_3: string;
}

// tslint:disable-next-line: class-name
export class Images {
	logo: string;
	cachet?: string;
}
