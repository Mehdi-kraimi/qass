
export class DocumentAttacher {
	comment: string;
	designation: string;
	rubricId: string;
	types: Type[];
	attachments: Attachement[];
}

export class Type {
	id: string;
	name: string;
	isDefault: boolean;
}

export class Attachement {
	fileId?: string;
	content?: string;
	fileName?: string;
	fileType?: string;
}
