
export class AssociatedDoc {
	id: string;
	reference: string;
	status: string;
	documentType: string;
	typeInvoice: any;
	creationDate: Date
}

export class Email {
	mailId: string;
	to: string[];
	subject: string;
	content: string;
	sentOn: Date;
	user: {
		id: string;
		userName: string
	}
}

export class Technicians {
	id: string;
	firstName: string;
	lastName: string;
	userName: string
}

export class ClientSignature {
	signatureDate: Date;
	name: string;
	id: string;
	imageContent: string
}

export class PieceJoin {
	fileType: string;
	fileName: string;
	content: string;
}

