import { TypeValue } from 'app/Enums/typeValue.Enums';
import { OrderProductsDetailsType } from 'app/Enums/productsDetailsType.enum';

export class OrderDetails {
	id?: string;
	globalDiscount?: GlobalDiscount;
	holdbackDetails?: HoldbackDetails;
	globalVAT_Value?: number;
	puc?: number;
	proportion?: number;
	productsDetailsType?: OrderProductsDetailsType;
	lineItems?: LineItems[];
	productsPricingDetails?: ProductsPricingDetails;
	totalHT?: number;
	totalTTC?: number;
	productsDetails_File?: ProductsDetailsFile;
}

export class ProductsDetailsFile {
	fileId: string;
	fileName: string;
	fileType: string
}

export class LineItems {
	quantity: 0;
	type: string;
	product: {}
}

export class GlobalDiscount {
	value: number;
	type: TypeValue;
}
export class HoldbackDetails {
	status?: string;
	warrantyPeriod: number;
	holdback: number;
	warrantyExpirationDate: Date;
}

export class ProductsPricingDetails {
	totalHours?: number;
	materialCost?: number;
	hourlyCost?: number;
	achatsMateriels?: number;
	salesPrice?: number
}
