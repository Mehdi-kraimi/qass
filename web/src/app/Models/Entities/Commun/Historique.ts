export class Historique {
	dateAction: Date;
	action: string;
	information: string;
	fields: Champ[];
	user: User;
}

export class User {
	id: string;
	userName: string;
}

export class Champ {
	champ: string;
	valeurInitial: string;
	valeurFinal: string;
}
