/**
 * a publishing contract
 */
export interface PublishingContract {

    /**
     * the id of publishing contract
     */
    id: number;

    /**
     * the title of publishing
     */
    title: string;

    /**
     * the tags of contract
     */
    tags: string;

    /**
     * the filename
     */
    fileName: string;

    /**
     * the original name of file
     */
    orginalFileName: string;

    /**
    * the id of associate
    */
    associateId: number;

    /**
     * the date of creation
     */
    createdOn: string;
}
