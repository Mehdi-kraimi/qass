import { Client } from './Client';

export class Groupe {
	id: number;
	name: string;
	clients: Client[];
}
