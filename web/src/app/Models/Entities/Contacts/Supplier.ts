import { Adresse } from '../Commun/Adresse';
import { Historique } from '../Commun/Historique';
import { Contact } from '../Commun/Contact';
import { Memo } from '../Commun/Memo';

export class Supplier {

	id: string;
	reference: string;
	firstName: string;
	lastName: string;
	phoneNumber: string;
	landLine: string;
	email: string;
	website: string;
	siret: string;
	intraCommunityVAT: string;
	accountingCode: string;
	memos: Memo[];
	changesHistory: Historique[];
	contactInformations: Contact[];
	additionalInformations: [];
	address: Adresse;
	note: string;
	paymentCondition: string;
}

