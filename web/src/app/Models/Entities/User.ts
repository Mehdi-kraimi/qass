import { Historique } from './Commun/Historique';

export class User {
	id: string;
	createdOn: Date;
	reference: string;
	firstName: string;
	lastName: string;
	principal: true;
	userName: string;
	email: string;
	phoneNumber: string;
	statut: Boolean;
	lastLogin: Date;
	changesHistory: Historique[];
	role: Role;
	registrationNumber?: string;
}

export class Role {
	id: string;
	name: string;
	type: string;
	permissions: Permission[]

}

export class Permission {
	id: string;
	name: string;
	description: string;
	module: any
}
