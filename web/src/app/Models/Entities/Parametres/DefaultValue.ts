
export class DefaultValue {

	defaultVAT: number
	salesPrice: number
	buyingPrice: number
	cartCost: number
	displacementCost: number
	startingHour: string
	endingHour: string

}
