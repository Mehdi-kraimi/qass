export class PlanComptableModel {
	id: number;
	label: string;
	type: string;
	code: string;
	description: string;
	vatValue: number;
	parentId: number;
	subCategories: any[];
}
export class PlanComptableTvaModel {
	tva: string;
	codeComptable: number;
}
