
export class NbDocumentsChantieModel {
    nbFacture: number = 0;
    nbBC: number = 0;
    nbDevis: number = 0;
}

export class ChangeStatusRetenueGarentieResponse {
    idFacture: number;
    StatusRetenueGarantie: number;
}

export class ChangeDateRetenueGarentieResponse {
    idFacture: number;
    date: Date;
}