import { StatutChantier } from "app/Enums/Statut/StatutChantier.Enum";

export class ChangeStatutBodyRequest {
    idChantier: number;
    statutChantier: StatutChantier;
}

export class changementTauxAvencementBodyRequest {
    idChantier: number;
    tauxAvencement: number;
}

