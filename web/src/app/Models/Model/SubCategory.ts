export class SubCategory {
	id: string;
	code: string;
	description: string;
	parentId: string;
	type: Number;
	vatValue: number
	subCategories: [];
}
