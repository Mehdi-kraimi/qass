
/**
 * a class that defines the filter option to get the data as paged list
 */
export interface IFilterOption {


	DateStart?: string;
	/**
	* the end point of the date
	*/
	DateEnd?: string;
	/**
	 * the search query to search with it
	 */
	SearchQuery: string;
	/**
	 * the index of the page to retrieve
	 */
	Page: number;
	/**
	 * size of the page, how many records to include
	 */
	PageSize: number;
	/**
	 * the sort direction : Descending or Ascending
	 */
	SortDirection: SortDirection;
	/**
	 * what property to order by with it
	 */
	OrderBy?: string;

}

export enum OperationType {
	Incoming,
	Outgoing,
	All
}

export enum SortDirection {
	Descending = 0,
	Ascending = 1
}

