export class RetenueGarantieModel {
	amount: number;
	holdback: number;
	invoiceId: string;
	invoiceReference: string;
	status: string;
	warrantyExpirationDate: Date
	warrantyPeriod: number;
}


