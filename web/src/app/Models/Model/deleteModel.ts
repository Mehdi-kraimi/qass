export class DeleteModel {
	status: string;
	message: string;
	messageCode: any;
	error: {
		errorMessage: string;
		errorCode: any;
		errorSource: string
	};
	hasValue: boolean;
	hasError: boolean;
	isSuccess: boolean;
}
