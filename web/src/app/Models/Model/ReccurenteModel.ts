import { Invoice } from "../Entities/Documents/Invoice";

export class ReccurenteModel {
	// public repetition: number;
	// public type_repetition: any;
	// public dateRepetion: any;
	// public type_termine: number;
	// public termine: string;


	public nbrfoistermine: number;
	public dateTermine: Date | string;
	public newDateCreation: Date | string;
	public dateValidation: Date | string;
	public typeRepetition: number
	public jour: number;
	public jourSemaine: any;
	public nbrRepetition: number;
	public typeTermine: number

}

export class Reccurente {

	public periodicityOptions: Periodicity[];
	public periodicityType: any;
	public endingType: any;
	public occurringCount: number;
	public endingDate: Date;
	public startingDate: Date;
	public status: Date;
	public documentType: string;
	public document: Invoice;
}

export class Periodicity {
	public daysOfWeek: string[];
	public dayOfMonth: number;
	public recurringType: any;
	public repeatEvery: number
}
