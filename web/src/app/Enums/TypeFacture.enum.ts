export enum TypeFacture {
	None = 0,
	Situation = 1,
	Acompte = 2,
	Cloturer = 3
}