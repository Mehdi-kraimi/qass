export enum TypeInterventionMaintenance {
	/// <summary>
	/// the type is maintenance
	/// </summary>
	Maintenance,
	/// <summary>
	/// the type is after-sales service
	/// </summary>
	AfterSalesService
}
