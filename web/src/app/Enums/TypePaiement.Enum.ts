export enum TypePaiement {
	/// <summary>
	/// a payment
	/// </summary>
	Payment,
	/// <summary>
	/// a grouped payment
	/// </summary>
	GroupedPayment,
	/// <summary>
	/// a transfer from an account
	/// </summary>
	Transfer_From,
	/// <summary>
	/// a transfer to an account
	/// </summary>
	Transfer_To,
	/// <summary>
	/// a payment with a CreditNote
	/// </summary>
	CreditNotePayment
}