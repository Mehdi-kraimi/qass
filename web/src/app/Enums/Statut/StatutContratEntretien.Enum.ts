export enum StatutContratEntretien {
	Brouillon = 'draft',
	Enattente = 'waiting',
	Encours = 'in_progress',
	Termine = 'finished',
	Annule = 'canceled',
}



