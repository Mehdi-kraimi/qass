export enum StatutFacture {
	Brouillon = 'draft',
	Encours = 'in_progress',
	Enretard = 'late',
	Cloture = 'closed',
	Annule = 'canceled'


}
