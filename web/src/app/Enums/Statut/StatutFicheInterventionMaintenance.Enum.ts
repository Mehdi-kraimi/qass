export enum StatutFicheInterventionMaintenancev {

	Brouillon = 'draft',
	Planifiee = 'planned',
	Realisee = 'realized',
	Annulee = 'canceled',
	Facturee = 'billed',
	Areplanifiee = 'areplanified',
	Late = 'late'

}