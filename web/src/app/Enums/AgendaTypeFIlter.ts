export enum AgendaTypeFilter {
	Rappels = 3,
	Taches = 4,
	Evenements = 5,
	InterventionsMaintenance = 1,
	InterventionsChantier = 2
}
