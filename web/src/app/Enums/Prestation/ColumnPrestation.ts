import { ColumnType } from 'app/components/form-data/data-table/data-table.component';

export class ColumnPrestation {

	/* 'Lots/Noms',
		'Qté',
		'Unité',
		'Tva',
		'Heures',
		'Catégorie',
		'Coût matériel',
		'Total HT',
		'Total TTC', */

	static getColumns(show, ShowRemise?) {

		const column = [
			{ name: 'name', nameTranslate: 'columns.name', isOrder: true, type: ColumnType.any, appear: true, updated: false },
			{ name: 'quantity', nameTranslate: 'columns.Qte', isOrder: true, type: ColumnType.any, appear: true, updated: true },
			{ name: 'unite', nameTranslate: 'columns.Unite', isOrder: true, type: ColumnType.any, appear: show, updated: false },
			{ name: 'tva', nameTranslate: 'columns.tva', isOrder: true, type: ColumnType.any, appear: show, updated: true },
			{ name: 'totalHours', nameTranslate: 'columns.HeurescoutHoraire', isOrder: true, type: ColumnType.any, appear: show, updated: false },
			{ name: 'category', nameTranslate: 'columns.Categorie', isOrder: true, type: ColumnType.any, appear: show, updated: false },
			{ name: 'materialCost', nameTranslate: 'columns.coutMateriel', isOrder: true, type: ColumnType.any, appear: show, updated: false },
			{ name: 'remise', nameTranslate: 'columns.Remise', isOrder: true, type: ColumnType.any, appear: ShowRemise, updated: false },

			{ name: 'totalHt', nameTranslate: 'columns.TotalHt', isOrder: true, type: ColumnType.any, appear: show, updated: false },
			{ name: 'totalTTC', nameTranslate: 'columns.TotalTTC', isOrder: true, type: ColumnType.any, appear: show, updated: false },
		]
		return column.filter(x => x.appear === true);
	}

}
