export enum PeriodType {
	/// <summary>
	/// no date filter is applied
	/// </summary>
	None = 0,

	/// <summary>
	/// the period is defined an a date interval
	/// </summary>
	Interval = 1,

	/// <summary>
	/// the current month
	/// </summary>
	CurrentMonth = 2,

	/// <summary>
	/// the last month
	/// </summary>
	LastMonth = 3,

	/// <summary>
	/// the last three months
	/// </summary>
	LastThreeMonths = 4,

	/// <summary>
	/// the last six months
	/// </summary>
	LastSixMonths = 5,

	/// <summary>
	/// the current year
	/// </summary>
	CurrentYear = 6,

	/// <summary>
	/// the last year
	/// </summary>
	LastYear = 7,

	/// <summary>
	/// the current accounting year
	/// </summary>
	CurrentAccountingPeriod = 8

}