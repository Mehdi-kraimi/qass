export enum MessageCode {
	//#region common

	/// <summary>
	/// the operation has been Succeeded
	/// </summary>
	OperationSucceeded = 1,

	/// <summary>
	/// operation has been failed
	/// </summary>
	OperationFailed = 2,

	/// <summary>
	/// the operation has been failed cause no record has been found to perform the operation
	/// </summary>
	OperationFailedNotFound = 3,

	/// <summary>
	/// the operation has been failed cause an exception has been thrown
	/// </summary>
	OperationFailedException = 4,

	/// <summary>
	/// the operation has been failed for unknown reason
	/// </summary>
	OperationFailedUnknown = 5,

	/// <summary>
	/// provided in valid data
	/// </summary>
	InvalidData = 6,

	/// <summary>
	/// an error has accrued during the operation
	/// </summary>
	InternalError = 7,

	/// <summary>
	/// Operation Not Supported
	/// </summary>
	NotSupportedOperation = 8,

	//#endregion

	//#region Validation

	/// <summary>
	/// Validation Failed
	/// </summary>
	ValidationFailed = 100,

	/// <summary>
	/// invalid token
	/// </summary>
	InvalidToken = 101,

	/// <summary>
	/// invalid token
	/// </summary>
	PasswordMismatch = 102,

	/// <summary>
	/// Reference Not Unique
	/// </summary>
	ReferenceNotUnique = 103,

	/// <summary>
	/// Not Allowed
	/// </summary>
	NotAllowed = 104,

	/// <summary>
	/// invalid token
	/// </summary>
	InvalidLoginCredentials = 105,

	/// <summary>
	/// invalid token
	/// </summary>
	InvalideSignature = 106,

	/// <summary>
	/// invalid token
	/// </summary>
	InvalidApplicationSettingType = 107,

	/// <summary>
	/// Duplicate Entry
	/// </summary>
	DuplicateEntry = 108,

	EmailIsTooLong = 109,
	EmailIsRequired = 110,
	InvalidEmailHost = 111,
	InvalidEmailFormat = 112,
	RoleNameIsRequired = 113,
	PropertyValueTooLong = 114,
	PasswordIsRequired = 115,
	RoleIsRequired = 116,
	RoleNotExist = 117,
	ReferenceIsRequired = 118,
	NameIsRequired = 119,
	invalidDateFormat = 120,
	InvalidDocumentStatus = 121,
	FileMustHaveContent = 122,
	FileTypeIsRequired = 123,
	FileNameIsRequired = 124,
	FileMimeTypeIsNotValid = 125,
	FileNameWithExtensionIsRequired = 126,
	FileExtensionDontMatchMimeType = 127,
	EmailSubjectIsRequired = 128,
	EmailBodyIsRequired = 129,
	IdentifierIsRequired = 130,
	PageIndexMustBeGreaterThanZero = 131,
	PageSizeMustBeGreaterThanZero = 132,
	GroupNotExist = 134,
	RequiredValue = 135,
	ClientNotExist = 136,
	InvalidProductDetailsType = 137,
	UserNotExist = 138,

	//#endregion

	//#region Authentication

	/// <summary>
	/// Validation Failed
	/// </summary>
	UserIsLockedOut = 200,

	/// <summary>
	/// Unauthorized
	/// </summary>
	Unauthorized = 201,
	InvalidIdentifier = 202,
	InvalidIdentifierFormat = 203,
	MaintenanceContractIsRequired = 204,
	MaintenanceContractNotExist = 205,
	InvalidMonth = 206,

	// #endregion
}
