
export enum ApiUrl {

	// #region login

	login = 'Account/login',
	acount = 'Account',
	role = 'Role',

	updatePassword = 'Account/UpdatePassword',
	recetPassword = 'Account/RestPassword',
	forgetPassword = 'Account/ForgetPassword',

	// #endregion

	// #region contacts
	Mission = 'Mission',
	Client = 'Client',
	User = 'Account',
	File = 'File',
	Fournisseur = 'Supplier',
	Groupe = 'Group',
	Produit = 'Product',
	Lot = 'Lot',
	Chantier = 'Workshop',
	Devis = 'Quote',
	BonCommandeF = 'SupplierOrder',
	ProductsByFournisseur = 'Supplier/Products',
	Depense = 'Expense',
	Invoice = 'Invoice',
	FicheIntervention = 'OperationSheet',
	Payment = 'Payment',
	avoir = 'CreditNote',
	journal = 'Accounting/Journal',
	analytic = 'Analytics',
	typeProduct = 'Product/Categories',

	//#endregion

	//#region maintenance

	GME = 'EquipmentMaintenance',
	MaintenanceContrat = 'MaintenanceContract',
	VisitMaintenance = 'MaintenanceVisit',
	FicheInterventionMaintenance = 'MaintenanceOperationSheet',
	MaintenanceOperationSheet = 'MaintenanceOperationSheet',
	Recurring = 'Recurring',
	//#endregion

	//#region reference
	CheckReference = '/Check/Reference',
	CheckName = '/Check/Name',
	CheckUserName = '/Check/userName',

	//#endregion

	//#region
	Publishing = 'Publishing',
	ExtractTagsFromDocument = 'Publishing/ExtractTagsFromDocument',
	PublishingDownload = 'Publishing/Download',
	PublishingUpload = 'Publishing/Upload',
	//#endregion

	// #region parametres
    typesMission = 'Mission/Type',
	configurationCounter = 'Configuration/counter',
	configurationGoals = 'Configuration/Goals',
	configurationTax = 'Configuration/Tax',
	configurationUnit = 'Configuration/Unit',
	configurationLabel = 'Configuration/Label',
	configMessagerie = 'Configuration/messaging',
	configDefaultValue = 'Configuration/default_values',
	configGoogleConfig = 'Configuration/google_configuration',
	configDocument = 'Configuration/documents_configuration',
	configCategory = 'Configuration/Category',
	configModeRegelemnt = 'Configuration/PaymentMethod',
	configCompte = 'Configuration/FinancialAccount',
	configReference = 'Configuration/',
	configDocumentType = 'Configuration/WorkshopDocumentType',
	configTypeChantier = 'Configuration/WorkshopDocumentType',
	configRubric = 'Configuration/WorkshopDocumentRubric',
	configAccountingPeriode = 'Configuration/AccountingPeriod',

	configClassificationExpenses = 'Configuration/Classification/type/expense',
	configClassificationSales = 'Configuration/Classification/type/sales',
	configClassification = 'Configuration/Classification',
	configParametragePDF = 'Configuration/pdf_options',
	// #endregion
}

export enum ACTION_API {
	create = '/Create',
	update = '/Update',
	Delete = '/Delete/',
	Mail = '/Email',
}
