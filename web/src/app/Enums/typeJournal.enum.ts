export enum JournalType {
	/// <summary>
	/// the sales journal
	/// </summary>
	Sales,
	/// <summary>
	/// the expense journal
	/// </summary>
	Expense,
	/// <summary>
	/// the bank journal
	/// </summary>
	Bank,
	/// <summary>
	/// the cash journal
	/// </summary>
	Cash
}