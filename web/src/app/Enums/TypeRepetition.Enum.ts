export enum PeriodicityRecurringType {
	/// <summary>
	/// run every day
	/// </summary>
	EveryDay,

	/// <summary>
	/// run every week
	/// </summary>
	EveryWeek,

	/// <summary>
	/// run every month
	/// </summary>
	EveryMonth
}

export enum TypeTermine {
	Ilimite = 1,
	Jusque = 2,
	Occurence = 3,
}

export enum DayOfWeek {
	//
	// Summary:
	//     Indicates Sunday.
	Sunday = 0,
	//
	// Summary:
	//     Indicates Monday.
	Monday = 1,
	//
	// Summary:
	//     Indicates Tuesday.
	Tuesday = 2,
	//
	// Summary:
	//     Indicates Wednesday.
	Wednesday = 3,
	//
	// Summary:
	//     Indicates Thursday.
	Thursday = 4,
	//
	// Summary:
	//     Indicates Friday.
	Friday = 5,
	//
	// Summary:
	//     Indicates Saturday.
	Saturday = 6
}

/// <summary>
/// type of the Periodicity
/// </summary>
export enum PeriodicityType {
	/// <summary>
	/// a custom date configuration
	/// </summary>
	Custom,

	/// <summary>
	/// every month
	/// </summary>
	Monthly,

	/// <summary>
	/// every two months
	/// </summary>
	BiMonthly,

	/// <summary>
	/// every 3 months
	/// </summary>
	Quarterly,

	/// <summary>
	/// every 4 months
	/// </summary>
	EveryFourMonths,

	/// <summary>
	/// every 6 months
	/// </summary>
	Biannual,

	/// <summary>
	/// every year
	/// </summary>
	Annually,
}

export enum PeriodicityEndingType {
	/// <summary>
	/// the ending is not defined, this will require a manual stopping of the background service
	/// </summary>
	Undifined,

	/// <summary>
	/// the background service will end after a reaching a specific date
	/// </summary>
	SpecificDate,

	/// <summary>
	/// the background service will end after a specific number of time
	/// </summary>
	SpecificNumberOfTime
}

