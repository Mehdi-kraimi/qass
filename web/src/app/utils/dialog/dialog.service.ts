import { Injectable, EventEmitter } from '@angular/core';

export enum DialogChangeType {
	Show,
	Hide
}

@Injectable({
	providedIn: 'root'
})
export class DialogService {

	//#region Properties

	dialogChange: EventEmitter<DialogChangeType> = new EventEmitter<DialogChangeType>();

	//#endregion

	//#region Life-cycle

	constructor() { }

	//#endregion

	//#region Methods

	/**
	 * Emits a dialog change event
	 *
	 * @param type The type of emission
	 */
	emit(type: DialogChangeType): void {
		this.dialogChange.emit(type);
	}

	//#endregion
}
