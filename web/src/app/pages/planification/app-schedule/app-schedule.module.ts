import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { AppScheduleComponent } from './app-schedule.component';

import { ScheduleModule, DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService, RecurrenceEditorAllModule, ResizeService, DragAndDropService, ScheduleAllModule } from '@syncfusion/ej2-angular-schedule';

import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { TreeViewModule } from '@syncfusion/ej2-angular-navigations';
import { DropDownListAllModule, MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { MaskedTextBoxModule, UploaderAllModule } from '@syncfusion/ej2-angular-inputs';
import { ToolbarAllModule, ContextMenuAllModule } from '@syncfusion/ej2-angular-navigations';
import { ButtonAllModule } from '@syncfusion/ej2-angular-buttons';
import { CheckBoxAllModule } from '@syncfusion/ej2-angular-buttons';
import { DatePickerAllModule, TimePickerAllModule, DateTimePickerAllModule } from '@syncfusion/ej2-angular-calendars';
import { NumericTextBoxAllModule } from '@syncfusion/ej2-angular-inputs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { CommonModules } from 'app/common/common.module';
import { CalendarModule, SplitButtonModule } from "app/custom-module/primeng/primeng";
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatNativeDateModule, MatSelectModule, MatTabsModule } from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';
import { ShowvisiteModule } from '../show-visite/showvisite.module';
import { OverlayModule } from '@angular/cdk/overlay';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/ficheinterventionmaintenance/', '.json');
}
@NgModule({
	declarations: [
		AppScheduleComponent
	],
	imports: [
		CommonModule,
		HttpModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		ScheduleAllModule,
		NumericTextBoxAllModule,
		DatePickerAllModule,
		TimePickerAllModule,
		DateTimePickerAllModule,
		CheckBoxAllModule,
		ToolbarAllModule,
		DropDownListAllModule,
		ContextMenuAllModule,
		MaskedTextBoxModule,
		UploaderAllModule,
		MultiSelectAllModule,
		TreeViewModule,
		ButtonAllModule,
		FormsModule,
		OverlayModule,
		CommonModules,
		CalendarModule,
		NgbTooltipModule,
		MatDialogModule,
		NgSelectModule,
		ReactiveFormsModule,
		RecurrenceEditorAllModule,
		DateTimePickerAllModule,
		MatButtonModule,
		MatCheckboxModule,
		MatFormFieldModule,
		MatSelectModule,
		MatIconModule,
		ScheduleAllModule,
		MatDatepickerModule,
		MatInputModule,
		MatTabsModule,
		MatNativeDateModule,

	],
	exports: [
		AppScheduleComponent,
	],
	providers: [
		DayService,
		WeekService,
		WorkWeekService,
		MonthService,
		AgendaService,
		MonthAgendaService,
		TimelineViewsService,
		TimelineMonthService,
		TimelineViewsService,
		TimelineMonthService,
		ResizeService,
		DragAndDropService,
	],
	// entryComponents: [MapsComponent]

})
export class CalendarScheduleModule { }
