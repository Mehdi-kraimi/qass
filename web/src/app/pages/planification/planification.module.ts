import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { PlanificationRoutingModule } from './planification-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CommonModules } from 'app/common/common.module';
import { CalendarScheduleModule } from './app-schedule/app-schedule.module';
import { ShowvisiteModule } from './show-visite/showvisite.module';
import { FicheInterventionModule } from '../ficheInterv/ficheIntervention.module';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';


export function createTranslateLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
		{ prefix: './assets/i18n/prestation/', suffix: '.json' },
		{ prefix: './assets/i18n/ficheinterventionmaintenance/', suffix: '.json' },
	]);
}

@NgModule({
	declarations: [IndexComponent],
	imports: [
		CommonModule,
		CalendarScheduleModule,
		PlanificationRoutingModule,
		FicheInterventionModule,
		CommonModules,

		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		FormsModule,
		ReactiveFormsModule,


		AngularEditorModule,

		NgSelectModule,
		NgbTooltipModule,
		// CalendarModule,
		DataTablesModule,
		// SplitButtonModule,
		InfiniteScrollModule,
		ShowvisiteModule
	]
})
export class PlanificationModule { }
