import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { EditFicheInterventionComponent } from '../ficheInterv/details-ficheIntervention/edit-ficheIntervention/edit-ficheIntervention.component';

const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				component: IndexComponent,
			},

			{
				path: 'edit/:id',
				component: EditFicheInterventionComponent,
				data: {
					isPlanification: 'planification '
				}
			},
			{
				path: 'detail/:id',
				loadChildren: 'app/pages/ficheInterv/details-ficheIntervention/details-ficheIntervention.module#DetailsFicheInterventionModule',
				data: {
					isPlanification: 'planification '
				}
			},
			{
				path: 'create',
				component: EditFicheInterventionComponent,
				data: {
					isPlanification: 'planification '
				}
			},

		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PlanificationRoutingModule { }
