import { HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, Inject } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { SelectContractComponent } from './select-contract/select-contract.component';
import { ContratEntretien } from '../../../../Models/Entities/Maintenance/ContratEntretien';
import { PublishingContract } from '../../../../Models/Entities/publishing-contract/publishing-contract';
import { FillTagsComponent } from '../../../../common/fill-tags/fill-tags.component';
import { ApiUrl } from '../../../../Enums/Configuration/api-url.enum';
import { FileUtils } from '../../../../shared/utils/file';
import { PublishingService } from '../../../../services/publishing/publishing.service';
import { IGenericRepository } from '../../../../shared/repository/igeneric-repository';
import { AppSettings } from '../../../../app-settings/app-settings';


declare var toastr: any;

@Component({
	selector: 'app-contract',
	templateUrl: './contract.component.html'
})
export class ContractComponent implements OnInit {

	@Input() contratEntretien: ContratEntretien;
	contracts: PublishingContract[] = [];

	/**
	* emit parent success operations ( add, update )
	*/
	@Output() onSuccess = new EventEmitter();

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private modalService: NgbModal,
		private servicePublishing: PublishingService,
		// private businessManagementService: BusinessManagementService,
		private translate: TranslateService,
		// private publishingService: PublishingService
	) { }

	ngOnInit() {
	}

	ngOnChanges() {
	}


	/**
	* add new contract
	*/
	async addContract() {
		const contract = await this.selectContract()

		if (contract) {
			const listTags = [];
			Object.keys(contract.tags).forEach(tags => listTags.push(tags));
			if (listTags.includes('#AdresseClient#')) contract.tags['#AdresseClient#'] = this.contratEntretien.client.addresses.find(a => a.isDefault === true)['street'];
			if (listTags.includes('#NomClient#')) contract.tags['#NomClient#'] = this.contratEntretien.client.name;
			if (listTags.includes('#DateDébut#')) contract.tags['#DateDébut#'] = this.contratEntretien.startDate;
			if (listTags.includes('#DateFin#')) contract.tags['#DateFin#'] = this.contratEntretien.endDate;
			if (listTags.includes('#NumContrat#')) contract.tags['#NumContrat#'] = this.contratEntretien.reference;
			if (listTags.includes('#TotalTTC#')) contract.tags['#TotalTTC#'] = this.contratEntretien.orderDetails.totalTTC;
			if (listTags.includes('#TotalHT#')) contract.tags['#TotalHT#'] = this.contratEntretien.orderDetails.totalHT;
			if (listTags.includes('#site#')) contract.tags['#site#'] = this.contratEntretien.site.street;
			const tagsFilled = await this.fillTags(Object.keys(contract.tags), contract.tags);
			contract.tags = tagsFilled;
			contract.id = new Date().getTime().toString();
			contract.id = parseInt(contract.id.substring(contract.id.length, 7), 0);

			this.contratEntretien.publishingContracts.push(contract);
			// const businessManagementContract: PublishingContract = {
			// 	tags = 
			// 	publishingContractId: contract.id,
			// 	fileName: contract.fileName,
			// }
			this.updateContratEntretien();
		}
	}

	// #region modals

	/**
	 * select contract modal
	 */
	async selectContract() {
		const referenceModal = this.modalService.open(SelectContractComponent, { centered: true });
		return await referenceModal.result;
	}

	/**
	* fill tags of contract
	*/
	async fillTags(tags: string[], defaultValue: any) {
		const referenceModal = this.modalService.open(FillTagsComponent, { centered: true });
		referenceModal.componentInstance.tags = tags;
		referenceModal.componentInstance.defaultValue = defaultValue;
		return await referenceModal.result;
	}

	// #endregion modals

	// #region services

	/**
	* add business management contract model
	*/
	// addBusinessManagementContract() {
	updateContratEntretien() {
		this.service.updatePublishing(`MaintenanceContract/${this.contratEntretien.id}/Update/PublishingDocument`,
			this.contratEntretien.publishingContracts,
			this.contratEntretien.id)
			.subscribe(result => {
				if (result) {
					toastr.success(this.translate.instant('toast.add-sucsess'), { positionClass: 'toast-top-center', containerId: 'toast-top-center' })
					this.onSuccess.emit('');
				} else {
					toastr.error(this.translate.instant('error.server'), { positionClass: 'toast-top-center', containerId: 'toast-top-center' })
				}
			});
	}

	// /**
	//  * update business management contract
	//  * @param id the id of business management contract
	//  * @param businessManagementContract the business management contract model
	//  */
	// updateBusinessManagementContract(id: number, businessManagementContract: BusinessManagementContractModel) {
	// 	this.businessManagementService.UpdateBusinessManagementContract(id, businessManagementContract).subscribe(result => {
	// 		if (result.status === ResultStatus.Succeed) {
	// 			Toast.success(this.translate.instant('success.update'))
	// 			this.onSuccess.emit('');
	// 		} else {
	// 			Toast.error(this.translate.instant('error.server'))
	// 		}
	// 	});
	// }

	// #endregion

	// #region actions

	/**
	 * fill tags of contract
	 * @param index the index of item
	 */
	async fillTagsAction(index: number) {
		const Contract = this.contratEntretien.publishingContracts[index];
		const tags = Contract.tags;
		const tagsFilled = await this.fillTags(Object.keys(tags), tags);
		if (tagsFilled != null) {
			Contract.tags = tagsFilled;
			// businessContract.statePublishingContract = JSON.stringify(Contract);
			this.updateContratEntretien();
			// this.updateBusinessManagementContract(businessContract.id, businessContract);
		}
	}

	/**
	 * delete publishing contract
	 * @param id the id of publishing contract
	 */
	deleteAction(index: number) {
		this.contratEntretien.publishingContracts.splice(index, 1);
		this.updateContratEntretien();
	}

	/**
	* display a contract action
	*/
	downloadAction(id: number) {
		const contract = this.contratEntretien.publishingContracts.find(e => e.id === id);
		this.servicePublishing.downloadPublishingContrat(`${ApiUrl.PublishingDownload}/${contract.id}/MaintenanceContract/${this.contratEntretien.id}`
		).subscribe(data => {
			let progress = 0;
			if (data.type === HttpEventType.UploadProgress) {
				progress = Math.round(100 * data.loaded / data.total);
				// this.loaderService.show(progress)
			} else if (data.type === HttpEventType.Response) {
				// this.loaderService.hide()
				FileUtils.downloadBlob(data.body, contract.orginalFileName)
			}
		})
	}

	// #endregion

	// #region tools

	/**
	* de serialize contract
	*/
	// deSerializeContract = (contract: BusinessManagementContract) => JSON.parse(contract.statePublishingContract);

	// #endregion

}
