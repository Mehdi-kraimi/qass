import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './add/add.component';
import { ShowComponent } from './show/show.component';
import { ShowComponentVisiteMaintenance } from '../visiteMaintenence/show/show.component';

const routes: Routes = [
	{
		path: '',
		children: [
			{

				path: '',
				pathMatch: 'full',
				loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',

			},
			{
				path: 'create',
				component: AddComponent
			},
			{
				path: 'dupliquer-create/:id',
				component : AddComponent
			},
			{
				path: 'create/:id',
				component: AddComponent
			},
			{
				path: 'edit/:id',
				component: AddComponent
			},
			{
				path: 'detail/:id',
				component: ShowComponent
			},
			{
				path: 'detail/:idContrat/visitemaintenance/:id',
				component: ShowComponentVisiteMaintenance,
				data: {
					title: 'detail visite maintenance'
				}
			},

			{
				path: 'detail/:idContrat/visitemaintenance/:idContratVisite/:Year/:month',
				component: ShowComponentVisiteMaintenance,
			},

		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ContratEntretienRoutingModule { }
