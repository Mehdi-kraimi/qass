import { Component, OnInit, Output, EventEmitter, Input, ViewChild, Inject } from '@angular/core';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Client } from 'app/Models/Entities/Contacts/Client';
import * as _ from 'lodash';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
declare var toastr: any;
declare var swal: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'show-Groupe',
	templateUrl: './show.component.html',
	styleUrls: ['./show.component.scss']
})
export class ShowComponentGroupe implements OnInit {

	groupe: Groupe;
	@ViewChild('InputString') public InputString;

	form;
	client: Client;
	listClients: Client[] = [];
	IdChantier
	name: string;

	constructor(
		public router: Router,
		public dialogRef: MatDialogRef<ShowComponentGroupe>,
		@Inject(MAT_DIALOG_DATA) public data: { data: Groupe },
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		private translate: TranslateService) { }

	async ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.GetGroupeById(this.data.data)
		await this.getClients();
	}

	getClients() {
		this.service.getAll(ApiUrl.Client).subscribe(res => {
			this.listClients = res.value.filter(client => client.groupeId === null);
		});
	}

	GetGroupeById(id) {
		this.service.getById(ApiUrl.Groupe, id).subscribe(res => {
			this.groupe = res.value
		})
	}

	close() {
		this.dialogRef.close();
	}


	async addclientToGroupe() {
		this.client.groupeId = this.groupe.id;

		try {
			await this.update(this.client);
			this.listClients.forEach((x, index) => {
				if (x.id = this.client.id) {
					this.listClients.splice(index, 0);
				}
			})

			this.GetGroupeById(this.data.data);
			this.router.navigate(['/groupes']);
			this.translate.get('add').subscribe(text => {
				toastr.success(text.msg, text.title, {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
			// afficher msg success
		} catch (ex) {
			// afficher msg d error
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.serveur, '', {
					positionClass: 'toast-top-center',
					containerId: 'toast-top-center',
				});
			});
		}
	}

	/**
	 * Supprimer client pour groupe
	 * @param client
	 * @param index
	 */
	async removeClientFromGroupe(client, index) {
		this.translate.get('groupes.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.questionclient,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(async isConfirm => {
				if (isConfirm) {
					try {
						client.groupeId = null;
						await this.update(client);
						this.listClients.unshift(client);
						await this.getClients();
						this.groupe.clients.splice(index, 0);
						swal(text.success, '', 'success');
						this.GetGroupeById(this.data.data);

					} catch (ex) {
						swal(text.error, '', 'error');
					}
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}
	update(client: Client) {
		return new Promise((resolve, reject) => {
			this.service.updateAll(ApiUrl.Client + '/' + client.id + ACTION_API.update, client).subscribe(res => {
				resolve(res);
				this.GetGroupeById(this.data.data);
			}, error => {
				reject(error)
			})
		});
	}


	lgbtnclick() {
		alert(this.name);
	}

	setSelectedClient(value) {
		this.client = value;
	}
}
