import { Component, OnInit, OnChanges, Input, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { Router } from '@angular/router';

declare var toastr: any;
declare var jQuery: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'Add-Groupe',
	templateUrl: './add.component.html',
	styleUrls: ['./add.component.scss']
})
export class AddGroupeComponent implements OnInit, OnChanges {

	form;
	constructor(
		private router: Router,
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public dialogRef: MatDialogRef<AddGroupeComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { userId: string },
		private fb: FormBuilder,
		private translate: TranslateService) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.form = this.fb.group({
			Name: ['', Validators.required, this.checkUniqueNom.bind(this)]
		});
	}

	close() {
		this.dialogRef.close();
	}

	checkUniqueNom(control: FormControl) {
		const promise = new Promise((resolve) => {
			this.service.getById(ApiUrl.Groupe + '/IsNameUnique', control.value).
				subscribe(res => {
					if (!res) {
						resolve({ checkUniqueNom: true });
					} else {
						resolve(null);
					}
				});
		});
		return promise;
	}

	ngOnChanges(): void {
	}
	get f() { return this.form.controls; }

	add() {
		const grp = new Groupe();
		grp.name = this.form.value.Name;
		this.service.create(ApiUrl.Groupe + ACTION_API.create, grp).subscribe(res => {
			this.dialogRef.close();
			this.router.navigate(['/groupes']);
			this.translate.get('groupes').subscribe(labels => {
				toastr.success(labels.addMsg, labels.addTitle, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		}, err => {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		});
	}
}
