import { Component, OnInit, Input, OnChanges, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

declare var toastr: any;
declare var jQuery: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'Edit-Groupe',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.scss']
})
export class EditGroupeComponent implements OnInit, OnChanges {

	form;
	groupe: Groupe;

	constructor(
		private router: Router,
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		public dialogRef: MatDialogRef<EditGroupeComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { data: Groupe },
		private translate: TranslateService) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.groupe = this.data.data;
		this.form = this.fb.group({
			Name: [this.groupe.name, Validators.required, this.checkUniqueNom.bind(this)]
		});
	}

	checkUniqueNom(control: FormControl) {
		const promise = new Promise((resolve, reject) => {
			this.service
				.getById(ApiUrl.Groupe + '/IsNameUnique', control.value)
				.subscribe(res => {
					(!res && this.groupe.name !== control.value) ? resolve({ checkUniqueNom: true }) : resolve(null)
				});
		});
		return promise;
	}

	ngOnChanges(): void {
	}

	get f() { return this.form.controls; }


	close() {
		this.dialogRef.close();
	}

	update() {
		const grp = new Groupe();
		grp.id = this.groupe.id;
		grp.name = this.form.value.Name;

		this.service.updateAll(ApiUrl.Groupe + '/' + grp.id + ACTION_API.update, grp)
			.subscribe(res => {
				this.dialogRef.close();
				this.router.navigate(['/groupes']);
				this.translate.get('groupes').subscribe(groupes => {
					toastr.success(groupes.updateMsg, groupes.updateTitle, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			}, err => {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		});
	}
}
