import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ACTION_API, ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { Invoices, PaymentAdd } from 'app/Models/Model/PaymentAdd';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
declare var toastr: any;

@Component({
	selector: 'select-facture',
	templateUrl: './select-facture.component.html',
	styleUrls: ['./select-facture.component.scss']
})
export class SelectFactureComponent implements OnInit {
	search = ''
	factures: Invoice[] = []
	finished = true;
	page = 1;
	totalPages = 0;
	montant = 0;
	totalMontantFacture = 0
	idChantier = null;

	loading = false;
	clients: Client[] = []
	form
	paiement: PaymentAdd;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb: FormBuilder,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<SelectFactureComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { idClient: string, paiement: PaymentAdd, montant },
		private dialog: MatDialog,
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.createForm();
	}
	getPlaceholder() {
		return 'Recherche';
	}
	createForm() {
		this.form = this.fb.group({
			'montant': [null, [Validators.required]],
			'idCaisse': [null, [Validators.required]],
			'datePaiement': [new Date(), [Validators.required]],
			'idModePaiement': [null, [Validators.required]],
			'description': ['Paiement factures'],
		})
	}
	ngOnInit() {
		if (this.data) {
			this.paiement = this.data.paiement;
			this.GetFactures();
			this.montant = this.data.montant
		}
	}
	/**
	 * Liste des facture par client
	 */
	getFilters() {
		return {
			'externalPartnerId': this.data.idClient,
			'workshopId': this.idChantier,
			'status': [StatutFacture.Encours, StatutFacture.Enretard],
			'dateStart': '',
			'dateEnd': '',
			'searchQuery': this.search,
			'page': this.page,
			'pageSize': 10,
			'sortDirection': 'Ascending',
			'orderBy': 'creationDate',
			'ignorePagination': false
		};
	}
	GetFactures() {
		this.service.getAllPagination(ApiUrl.Invoice, this.getFilters())
			.subscribe(res => {
				this.totalPages = res.pagesCount;
				const newFactures = res.value.filter(value => this.factures.filter(x => x.id === value.id).length === 0);
				newFactures.forEach(
					item => {
						const restPayer = item.restToPay
						const totalFacture = this.totalMontant();
						const restMontant = AppSettings.formaterNumber(this.montant) - AppSettings.formaterNumber(totalFacture)
						if (restMontant > 0) {
							item['montant'] = (AppSettings.formaterNumber(restMontant) - AppSettings.formaterNumber(restPayer) < 0 ? restMontant : restPayer)
							item['checked'] = true
						}
						this.factures.push(item);
					}
				)
			})
	}

	// calcule total des montants des factures selectioné
	totalMontant() {
		return this.factures.reduce((x, y) => x + (y['montant'] == null ? 0 : y['montant']), 0)
	}

	// Vérifier si la montant saisir dans facture est valide
	checkValidMontant(index) {
		const facture = this.factures[index];
		if (facture['montant'] == null || facture['montant'] <= 0 || AppSettings.formaterNumber(
			this.getResterPayer(facture.totalTTC, facture.payments, facture['montant'])) < 0) {
			this.factures[index]['invalid'] = true;
			return true;
		}
		this.factures[index]['invalid'] = false;
		return false;
	}

	getResterPayer(total, facturePaiements, montant = 0) {
		if (total != null && facturePaiements != null) {
			return total - facturePaiements.reduce((x, y) => x + y.montant, 0) - montant
		} else {
			return 0;
		}
	}

	// Recherche dans les factures
	searchFacture() {
		this.factures = this.factures.filter(x => x['checked'] === true)
		this.page = 1;
		this.totalPages = 0;
		this.GetFactures()
	}

	// Event scroll dans la liste des factures
	onScroll() {
		this.finished = false;
		if (this.totalPages <= this.page) {
			this.finished = true;
		} else {
			this.page++;
			this.GetFactures();
		}
	}
	// Sauvgarder facture
	save() {
		const factureSelectione = this.factures.filter(x => x['checked']);
		if (factureSelectione.filter(x => x['invalid']).length > 0) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		} else if (AppSettings.formaterNumber(this.totalMontant()) > AppSettings.formaterNumber(this.montant)) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.montantSup, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		} else if (AppSettings.formaterNumber(this.totalMontant()) < AppSettings.formaterNumber(this.montant)) {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.montantInf, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		} else if (AppSettings.formaterNumber(this.totalMontant()) === AppSettings.formaterNumber(this.montant)) {
			this.loading = true;
			factureSelectione.forEach(facture => {
				const facturePaiement = new Invoices();
				facturePaiement['documentId'] = facture.id
				facturePaiement['amount'] = facture['montant']
				this.paiement.invoices.push(facturePaiement)
			})
			this.service.create(ApiUrl.Payment + ACTION_API.create, this.paiement).subscribe(res => {
				this.loading = false;
				if (res) {
					this.translate.get('list').subscribe(text => {
						this.form.reset();
						this.form.controls['datePaiement'].setValue(new Date());
						toastr.success(text.success, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						//jQuery('#choixFacture').modal('hide')
						//this.refresh.emit('')
					})
					this.dialogRef.close(res.value);
				} else {
					this.errorMontantIncorrect()
				}
			}, err => {
				this.loading = false;
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.errorServer, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			})
		}
	}

	// Montant incorrect
	errorMontantIncorrect() {
		this.translate.get('errors').subscribe(text => {
			toastr.warning(text.montantNegative, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			setTimeout(() => { location.reload(); }, 1000)
		})
	}


	onFactureCheck(isChecked: boolean, indexFacture: number): void {
		// onFactureCheck
		if (isChecked) {
			this.factures[indexFacture]['montant'] = 0;
		}
	}

	close() { this.dialogRef.close(); }



}
