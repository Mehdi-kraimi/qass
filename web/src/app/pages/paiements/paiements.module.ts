import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaiementsRoutingModule } from './paiements-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModules } from 'app/common/common.module';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { ShowComponent } from './show/show.component';
import { InformationComponent } from './show/information/information.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { MatNativeDateModule, MatDatepickerModule, MatFormFieldModule, MatInputModule, MatTabsModule, MatMenuModule } from '@angular/material';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { SelectFactureComponent } from './index/paiement-groupe/select-facture/select-facture.component';

// export function createTranslateLoader(http: HttpClient) {
// 	return new TranslateHttpLoader(http, "./assets/i18n/paiements/", ".json");
// }
export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/paiements/', suffix: '.json' },
	]);
}
@NgModule({
	declarations: [ShowComponent, InformationComponent],
	imports: [
		CommonModule,
		PaiementsRoutingModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		DataTablesModule,
		CommonModules,
		NgbTooltipModule,
		NgSelectModule,
		FormsModule,
		InfiniteScrollModule,
		ReactiveFormsModule,
		CalendarModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatFormFieldModule,
		MatInputModule,
		MatTabsModule,
		MatMenuModule,
	],
	providers: [
		PreviousRouteService,
	]
})
export class PaiementsModule { }
