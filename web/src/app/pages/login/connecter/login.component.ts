import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { UserStatut } from 'app/Enums/UserStatut.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { TokenHelper } from 'app/shared/utils/token-helper';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { PermissionRole } from 'app/Enums/permissions';
import { FormBuilder, Validators } from '@angular/forms';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { MenuService } from 'app/services/menu/menu.service';
declare var toastr: any;

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	user;
	form;
	view = true;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private router: Router,
		private fb: FormBuilder,
		private translate: TranslateService) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);

		if (TokenHelper.hasValidToken() && this.tokenNotExpired()) {
			this.router.navigateByUrl('/dashboard');
		}

		this.form = this.fb.group({
			'UserName': ['', [Validators.required]],
			'Password': ['', [Validators.required]]
		});
	}

	tokenNotExpired = () => typeof TokenHelper.isExpired() === 'boolean' && !TokenHelper.isExpired();

	login() {

		if (this.form.valid) {
			const login = {
				'userName': this.form.value.UserName,
				'password': this.form.value.Password
			}

			this.translate.get('login').subscribe(text => {
				this.service.create(ApiUrl.login, login)
					.subscribe(
						async response => {
							if (response && response.isSuccess) {
								localStorage.setItem(LocalElements.TOKEN, response.value.token);
								const userId = TokenHelper.GetUserToken().id;
								const res = await this.service.getById(ApiUrl.acount, userId).toPromise();
								this.user = res.value;
								TokenHelper.TypeUser = this.user.role.type;
								const checkProfileAccess = TokenHelper.GetUserToken().permissions.includes(PermissionRole.WebAppAccess);
								if (!checkProfileAccess) {
									toastr.warning(text.accesnonautorise, { positionClass: 'toast-top-center', containerId: 'toast-top-center' })
									return
								} else {
									localStorage.setItem(LocalElements.PDB_USER, JSON.stringify(this.user));
									this.user.statut === UserStatut.active ? this.router.navigate(['/']) : this.router.navigate(['/login/change-password']);
								}

							} else {
								toastr.warning(text.incorrectLogin, { positionClass: 'toast-top-center', containerId: 'toast-top-center' })
							}
						},
						error => {
							toastr.warning(text.incorrectLogin, { positionClass: 'toast-top-center', containerId: 'toast-top-center' })
						});
			})
		} else {
			this.translate.get('login').subscribe(text => {
				toastr.warning(text.incorrectLogin, { positionClass: 'toast-top-center', containerId: 'toast-top-center' })
			});
		}
	}

	onViewPassword() {
		this.view = !this.view;
	}

}
