import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
declare var toastr: any;

@Component({
	selector: 'app-recorver-password',
	templateUrl: './recorver-password.component.html',
	styleUrls: ['./recorver-password.component.scss']
})
export class RecorverPasswordComponent implements OnInit {

	form;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService, private fb: FormBuilder, private router: Router) {
		this.form = this.fb.group({
			'email': ['', [Validators.required, Validators.email]]
		});
	}

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	recover() {
		if (this.form.valid) {
			this.translate.get('recover').subscribe(text => {
				this.service.updateAll(ApiUrl.forgetPassword, { email: this.form.value.email })
					.subscribe(res => {
						if (!res.isSuccess) {
							this.translate.get('recover').subscribe(textt => {
								toastr.error(textt.error, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
							})
						} else {
							toastr.success(text.recoversucses, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
							this.router.navigate(['/login'])
						}
					}, err => {
						if (err.status) {
							toastr.error(text.notExist, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						} else {
							toastr.error(text.error, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
						}
					})
			})
		}
	}

	get f() { return this.form.controls; }
}
