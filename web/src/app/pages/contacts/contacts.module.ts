import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { ContactsRoutingModule } from './contacts-routing.module';
import { MatTabsModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatDialogModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EditContactsComponent } from './details-contacts/edit-contacts/edit-contacts.component';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { CommonModules } from 'app/common/common.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { SplitButtonModule, CalendarModule } from 'app/custom-module/primeng/primeng';


export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
	]);
}


@NgModule({
	providers: [
		PreviousRouteService],
	imports: [
		CommonModule,
		FormsModule,
		ContactsRoutingModule,
		SplitButtonModule,
		InputTextareaModule,
		MatTabsModule,
		MatDatepickerModule,
		ReactiveFormsModule,
		MatSelectModule,
		MatNativeDateModule,
		NgbTooltipModule,
		MatDialogModule,
		CommonModules,
		CalendarModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
	],
	exports: [EditContactsComponent],
	declarations: [EditContactsComponent]
})

export class ContactsModule { }

