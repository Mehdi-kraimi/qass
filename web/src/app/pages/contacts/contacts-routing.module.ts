import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditContactsComponent } from './details-contacts/edit-contacts/edit-contacts.component';

export const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				pathMatch: 'full',
				loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
			},
			{
				path: 'detail/:id',
				loadChildren: './details-contacts/details-contacts.module#DetailsContactsModule'
			},
			{
				path: 'create',
				component : EditContactsComponent
			},
			{
				path: 'generer-create/:id',
				component : EditContactsComponent
			},
			{
				path: 'dupliquer-create/:id',
				component : EditContactsComponent
			},
			{
				path: 'edit/:id',
				component: EditContactsComponent
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ContactsRoutingModule { }
