import { Component, OnInit, Input } from '@angular/core';
import { User } from 'app/Models/Entities/User';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'user-information',
	templateUrl: './information.component.html',
	styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit {

	// tslint:disable-next-line:no-input-rename
	@Input('user') user: User;

	constructor() { }
	ngOnInit() {

	}


}
