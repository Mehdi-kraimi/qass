import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowComponent } from './show.component';

const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
			},
			{
				path: 'create',
				component: ShowComponent
			},
			{
				path: 'create/:id',
				component: ShowComponent,
			},
			{
				path: 'edit/:id',
				component: ShowComponent
			},
			{
				path: 'detail/:id',
				component: ShowComponent
			}

		]
	}
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class GammeMaintenanceEquipementRoutingModule { }
