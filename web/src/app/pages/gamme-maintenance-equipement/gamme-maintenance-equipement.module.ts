import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowComponent } from './show.component';
import { GammeMaintenanceEquipementRoutingModule } from './gamme-maintenance-equipement-routing.module';
import { CommonModules } from 'app/common/common.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { MatDialogModule } from '@angular/material';
import { GammeMaintenanceEquipementModules } from 'app/common/gamme-maintenance-equipement/gamme-maintenance-equipement.module';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';

// export function createTranslateLoader(http: HttpClient) {
// 	return new TranslateHttpLoader(http, './assets/i18n/shared/', '.json');
// 	return new TranslateHttpLoader(http, './assets/i18n/gammemaintenaceequipement/', '.json');

// }


export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/gammemaintenaceequipement/', suffix: '.json' },
	]);
}
@NgModule({
	declarations: [ShowComponent],
	imports: [
		GammeMaintenanceEquipementRoutingModule,
		CommonModule,
		CommonModules,
		// TranslateModule.forChild({
		// 	loader: {
		// 		provide: TranslateLoader,
		// 		useFactory: createTranslateLoader,
		// 		deps: [HttpClient],
		// 	},
		// 	isolate: true,
		// }),
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		ReactiveFormsModule,
		NgSelectModule,
		NgbTooltipModule,
		DataTablesModule,
		GammeMaintenanceEquipementModules,
		MatDialogModule
	],
	providers: [
		PreviousRouteService
	],
	entryComponents: []
})
export class GammeMaintenanceEquipementModule { }
