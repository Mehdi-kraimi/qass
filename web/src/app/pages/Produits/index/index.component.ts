import { Component, OnInit, Inject } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Label } from '../../../Models/Entities/Parametres/Label';
import { LabelShowModel } from '../../../Models/Model/LabelShowModel';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { Router, ActivatedRoute } from '@angular/router';
import { Constants } from 'app/shared/utils/constants';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { HeaderService } from 'app/services/header/header.service';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { CONNREFUSED } from 'dns';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var swal: any;
declare var toastr: any;
@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
	public dtElement: DataTableDirective;
	public Produits;
	public selectedLables: LabelShowModel[] = [];
	public labelsList: LabelShowModel[] = [];
	public labelSearch: string[] = [];
	public searchQuery = '';
	public pageNamber = 1;
	public pageSize = 10;
	public showCrud = true;
	public showAction: number = null;
	public id;
	preventSingleClick = false;
	timer: any;
	profile
	processing = false;
	typeProfile: typeof UserProfile = UserProfile;
	filter;
	filtersOpened = false;
	docType = '';

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate?: TranslateService,
		private router?: Router,
		private header?: HeaderService,
		public activatedRoute?: ActivatedRoute,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.docType = this.activatedRoute.snapshot.data.docType;
		ManageDataTable.docType = this.docType;
	}

	ngOnInit() {
		this.prepareBreadcrumb();
		localStorage.setItem(LocalElements.docType, this.docType); this.filter = {
			SearchQuery: this.searchQuery,
			Page: this.pageNamber,
			PageSize: this.pageSize,
			OrderBy: 'reference',
			SortDirection: 'Ascending',
		};
		this.profile = Constants.getUser()['idProfile'];
		// récupérer la list des produit
		this.GetAll();

		// récupérer la list des labels
		this.GetAllLabels();
	}
	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.Product'), route: ManageDataTable.getListRoute() },
			];
		});
	}
	onFiltersTriggered(): void {
		this.filtersOpened = !this.filtersOpened;
	}
	onBackdropClicked() {
		this.selectedLables = [];
		this.labelSearch = [];
		this.GetAll();
	}
	getPlacholder() {
		const text = this.translate.instant('labels.filtrerLabel')
		return text;
	}


	GetAll(): void {
		this.processing = true;
		if (this.labelSearch.length === 1 && this.labelSearch[0] === '') {
			this.labelSearch = [];
		} else {
			this.filter.label = this.labelSearch;
		}
		this.service.getAllPagination(ApiUrl.Produit, this.filter)
			.subscribe(data => {
				this.Produits = data;
				this.processing = false;
			}, err => this.translate.get('errors').subscribe(errors => {
				if (this.processing) {
					this.processing = false;
					toastr.warning(errors.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			}));
	}


	TotalHt(item) {
		const calculate_cout_horaire = item.totalHours * item.hourlyCost;
		return (calculate_cout_horaire + item.materialCost).toFixed(2)
	}


	// boucle synchrone (helper)
	async asyncForEach(array: any[], callback: any): Promise<any> {
		for (let index = 0; index < array.length; index++) {
			await callback(array[index], index, array)
		}
	}

	GetAllLabels() {
		this.service.getAll(ApiUrl.configurationLabel).subscribe(labels => {
			this.labelsList = this.formaterLabels(labels.value);
		});
	}

	clear() {
		this.labelSearch = [];
		this.GetAll();
	}

	formaterLabels(list: Label[]): LabelShowModel[] {
		const labelsList: LabelShowModel[] = [];
		list.forEach((label, index) => {
			labelsList.push({ id: label.id, name: label.value, disabled: false });
		});
		return labelsList;
	}

	filterParLabel(): void {
		if (this.selectedLables.length > 0) {
			this.filter.Page = 1;
			this.formatLabelsPourFilterDesParduitParLabel(this.selectedLables).then((res: string[]) => {
				this.labelSearch = res;
				this.GetAll();
			})
		} else {
			this.labelSearch = [];
			this.GetAll();
		}

	}

	formatLabelsPourFilterDesParduitParLabel(selectedLablesList: LabelShowModel[]): Promise<string[]> {
		return new Promise((resolve) => {
			let selectedLables: string[] = [];
			if (selectedLablesList.length !== 0) {
				selectedLablesList.forEach(element => {
					selectedLables.push(element.id)
				});
			} else {
				selectedLables = ['']
			}
			resolve(selectedLables);
		});
	}

	searchByQuery(query: string): void {
		if (query.length > 0 && query.length < 3) { return }
		this.filter.Page = 1;
		this.filter.SearchQuery = query;
		this.GetAll();
	}

	chagePageNamber(pageNamber: number): void {
		this.filter.Page = pageNamber;
		this.GetAll();
	}

	changePageSize(size: number): void {
		this.filter.PageSize = size;
		this.GetAll();
	}

	delete(id: number): void {
		this.translate.get('list.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.envoyerDemandeSuppressionAuServeur(id, text);
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	envoyerDemandeSuppressionAuServeur(id, text: any): void {

		this.service.delete(ApiUrl.Produit, id).subscribe(res => {
			if (res) {
				swal(text.success, '', 'success');
				this.GetAll();
			} else {

				swal(text.ImpossibleDeSuppression, '', 'error');
				// swal(text.error, "", "error");
			}
		});
	}

	swalAlertConfig(text: any): object {
		return {
			title: text.title,
			text: text.question,
			icon: 'warning',
			buttons: {
				cancel: {
					text: text.cancel,
					value: null,
					visible: true,
					className: '',
					closeModal: false
				},
				confirm: {
					text: text.confirm,
					value: true,
					visible: true,
					className: '',
					closeModal: false
				}
			}
		};
	}

	compareDates(a1: string, a2: string): boolean {
		const date1 = new Date(a1);
		const date2 = new Date(a2);
		/*-----------------------*/
		let fDate, lDate, cDate;
		fDate = new Date(date1);
		lDate = new Date(date2);
		cDate = Date.now();
		/*-----------------------*/
		if ((cDate <= lDate && cDate >= fDate)) {
			return true;
		}
		return false;
		/*-----------------------*/
	}

	prixTtc(produit): number | 0 {
		if (produit === undefined) { return; }
		return produit.totalHT * ((produit.tva / 100) + 1);
	}

	doubleClick(idProduit) {
		this.preventSingleClick = true;
		clearTimeout(this.timer);
		this.router.navigate(['/produits/detail', idProduit]);
	}
}

