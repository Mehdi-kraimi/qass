import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { AppSettings } from 'app/app-settings/app-settings';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { PrixParFournisseur_response } from 'app/Models/Model/prixParFournisseur';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';
import { LocalElements } from 'app/shared/utils/local-elements';
declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'app-edit-produit',
	templateUrl: './edit-produit.component.html',
	styleUrls: ['./edit-produit.component.scss']
})
export class EditProduitComponent implements OnInit {
	id
	public form;
	public produit;
	public newTags = [];
	public historiques: [];
	public memos: Memo[] = [];
	public memo: Memo = new Memo();
	CoutVenteDefault: any = 0;
	@ViewChild('prix') public prix;
	@ViewChild('nombreMaximum') public nombreMaximum;
	@ViewChild('numberMinimum') public numberMinimum;
	editorConfig: AngularEditorConfig = {
		editable: true,
		spellcheck: true,
		height: '25rem',
		translate: 'yes',

	}
	typeProduct = [];
	public processing = false;
	public tagsSelected: { value: any, origine: boolean }[] = [];
	public loadData: any = {};
	public ListeTva = [];
	public ListeUnite = [];
	public ListeCategorie;
	public allTagsList = [];
	produitSuppliers = [];
	isModif = false;
	docType = '';
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private fb?: FormBuilder,
		private translate?: TranslateService,
		private router?: Router,
		private route?: ActivatedRoute,
		private header?: HeaderService,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.createForm();

	}

	ngOnInit() {
		this.processing = true;
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;

		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
			];

		});
		this.getTypeProduct();
		this.getListeUnite();
		this.getListeCategorie();
		this.route.params.subscribe(params => {
			this.GetAllTags();
			this.getCouteVenteFromParamerage();
			this.id = params['id'];
			// tslint:disable-next-line: triple-equals
			if (this.id != undefined) {
				this.isModif = true;
				this.GetProduit(this.id);
			}

		})
		this.processing = false;
	}
	//#region Form
	createForm() {
		this.form = this.fb.group({
			reference: [null, [Validators.required], this.CheckUniqueReference.bind(this)],
			description: [''],
			designation: ['', [Validators.minLength(2), Validators.required]],
			vat: [0.00],
			unite: [null],
			categoryId: ['', [Validators.required]],
			labels: [null],
			totalHours: [0],
			materialCost: [0.00],
			hourlyCost: [0.00],
			productCategoryTypeId: ['', [Validators.required]],
		});
	}
	get f() { return this.form.controls; }


	InitializeForm(produit) {
		this.form.controls['reference'].setValue(produit.reference);
		this.form.controls['description'].setValue(produit.description);
		this.form.controls['designation'].setValue(produit.designation);
		this.form.controls['categoryId'].setValue(+produit.category.id);
		this.form.controls['labels'].setValue(produit.labels);
		this.form.controls['unite'].setValue(produit.unite);
		this.form.controls['vat'].setValue(produit.vat);
		this.form.controls['totalHours'].setValue(produit.totalHours);
		this.form.controls['materialCost'].setValue(produit.materialCost);
		this.form.controls['hourlyCost'].setValue(produit.hourlyCost);
		this.form.controls['productCategoryTypeId'].setValue(produit.productCategoryType.id);
		this.form.controls['totalHT'] = this.TotalHt();
		this.historiques = produit.historique;
		this.memos = produit.fichesTechniques as Memo[];
	}
	//#endregion
	//#region reference

	CheckUniqueReference(control: FormControl) {
		if (control.value !== '') {
			const promise = new Promise((resolve, reject) => {
				this.service
					.getById(ApiUrl.Produit + ApiUrl.CheckReference, control.value)
					.subscribe(res => {
						if (!res && this.produit && this.form.value.reference !== this.produit.reference) {
							resolve({ CheckUniqueReference: true });
						} else if (!res && !this.produit) {
							resolve({ CheckUniqueReference: true });
						} else {
							resolve(null);
						}
					});
			});
			return promise;
		}
	}


	//#endregion

	//#region fct serveur
	GetProduit(id) {
		this.service.getById(ApiUrl.Produit, id).subscribe(produit => {
			if (produit) {
				this.produit = produit.value;
				this.produitSuppliers = this.produit.productSuppliers;
				this.produit.labels.forEach(tag => {
					this.newTags.push({ value: tag, origine: false });
				});
				this.InitializeForm(this.produit);
				this.translate.get(`doctype.`).toPromise().then((translation: string) => {

					this.header.breadcrumb = [
						{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
						{ label: this.produit.reference, route: ManageDataTable.getRouteDetails(this.produit) }
					];
				});
			}
		});
	}

	getCouteVenteFromParamerage() {

		this.service.getAll(ApiUrl.configDefaultValue).subscribe(res => {
			const valDefault = JSON.parse(res);
			this.form.controls['hourlyCost'].setValue(valDefault.salesPrice);
		});

		this.service.getAll(ApiUrl.configDefaultValue).subscribe(res => {
			const data = JSON.parse(res);
			this.form.controls['vat'].setValue(parseFloat(data['defaultVAT']));
		});

		this.service.getAll(ApiUrl.configDefaultValue).subscribe(res => {
			const valDefault = JSON.parse(res);
			this.CoutVenteDefault = valDefault.prixVente;
			// this.form.controls['vat'].setValue(parseFloat(valDefault.defaultVAT));
		});
	}

	getTypeProduct() {
		this.service.getAll(ApiUrl.typeProduct).subscribe(typeProduct => {
			this.typeProduct = typeProduct.value;
		});
	}
	getListeUnite(): void {
		this.service.getAll(ApiUrl.configurationUnit).subscribe(unite => {
			this.ListeUnite = unite.value;
		});
	}

	getListeCategorie(): void {
		this.service.getAll(ApiUrl.configClassificationSales).subscribe(res => {
			this.ListeCategorie = res.value;
		});
	}
	GetAllTags() {
		this.service.getAll(ApiUrl.configurationLabel).subscribe(tags => {
			tags.value.forEach(tag => {
				this.allTagsList.push({ value: tag, origine: true });
			})
		});
	}
	onTagsChange(Tags: [{ value: string, origine: boolean }]) {
		this.tagsSelected = Tags;
	}

	//#endregion

	//#region fct calcul
	TotalHt() {
		return (this.calculate_cout_horaire() + this.form.value.materialCost).toFixed(2)
	}
	calculate_cout_horaire(): any {
		return parseFloat(this.form.value.totalHours) * parseFloat(this.form.value.hourlyCost);
	}
	prixTtc(cout_materiel: number, tva: number) {
		const total = (this.calculate_cout_horaire() + cout_materiel)
		if (total != null) {
			return (total * (1 + tva / 100)).toFixed(2)
		} else {
			return 0;
		}
	}
	//#endregion
	//#region save
	sauvegarderUpdate(produit, text) {
		this.service.update(ApiUrl.Produit, produit, this.id).subscribe(
			res => {
				if (res) {
					toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					this.router.navigate(['/produits/detail', res.value.id]);
				}
			}
		);
	}
	sauvegarderadd(formValues) {
		this.service.create(ApiUrl.Produit + ACTION_API.create, formValues).subscribe(res => {
			if (res) {
				this.translate.get('add').subscribe(text => {
					toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
				this.router.navigate(['/produits/detail', res.value.id]);
			}
		});
	}
	save() {
		if (!this.isModif) {
			this.add();
		}
		if (this.isModif) {
			this.update();
		}
	}
	async add() {
		if (this.form.valid) {
			const formValues = this.form.value;
			formValues['labels'] = [];
			this.tagsSelected.forEach(tag => {
				formValues['labels'].push(tag.value);
			});

			const valid = this.produitSuppliers.find(x => x.isDefault === true);
			if (!valid && this.form.value.materialCost !== 0) {
				const text = this.translate.instant('PrixParFournisseur.errors.required')
				toastr.warning(text, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' }); return false
			}

			formValues['productSuppliers'] = this.produitSuppliers;
			formValues['totalHT'] = this.TotalHt();
			formValues['totalTTC'] = this.prixTtc(this.form.value.materialCost, this.form.value.vat);

			if (valid && this.form.value.materialCost === 0) {
				this.translate.get('PrixParFournisseur.prix').subscribe(async text => {
					swal(this.swalWarningConfig(text)).then(isConfirm => {
						if (isConfirm) {
							this.sauvegarderadd(formValues)
						}
					});
				});
			} else {
				this.sauvegarderadd(formValues);
			}

		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}



	update() {
		if (this.form.valid) {
			this.translate.get('update').subscribe(async textt => {

				const produit = this.form.value;
				produit['id'] = this.id;
				produit['historique'] = this.historiques;
				produit['dataSheets'] = this.memos;
				produit['hourlyCost'] = this.CoutVenteDefault;
				produit['labels'] = [];
				this.tagsSelected.forEach(tag => {
					produit['labels'].push(tag.value);
				});

				const valid = this.produitSuppliers.find(x => x.isDefault === true);
				if (!valid && this.form.value.materialCost !== 0) {
					const text = this.translate.instant('PrixParFournisseur.errors.required')
					toastr.warning(text, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					return false
				}

				produit['productSuppliers'] = this.produitSuppliers;

				produit['productSuppliers'].forEach(item => {
					delete item.fournisseur
				});

				produit['totalHT'] = this.TotalHt();
				produit['totalTTC'] = this.prixTtc(this.form.value.materialCost, this.form.value.vat);

				if (valid && this.form.value.materialCost === 0) {
					this.translate.get('PrixParFournisseur.prix').subscribe(async text => {
						swal(this.swalWarningConfig(text)).then(isConfirm => {
							if (isConfirm) {
								this.sauvegarderUpdate(produit, textt)
							} else {
							}
						});
					});
				} else {
					this.sauvegarderUpdate(produit, textt);
				}
			});
		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

	swalWarningConfig = (text: any) => {
		return {
			title: text.title,
			text: text.question,
			icon: 'warning',
			buttons: {
				cancel: {
					text: text.cancel,
					value: null,
					visible: true,
					className: '',
					closeModal: true
				},
				confirm: {
					text: text.confirm,
					value: true,
					visible: true,
					className: '',
					closeModal: true
				}
			}
		};
	};

	//#endregion

}
