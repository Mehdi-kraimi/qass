import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component'
import { ProduitRoutingModule } from './produit-routing.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { CommonModules } from './../../common/common.module';
import { ShowComponent } from '../Produits/show/show.component';
import { InformationsComponent } from '../Produits/show/informations/informations.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { ShowCrudInProduitModule } from '../../common/Helpers/ShowCrudInProduitModule';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeDECH from '@angular/common/locales/fr-BI';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { MatDialogModule, MatTableModule, MatSortModule, MatCheckboxModule, MatPaginatorModule, MatTooltipModule, MatIconModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatMenuModule, MatDatepickerModule, MatNativeDateModule, MatTabsModule } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { EditProduitComponent } from './edit-produit/edit-produit.component';
import { FournisseurCardModule } from 'app/shared/fournisseur-card/fournisseur-card.module';

registerLocaleData(localeDECH);

export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/produits/', suffix: '.json' },
	]);
}

@NgModule({
	providers: [
		ShowCrudInProduitModule,
		{ provide: LOCALE_ID, useValue: 'fr-BI' },
	],
	imports: [
		CommonModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		ProduitRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		DataTablesModule,
		CommonModules,
		NgbTooltipModule,
		FournisseurCardModule,
		NgSelectModule,
		AngularEditorModule,
		InputTextareaModule,
		MatDialogModule,
		MatTableModule,
		OverlayModule,
		MatSortModule,
		NgSelectModule,
		CalendarModule,
		MatCheckboxModule,
		NgbTooltipModule,
		MatPaginatorModule,
		InfiniteScrollModule,
		MatTooltipModule,
		MatIconModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatMenuModule,
		ShowHidePasswordModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatTabsModule,

	],
	declarations: [IndexComponent, ShowComponent, InformationsComponent, EditProduitComponent]
})
export class ProduitModule { }
