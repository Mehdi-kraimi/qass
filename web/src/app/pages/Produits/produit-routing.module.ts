import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { ShowComponent } from './show/show.component';
import { EditProduitComponent } from './edit-produit/edit-produit.component';
// import { CartComponent } from './cart/cart.component';


const routes: Routes = [{
	path: '',
	children: [
		{
			path: '',
			component: IndexComponent,
			data: {
				title: 'Produits'
			}
		}, {
			path: 'ajouter',
			component: EditProduitComponent,
			data: {
				title: 'Ajouter Produit'
			}
		}, {
			path: 'modifier/:id',
			component: EditProduitComponent,
			data: {
				title: 'modifier Produit'
			}
		}, {
			path: 'detail/:id',
			component: ShowComponent,
			data: {
				title: 'detail Produit'
			}
		}
	]
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ProduitRoutingModule { }
