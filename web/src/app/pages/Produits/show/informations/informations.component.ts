import { Component, OnInit, Input } from '@angular/core';
import { conversion } from 'app/common/prix-conversion/conversion';

@Component({
	selector: 'produit-informations',
	templateUrl: './informations.component.html',
	styleUrls: ['./informations.component.scss']
})

export class InformationsComponent implements OnInit {

	@Input('produit') produit;
	@Input('PrixParFournisseur') PrixParFournisseur = [];
	@Input('prixParTranche') prixParTranche;
	@Input('allTagsList') allTagsList = [];
	images;
	conversion = new conversion()

	constructor() { }

	ngOnInit() {
	}


	prixTtc(cout_materiel: number, tva: number) {
		if (this.calculate_cout_horaire() != null && cout_materiel != null) {
			return ((this.calculate_cout_horaire() + cout_materiel) * (1 + tva / 100)).toFixed(2)
		} else {
			return 0;
		}
	}

	TotalHt() {
		if (!this.produit) {
			return;
		}
		return (this.calculate_cout_horaire() + this.produit.materialCost).toFixed(2)
	}


	calculate_cout_horaire(): any {
		if (!this.produit) {
			return;
		}
		return parseFloat(this.produit.totalHours) * parseFloat(this.produit.hourlyCost);
	}
}
