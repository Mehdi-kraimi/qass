import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Historique } from 'app/Models/Entities/Commun/Historique';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Memo } from 'app/Models/Entities/Commun/Memo';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { GlobalInstances } from 'app/app.module';
import { HeaderService } from 'app/services/header/header.service';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { TranslateConfiguration } from 'app/libraries/translation';
import { LocalElements } from 'app/shared/utils/local-elements';


declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'app-show',
	templateUrl: './show.component.html',
	styleUrls: ['./show.component.scss']
})

export class ShowComponent implements OnInit {
	public id;
	public produit;
	public historique: Historique[];
	public memos: Memo[] = [];
	public allTagsList = [];
	public showCrud = true;
	public processIsStarting = false;
	public PrixParFournisseur: any;
	// public pomotionList: promotion[] = [];
	PrixParDefaut = null;
	selectedTabs = 'information';
	docType = '';

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private route?: ActivatedRoute,
		private translate?: TranslateService,
		private header?: HeaderService,
		public activatedRoute?: ActivatedRoute,
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);

	}

	ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType);;
		ManageDataTable.docType = this.docType;
		this.route.params.subscribe(params => {
			this.id = params['id'];
			this.GetProduit();
			if (this.produit != undefined) {
				this.prepareBreadcrumb();
			}
		});

	}
	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
	}
	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('doctype.' + ManageDataTable.docType), route: ManageDataTable.getListRoute() },
				{ label: this.produit.reference, route: ManageDataTable.getRouteDetails(this.produit) }
			];
		});
	}
	GetProduit() {
		this.service.getById(ApiUrl.Produit, this.id).subscribe(value => {
			this.produit = value.value;

			this.memos = this.produit.memo;
			const labels: any = this.produit.labels;
			labels.forEach(tag => {
				this.allTagsList.push({ value: tag, origine: false });
			});
			this.historique = this.produit.changesHistory;
		});
	}
	modifier() {
		return GlobalInstances.router.navigateByUrl(`/produits/modifier/${this.produit.id}`);
	}

	getimagesSrc(images): Promise<{ name: string, base64: string }[]> {
		const getbase64 = (img) => {
			return new Promise((resolve, reject) => {
				// this.fileManagerService.Get(img).subscribe(res => {
				// 	resolve({ name: img, base64: res });
				// 	// resolve({ name: img, base64: res.data });
				// });
			});
		};

		return new Promise((resolve, reject) => {
			try {
				const imagesBase64 = [];
				(images).forEach(img => {
					getbase64(img).then(res => {
						imagesBase64.push(res);
					});
				})
				resolve(imagesBase64)
			} catch (ex) {
				reject(ex)
			}
		});
	}

	saveMemo(memo) {
		memo['id'] = AppSettings.guid();
		// if (memo.attachments.length === 0) {
		// 	toastr.warning('Ajouter une fichier', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		// 	return
		// }
		this.processIsStarting = true;
		this.service.create(ApiUrl.File + '/' + this.id + '/Create/Memo', memo)
			.subscribe(async res => {
				if (res) {
					this.GetProduit();
					this.processIsStarting = false;
				}

			}, err => { console.log(err); this.processIsStarting = false; });
	}

	downloadPieceJointe(event) {
		const pieceJointe = event;
		this.service.getById(ApiUrl.File, pieceJointe.fileId).subscribe(
			value => {
				AppSettings.downloadBase64(value.value, pieceJointe.fileName,
					value.value.substring('data:'.length, value.value.indexOf(';base64')),
					pieceJointe.fileType)
			}
		)
	}

	deleteMemo(index: number) {
		this.translate.get('commun.deleteFicheTechnique').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					const id = this.memos[index].id;
					const deleteId = { 'memosIds': [id] };
					this.service.create(ApiUrl.File + '/' + this.id + '/Delete/Memo', deleteId)
						.subscribe(async res => {
							this.GetProduit();
							this.processIsStarting = false;
						}, err => { console.log(err); this.processIsStarting = false; });
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	updateMemo(data: { memo: Memo, index: number }) {
		// if (data.memo.attachments.length === 0) {
		// 	toastr.warning('Ajouter une fichier', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		// 	return
		// }
		const id = this.memos[data.index].id;
		this.service.create(ApiUrl.File + '/' + this.id + '/Update/Memo/' + id, data.memo).subscribe(res => {
			if (res) {
				this.translate.get('commun.update').subscribe(text => {
					this.GetProduit();
					toastr.success(text.success, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			}
		});
	}
}
