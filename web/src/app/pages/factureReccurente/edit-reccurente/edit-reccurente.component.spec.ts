import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditReccurenteComponent } from './edit-reccurente.component';

describe('EditReccurenteComponent', () => {
  let component: EditReccurenteComponent;
  let fixture: ComponentFixture<EditReccurenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditReccurenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditReccurenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
