import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ShowComponent } from './chantier-details/chantier-details.component';
import { RebriqueContainerComponent } from './rebrique/rebrique-container/rebrique-container.component';
import { AddFactureSituationComponent } from '../../components/add-facture-situation/add-facture-situation.component';
import { AddFactureAcompteComponent } from '../../components/add-facture-acompte/add-facture-acompte.component';
import { AddFactureClotureComponent } from '../../components/add-facture-cloture/add-facture-cloture.component';
import { EditDocumentComponent } from '../documents/details-document/edit-document/edit-document.component';
//import { ff } from 'app/pages/documents/details-document';

const routes: Routes = [{
	path: '',
	children: [
		{
			path: '',
			pathMatch: 'full',
			loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
		},
		{
			path: 'detail/:id',
			component: ShowComponent,
		},
		{
			path: ':id/documents/:name/:module',
			component: RebriqueContainerComponent,
		},

		{
			path: ':idChantier/documents/:type/create/devis',
			component: EditDocumentComponent,
		},
		{
			path: ':idChantier/documents/:type/create/facture',
			component: EditDocumentComponent,
		},
		{
			path: ':idChantier/documents/:type/create/commandes',
			component: EditDocumentComponent,
		},
		{
			path: ':idChantier/documents/1/detail/:id',
			loadChildren: 'app/pages/documents/details-document/details-document.module#DetailsDocumentModule',
		},
		{
			path: ':idChantier/documents/1/:idDevis/factures/detail/:id',
			loadChildren: 'app/pages/documents/details-document/details-document.module#DetailsDocumentModule',
		},
		{
			path: ':idChantier/documents/1/edit/:id',
			component: EditDocumentComponent,
		},
		{
			path: ':idChantier/documents/1/:id/factureSituation',
			component: AddFactureSituationComponent,
		},
		{
			path: ':idChantier/documents/1/:id/factureSituationCloture',
			component: AddFactureClotureComponent,
		},
		{
			path: ':idChantier/documents/1/:id/factureAcompte',
			component: AddFactureAcompteComponent,
		},
		{
			path: ':idChantier/documents/1/dupliquer-create/:id',
			component: EditDocumentComponent
		},
		{
			path: ':idChantier/documents/3/create',
			component: EditDocumentComponent,
		},
		{
			path: ':idChantier/documents/3/generer-create/:id',
			component: EditDocumentComponent
		},
		{
			path: ':idChantier/documents/3/dupliquer-create/:id',
			component: EditDocumentComponent
		},
		{
			path: ':idChantier/documents/3/edit/:id',
			component: EditDocumentComponent,
		},
		{
			path: ':idChantier/documents/3/detail/:id',
			loadChildren: 'app/pages/documents/details-document/details-document.module#DetailsDocumentModule',
		},
		{
			path: ':idChantier/documents/2/detail/:id',
			loadChildren: 'app/pages/documents/details-document/details-document.module#DetailsDocumentModule',
		},
		{
			path: ':idChantier/documents/2/generer-create/:id',
			component: EditDocumentComponent
		},
		{
			path: ':idChantier/documents/2/dupliquer-create/:id',
			component: EditDocumentComponent
		},
		{
			path: ':idChantier/documents/2/create',
			component: EditDocumentComponent
		},
		{
			path: ':idChantier/documents/2/edit/:id',
			component: EditDocumentComponent
		},
	]
}];

@NgModule({
	// imports: [
	// 	RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
	// ],
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ChantierRoutingModule { }
