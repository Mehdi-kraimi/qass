import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Historique } from 'app/Models/Entities/Commun/Historique';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { StatutChantier } from 'app/Enums/Statut/StatutChantier.Enum';
import { RetenueGarantieModel } from 'app/Models/Model/RetenueGarantieModel';
import { StatutRetenueGarantie } from 'app/Enums/DelaiGaranties.Enum';
import { IFormType } from 'app/pages/lots/lots-form/IFormType.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { HeaderService } from 'app/services/header/header.service';
import { DialogHelper } from 'app/libraries/dialog';
import { chantierFromComponent } from 'app/common/chantier-form/from.component';
import { MatDialog } from '@angular/material';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { MenuService } from 'app/services/menu/menu.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var swal: any;
declare var toastr: any;


@Component({
	selector: 'app-chantier-details',
	templateUrl: './chantier-details.component.html',
	styleUrls: ['./chantier-details.component.scss']
})
export class ShowComponent implements OnInit {

	chantier;
	historique: Historique[] = [];
	formType: typeof IFormType = IFormType;
	id: number;
	statutChantier: typeof StatutChantier = StatutChantier;
	statuts: { id: number, label: string, color: string }[];
	processing = false;
	recapitulatifFinancierData = null;
	statutsRetenueGarantie: { id: number, label: string, color: string }[];
	statutRetenueGarantie: typeof StatutRetenueGarantie = StatutRetenueGarantie;
	listRetenueGaranties: RetenueGarantieModel[] = [];
	nbDocuments;
	activetab = '';
	public formConfig = {
		type: null,
		defaultData: null
	}

	selectedTabs = 'information';

	constructor(
		private dialog: MatDialog,
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private route: ActivatedRoute,
		private translate: TranslateService,
		private header: HeaderService,
		public menu: MenuService,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
	}

	async ngOnInit() {
		this.activetab = localStorage.getItem('activetab');
		await this.refresh();
		this.translate.get('statutsRetenue').subscribe((statuts: { id: number, label: string, color: string }[]) => {
			this.statutsRetenueGarantie = statuts;
		});

		this.prepareBreadcrumb();
		if (this.activetab === 'potes') {
			let doc = document.getElementById('potes-tab');
			doc.click();
		}
	}
	prepareBreadcrumb(): void {
		//this.translate.get(`title`).toPromise().then((translation: string) => {
		this.header.breadcrumb = [
			{ label: 'Chantiers', route: '/chantiers' },
			{ label: this.chantier.name, route: `/chantiers/detail/${this.id}` }];
		//		});
	}
	modifier() {
		DialogHelper.openDialog(
			this.dialog,
			chantierFromComponent,
			DialogHelper.SIZE_LARGE,
			{ data: { defaultData: this.chantier, type: IFormType.update } }
		).subscribe(async response => {
			this.chantier = response;
		});
	}

	tabs(e) {
		this.selectedTabs = e.tab.content.viewContainerRef.element.nativeElement.dataset.name;
		if (this.selectedTabs === 'retenuegarantie') {
			this.getRetenueGarantie();
		}
		if (this.selectedTabs === 'recaptulatiffinancier') {
			this.GetRecapitulatifFinancier();
		}
		if (this.selectedTabs === 'documents') {
			this.getNbDocuments();
		}
	}
	removePote() {
		localStorage.removeItem('activetab');
	}

	ngOnDestroy(): void {
		localStorage.removeItem('activetab');
	}


	refresh(event?: any) {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(async params => {
				this.processing = true;
				this.id = params['id'];
				this.chantier = await this.getChantier(params['id']);
				localStorage.setItem('nomChantier', this.chantier.name);

				this.historique = this.chantier.changesHistory;
				this.processing = false;

				resolve();
			});
		});
	}

	getChantier(id): Promise<Workshop> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.Chantier, id).subscribe(chantier => {
				resolve(chantier.value);
			}, err => {
				reject(err);
			});
		});
	}




	getLabelleByStatut(statut): string {
		if (statut === undefined) { return; }
		const statuts = this.statuts.filter(S => S.id === statut)[0];
		return statuts === undefined ? '' : statuts.label;
	}

	/** changer le statut du chantier  */
	changeStatutChantier(changeStatut: StatutChantier) {

		this.translate.get(this.getTransalteLocationRequest(changeStatut)).subscribe(text => {
			swal({
				title: `${text.title} " ${this.chantier.name} "`,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.service.updateAll(ApiUrl.Chantier + '/' + this.chantier.id + '/Update/Status', {
						status: changeStatut
					}).subscribe(res => {
						if (res) {
							swal(text.success, '', 'success');
							this.chantier = res.value
							this.historique = this.chantier.changesHistory;
						} else {
							swal(text.hasBillsNotPaid, '', 'error');
						}
					}, err => {
						swal(text.error, '', 'error');
					});
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}

	getTransalteLocationRequest(statut: StatutChantier): string {
		return `changeStatut.${statut}`;
	}


	getNbDocuments() {
		this.processing = true;
		this.service.getAll(ApiUrl.Chantier + '/' + this.id + '/Documents/Count').subscribe((nbDocuments) => {
			this.nbDocuments = nbDocuments['value'];
			this.processing = false;
		});
	}

	GetRecapitulatifFinancier() {
		localStorage.removeItem('activetab');
		this.processing = true;
		this.service.getAll(ApiUrl.Chantier + '/' + this.chantier.id + '/FinancialSummary').subscribe(res => {
			this.recapitulatifFinancierData = res.value;
		}, (err) => this.translate.get('errors').subscribe(text => {
			toastr.warning(text.serveur, '', {
				positionClass: 'toast-top-center',
				containerId: 'toast-top-center',
			});
		})
			, () => { this.processing = false; }
		);
	}

	getRetenueGarantie(event?) {
		localStorage.removeItem('activetab');
		this.processing = true;
		this.service.getAll(ApiUrl.Invoice + '/Workshop/' + this.chantier.id + '/Holdback').subscribe(res => {
			this.listRetenueGaranties = res.value;
		}, (err) => this.translate.get('errors').subscribe(text => {
			toastr.warning(text.serveur, '', {
				positionClass: 'toast-top-center',
				containerId: 'toast-top-center',
			});
		})
			, () => { this.processing = false; }
		);
	}

	changeStatut(event) {
		const rs = {
			'status': event.StatusRetenueGarantie,
			'warrantyExpirationDate': event.date
		}
		this.service.updateAll(ApiUrl.Invoice + '/' + event.idFacture + '/Update/Holdback', rs).subscribe(res => {
			this.translate.get(this.getTransalteChangeStatutRetenueLocationRequest(event.StatusRetenueGarantie)).subscribe(text => {
				if (res) {
					swal(text.success, '', 'success');
					this.getRetenueGarantie();
				} else {
					swal(text.ImpossibleDeChangerStatut, '', 'error');
				}
			});
		});

	}

	changeDateRetenue({ idFacture: idFature, date: dateRetenue, status: status }) {
		const rs = {
			'status': status,
			'warrantyExpirationDate': dateRetenue
		}
		this.service.updateAll(ApiUrl.Invoice + '/' + idFature + '/Update/Holdback', rs).subscribe(res => {
			this.translate.get('changeDateRetenueGarantie').subscribe(text => {
				if (res) {
					swal(text.success, '', 'success');
					this.getRetenueGarantie();


				} else {
					swal(text.serveur, '', 'error');
				}
			});
		});

	}

	getTransalteChangeStatutRetenueLocationRequest(statut: StatutRetenueGarantie): string {
		return `changeStatutRetenueGarantie.${statut}`;
	}

}
