import { Component, OnInit, Input, OnChanges, AfterViewInit } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import * as Chart from 'chart.js';

@Component({
	selector: 'app-recaptulatif-financier',
	templateUrl: './recaptulatif-financier.component.html',
	styleUrls: ['./recaptulatif-financier.component.scss']
})
export class RecaptulatifFinancierComponent implements OnInit, OnChanges, AfterViewInit {

	// tslint:disable-next-line:no-input-rename
	@Input('recapitulatifFinancierData') data = null;
	pieChartOptions: ChartOptions = {
		responsive: true,
		legend: {
			position: 'top',
		},
		plugins: {
			datalabels: {
				formatter: (value, ctx) => {
					const label = ctx.chart.data.labels[ctx.dataIndex];
					return label;
				},
			},
		}
	};

	pieChartType: ChartType = 'pie';
	pieChartLegend = false;
	pieChartPlugins = [/*pluginDataLabels*/];
	pieChartColors = [
		{
			backgroundColor: ['#81ecec', 'rgb(234, 153, 153)', '#a29bfe'],
		},
	];
	pieChartData = {
		// pieChartData: []
		total_devis: [],
		depensee_Aprevoir: [],
		marge_Previsionnel: [],
		caFacture: [],
		depenseeEngagees: [],
		margeReele: [],
	};
	pieChartLabels = {
		// pieChartData: []
		total_devis: [],
		depensee_Aprevoir: [],
		marge_Previsionnel: [],
		caFacture: [],
		depenseeEngagees: [],
		margeReele: [],
	}

	date = new Date();
	AvancDepense = 0;
	AvancFacture = 0;

	constructor(private translate: TranslateService) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);

	}

	async ngOnChanges() {
		this.pieChartData = {
			total_devis: [],
			depensee_Aprevoir: [],
			marge_Previsionnel: [],
			caFacture: [],
			depenseeEngagees: [],
			margeReele: []
		};
		if (this.data) {

			this.AvancDepense = (this.data.billingTreasury.expenditures.total / this.data.predictions.expenses.total ) * 100;
			this.AvancFacture = (this.data.billingTreasury.invoicesDetails.total / this.data.predictions.quotes.total ) * 100

			const labelsTransaltion = await this.labelsTransaltion();

			this.pieChartLabels.total_devis.push(labelsTransaltion.vente_main_oveure);
			this.pieChartLabels.total_devis.push(labelsTransaltion.vente_materiel);

			this.pieChartData.total_devis.push(this.data.predictions.quotes.workforceSales);
			this.pieChartData.total_devis.push(this.data.predictions.quotes.materialSales);


			this.pieChartLabels.depensee_Aprevoir.push(labelsTransaltion.achat_materiel);
			this.pieChartLabels.depensee_Aprevoir.push(labelsTransaltion.achat_main_oveure);
			this.pieChartLabels.depensee_Aprevoir.push(labelsTransaltion.sous_traitance);


			this.pieChartData.depensee_Aprevoir.push(this.data.predictions.expenses.totalMaterialPurchases);
			this.pieChartData.depensee_Aprevoir.push(this.data.predictions.expenses.totalWorkforcePurchases);
			this.pieChartData.depensee_Aprevoir.push(this.data.predictions.expenses.subContracting);

			this.pieChartLabels.marge_Previsionnel.push(labelsTransaltion.retenueGarantier);
			this.pieChartLabels.marge_Previsionnel.push(labelsTransaltion.margeMateriel);
			this.pieChartLabels.marge_Previsionnel.push(labelsTransaltion.margeMainOeuvre);

			this.pieChartData.marge_Previsionnel.push(this.data.predictions.margin.totalHoldback);
			this.pieChartData.marge_Previsionnel.push(this.data.predictions.margin.materialMargin);
			this.pieChartData.marge_Previsionnel.push(this.data.predictions.margin.workforceMargin);

			this.pieChartLabels.caFacture.push(labelsTransaltion.payes);
			this.pieChartLabels.caFacture.push(labelsTransaltion.enAttentepaiement);

			this.pieChartData.caFacture.push(this.data.billingTreasury.invoicesDetails.totalPaid);
			this.pieChartData.caFacture.push(this.data.billingTreasury.invoicesDetails.totalRestToPay);

			this.pieChartLabels.depenseeEngagees.push(labelsTransaltion.achat_materiel);
			this.pieChartLabels.depenseeEngagees.push(labelsTransaltion.achat_main_oveure);
			this.pieChartLabels.depenseeEngagees.push(labelsTransaltion.sousTraitance);

			this.pieChartData.depenseeEngagees.push(this.data.billingTreasury.expenditures.expenseDetails[0] !== undefined ?
				this.data.billingTreasury.expenditures.expenseDetails[0].totalPaid +
				this.data.billingTreasury.expenditures.expenseDetails[0].restToPay : 0);
			this.pieChartData.depenseeEngagees.push(this.data.billingTreasury.expenditures.operationSheetDetails.total);
			this.pieChartData.depenseeEngagees.push(this.data.billingTreasury.expenditures.expenseDetails[1] !== undefined ?
				this.data.billingTreasury.expenditures.expenseDetails[1].totalPaid +
				this.data.billingTreasury.expenditures.expenseDetails[1].restToPay : 0);

			this.pieChartLabels.margeReele.push(labelsTransaltion.retenueGarantier);
			this.pieChartLabels.margeReele.push(labelsTransaltion.margeMateriel);
			this.pieChartLabels.margeReele.push(labelsTransaltion.margeMainOeuvre);

			this.pieChartData.margeReele.push(this.data.billingTreasury.margin.totalHoldback);
			this.pieChartData.margeReele.push(this.data.billingTreasury.margin.materialMargin);
			this.pieChartData.margeReele.push(this.data.billingTreasury.margin.workforceMargin);

			this.initCharts();
		}
	}

	ngAfterViewInit(): void {
		this.initCharts();
	}

	getStat(per) {
		if (per !== 'NaN' && per !== '-Infinity'  && per !== 'Infinity') { return true; }
		return false;
	}

	getnull(per) {
		if (per !== 'NaN' && per !== '-Infinity'  && per !== 'Infinity') { return per; }
		return 0;
	}

	labelsTransaltion(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get('labels').subscribe(labels => {
				resolve(labels);
			});
		});
	}

	initCharts(): void {
		this.initCACharts();
		this.initDCCharts();
		this.initMCCharts();
	}

	/**
	 * Initializes chiffre d'affaires charts
	 */
	initCACharts(): void {
		const caPrev = document.getElementById('ca_prev') as any;
		const caReel = document.getElementById('ca_reel') as any;

		const caPrevChart = new Chart(caPrev.getContext('2d'), {
			type: 'doughnut',
			data: {
				datasets: [{
					data: this.pieChartData.total_devis,
					backgroundColor: ['#47A2C1', '#F53C56'],
				}],
				labels: this.pieChartLabels.total_devis
			},
			options: {
				legend: { display: false },
				maintainAspectRatio: false
			}
		});

		const caReelChart = new Chart(caReel.getContext('2d'), {
			type: 'doughnut',
			data: {
				datasets: [
					{
						data: [75, 25],
						type: 'doughnut',
						backgroundColor: ['#38b475', '#38b4754a'],
						weight: 0.3
					},
					{
						data: [49, 59],
						backgroundColor: ['#47A2C1', '#F53C56'],
					}
				],
				labels: ['Vente main d\'oeuvre 6h', 'Vente matériel']
			},
			options: {
				legend: { display: false },
				maintainAspectRatio: false
			}
		});
	}

	/**
	 * Initializes dépenses chantier charts
	 */
	initDCCharts(): void {
		const dcPrev = document.getElementById('dc_prev') as any;
		const dcReel = document.getElementById('dc_reel') as any;

		const dcPrevChart = new Chart(dcPrev.getContext('2d'), {
			type: 'doughnut',
			data: {
				datasets: [{
					data: this.pieChartData.depensee_Aprevoir,
					backgroundColor: ['#F53C56', '#47A2C1', '#feb969'],
				}],
				labels: this.pieChartLabels.depensee_Aprevoir
			},
			options: {
				legend: { display: false },
				maintainAspectRatio: false
			}
		});

		const dcReelChart = new Chart(dcReel.getContext('2d'), {
			type: 'doughnut',
			data: {
				datasets: [
					{
						data: [],
						type: 'doughnut',
						backgroundColor: ['#38b475', '#38b4754a'],
						weight: 0.3
					},
					{
						data: this.pieChartData.depenseeEngagees,
						backgroundColor: ['#F53C56', '#47A2C1', '#feb969'],
					}
				],
				labels: this.pieChartLabels.depenseeEngagees
			},
			options: {
				legend: { display: false },
				maintainAspectRatio: false
			}
		});
	}

	/**
	 * Initializes marge chantier charts
	 */
	initMCCharts(): void {
		const mcPrev = document.getElementById('mc_prev') as any;
		const mcReel = document.getElementById('mc_reel') as any;

		const mcPrevChart = new Chart(mcPrev.getContext('2d'), {
			type: 'doughnut',
			data: {
				datasets: [{
					data: this.pieChartData.marge_Previsionnel,
					backgroundColor: ['#F53C56', '#47A2C1', '#feb969'],
				}],
				labels: this.pieChartLabels.marge_Previsionnel,
			},
			options: {
				legend: { display: false },
				maintainAspectRatio: false
			}
		});
		const mcReelChart = new Chart(mcReel.getContext('2d'), {
			type: 'doughnut',
			data: {
				datasets: [
					{
						data: [this.data !== null ? this.data.billingTreasury.margin.total : 0,
						this.getSubract(this.data !== null ? this.data.billingTreasury.margin.total : 0)],
						type: 'doughnut',
						backgroundColor: ['#38b475', '#38b4754a'],
						weight: 0.3
					},
					{
						data: this.pieChartData.margeReele,
						backgroundColor: ['#F53C56', '#47A2C1', '#feb969'],
					}
				],
				labels: this.pieChartLabels.margeReele
			},
			options: {
				legend: { display: false },
				maintainAspectRatio: false
			}
		});
	}

	getSubract(data) {
		return +data > 100 || + data < 100 ? 0 : 100 - parseInt(data);
	}
}
