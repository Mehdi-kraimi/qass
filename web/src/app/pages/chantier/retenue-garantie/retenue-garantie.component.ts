import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ResultatCalculModel } from 'app/Models/Model/ResultatCalculModel';
import { RetenueGarantieModel } from 'app/Models/Model/RetenueGarantieModel';
import { StatutRetenueGarantie } from 'app/Enums/DelaiGaranties.Enum';
import { ColumnType } from 'app/components/form-data/data-table/data-table.component';
import { DataTableRowActions } from 'app/libraries/manage-data-table';
import { Color } from 'app/Enums/color';


declare var jQuery: any;
declare var swal: any;
declare var toastr: any;
@Component({
	selector: 'app-retenue-garantie',
	templateUrl: './retenue-garantie.component.html',
	styleUrls: ['./retenue-garantie.component.scss']
})
export class RetenueGarantieComponent implements OnInit {

	// tslint:disable-next-line:no-output-rename
	@Output('ChangeStatut') ChangeStatut = new EventEmitter();
	// tslint:disable-next-line:no-output-rename
	@Output('ChangeDateRetenue') ChangeDateRetenue = new EventEmitter();
	// tslint:disable-next-line:no-input-rename
	@Input('listRetenueGaranties') listRetenueGaranties: RetenueGarantieModel[] = [];

	statutRetenueGarantie: typeof StatutRetenueGarantie = StatutRetenueGarantie;
	dateEcheanceRetenue = new Date();
	dateLang: any;
	statuts: { id: number, label: string, color: string }[];
	editDate = false
	idFacture = null;
	columns: any[];
	actions: DataTableRowActions[];
	selected: any;
	columnType: typeof ColumnType = ColumnType;

	facture = null;
	headerDate = '';
	modalDateState = false
	selectedStatus = '';
	status = [];
	headerStatut = '';
	modalStatutState = false;
	color: typeof Color = Color;
	constructor(
		private translate: TranslateService
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('statutsRetenue').subscribe((statuts: { id: number, label: string, color: string }[]) => {
			this.statuts = statuts;
		});
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});

		this.columns = [

			{
				name: 'invoiceReference', nameTranslate: 'labels.reference', isOrder: false, type: ColumnType.any,
				appear: true
			},
			{
				name: 'holdback', nameTranslate: 'labels.retenuegarantie', isOrder: true, type: ColumnType.any,
				appear: true
			},
			{
				name: 'amount', nameTranslate: 'labels.montant', isOrder: false, type: ColumnType.Currency,
				appear: true
			},
			{
				name: 'warrantyExpirationDate', nameTranslate: 'labels.dateEcheance', isOrder: false, type: ColumnType.Date,
				appear: true
			},
			{
				name: 'status', nameTranslate: 'labels.statusRetenuegarantie', isOrder: false, type: ColumnType.Status,
				appear: true
			},

		];
	}
	getValue(obj: any, key): any {
		let result: string = '';
		if (Array.isArray(key)) {
			for (const k of key) {
				const value = this.getValue(obj, k);

				if (value) {
					result = value;
				}
			}
		} else {
			if (key && key.indexOf('.') >= 0) {
				const keys = (key as string).split('.');
				let val = obj;

				keys.forEach(k => {
					if (k.includes('[') && k.includes(']')) {
						const index = parseInt(k.substring(k.indexOf('[') + 1, k.indexOf(']')), 10);
						const baseKey = k.substring(0, k.indexOf('['));

						val = val ? val[baseKey][index] : '';
					} else {
						val = val ? val[k] : '';
					}
				});

				result = val;
			} else {

				result = obj[(key as string)];
			}
		}


		const header = this.columns.find(e => e.name === key);
		if (header) {
			// translations
			if (header['keyTranslate']) {
				return this.translate.instant(`${(header['keyTranslate'] as string).replace('{val}', result)}`);
			}

		}

		return result;
	}

	onMenuOpened(element: any): void {
		this.selected = element;
		this.actions = this.getActions(this.selected);
	}

	public getActions(ele): DataTableRowActions[] {

		return [
			{
				id: 2,
				icon: './assets/app/images/Icons/edit.svg',
				label: 'labels.editDate',
				color: 'warn',
				action: (element) => {
					this.editDateEcheance(element);
				},
				//appear: (ele) => ele !== undefined && this.conditionRetenu(ele.invoiceId)
				appear: () => true
			},
			{
				id: 3,
				icon: './assets/app/images/Icons/history.svg',
				label: 'labels.ChangementStatut',
				color: 'primary',
				action: (element) => {
					this.openchangerStatut(element);
				},
				appear: () => true

			},
		];


	}

	openchangerStatut(facture) {
		if (facture.status === StatutRetenueGarantie.nonrecuperer) {
			this.status = [StatutRetenueGarantie.nonrecuperer, StatutRetenueGarantie.encours, StatutRetenueGarantie.enretard, StatutRetenueGarantie.recuperer];
		}
		if (facture.status === StatutRetenueGarantie.encours) {
			this.status = [StatutRetenueGarantie.encours, StatutRetenueGarantie.nonrecuperer, StatutRetenueGarantie.enretard, StatutRetenueGarantie.recuperer];
		}
		if (facture.status === StatutRetenueGarantie.enretard) {
			this.status = [StatutRetenueGarantie.enretard, StatutRetenueGarantie.nonrecuperer, StatutRetenueGarantie.encours, StatutRetenueGarantie.recuperer];
		}
		if (facture.status === StatutRetenueGarantie.recuperer) {
			this.status = [StatutRetenueGarantie.recuperer, StatutRetenueGarantie.nonrecuperer, StatutRetenueGarantie.encours, StatutRetenueGarantie.enretard];
		}
		this.facture = facture;
		if (this.status.length > 0) {
			this.selectedStatus = this.facture.status;
			this.headerStatut = 'Modifier Statut';
			this.modalStatutState = true;
		}


	}

	onStatusChanged(e: any): void {
		this.selectedStatus = e;
	}
	ngOnChanges() {
		this.listRetenueGaranties = this.listRetenueGaranties.filter(x => x.holdback !== 0);
	}

	changerStatut(idFature, statut: StatutRetenueGarantie, dateRetenue) {
		this.translate.get(this.getTransalteLocationRequest(statut)).subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.ChangeStatut.emit({ idFacture: idFature, date: dateRetenue, StatusRetenueGarantie: statut });

				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}
	/**
	 * @summary fonction générique s'utiliser dans la fonction du changement du statut d'un chantier
	 * @todo déterminer la requête pour récupérer la traduction à partirdu ficher json de traduction
	 * @param statut le statut du chantier qui nous voulons récupérer leur traduction
	 */
	getTransalteLocationRequest(statut: StatutRetenueGarantie): string {
		return `changeStatutRetenueGarantie.${statut}`;
	}


	getStatutRetenue(dateEcheance): StatutRetenueGarantie {
		if (dateEcheance != null) {
			const result = AppSettings.compareDate(dateEcheance, new Date());
			if (result) {
				return StatutRetenueGarantie.encours;

			} else {
				return StatutRetenueGarantie.enretard;

			}
		} else {
			return StatutRetenueGarantie.encours;
		}

	}



	saveDateEcheance(dateRetenue: Date, idFacture: number, status: string) {
		this.translate.get('changeDateRetenueGarantie').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.ChangeDateRetenue.emit({ idFacture: idFacture, date: dateRetenue, status: status })
					this.editDate = false;
					this.idFacture = null;
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}

	annuler() {
		this.editDate = false;
		this.idFacture = null;
	}
	conditionRetenu(idFactureRetenue) {
		if (this.idFacture == null) { return true }
		if (this.editDate && this.idFacture === idFactureRetenue) { return false }
		if (this.editDate && this.idFacture !== idFactureRetenue) { return true }
	}

	editDateEcheance(facture) {
		this.headerDate = 'Modifier date écheance';
		this.dateEcheanceRetenue = facture.warrantyExpirationDate;
		this.modalDateState = true;
		this.facture = facture;

	}
	editDateEch() {
		this.saveDateEcheance(this.dateEcheanceRetenue, this.facture.invoiceId, this.facture.status);
		this.modalDateState = false;
		this.dateEcheanceRetenue = new Date();
	}



	editStatut() {
		this.changerStatut(this.facture.invoiceId, this.selectedStatus as StatutRetenueGarantie, this.facture.warrantyExpirationDate)
		this.modalStatutState = false;
		this.selectedStatus = null;
	}

	getColor(status) {
		return this.color[status]
	};



}
