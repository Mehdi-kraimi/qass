import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IFormType } from '../../../../Enums/IFormType.enum';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { DocumentAttacher } from 'app/Models/Entities/Commun/DocumentAttacher';
import { RubriqueEnum } from 'app/Enums/RubriqueEnum.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { LocalElements } from 'app/shared/utils/local-elements';
import { ColumnType } from 'app/components/form-data/data-table/data-table.component';
import { DataTableRowActions } from 'app/libraries/manage-data-table';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { FormRebriqueComponent } from '../form-rebrique/form-rebrique.component';
import { EditRubriqueFormComponent } from 'app/components/edit-rubrique-form/edit-rubrique-form.component';
import { Rubrique } from 'app/Models/Entities/Documents/Rubrique';
import { DocumentRubricType } from 'app/Enums/DocumentRubricType.Enum';
import { textareaAnimations } from 'app/shared/animations/textarea.animations';
import { IFilterOption, SortDirection } from 'app/Models/Model/filter-option';
import { StringHelper } from 'app/libraries/string';
import { PagedResult } from 'app/Models/Model/ListModel';
import { TypeRubrique } from 'app/Enums/TypeRubrique.Enum';

declare var toastr: any;
declare var swal: any;

@Component({
	selector: 'app-rebrique',
	templateUrl: './rebrique-list.component.html',
	styleUrls: ['./rebrique-list.component.scss']
})
export class RebriqueComponent implements OnInit {
	// tslint:disable-next-line:no-output-rename
	@Output('OnRefresh') refresh = new EventEmitter();
	// tslint:disable-next-line:no-input-rename
	@Input('nbDocuments') nbDocuments;
	formType: typeof IFormType = IFormType;
	formConfig = {
		type: null,
		defaultData: null
	}
	typeForSearch = null;
	searchQuery = '';
	chantier: Workshop;
	idChantier = 0;
	typesDocuments = null;
	documentAttacher: DocumentAttacher[] = [];
	rubriqueEnum: typeof RubriqueEnum = RubriqueEnum;
	RubriqueInfo: {
		id: number,
		nbrDoc: number,
		lastEditDate: Date
	}[] = [];
	idrubrique;
	emitter: any = {};
	Listrubric = [];
	rubriqueCount = null;;
	Columns: any[];
	columnType: typeof ColumnType = ColumnType;
	actions: DataTableRowActions[];
	selected: any;
	/**
 * request data
 */
	items: any[];
	dataitems: any;
	rowCount: number;
	checkedColumns: boolean[] = [];
	pageSizeOptions = AppSettings.PAGE_SIZE_OPTIONS;

	state: IFilterOption = {
		Page: 1,
		PageSize: AppSettings.DEFAULT_PAGE_SIZE,
		OrderBy: 'id',
		SortDirection: SortDirection.Descending,
		SearchQuery: ''
	};
	filterOptions: IFilterOption;
	data: PagedResult;
	search = ''
	page = 1;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private route: ActivatedRoute,
		private translate: TranslateService,
		private router: Router,
		private dialog: MatDialog,

	) { }

	async ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);

		this.route.params.subscribe(async params => {
			this.idChantier = params['id'];
		});
		this.initData();
		this.getDocumentionTypes();

		this.chantier = await this.getDocumention(this.idChantier);
		this.Columns = [

			{
				name: 'value', nameTranslate: 'labels.rubrique', isOrder: false, type: ColumnType.any, list: [
					{ name: 'associatedDocumentsCount', nameTranslate: null, isOrder: false, type: ColumnType.Number }
				],
				appear: true
			},


			{
				name: 'totalDocuments', nameTranslate: 'labels.nbrDocument', isOrder: true, type: ColumnType.Number,
				appear: true
			},
			{
				name: 'lastModifiedOn', nameTranslate: 'labels.derniermodificationt', isOrder: false, type: ColumnType.DateTime,
				appear: true
			},

		];
	}


	//#region Pagination
	initState() {
		this.state = {
			Page: 1,
			PageSize: AppSettings.DEFAULT_PAGE_SIZE,
			OrderBy: 'id',
			SortDirection: SortDirection.Descending,
			SearchQuery: ''
		}
	}
	chagePageNumber(event: any) {
		this.state.Page = event;
		this.saveState();
	}
	changePageSize(event: any) {
		this.state.PageSize = event;
		this.saveState();
	}
	sortChange(direction, event: any) {
		if (!StringHelper.isEmptyOrNull(event)) {
			this.state.OrderBy = event;
			this.state.SortDirection = direction;
			this.saveState();
		}
	}
	saveState() {
		localStorage.setItem(`state_rebrique_rub`, JSON.stringify(this.state));
		this.emitChange();
	}
	emitChange() {
		this.changeFiltersEvent(this.state);
	}


	changeFiltersEvent(dataTableOutput: IFilterOption) {
		this.filterOptions = { ...this.filterOptions, ...dataTableOutput };
		this.getData()
			.then(() => {
				//	this.processing = false;
			});
	}
	//#endregion


	//#region create && update rubrique
	formRubrique(type, rubrique: Rubrique) {

		const dialogLotConfig = new MatDialogConfig();
		// dialogLotConfig.data = null;
		dialogLotConfig.width = '370px';
		dialogLotConfig.height = '240px';
		dialogLotConfig.data = { type: type, data: rubrique };
		const dialogRef = this.dialog.open(EditRubriqueFormComponent, dialogLotConfig);

		dialogRef.afterClosed().subscribe((data) => {
			// && Array.from(data).length
			if (typeof (data) === 'object') {
				if (rubrique == null) {
					// tslint:disable-next-line: prefer-const
					let newrubrique: Rubrique = {
						id: null,
						value: data.Value,
						type: DocumentRubricType.Custom,
						totalDocuments: 0,
						workshopId: this.idChantier,
						documents: null,
						workshop: null,
					}
					const text = this.translate.instant('toast.add-sucsess')
					this.createRebrique(newrubrique, text)

				} else {
					rubrique.value = data.Value;
					const text = this.translate.instant('toast.update-sucsess')
					this.createRebrique(rubrique, text)
				}

			}

		});
	}

	createRebrique(rubrique, text) {
		this.service.updateAll(ApiUrl.configRubric, rubrique).subscribe(res => {
			toastr.success('', text, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			this.initData();
		}
			, err => {
				console.log('err', err)
				const text = this.translate.instant('errors.serveur')
				toastr.warning('', text, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
	}
	//#endregion

	//#region menu action
	onMenuOpened(element: any): void {
		this.selected = element;
		this.actions = this.getActions(this.selected);
	}

	public getActions(ele): DataTableRowActions[] {

		return [
			{
				id: 1,
				icon: './assets/app/images/Icons/eye.svg',
				label: 'LABELS.SHOW',
				default: true,
				color: 'accent',
				action: (element: any) => {
					//	this.navigateToDetailFacture(element.invoiceId);
					this.open(element)
				},
				appear: () => true

			},
			{
				id: 1,
				icon: './assets/app/images/Icons/edit.svg',
				label: 'LABELS.EDIT',
				default: true,
				color: 'accent',
				action: (element: any) => {
					//	this.navigateToDetailFacture(element.invoiceId);
					this.formRubrique('edit', element)
				},
				appear: () => true

			},
			{
				id: 2,
				icon: './assets/app/images/Icons/document.svg',
				label: 'labels.attacherPieceJointe',
				color: 'warn',
				action: (element) => {
					//	this.setformConfig(element.id, null, this.formType.add);
					this.openFormRebrique(element);
				},
				appear: () => true
			},
			{
				id: 3,
				icon: './assets/app/images/Icons/plus.svg',
				label: this.translate.instant('addDoc.' + ele.type),
				color: 'primary',
				action: (element) => {
					this.addDocument(element.type)
				},
				appear: (ele) => ele !== undefined && this.candisplayNumberDocu(ele)
			},

			{
				id: 1,
				icon: './assets/app/images/Icons/trash-2.svg',
				label: 'LABELS.DELETE',
				default: true,
				color: 'accent',
				action: (element: any) => {
					this.deleteRubrique(element.id)
				},
				// tslint:disable-next-line: no-shadowed-variable
				appear: (ele) => ele !== undefined && this.candelete(ele)

			},
		];


	}
	//#endregion

	//#region condition
	candelete(rubrique: Rubrique) {
		const res = rubrique.type === DocumentRubricType.Custom ? true : false;
		return res;
	}

	candisplayNumberDocu(rubrique: Rubrique) {
		const res = rubrique.type === DocumentRubricType.Custom ? false : true;
		return res;
	}
	//#endregion

	//#region delete rubrique
	deleteRubrique(id: number): void {
		this.translate.get('list.delete').subscribe(text => {
			swal({
				title: text.title,
				text: text.question,
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: true
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: true
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.envoyerDemandeSuppressionAuServeur(id, text);
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	envoyerDemandeSuppressionAuServeur(id, text: any): void {
		const url = ApiUrl.Chantier + '/' + this.idChantier + '/Rubric/' + id
		this.service.deleteAll(url).subscribe(res => {
			if (res) {
				swal(text.success, '', 'success');
				this.initData();
			} else {

				swal(text.ImpossibleDeSuppression, '', 'error');
				// swal(text.error, "", "error");
			}
		});
	}
	//#endregion

	//#region document associe
	openFormRebrique(element) {
		this.idrubrique = element.id;
		const dialogVisiteConfig = new MatDialogConfig();
		dialogVisiteConfig.width = '850px';
		dialogVisiteConfig.height = '700px';
		dialogVisiteConfig.data = { defaultData: null, type: this.formType.add, readOnly: false, typesDocuments: null };
		const dialogRef = this.dialog.open(FormRebriqueComponent, dialogVisiteConfig);

		dialogRef.afterClosed().subscribe((data) => {
			if (data) {
				// this.envoyer(data.emailTo, data.object, data.contents)
				this.OnSave(data, this.formType.add)
			}
		});
	}

	async addDocumentAttacher(documentation: DocumentAttacher) {
		documentation.rubricId = this.idrubrique;
		this.service.create(ApiUrl.Chantier + '/' + this.idChantier + '/Documents', documentation).subscribe(async res => {
			if (res) {
				//	this.getRubriqueInfo();
				this.initData();
				this.translate.get('adddoc').subscribe(text => {
					toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			}
			this.refresh.emit('1');

		}, err => {
			console.log(err)
			this.translate.get('adddoc').subscribe(text => {
				toastr.warning(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		});
	}

	async update(documentation) {
		this.service.create(ApiUrl.Chantier + '/' + this.idChantier + '/Update/Documents/' + this.idrubrique,
			documentation).subscribe(async res => {
				if (res) {
					//	this.getRubriqueInfo();
					this.initData();
					this.translate.get('updatedoc').subscribe(text => {
						toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
					});
				}
				this.refresh.emit('1');
			}, err => {
				console.log(err)
				this.translate.get('updatedoc').subscribe(text => {
					toastr.warning(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			});
	}

	async OnSave(data, type) {
		if (type === IFormType.add) {
			await this.addDocumentAttacher(data)
		}
		if (type === IFormType.update) {
			this.update(data);
		}
	}

	deleteFile(name: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.delete(ApiUrl.File, name).subscribe(res => {
				resolve(res)
			}, err => {
				reject(err)
			})
		})
	}
	//#endregion

	//#region list
	getValue(obj: any, key): any {
		let result: string = '';
		if (Array.isArray(key)) {
			for (const k of key) {
				const value = this.getValue(obj, k);

				if (value) {
					result = value;
				}
			}
		} else {
			if (key && key.indexOf('.') >= 0) {
				const keys = (key as string).split('.');
				let val = obj;

				keys.forEach(k => {
					if (k.includes('[') && k.includes(']')) {
						const index = parseInt(k.substring(k.indexOf('[') + 1, k.indexOf(']')), 10);
						const baseKey = k.substring(0, k.indexOf('['));

						val = val ? val[baseKey][index] : '';
					} else {
						val = val ? val[k] : '';
					}
				});

				result = val;
			} else {

				result = obj[(key as string)];
			}
		}


		const header = this.Columns.find(e => e.name === key);
		if (header) {
			// translations
			if (header['keyTranslate']) {
				return this.translate.instant(`${(header['keyTranslate'] as string).replace('{val}', result)}`);
			}

		}

		return result;
	}
	//#endregion


	getDocTYPE(Type) {
		switch (Type) {
			case DocumentRubricType.Billing: return ApiUrl.Invoice;
			case DocumentRubricType.Orders: return ApiUrl.BonCommandeF;
			case DocumentRubricType.Quote: return ApiUrl.Devis;
				break;
		}
	}


	getDocumention(id: number): Promise<Workshop> {

		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.Chantier, id).subscribe(chantier => {
				resolve(chantier.value);
			}, err => {
				reject(err);
			});
		});
	}


	public initData() {
		this.initFilters();
		this.getData();
	}
	initFilters() {
		this.data = null;
		// ManageDataTable.currentPage = 0;

		this.filterOptions = {
			Page: 1,
			PageSize: AppSettings.DEFAULT_PAGE_SIZE,
			SearchQuery: '',
			SortDirection: SortDirection.Descending,
			OrderBy: 'id',
		};
	}
	getData(cache: boolean = true): Promise<any> {
		return new Promise((resolve, reject) => {
			const filters: IFilterOption = Object.assign(this.filterOptions);
			this.service
				.getAllPagination(ApiUrl.Chantier + '/' + this.idChantier + '/Rubric', filters)
				.subscribe((data) => {
					if (data.isSuccess) {
						this.data = data;
						this.dataitems = data;
						if (data == null) {
							this.items = [];
							this.rowCount = 0;
						} else {
							this.items = data.value;
							this.rowCount = data.rowsCount;
							//	this.processing = false;
						}
						resolve(data);
					}
				}, (err: any) => {
					reject(err);
				});


		});
	}

	async addDocument(type) {

		this.idChantier = this.route.snapshot.params.id as number;
		let doc = '';
		if (type === TypeRubrique.Devis) {
			doc = 'devis'
		};
		if (type === TypeRubrique.Facturation) {
			doc = 'facture'
		};
		if (type === TypeRubrique.Commandes) {
			doc = 'commandes'
		};
		localStorage.setItem(LocalElements.docType, this.getDocTYPE(type));
		const url = `/chantiers/${this.idChantier}/documents/${type}/create/${doc}`;
		this.router.navigate([url]);
	}


	open(doc) {
		this.route.params.subscribe(params => {
			this.router.navigate([`chantiers/${params['id']}/documents/${doc.value}/${doc.type}`]);
		});
	}

	getDocumentionTypes() {
		if (this.typesDocuments == null) {
			this.service.getAll(ApiUrl.configTypeChantier).subscribe(data => {
				this.typesDocuments = data.value;
			});
		}
	}

}
