import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { HeaderService } from 'app/services/header/header.service';
import { ManageDataTable } from 'app/libraries/manage-data-table';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { LocalElements } from 'app/shared/utils/local-elements';
import { TypeRubrique } from 'app/Enums/TypeRubrique.Enum';
import { TranslateConfiguration } from 'app/libraries/translation';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'document-container',
	templateUrl: './rebrique-container.component.html',
	styleUrls: ['./rebrique-container.component.scss'],
})
export class RebriqueContainerComponent implements OnInit {

	moduleName: string = null;
	idChantier: number = null;
	moduleNameFromTransaltion;
	chantier: Workshop = null;
	docType = '';
	typeModule
	typeRubrique: typeof TypeRubrique = TypeRubrique;
	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,

		private route: ActivatedRoute,
		private router: Router,
		private _location: Location,
		private translate: TranslateService,
		private header: HeaderService,
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
	}

	async ngOnInit() {
		this.docType = localStorage.getItem(LocalElements.docType);
		ManageDataTable.docType = this.docType;
		this.prepareBreadcrumb();
		this.typeModule = await this.getParmsFromUrl('module') as string;
		this.moduleName = await this.getParmsFromUrl('name') as string;
		this.idChantier = await this.getParmsFromUrl('id') as number;
		localStorage.setItem('moduleName', this.moduleName);



	}

	prepareBreadcrumb(): void {
		this.translate.get(`doctype.`).toPromise().then((translation: string) => {
			const chantierName = localStorage.getItem('nomChantier');
			this.header.breadcrumb = [
				{ label: 'Chantiers', route: `/chantiers` },
				{ label: chantierName, route: `/chantiers/detail/${this.idChantier}` },
				{ label: this.moduleName },
			];
		});

	}
	getChantier(id): Promise<Workshop> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.Chantier, id).subscribe(chantier => {
				resolve(chantier.value);
			}, err => {
				reject(err);
			});
		});
	}
	close() {
		this.navigateToChantier();
		localStorage.setItem('activetab', 'potes');
	}


	/**
	* return the type of the document
	*/
	getParmsFromUrl(key: string): Promise<string | number> {
		return new Promise((resolve, reject) => {
			this.route.params.subscribe(params => resolve(params[key]))
		});
	}

	getTransaltion(key: string) {
		return new Promise((reslove, reject) => {
			this.translate.get(key).subscribe(transaltion => {
				reslove(transaltion);
			});
		});
	}
	/**
	 * back to list chantiers
	*/
	navigateToListChantiers(): void {
		const url = `/chantiers`;
		this.router.navigate([url]);
	}
	/**
	 * back to chantier
	 */
	navigateToChantier(): void {
		const url = `/chantiers/detail/${this.idChantier}`;
		this.router.navigate([url]);
	}

}
