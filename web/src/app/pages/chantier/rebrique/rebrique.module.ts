import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { RebriqueComponent } from './rubrique-list/rebrique-list.component';
import { DocumentAttacherComponent } from './rebrique-document/rebrique-document.component';
import { RebriqueContainerComponent } from './rebrique-container/rebrique-container.component';
import { ChantierRoutingModule } from '../chantier-routing.module';
import { CommonModules } from 'app/common/common.module';
import { IndexListGlobalComponent } from 'app/components/form-data/index-list/index.component';
import { ListIndexModule } from 'app/components/form-data/index-list/index.module';
import { DocumentsModule } from 'app/pages/documents/documents.module';
import { MatDatepickerModule, MatMenuModule, MatDialogModule, MatTableModule, MatSortModule, MatCheckboxModule, MatPaginatorModule, MatTooltipModule, MatIconModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatNativeDateModule } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { FormRebriqueComponent } from './form-rebrique/form-rebrique.component';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';

export function HttpLoaderFactory(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/prestation/', suffix: '.json' }
	]);
}



@NgModule({
	providers: [
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		DataTablesModule,
		NgbTooltipModule,
		NgSelectModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			},
			isolate: true
		}),
		ChantierRoutingModule,
		CommonModules,
		DocumentsModule,
		ListIndexModule,
		MatDatepickerModule,
		MatDialogModule,
		MatTableModule,
		OverlayModule,
		MatSortModule,
		NgSelectModule,
		CalendarModule,
		MatCheckboxModule,
		NgbTooltipModule,
		MatPaginatorModule,
		InfiniteScrollModule,
		MatTooltipModule,
		MatIconModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatMenuModule,
		ShowHidePasswordModule,
		MatDatepickerModule,
		MatNativeDateModule,

	],
	exports: [
		IndexListGlobalComponent,
		RebriqueComponent,
		DocumentAttacherComponent,
		RebriqueContainerComponent
	],
	declarations: [
		RebriqueComponent,
		DocumentAttacherComponent,
		RebriqueContainerComponent,
		FormRebriqueComponent
	],
	entryComponents: [IndexListGlobalComponent, FormRebriqueComponent]

})
export class RebriqueModule { }
