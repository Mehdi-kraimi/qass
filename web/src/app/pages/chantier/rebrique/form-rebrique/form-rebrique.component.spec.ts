import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRebriqueComponent } from './form-rebrique.component';

describe('FormRebriqueComponent', () => {
  let component: FormRebriqueComponent;
  let fixture: ComponentFixture<FormRebriqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormRebriqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormRebriqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
