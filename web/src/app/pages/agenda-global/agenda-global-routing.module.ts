import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgendaGlobalComponent } from './agenda-global/agenda-global.component';
import { EditFicheInterventionComponent } from '../ficheInterv/details-ficheIntervention/edit-ficheIntervention/edit-ficheIntervention.component';

const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				component: AgendaGlobalComponent,
			},
		]
	},
	{
		path: 'detail/:id',
		loadChildren: 'app/pages/ficheInterv/details-ficheIntervention/details-ficheIntervention.module#DetailsFicheInterventionModule',
		data: {
			isAgendaGlobale: 'isAgendaGlobale'
		}
	},
	{
		path: 'edit/:id',
		component: EditFicheInterventionComponent,
		data: {
			isAgendaGlobale: 'isAgendaGlobale'
		}
	},
]
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AgendaGlobalRoutingModule { }
