import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { AgendaGlobalComponent } from './agenda-global/agenda-global.component';
import { ScheduleModule, DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService, RecurrenceEditorAllModule, ResizeService, DragAndDropService } from '@syncfusion/ej2-angular-schedule';
import { TreeViewModule } from '@syncfusion/ej2-angular-navigations';
import { DropDownListAllModule, MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';
import { MaskedTextBoxModule, UploaderAllModule } from '@syncfusion/ej2-angular-inputs';
import { ToolbarAllModule, ContextMenuAllModule } from '@syncfusion/ej2-angular-navigations';
import { ButtonAllModule } from '@syncfusion/ej2-angular-buttons';
import { CheckBoxAllModule } from '@syncfusion/ej2-angular-buttons';
import { DatePickerAllModule, TimePickerAllModule, DateTimePickerAllModule } from '@syncfusion/ej2-angular-calendars';
import { NumericTextBoxAllModule } from '@syncfusion/ej2-angular-inputs';
import { ScheduleAllModule } from '@syncfusion/ej2-angular-schedule';
import { HttpModule } from '@angular/http';
import { AgendaGlobalRoutingModule } from './agenda-global-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonModules } from 'app/common/common.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { FicheInterventionModule } from '../ficheInterv/ficheIntervention.module';
import { DetailsFicheInterventionModule } from '../ficheInterv/details-ficheIntervention/details-ficheIntervention.module';
import { MatCheckboxModule, MatDatepickerModule, MatIconModule, MatOptionModule, MatSelectModule, MAT_DATE_LOCALE } from '@angular/material';
import localeFr from '@angular/common/locales/fr';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';

registerLocaleData(localeFr, 'fr-FR');


export function MultiLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/status/', suffix: '.json' },
    { prefix: './assets/i18n/agendaglobal/', suffix: '.json' }
  ]);
}



@NgModule({
  declarations: [AgendaGlobalComponent],
  imports: [
    CommonModule,
    AgendaGlobalRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: MultiLoader,
        deps: [HttpClient]
      },
      isolate: true
    }),
    InputTextareaModule,
    ScheduleModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    MatOptionModule,
    MatIconModule,
    RecurrenceEditorAllModule,
    FicheInterventionModule,
    HttpModule,
    ScheduleAllModule,
    RecurrenceEditorAllModule,
    MatCheckboxModule,
    NumericTextBoxAllModule,
    DatePickerAllModule,
    TimePickerAllModule,
    DateTimePickerAllModule,
    MatDatepickerModule,
    CheckBoxAllModule,
    ToolbarAllModule,
    DropDownListAllModule,
    ContextMenuAllModule,
    MaskedTextBoxModule,
    UploaderAllModule,
    MultiSelectAllModule,
    TreeViewModule,
    ButtonAllModule,
    NgSelectModule,
    CommonModules,
    NgbTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    DetailsFicheInterventionModule,
  ],
  providers: [
    DayService,
    WeekService,
    WorkWeekService,
    MonthService,
    AgendaService,
    MonthAgendaService,
    TimelineViewsService,
    TimelineMonthService,
    TimelineViewsService,
    TimelineMonthService,
    ResizeService,
    DragAndDropService,
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    { provide: LOCALE_ID, useValue: 'fr-FR' }
  ],
})
export class AgendaGlobalModule { }
