import { Component, OnInit, ViewChild, ElementRef, Input, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { DashboardAchatModel, FournisseurDetail, ReparationAchatParFournisseur, DetailArticle } from 'app/Models/Entities/Dashboard/dashboardAchat';
import * as Chart from 'chart.js';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Supplier } from 'app/Models/Entities/Contacts/Supplier';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Product } from 'app/Models/Entities/Biobiotheque/Product';
declare var toastr: any;

@Component({
	selector: 'app-dashboard-achat',
	templateUrl: './dashboard-achat.component.html',
	styleUrls: ['./dashboard-achat.component.scss']
})
export class DashboardAchatComponent implements OnInit {

	// tslint:disable-next-line:no-input-rename
	@Input('dataAchat') dataAchat = false;

	dashbordAchat: DashboardAchatModel = null;
	classementCategories: { labelName: string, total: number }[] = [];
	top10fournisseurCa: FournisseurDetail[] = [];
	idProduit: [] = [];
	idFournisseur: [] = [];
	idArticle = null;
	produits = null;
	repartitionAchatParFournisseur: ReparationAchatParFournisseur = null;
	detailArticleReparation: DetailArticle
	fournisseurList: FournisseurDetail[];
	top10articleQte: DetailArticle[];
	top10articlePrix: DetailArticle[];
	filterlistProduit = [];
	listProduits: Product[] = null;
	chart: any;
	periode = PeriodType.None
	periodeEnum: typeof PeriodType = PeriodType
	dateMinimal;
	dateMaximal;
	dateLang;
	chartOptions = {
		tooltips: {
			callbacks: {
				label: (tooltipItem, data) => {

					// get the concerned label
					const label = data.labels[tooltipItem.index];

					// get the concerned dataset
					const dataset = data.datasets[tooltipItem.datasetIndex];

					// calculate the total of this data set
					const total = dataset.data.reduce((previousValue, currentValue, _currentIndex, _array) => previousValue + currentValue, 0);

					// get the current items value
					const currentValueItem = dataset.data[tooltipItem.index];

					// calculate the percentage based on the total and current item, also this does a rough rounding to give a whole number
					const percentage = Math.floor(((currentValueItem / total) * 100) + 0.5);

					return `${label} : ${percentage} %`;
				}
			}
		}
	}

	filterlistFournisseur = [];
	listFournisseurs: Supplier[] = null;
	@ViewChild('selecteFournisseurs') selecteFournisseurs: NgSelectComponent;

	@ViewChild('selecteProduits') selecteProduits: NgSelectComponent;
	@ViewChild('pieCanvas') pieCanvas: ElementRef;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	ngOnInit() {
		this.dashboardAchat();
	}

	filter() {
		this.dashboardAchat();
	}

	async dashboardAchat() {
		this.dateMinimal = this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null;
		this.dateMaximal = this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;

		const res = {
			prodcutId: this.idProduit,
			purchaseAnalyticsFor: this.idArticle,
			externalPartnerId: this.idFournisseur,
			dateStart: this.dateMinimal,
			dateEnd: this.dateMaximal,
			periodType: this.periode,
		}
		this.service.create(ApiUrl.analytic + '/Purchase', res).subscribe(result => {
			this.dashbordAchat = result.value;
			this.top10fournisseurCa = this.dashbordAchat.topSuppliers;
			this.top10articlePrix = this.dashbordAchat.topProductsBySupplierPrice;
			this.top10articleQte = this.dashbordAchat.topProductsByQuantity;
			if (this.dashbordAchat.selectedProductPurchaseDetails != null) {
				this.fournisseurList = this.dashbordAchat.selectedProductPurchaseDetails;
				let sum = 0
				this.fournisseurList.forEach(element => {
					sum += element['price'] * element['quantity'];
				});
				try {
					this.calculations(this.fournisseurList, sum).then((val) => {
						this.classementCategories = val as any;
						setTimeout(() => {
							this.buildChart();
						}, 0);
					});
				} finally {
					// this.loader.hide();
				}
			}
			this.buildChart();

		});
	}


	async calculations(ListFournisseur, sum) {
		return new Promise(async (resolve) => {
			const classementCategories = [];

			ListFournisseur.forEach(element => {
				const pourcentage = (((element.price * element.quantity) / sum) * 100);
				classementCategories.push({ labelName: element.name, total: pourcentage });

			});
			resolve(classementCategories.sort((a, b) => a.total > b.total ? -1 : 1));
		});
	}



	buildChart() {
		if (this.pieCanvas !== undefined) {
			if (this.chart) {
				this.chart.destroy();
			}
			this.chart = new Chart(
				this.pieCanvas.nativeElement, {
				type: 'pie',
				data: {
					labels: this.classementCategories.map(e => e.labelName),
					datasets: [{
						backgroundColor: [
							'#81ecec',
							'#fd79a8',
							'#ff7675',
							'#fab1a0',
							'#55efc4',
							'#a29bfe',
							'#74b9ff',
							'#ffeaa7',
							'#00cec9'
						],
						data: this.classementCategories.map(e => e.total)
					}]
				},
				options: this.chartOptions
			});
		}
	}


	getFournisseurs() {

		if (this.listFournisseurs == null) {
			this.service.getAll(ApiUrl.Fournisseur).subscribe(res => {
				this.listFournisseurs = res.value;
				this.filterlistFournisseur = this.formaterFournisseur(this.listFournisseurs);
			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteFournisseurs.isOpen = true;
			});
		}
	}


	formaterFournisseur(list: Supplier[]) {
		const listClient = [];
		list.forEach((supplier, index) => {
			listClient.push({ id: supplier.id, name: supplier.firstName, disabled: false });
		});
		return listClient;
	}


	getProduits() {

		if (this.listProduits == null) {
			this.service.getAll(ApiUrl.Produit).subscribe(res => {
				this.listProduits = res.value;
				this.filterlistProduit = this.formaterProduit(this.listProduits);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteProduits.isOpen = true;
			});
		}
	}

	formaterProduit(list: Product[]) {
		const listProduits = [];
		list.forEach((prouit, index) => {
			listProduits.push({ id: prouit.id, name: prouit.name, disabled: false });
		});
		return listProduits;
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}
}

