import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAchatComponent } from './dashboard-achat.component';

describe('DashboardAchatComponent', () => {
  let component: DashboardAchatComponent;
  let fixture: ComponentFixture<DashboardAchatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAchatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAchatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
