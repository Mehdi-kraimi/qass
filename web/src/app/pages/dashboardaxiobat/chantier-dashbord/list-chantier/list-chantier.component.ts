import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ChantierList } from 'app/Models/Entities/Dashboard/dashbordChantier';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';

@Component({
	selector: 'app-list-chantier',
	templateUrl: './list-chantier.component.html',
	styleUrls: ['./list-chantier.component.scss']
})

export class ListChantierComponent implements OnInit {
	chantiers: ChantierList[] = [];
	constructor(
		private translate: TranslateService,
		public dialogRef: MatDialogRef<ListChantierComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { listes: ChantierList[] }
	) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnInit() { this.chantiers = this.data.listes; }
	fermer() { this.dialogRef.close(); }
}
