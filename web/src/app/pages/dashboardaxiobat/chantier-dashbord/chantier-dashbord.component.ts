import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ChantierDetail, DashboardChantier, ChantierParStatus, FicheInterventionParStatus, ChantierList } from 'app/Models/Entities/Dashboard/dashbordChantier';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ListChantierComponent } from './list-chantier/list-chantier.component';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
declare var toastr: any;
@Component({
	selector: 'app-chantier-dashbord',
	templateUrl: './chantier-dashbord.component.html',
	styleUrls: ['./chantier-dashbord.component.scss']
})
export class ChantierDashbordComponent implements OnInit {

	resdashbordChantier: DashboardChantier = null;
	top10ChantierParCA: ChantierDetail[] = [];
	top10ChantierParHeures: ChantierDetail[] = [];
	chantierParStatusCA = null;
	//  chantierParStatusCA
	chantierParStatuEncours: ChantierParStatus = null;
	chantierParStatuTermine: ChantierParStatus = null;
	ficheInterventionParStatuts: FicheInterventionParStatus[] = [];
	ficheInterventionPlanifiee: FicheInterventionParStatus = null;
	ficheInterventionRealisee: FicheInterventionParStatus = null;
	nbrTotalIntervention = 0;
	nbrinterventionPlanifiee = 0;
	nbrinterventioRealise = 0;
	totalNbrChantier = 0;
	nbrCAtermine = 0;
	nbrCAEncours = 0;
	totalCA = 0;
	totalCaEncours = 0;
	totalCaTermine = 0;
	idClient: [] = [];
	idChantier: [] = [];
	idGroupe: [] = [];
	margeInChantier = 0;
	margeMainOeuvre = 0;
	margeMateriel = 0;
	periode = PeriodType.None
	periodeEnum: typeof PeriodType = PeriodType;
	dateMinimal;
	dateMaximal;
	dateLang;
	listClients: Client[] = null;
	listGroupes: Groupe[] = null;
	filterlistGroupe = [];
	listChantier: ChantierList[] = [];
	listChantiers: any = null;
	filterlistChantier = []
	filterlistClient = [];

	@ViewChild('selecteGroupes') selecteGroupes: NgSelectComponent;
	@ViewChild('selecteChantiers') selecteChantiers: NgSelectComponent;
	@ViewChild('selecteClients') selecteClients: NgSelectComponent;


	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private dialog: MatDialog) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	ngOnInit() {
		this.dashbordChantier();
	}

	filter() {
		this.dashbordChantier();
	}

	dashbordChantier() {

		const res = {
			'workshopId': this.idChantier,
			'clientId': this.idClient,
			'groupeId': this.idGroupe,
			'dateStart': this.dateMaximal,
			'dateEnd': this.dateMinimal,
			'periodType': this.periode,
		}

		this.dateMinimal = this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null;
		this.dateMaximal = this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;
		this.service.create(ApiUrl.analytic + '/Workshop' , res).subscribe(result => {
			this.resdashbordChantier = result.value;
			this.top10ChantierParCA = this.resdashbordChantier.topWorkshopsByTurnover;
			this.chantierParStatusCA = this.resdashbordChantier.workshopsByStatus;
			this.top10ChantierParHeures = this.resdashbordChantier.topWorkshopsByWorkingHours;
			this.ficheInterventionParStatuts = this.resdashbordChantier.operationSheetsByStatus;
			this.chantierParStatut();
			this.ficheInterventionParStatut();
			this.margeChantier();
		})
	}

	ficheInterventionParStatut() {
		if (this.ficheInterventionParStatuts.length !== 0) {
			this.ficheInterventionPlanifiee = this.ficheInterventionParStatuts.find(x => x.status === StatutFicheIntervention.Planifiee);
			this.ficheInterventionRealisee = this.ficheInterventionParStatuts.find(x => x.status === StatutFicheIntervention.Realisee);
			this.nbrinterventionPlanifiee = this.ficheInterventionPlanifiee !== undefined ?
				this.ficheInterventionPlanifiee.operationSheets.length : 0;
			this.nbrinterventioRealise = this.ficheInterventionRealisee !== undefined ? this.ficheInterventionRealisee.operationSheets.length : 0;
			this.nbrTotalIntervention = this.nbrinterventionPlanifiee + this.nbrinterventioRealise;
		} else {
			this.nbrTotalIntervention = 0;
			this.nbrinterventionPlanifiee = 0;
			this.nbrinterventioRealise = 0;
		}

	}



	chantierParStatut() {
		this.chantierParStatuEncours = this.chantierParStatusCA.find(x => x.status === 'accepted') !== undefined ?
		this.chantierParStatusCA.find(x => x.status === 'accepted') : null;
		this.chantierParStatuTermine = this.chantierParStatusCA.filter(x => x.status === 'finished') !== undefined ?
		this.chantierParStatusCA.find(x => x.status === 'finished') : null;
		if (this.chantierParStatuEncours != null) {
			this.totalCaEncours = this.chantierParStatuEncours.totalTurnover;
			this.nbrCAEncours = this.chantierParStatuEncours.workshops.length !== 0 ? this.chantierParStatuEncours.workshops.length : 0;
		}
		if (this.chantierParStatuEncours == null) {

			this.totalCaEncours = 0;
			this.nbrCAEncours = 0;
		}
		if (this.chantierParStatuTermine != null) {
			this.totalCaTermine = this.chantierParStatuTermine.totalTurnover;
			this.nbrCAtermine = (this.chantierParStatuTermine.workshops.length !== 0) ? this.chantierParStatuTermine.workshops.length : 0;
		}
		if (this.chantierParStatuTermine == null) {
			this.totalCaTermine = 0;
			this.nbrCAtermine = 0;
		}
		this.totalCA = this.totalCaEncours + this.totalCaTermine;
		this.totalNbrChantier = this.nbrCAtermine + this.nbrCAEncours;
	}

	margeChantier() {
		this.margeInChantier = this.resdashbordChantier.marginDetails.wokshopMargin;
		this.margeMainOeuvre = this.resdashbordChantier.marginDetails.workforceMargin;
		this.margeMateriel = this.resdashbordChantier.marginDetails.subcontracting;
		this.listChantier = this.chantierParStatusCA.find(x => x.status === 'accepted') !== undefined ?
		this.chantierParStatusCA.find(x => x.status === 'accepted').workshops : [];
	}

	LoadListChantier() {
		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '450px';
		dialogLotConfig.height = '450px';
		dialogLotConfig.data = { listes: this.listChantier };
		const dialogRef = this.dialog.open(ListChantierComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((data) => {
		});

	}

	getChantiers() {
		if (this.listChantiers == null) {
			this.service.getAll(ApiUrl.Chantier).subscribe(res => {
				this.listChantiers = res.value;
				this.filterlistChantier = this.formaterChantier(this.listChantiers);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteChantiers.isOpen = true;
			});
		}
	}

	formaterChantier(list: Workshop[]) {
		const listChantier = [];
		list.forEach((chantier, index) => {
			listChantier.push({ id: chantier.id, name: chantier.name, disabled: false });
		});
		return listChantier;
	}

	getClients() {
		if (this.listClients == null) {
			this.service.getAll(ApiUrl.Client).subscribe(res => {
				this.listClients = res.value;
				this.filterlistClient = this.formaterClient(this.listClients);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteClients.isOpen = true;
			});
		}
	}

	formaterClient(list: Client[]) {
		const listClient = [];
		list.forEach((client, index) => {
			listClient.push({ id: client.id, name: client.firstName, disabled: false });
		});
		return listClient;
	}

	getGroupes() {
		if (this.listGroupes == null) {
			this.service.getAll(ApiUrl.Groupe).subscribe(res => {
				this.listGroupes = res.value;
				this.filterlistGroupe = this.formaterGroupe(this.listGroupes);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteGroupes.isOpen = true;
			});
		}
	}

	formaterGroupe(list: Groupe[]) {
		const listGroupe = [];
		list.forEach((groupe, index) => {
			listGroupe.push({ id: groupe.id, name: groupe.name, disabled: false });
		});
		return listGroupe;
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}
}
