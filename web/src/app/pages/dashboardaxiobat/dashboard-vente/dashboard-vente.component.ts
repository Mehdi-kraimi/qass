import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';
import { AppSettings } from 'app/app-settings/app-settings';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { NgSelectComponent } from '@ng-select/ng-select';
import * as Chart from 'chart.js';
import {
	DashboardVenteModel, Top10CAparClients, DevisParStatus, TauxDevis, LabelDetails, ObjectifsAnnuelCA,
	ObjectifsMensuelleCA, DevisDetail
} from 'app/Models/Entities/Dashboard/DashbordVente';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';
import { DetailArticle } from 'app/Models/Entities/Dashboard/dashboardAchat';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ListDevisComponent } from './list-devis/list-devis.component';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { Product } from 'app/Models/Entities/Biobiotheque/Product';
declare var toastr: any;

@Component({
	selector: 'app-dashboard-vente',
	templateUrl: './dashboard-vente.component.html',
	styleUrls: ['./dashboard-vente.component.scss']
})

export class DashboardVenteComponent implements OnInit {

	/**
	 * view child of chart bar
	 */
	@ViewChild('barChart') chiffreAffaireChartBar;
	@ViewChild('lineChart') ObjectCAChartLine;
	@ViewChild('selecteProduits') selecteProduits: NgSelectComponent;
	@ViewChild('selecteClients') selecteClients: NgSelectComponent;
	@ViewChild('baseChart') baseChart: ElementRef;
	@ViewChild('baseChartMarge') baseChartMarge: ElementRef;

	labels = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
	chart: any;
	chartMarge: any;

	periode = PeriodType.None;
	periodeEnum: typeof PeriodType = PeriodType;

	dateMinimal
	dateMaximal
	dateLang
	idClient: [] = [];

	idProduit: [] = [];
	listClients: Client[] = null;
	dashbordVente: DashboardVenteModel = null;
	top10CAClient: Top10CAparClients[] = [];
	top10CAparArticle: DetailArticle[] = [];
	top10CAparLabel: LabelDetails[] = [];

	statutDevis: typeof StatutDevis = StatutDevis
	devisParStatusAccepte: DevisParStatus;
	devisParStatusFacture: DevisParStatus;
	devisParStatusEnAttente: DevisParStatus;
	devisParStatusRefuse: DevisParStatus;

	totalDevisAccepte = 0;
	totalDevisEnAttente = 0;
	totalDevisTermine = 0;
	totalDevisRefuse = 0;

	totlDevisParStatut = 0;

	nbrDevisAccepte = 0;
	nbrDevisEnAttente = 0;
	nbrDevisTermine = 0;
	nbrDevisRefuse = 0;

	nbrDevisParStatut = 0;
	ListtauxDevis: TauxDevis;
	tauxDevis = 0;

	datasetsCA: any = [];
	datasetsMarge: any = [];

	objectifAnnuelCA: ObjectifsAnnuelCA;
	objectifsMensuelleCA: ObjectifsMensuelleCA[];
	caAnnuelle = null;
	filterlistClient = [];
	filterlistProduit = [];
	listProduits: Product[] = null;
	chartLine: any;
	devisDetails: DevisDetail[] = [];


	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private dialog: MatDialog) {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	ngOnInit() {
		this.dashboardVente()
	}

	filter() {
		this.dashboardVente();
	}

	dashboardVente() {
		this.dateMinimal = this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null;
		this.dateMaximal = this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;

		const res = {
			prodcutId: this.idProduit,
			purchaseAnalyticsFor: '',
			externalPartnerId: this.idClient,
			dateStart: this.dateMinimal,
			dateEnd: this.dateMaximal,
			periodType: this.periode,
		}

		// this.dashbordService.DashbordVente(this.idClient, this.idProduit, this.dateMinimal, this.dateMaximal, this.periode).subscribe(
		this.service.create(ApiUrl.analytic + '/Sales', res).subscribe(result => {
			this.dashbordVente = result.value;
			this.top10CAparArticle = this.dashbordVente.productsDetails.topProductsByTotalSales;
			this.top10CAparLabel = this.dashbordVente.productsDetails.topLabelsByTotalSales;
			this.datasetsCA = this.dashbordVente.turnoverDetails.turnoverIncreaseDetails.monthlyIncreaseDetails.map(x => x.totalIncome);
			this.datasetsMarge = this.dashbordVente.turnoverDetails.turnoverIncreaseDetails.monthlyIncreaseDetails.map(x => x.margin);
			this.top10CAClient = this.dashbordVente.topClients;
			this.devisParStatusAccepte = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.Acceptee);
			this.devisParStatusEnAttente = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.EnAttente);
			this.devisParStatusFacture = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.Facture);
			this.devisParStatusRefuse = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.NonAcceptee);
			this.nbrDevisAccepte = this.devisParStatusAccepte !== undefined ? this.devisParStatusAccepte.quotes.length : 0;
			this.nbrDevisEnAttente = this.devisParStatusEnAttente !== undefined ? this.devisParStatusEnAttente.quotes.length : 0;
			this.nbrDevisTermine = this.devisParStatusFacture !== undefined ? this.devisParStatusFacture.quotes.length : 0;
			this.nbrDevisRefuse = this.devisParStatusRefuse !== undefined ? this.devisParStatusRefuse.quotes.length : 0;
			this.nbrDevisParStatut = this.dashbordVente.quoteDetails.count;

			this.totalDevisAccepte = this.devisParStatusAccepte !== undefined ?
				this.devisParStatusAccepte.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;
			this.totalDevisEnAttente = this.devisParStatusEnAttente !== undefined ?
				this.devisParStatusEnAttente.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;
			this.totalDevisTermine = this.devisParStatusFacture !== undefined ?
				this.devisParStatusFacture.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;
			this.totalDevisRefuse = this.devisParStatusRefuse !== undefined ?
				this.devisParStatusRefuse.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;

			this.totlDevisParStatut = this.dashbordVente.quoteDetails.total;
			this.tauxDevis = this.devisParStatusAccepte !== undefined  ? this.devisParStatusAccepte.percent : 0;

			this.objectifsMensuelleCA = this.dashbordVente.turnoverDetails.turnoverAchievementDetails.monthlyDetails;
			this.caAnnuelle = Math.min(this.dashbordVente.turnoverDetails.turnoverAchievementDetails.percentage).toFixed(2);

			this.devisDetails = this.devisParStatusAccepte !== undefined ? this.devisParStatusAccepte.quotes : [];

			this.showChartEvolutionCA();
			this.showChartEvolutionMarge();
			this.chartObjectCALine(this.objectifsMensuelleCA.map(x =>
				Math.min(x.percentage).toFixed(2)
			))
		}
		)
	}

	getClients() {
		if (this.listClients == null) {
			this.service.getAll(ApiUrl.Client).subscribe(res => {
				this.listClients = res.value;
				this.filterlistClient = this.formaterClient(this.listClients);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteClients.isOpen = true;
			});
		}
	}

	formaterClient(list: Client[]) {
		const listClient = [];
		list.forEach((client, index) => {
			listClient.push({ id: client.id, name: client.firstName, disabled: false });
		});
		return listClient;
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}

	getProduits() {
		if (this.listProduits == null) {
			this.service.getAll(ApiUrl.Produit).subscribe(res => {
				this.listProduits = res.value;
				this.filterlistProduit = this.formaterProduit(this.listProduits);

			}, async  err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteProduits.isOpen = true;
			});
		}
	}

	formaterProduit(list: Product[]) {
		const listProduc = [];
		list.forEach((prod, index) => {
			listProduc.push({ id: prod.id, name: prod.name, disabled: false });
		});
		return listProduc;
	}

	showChartEvolutionCA() {
		if (this.chart) {
			this.chart.destroy();
		}

		this.chart = new Chart(this.baseChart.nativeElement, {
			type: 'bar',
			data: {
				labels: this.labels,
				datasets: [
					{
						label: 'Chaiffre d affaire',
						data: this.datasetsCA,
						backgroundColor: [
							'#8ce89e',
							'#ade2ff',
							'#99e4e4',
							'#74e5cc',
							'#5cc5c5',
							'#97d9ff',
							'#93a9cf',
							'#d19392',
							'#b9cd96',
							'#91c3d5'

						],
						fill: true,
						lineTension: 0.2,
						borderWidth: 1
					}
				]
			},
			options: {
				responsive: true,
				title: {
					display: true
				},
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}

	showChartEvolutionMarge() {
		if (this.chartMarge) {
			this.chartMarge.destroy();
		}

		this.chartMarge = new Chart(this.baseChartMarge.nativeElement, {
			type: 'bar',
			data: {
				labels: this.labels,
				datasets: [
					{
						label: 'Marge',
						data: this.datasetsMarge,
						backgroundColor: [
							'#8ce89e',
							'#ade2ff',
							'#99e4e4',
							'#74e5cc',
							'#5cc5c5',
							'#97d9ff',
							'#93a9cf',
							'#d19392',
							'#b9cd96',
							'#91c3d5'

						],
						fill: true,
						lineTension: 0.2,
						borderWidth: 1
					}
				]
			},
			options: {
				responsive: true,
				title: { display: true },
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	}

	chartObjectCALine(dataPoints) {
		if (this.chartLine) {
			this.chartLine.destroy();
		}

		this.chartLine = new Chart(this.ObjectCAChartLine.nativeElement, {
			type: 'line',
			data: {
				labels: this.labels,
				datasets: [
					{
						label: 'Pourcentage CA',

						data: dataPoints,
						backgroundColor: '#f8fdff',
						borderColor: '#80b6f4',
						pointBorderColor: '#80b6f4',
						pointBackgroundColor: '#80b6f4',
						pointHoverBackgroundColor: '#80b6f4',
						pointHoverBorderColor: '#80b6f4',
						pointBorderWidth: 10,
						pointHoverRadius: 10,
						pointHoverBorderWidth: 1,
						pointRadius: 1,
						borderWidth: 2,
						fill: true
					}
				]
			},
			options: {
				legend: {
					position: 'bottom'
				},
				scales: {
					yAxes: [{
						ticks: {
							fontColor: 'rgba(0,0,0,0.5)',
							beginAtZero: true,
							maxTicksLimit: 5,
							padding: 20
						},
						gridLines: {
							drawTicks: false,
							display: false
						}
					}],
					xAxes: [{
						gridLines: {
							zeroLineColor: 'transparent'
						},
						ticks: {
							padding: 20,
							fontColor: 'rgba(0,0,0,0.5)',
						}
					}]
				}
			}
		});
	}

	LoadListDevis() {
		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '600px';
		dialogLotConfig.data = { listes: this.devisDetails };
		const dialogRef = this.dialog.open(ListDevisComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((data) => {
		});

	}
}
