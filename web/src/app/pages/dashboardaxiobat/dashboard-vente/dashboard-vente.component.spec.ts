import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardVenteComponent } from './dashboard-vente.component';

describe('DashboardVenteComponent', () => {
  let component: DashboardVenteComponent;
  let fixture: ComponentFixture<DashboardVenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardVenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardVenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
