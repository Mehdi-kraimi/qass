import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ChantierDetail, DashboardChantier, ChantierParStatus, FicheInterventionParStatus, ChantierList } from 'app/Models/Entities/Dashboard/dashbordChantier';
import { PeriodType } from 'app/Enums/Commun/PeriodeEnum.Enum';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Groupe } from 'app/Models/Entities/Contacts/Groupe';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ListChantierComponent } from './chantier-dashbord/list-chantier/list-chantier.component';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Workshop } from 'app/Models/Entities/Documents/Workshop';
import * as Chart from 'chart.js';
import {
	DashboardVenteModel, Top10CAparClients, DevisParStatus, TauxDevis, LabelDetails, ObjectifsAnnuelCA,
	ObjectifsMensuelleCA, DevisDetail
} from 'app/Models/Entities/Dashboard/DashbordVente';
import { DashboardFinanceModel } from 'app/Models/Entities/Dashboard/DashboardFinance';

import { DashboardAchatModel, FournisseurDetail, ReparationAchatParFournisseur, DetailArticle } from 'app/Models/Entities/Dashboard/dashboardAchat';

import { Product } from 'app/Models/Entities/Biobiotheque/Product';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';
import { HeaderService } from 'app/services/header/header.service';
import { MenuService } from 'app/services/menu/menu.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var toastr: any;

@Component({
	selector: 'app-dashboardaxiobat',
	templateUrl: './dashboardaxiobat-page.component.html',
	styleUrls: ['./dashboardaxiobat-page.component.scss']
})
export class DashboardAxioBatPageComponent implements OnInit {

	dataMaintenance = false;
	dataAchat = false;
	processing = false;
	resdashbordChantier: DashboardChantier = null;
	top10ChantierParCA: ChantierDetail[] = [];
	top10ChantierParHeures: ChantierDetail[] = [];
	chantierParStatusCA = null;
	//  chantierParStatusCA
	chantierParStatuEncours: ChantierParStatus = null;
	chantierParStatuTermine: ChantierParStatus = null;
	ficheInterventionParStatuts: FicheInterventionParStatus[] = [];
	ficheInterventionPlanifiee: FicheInterventionParStatus = null;
	ficheInterventionRealisee: FicheInterventionParStatus = null;
	nbrTotalIntervention = 0;
	nbrinterventionPlanifiee = 0;
	nbrinterventioRealise = 0;
	totalNbrChantier = 0;
	totalNbrCE = 0;
	nbrCAtermine = 0;
	nbrCAEncours = 0;
	totalCA = 0;
	totalCaEncours = 0;
	totalCaTermine = 0;
	idClient: [] = [];
	idChantier: [] = [];
	idGroupe: [] = [];
	margeInChantier = 0;
	margeMainOeuvre = 0;
	margeMateriel = 0;
	periode = PeriodType.None
	periodeEnum: typeof PeriodType = PeriodType;
	dateMinimal;
	dateMaximal;
	dateLang;
	listClients: Client[] = null;
	listGroupes: Groupe[] = null;
	filterlistGroupe = [];
	listChantier: ChantierList[] = [];
	listChantiers: any = null;
	filterlistChantier = []
	filterlistClientSale = [];
	contractsPerStatus;
	objectifCaYearly = [];

	/*  vente table  */
	labels = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
	chart: any;
	chartMarge: any;

	periodeBuy = PeriodType.None;
	periodeEnumBuy: typeof PeriodType = PeriodType;

	dateMinimalSale;
	dateMaximalSale;
	idClientSale: [] = [];

	idProduit: [] = [];
	listClientsBuy: Client[] = null;
	dashbordVente: DashboardVenteModel = null;
	top10CAClient: Top10CAparClients[] = [];
	top10CAparArticle: DetailArticle[] = [];
	top10CAparLabel: LabelDetails[] = [];

	statutDevis: typeof StatutDevis = StatutDevis
	devisParStatusAccepte: DevisParStatus;
	devisParStatusFacture: DevisParStatus;
	devisParStatusEnAttente: DevisParStatus;
	devisParStatusRefuse: DevisParStatus;
	devisParStatusSigne: DevisParStatus;

	totalDevisAccepte = 0;
	totalDevisEnAttente = 0;
	totalDevisTermine = 0;
	totalDevisRefuse = 0;
	totalDevisSigne = 0;

	totlDevisParStatut = 0;

	nbrDevisAccepte = 0;
	nbrDevisEnAttente = 0;
	nbrDevisTermine = 0;
	nbrDevisRefuse = 0;
	nbrDevisSigne = 0;

	nbrDevisParStatut = 0;
	ListtauxDevis: TauxDevis;
	tauxDevis = 0;

	datasetsCA: any = [];
	datasetsMarge: any = [];

	objectifAnnuelCA: ObjectifsAnnuelCA;
	objectifsMensuelleCA: ObjectifsMensuelleCA[];
	caAnnuelle = null;
	filterlistClient = [];
	filterlistProduit = [];
	listProduits: Product[] = null;
	chartLine: any;
	devisDetails: DevisDetail[] = [];

	/* finance */

	montantencaissements = 0;
	CArestantencaisser = 0;
	achatrestantencaisser = 0;
	soldeFinance = 0;

	objectifsAnnuelCAN: ObjectifsAnnuelCA;
	dashboardFinance: DashboardFinanceModel = null;
	labelsYears: any;
	datasetsCA1: any = [];
	datasetsMarge1: any = [];
	datasetsCA2: any = [];
	datasetsMarge2: any = [];
	chartMunsuelle: any;
	datasetCaAnnuelle1: any = [];
	datasetCaAnnuelle2: any = [];

	datasetMargeAnnuelle1: any = [];
	datasetMargeAnnuelle2: any = [];
	yearCurrent = new Date().getFullYear();
	yearLast = new Date().getFullYear() - 1;
	chartMargeMunsuelle: any;
	chartcaAnnuelle: any;
	chartobjectifsAnnuelle: any;
	chartmargeAnnuelle: any;
	//chartLine: any;
	dataCurrentYearFinance: any;
	dataLastYearFinance: any;


	/* dacboard achat */


	dashbordAchat: DashboardAchatModel = null;
	classementCategories: { labelName: string, total: number }[] = [];
	top10fournisseurCa: FournisseurDetail[] = [];
	idProduitFournisseur: [] = [];
	idFournisseur: [] = [];
	idArticle = null;
	produits = null;
	repartitionAchatParFournisseur: ReparationAchatParFournisseur = null;
	detailArticleReparation: DetailArticle
	fournisseurList: FournisseurDetail[];
	top10articleQte: DetailArticle[];
	top10articlePrix: DetailArticle[];
	//filterlistProduit = [];
	//listProduits: Product[] = null;

	periodeFournisseur = PeriodType.None

	dateMinimalFournisseur;
	dateMaximalFournisseur;

	years = [];
	currentYearCA = 0;
	currentYearHT = 0;

	@ViewChild('selecteGroupes') selecteGroupes: NgSelectComponent;
	@ViewChild('selecteChantiers') selecteChantiers: NgSelectComponent;
	@ViewChild('selecteClients') selecteClients: NgSelectComponent;
	@ViewChild('baseChart') baseChart: ElementRef;
	@ViewChild('lineChart') ObjectCAChartLine: ElementRef;
	@ViewChild('chartMonthlyEvolutionCa') chartMonthlyEvolutionCa: ElementRef;
	@ViewChild('chartDevis') chartDevis: ElementRef;
	@ViewChild('lineObjectifMunsuelle') lineObjectifMunsuelle: ElementRef;

	constructor(
		private translate: TranslateService,
		private header: HeaderService,
		public menu: MenuService,
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private dialog: MatDialog,

	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});


	}

	prepareBreadcrumb(): void {
		this.translate.get(`title`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: translation, route: '/' }
			];
		});
	}
	ngOnInit() {
		this.prepareBreadcrumb();
		this.dashbordChantier();
		this.dashboardVente();
		this.dashboardFinanceVente();
		this.dashboardAchat();
		this.dashbordMaintenance();

		// Generating a list of years ranging between (current year) to 1900
		this.years = this.prepareYears();
		this.currentYearCA = new Date().getFullYear();
		this.currentYearHT = new Date().getFullYear();
	}

	dashbordMaintenance() {
		this.dataMaintenance = true;
		const res = {
			'Technicians': [],
			'clientId': this.idClient,
			'groupeId': this.idGroupe,
			'dateStart': null,
			'dateEnd': null,
			'periodType': PeriodType.None,
		}
		// this.dateMinimal = this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null;
		// this.dateMaximal = this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;
		this.service.create(ApiUrl.analytic + '/Maintenance', res).subscribe(result => {
			this.contractsPerStatus = result.value.contractsPerStatus;
			this.contractsPerStatus.forEach(element => {
				this.totalNbrCE += element.contractsCount;
			});
		})
	}


	filter() {
		this.dashbordChantier();
	}


	dashbordChantier() {
		const res = {
			'workshopId': this.idChantier,
			'clientId': this.idClient,
			'groupeId': this.idGroupe,
			'dateStart': this.dateMaximal,
			'dateEnd': this.dateMinimal,
			'periodType': this.periode,
		}
		this.dateMinimal = this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null;
		this.dateMaximal = this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;
		this.service.create(ApiUrl.analytic + '/Workshop', res).subscribe(result => {
			this.resdashbordChantier = result.value;
			this.top10ChantierParCA = this.resdashbordChantier.topWorkshopsByTurnover;
			this.chantierParStatusCA = this.resdashbordChantier.workshopsByStatus;
			this.top10ChantierParHeures = this.resdashbordChantier.topWorkshopsByWorkingHours;
			this.ficheInterventionParStatuts = this.resdashbordChantier.operationSheetsByStatus;
			this.chantierParStatut();
			this.ficheInterventionParStatut();
			this.margeChantier();
		})
	}


	ficheInterventionParStatut() {
		if (this.ficheInterventionParStatuts.length !== 0) {
			this.ficheInterventionPlanifiee = this.ficheInterventionParStatuts.find(x => x.status === StatutFicheIntervention.Planifiee);
			this.ficheInterventionRealisee = this.ficheInterventionParStatuts.find(x => x.status === StatutFicheIntervention.Realisee);
			this.nbrinterventionPlanifiee = this.ficheInterventionPlanifiee !== undefined ?
				this.ficheInterventionPlanifiee.operationSheets.length : 0;
			this.nbrinterventioRealise = this.ficheInterventionRealisee !== undefined ? this.ficheInterventionRealisee.operationSheets.length : 0;
			this.nbrTotalIntervention = this.nbrinterventionPlanifiee + this.nbrinterventioRealise;
		} else {
			this.nbrTotalIntervention = 0;
			this.nbrinterventionPlanifiee = 0;
			this.nbrinterventioRealise = 0;
		}

	}

	chantierParStatut() {
		this.chantierParStatuEncours = this.chantierParStatusCA.find(x => x.status === 'accepted') !== undefined ?
			this.chantierParStatusCA.find(x => x.status === 'accepted') : null;
		this.chantierParStatuTermine = this.chantierParStatusCA.filter(x => x.status === 'finished') !== undefined ?
			this.chantierParStatusCA.find(x => x.status === 'finished') : null;
		if (this.chantierParStatuEncours != null) {
			this.totalCaEncours = this.chantierParStatuEncours.totalTurnover;
			this.nbrCAEncours = this.chantierParStatuEncours.workshops.length !== 0 ? this.chantierParStatuEncours.workshops.length : 0;
		}
		if (this.chantierParStatuEncours == null) {

			this.totalCaEncours = 0;
			this.nbrCAEncours = 0;
		}
		if (this.chantierParStatuTermine != null) {
			this.totalCaTermine = this.chantierParStatuTermine.totalTurnover;
			this.nbrCAtermine = (this.chantierParStatuTermine.workshops.length !== 0) ? this.chantierParStatuTermine.workshops.length : 0;
		}
		if (this.chantierParStatuTermine == null) {
			this.totalCaTermine = 0;
			this.nbrCAtermine = 0;
		}
		this.totalCA = this.totalCaEncours + this.totalCaTermine;
		this.totalNbrChantier = this.nbrCAtermine + this.nbrCAEncours;
	}


	margeChantier() {
		this.margeInChantier = this.resdashbordChantier.marginDetails.wokshopMargin;
		this.margeMainOeuvre = this.resdashbordChantier.marginDetails.workforceMargin;
		this.margeMateriel = this.resdashbordChantier.marginDetails.subcontracting;
		this.listChantier = this.chantierParStatusCA.find(x => x.status === 'accepted') !== undefined ?
			this.chantierParStatusCA.find(x => x.status === 'accepted').workshops : [];
	}

	LoadListChantier() {
		const dialogLotConfig = new MatDialogConfig();
		dialogLotConfig.width = '450px';
		dialogLotConfig.height = '450px';
		dialogLotConfig.data = { listes: this.listChantier };
		const dialogRef = this.dialog.open(ListChantierComponent, dialogLotConfig);
		dialogRef.afterClosed().subscribe((data) => {
		});

	}

	getChantiers() {
		if (this.listChantiers == null) {
			this.service.getAll(ApiUrl.Chantier).subscribe(res => {
				this.listChantiers = res.value;
				this.filterlistChantier = this.formaterChantier(this.listChantiers);
			}, async err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteChantiers.isOpen = true;
			});
		}
	}

	formaterChantier(list: Workshop[]) {
		const listChantier = [];
		list.forEach((chantier, index) => {
			listChantier.push({ id: chantier.id, name: chantier.name, disabled: false });
		});
		return listChantier;
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}

	/* you are in chart graph  */

	dashboardVente() {
		this.dateMinimalSale = this.dateMinimalSale != null ? AppSettings.formaterDatetime(this.dateMinimalSale.toString()) : null;
		this.dateMaximalSale = this.dateMaximalSale != null ? AppSettings.formaterDatetime(this.dateMaximalSale.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;

		const res = {
			prodcutId: this.idClientSale,
			purchaseAnalyticsFor: '',
			externalPartnerId: this.idClient,
			dateStart: this.dateMinimalSale,
			dateEnd: this.dateMaximalSale,
			periodType: this.periode,
		}

		// this.dashbordService.DashbordVente(this.idClient, this.idClientSale, this.dateMinimal, this.dateMaximal, this.periode).subscribe(
		this.service.create(ApiUrl.analytic + '/Sales', res).subscribe(result => {
			this.dashbordVente = result.value;
			this.top10CAparArticle = this.dashbordVente.productsDetails.topProductsByTotalSales;
			this.top10CAparLabel = this.dashbordVente.productsDetails.topLabelsByTotalSales;
			this.datasetsCA = this.dashbordVente.turnoverDetails.turnoverIncreaseDetails.monthlyIncreaseDetails.map(x => x.totalIncome);
			this.datasetsMarge = this.dashbordVente.turnoverDetails.turnoverIncreaseDetails.monthlyIncreaseDetails.map(x => x.margin);
			this.top10CAClient = this.dashbordVente.topClients;
			this.devisParStatusAccepte = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.Acceptee);
			this.devisParStatusEnAttente = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.EnAttente);
			this.devisParStatusFacture = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.Facture);
			this.devisParStatusRefuse = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.NonAcceptee);
			this.devisParStatusSigne = this.dashbordVente.quoteDetails.quotesByStatus.find(x => x.status === this.statutDevis.Signed);
			this.nbrDevisAccepte = this.devisParStatusAccepte !== undefined ? this.devisParStatusAccepte.quotes.length : 0;
			this.nbrDevisEnAttente = this.devisParStatusEnAttente !== undefined ? this.devisParStatusEnAttente.quotes.length : 0;
			this.nbrDevisTermine = this.devisParStatusFacture !== undefined ? this.devisParStatusFacture.quotes.length : 0;
			this.nbrDevisRefuse = this.devisParStatusRefuse !== undefined ? this.devisParStatusRefuse.quotes.length : 0;
			this.nbrDevisSigne = this.devisParStatusSigne !== undefined ? this.devisParStatusSigne.quotes.length : 0;
			this.nbrDevisParStatut = this.dashbordVente.quoteDetails.count;

			this.totalDevisAccepte = this.devisParStatusAccepte !== undefined ?
				this.devisParStatusAccepte.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;
			this.totalDevisEnAttente = this.devisParStatusEnAttente !== undefined ?
				this.devisParStatusEnAttente.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;
			this.totalDevisTermine = this.devisParStatusFacture !== undefined ?
				this.devisParStatusFacture.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;
			this.totalDevisRefuse = this.devisParStatusRefuse !== undefined ?
				this.devisParStatusRefuse.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;
			this.totalDevisSigne = this.devisParStatusSigne !== undefined ?
				this.devisParStatusSigne.quotes.reduce((x, y) => x + y.totalHT, 0) : 0;

			this.totlDevisParStatut = this.dashbordVente.quoteDetails.total;
			this.tauxDevis = this.dashbordVente.quoteDetails.acceptanceRate;

			this.objectifsMensuelleCA = this.dashbordVente.turnoverDetails.turnoverAchievementDetails.monthlyDetails;
			this.caAnnuelle = Math.min(this.dashbordVente.turnoverDetails.turnoverAchievementDetails.percentage).toFixed(2);

			this.devisDetails = this.devisParStatusAccepte !== undefined ? this.devisParStatusAccepte.quotes : [];


			this.objectifCaYearly = this.objectifsMensuelleCA.map(x => Math.min(x.percentage).toFixed(2));

			this.showChartEvolutionCA();
			this.showChartDevis();

		}
		)
	}

	dashboardFinanceVente() {
		const res = {
			externalPartnerId: this.idClient,
			dateStart: this.dateMinimal,
			dateEnd: this.dateMinimal,
			periodType: this.periode,
		};
		this.dateMinimal = this.dateMinimal != null ? AppSettings.formaterDatetime(this.dateMinimal.toString()) : null;
		this.dateMaximal = this.dateMaximal != null ? AppSettings.formaterDatetime(this.dateMaximal.toString()) : null;
		this.periode = this.periode == null ? PeriodType.None : this.periode;
		this.service.create(ApiUrl.analytic + '/Financial', res).subscribe(result => {
			this.dashboardFinance = result.value;
			this.datasetsCA1 = this.dashboardFinance.turnoverIncreaseDetails.currentPeriod.monthlyIncreaseDetails.map(x => x.totalIncome);
			this.datasetsMarge1 = this.dashboardFinance.turnoverIncreaseDetails.currentPeriod.monthlyIncreaseDetails.map(x => x.margin);

			this.datasetsCA2 = this.dashboardFinance.turnoverIncreaseDetails.preCurrentPeriod.monthlyIncreaseDetails.map(x => x.totalIncome);
			this.datasetsMarge2 = this.dashboardFinance.turnoverIncreaseDetails.preCurrentPeriod.monthlyIncreaseDetails.map(x => x.margin);

			this.datasetCaAnnuelle1 = [this.dashboardFinance.turnoverIncreaseDetails.currentPeriod.totalIncome];
			this.datasetCaAnnuelle2 = [this.dashboardFinance.turnoverIncreaseDetails.preCurrentPeriod.totalIncome];

			this.datasetMargeAnnuelle1 = [this.dashboardFinance.turnoverIncreaseDetails.currentPeriod.margin];
			this.datasetMargeAnnuelle2 = [this.dashboardFinance.turnoverIncreaseDetails.preCurrentPeriod.margin];

			this.labelsYears = this.getLabels(this.yearLast, this.yearCurrent);

			this.objectifAnnuelCA = this.dashboardFinance.turnoverAchievementDetails.currentPeriod;
			this.objectifsAnnuelCAN = this.dashboardFinance.turnoverAchievementDetails.preCurrentPeriod;

			// this.grapheProgressionMunsuelle();
			// this.grapheProgressionMargeMunsuelle();
			// this.grapheCAannuelle();
			// this.grapheMargeannuelle();

			// this.grapheObjectifsannuelle(Math.min(this.ifNan(this.objectifsAnnuelCAN.percentage)).toFixed(2),
			// 	Math.min(this.ifNan(this.objectifAnnuelCA.percentage)).toFixed(2)
			// );


			this.dataLastYearFinance = Math.min(this.ifNan(this.objectifsAnnuelCAN.percentage)).toFixed(2);
			this.dataCurrentYearFinance = Math.min(this.ifNan(this.objectifAnnuelCA.percentage)).toFixed(2)

			this.montantencaissements = this.dashboardFinance.turnoverDetails.totalIncome;
			this.CArestantencaisser = this.dashboardFinance.turnoverDetails.totalUnpaidIncome;
			this.achatrestantencaisser = this.dashboardFinance.turnoverDetails.totalUnpaidExpenses;
			this.soldeFinance = this.CArestantencaisser - this.achatrestantencaisser;
			const data1 = this.objectifsAnnuelCAN.monthlyDetails.map(x =>
				Math.min(this.ifNan(x.percentage)).toFixed(2));

			const data2 = this.objectifAnnuelCA.monthlyDetails.map(x =>
				Math.min(this.ifNan(x.percentage)).toFixed(2))

			this.chartObjectCALine(data1, data2);

		}
		);
	}

	ifNan(perce) {
		if (perce === 'Infinity') { return 100; }
		if (perce === 'NaN') { return 0; }
		// tslint:disable-next-line:radix
		return parseInt(perce);
	}

	showChartEvolutionCA() {

		if (this.chart) {
			this.chart.destroy();
		}

		const margeColor = this.datasetMargeAnnuelle1 > 0
			? '#38b475'
			: this.datasetMargeAnnuelle1 < 0
				? '#f53c56'
				: '#8d9cad';

		this.chart = new Chart(this.chartMonthlyEvolutionCa.nativeElement, {

			type: 'line',
			data: {
				labels: this.labels,
				datasets: [
					{
						label: 'Chiffre d\'affaires',
						data: this.datasetsCA,
						backgroundColor: '#05658d',
						borderColor: '#05658d',
						pointBorderWidth: 4,
						pointHoverRadius: 4,
						fill: false,
						borderWidth: 2
					},
					{
						label: 'Marge',
						data: this.datasetsMarge,
						backgroundColor: margeColor,
						borderColor: margeColor,
						pointBorderWidth: 4,
						pointHoverRadius: 4,
						borderWidth: 2,
						fill: false,
					}
				]
			},
			options: {
				legend: {
					display: false
				},
				scales: {
					yAxes: [{
						ticks: {
							fontColor: 'rgba(0,0,0,0.5)',
							beginAtZero: true,
							padding: 40
						},
						gridLines: {
							drawTicks: false,
							display: false
						}
					}],
					xAxes: [{
						ticks: {
							fontColor: 'rgba(0,0,0,0.5)',
							beginAtZero: true,
							padding: 40
						},
						gridLines: {
							drawTicks: false,
							display: true
						}
					}]
				}
			}
		});
	}

	showChartDevis() {
		this.chart = new Chart(this.chartDevis.nativeElement, {
			type: 'doughnut',
			data: {
				labels: [
					this.nbrDevisAccepte + ' Accepté ' + this.totalDevisAccepte + '€',
					this.nbrDevisEnAttente + ' En Attente ' + this.totalDevisEnAttente + '€',
					this.nbrDevisTermine + ' Facturé ' + this.totalDevisEnAttente + '€',
					this.nbrDevisRefuse + ' Refusé ' + this.totalDevisRefuse + '€',
					this.nbrDevisSigne + ' Signé ' + this.totalDevisSigne + '€'
				],
				datasets: [
					{
						data: [this.totalDevisAccepte, this.totalDevisEnAttente, this.totalDevisEnAttente, this.totalDevisRefuse, this.totalDevisSigne],
						backgroundColor: [
							'#16d39a ',
							'#FFA87D',
							'#459dff',
							'#fa5502 ',
							'#78A300',
						],
						pointHoverBorderWidth: 1,
						pointRadius: 1,
					},
				]
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					position: 'bottom'
				},
			}
		});
	}



	getClients() {
		if (this.listClients == null) {
			this.service.getAll(ApiUrl.Client).subscribe(res => {
				this.listClients = res.value;
				this.filterlistClientSale = this.formaterClient(this.listClients);

			}, async err => {
				const transaltions = await this.getTranslationByKey('errors');
				toastr.warning(transaltions.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, () => {
				this.selecteClients.isOpen = true;
			});
		}
	}

	formaterClient(list: Client[]) {
		const listClient = [];
		list.forEach((client, index) => {
			listClient.push({ id: client.id, name: client.firstName, disabled: false });
		});
		return listClient;
	}

	getLabels(yearLast, yearCurrent) {
		return [yearLast.toString(), yearCurrent.toString()]
	}

	chartObjectCALine(data1, data2) {
		this.chartLine = new Chart(this.lineObjectifMunsuelle.nativeElement, {
			type: 'line',
			data: {
				labels: this.labels,
				datasets: [{
					label: 'Prévision CA  ' + this.yearLast,
					backgroundColor: '#05658d',
					borderColor: '#05658d',
					data: data1,
					fill: false,
					borderWidth: 2
				}, {
					label: 'Prévision CA  ' + this.yearCurrent,
					backgroundColor: '#ff7588',
					borderColor: '#ff7588',
					data: data2,
					fill: false,
					borderWidth: 2
				}]
			},
			options: {
				legend: {
					display: false
				},
				scales: {
					yAxes: [{
						ticks: {
							fontColor: 'rgba(0,0,0,0.5)',
							beginAtZero: true,
							maxTicksLimit: 5,
							padding: 20
						},
						gridLines: {
							drawTicks: false,
							display: false
						}
					}],
					xAxes: [{
						gridLines: {
							zeroLineColor: 'transparent'
						},
						ticks: {
							padding: 20,
							fontColor: 'rgba(0,0,0,0.5)',
						}
					}]
				}
			}
		});
	}

	async dashboardAchat() {
		this.dateMinimalFournisseur = this.dateMinimalFournisseur != null ? AppSettings.formaterDatetime(this.dateMinimalFournisseur.toString()) : null;
		this.dateMaximalFournisseur = this.dateMaximalFournisseur != null ? AppSettings.formaterDatetime(this.dateMaximalFournisseur.toString()) : null;
		this.periodeFournisseur = this.periode == null ? PeriodType.None : this.periode;

		const res = {
			prodcutId: this.idProduitFournisseur,
			purchaseAnalyticsFor: this.idArticle,
			externalPartnerId: this.idFournisseur,
			dateStart: this.dateMinimalFournisseur,
			dateEnd: this.dateMaximalFournisseur,
			periodType: this.periode,
		}
		this.service.create(ApiUrl.analytic + '/Purchase', res).subscribe(result => {
			this.dashbordAchat = result.value;
			this.top10fournisseurCa = this.dashbordAchat.topSuppliers;
			this.top10articlePrix = this.dashbordAchat.topProductsBySupplierPrice;
			this.top10articleQte = this.dashbordAchat.topProductsByQuantity;
			if (this.dashbordAchat.selectedProductPurchaseDetails != null) {
				this.fournisseurList = this.dashbordAchat.selectedProductPurchaseDetails;
				let sum = 0
				this.fournisseurList.forEach(element => {
					sum += element['price'] * element['quantity'];
				});
				try {
					this.calculations(this.fournisseurList, sum).then((val) => {
						this.classementCategories = val as any;
						setTimeout(() => {
							//this.buildChart();
						}, 0);
					});
				} finally {
					// this.loader.hide();
				}
			}
			//this.buildChart();

		});
	}


	async calculations(ListFournisseur, sum) {
		return new Promise(async (resolve) => {
			const classementCategories = [];

			ListFournisseur.forEach(element => {
				const pourcentage = (((element.price * element.quantity) / sum) * 100);
				classementCategories.push({ labelName: element.name, total: pourcentage });

			});
			resolve(classementCategories.sort((a, b) => a.total > b.total ? -1 : 1));
		});
	}

	onYearCAChanged(): void {
		// this.prepareChart(this.currentYear);
	}
	
	onYearHTChanged(): void {
		// this.prepareChart(this.currentYear);
	}

	getCurrentYear = () => new Date().getFullYear();

	prepareYears = () => new Array(this.getCurrentYear() - 1900 + 1).fill(0).map((_, i: number) => this.getCurrentYear() - i);
}
