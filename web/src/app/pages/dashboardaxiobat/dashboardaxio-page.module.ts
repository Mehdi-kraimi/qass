import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DashboardAxioBatPagesRoutingModule } from './dashboardaxiobat-routing.module';
import { DashboardAxioBatPageComponent } from './dashboardaxiobat-page.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { ChantierDashbordComponent } from './chantier-dashbord/chantier-dashbord.component';
import { CommonModules } from 'app/common/common.module';
import { ChartsModule } from 'ng2-charts';
import { DashboardAchatComponent } from './dashboard-achat/dashboard-achat.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { DashboardVenteComponent } from './dashboard-vente/dashboard-vente.component';
import { DashboardFinanceComponent } from './dashboard-finance/dashboard-finance.component';
import { ListChantierComponent } from './chantier-dashbord/list-chantier/list-chantier.component';
import { MatDialogModule, MatRippleModule, MatSelectModule } from '@angular/material';
import { ListDevisComponent } from './dashboard-vente/list-devis/list-devis.component';
import { MaintenanceSavDashboardComponent } from './maintenance-sav-dashboard/maintenance-sav-dashboard.component';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/dashboard/', '.json');
}

@NgModule({
	imports: [
		CommonModule,
		DashboardAxioBatPagesRoutingModule,
		FormsModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		CommonModules,
		ChartsModule,
		NgSelectModule,
		NgbTooltipModule,
		CalendarModule,
		MatDialogModule,
		MatRippleModule,
		MatSelectModule
	],
	declarations: [
		DashboardAxioBatPageComponent,
		ChantierDashbordComponent,
		DashboardAchatComponent,
		DashboardVenteComponent,
		DashboardFinanceComponent,
		MaintenanceSavDashboardComponent,
		ListChantierComponent,
		ListDevisComponent],
	providers: [],

	entryComponents: [ListChantierComponent, ListDevisComponent]

})
export class DashboardAxioPagesModule { }
