import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceSavDashboardComponent } from './maintenance-sav-dashboard.component';

describe('MaintenanceSavDashboardComponent', () => {
	let component: MaintenanceSavDashboardComponent;
	let fixture: ComponentFixture<MaintenanceSavDashboardComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [MaintenanceSavDashboardComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(MaintenanceSavDashboardComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
