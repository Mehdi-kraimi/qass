import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowComponentVisiteMaintenance } from './show/show.component';

const routes: Routes = [{
	path: '',
	children: [
		{
			path: '',
			pathMatch: 'full',
			loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
		},
		{
			path: 'detail/:id',
			component: ShowComponentVisiteMaintenance,
		},
		{
			path: 'detail/:idContratVisite/:Year/:month',
			component: ShowComponentVisiteMaintenance,
		},
	],
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class VisiteMaintenanceRoutingModule { }
