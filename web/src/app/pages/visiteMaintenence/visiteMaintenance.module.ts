import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisiteMaintenanceRoutingModule } from './visite-maintenance-routing.module';
import { ShowComponentVisiteMaintenance } from './show/show.component';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { CommonModules } from 'app/common/common.module';
import { CalendarModule, SplitButtonModule } from 'app/custom-module/primeng/primeng';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MatDialogModule } from '@angular/material';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';


export function createTranslateLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/visitemaintenance/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' }
	]);
}


@NgModule({
	declarations: [
		ShowComponentVisiteMaintenance],
	imports: [
		CommonModule,
		VisiteMaintenanceRoutingModule,
		CommonModules,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		FormsModule,
		ReactiveFormsModule,
		NgSelectModule,
		NgbTooltipModule,
		CalendarModule,
		DataTablesModule,
		SplitButtonModule,
		InfiniteScrollModule,
		MatDialogModule,
	],
	providers: [
		PreviousRouteService,
		PreviousRouteService,

	],
	exports: [ShowComponentVisiteMaintenance]

})
export class VisiteMaintenanceModule { }
