import { Component, OnInit, Input, Inject } from '@angular/core';
import { StatutVisiteMaintenance } from 'app/Enums/Statut/StatutVisiteMaintenance';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppSettings } from 'app/app-settings/app-settings';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { VisiteMaintenance } from 'app/Models/Entities/Maintenance/ContratEntretien';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { moisEnum } from 'app/Enums/Maintenance/mois.enum';
import { Color } from 'app/Enums/color';
import { GlobalInstances } from 'app/app.module';
export declare var swal: any;

@Component({
	selector: 'app-show-visite',
	templateUrl: './show.component.html',
	styleUrls: ['./show.component.scss']
})
export class ShowComponentVisiteMaintenance implements OnInit {
	dateLang: any;
	processing = false;
	adresses: Adresse[] = [];
	client: Client = new Client();
	visiteMaintenance: VisiteMaintenance = new VisiteMaintenance();
	statutVisiteMaintenance: typeof StatutVisiteMaintenance = StatutVisiteMaintenance;
	isPlanification = false;
	idContrat = null;
	labels: any = null;
	statuts: { id: number; label: string; color: string }[];
	// tslint:disable-next-line:no-input-rename
	@Input('idContratVisite') idContratVisite = null;
	@Input('year') yearVisite = null;
	@Input('month') monthVisite = null;
	idContratRoute
	Year
	Month
	id;
	ListGammeEquipement = [];
	date;
	listVisite: VisiteMaintenance[] = [];
	listObservation = [];
	treeColapse = [];
	color: typeof Color = Color;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	async ngOnInit() {

		this.init();
		this.route.params.subscribe(

			async params => {
				this.init();
			}
		);
	}

	NavigateDetailsVisiteMaintenance(visite: VisiteMaintenance) {
		if (visite.status === StatutVisiteMaintenance.Unplanned && visite.contractId != undefined) {
			let url = `/visitesMaintenance/detail/${visite.contractId}/${visite.year}/${visite.month}`;
			GlobalInstances.router.navigate([url]);

		} else {
			let url = `/visitesMaintenance/detail/${visite.id}`;
			GlobalInstances.router.navigate([url]);

		}
	}

	refreshVisite(Visite) {
		this.NavigateDetailsVisiteMaintenance(Visite);
		this.id = Visite.id;
		this.refresh();
	}

	init() {
		this.processing = true;
		this.selectLanguage();
		this.translate.get('labels').subscribe(text => {
			this.labels = text;
		});
		if (this.idContratVisite != null) {
			//	this.id = this.idVisite;
			this.isPlanification = true;
		} else {
			this.route.params.subscribe(async params => {

				this.id = params['id'];
			});
			this.route.params.subscribe(async params => {
				this.idContratRoute = params['idContratVisite'];
				this.Year = params['Year'];
				this.Month = params['month'];
			});
		}

		this.route.params.subscribe(params => {
			this.idContrat = params['idContrat'];
		});
		this.refresh();
		this.translate.get('statuts').subscribe((statuts: { id: number; label: string; color: string }[]) => this.statuts = statuts);
		// await this.refresh();
		this.processing = false;
	}

	visiteMaintenanceById(iContrat, year, month): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getAll(ApiUrl.VisitMaintenance + '/contract/' + iContrat + '/' + month + '/' + year)
				.subscribe(res =>
					resolve(res.value), err => reject(err))
		});
	}

	getVisiteMaintenence(id): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.VisitMaintenance, this.id)
				.subscribe(res =>
					resolve(res.value), err => reject(err))
		});
	}



	async refresh() {
		if (this.Year != null) {
			this.visiteMaintenance = await this.visiteMaintenanceById(this.idContratRoute, this.Year, this.Month)
		}
		if (this.idContratVisite != null && this.yearVisite != null && this.monthVisite != null) {
			this.visiteMaintenance = await this.visiteMaintenanceById(this.idContratVisite, this.yearVisite, this.monthVisite)

		}
		if (this.id != null) {
			this.visiteMaintenance = await this.getVisiteMaintenence(this.id);
		}

		if (this.visiteMaintenance !== undefined && Object.keys(this.visiteMaintenance).length > 0) {
			this.ListGammeEquipement = this.visiteMaintenance.equipmentDetails !== undefined ? this.visiteMaintenance.equipmentDetails : null;
			await this.displayFormatDate();
		}
	}

	selectLanguage(): void {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('datePicker').subscribe(text => {
			this.dateLang = text;
		});
	}

	getColor(status) { return this.color[status] };

	getLabelleByStatut(statut: StatutVisiteMaintenance) {

		if (this.labels == null) {
			return
		}
		switch (statut) {
			case this.statutVisiteMaintenance.Unplanned:
				return this.labels.aplanifier;
				break;
			case this.statutVisiteMaintenance.Planned:
				return this.labels.planifier;
				break;
		}
	}

	naigateToListVisiteMaintenance() {
		const url = this.idContrat == null ? `/visitesMaintenance` : `/contratentretiens/detail/${this.idContrat}/`;
		this.router.navigate([url]);
	}

	getObservations(id) {
		if (this.listObservation.length === 0) {
			return null;
		} else {
			const observationInList = this.listObservation.filter(x => x.id === id)[0];
			return observationInList !== undefined ? (observationInList.observations == null ? null : observationInList.observations) : null;
		}
	}
	async displayFormatDate() {
		const selector = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout',
			'septembre', 'octobre', 'novembre', 'decembre'];
		const translate = await this.getTranslationByKey('mois')
		//translate[selector[this.visiteMaintenance.month - 1]]
		const formatDate = this.getMonth(this.visiteMaintenance.month) + ' ' + this.visiteMaintenance.year;
		this.date = formatDate;
	}

	getTranslationByKey(key: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get(key).subscribe(translation => {
				resolve(translation);
			});
		});
	}

	openElement(index) {
		if (this.treeColapse.filter(x => x.index === index).length === 0) {
			this.treeColapse.push({
				isOpen: true,
				index: index,
				childs: []
			});
		} else {
			this.treeColapse = this.treeColapse.map(x => {
				if (x.index === index) {
					x.isOpen = !x.isOpen
				}
				return x;
			});
		}
	}
	openChild(indexElemnet, indexChild) {

		if (this.treeColapse[indexElemnet].childs.filter(x => x.index === indexChild).length === 0) {
			this.treeColapse[indexElemnet].childs.push({
				isOpen: true,
				index: indexChild,
			});
		} else {
			this.treeColapse = this.treeColapse.map(x => {
				if (x.index === indexElemnet) {
					x.childs = x.childs.map(child => {
						if (child.index === indexChild) {
							child.isOpen = !child.isOpen;
						}
						return child;
					});
				}
				return x;
			});
		}
	}
	checkElementIsOpen(indexElemnet) {
		const element = this.treeColapse.filter(x => x.index === indexElemnet);
		return element.length === 0 ? false : element[0].isOpen;
	}

	checkChildIsOpen(indexElemnet, indexChild) {
		if (this.treeColapse.filter(x => x.index === indexElemnet && x.childs.filter(y => y.index === indexChild).length !== 0).length === 0) {
			return false;
		};
		return this.treeColapse.filter(x => x.index === indexElemnet)[0].childs.filter(y => y.index === indexChild)[0].isOpen
	}


	jsonParse(data) {
		try {
			return JSON.parse(data);
		} catch (err) { }
	}
	getMonth(mois) {
		const selector = ['janvier', 'fevrier', 'mars', 'avril', 'mai', 'juin', 'juillet',
			'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
		switch (mois) {
			case moisEnum.January:
				return 'Janvier';
				break;
			case moisEnum.February:
				return 'Février';
				break;
			case moisEnum.March:
				return 'Mars';
				break;
			case moisEnum.April:
				return 'Avril';
				break;
			case moisEnum.May:
				return 'Mai';
				break;
			case moisEnum.June:
				return 'Juin';
				break;
			case moisEnum.July:
				return 'Juillet';
				break;
			case moisEnum.August:
				return 'Août';
				break;
			case moisEnum.September:
				return 'Septembre';
				break;
			case moisEnum.October:
				return 'Octobre';
				break;
			case moisEnum.November:
				return 'Novembre';
				break;
			case moisEnum.December:
				return 'Décembre';
				break;
		}
	}


}
