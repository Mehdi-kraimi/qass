import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsFicheInterventionComponent } from './details-ficheIntervention.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MultiLoader } from '../ficheIntervention.module';
import { MatTabsModule, MatMenuModule, MatIconModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { FicheFicheInterventionComponent } from './fiche-ficheIntervention/fiche-ficheIntervention.component';
import { CommonModules } from 'app/common/common.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { SplitButtonModule } from 'app/custom-module/primeng/primeng';
import { SelectUserModule } from 'app/common/select-user/select-user.module';
import { TableArticleModule } from 'app/common/table-article/table-article.module';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { AddChantierFormModule } from 'app/common/chantier-form/add-chantier-form.module';
import { ActionsModule } from 'app/shared/actions/actions.module';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import {
	NgxMatDatetimePickerModule,
	NgxMatNativeDateModule,
	NgxMatTimepickerModule
} from '@angular-material-components/datetime-picker';

@NgModule({
	declarations: [
		DetailsFicheInterventionComponent,
		FicheFicheInterventionComponent
	],
	imports: [
		CommonModule,
		MatTabsModule,
		MatMenuModule,
		MatIconModule,
		SelectUserModule,
		NgxMatDatetimePickerModule,
        NgxMatTimepickerModule,
        NgxMatNativeDateModule,
		TableArticleModule,
		InputTextareaModule,
		AddChantierFormModule,
		CommonModules,
		NgbTooltipModule,
		SplitButtonModule,
		ReactiveFormsModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		RouterModule.forChild([
			{
				path: '',
				component: DetailsFicheInterventionComponent,
			}
		]),
		ActionsModule,

	],
	providers: [],
	entryComponents: []
})
export class DetailsFicheInterventionModule { }
