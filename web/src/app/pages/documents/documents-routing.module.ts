import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditDocumentComponent } from './details-document/edit-document/edit-document.component';
import { AddFactureSituationComponent } from '../../components/add-facture-situation/add-facture-situation.component';
import { AddFactureAcompteComponent } from '../../components/add-facture-acompte/add-facture-acompte.component';

export const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				pathMatch: 'full',
				loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
			},
			{
				path: 'detail/:id',
				loadChildren: './details-document/details-document.module#DetailsDocumentModule'
			},
			{
				path: 'create',
				component : EditDocumentComponent
			},
			{
				path: 'generer-create/:id',
				component : EditDocumentComponent
			},
			{
				path: 'dupliquer-create/:id',
				component : EditDocumentComponent
			},
			{
				path: 'edit/:id',
				component: EditDocumentComponent
			},
			{
				path: 'factureSituation/:id',
				component: AddFactureSituationComponent,
				data: {
					title: 'Ajouter Facture Situation'
				}
			},
			{
				path: 'factureAcompte/:id',
				component: AddFactureAcompteComponent,
				data: {
					title: 'Ajouter Facture Acompte'
				}
			},
			{
				path: 'factureSituationCloture/:id',
				loadChildren: './../../components/add-facture-cloture/add-facture-cloture.module#AddFactureClotureModule',
			},
			{
				path: ':idDevis/factures/detail/:id',
				loadChildren: './details-document/details-document.module#DetailsDocumentModule'
			},
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class DocumentsRoutingModule { }
