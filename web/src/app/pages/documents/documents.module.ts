import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { DocumentsRoutingModule } from './documents-routing.module';
import { MatTabsModule, MatDatepickerModule, MatNativeDateModule, MatSelectModule, MatCheckboxModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EditDocumentComponent } from './details-document/edit-document/edit-document.component';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { CommonModules } from 'app/common/common.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { AddChantierFormModule } from 'app/common/chantier-form/add-chantier-form.module';
import { InputTextareaModule } from 'app/common/input-textarea/input-textarea.module';
import { TableArticleModule } from 'app/common/table-article/table-article.module';
import { SplitButtonModule, CalendarModule } from 'app/custom-module/primeng/primeng';
import { TableArticleDepenseModule } from 'app/common/table-article-depense/table-article-depense.module';
import { TableArticleFactureAcompteModule } from 'app/common/table-article-facture-acompte/table-article-facture-acompte.module';
import { SliderModule } from 'app/common/slider/slider.module';
import { AddFactureClotureModule } from 'app/components/add-facture-cloture/add-facture-cloture.module';
import { SharedModule } from 'app/shared/shared.module';
import { FournisseurCardModule } from 'app/shared/fournisseur-card/fournisseur-card.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

export function MultiLoader(http: HttpClient) {
	return new MultiTranslateHttpLoader(http, [
		{ prefix: './assets/i18n/prestation/', suffix: '.json' },
		{ prefix: './assets/i18n/shared/', suffix: '.json' },
		{ prefix: './assets/i18n/chantier/', suffix: '.json' },
		{ prefix: './assets/i18n/status/', suffix: '.json' },
		{ prefix: './assets/i18n/selectProduit/', suffix: '.json' },

	]);
}


@NgModule({
	providers: [
		PreviousRouteService],
	imports: [
		CommonModule,
		FormsModule,
		DocumentsRoutingModule,
		AddChantierFormModule,
		SplitButtonModule,
		TableArticleDepenseModule,
		TableArticleFactureAcompteModule,
		InputTextareaModule,
		NgxMatSelectSearchModule,
		FournisseurCardModule,
		TableArticleModule,
		MatTabsModule,
		MatDatepickerModule,
		MatNativeDateModule,
		ReactiveFormsModule,
		MatSelectModule,
		SharedModule,
		MatCheckboxModule,
		NgbTooltipModule,
		CommonModules,
		CalendarModule,
		SliderModule,
		AddFactureClotureModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: MultiLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
	],
	exports: [EditDocumentComponent],
	declarations: [EditDocumentComponent]
})

export class DocumentsModule { }

