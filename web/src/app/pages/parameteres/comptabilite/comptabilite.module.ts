import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModules } from 'app/common/common.module';
import { ComptabiliteComponent } from './comptabilite.component';
import { CalendarModule } from 'app/custom-module/primeng/primeng';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { DialogModule } from 'primeng/dialog';

import { ParametrageCompteComponent } from './parametrage-compte/parametrage-compte.component';
import { ClassificationVenteComponent } from './classification-vente/classification-vente.component';
import { ClassificationDepenseComponent } from './classification-depense/classification-depense.component';
import { MatDatepickerModule, MatExpansionModule, MatMenuModule, MatNativeDateModule, MatSelectModule, MatTabsModule } from '@angular/material';

export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/parametres/', '.json');
}

@NgModule({
	providers: [],
	declarations: [
		ComptabiliteComponent,
		ParametrageCompteComponent,
		ClassificationVenteComponent,
		ClassificationDepenseComponent,
	],
	imports: [
		CommonModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		ReactiveFormsModule,
		CommonModules,
		DataTablesModule,
		NgSelectModule,
		NgbTooltipModule,
		CalendarModule,
		DynamicDialogModule,
		DialogModule,
		NgSelectModule,
		MatSelectModule,
		MatExpansionModule,
		MatTabsModule,
		MatMenuModule,
		MatExpansionModule,
		MatTabsModule,
		MatMenuModule,
		MatDatepickerModule,
		MatNativeDateModule,

	]
})
export class ParametrageComptabiliteModule { }
