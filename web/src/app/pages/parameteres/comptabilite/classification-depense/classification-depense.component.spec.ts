import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassificationDepenseComponent } from './classification-depense.component';

describe('ClassificationDepenseComponent', () => {
  let component: ClassificationDepenseComponent;
  let fixture: ComponentFixture<ClassificationDepenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassificationDepenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassificationDepenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
