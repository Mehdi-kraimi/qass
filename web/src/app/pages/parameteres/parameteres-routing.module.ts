import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NumerotationPrefixeComponent } from './numerotation-prefixe/numerotation-prefixe.component';
import { PrixComponent } from './prix/prix.component';
// import { CategorieComponent } from './Categorie/categorie.component';
import { TvaComponent } from './tva/tva.component';
import { MessagerieComponent } from './messagerie/messagerie.component';
import { HoraireTravailComponent } from './horaire-travail/horaire-travail.component';
import { SyncAgendaGoogleComponent } from './sync-agenda-google/sync-agenda-google.component';
import { ParametrageDocumentComponent } from './parametrage-document/parametrage-document.component';
import { ComptabiliteComponent } from './comptabilite/comptabilite.component';
import { ObjectifsCaComponent } from './objectifs-ca/objectifs-ca.component';
import { GenerateContractComponent } from './generate-contract/generate-contract.component';
import { ParametragePdfComponent } from './parametrage-pdf/parametrage-pdf.component';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { AgendaComponent } from './agenda/agenda.component';


const routes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'numerotationPrefixe',
				component: NumerotationPrefixeComponent
			},
			{
				path: 'messagerie',
				component: MessagerieComponent
			},
			{
				path: 'prix',
				component: PrixComponent
			},
			{
				path: 'labels',
				loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
				data: {
					docType: ApiUrl.configurationLabel
				}
			},
			{
				path: 'tva',
				component: TvaComponent
			},
			{
				path: 'typedocument',
				loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
				data: {
					docType: ApiUrl.configDocumentType
				}
			},
			{
				path: 'modelregelement',
				loadChildren: './../../components/form-data/index-list/index.module#ListIndexModule',
				data: {
					docType: ApiUrl.configModeRegelemnt
				}
			},
			{
				path: 'parametrageDocument',
				component: ParametrageDocumentComponent
			},
			{
				path: 'horairetravail',
				component: HoraireTravailComponent
			},
			{
				path: 'synagenda',
				component: SyncAgendaGoogleComponent
			},
			{
				path: 'comptabilite',
				component: ComptabiliteComponent
			},
			{
				path: 'objectifsca',
				component: ObjectifsCaComponent
			},
			{
				path: 'editique',
				component: GenerateContractComponent
			},
			{
				path: 'agenda',
				component: AgendaComponent
			},
			{
				path: 'parametragePdf',
				component: ParametragePdfComponent
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

export class ParameteresRoutingModule { }
