import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from "angular-datatables";
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from "@ng-bootstrap/ng-bootstrap";
import { CommonModules } from 'app/common/common.module';
import { NumerotationPrefixeComponent } from './numerotation-prefixe.component';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { CustomMissingTranslationHandler } from 'app/libraries/translation-handler';
import { MatDatepickerModule, MatExpansionModule, MatSelectModule } from '@angular/material';


// export function createTranslateLoader(http: HttpClient) {
//     return new TranslateHttpLoader(http, "./assets/i18n/parametres/", ".json");
// }

export function Loader(http: HttpClient) {
    return new MultiTranslateHttpLoader(http, [
        { prefix: './assets/i18n/parametres/', suffix: '.json' },
        { prefix: './assets/i18n/shared/', suffix: '.json' },
    ]);
}

@NgModule({
    providers: [],
    declarations: [NumerotationPrefixeComponent],
    imports: [
        CommonModule,
        // TranslateModule.forChild({
        //     loader: {
        //         provide: TranslateLoader,
        //         useFactory: createTranslateLoader,
        //         deps: [HttpClient]
        //     },
        //     isolate: true
        // }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: Loader,
                deps: [HttpClient]
            },
            missingTranslationHandler: { provide: MissingTranslationHandler, useClass: CustomMissingTranslationHandler },
            isolate: true
        }),
        FormsModule,
        ReactiveFormsModule,
        CommonModules,
        DataTablesModule,
        NgSelectModule,
        NgbTooltipModule,
        MatDatepickerModule,
        NgSelectModule,
        MatSelectModule,
        MatExpansionModule,
    ]
})
export class NumerotationPrefixeModule { }
