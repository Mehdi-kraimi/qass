import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';
declare var toastr: any;

@Component({
	selector: 'app-sync-agenda-google',
	templateUrl: './sync-agenda-google.component.html',
	styleUrls: ['./sync-agenda-google.component.scss']
})
export class SyncAgendaGoogleComponent implements OnInit {
	form: any;
	loading;
	oldCalendarId;
	emailToShareWith = AppSettings.emailToShareWith;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private fb: FormBuilder,
		private header: HeaderService
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);
		this.form = this.fb.group({
			calendarId: [''],
		})
	}

	ngOnInit() {
		this.prepareBreadcrumb();
		this.GetParametrageAgendaGoogle();

	}
	prepareBreadcrumb(): void {
		this.translate.get(`synagenda.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('synagenda.title'), route: '/parameteres/synagenda' }
			];
		});
	}
	GetParametrageAgendaGoogle() {
		this.service.getAll(ApiUrl.configGoogleConfig).subscribe(res => {
			const data = JSON.parse(res);
			this.oldCalendarId = data['calendarId'];
			this.form.controls['calendarId'].setValue(data['calendarId']);
		});
	}

	update() {
		if (this.oldCalendarId !== this.form.value.calendarId) {
			this.service.updateAll(ApiUrl.configGoogleConfig, { value: JSON.stringify(this.form.value) }).subscribe(res => {
				this.translate.get('labels').subscribe(text => {
					toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
				// this.GetParametrageAgendaGoogle();
			})
		} else {
			this.translate.get('labels').subscribe(text => {
				toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		}
	}
}
