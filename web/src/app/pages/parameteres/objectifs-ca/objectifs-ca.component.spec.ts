import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectifsCaComponent } from './objectifs-ca.component';

describe('ObjectifsCaComponent', () => {
  let component: ObjectifsCaComponent;
  let fixture: ComponentFixture<ObjectifsCaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectifsCaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectifsCaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
