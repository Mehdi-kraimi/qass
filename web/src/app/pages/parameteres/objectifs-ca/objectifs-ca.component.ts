import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ObjectifsCaModel } from 'app/Models/Entities/Parametres/objectifs-ca';
import { Component, OnInit, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';

@Component({
	selector: 'app-objectifs-ca',
	templateUrl: './objectifs-ca.component.html',
	styleUrls: ['./objectifs-ca.component.scss']
})
export class ObjectifsCaComponent implements OnInit {
	caMonths
	caYears;
	objectifsGlobal = 0
	indexObjetGlobal = null;
	isOpenGlobal = false;
	currentYears: number = null;
	listObjectCa: ObjectifsCaModel[] = [];
	isOpen = false;
	indexObjet = null
	indexCA = null;
	isOpenYears = false;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private header: HeaderService
	) {
		TranslateConfiguration.setCurrentLanguage(this.translate);


	}

	prepareBreadcrumb(): void {
		this.translate.get(`objet.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('objet.title'), route: '/parameteres/objectifsca' }
			];
		});
	}
	ngOnInit() {
		this.prepareBreadcrumb();
		this.currentYears = (new Date()).getFullYear();
		this.getAllObjectCA();
	}

	getAllObjectCA() {
		this.service.getAll(ApiUrl.configurationGoals).subscribe(res => {
			this.listObjectCa = res.value;
		})
	}


	editChiifreAffaire(chiffre, indexChiffre, indecObjectifs) {
		this.caMonths = chiffre.goal;
		this.indexObjet = indecObjectifs;
		this.indexCA = indexChiffre;
		this.isOpen = true;
	}

	conditionOpen(indexChiffre, indecObjectifs) {
		if (!this.isOpen) {
			return false;
		}
		if (this.isOpen && (indexChiffre === this.indexCA && indecObjectifs === this.indexObjet)) {
			return true
		}
		if (this.isOpen && (indexChiffre !== this.indexCA && indecObjectifs === this.indexObjet)) {
			return false
		}
		if (this.isOpen && (indexChiffre === this.indexCA && indecObjectifs !== this.indexObjet)) {
			return false
		} else {
			return false;
		}
	}


	editCAffaireyEARS(object, indexObjectifs) {
		this.caYears = object.goal;
		this.indexObjet = indexObjectifs;
		this.isOpenYears = true;
	}

	openYears(indecObjectifs) {
		if (!this.isOpenYears) {
			return false;
		}
		if (this.isOpenYears && indecObjectifs === this.indexObjet) {
			return true
		} else {
			return false;
		}
	}

	updateCaYears(indexObjectifs) {
		this.listObjectCa[indexObjectifs].goal = this.caYears;
		this.isOpenYears = false;
		const objectifUpdate = this.listObjectCa[indexObjectifs] as ObjectifsCaModel;
		this.service.updateAll(ApiUrl.configurationGoals, objectifUpdate).subscribe(res => {
		})

	}
	update(indexChiffre, indecObjectifs) {
		this.listObjectCa[indecObjectifs].monthlyGoals[indexChiffre].goal = this.caMonths;
		this.isOpen = false;
		const objectifUpdate = this.listObjectCa[indecObjectifs] as ObjectifsCaModel;
		this.service.updateAll(ApiUrl.configurationGoals, objectifUpdate).subscribe(res => {
		})

	}

	addObjectGlobal(indexObjectifs) {
		this.indexObjetGlobal = indexObjectifs;
		this.isOpenGlobal = true;
	}

	AddObjectifsGlobal(indexObjectifs) {
		if (!this.isOpenGlobal && this.listObjectCa[indexObjectifs].id === this.currentYears) {
			return true;
		} else {
			return false;
		}
	}
	ObjectifsGlobal(indexObjectifs) {
		if (this.isOpenGlobal && this.listObjectCa[indexObjectifs].id === this.currentYears) {
			return true;
		} else {
			return false;
		}
	}

	updateObjectifs(indexObjectifs) {

		this.listObjectCa[indexObjectifs].monthlyGoals.forEach(element => {
			element.goal = this.objectifsGlobal;
			element.month = element.month;
		});
		this.listObjectCa[indexObjectifs].monthlyGoals.map(element => {
			// tslint:disable-next-line:radix
			return element.goal = parseInt(this.objectifsGlobal.toString());
		})
		this.isOpenGlobal = false;
		const objectifUpdate = this.listObjectCa[indexObjectifs] as ObjectifsCaModel;

		this.service.updateAll(ApiUrl.configurationGoals, objectifUpdate).subscribe(res => {
		})
	}

	annulerObjectifs() {
		this.objectifsGlobal = 0;
		this.isOpenGlobal = false;
	}

	annulerModification() {
		this.isOpen = false;
		this.caMonths = null;
		this.caYears = null;
		this.isOpenYears = null;
	}

}
