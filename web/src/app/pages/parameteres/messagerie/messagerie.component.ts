import { Component, OnInit, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { HeaderService } from 'app/services/header/header.service';
import { TranslateConfiguration } from 'app/libraries/translation';

declare var toastr: any;

@Component({
	selector: 'app-messagerie',
	templateUrl: './messagerie.component.html',
	styleUrls: ['./messagerie.component.scss']
})
export class MessagerieComponent implements OnInit {

	form;
	data;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private header: HeaderService,
		private translate: TranslateService, private fb: FormBuilder) {
		TranslateConfiguration.setCurrentLanguage(this.translate);

		this.form = this.fb.group({
			'UserName': ['', [Validators.required]],
			'Password': ['', [Validators.required]],
			'Server': ['', [Validators.required, Validators.pattern(AppSettings.regexURL)]],
			'Port': ['', [Validators.required]],
			'SSL': [false, [Validators.required]],
		})
	}

	ngOnInit() {
		this.prepareBreadcrumb();
		this.GetMessagerie();
	}


	prepareBreadcrumb(): void {
		this.translate.get(`messagerie.`).toPromise().then((translation: string) => {
			this.header.breadcrumb = [
				{ label: this.translate.instant('messagerie.config'), route: '/parameteres/messagerie' }
			];
		});
	}
	GetMessagerie() {
		this.service.getAll(ApiUrl.configMessagerie).subscribe(res => {
			this.data = JSON.parse(res);
			this.form.patchValue(this.data);
		});
	}

	get f() { return this.form.controls; }

	update() {
		if (this.form.valid) {
			this.data = this.form.value;
			this.service.updateAll(ApiUrl.configMessagerie, { value: JSON.stringify(this.data) }).subscribe(res => {

				this.translate.get('labels').subscribe(text => {
					toastr.success('', text.modifierSuccess, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
				this.GetMessagerie();
			})
		}
	}

}
