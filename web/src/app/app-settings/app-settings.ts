import { HttpHeaders } from '@angular/common/http';
import { User } from 'app/Models/Entities/User';
import { RequestType } from 'app/Enums/Configuration/request-type.enum';
import { DatePipe } from '@angular/common';

export class AppSettings {

	// public static API_ENDPOINT = 'http://demo.artinove.net/Axiobat_API/api/';

	// public static API_ENDPOINT = 'http://demo.artinove.net/AXIOBAT-TEST-API/api/';

	// My docker container link from azure server  

	// public static API_ENDPOINT = 'https://yes-axiobatapi.azurewebsites.net/api/';

	public static API_ENDPOINT = 'http://ec2-35-180-209-109.eu-west-3.compute.amazonaws.com/api/';

	// public static API_ENDPOINT = 'https://demo.foliatech.app/Axiobat-API/api/';
	// public static API_ENDPOINT = 'https://ets-borel-api.axiobat.com/api/';

	/* User connecter */
	static user: User = new User();

	/* La langue de site web */
	static lang = 'fr';

	/* Pagination par default */
	static SIZE_PAGE = 10;
	static PAGE_SIZE_OPTIONS = [10, 25, 50];
	static DEFAULT_PAGE_SIZE = 10;

	/* Maximum données obtenir à partir de l'api */
	static MAX_GET_DATA = 5000000;

	/* Contient les ids des pays à des villes */
	static SHOW_CITY_OF_COUNTRY = [75]

	/** NBR ITEM CHARGER BY PAGE */
	static NBR_ITEM_PER_PAGE = 30

	/*Email To Share Google Calendar*/
	static emailToShareWith = 'axiobat@axiobat-275512.iam.gserviceaccount.com';
	/*------------------------------------------------------------------*/
	/*                  Regular expression                              */
	/*------------------------------------------------------------------*/
	static regexURL = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
	static regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	static regexPhone = /^\+?\d*$/;
	/* Name variable in token */
	static SELECTED_PRODUIT = 'selected_products'

	// public static API_ENDPOINT = 'https://demo.foliatech.app/batinoveapi/api/';

	// static RequestOptions() {
	//     return {
	//         headers: new HttpHeaders({
	//             'Content-Type': 'application/json',
	//             'Authorization': localStorage.getItem("token") != null ? localStorage.getItem("token") : ""
	//         })
	//     };
	// }

	static RequestOptions(requestType?: RequestType) {
		return {
			headers: new HttpHeaders({
				'Content-Type': requestType,
				// tslint:disable-next-line: object-literal-key-quotes
				'Authorization': localStorage.getItem('token') != null ? 'Bearer '
					+ localStorage.getItem('token') : ''
			})
		};
	}

	/*------------------------------------------------------------------*/
	/*                  Commun Function                                 */
	/*------------------------------------------------------------------*/
	static downloadBase64(data, fileName, fileType, extension) {
		let buffer, blob, url, a;
		if (fileType.includes('application/octet-stream')) {
			fileType = 'application/' + extension;
		}

		var binary_string = window.atob(data.split(',')[1]);
		var len = binary_string.length;
		var bytes = new Uint8Array(len);
		for (var i = 0; i < len; i++) {
			bytes[i] = binary_string.charCodeAt(i);
		}
		buffer = bytes.buffer;

		blob = new Blob([buffer], { type: fileType });

		// Set view.
		if (blob) {
			// Read blob.
			url = window.URL.createObjectURL(blob);

			// Create link.
			a = document.createElement('a');
			// Set link on DOM.
			document.body.appendChild(a);
			// Set link's visibility.
			a.style = 'display: none';
			// Set href on link.
			a.href = url;
			// Set file name on link.
			a.download = fileName;

			// Trigger click of link.
			a.click();

			// Clear.
			window.URL.revokeObjectURL(url);
		}
	}

	// create file from binary file data and download it .
	public static printPdf(data, fileName, fileType, extension) {
		// Set objects for file generation.
		var blob;
		// Get time stamp for fileName.
		var stamp = new Date().getTime();

		// Set MIME type and encoding.
		fileType = (fileType || 'application/pdf');

		// Set file name.
		fileName = (fileName || stamp + '.' + extension);

		// Set data on blob.
		blob = new Blob([data], { type: fileType });

		// Set view.
		if (blob) {
			//
			return window.URL.createObjectURL(blob);
		} else {
			// Handle error.
		}
		return data;
	}
	static guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}

	static ConvertEmptyValueToNull(values) {
		for (var i in values) {
			if (values[i] === '') {
				values[i] = null
			}
		}
		return values;
	}

	static generateStampHM() {
		const convertDate = new Date();
		return convertDate.getHours() + '' + convertDate.getMinutes() + '' + convertDate.getSeconds();
	}

	/**
	 * FORMATER DATETIME
	 */
	static formaterDatetime(date: string) {
		const convertDate = new Date(date);
		const year = convertDate.getFullYear()
		const month = (convertDate.getMonth() + 1).toString().length === 1 ? '0' + (convertDate.getMonth() + 1) : (convertDate.getMonth() + 1)
		const day = convertDate.getDate().toString().length === 1 ? '0' + convertDate.getDate() : convertDate.getDate()
		const heure = convertDate.getHours().toString().length === 1 ? '0' + convertDate.getHours() : convertDate.getHours();
		const minutes = convertDate.getMinutes().toString().length === 1 ? '0' + convertDate.getMinutes() : convertDate.getMinutes();
		return year + '-' + month + '-' + day + 'T' + heure + ':' + minutes;
	}
	/**
	 * FORMATER DATETIME
	 */
	static formaterNotTime(date: string) {
		const convertDate = new Date(date);
		const year = convertDate.getFullYear()
		const month = (convertDate.getMonth() + 1).toString().length === 1 ? '0' + (convertDate.getMonth() + 1) : (convertDate.getMonth() + 1)
		const day = convertDate.getDate().toString().length === 1 ? '0' + convertDate.getDate() : convertDate.getDate()
		const heure = convertDate.getHours().toString().length === 1 ? '0' + convertDate.getHours() : convertDate.getHours();
		const minutes = convertDate.getMinutes().toString().length === 1 ? '0' + convertDate.getMinutes() : convertDate.getMinutes();
		return year + '-' + month + '-' + day;
	}
	static formaterDatewithTime(date: string, time) {
		const convertDate = new Date(date);
		const hoursMinue = time.split(':');
		const year = convertDate.getFullYear()
		const month = (convertDate.getMonth() + 1).toString().length === 1 ? '0' + (convertDate.getMonth() + 1) : (convertDate.getMonth() + 1)
		const day = convertDate.getDate().toString().length === 1 ? '0' + convertDate.getDate() : convertDate.getDate();
		var heure;
		var minutes;
		if (time !== '') {
			heure = hoursMinue[0];
			minutes = hoursMinue[1];
		} else {
			heure = convertDate.getHours().toString().length === 1 ? '0' + convertDate.getHours() : convertDate.getHours();
			minutes = convertDate.getMinutes().toString().length === 1 ? '0' + convertDate.getMinutes() : convertDate.getMinutes();
		}

		return year + '-' + month + '-' + day + 'T' + heure + ':' + minutes;
	}


	static formatDate(date) {
		return new Date(date).toISOString()
		// return new DatePipe('fr').transform(date, 'yyyy/mm/dd');
	}

	static _base64ToArrayBuffer(base64) {
		var binary_string = window.atob(base64);
		var len = binary_string.length;
		var bytes = new Uint8Array(len);
		for (var i = 0; i < len; i++) {
			bytes[i] = binary_string.charCodeAt(i);
		}
		return bytes.buffer;
	}

	// create file from binary file data and download it .
	static setFile(data, fileName, fileType, extension) {
		// Set objects for file generation.
		var blob, url, a;
		// Get time stamp for fileName.
		var stamp = new Date().getTime();

		// Set MIME type and encoding.
		fileType = (fileType || 'application/pdf');

		// Set file name.
		fileName = (fileName || 'FCCClient_' + stamp + '.' + extension);

		// Set data on blob.
		blob = new Blob([data], { type: fileType });

		// Set view.
		if (blob) {
			// Read blob.
			url = window.URL.createObjectURL(blob);

			// Create link.
			a = document.createElement('a');
			// Set link on DOM.
			document.body.appendChild(a);
			// Set link's visibility.
			a.style = 'display: none';
			// Set href on link.
			a.href = url;
			// Set file name on link.
			a.download = fileName;

			// Trigger click of link.
			a.click();

			// Clear.
			window.URL.revokeObjectURL(url);
		} else {
			// Handle error.
		}
		return data;
	}

	/**
	 * Formater Number
	 */
	static formaterNumber(montant) {
		return parseFloat(Number(montant).toFixed(2))
	}

	static objectSize(obj) {
		let size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}	/***
	 * compare two  date
	 */
	static compareDate(date1, date2) {

		const dateOne = new Date(date1);
		const dateTwo = new Date(date2);
		if (dateOne > dateTwo) {
			return true;
		} else {
			return false;
		}
	}

	static cloneObject = <T>(target: T): T => {
		if (target === null) {
			return target;
		}
		if (target instanceof Date) {
			return new Date(target.getTime()) as any;
		}
		if (target instanceof Array) {
			const cp = [] as any[];
			(target as any[]).forEach((v) => { cp.push(v); });
			return cp.map((n: any) => AppSettings.cloneObject<any>(n)) as any;
		}
		if (typeof target === 'object' && target !== {}) {
			const cp = { ...(target as { [key: string]: any }) } as { [key: string]: any };
			Object.keys(cp).forEach(k => {
				cp[k] = AppSettings.cloneObject<any>(cp[k]);
			});
			return cp as T;
		}
		return target;
	}

}
