

// import { uuid } from 'uuidv4';

/**
 * an interface describe string helpers
 */
export class StringHelper {

    /**
     * check string is empty or null
     * @param value the value that we want to check
     */
	static isEmptyOrNull(value: string): boolean {
		return value === null || value === '' || value === undefined || value === 'null';
	}

	// static guid() {
	// 	return uuid();
	// }

    /**
     * safe navigate
     * @param val the value we want to check
     */
	static safeNavigate(val: string) {
		return StringHelper.isEmptyOrNull(val) ? '' : val;
	}

	/** check if data is array type or not */
	static hasdata(data) {
		if (Array.isArray(data)) {
			return data.length > 0;
		} else {
			return data != null || data !== 'null';
		}
	}
	/** check if data dashboard or not */
	static hasDashboarddata(data) {
		if (Array.isArray(data)) {
			return data.reduce((x, y) => x + y, 0) !== 0;
		} else {
			return data != null;
		}
	}
}
