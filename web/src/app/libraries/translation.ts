import { TranslateService } from '@ngx-translate/core';
import { LocalElements } from 'app/shared/utils/local-elements';


export class TranslateConfiguration {
	public static PATH_FILES = './assets/i18n/';
	public static FILE_EXTENTION = '.json';
	public static LANG = 'fr';
	public static EXISTING_LANGUAGES = ['fr', 'en', 'es'];

	public static async setCurrentLanguage(translate: TranslateService) {
		if (translate !== undefined) {
			let currentLanguage = localStorage.getItem(LocalElements.currentLanguage);

			currentLanguage = (currentLanguage !== 'undefined' && currentLanguage != null)
				? currentLanguage
				: TranslateConfiguration.LANG;

			TranslateConfiguration.LANG = currentLanguage;
			translate.setDefaultLang(currentLanguage);
			await translate.use(currentLanguage).toPromise();
		}
	}

	public static async setPdfLanguage(translate: TranslateService, langue: string) {
		translate.setDefaultLang(langue);
		await translate.use(langue).toPromise();
	}

	public static getCurrentLanguage() {
		const currentLanguage = localStorage.getItem(LocalElements.currentLanguage);
		return (typeof currentLanguage === 'string' && currentLanguage.length > 0) ?
			currentLanguage : TranslateConfiguration.LANG;
	}

	/**
   * @returns
   * EMPTY ==> false , cannot find translation
   * STRING ===> true , element translated
   */
	public static checkStringTranslated(translateInstant: TranslateService, stringToTranslate: string) {
		const result = translateInstant.instant(stringToTranslate);
		return result === stringToTranslate ? '' : result;
	}


}
