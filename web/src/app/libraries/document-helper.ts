// #region imports

import { GlobalInstances } from 'app/app.module';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';
import { Quote } from 'app/Models/Entities/Documents/Quote';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { StatutDevis } from 'app/Enums/Statut/StatutDevis';
import { StatutFacture } from 'app/Enums/Statut/StatutFacture.Enum';
import { TypeFacture } from 'app/Enums/TypeFacture.enum';
import { StatutAvoir } from 'app/Enums/Statut/StatutAvoir.Enum';
import { StatutBonCommandeFournisseur } from 'app/Enums/Statut/StatutBonCommandeFournisseur.Enum';
import { StatutDepense } from 'app/Enums/Statut/StatutDepense.Enum';
import { StatutContratEntretien } from 'app/Enums/Statut/StatutContratEntretien.Enum';
import { VisiteMaintenance } from 'app/Models/Entities/Maintenance/ContratEntretien';
import { StatutVisiteMaintenance } from 'app/Enums/Statut/StatutVisiteMaintenance';
import { StatutFicheInterventionMaintenance } from 'app/Enums/Statut/StatutFicheInterventionMain.enum';
import { StatutChantier } from 'app/Enums/Statut/StatutChantier.Enum';
import { StatutFicheIntervention } from 'app/Enums/Statut/StatutFicheIntervention.enum';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { FormGroup } from '@angular/forms';
import { Action } from 'app/Enums/action';
import { LocalElements } from 'app/shared/utils/local-elements';


// #endregion
declare var toastr: any;

// #region Status management
export class StatusManagement {

	public static getStatusKey(document: Partial<any>) {
		return document.status;
	}

	/**
	 * devis
	 * manage factured || factured (%) situation
	 */
	public static controlStatus(document, doctype) {
		var docStatus = document.status;
		// general
		if ((ApiUrl.Invoice === doctype || ApiUrl.Recurring === doctype
			|| ApiUrl.avoir === doctype || ApiUrl.Depense === doctype ||
			ApiUrl.MaintenanceContrat === doctype
			|| ApiUrl.BonCommandeF === doctype) && document.status === 'in_progress') {
			docStatus = 'encours';
		}
		if (ApiUrl.MaintenanceContrat === doctype && document.status === 'finished') { docStatus = 'finishedContrat'; }
		return docStatus;
	}
}
// #endregion

// #region Document helper
export class DocumentHelper {

	public static service;
	public static statutFacture: typeof StatutFacture = StatutFacture;

	public static articlesBnSupplier;

	public static getStatus(docType: string) {
		switch (docType) {
			case ApiUrl.Devis: return StatutDevis; break;
			case ApiUrl.Invoice: return StatutFacture; break;
			case ApiUrl.avoir: return StatutAvoir; break;
			case ApiUrl.BonCommandeF: return StatutBonCommandeFournisseur; break;
			case ApiUrl.Depense: return StatutDepense; break;
			case ApiUrl.MaintenanceContrat: return StatutContratEntretien; break;
			case ApiUrl.VisitMaintenance: return StatutVisiteMaintenance; break;
			case ApiUrl.MaintenanceOperationSheet: return StatutFicheInterventionMaintenance; break;
			case ApiUrl.Chantier: return StatutChantier; break;
			case ApiUrl.FicheIntervention: return StatutFicheIntervention; break;
		}
	}

	public static async createFacture(id, idChantier) {
		localStorage.setItem(LocalElements.docType, ApiUrl.Invoice);
		localStorage.setItem(LocalElements.docTypeGenerer, ApiUrl.Devis);
		const url = idChantier && idChantier != null ? `/chantiers/${idChantier}/documents/3/generer-create/${id}` : `/factures/generer-create/${id}`;
		GlobalInstances.router.navigate([url]);
	}

	public static GetDevis(id): Promise<Quote> {
		return new Promise((resolve, reject) => {
			DocumentHelper.service.getById(ApiUrl.Devis, id)
				.subscribe(
					res => resolve(res.value),
					error => reject(error)
				)
		});
	}

	public static navigateToFactureAcompte(id, idChantier) {
		const url = idChantier && idChantier != null ?
			`/chantiers/${idChantier}/documents/1/${id}/factureAcompte`
			: `devis/factureAcompte/${id}`;
		GlobalInstances.router.navigate([url]);
	}

	public static navigateToFactureSituation(id, idChantier) {
		const url = idChantier && idChantier != null ?
			`/chantiers/${idChantier}/documents/1/${id}/factureSituation`
			: `devis/factureSituation/${id}`;
		GlobalInstances.router.navigate([url]);
	}



	public static navigateToCloture(id, idChantier) {
		const url = idChantier && idChantier != null ?
			`/chantiers/${idChantier}/documents/1/${id}/factureSituationCloture`
			: `devis/factureSituationCloture/${id}`;
		GlobalInstances.router.navigate([url]);
	}

	public static async generateFicheIntervention(id) {
		localStorage.setItem(LocalElements.docType, ApiUrl.FicheIntervention);
		localStorage.setItem(LocalElements.docTypeGenerer, ApiUrl.Devis);
		GlobalInstances.router.navigate([`/ficheintervention/generer-create/${id}`]);
	}

	public static conditionCrudInvoice(facture) {
		if (facture.status === this.statutFacture.Encours && facture.totalTTC === facture.restToPay && facture.typeInvoice === TypeFacture.None) {
			return true
		}
		if (facture.status === this.statutFacture.Brouillon) {
			return true
		} else {
			return false
		}
	}

	public static async createFactureToMainteneContract(id) {
		localStorage.setItem(LocalElements.docType, ApiUrl.Invoice);
		localStorage.setItem(LocalElements.docTypeGenerer, ApiUrl.MaintenanceContrat);
		GlobalInstances.router.navigate([`/factures/generer-create/${id}`]);
	}

	public static async GetContrat(id) {
		const res = await this.service.getById(ApiUrl.MaintenanceContrat, id).toPromise();
		return res.value;
	}

	public static NavigateDetailsVisiteMaintenance(visite: VisiteMaintenance) {
		if (visite.status === StatutVisiteMaintenance.Unplanned && visite.contractId !== undefined) {
			const url = `/visitesMaintenance/detail/${visite.contractId}/${visite.year}/${visite.month}`;
			GlobalInstances.router.navigate([url]);

		} else {
			const url = `/visitesMaintenance/detail/${visite.id}`;
			GlobalInstances.router.navigate([url]);

		}
	}

	public static async createFactureinMOS(id) {
		localStorage.setItem(LocalElements.docType, ApiUrl.Invoice);
		localStorage.setItem(LocalElements.docTypeGenerer, ApiUrl.FicheInterventionMaintenance);
		GlobalInstances.router.navigate(['/factures/generer-create', id]);
	}

	public static GetFicheInterventionM(id): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.FicheInterventionMaintenance, id)
				.subscribe(
					res => resolve(res.value),
					error => reject(error)
				)
		});
	}

	public static GetFicheIntervention(id): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.FicheIntervention, id)
				.subscribe(
					res => resolve(res.value),
					error => reject(error)
				)
		});
	}

	public static async createFacturinFI(id) {
		localStorage.setItem(LocalElements.docType, ApiUrl.Invoice);
		localStorage.setItem(LocalElements.docTypeGenerer, ApiUrl.FicheIntervention)
		GlobalInstances.router.navigate(['/factures/generer-create', id]);
	}

	public static transfertFIToFacture(ficheIntervention) {
		const facture = new Invoice();
		facture.orderDetails = {
			'lineItems': ficheIntervention.orderDetails.lineItems,
			'proportion': 0,
			'puc': 0,
			'globalVAT_Value': null,

			'globalDiscount': {
				'value': 0,
				'type': TypeValue.Amount
			},
			'holdbackDetails': {
				'holdback': 0,
				'warrantyPeriod': 12,
				'warrantyExpirationDate': new Date()
			}
		}
		facture.workshopId = ficheIntervention.workshop !== null ? ficheIntervention.workshop.id : null;
		facture.addressIntervention = ficheIntervention.addressIntervention;
		facture.typeInvoice = TypeFacture.None;

		facture.workshop = ficheIntervention.workshop;
		facture.client = ficheIntervention.client;
		return facture
	}

}
// #endregion

// #region Edit document helper
export class EditDocumentHelper {

	public static form: FormGroup;
	public static typeAction: Action = Action.INFORMATION;

	public static getTypeAction() {
		switch (true) {
			case GlobalInstances.router.url.includes('fiche-'):
			case GlobalInstances.router.url.includes('detail'):
			case GlobalInstances.router.url.includes('paiements'):
			case GlobalInstances.router.url.includes('historique'):
			case GlobalInstances.router.url.includes('memos'): return Action.INFORMATION; break;
			case GlobalInstances.router.url.includes('edit'): return Action.MODIFIER; break;
			case GlobalInstances.router.url.includes('generer-'): return Action.GENERER; break;
			case GlobalInstances.router.url.includes('dupliquer-'): return Action.DUPLIQUER; break;
			case GlobalInstances.router.url.includes('create'): return Action.AJOUTER; break;
			// case GlobalInstances.router.url.
			// 	includes('preview'): return parseInt(localStorage.getItem(LocalElements.ACTION), 10) || Action.AJOUTER; break;
		}
	}

	public static isAction = (action: Action) => EditDocumentHelper.typeAction === action;


}
// #endregion

