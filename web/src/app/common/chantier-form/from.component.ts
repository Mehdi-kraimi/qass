import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { StatutChantier } from 'app/Enums/Statut/StatutChantier.Enum';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Client } from 'app/Models/Entities/Contacts/Client';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
import { IFormType } from 'app/pages/lots/lots-form/IFormType.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl, ACTION_API } from 'app/Enums/Configuration/api-url.enum';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';
import { DialogHelper } from 'app/libraries/dialog';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';
import { Rubrique } from 'app/Models/Entities/Documents/Rubrique';
import { DocumentRubricType } from 'app/Enums/DocumentRubricType.Enum';

declare var toastr: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'chantier-from',
	templateUrl: './from.component.html',
	styleUrls: ['./from.component.scss']
})

export class chantierFromComponent implements OnInit, OnChanges {

	get f() { return this.form.controls; }

	// tslint:disable-next-line:no-input-rename
	type;
	// tslint:disable-next-line:no-input-rename
	defaultData;
	progressRate = 0;
	// tslint:disable-next-line:no-output-rename
	// @Output('OnSave') OnSave = new EventEmitter();
	newIdClient = 0;
	formType: typeof IFormType = IFormType;
	labels: any;
	public form = null;
	statuts: { id: number, label: string, color: string }[];
	clientsListFristloading = true;
	clients: Client[] = [];
	adrresses: Adresse[] = [];
	chantier = [];
	adresseList = [];
	listClients = [];
	selectedStatus;
	filteredChantiers: Observable<any>;
	isModif = false;
	public newTags = [];

	public allTagsList: any;

	public rubriqueList: [] = [];
	constructor(
		private dialog: MatDialog,
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<chantierFromComponent>,
		@Inject(MAT_DIALOG_DATA) public data: {},
		private fb: FormBuilder) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.translate.get('labels').subscribe(labels => {
			this.labels = labels;
		});
		this.translate.get('statuts').subscribe((statuts: { id: number, label: string, color: string }[]) => {
			this.statuts = statuts;
		});
		this.getClients();
		this.defaultData = this.data['data'].defaultData;
		this.type = this.data['data'].type;
		this.createEmptyForm();

		if (this.defaultData == null) {
			//'Facturation', 'Devis', 'Commanends', 
			this.allTagsList = ['Facturation', 'Devis', 'Commandes', 'Appel d\'Offre', 'DOE', 'Planning', 'Suivi de chantier	']


		}

	}

	ngOnChanges() {
		this.getClients();
		this.defaultData = this.data['data'].defaultData;
		this.type = this.data['data'].type;
		this.createEmptyForm();
	}

	close() { this.dialogRef.close(); }

	createEmptyForm() {
		let selectedClient = null;
		if (this.defaultData != null && this.defaultData.client !== null) {
			selectedClient = this.getClientById(this.defaultData.client);
			this.adresseList = selectedClient.addresses
		}

		this.form = this.fb.group({
			nom: [this.defaultData == null ? '' : this.defaultData.name, [Validators.minLength(2), Validators.required]],
			description: [this.defaultData == null ? '' : this.defaultData.description],
			commentaire: [this.defaultData == null ? '' : this.defaultData.comment],
			idclient: [this.defaultData == null ? '' : selectedClient.id, [Validators.required]],
			statut: [this.defaultData == null ? StatutChantier.EnEtude : this.defaultData.status],
			nombrHeure: [this.defaultData == null ? 0 : this.defaultData.totalHours],
			montant: [this.defaultData == null ? 0 : this.defaultData.amount],
			tauxAvancement: [this.defaultData == null ? 0 : this.defaultData.progressRate]
		});
		this.selectedStatus = this.defaultData ? this.getStatus() : '';
	}

	addClient() {
		DialogHelper.openDialog(
			this.dialog,
			AddClientPopComponent,
			DialogHelper.SIZE_LARGE,
			{ docType: ApiUrl.Client }
		).subscribe(async client => {
			if (client !== undefined) {
				await this.getClients();
				this.listClients.unshift(client);
				this.form.controls['idclient'].setValue(client.id);
			}
		});
	}


	// Pour récupérer la liste des clients
	getClients() {
		if (this.clientsListFristloading) {
			this.clientsListFristloading = false;
			this.service.getAll(ApiUrl.Client).subscribe((res) => {
				this.clients = res.value;
				this.listClients = res.value;
			});
		}
	}

	getClientById(client) {
		const elem = this.clients.find(e => e.id === client.id);
		if (elem === undefined) {
			this.clients.unshift(client);
		}
		return this.clients.find(e => e.id === client.id);
	}


	filterclient(e) {
		if (e !== '') {
			this.clients = [];
			this.clients = this.listClients.filter(x => x.name.includes(e.toUpperCase()) || x.reference.includes(e.toUpperCase()));
		} else {
			this.clients = this.listClients;
		}

	}

	getmodelName() {
		try {
			if (this.type === IFormType.add) {

				return `${this.labels.add} ${this.labels.chantier}`;
			}
			if (this.type === IFormType.preview) {
				return `${this.labels.afficher} ${this.labels.chantier}`;
			}
			if (this.type === IFormType.update) {
				return `${this.labels.modifier} ${this.labels.chantier}`;
			}
		} catch (err) { }
	}

	CheckUniqueName(control: FormControl): Promise<{}> {
		if (control.value !== '') {
			const promise = new Promise((resolve, reject) => {
				this.service
					.getById(ApiUrl.Chantier + ApiUrl.CheckName, control.value)
					.subscribe(res => {
						if (res === true) {
							resolve({ CheckUniqueReference: true });
						} else {
							resolve(null);
						}
					});
			});
			return promise;
		}
	}

	submit() {
		if (this.form.valid) {
			let list = [];
			this.rubriqueList.forEach(element => {
				if (element != 'Devis' && element != 'Facturation' && element != 'Commandes') {
					list.push(element);
				}
			});
			this.newIdClient = this.form.value.idclient;
			const res = {
				id: this.type === IFormType.add ? 0 : this.defaultData.id,
				name: this.form.value.nom,
				description: this.form.value.description,
				comment: this.form.value.commentaire,
				totalHours: this.form.value.nombrHeure,
				amount: this.form.value.montant,
				progressRate: this.defaultData == null ? this.progressRate : this.defaultData.progressRate,
				status: this.form.value.statut == null ? StatutChantier.EnEtude : this.form.value.statut,
				clientId: this.newIdClient,
				client: this.clients.find(x => x.id === this.form.value.idclient),
				rubrics: list,
			};
			this.OnSave(res);
		} else {
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.fillAll, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			})
		}
	}

	getLabelleByStatut(statut): string {
		return this.statuts.filter(S => S.id === statut)[0].label;
	}

	retrunNewClient(client: Client) {
		this.clients.unshift(client);
		this.form.controls['idclient'].setValue(client);
	}

	getStatus = () => this.statuts[Object.entries(StatutChantier).map(k => k[1]).indexOf(this.form.value.statut)];

	onStatusChanged(e: any): void {
		this.selectedStatus = e;
		this.form.get('statut').setValue(Object.entries(StatutChantier).map(k => k[1])[e.id - 1]);
	}

	onTimeUpdate(e: any): void {
		this.form.get('nombrHeure').setValue(e);
	}

	onTauxUpdate(e: any): void {
		this.form.get('tauxAvancement').setValue(e);
		if (!this.defaultData) {
			this.progressRate = e;
		} else {
			this.defaultData.progressRate = e;
		}
	}

	OnSave(chantier) {
		this.form.reset();
		switch (this.type) {
			case IFormType.add:
				this.add(chantier);
				break;
			case IFormType.update:
				this.update(chantier);
				break;
		}
	}

	add(chantier) {
		this.service.create(ApiUrl.Chantier + ACTION_API.create, chantier).subscribe(res => {
			this.translate.get('addchantier').subscribe(text => {
				this.dialogRef.close(res.value);
				toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			}, err => {
				this.translate.get('errors').subscribe(text => {
					toastr.warning(text.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				});
			});
		});
	}

	update(chantier) {
		this.service.update(ApiUrl.Chantier, chantier, chantier.id).subscribe(res => {
			this.translate.get('update').subscribe(text => {
				this.dialogRef.close(res.value);
				toastr.success(text.msg, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		}, err => {
			console.log(err);
			this.close();
			this.translate.get('errors').subscribe(text => {
				toastr.warning(text.serveur, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
		});
	}

	onTagsChange(Tags: any) {
		this.rubriqueList = Tags;
	}


}
