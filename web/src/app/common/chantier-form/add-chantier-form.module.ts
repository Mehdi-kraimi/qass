import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { chantierFromComponent } from './from.component';
import { MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { TimePickerModule } from '../time-picker/time-picker.module';
import { SliderModule } from '../slider/slider.module';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';
import { CommonModules } from '../common.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';


export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/chantier/', '.json');
}

@NgModule({
	imports: [
		CommonModule,
		NgSelectModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			},
			isolate: true
		}),
		FormsModule,
		MatNativeDateModule,
		NgxMatSelectSearchModule,
		MatDatepickerModule,
		MatCheckboxModule,
		MatAutocompleteModule,
		TimePickerModule,
		SliderModule,
		MatSelectModule,
		CommonModules,
		ReactiveFormsModule,
		DataTablesModule,
		NgbTooltipModule
	],
	exports: [chantierFromComponent],
	declarations: [
		chantierFromComponent
	],
	entryComponents : [chantierFromComponent]
})
export class AddChantierFormModule { }
