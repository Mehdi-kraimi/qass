import { Component, OnInit, OnChanges, Output, EventEmitter, Input, Inject } from '@angular/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { User } from 'app/Models/Entities/User';
import { UserProfile } from 'app/Enums/user-profile.enum';
import { TranslateService } from '@ngx-translate/core';
import { DOCUMENT } from '@angular/common';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
declare var toastr: any;
declare var jQuery: any;

@Component({
	// tslint:disable-next-line: component-selector
	selector: 'select-user',
	templateUrl: './select-user.component.html',
	styleUrls: ['./select-user.component.scss']
})

export class SelectUserComponent implements OnInit, OnChanges {
	page: number = 1;
	totalPage: number = 1;
	techniciens = [];
	search: string = '';
	filter;

	@Input('selected') technicienTmp: User[] = [];
	@Output('returnSelectedItems') emitReturnSelectedItems = new EventEmitter();

	@Input('readOnly') readOnly = false;
	@Input('usersToDispaly') technicientToDispaly: User[] = [];
	@Input('listTechnicien') listTechnicien = [];
	userProfile: typeof UserProfile = UserProfile;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService, @Inject(DOCUMENT) private document: any) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		//this.getTechniciens(this.technicientToDispaly);
	}

	ngOnChanges() {
		this.getTechniciens();
	}


	removeTmpFromOrigin() {
		if (this.technicienTmp && this.technicienTmp.length > 0) {
			this.technicienTmp.forEach((technicien, index) => {
				this.techniciens = this.techniciens.filter(res => res.id !== technicien.id);
				// this.technicienTmp.splice(index, 1);
			});
		}

	}

	getTechniciens(technicientToDispaly?) {
		this.filter = {
			roleIds: this.listTechnicien,
			SearchQuery: this.search,
			Page: this.page,
			PageSize: 10,
			OrderBy: 'firstName',
			SortDirection: 'Descending',
			ignorePagination: false
		};
		this.service.getAllPagination(ApiUrl.User, this.filter)
			.subscribe(res => {
				if (res.value.length > 0) { this.techniciens = [...this.techniciens, ...res.value]; }
				this.totalPage = res.rowsCount;
				this.removeTmpFromOrigin();

			}, err => { });


	}

	returnSelectedItems() {
		debugger
		if (this.technicienTmp.length === 0) {
			this.translate.get('errors').subscribe(errors => {
				toastr.warning(errors.technicienRequired, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			});
			return;
		}
		jQuery('#selectUserModal').modal('hide');
		this.emitReturnSelectedItems.emit(this.technicienTmp);
		const technicienTmp = this.technicienTmp;
		this.technicientToDispaly = [].concat(technicienTmp);
	}

	// On cas de scroll dans popup des lots
	onScrollLots() {
		if (this.totalPage !== this.page) {
			this.page++;
			this.getTechniciens()
		}
	}


	addTmpTechnicien(index) {
		this.technicienTmp.unshift(this.techniciens[index]);
		this.techniciens.splice(index, 1);
	}

	removeTmpTechnicien(index) {

		this.techniciens.unshift(this.technicienTmp[index]);
		this.technicienTmp.splice(index, 1);
	}

	delete(index) {
		this.techniciens.unshift(this.technicientToDispaly[index]);
		this.technicientToDispaly.splice(index, 1);
		this.technicienTmp.splice(index, 1);
	}
	navigateToDtailsPage(id) {
		const origin = this.document.location.origin;
		const url = `${origin}/utilisateurs/detail/${id}`;
		window.open(url, '_blank');
	}
}