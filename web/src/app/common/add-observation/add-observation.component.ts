import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';

export declare var swal: any;
declare var jQuery: any;

@Component({
	selector: 'app-add-observation',
	templateUrl: './add-observation.component.html',
	styleUrls: ['./add-observation.component.scss']
})
export class AddObservationComponent implements OnInit, OnChanges {

	docType = '';
	processing = false;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	document;
	observation = '';

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public dialogRef: MatDialogRef<AddObservationComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { docType: string, document },
		private translate: TranslateService) {
		this.docType = this.data.docType;
		this.document = this.data.document;
	}

	ngOnInit() {
		if (this.data) {
		}
	}

	ngOnChanges() {

	}

	async save() {
		await this.service.updateAll(ApiUrl.FicheIntervention + '/' + this.document.id + '/Update/ModeStatus',
			{ 'type': 0, 'observation': this.observation }).subscribe(
				res => {
					this.close();
				},
				err => {
					console.log(err)

				}
			);
	}

	close() {
		this.dialogRef.close();
	}

}
