import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SendMailParams } from 'app/Models/Entities/Commun/SendMailParams';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { ConfigMessagerie } from 'app/Models/Entities/Parametres/ConfigMessagerie';
import { TypeNumerotation } from 'app/Enums/TypeNumerotation.Enum';
import { CreatePdf } from '../../libraries/PDF/create-pdf';
import { ActionPdf } from '../../Enums/typeValue.Enums';

export declare var swal: any;
declare var jQuery: any;

@Component({
	selector: 'app-send-email-pdf',
	templateUrl: './send-email.component.html',
	styleUrls: ['./send-email.component.scss']
})
export class SendMailComponent implements OnInit, OnChanges {

	docType = '';
	processing = false;
	typeNumerotation: typeof TypeNumerotation = TypeNumerotation;
	pdf;
	document;
	emailClient = null

	formSendEmail = new FormGroup({
		emailTo: new FormControl('', Validators.required),
		object: new FormControl('', Validators.required),
		contents: new FormControl('', Validators.required)
	});

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public dialogRef: MatDialogRef<SendMailComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { docType: string, doc },
		private translate: TranslateService) {

	}
	async ngOnInit() {
		if (this.data) {
			this.docType = this.data.docType;
			this.document = this.data.doc;
			const client = await this.GetClient(this.document.client.id);
			this.emailClient = client != undefined ? client['email'] : this.document.client.email;
			this.getInfosMessagerie();
		}
	}


	GetClient(id): Promise<any> {
		return new Promise((resolve, reject) => {
			this.service.getById(ApiUrl.Client, id)
				.subscribe(
					res => resolve(res.value),
					error => reject(error)
				)
		});
	}
	ngOnChanges() {

	}

	async getInfosMessagerie() {
		await this.service.getAll(ApiUrl.configMessagerie).subscribe(res => {
			const data = JSON.parse(res) as ConfigMessagerie;
			const resultat = data.password !== '' && data.port !== '' && data.server !== '';
			if (resultat) {
				this.getParametrageDevis(this.document);
			} else {
				this.close();
				swal('votre messagerie n\'est pas encore configurée, veuillez demander à l\'administrateur de la configurer  ', '', 'success');
			}
		})
	}

	getTypeNumer() {
		if (this.docType === ApiUrl.Devis) { return this.typeNumerotation.devis };
		if (this.docType === ApiUrl.Invoice) { return this.typeNumerotation.facture };
		if (this.docType === ApiUrl.avoir) { return this.typeNumerotation.avoir };
		if (this.docType === ApiUrl.BonCommandeF) { return this.typeNumerotation.boncommande_fournisseur };
		if (this.docType === ApiUrl.Depense) { return this.typeNumerotation.depense };
		if (this.docType === ApiUrl.FicheIntervention) { return this.typeNumerotation.fiche_intervention };
		if (this.docType === ApiUrl.MaintenanceOperationSheet) { return this.typeNumerotation.fiche_intervention };
	}


	getParametrageDevis(datadevis) {
		this.service.getAll(ApiUrl.configDocument).subscribe(async res => {
			const data = JSON.parse(res).find(element => element.documentType === this.getTypeNumer());
			if (datadevis && datadevis.emails && datadevis.emails.length === 0) {
				this.formSendEmail.setValue({
					emailTo: [this.emailClient],
					object: data.Email !== undefined && data.Email !== null ? data.Email.subject : '',
					contents: data.Email !== undefined && data.Email !== null ? data.Email.content : ''
				});
			}
			if (datadevis.emails === undefined) {
				this.formSendEmail.setValue({
					emailTo: [this.emailClient],
					object: data.Email !== undefined && data.Email !== null ? data.Email.subject : '',
					contents: data.Email !== undefined && data.Email !== null ? data.Email.content : ''
				});
			} else {
				this.formSendEmail.setValue({
					emailTo: [this.emailClient],
					object: data && data.reflationEmail !== undefined && data.reflationEmail !== null ? data.reflationEmail.subject : '',
					contents: data && data.reflationEmail !== undefined && data.reflationEmail !== null ? data.reflationEmail.content : ''
				});
			}
		})
	}

	envoyer() {
		if (
			!this.formSendEmail.controls.emailTo.invalid &&
			!this.formSendEmail.controls.object.invalid &&
			!this.formSendEmail.controls.contents.invalid
		) {
			this.processing = true;
			CreatePdf.generatePdf(this.document, this.docType, ActionPdf.base64, this.formSendEmail).then(res => {
				this.close();
				this.processing = false;
			});
			// this.pdf.getBase64(async (base) => {
			// 	const mailParams: SendMailParams = {
			// 		To: [this.formSendEmail.controls.emailTo.value],
			// 		Subject: this.formSendEmail.controls.object.value,
			// 		body: this.formSendEmail.controls.contents.value,
			// 		path: '',
			// 		Bcc: [],
			// 		Cc: [],
			// 		attachments: [base],
			// 	};
			// 	this.service.sendMail(this.docType, this.document.id, mailParams).subscribe(res => {

			// 	}, reject => console.error(reject));
			// });
		}
	}

	close() {
		this.dialogRef.close();
	}

}
