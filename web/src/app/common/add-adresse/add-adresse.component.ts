import { Component, OnInit, OnChanges, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators } from '@angular/forms';

export declare var swal: any;
declare var jQuery: any;

@Component({
	selector: 'app-add-adresse',
	templateUrl: './add-adresse.component.html',
	styleUrls: ['./add-adresse.component.scss']
})
export class AddAdresseComponent implements OnInit, OnChanges {

	form;
	default = true;
	constructor(
		public dialogRef: MatDialogRef<AddAdresseComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { docType: string, document },
		private fb: FormBuilder,
		private translate: TranslateService) {
		this.cretaeForm();
	}

	ngOnInit() {
		if (this.data) {
		}
	}

	cretaeForm() {
		this.form = this.fb.group({
			'designation': ['', [Validators.required]],
			'street': ['', [Validators.required]],
			'complement': [''],
			'department': [''],
			'city': ['', [Validators.required]],
			'postalCode': [''],
			'countryCode': [''],
			'isDefault': [this.default]
		});
	}

	ngOnChanges() {

	}

	checkDefault(e) {
		this.default = !this.default;
		this.form.controls.isDefault.setValue(this.default);
	}

	get f() { return this.form.controls; }

	async save() {
		if (this.form.valid) {
			this.dialogRef.close(this.form.value);
		} else { return; }
	}

	close() {
		this.dialogRef.close();
	}

}
