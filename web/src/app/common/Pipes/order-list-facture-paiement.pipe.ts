import { Pipe, PipeTransform } from '@angular/core';
import { Invoice } from 'app/Models/Entities/Documents/Invoice';


@Pipe({
	name: 'OrderListFacturePaiement',
	pure: false
})
export class OrderListFacturePaiementPipe implements PipeTransform {

	private cachedList = [];

	constructor() { }

	transform(list: Invoice[], args?): any {

		if (list == null) {
			return [];
		}

		list.sort(function (a, b) {
			if (a.dueDate > b.dueDate) return 1;
			if (a.dueDate < b.dueDate) return -1;
			return 0;
		});

		return list;
	}

}
