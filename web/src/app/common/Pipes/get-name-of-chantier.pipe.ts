
import { Pipe, PipeTransform, Inject } from '@angular/core';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';

@Pipe({
	name: 'GetNameOfChantier',
	pure: false
})
export class GetNameOfChantierPipe implements PipeTransform {
	private name: any = null;
	private cachedIdGroupe = null;

	constructor(@Inject('IGenericRepository') private service: IGenericRepository<any>, ) { }

	transform(idChantier: any, args?: any): any {

		if (!idChantier || typeof (parseInt(idChantier)) != "number") {
			return null;
		}

		if (idChantier !== this.cachedIdGroupe) {
			this.name = null;
			this.cachedIdGroupe = idChantier;

			this.service.getById(ApiUrl.Chantier, idChantier).subscribe(groupe => {
				this.name = groupe.value.name
			});
		}

		return this.name;
	}

}
