import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'mapToIterable'
})
export class MapToIterablePipe implements PipeTransform {
    transform(dict: Object) {
        const array = [];
        for (const key in dict) {
            if (dict.hasOwnProperty(key)) {
                array.push({ key: key, val: dict[key] });
            }
        }
        return array;
    }
}
