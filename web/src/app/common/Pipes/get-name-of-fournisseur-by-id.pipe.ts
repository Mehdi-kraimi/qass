import { Pipe, PipeTransform, Inject } from '@angular/core';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';


@Pipe({
  name: 'GetNameOfFournisseurById',
  pure: false
})
export class GetNameOfFournisseurByIdPipe implements PipeTransform {

  // userName:string = "not found";
  private userName: any = null;
  private cachedIdFournisseur = null;

  constructor( @Inject('IGenericRepository') private service: IGenericRepository<any>) { }

  transform(idFournisseur: any, args?: any): any {

    if (!idFournisseur || typeof (parseInt(idFournisseur)) != "number") {
      return null;
    }

    if (idFournisseur !== this.cachedIdFournisseur) {
      this.userName = null;
      this.cachedIdFournisseur = idFournisseur;

      this.service.getById(ApiUrl.Fournisseur, idFournisseur).subscribe(value => {
        const fournisseur = value.value;
        this.userName = fournisseur.name
      });
    }

    return this.userName;
  }

}
