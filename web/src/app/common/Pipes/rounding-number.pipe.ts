import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
	name: 'RoudingNumber',
	pure: false
})
export class RoudingNumberPipe implements PipeTransform {


	transform(number: any, args?: any): any {

		let num = Number(number) // The Number() only visualizes the type and is not needed
		if (isNaN(num)) { num = 0; }
		const roundedString = num.toFixed(2);
		return this.numberWithSpaces(roundedString);
	}


	numberWithSpaces(x) {
		const parts = x.toString().split('.');
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
		return parts.join('.');
	}

}
