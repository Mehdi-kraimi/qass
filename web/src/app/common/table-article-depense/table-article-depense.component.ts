import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core';
import { ICalcule } from 'app/calcule/ICalcule';
import { Calcule } from 'app/calcule/Calcule';
import { CalculTva } from 'app/Models/Model/calcul-tva';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';;
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { SelectProduitByFournisseurComponent } from './select-produit/select-produit.component';
import { TypeValue } from 'app/Enums/typeValue.Enums';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';

declare var jQuery: any;
declare var toastr: any;

@Component({
	selector: 'table-article-depense',
	templateUrl: './table-article-depense.component.html',
	styleUrls: ['./table-article-depense.component.scss']
})
export class TableArticleDepenseComponent implements OnInit, OnChanges {

	@Input('load') load: { getDateToSave };
	@Input('readOnly') readOnly: boolean = true;
	@Input('IdFournisseur') IdFournisseur = null;
	@Input('articles') articles: { product: any, quantity: number, remise: number, type: number }[] = [];

	produits: any[] = [];
	list = [];
	loadingFinished = false; // si la liste chargé
	search = ''; // zone de recherche
	totalPage = 1; // total des pages
	page = 1; // Current page des
	res: { id: number; nom: string; produits: any[] };
	collapseClosed: number[] = [];
	AllCollapsesIsClosed: boolean = true;
	montantHT: number = 0;
	totalGeneral = 0;
	globalTotalHTRemise = 0;
	globalTotalTTC = 0;
	MontantHt = 0;
	calcule: ICalcule = new Calcule();
	TotalTva = 0;
	totalTTC = 0;
	montantTva = 0;
	NouveauTotalGeneral = 0;
	remiseGloabl = 0;
	emitter: any = {};
	tva: any
	prix_fournisseur = 0;
	form: any;
	produit;
	prix_Ht = 0;
	prix_Ttc = 0;
	newProduit: boolean = false;
	categories = [];
	globalCategorie = [];
	typeProduct = [];

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		private formBuilder: FormBuilder,
		private dialog: MatDialog) {
		this.form = this.formBuilder.group({
			nom: [null, [Validators.required]],
			qte: [0, [Validators.required]],
			categorie: [null, [Validators.required]],
			prixparfournisseur: [null, [Validators.required]],
			tva: [0, [Validators.required]],
			type: [null, [Validators.required]],
		});
	}

	ngOnInit() {
		this.getListeCategorie();
		this.getTypeProduct();
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}

	ngOnChanges() {
		if (this.load === undefined) { return; }
		this.load.getDateToSave = this.getDateToSave.bind(this);
	}

	showCategory(articleIndex) {
		return this.articles[articleIndex].product.category === null || this.articles[articleIndex].product.category === 'null' ? true : false;
	}

	getListeCategorie(): void {
		this.service.getAll(ApiUrl.configClassificationExpenses).subscribe(res => {
			this.categories = res.value;
			this.categories.forEach(element => {
				this.globalCategorie = [...this.globalCategorie, ...element.subClassification]
			});
		});
	}

	getDateToSave(callBack) {
		const calcTotalTva = this.calcTotalTva();
		callBack({
			prestation: this.articles,
			totalHt: this.totalGeneral,
			totalTtc: calcTotalTva.totalTTC,
			tva: this.groupTVA(),
			totalTVA: calcTotalTva.totalTva,
		});
	}

	onChangeCategorie(articleIndex, event) {
		try {
			if (event.target.value === '') {
				this.articles[articleIndex].product.category = null;
			} else {
				this.articles[articleIndex].product.category = this.globalCategorie.find(x => + x.id === +event.target.value),
					this.articles[articleIndex].product.categoryId = (event.target.value).id
			}
		} catch (ex) {
			console.log(ex)
		}
	}


	setTvaArticle(articleIndex, event) {
		try {
			if (event.target.value == '') {
				this.articles[articleIndex].product.vat = 0
			} else {
				this.articles[articleIndex].product.vat = event.target.value
			}
		} catch (ex) {
			console.log(ex)
		}
	}


	setPrixFournisseurArticle(articleIndex, event) {
		if (this.IdFournisseur !== null) {
			this.articles[articleIndex].product.productSuppliers.filter(x => x.supplierId == this.IdFournisseur)[0].price = event.target.value
		}

	}
	setQteArticle(articleIndex, event) {
		this.articles[articleIndex].quantity = event.target.value
	}
	prixTtc(qte, articleIndex) {
		this.tva = this.articles[articleIndex].product.vat;
		if (this.IdFournisseur !== null) {
			this.prix_fournisseur = this.articles[articleIndex].product.productSuppliers.filter(x => x.supplierId == this.IdFournisseur)[0].price;
		}
		if (this.tva !== '') {
			const tvaEnPr = 1 + parseFloat(this.tva) / 100;
			return this.prix_fournisseur * parseInt(qte) * tvaEnPr;
		} else {

			this.tva = 0;
			const tvaEnPr = 1 + parseFloat(this.tva) / 100;
			return this.prix_fournisseur * parseInt(qte) * tvaEnPr;
		}

	}

	prixHt(articleIndex, qte) {

		if (this.IdFournisseur) {
			this.prix_fournisseur = this.articles[articleIndex].product.productSuppliers.filter(x => x.supplierId == this.IdFournisseur)[0].price;
			return this.prix_fournisseur * parseInt(qte);
		}

	}

	calcTotalTva(): { totalTva: number; totalTTC: number; montantTva: number } {
		const montantTva = this.groupTVA().reduce((x, y) => x + y.totalTVA, 0);
		const total = this.NouveauTotalGeneral === 0 ? this.totalGeneral : this.NouveauTotalGeneral;
		const TotalTva = (montantTva / total) * 100;
		const totalTTC = total + montantTva;
		return { totalTva: TotalTva, totalTTC: totalTTC, montantTva: montantTva };
	}

	calcTotalGeneralTtc() {
		return this.calcTotalTva().totalTTC;
	}

	clalcTotalGeneral(): number {
		if (this.IdFournisseur !== null) {
			const articles = this.getProductsOfArticles(this.articles);
			let totalHt = 0;
			articles.forEach(article => {
				let price = article.productSuppliers.filter(x => x.supplierId == this.IdFournisseur)[0].price;
				totalHt = totalHt + price * article.quantity;
			});
			this.totalGeneral = totalHt;
			return totalHt;
		}
		return 0;

	}

	groupTVA(): CalculTva[] {
		let calculTvas = [];
		let articles = this.getProductsOfArticles(this.articles);
		calculTvas = this.calcule.calculVentilationRemiseDepense(
			articles,
			this.totalGeneral,
			this.remiseGloabl,
			TypeValue.Amount,
			this.IdFournisseur
		);
		return calculTvas;
	}


	getTypeProduct() {
		this.service.getAll(ApiUrl.typeProduct).subscribe(typeProduct => {
			this.typeProduct = typeProduct.value;
		});
	}

	getProductsOfArticles(articles) {
		var articlesTmp = [];
		articles.forEach(article => {
			article.product.quantity = article.quantity;
			article.product.remise = article.remise;
			articlesTmp.push(article.product);
		});
		return articlesTmp;
	}

	removeArticle(i, ii) {
		if (ii == null) {
			this.articles.splice(i, 1);
		} else {
			this.articles[i].product.lotProduits.splice(ii, 1);
		}
	}

	LoadListProduit() {
		if (!this.IdFournisseur) {
			toastr.warning('Veuillez sélectionner le fournisseur', '', {
				positionClass: 'toast-top-center',
				containerId: 'toast-top-center',
			});
			return;
		}
		//  this.emitter.loadProduit();
		let dialogProduitConfig = new MatDialogConfig();
		dialogProduitConfig.width = '1500px';
		dialogProduitConfig.height = '500px';
		dialogProduitConfig.data = { idFournisseur: this.IdFournisseur };
		const dialogRef = this.dialog.open(SelectProduitByFournisseurComponent, dialogProduitConfig);

		dialogRef.afterClosed().subscribe((data) => {
			if (data && Array.from(data).length) {
				data.forEach(produit => {
					produit.product.discount = {
						value: 0,
						type: TypeValue.Amount
					};
					this.articles.push(produit);
				});
			}

		});
	}


	selectPrixParFounisseur(prixParFournisseur, id: number, index): number {
		var result = null
		if (this.IdFournisseur !== null) {
			result = prixParFournisseur.filter(x => x.supplierId == this.IdFournisseur)[0];
			return result != null ? result.price : null;
		}

	}

	addNewProduit() {
		this.newProduit = true;
	}

	get f() {
		return this.form.controls;
	}


	calculate_ht(qte, prixFournisseur) {
		this.prix_Ht = prixFournisseur * parseInt(qte);
		return this.prix_Ht == undefined ? 0 : this.prix_Ht;
	}

	calculate_ttc(qte, tva, prixFournisseur) {
		const tvaEnPr = 1 + parseFloat(tva) / 100;
		this.prix_Ttc = this.calculate_ht(qte, prixFournisseur) * tvaEnPr;
		return this.prix_Ttc == undefined ? 0 : this.prix_Ttc.toFixed(2);
	}


	saveArticle() {
		let formValues = this.form.value;
		if (formValues.categorie === null || formValues.categorie === 'null') {
			return;
		} else {
			let produit = {};
			try {
				produit['reference'] = formValues.nom;
				produit['designation'] = formValues.nom;
				produit['description'] = formValues.nom;
				produit['totalHours'] = 0;
				produit['materialCost'] = 0;
				produit['hourlyCost'] = 0;
				produit['vat'] = formValues.tva;
				produit['unite'] = '';
				// produit['categoryId'] = formValues.categorie.id;
				produit['category'] = formValues.categorie;
				produit['productCategoryType'] = formValues.type;
				produit['dataSheets'] = [];
				produit['labels'] = [];
				produit['changesHistory'] = [];
				produit['quantity'] = formValues.qte;
				produit['id'] = '';
				produit['discount'] = {
					'type': TypeValue.Amount,
					'value': 0
				}
				produit['totalHT'] = this.prix_Ht;
				produit['totalTTC'] = this.prix_Ttc;
				produit['productSuppliers'] = [{
					'price': formValues.prixparfournisseur,
					'isDefault': true,
					'supplierId': this.IdFournisseur

				}];
			} catch (_error) {
				console.log(_error)
			}

			let article = {
				product: produit,
				quantity: formValues.qte,
				remise: 0,
				type: 1,
			};
			this.articles.push(article);
			this.form.reset();
			this.newProduit = false;
		}
	}

	annulerArticle() {
		this.newProduit = false;
	}
}
