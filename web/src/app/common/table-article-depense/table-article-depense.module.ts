import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { SelectProduitByFournisseurComponent } from './select-produit/select-produit.component';
import { TableArticleDepenseComponent } from './table-article-depense.component';

import { CommonModules } from '../common.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { PreviousRouteService } from 'app/services/previous-route/previous-route.service';
import { MatDialogModule } from '@angular/material';
export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/selectProduit/', '.json');
}
@NgModule({
	providers: [
		PreviousRouteService,

	],
	imports: [
		CommonModule,
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
			isolate: true,
		}),
		FormsModule,
		ReactiveFormsModule,
		DataTablesModule,
		CommonModules,
		NgSelectModule,
		NgbTooltipModule,
		// GroupesModule
		InfiniteScrollModule,
		AngularEditorModule,
		MatDialogModule

	],
	exports: [TableArticleDepenseComponent],
	declarations: [
		TableArticleDepenseComponent,
		SelectProduitByFournisseurComponent,
	],
	entryComponents: [SelectProduitByFournisseurComponent]


})
export class TableArticleDepenseModule { }
