import { Component, Inject, OnInit, OnChanges } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { GammeMaintenanceEquipement } from 'app/Models/Entities/Maintenance/GammeMaintenanceEquipement';

declare var toastr: any;
declare var jQuery: any;
declare var swal: any;

@Component({
	selector: 'app-liste-gamme-maintenance-equipement',
	templateUrl: './liste-gamme-maintenance-equipement.component.html',
	styleUrls: ['./liste-gamme-maintenance-equipement.component.scss']
})
export class ListeGammeMaintenanceEquipementComponent implements OnInit, OnChanges {

	selected: GammeMaintenanceEquipement[] = [];
	search = '';
	gammeInDb;
	result: any = [];

	constructor(
		public dialogRef: MatDialogRef<ListeGammeMaintenanceEquipementComponent>,
		@Inject(MAT_DIALOG_DATA) public data: GammeMaintenanceEquipement[]) { }

	ngOnInit() {
		this.gammeInDb = this.data;
	}

	ngOnChanges() {
		this.gammeInDb = this.data;
	}

	searchProduit() {
		this.data = this.gammeInDb
		this.data = this.filterTexte(this.data, this.search)
		if (this.search === '') {
			this.data = this.gammeInDb
		}
	}


	filterTexte = (arr, requete) => {
		return arr.filter(el => el.equipmentName.toLowerCase().indexOf(requete.toLowerCase()) !== -1);
	}

	checkElement(id) {
		this.selected.unshift(this.data.filter(x => x.id === id)[0]);
		this.data = this.data.filter(x => x.id !== id);
		const index = this.gammeInDb.findIndex(x => x.id === id);
		const gamme = this.gammeInDb.splice(index, 1);
	}

	IncheckElement(id) {

		this.data.unshift(this.selected.filter(x => x.id === id)[0]);
		this.gammeInDb.unshift(this.selected.filter(x => x.id === id)[0]);
		if (this.search === '') {
			this.data = this.gammeInDb;
		}
		this.selected = this.selected.filter(x => x.id !== id);
		this.data = this.removeDuplicates()

	}

	removeDuplicates(): any {
		this.data.forEach((item) => {
			if (this.result.indexOf(item) < 0) {
				this.result.push(item);
			}
		});
		return this.result;
	}


	save() {
		if (this.selected.length === 0) {
			toastr.warning('text.serveur', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		} else {
			this.dialogRef.close(this.selected);
		}
	}
}