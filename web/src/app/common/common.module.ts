import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { HistoriquesComponent } from './historiques/historiques.component';
import { RechercheAvanceeComponent } from './recherche-avancee/recherche-avancee.component';
import { ShowHideTableColumnsComponent } from './show-hide-table-columns/show-hide-table-columns.component';
import { CustomLoaderComponent } from './custom-loader/custom-loader.component';
import { ListAddressComponent } from 'app/common/list-address/list-address.component';
import { ListesContactsComponent } from 'app/common/listes-contacts/listes-contacts.component';
import { MemosComponent } from 'app/common/memos/memos.component';
import { GetNameOfUserByIdPipe } from './Pipes/get-name-of-user-by-id.pipe';
import { GetNameOfFournisseurByIdPipe } from './Pipes/get-name-of-fournisseur-by-id.pipe';
import { GetNameOfGroupePipe } from './Pipes/get-name-of-groupe.pipe';
import { RoudingNumberPipe } from './Pipes/rounding-number.pipe';
import { Historique } from 'app/Models/Entities/Commun/Historique';
import { NgSelectModule } from '@ng-select/ng-select';
import { DataTablesModule } from 'angular-datatables';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { GetUserProfileLibelle } from './Pipes/get-user-profile-libelle.pipe';
import { GetImageBase64ByName } from './Pipes/get-image-base64-by-name.pipe';
import { TruncatePipe } from './Pipes/truncate.pipe';
import { ShowCurrencyPipe } from './Pipes/show-currency.pipe';
import { RoudingNumberInputPipe } from './Pipes/rounding-number-input.pipe';
import { PrixParFournisseurComponent } from './prix-par-fournisseur/prix-par-fournisseur.component';
import { ModalImageComponent } from './modal-image/modal-image.component';
import { TagsComponent } from './tags/tags.component';
import { PainationComponent } from './paination/paination.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FolderComponent } from './folder/folder.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { PortesComponent } from './portes/portes.component';
import { AngularTreeGridModule } from 'angular-tree-grid';
import { FullCalendarModule } from '@fullcalendar/angular'; // for FullCalendar!
import { GetNameOfChantierPipe } from './Pipes/get-name-of-chantier.pipe';
import { ArticlesMinimalisteComponent } from './articles-minimaliste/articles-minimaliste.component';
import { InputFormatDirective } from './directives/input-format.directive';
import { HistoriqueEmailComponent } from './historique-email/historique-email.component';
import { FullCalendarComponent } from './full-calendar/full-calendar.component';
import { CalendarModule, SplitButtonModule } from 'app/custom-module/primeng/primeng';
import { ChoixChantierComponent } from './choix-chantier/choix-chantier.component';
import { OrderListFacturePaiementPipe } from './Pipes/order-list-facture-paiement.pipe';
import { RestePayerPipe } from './Pipes/reste-payer.pipe';
import { InputSearchComponent } from './input-search/input-search.component';
import { VisualiserPdfComponent } from './visualiser-pdf/visualiser-pdf.component';
import { ChoixClientComponent } from './choix-client/choix-client.component';
import {
  MatDialogModule, MatTableModule, MatSortModule, MatCheckboxModule,
  MatPaginatorModule, MatTooltipModule, MatIconModule, MatButtonModule, MatFormFieldModule,
  MatInputModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatSliderModule
} from '@angular/material';
import { CategoryListComponent } from './lists/category-list/category-list.component';
import { FillTagsComponent } from './fill-tags/fill-tags.component';
import { NameModelComponent } from '../pages/parameteres/generate-contract/name-model/name-model.component';
import { UploadFileDialogComponent } from './upload-file-dialog/upload-file-dialog.component';
import { MapToIterablePipe } from './Pipes/map-to-iterable.pipe';
import { FileDropModule } from 'ngx-file-drop';
import { MatMenuModule } from '@angular/material/menu';
import { FactureAcompte } from 'app/components/facture-acompte/facture-acompte.component';
import { FactureSitutaion } from 'app/components/facture-situation/facture-situation.component';
import { TableArticleSituationComponent } from './tableArticleSituation/table-article-situation.component';
import { AddFactureAcompteComponent } from 'app/components/add-facture-acompte/add-facture-acompte.component';
import { AddFactureSituationComponent } from 'app/components/add-facture-situation/add-facture-situation.component';
import { InputTextareaModule } from './input-textarea/input-textarea.module';
import { SendMailComponent } from './send-email/send-email.component';
import { CreateDepenseComponent } from './create-depense/create-depense.component';
import { CancelAvoirComponent } from './cancel-avoir/cancel-avoir.component';
import { PayByAvoirDepenseComponent } from 'app/components/pay-by-avoir-depense/pay-by-avoir.component';
import { PayByAvoirComponent } from 'app/components/pay-by-avoir-facture/pay-by-avoir.component';
import { AddObservationComponent } from './add-observation/add-observation.component';
import { TableArticleFactureAcompteComponent } from './table-article-facture-acompte/table-article-facture-acompte.component';
import { ListUserClientComponent } from 'app/components/list-user-client/list-user-client.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { SliderModule } from './slider/slider.module';
import { ListmaterielComponent } from 'app/components/list-materiel/list-materiel.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AddAdresseComponent } from './add-adresse/add-adresse.component';
import { AddmodeReglementComponent } from 'app/components/forms/parametres/add-modeReglement/add-modeReglement.component';
import { AddlabelComponent } from 'app/components/forms/parametres/add-label/add-label.component';
import { AddtypeDComponent } from 'app/components/forms/parametres/add-typeD/add-typeD.component';
import { AddClientPopComponent } from 'app/components/add-client-pop/add-client-pop.component';
import { RubriqueTagsComponent } from './rubrique-tags/rubrique-tags.component';
import { PaimentDetailsComponent } from 'app/components/paiment-details/paiment-details.component';
import { AreplanifierFormComponent } from 'app/components/areplanifier-form/areplanifier-form.component';
import { AdddLigneComponent } from 'app/components/add-ligne/add-ligne.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MemoCardModule } from 'app/shared/memo-card/memo-card.module';
import { PaiementDepenseComponent } from 'app/components/paiement-depense/paiement.component';
import { PaiementComponent } from 'app/components/paiement-facture/paiement.component';
import { ClientSituationComponent } from './client-situation/client-situation.component';
import { AddTypekindComponent } from 'app/components/add-typekind/add-typekind.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/common/', '.json');
}

@NgModule({
	exports: [
		AddTypekindComponent,
		AdddLigneComponent,
		AddClientPopComponent,
		AddtypeDComponent,
		AddlabelComponent,
		AddmodeReglementComponent,
		ListUserClientComponent,
		ListmaterielComponent,
		AddObservationComponent,
		AddAdresseComponent,
		PaiementDepenseComponent,
		PayByAvoirDepenseComponent,
		PaiementComponent,
		PayByAvoirComponent,
		CancelAvoirComponent,
		CreateDepenseComponent,
		SendMailComponent,
		TableArticleSituationComponent,
		TableArticleFactureAcompteComponent,
		FactureAcompte,
		AddFactureAcompteComponent,
		FactureSitutaion,
		AddFactureSituationComponent,
		HistoriquesComponent,
		ShowHideTableColumnsComponent,
		GetNameOfUserByIdPipe,
		GetNameOfFournisseurByIdPipe,
		CustomLoaderComponent,
		ListesContactsComponent,
		TagsComponent,
		ModalImageComponent,
		PainationComponent,
		PrixParFournisseurComponent,
		MemosComponent,
		ListAddressComponent,
		GetNameOfFournisseurByIdPipe,
		GetNameOfGroupePipe,
		ShowCurrencyPipe,
		GetNameOfChantierPipe,
		TruncatePipe,
		GetImageBase64ByName,
		RoudingNumberPipe,
		RoudingNumberInputPipe,
		GetUserProfileLibelle,
		RechercheAvanceeComponent,
		FolderComponent,
		PortesComponent,
		ArticlesMinimalisteComponent,
		InputFormatDirective,
		HistoriqueEmailComponent,
		FullCalendarComponent,
		ChoixChantierComponent,
		OrderListFacturePaiementPipe,
		RestePayerPipe,
		MapToIterablePipe,
		InputSearchComponent,
		VisualiserPdfComponent,
		ChoixClientComponent,
		CategoryListComponent,
		RubriqueTagsComponent,
		PaimentDetailsComponent,
		AreplanifierFormComponent,
		MatCheckboxModule,
		ChangePasswordComponent,
		PaiementDepenseComponent,
		PaiementComponent,
		ClientSituationComponent,
	],
	imports: [
		NgxMatSelectSearchModule,
		SplitButtonModule,
		NgbTooltipModule,
		InputTextareaModule,
		RouterModule,
		CommonModule,
		NgbModule,
		FormsModule,
		TranslateModule,
		ReactiveFormsModule,
		NgSelectModule,
		DataTablesModule,
		InfiniteScrollModule,
		MatMenuModule,
		ShowHidePasswordModule,
		AngularTreeGridModule,
		MemoCardModule,
		AngularEditorModule,
		PdfViewerModule,
		CalendarModule,
		FullCalendarModule,
		MatDialogModule,
		FileDropModule,
		MatSelectModule,
		MatTableModule,
		OverlayModule,
		MatSortModule,
		NgSelectModule,
		CalendarModule,
		MatCheckboxModule,
		NgbTooltipModule,
		MatPaginatorModule,
		InfiniteScrollModule,
		MatTooltipModule,
		MatIconModule,
		MatButtonModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatMenuModule,
		ShowHidePasswordModule,
		MatDatepickerModule,
		MatNativeDateModule,
		SliderModule,
		MatSliderModule,
	],
	declarations: [
		AddTypekindComponent,
		AdddLigneComponent,
		AddClientPopComponent,
		AddtypeDComponent,
		AddlabelComponent,
		AddmodeReglementComponent,
		ListmaterielComponent,
		ListUserClientComponent,
		AddAdresseComponent,
		AddObservationComponent,
		PaiementDepenseComponent,
		PayByAvoirDepenseComponent,
		PaiementComponent,
		PayByAvoirComponent,
		CancelAvoirComponent,
		CreateDepenseComponent,
		SendMailComponent,
		AddFactureSituationComponent,
		AddFactureAcompteComponent,
		TableArticleSituationComponent,
		TableArticleFactureAcompteComponent,
		FactureAcompte,
		FactureSitutaion,
		FillTagsComponent,
		HistoriquesComponent,
		ShowHideTableColumnsComponent,
		GetNameOfUserByIdPipe,
		CustomLoaderComponent,
		ListesContactsComponent,
		TagsComponent,
		ModalImageComponent,
		PainationComponent,
		PrixParFournisseurComponent,
		MemosComponent,
		ListAddressComponent,
		GetNameOfFournisseurByIdPipe,
		GetNameOfFournisseurByIdPipe,
		GetNameOfGroupePipe,
		ShowCurrencyPipe,
		GetNameOfChantierPipe,
		TruncatePipe,
		GetImageBase64ByName,
		RoudingNumberPipe,
		RoudingNumberInputPipe,
		GetUserProfileLibelle,
		RechercheAvanceeComponent,
		FolderComponent,
		PortesComponent,
		ArticlesMinimalisteComponent,
		InputFormatDirective,
		HistoriqueEmailComponent,
		FullCalendarComponent,
		ChoixChantierComponent,
		OrderListFacturePaiementPipe,
		RestePayerPipe,
		InputSearchComponent,
		VisualiserPdfComponent,
		ChoixClientComponent,
		CategoryListComponent,
		NameModelComponent,
		UploadFileDialogComponent,
		MapToIterablePipe,
		RubriqueTagsComponent,
		PaimentDetailsComponent,
		AreplanifierFormComponent,
		ChangePasswordComponent,
    ClientSituationComponent
	],
	providers: [
		Historique
	],
	entryComponents: [PrixParFournisseurComponent, FillTagsComponent, NameModelComponent, AddTypekindComponent,
		UploadFileDialogComponent, AddClientPopComponent, AdddLigneComponent,
		AddmodeReglementComponent, AddAdresseComponent, AddlabelComponent, AddtypeDComponent,
		VisualiserPdfComponent, SendMailComponent, CreateDepenseComponent, CancelAvoirComponent,
		AddObservationComponent, AreplanifierFormComponent,
		ChangePasswordComponent
	]
})
export class CommonModules { }
