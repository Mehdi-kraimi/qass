import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { FileSystemFileEntry, UploadEvent } from 'ngx-file-drop';

declare var Toast: any;

@Component({
	selector: 'app-upload-file-dialog',
	templateUrl: './upload-file-dialog.component.html',
	styleUrls: ['./upload-file-dialog.component.scss']
})
export class UploadFileDialogComponent {

	file: File;
	requiredExtensions: string[] = [];

	constructor(
		private translate: TranslateService,
		public modal: NgbActiveModal) {
	}

	/**
	 * dropped or choose file
	 */
	dropped(event: UploadEvent) {

		for (const droppedFile of event.files) {
			// Is it a file?
			if (droppedFile.fileEntry.isFile) {
				const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
				fileEntry.file((file: File) => {
					if (
						(this.isHasRequiredExtensions() && this.isFileHasExtensions(file)) ||
						!this.isHasRequiredExtensions()
					) {
						this.file = file
					} else if (this.isHasRequiredExtensions()) {
						Toast.error(this.translate.instant('errors.formatInvalidDocument'));
					}

				});
			}
		}

	}


	isHasRequiredExtensions = () => this.requiredExtensions.length > 0

	isFileHasExtensions = (file: File) => this.IsFileHasExtension(file, this.requiredExtensions);

	IsFileHasExtension(file: File, extensions: string[]): boolean {
		if (file === null) { return false; }
		const filename = file.name;
		const extension = filename.substring(filename.lastIndexOf('.') + 1);
		return extensions.includes(extension);
	}

	/**
	 * save
	 */
	save() {
		this.modal.close(this.file)
	}
}
