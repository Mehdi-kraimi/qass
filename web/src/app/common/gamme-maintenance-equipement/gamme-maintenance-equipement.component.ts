import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SwalConfig } from 'app/Models/Entities/Maintenance/Gme.interface';
import { moisEnum } from 'app/Enums/Maintenance/mois.enum';
import { OperationMaintenance } from 'app/Models/Entities/Maintenance/GammeMaintenanceEquipement';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

declare var toastr: any;
declare var swal: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'gamme-maintenance-equipement',
	templateUrl: './gamme-maintenance-equipement.component.html',
	styleUrls: ['./gamme-maintenance-equipement.component.scss']
})

export class GammeMaintenanceEquipement implements OnInit, OnChanges {

	// tslint:disable-next-line:no-input-rename
	@Input('libelles') libelles: OperationMaintenance[] = [];
	// tslint:disable-next-line:no-input-rename
	@Input('load') load: { getOperations };
	// tslint:disable-next-line:no-input-rename
	@Input('readOnly') readOnly = false;

	public showLibelleAction: number = null;
	public showOperationAction: number = null;
	periodiciteVide = [{ mois: 1, value: false }, { mois: 2, value: false }, { mois: 3, value: false },
	{ mois: 4, value: false }, { mois: 5, value: false }, { mois: 6, value: false }, { mois: 7, value: false },
	{ mois: 8, value: false }, { mois: 9, value: false }, { mois: 10, value: false },
	{ mois: 11, value: false }, { mois: 12, value: false }];
	formInputs
	libelleCrud = {
		showAddLibelleform: false,
		indexLibelle_A_Modifier: null,
		ajouterPeriodiciteAuLibelleIndex: null
	}
	operationCrud = {
		indexLibelle_A_Modifie: null,
		indexOperation_A_Modifier: null,
		indexLibelle_Ajouter: null,
	}
	moisTransalttion = null;

	constructor(private translate: TranslateService) { }

	async ngOnInit() {
		if (this.moisTransalttion == null) {
			this.moisTransalttion = await this.getMonthsTranslation();
		}
	}
	ngOnChanges() {
		this.formInputs = {
			inputString: '',
			periodiciteList: this.periodiciteVide
		}
		if (this.load !== undefined) {
			this.load.getOperations = this.libelles;
		}
	}


	ajouterLot(): void {
		const names = this.libelles.map(x => x.name);
		if (!names.includes(this.formInputs.inputString)) {
			this.translate.get('equipement.errors').subscribe(text => {
				if (this.formInputs.inputString.length < 3) {
					toastr.warning(text.minLengthLibelle, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				} else {
					const libelle: OperationMaintenance = {
						name: this.formInputs.inputString,
						subOperations: [],
						periodicity: [],
					}
					this.libelles.push(libelle);
					this.initAll();
					toastr.success('cette libellé est ajouté avec succes', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		} else {
			toastr.warning('cette libellé est déja existe', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		}
	}

	modifierLot(): void {
		const names = this.libelles.map(x => x.name);
		if (this.libelleCrud.indexLibelle_A_Modifier != null && (!names.includes(this.formInputs.inputString) ||
			this.libelles[this.libelleCrud.indexLibelle_A_Modifier].name === this.formInputs.inputString)
		) {
			this.translate.get('equipement.errors').subscribe(text => {
				if (this.formInputs.inputString.length < 3) {
					toastr.warning(text.minLengthLibelle, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				} else {
					const libelle = this.libelles[this.libelleCrud.indexLibelle_A_Modifier];
					libelle.name = this.formInputs.inputString;
					this.initAll();
					toastr.success('cette libellé modifier avec succes', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

				}
			});
		} else {
			toastr.warning('cette libellé est déja existe', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		}
	}

	supprimerLibelle(indexLot): void {
		this.translate.get('equipement.deleteLibelle').subscribe(text => {
			swal(this.swalConfig(text)).then(isConfirm => {
				if (isConfirm) {
					this.libelles.splice(indexLot, 1);
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}


	ajouterOperation(): void {
		this.translate.get('equipement.errors').subscribe(text => {
			const periodiciteNb = this.formInputs.periodiciteList.filter(x => x.value === true).length;
			const inputLength = this.formInputs.inputString.length;
			if (inputLength < 3) {
				toastr.warning(text.minLengthNom, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			} else if (periodiciteNb === 0) {
				toastr.warning(text.periodicite, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			} else {
				const operations = this.libelles[this.operationCrud.indexLibelle_Ajouter].subOperations;
				const names = this.libelles.map(x => x.name);
				if (!names.includes(this.formInputs.inputString) && !operations.map(x => x.name).includes(this.formInputs.inputString)) {
					const { inputString, periodiciteList } = this.formInputs;
					operations.push({ name: inputString, periodicity: periodiciteList, id: '', subOperations: [] });
					this.initAll();
					toastr.success('cette operation ajouter avec succes', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

				} else {
					toastr.warning('cette operation est déja existe', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			}
		});

	}


	modifierOperation(): void {
		let name = this.libelles[this.operationCrud.indexLibelle_A_Modifie]
			.subOperations[this.operationCrud.indexOperation_A_Modifier].name;
		this.translate.get('equipement.errors').subscribe(text => {
			const periodiciteNb = this.formInputs.periodiciteList.filter(x => x.value === true).length;
			const inputLength = this.formInputs.inputString.length;
			if (inputLength < 3) {
				toastr.warning(text.minLengthNom, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			} else if (periodiciteNb === 0) {
				toastr.warning(text.periodicite, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			} else {
				const names = this.libelles[this.operationCrud.indexLibelle_A_Modifie].subOperations.map(x => x.name);
				if ((name == this.formInputs.inputString && names.includes(this.formInputs.inputString)) ||
					(name != this.formInputs.inputString && !names.includes(this.formInputs.inputString))
				) {
					this.libelles[this.operationCrud.indexLibelle_A_Modifie]
						.subOperations[this.operationCrud.indexOperation_A_Modifier] = {
						name: this.formInputs.inputString,
						periodicity: this.formInputs.periodiciteList,
						id: '', subOperations: []
					};
					this.initAll();
					toastr.success('cette operation modifier avec succes', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });

				} else {
					toastr.warning('cette operation est déja existe', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			}
		});
	}


	supprimerOperation(indexLibelle: number, indexOperation: number): void {
		this.initAll();
		this.translate.get('equipement.deleteOperation').subscribe(text => {
			swal(this.swalConfig(text)).then(isConfirm => {
				if (isConfirm) {
					this.libelles[indexLibelle].subOperations.splice(indexOperation, 1);
				} else {
					toastr.success(text.failed, text.title, { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
				}
			});
		});
	}

	swalConfig(text): SwalConfig {
		return {
			title: text.title,
			text: text.question,
			icon: 'warning',
			buttons: {
				cancel: {
					text: text.cancel,
					value: null,
					visible: true,
					className: '',
					closeModal: true
				},
				confirm: {
					text: text.confirm,
					value: true,
					visible: true,
					className: '',
					closeModal: true
				}
			}
		}
	}

	drop(event: CdkDragDrop<string[]>) {
		if (event.previousContainer === event.container) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
		} else {
			transferArrayItem(event.previousContainer.data,
				event.container.data,
				event.previousIndex,
				event.currentIndex);
		}
	}

	async initAll() {

		this.formInputs = {
			inputString: '',
			periodiciteList: [{ mois: 1, value: false }, { mois: 2, value: false }, { mois: 3, value: false },
			{ mois: 4, value: false }, { mois: 5, value: false }, { mois: 6, value: false },
			{ mois: 7, value: false }, { mois: 8, value: false }, { mois: 9, value: false },
			{ mois: 10, value: false }, { mois: 11, value: false }, { mois: 12, value: false }],
		}
		this.libelleCrud = {
			showAddLibelleform: false,
			indexLibelle_A_Modifier: null,
			ajouterPeriodiciteAuLibelleIndex: null
		}
		this.operationCrud = {
			indexLibelle_Ajouter: null,
			indexLibelle_A_Modifie: null,
			indexOperation_A_Modifier: null,
		}
	}
	openLabelleEditForm(index: number) {

		this.initAll();
		const libelle = this.libelles[index];
		this.formInputs.inputString = libelle.name;
		if (libelle.periodicity != null) {
			this.formInputs.periodiciteList = libelle.periodicity;
		}
		this.libelleCrud.indexLibelle_A_Modifier = index;
	}

	openOperationEditForm(indexLibelle: number, indexOperation: number) {

		this.initAll();
		const operation = this.libelles[indexLibelle].subOperations[indexOperation];

		this.formInputs.inputString = operation.name;
		this.formInputs.periodiciteList = operation.periodicity;
		this.operationCrud.indexOperation_A_Modifier = indexOperation;
		this.operationCrud.indexLibelle_A_Modifie = indexLibelle;
	}

	getMois(index): string {

		try {
			const key = moisEnum[index + 1];
			const t = this.moisTransalttion[key];
			return t;
		} catch (err) {
			return '';
		}
	}

	async getMonthsTranslation(): Promise<any> {
		return new Promise((resolve, reject) => {
			this.translate.get('labels.mois').subscribe(T => {
				resolve(T === 'labels.mois' ? null : T);
			});
		});

	}
	shoowOperation(indexLot, idOperation) {
		if (this.operationCrud.indexLibelle_A_Modifie !== indexLot) {
			return true;
		} else {
			if (this.operationCrud.indexOperation_A_Modifier !== idOperation) {
				return true;
			} else {
				return false;
			}
		}
	}

	ouverirPeriodiciteAuLibelle(index: number) {
		this.initAll();
		this.libelleCrud.ajouterPeriodiciteAuLibelleIndex = index;
		if (this.libelles[index].periodicity.length !== 0) {
			this.formInputs.periodiciteList = this.libelles[index].periodicity;
		}
	}

	ajouterPeriodiciteAuLibelle() {

		this.translate.get('equipement.errors').subscribe(text => {
			const periodiciteNb = this.formInputs.periodiciteList.filter(x => x.value === true).length;
			if (periodiciteNb === 0) {
				toastr.warning(text.periodicite, '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
			} else {
				this.libelles[this.libelleCrud.ajouterPeriodiciteAuLibelleIndex].periodicity = this.formInputs.periodiciteList;
				this.initAll();
			}
		});
	}
}
