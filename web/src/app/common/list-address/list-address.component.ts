import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Adresse } from 'app/Models/Entities/Commun/Adresse';
declare var jQuery: any;
declare var swal: any;

@Component({
	selector: 'list-address',
	templateUrl: './list-address.component.html',
	styleUrls: ['./list-address.component.scss']
})

export class ListAddressComponent implements OnInit {

	form;
	editForm;
	indexEditAddress;
	afficherList = false; /* Afficher la liste des département et des villes si il a dans la bdd */
	@Input('addresses') addresses: Adresse[] = [];
	@Input('show') show: boolean = true;
	@Output('remove') removeAddressByIndex = new EventEmitter();
	@Output('listContacts') listAdresse = new EventEmitter();
	//afficher uniquement le formulaire
	@Input('displayJustTheForm') displayJustTheForm: boolean = false;
	@Input('InputsIsOutSide') InputsIsOutSide = false;

	@Input('addModalId') addModalId = 'addAddress'
	@Input('hideAddBtn') hideAddBtn: boolean = false;
	@Input('editModalId') editModalId = 'editAddress';

	constructor(
		private translate: TranslateService,
		private fb: FormBuilder) { }

	ngOnInit() {
		this.form = this.fb.group({
			'designation': [null, [Validators.required]],
			'street': [null, [Validators.required]],
			'complement': [null],
			'city': [null, [Validators.required]],
			'postalCode': [null, [Validators.required]],
			'department': [null],
			'countryCode': [null],
			'isDefault': [null]
		})
		setTimeout(() => {
			if (this.addresses && this.addresses.length === 0) {
				this.form.controls['isDefault'].setValue(true);
			}
		}, 1000)
	}

	removeAddress(addressIndex) {
		this.translate.get('list.delete').subscribe(text => {
			swal({
				title: text.title,
				text: '',
				icon: 'warning',
				buttons: {
					cancel: {
						text: text.cancel,
						value: null,
						visible: true,
						className: '',
						closeModal: false
					},
					confirm: {
						text: text.confirm,
						value: true,
						visible: true,
						className: '',
						closeModal: false
					}
				}
			}).then(isConfirm => {
				if (isConfirm) {
					this.removeAddressByIndex.emit({ addressIndex });
					swal(text.success, '', 'success');
				} else {
					swal(text.cancel, '', 'error');
				}
			});
		});
	}
	createId(name) {
		return '#' + name;
	}

	add() {

		if (this.form.valid) {
			let values = this.form.value;
			if (values.isDefault && this.addresses.length > 0) {
				this.addresses.map(x => x.isDefault = false);
			}
			this.addresses.push(values);
			this.listAdresse.emit(this.addresses);
			this.form.reset();
			jQuery(this.createId(this.addModalId)).modal('hide');

		}
	}

	chargeAddress(i) {
		const address = this.addresses[i]
		this.indexEditAddress = i;
		this.editForm = this.fb.group({
			'designation': [address.designation, [Validators.required]],
			'street': [address.street, [Validators.required]],
			'complement': [address.complement],
			'department': [address.department],
			'city': [address.city, [Validators.required]],
			'postalCode': [address.postalCode],
			'countryCode': [address.countryCode],
			'isDefault': [address.isDefault]
		});
	}

	update() {
		if (this.editForm.valid) {
			const values = this.editForm.value;
			if (values.isDefault) {
				this.addresses.map(x => x.isDefault = false)
			}
			this.addresses[this.indexEditAddress] = values;
			this.listAdresse.emit(this.addresses);
			this.editForm.reset();
			jQuery(this.createId(this.editModalId)).modal('hide');
		}
	}

	get f() { return this.form.controls; }

	get fEdit() { return this.editForm.controls; }

	modalDismiss() {
		jQuery(this.createId(this.addModalId)).modal('hide');
		jQuery(this.createId(this.editModalId)).modal('hide');
	}

}
