import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from 'app/app-settings/app-settings';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'gamme-maintenance-equipement-container-component',
	templateUrl: './gamme-maintenance-equipement-container.component.html',
	styleUrls: ['./gamme-maintenance-equipement-container.component.scss']
})

// tslint:disable-next-line:class-name
export class gammeMaintenanceEquipementContainerComponent implements OnInit {
	emitter: any = {};
	constructor(
		private translate: TranslateService,
		public dialogRef: MatDialogRef<gammeMaintenanceEquipementContainerComponent>,

		@Inject(MAT_DIALOG_DATA) public data: { listes:[], readOnly: boolean, nom: string }

	) { }
	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
	}
	getOperation() {
		return this.emitter.getOperations;
	}

	save() {
		const result = {
			listes: this.getOperation(),
			nom: this.data.nom
		}
		this.dialogRef.close(result);
	}
}