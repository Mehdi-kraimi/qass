import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientSituationComponent } from './client-situation.component';

describe('ClientSituationComponent', () => {
  let component: ClientSituationComponent;
  let fixture: ComponentFixture<ClientSituationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientSituationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientSituationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
