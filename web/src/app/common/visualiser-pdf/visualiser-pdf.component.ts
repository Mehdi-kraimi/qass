import { Component, OnInit, Input, OnChanges, Inject } from '@angular/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-visualiser-pdf',
	templateUrl: './visualiser-pdf.component.html',
	styleUrls: ['./visualiser-pdf.component.scss']
})
export class VisualiserPdfComponent implements OnInit ,  OnChanges {

	displayData;
	// tslint:disable-next-line:no-input-rename
	@Input('base64') base64 = null;
	docType = '';
	document;

	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		public dialogRef: MatDialogRef<VisualiserPdfComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { base64: string, docType: string, doc },
		private translate: TranslateService) {
		this.base64 = this.data.base64;
		this.docType = this.data.docType;
		this.document = this.data.doc;
	}

	ngOnInit() {

		this.VisualiserPDF();
	}

	ngOnChanges() {

		this.VisualiserPDF();
	}

	VisualiserPDF() {

		if (this.base64 == null) { return; }

		// Get time stamp for fileName.
		const stamp = new Date().getTime();

		const fileType = 'application/pdf';

		const fileData = AppSettings._base64ToArrayBuffer(this.base64);

		const extension = 'pdf';

		const pdfSrc = AppSettings.printPdf(fileData, 'document_' + stamp + '.' + extension, fileType, extension);

		this.displayData = {
			pdfSource: {
				url: pdfSrc
			},
			base64: this.base64
		}
	}

	generatePDF() {

		this.service.visualPdf(this.docType, this.document.id).subscribe(res => {
			// Get time stamp for fileName.
			const stamp = new Date().getTime();

			// file type
			const fileType = 'application/pdf';

			// file extension
			const extension = 'pdf';

			AppSettings.downloadBase64(',' + res.value, this.document.reference + '.' + extension, fileType, extension);

		});
	}

	close() {
		this.dialogRef.close();
	}

}
