import { Component, OnInit, Inject } from '@angular/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { ArticleType } from 'app/Models/Entities/Commun/article-type';
import { TranslateService } from '@ngx-translate/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IGenericRepository } from 'app/shared/repository/igeneric-repository';
import { SortDirection } from 'app/Models/Model/filter-option';
import { ApiUrl } from 'app/Enums/Configuration/api-url.enum';
import { MinimalProductDetails } from 'app/Models/Entities/Commun/MinimalProductDetails';
import { MinimalLotDetails } from 'app/Models/Entities/Biobiotheque/Lots';
declare var toastr: any;

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'select-lot',
	templateUrl: './select-lot.component.html',
	styleUrls: ['./select-lot.component.scss']
})
export class SelectLotComponent implements OnInit {

	search = ''; // zone de recherche
	page = 1; // Current page des
	lots = [];
	totalPage = 1; // total des pages
	loadingFinished = false; // si la liste chargé
	lotsTmp: {
		product: (MinimalProductDetails | MinimalLotDetails), quantity: number,
		type: number, remise: number
	}[] = []; // les Produit choisi dans popup
	articleType: ArticleType = new ArticleType();
	selected: { product: (MinimalProductDetails | MinimalLotDetails), quantity: number, type: number, remise: number }[] = [];


	constructor(
		@Inject('IGenericRepository') private service: IGenericRepository<any>,
		private translate: TranslateService,
		public dialogRef: MatDialogRef<SelectLotComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { isAvoir: boolean }
	) { }

	ngOnInit() {
		this.translate.setDefaultLang(AppSettings.lang);
		this.translate.use(AppSettings.lang);
		this.getlots();
	}

	getlots() {
		this.lotsTmp = [];
		this.service.getAllPagination(ApiUrl.Lot, {
			SearchQuery: this.search,
			Page: this.page,
			PageSize: 10,
			OrderBy: 'reference',
			SortDirection: SortDirection.Descending
		}).subscribe(res => {
			//	if (Array.isArray(res.value))
			if (res.value.length > 0) { this.lots = [...this.lots, ...res.value]; }
			this.lots = this.lots.filter((l) => !this.selected.some(e => e.product.id === l.id));
			this.totalPage = res.count;
			this.loadingFinished = true;
		});
	}

	checkElement(index, value) {
		this.selected.unshift({
			product: !this.data.isAvoir ? this.lots[index] : this.formatLotIsAvoir(this.lots[index]),
			quantity: value != null ? +value : 1,
			type: this.articleType.lot,
			remise: 0
		}
		);
		this.lots.splice(index, 1);
	}

	IncheckElement(index) {
		const lotselected = !this.data.isAvoir ? this.selected[index].product : this.formatLotIsAvoir(this.selected[index].product);
		this.lots.unshift(lotselected as any);
		this.selected.splice(index, 1);
	}


	// On cas de scroll dans popup des lots
	onScrollLots() {
		if (this.totalPage !== this.page) {
			this.page++;
			this.getlots();
		}
	}

	// C'est pour change la quantite d'un lot
	changeQuantiteLot(index, value, changed) {
		const lot = this.selected[index];
		if (value !== null) { lot.quantity += value; }
		// tslint:disable-next-line:radix
		if (changed !== null && changed !== '') { lot.quantity = parseInt(changed); }
		if (lot.quantity <= 0) { lot.quantity = 1; }
	}


	formatLotIsAvoir(lots) {
		if (this.data.isAvoir) {
			lots.products = lots.products.map(x => {
				{
					x.productDetails.totalHT = x.productDetails.totalHT * (-1),
						x.productDetails.materialCost = x.productDetails.materialCost * (-1);
					x.productDetails.hourlyCost = x.productDetails.hourlyCost * (-1);
				} return x;
			})
			return lots;
		}
	}

	// Recherche des articles dans popup
	searchLots() {
		this.page = 1;
		this.lots = [];
		this.getlots();

	}
	save() {
		if (this.selected.length === 0) {
			toastr.warning('text.serveur', '', { positionClass: 'toast-top-center', containerId: 'toast-top-center' });
		} else {
			this.dialogRef.close(this.selected);
		}
	}
}
