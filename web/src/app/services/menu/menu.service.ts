import { Injectable } from '@angular/core';
import { ROUTES } from 'app/shared/sidebar/sidebar-routes.config';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class MenuService {

	//#region Properties

	opened = true;
	active = false;

	menuItems = [];
	subscriptions: Subscription = new Subscription();

	//#endregion

	//#region Lifecycle

	constructor(
		private router: Router
	) {
		this.initMenu();

		this.subscriptions.add(
			this.router.events
				.pipe(filter(e => e instanceof NavigationEnd))
				.subscribe(e => {
					this.active = !this.router.url.includes('login');
				})
		);
	}

	//#endregion

	//#region Methods

	/**
	 * Initializes menu items
	 */
	initMenu(): void {
		const routes = ROUTES.filter((menuItem: any) => menuItem.actif === true);

		this.menuItems = [
			{
				label: 'Accueil',
				iconStale: this.getIcon('accueil'),
				iconActive: this.getIcon('accueil', true),
				items: []
			}
		];

		routes.forEach((route) => {
			if (route.isTitleSection) {
				this.menuItems.push({
					label: route.title,
					iconStale: this.getIcon(route.title.toLowerCase()),
					iconActive: this.getIcon(route.title.toLowerCase(), true),
					items: []
				});
			} else {
				this.menuItems[this.menuItems.length - 1].items.push({
					label: route.title,
					route: route.path
				});
			}
		});
	}

	/**
	 * Checks if a group is active
	 */
	isGrouActive = (group: any) => group.items.some((item: any) => this.router.url.includes(item.route) && item.route)

	/**
	 * Retrieves the proper link of the icon
	 *
	 * @param route The route to point to
	 * @param active Whether it's on active mode or not
	 */
	getIcon(route: string, active: boolean = false): string {
		const path = `./assets/app/images/Icons/${active ? 'active/' : ''}`;

		switch (route) {
			case 'accueil': return `${path}home.svg`;
			case 'contacts': return `${path}users.svg`;
			case 'gestion chantiers': return `${path}hoist.svg`;
			case 'gestion maintenance': return `${path}repair.svg`;
			case 'facturation': return `${path}file-text.svg`;
			case 'bibliothèque': return `${path}grid.svg`;
			case 'comptabilité': return `${path}dollar-sign.svg`;
			case 'administration': return `${path}settings.svg`;
			default: return '';
		}
	}

	//#endregion
}
