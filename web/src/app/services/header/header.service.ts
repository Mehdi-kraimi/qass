import { Injectable } from '@angular/core';

export interface BreadcrumbItem {
	label: string;
	route?: any;
}

@Injectable({
	providedIn: 'root'
})
export class HeaderService {

	//#region Properties

	public breadcrumb: BreadcrumbItem[] = [];

	//#endregion

	//#region Lifecycle

	constructor() { }

	//#endregion
}
