import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from 'app/app-settings/app-settings';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class PublishingService {

	/**
	* the api end point of the type product service
	*/
	public static endPoint = AppSettings.API_ENDPOINT + 'Publishing/';

	constructor(private http: HttpClient) { }

	/**
	 * extract tags from document
	 * @param file the file that we want to extract tags
	 */
	ExtractTagsFromDocument(url: string, file: File) {
		const formData = new FormData();
		formData.append('file', file, file.name);
		return this.http.post(`${AppSettings.API_ENDPOINT}${url}`, formData, {
			reportProgress: true,
			observe: 'events',
			headers: new HttpHeaders()
				.set('Authorization', `Bearer ${localStorage.getItem('token')}`)


		});
	}

	/**
	 * upload contract
	 * @param file the file that we want to extract tags
	 */
	UploadContract(url: string, file: File) {
		const formData = new FormData();
		formData.append('file', file, file.name);
		return this.http.post(`${AppSettings.API_ENDPOINT}${url}`, formData, {
			reportProgress: true,
			observe: 'events',
			headers: new HttpHeaders()
				.set('Authorization', `Bearer ${localStorage.getItem('token')}`)
		});
	}


	/**
	 * update the given publishing contract to the database
	 * @param idPublishingContract the id of the publishing contract to be updated
	 * @param typeTask the publishing contract to be updated
	 */
	UpdatePublishingContract(idPublishingContract: number, typeTask) {
		return this.http.put(`${PublishingService.endPoint}Update/${idPublishingContract}`, typeTask);
	}

	/**
	 * delete the given publishing contract
	 * @param idPublishingContract the id of the publishing contract to be updated
	 */
	DeletePublishingContract(idPublishingContract: number) {
		return this.http.delete(`${PublishingService.endPoint}delete/${idPublishingContract}`);
	}

	/**
	  * download file
	  * @param id the id of document
	  */
	download(url: string, id: number) {
		return this.http.get(
			`${AppSettings.API_ENDPOINT}${url}/${id}`, {
			reportProgress: true,
			observe: 'events',
			responseType: 'blob',
			headers: new HttpHeaders()
				.set('Authorization', `Bearer ${localStorage.getItem('token')}`)
		});
	}
	/**
	  * download file
	  * @param id the id of document
	  */
	downloadPublishingContrat(url: String) {
		return this.http.get(
			`${AppSettings.API_ENDPOINT}${url}`, {
			reportProgress: true,
			observe: 'events',
			responseType: 'blob',
			headers: new HttpHeaders()
				.set('Authorization', `Bearer ${localStorage.getItem('token')}`)
		});
	}


}
