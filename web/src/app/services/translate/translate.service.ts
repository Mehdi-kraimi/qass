import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { I18nPages } from 'app/Enums/i18n-pages.enum';
import { TranslateConfiguration } from 'app/libraries/translation';

@Injectable({
	providedIn: 'root'
})
export class TranslateService {

	constructor() { }

	public setTranslateLoader(http: HttpClient, folder) {
		const folderDirectory = I18nPages[folder];
		return new TranslateHttpLoader(http, `${TranslateConfiguration.PATH_FILES}${folderDirectory}/`, TranslateConfiguration.FILE_EXTENTION);
	}

	public setMultiTranslateLoader(http: HttpClient, folders) {
		const directoryArray = [];
		folders.forEach(element => {
			directoryArray.push({ prefix: `${TranslateConfiguration.PATH_FILES}${I18nPages[element]}/`, suffix: '.json' });
		});
		return new MultiTranslateHttpLoader(http, directoryArray);
	}
}
