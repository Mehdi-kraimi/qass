﻿namespace Axiobat.Services.Contacts
{
    using Application.Data;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.Products;
    using AutoMapper;
    using Axiobat.Application.Exceptions;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// a class that implement <see cref="ISupplierService"/>
    /// </summary>
    public partial class SupplierService
    {
        /// <summary>
        /// retrieve the list of products of the supplier
        /// </summary>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="supplierId">the id of the supplier</param>
        /// <returns>the list of the out put result</returns>
        public Task<ListResult<TOut>> GetSupplierProductsAsync<TOut>(string supplierId)
            => _productService.GetSupplierProductsAsync<TOut>(supplierId);


        public Task<ExternalPartnerMinimalInfo[]> GetSuppliersForImportAsync()
        {
            return _dataAccess.GetAllAsync(options =>
            {
                options.AddDefaultIncludes(false);
            }
            , supplier => new ExternalPartnerMinimalInfo { Id = supplier.Id, Name = supplier.FullName, Reference = supplier.Reference });
        }
    }

    /// <summary>
    /// partial part for <see cref="SupplierService"/>
    /// </summary>
    public partial class SupplierService : ExternalPartnersService<Supplier>, ISupplierService
    {
        private readonly IProductService _productService;

        /// <summary>
        /// name of the entity
        /// </summary>
        protected override string EntityName => nameof(Supplier);

        public SupplierService(
            ISupplierDataAccess dataAccess,
            ISynchronizationResolverService synchronizationResolverService,
            IProductService productService,
            IFileService fileService,
            ILoggedInUserService currentUser,
            IValidationService validationsService,
            IHistoryService historyService,
            ITranslationService translationService,
            IApplicationConfigurationService appConfigService,
            ILoggerFactory loggerFactory,
            IMapper mapper
        ) : base(dataAccess, synchronizationResolverService, fileService, validationsService, historyService, currentUser, appConfigService, translationService, loggerFactory, mapper)
        {
            _productService = productService;
        }

        protected override async Task InDelete_BeforDeleteAsync(Supplier supplier)
        {
            var validation = await _validate.HasDocumentsAsync(supplier);
            if (!validation.IsSuccess)
                throw new ValidationException(
                    message: "you can't delete this user is associated with other documents",
                    messageCode: MessageCode.DocumentRelationshipConstraint,
                    validationInfo: validation);
        }
    }
}
