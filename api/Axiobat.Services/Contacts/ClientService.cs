﻿namespace Axiobat.Services.Contacts
{
    using App.Common;
    using Application.Data;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Axiobat.Application.Models;
    using Axiobat.Services.Accounting;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// a service implementation for <see cref="IClientService"/>
    /// </summary>
    public partial class ClientService
    {
        /// <summary>
        /// check if the given code is unique
        /// </summary>
        /// <param name="code">the code to be checked</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsCodeUniqueAsync(string code)
            => !await _dataAccess.IsExistAsync(e => e.Code == code);

        /// <summary>
        /// get the client documents details
        /// </summary>
        /// <param name="clientId">the id of the client to get the document details for it</param>
        /// <returns>the documents details result</returns>
        public async Task<ClientDocumentDetails> GetClientDocumentsDetailsAsync(string clientId)
        {
            var charts = await _chartOfAccountsService.GetChartOfAccountsAsync();

            var quotes = await _quoteDataAccess.GetAllAsync(options => options.AddPredicate(e => e.ClientId == clientId));
            var invoices = await _invoiceDataAccess.GetAllAsync(options => options.AddPredicate(e => e.ClientId == clientId));
            var CreditNotes = await _creditNoteDataAccess.GetAllAsync(options => options.AddPredicate(e => e.ClientId == clientId));

            var workshops = await _workshopDataAccess.GetAllAsync(options => options.AddPredicate(e => e.ClientId == clientId));
            var contracts = await _maintenanceContractDataAccess.GetAllAsync(options => options.AddPredicate(e => e.ClientId == clientId));

            var operationSheet = await _operationSheetDataAccess.GetAllAsync(options => options.AddPredicate(e => e.ClientId == clientId));
            var operationSheetMaintence = await _maintenanceOperationSheetDataAccess.GetAllAsync(options => options.AddPredicate(e => e.ClientId == clientId));

            return new ClientDocumentDetails(
                AnalyticsService.GetQuoteDetails(quotes),
                AnalyticsService.GetInvoiceDetails(invoices),
                AnalyticsService.GetWorkshopDetails(workshops, charts[ChartOfAccounts.Expenses]),
                AnalyticsService.GetContractDetails(contracts),
                AnalyticsService.GetOperationSheetDetails(operationSheet, operationSheetMaintence),
                AnalyticsService.GetClientTurnOverDetails(invoices, CreditNotes)
            );
        }

        /// <summary>
        /// get the turn over details of the given client
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public async Task<ClientTurnoverDetail> GetClientTurnOverDetailsAsync(string clientId, int year)
        {
            var startDate = new DateTime(year, 01, 01);
            var endDate = new DateTime(year, 12, 31);

            var documents = await GetClientDocumentsForTurnOverDetailsAsync(clientId, startDate, endDate);

            return new ClientTurnoverDetail
            {
                PerMonthTurnOver = DateHelper.GetDates(startDate, endDate)
                    .Select(date => new PerMonthTotalDetails(date.Year, date.Month, documents
                        .Where(e => DateHelper.IsBetween(e.CreationDate, date, date.End()))
                        .Sum(e =>
                        {
                            var totalHt = e.TotalHT;
                            if (e.DocumentType == Domain.Enums.DocumentType.CreditNote)
                                totalHt = Math.Abs(totalHt) * -1;

                            return totalHt;
                        })))
                    .ToArray(),
            };
        }

        private async Task<IEnumerable<MinimalDocumentModel>> GetClientDocumentsForTurnOverDetailsAsync(string clientId, DateTime startDate, DateTime endDate)
        {
            var invoices = await _invoiceDataAccess.GetAllAsync(options =>
            {
                options.AddPredicate(e => e.ClientId == clientId &&
                                          e.CreationDate.Date >= startDate.Date &&
                                          e.CreationDate.Date <= endDate.Date);
            },
            invoice => new MinimalDocumentModel
            {
                DocumentType = invoice.DocumentType,
                Id = invoice.Id,
                Reference = invoice.Reference,
                Status = invoice.Status,
                TotalHT = invoice.GetTotalHT(),
                TotalTTC = invoice.GetTotalTTC(),
                CreationDate = invoice.CreationDate,
                DueDate = invoice.DueDate
            });

            var CreditNotes = await _invoiceDataAccess.GetAllAsync(options =>
            {
                options.AddPredicate(e => e.ClientId == clientId &&
                                          e.CreationDate.Date >= startDate.Date &&
                                          e.CreationDate.Date <= endDate.Date);
            },
            creditNote => new MinimalDocumentModel
            {
                DocumentType = creditNote.DocumentType,
                Id = creditNote.Id,
                Reference = creditNote.Reference,
                Status = creditNote.Status,
                TotalHT = creditNote.GetTotalHT(),
                TotalTTC = creditNote.GetTotalTTC(),
                CreationDate = creditNote.CreationDate,
                DueDate = creditNote.DueDate
            });

            return invoices.Concat(CreditNotes);
        }
    }

    /// <summary>
    /// the partial part for <see cref="ClientService"/>
    /// </summary>
    public partial class ClientService : ExternalPartnersService<Client>, IClientService
    {
        private readonly IQuoteDataAccess _quoteDataAccess;
        private readonly IInvoiceDataAccess _invoiceDataAccess;
        private readonly IPaymentDataAccess _paymentDataAccess;
        private readonly ICreditNoteDataAccess _creditNoteDataAccess;
        private readonly IChartOfAccountsService _chartOfAccountsService;
        private readonly IOperationSheetDataAccess _operationSheetDataAccess;
        private readonly IConstructionWorkshopDataAccess _workshopDataAccess;
        private readonly IMaintenanceContractDataAccess _maintenanceContractDataAccess;
        private readonly IMaintenanceOperationSheetDataAccess _maintenanceOperationSheetDataAccess;

        /// <summary>
        /// name of the entity
        /// </summary>
        protected override string EntityName => nameof(Client);

        public ClientService(
            IClientDataAccess dataAccess,
            IQuoteDataAccess quoteDataAccess,
            IInvoiceDataAccess invoiceDataAccess,
            IChartOfAccountsService chartOfAccountsService,
            IMaintenanceContractDataAccess maintenanceContractDataAccess,
            IConstructionWorkshopDataAccess constructionWorkshopDataAccess,
            ISynchronizationResolverService synchronizationResolverService,
            IMaintenanceOperationSheetDataAccess maintenanceOperationSheetDataAccess,
            IOperationSheetDataAccess operationSheetDataAccess,
            ICreditNoteDataAccess creditNoteDataAccess,
            IPaymentDataAccess paymentDataAccess,
            IFileService fileService,
            ILoggedInUserService currentUser,
            IValidationService validationsService,
            IHistoryService historyService,
            ITranslationService translationService,
            IApplicationConfigurationService appConfigService,
            ILoggerFactory loggerFactory,
            IMapper mapper
        ) : base(dataAccess, synchronizationResolverService, fileService, validationsService, historyService, currentUser, appConfigService, translationService, loggerFactory, mapper)
        {
            _quoteDataAccess = quoteDataAccess;
            _invoiceDataAccess = invoiceDataAccess;
            _chartOfAccountsService = chartOfAccountsService;
            _maintenanceContractDataAccess = maintenanceContractDataAccess;
            _workshopDataAccess = constructionWorkshopDataAccess;
            _maintenanceOperationSheetDataAccess = maintenanceOperationSheetDataAccess;
            _operationSheetDataAccess = operationSheetDataAccess;
            this._creditNoteDataAccess = creditNoteDataAccess;
            this._paymentDataAccess = paymentDataAccess;
        }
    }


}
