﻿namespace Axiobat.Services.Accounting
{
    using App.Common;
    using Application.Data;
    using Application.Enums;
    using Application.Models;
    using Application.Services;
    using Application.Services.Accounting;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the accounting service
    /// </summary>
    public partial class AccountingService
    {
        private async Task<PagedResult<JournalEntry>> GetSalesJournalAsync(JournalFilterOptions filterOptions)
        {
            var charts = await _chartOfAccountsService.GetChartOfAccountsAsync();

            var journalCode = T("VENTE");
            var defaultClientCode = charts[ChartOfAccounts.General, ChartAccountItem.Clients];

            // first set the dates ranges
            _dateService.SetDateRange(filterOptions);

            // get the required data
            var invoices = await _dataAccess.GetInvoicesAsync(filterOptions);
            var creditNotes = await _dataAccess.GetCreditNoteAsync(filterOptions);

            // format data
            var journalEntries = new List<JournalEntry>();

            journalEntries.AddRange(ProcessCreditNotes(creditNotes, charts, journalCode, defaultClientCode.Code));
            journalEntries.AddRange(ProcessNormalInvoices(invoices, charts, journalCode, defaultClientCode.Code));
            journalEntries.AddRange(ProcessClotureSituationInvoices(invoices, charts, journalCode, defaultClientCode.Code));

            // return result
            return Result.PagedSuccess(journalEntries, invoices);
        }

        private IEnumerable<JournalEntry> ProcessCreditNotes(
            CreditNote[] creditNotes,
            ChartOfAccounts charts,
            string accountingCode,
            string clienCode)
        {
            var creditNoteCode = charts[ChartOfAccounts.General, ChartAccountItem.CreditNote];
            var result = new List<JournalEntry>();

            foreach (var document in creditNotes)
            {
                var documentClientCode = document.Client.AccountingCode.IsValid()
                    ? document.Client.AccountingCode
                    : clienCode;

                var taxDetails = document.OrderDetails.GetTaxDetails();
                var productsDetails = document.OrderDetails.GetAllProduct().Where(e => !(e.Product.Category is null));

                // create the first journal entry
                var journalEntry = new JournalEntry
                {
                    JournalCode = accountingCode,
                    PieceNumber = document.Reference,
                    EntryDate = document.CreationDate,
                    EntryType = JournalEntryType.Main,
                    ChartAccount = documentClientCode,
                    ExternalPartner = document.Client.FullName,
                    Amount = document.GetTotalTTC(),
                    DocumentType = DocumentType.CreditNote,
                };

                // add taxes
                journalEntry.Credits.AddRange(taxDetails
                   .Select(e => new JournalEntry
                   {
                       JournalCode = accountingCode,
                       Amount = e.TotalTax,
                       PieceNumber = document.Reference,
                       EntryDate = document.CreationDate,
                       ExternalPartner = document.Client.FullName,
                       EntryType = JournalEntryType.TaxDetails,
                       ChartAccount = charts.GetTaxCode(e.Tax),
                       DocumentType = DocumentType.CreditNote,
                   }));

                // add products
                journalEntry.Credits.AddRange(productsDetails
                    .GroupBy(e => e.Product.Category)
                    .Select(e => new
                    {
                        e.Key.ChartAccountItem,
                        Total = e.Sum(p => p.Quantity * p.Product.TotalHT)
                    })
                    .Where(e => !(e is null) && (e.ChartAccountItem != null))
                    .Select(e => new JournalEntry
                    {
                        Amount = e.Total,
                        JournalCode = accountingCode,
                        PieceNumber = document.Reference,
                        EntryDate = document.CreationDate,
                        ChartAccount = creditNoteCode.Code,
                        DocumentType = DocumentType.CreditNote,
                        ExternalPartner = document.Client.FullName,
                        EntryType = JournalEntryType.ProductCategory,
                    }));

                result.Add(journalEntry);
                //LogJournalEntry(document.Id, journalEntry);
            }

            return result;
        }

        private IEnumerable<JournalEntry> ProcessNormalInvoices(
            Invoice[] invoices,
            ChartOfAccounts charts,
            string accountingCode,
            string clienCode)
        {
            var result = new List<JournalEntry>();

            foreach (var document in invoices.Where(e => e.TypeInvoice == InvoiceType.General))
            {
                var documentClientCode = document.Client.AccountingCode.IsValid()
                    ? document.Client.AccountingCode
                    : clienCode;

                var taxDetails = document.OrderDetails.GetTaxDetails();
                var productsDetails = document.OrderDetails.GetAllProduct().Where(e => !(e.Product.Category is null));

                var journalEntry = new JournalEntry
                {
                    JournalCode = accountingCode,
                    PieceNumber = document.Reference,
                    EntryDate = document.CreationDate,
                    EntryType = JournalEntryType.Main,
                    ChartAccount = documentClientCode,
                    DocumentType = DocumentType.Invoice,
                    ExternalPartner = document.Client.FullName,
                    Amount = document.GetTotalTTC(),
                };

                journalEntry.Credits.AddRange(taxDetails
                    .Select(e => new JournalEntry
                    {
                        Amount = e.TotalTax,
                        JournalCode = accountingCode,
                        PieceNumber = document.Reference,
                        EntryDate = document.CreationDate,
                        ExternalPartner = document.Client.FullName,
                        EntryType = JournalEntryType.TaxDetails,
                        ChartAccount = charts.GetTaxCode(e.Tax),
                    }));

                journalEntry.Credits.AddRange(productsDetails
                    .GroupBy(e => e.Product.Category)
                    .Select(e => new
                    {
                        e.Key.ChartAccountItem,
                        Total = e.Sum(p => p.Quantity * p.Product.TotalHT)
                    })
                    .Where(e => !(e is null) && (e.ChartAccountItem != null))
                    .Select(e => new JournalEntry
                    {
                        Amount = e.Total,
                        JournalCode = accountingCode,
                        PieceNumber = document.Reference,
                        EntryDate = document.CreationDate,
                        DocumentType = DocumentType.Invoice,
                        ChartAccount = e.ChartAccountItem.Code,
                        ExternalPartner = document.Client.FullName,
                        EntryType = JournalEntryType.ProductCategory,
                    }));

                result.Add(journalEntry);
                //LogJournalEntry(document.Id, journalEntry);
            }

            return result;
        }

        private IEnumerable<JournalEntry> ProcessClotureSituationInvoices(
            Invoice[] invoices,
            ChartOfAccounts charts,
            string accountingCode,
            string clienCode)
        {
            var result = new List<JournalEntry>();

            foreach (var document in invoices.Where(e => e.TypeInvoice != InvoiceType.General))
            {
                var documentClientCode = document.Client.AccountingCode.IsValid()
                    ? document.Client.AccountingCode
                    : clienCode;

                var taxDetails = document.Quote.OrderDetails.GetTaxDetails();
                var productsDetails = document.Quote.OrderDetails.GetAllProduct().Where(e => !(e.Product.Category is null));

                var journalEntry = new JournalEntry
                {
                    JournalCode = accountingCode,
                    PieceNumber = document.Reference,
                    EntryDate = document.CreationDate,
                    EntryType = JournalEntryType.Main,
                    ChartAccount = documentClientCode,
                    DocumentType = DocumentType.Invoice,
                    ExternalPartner = document.Client.FullName,
                    Amount = document.GetTotalTTC(),
                };

                journalEntry.Credits.AddRange(taxDetails
                    .Select(e => new JournalEntry
                    {
                        Amount = e.TotalTax * document.Situation / 100,
                        JournalCode = accountingCode,
                        PieceNumber = document.Reference,
                        EntryDate = document.CreationDate,
                        ExternalPartner = document.Client.FullName,
                        EntryType = JournalEntryType.TaxDetails,
                        ChartAccount = charts.GetTaxCode(e.Tax),
                    }));

                journalEntry.Credits.AddRange(productsDetails
                    .GroupBy(e => e.Product.Category)
                    .Select(e =>
                    {
                        var total = e.Sum(p => p.Quantity * p.Product.TotalHT);
                        total = total * document.Situation / 100;

                        return new
                        {
                            Total = total,
                            e.Key.ChartAccountItem,
                        };
                    })
                    .Where(e => !(e is null) && (e.ChartAccountItem != null))
                    .Select(e => new JournalEntry
                    {
                        Amount = e.Total,
                        JournalCode = accountingCode,
                        PieceNumber = document.Reference,
                        EntryDate = document.CreationDate,
                        DocumentType = DocumentType.Invoice,
                        ChartAccount = e.ChartAccountItem.Code,
                        ExternalPartner = document.Client.FullName,
                        EntryType = JournalEntryType.ProductCategory,
                    }));

                result.Add(journalEntry);
                //LogJournalEntry(document.Id, journalEntry);
            }

            return result;
        }

        private void LogJournalEntry(string documentId, JournalEntry mainEntry)
        {
            _logger.LogInformation("================================");
            _logger.LogInformation("{id}, debit: {d} - credit: 0", documentId, mainEntry.Amount);
            _logger.LogInformation("{id}, debit: 0 - credit: {c}", documentId, mainEntry.Credits.Sum(e => e.Amount));
            _logger.LogInformation("================================");
        }
    }
}
