﻿namespace Axiobat.Services.Accounting
{
    using App.Common;
    using Application.Data;
    using Application.Enums;
    using Application.Models;
    using Application.Services;
    using Application.Services.Accounting;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Enums;
    using Domain.Constants;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation for <see cref="IAnalyticsService"/>
    /// </summary>
    public partial class AnalyticsService
    {
        public async Task<Result<WorkshopAnalyticsResult>> GetWorkshopAnalyticsAsync(WorkshopAnalyticsFilterOtions filterModel)
        {
            // get default values parameter
            _dateService.SetDateRange(filterModel);
            var charts = await _chartOfAccountsService.GetChartOfAccountsAsync();
            var defaultValues = await _configuration.GetAsync<DefaultValue>(ApplicationConfigurationType.DefaultValues);
            var workshops = await _dataAccess.GetWorkshopForAnalyticsAsync(filterModel);

            // get the workshop info
            var workshopInfo = workshops
                .Select(w => new MinimalWorkshopInfo
                {
                    Id = w.Id,
                    Name = w.Name,
                    Status = w.Status,
                    Turnover = GetTurnover(w.Invoices.ToArray(), w.CreditNotes.ToArray()),
                    Client = Map<ClientModel>(w.Client),
                    OperationSheets = w.OperationSheets
                        .Select(operationSheet => new OperationSheetsMinimalInfo
                        {
                            Id = operationSheet.Id,
                            Reference = operationSheet.Reference,
                            Status = operationSheet.Status,
                            WorkingHours = GetTotalWorkingHours(operationSheet.StartDate, operationSheet.EndDate, defaultValues),
                        }),
                })
                .ToList();

            // filter workshops
            var filterdWorkshops = workshopInfo
                .Where(e => (e.Status == WorkshopStatus.Accepted || e.Status == WorkshopStatus.Finished) &&
                    e.OperationSheets.Any(o => o.Status == OperationSheetStatus.Billed || o.Status == OperationSheetStatus.Realized));

            // set the workshops as lookup
            var workshopLookup = workshopInfo.ToLookup(e => e.Id);

            return new WorkshopAnalyticsResult
            {
                // group workshops by status
                WorkshopsByStatus = workshopInfo
                    .GroupBy(e => e.Status)
                    .Select(e => new WorkshopByStatus
                    {
                        Status = e.Key,
                        Workshops = e.Select(w => w),
                    }),

                // Top 10 workshops by turnover
                TopWorkshopsByTurnover = workshopInfo
                    .Where(e => (e.Status == WorkshopStatus.Accepted || e.Status == WorkshopStatus.Finished))
                    .OrderByDescending(e => e.Turnover)
                    .Take(10),

                // Top 10 workshops by total working hours
                TopWorkshopsByWorkingHours = filterdWorkshops
                    .OrderByDescending(e => e.WorkingHours)
                    .Take(10),

                // group operation sheets by status
                OperationSheetsByStatus = workshops
                    .SelectMany(e => e.OperationSheets
                        .Select(os => new OperationSheetsMinimalInfo
                        {
                            Id = os.Id,
                            Status = os.Status,
                            Reference = os.Reference,
                            Workshops = workshopLookup[os.WorkshopId].FirstOrDefault(),
                        }))
                    .GroupBy(e => e.Status)
                    .Select(e => new OperationSheetsByStatus
                    {
                        Status = e.Key,
                        operationSheets = e.Select(o => o),
                    }),

                // get the margin details
                MarginDetails = GetMarginDetails(workshops, charts[ChartOfAccounts.Expenses]),
            };
        }

        public async Task<Result<FinancialAnalyticsResult>> GetFinancialAnalyticsAsync(ExternalPartnerFilterOptions filterModel)
        {
            // set the date range
            _dateService.SetDateRange(filterModel, PeriodType.Year, DateExtendDirection.StartingDate, 1);

            // retrieve data
            var turnoverGoals = await _configuration.GetTurnoverGoalsAsync();
            var invoices = await _dataAccess.GetInvoicesForAnalyticsAsync(filterModel);
            var creditNotes = await _dataAccess.GetCreditNotesForAnalyticsAsync(filterModel);
            var expenses = await _dataAccess.GetExpensesForAnalyticsAsync(filterModel);

            var (increaseDetails, goalDetail) = GetTurnoverIncreaseDetails(invoices, creditNotes, expenses, turnoverGoals.Value);

            return new FinancialAnalyticsResult
            {
                // turnover details
                TurnoverDetails = new TurnoverDetails
                {
                    TotalIncome = (float) invoices
                        .Where(i =>
                            i.Status != InvoiceStatus.Draft &&
                            i.Status != InvoiceStatus.Canceled)
                        .Sum(e => e.GetTotalPaid()),

                    TotalUnpaidIncome = (float)invoices
                        .Where(i =>
                            i.Status != InvoiceStatus.Draft &&
                            i.Status != InvoiceStatus.Canceled)
                        .Sum(e => e.GetRestToPay()),

                    TotalExpenses = (float) expenses
                        .Where(e =>
                            e.Status != ExpenseStatus.Draft &&
                            e.Status != ExpenseStatus.Canceled)
                        .Sum(e => e.GetTotalPaid()),

                    TotalUnpaidExpenses = (float) expenses
                        .Where(e =>
                            e.Status != ExpenseStatus.Draft &&
                            e.Status != ExpenseStatus.Canceled)
                        .Sum(e => e.GetRestToPay()),
                },

                // TurnoverIncrease
                TurnoverIncreaseDetails = increaseDetails,

                // Turnover goal details
                TurnoverAchievementDetails = goalDetail,
            };
        }

        public async Task<Result<SalesAnalyticsResult>> GetSalesAnalyticsAsync(TransactionAnalyticsFilterOptions filterModel)
        {
            _dateService.SetDateRange(filterModel);

            // get required data
            var invoices = await _dataAccess.GetInvoicesForAnalyticsAsync(filterModel);
            var creditNotes = await _dataAccess.GetCreditNotesForAnalyticsAsync(filterModel);
            var quotes = await _dataAccess.getQuotesForAnalyticsAsync(filterModel);
            var expenses = await _dataAccess.GetExpensesForAnalyticsAsync(filterModel);

            var selectedPeriod = filterModel.DateStart?.Year ?? DateTime.Now.Year;

            return new SalesAnalyticsResult
            {
                // quote details
                QuoteDetails = GetQuoteDetails(quotes.ToArray()),

                TopClients = GetTopClients(invoices, creditNotes),

                TurnoverDetails = await GetTurnoverAnalyticsAsync(selectedPeriod, invoices, creditNotes, expenses),

                ProductsDetails = await GetProductsDetailsAsync(invoices)
            };
        }

        public async Task<Result<PurchaseAnalyticsResult>> GetPurchaseAnalyticsAsync(TransactionAnalyticsFilterOptions filterModel)
        {
            // get expenses
            var expenses = await _dataAccess.GetExpensesForPurchaseAnalyticsAsync(filterModel);

            // extract the articles from the expenses
            var articleInfo = expenses.Where(x => x.Status != ExpenseStatus.Canceled && x.Status != ExpenseStatus.Draft)
                .SelectMany(e => e.OrderDetails
                    .GetAllProduct()
                    .Select(p => new
                    {
                        p.Product.Id,
                        p.Product.Name,
                        p.Product.Reference,
                        p.Product.Category,
                        Supplier = new ExternalPartnerProductsAnalytics
                        {
                            Quantity = p.Quantity,
                            Id = e.Supplier.Id,
                            Name = e.Supplier.FullName,
                            Reference = e.Supplier.Reference,
                            Price = p.Product.GetPrice(e.SupplierId),
                        },
                        ExpensesDetails = new MinimalDocumentModel
                        {
                            Id = e.Id,
                            Status = e.Status,
                            Reference = e.Reference,
                            TotalTTC = e.GetTotalTTC(),
                            TotalHT = e.GetTotalHT(),
                            DocumentType = e.DocumentType,
                        }
                    }));

            // get the article details
            List<ExternalPartnerProductsAnalytics> productPurchaseDetails = null;
            if (filterModel.PurchaseAnalyticsFor.IsValid())
            {
                productPurchaseDetails = articleInfo
                .Where(e => e.Id == filterModel.PurchaseAnalyticsFor)
                .GroupBy(e => e.Id)
                .SelectMany(e => e.Select(p => p.Supplier))
                .ToList();
            }

            // apply product filter
            if (filterModel.ProdcutId.Count() > 0)
            {
                articleInfo = articleInfo
                    .Where(e => filterModel.ProdcutId.Contains(e.Id));
            }

            // article grouping
            var articleGrouping = articleInfo
                .GroupBy(e => e.Id)
                .Select(p =>
                {
                    var product = p.First();
                    return new ProductAnalyticsResult
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Reference = product.Reference,
                        Suppliers = p.Select(e => e.Supplier),
                        Classification = product.Category,
                    };
                });

            return new PurchaseAnalyticsResult
            {
                // top 10 suppliers
                TopSuppliers = articleInfo
                     .GroupBy(e => e.Supplier.Id)
                     .Select(g =>
                     {
                         var supplierInfo = g.First().Supplier;
                         return new SuppliersAnalyticsResult
                         {
                             ExpensesDetails = g.Select(e => e.ExpensesDetails),
                             Supplier = new ExternalPartnerMinimalInfo
                             {
                                 Id = supplierInfo.Id,
                                 Name = supplierInfo.Name,
                                 Reference = supplierInfo.Reference,
                             },
                         };
                     })
                     .OrderByDescending(e => e.TotalTransaction)
                     .Take(10),

                // top products by quantity
                TopProductsByQuantity = articleGrouping
                     .OrderByDescending(e => e.CountOrders)
                     .Take(10),

                // top products by supplier
                TopProductsBySupplierPrice = articleGrouping
                     .OrderByDescending(e => e.Suppliers.Max(p => p.Price))
                     .Take(10),

                // product purchase details by supplier
                SelectedProductPurchaseDetails = productPurchaseDetails,
            };
        }

        public async Task<Result<MaintenanceAnalyticsResult>> GetMaintenanceAnalyticsAsync(MaintenanceAnalyticsFilterOptions filterModel)
        {
            _dateService.SetDateRange(filterModel);

            var defaultValues = await _configuration.GetAsync<DefaultValue>(ApplicationConfigurationType.DefaultValues);
            var contracts = await _dataAccess.GetMaintenanceContractsForAnalyticsAsync(filterModel);

            return new MaintenanceAnalyticsResult
            {
                ContractsPerStatus = GetContractsPerStatus(contracts),
                OperationSheetByType = GetOperationSheetByType(contracts),
                TopContractByTurnover = GetTopContractByTurnOver(contracts),
                ContractExpireSoon = GetExpireSoonContracts(contracts, filterModel.ExpireIn),
                //TechniciansWorkingHours = GetTechniciansWorkingHoursDetails(contracts, defaultValues),
            };
        }

        private IEnumerable<OperationSheetsByType> GetOperationSheetByType(IEnumerable<MaintenanceContract> contracts)
        {
            return contracts.SelectMany(contract => contract.MaintenanceOperationsSheets
                .Select(operationSheet => new
                {
                    operationSheet.Id,
                    operationSheet.Type,
                    operationSheet.Status,
                    contractId = contract.Id,
                    operationSheet.Reference,
                }))
                .GroupBy(operationSheetDetail => operationSheetDetail.Type)
                .Select(grouping => new OperationSheetsByType
                {
                    Type = grouping.Key,
                    OperationSheets = grouping.Select(operationSheet => new OperationSheetsMinimalInfo
                    {
                        Id = operationSheet.Id,
                        Status = operationSheet.Status,
                        Reference = operationSheet.Reference,
                    })
                });
        }

        private IEnumerable<ContractByTurnover> GetTopContractByTurnOver(IEnumerable<MaintenanceContract> contracts)
        {
            return contracts.Select(contract => new ContractByTurnover
            {
                Turnover = contract.Invoices.Sum(invoice => invoice.OrderDetails.GetTotalHT()),
                Contract = new ContractDetails
                {
                    Id = contract.Id,
                    Site = contract.Site,
                    Client = contract.Client,
                    Status = contract.Status,
                    EndDate = contract.EndDate,
                    StartDate = contract.StartDate,
                }
            });
        }

        private IEnumerable<TechniciansWorkingHoursDetails> GetTechniciansWorkingHoursDetails(IEnumerable<MaintenanceContract> contracts, DefaultValue defaultValues)
        {
            return contracts.SelectMany(contract => contract.MaintenanceOperationsSheets
                .Select(operationSheet =>
                {
                    return new
                    {
                        ContractId = contract.Id,
                        OperationSheetId = operationSheet.Id,
                        Technician = Map<UserMinimalModel>(operationSheet.Technician),
                        TotalWorkingHours = GetTotalWorkingHours(operationSheet.StartDate, operationSheet.EndDate, defaultValues),
                    };
                }))
                .GroupBy(technicianDetails => technicianDetails.Technician)
                .Select(grouping => new TechniciansWorkingHoursDetails
                {
                    Technician = grouping.Key,
                    TotalWorkingHours = grouping.Sum(e => e.TotalWorkingHours),
                    OperationSheets = grouping.Select(e => e.OperationSheetId).Distinct(),
                    Contracts = grouping.Select(e => e.ContractId).Distinct(),
                });
        }

        private ContractPerDateDetails GetExpireSoonContracts(IEnumerable<MaintenanceContract> contracts, int? expireIn)
            => new ContractPerDateDetails
            {
                ContractDetails = contracts
                    .Where(e => !expireIn.HasValue || e.EndDate >= DateTime.Now && e.EndDate <= DateTime.Now.AddMonths((int)expireIn))
                    .Select(c => new ContractDetails()
                    {
                        Id = c.Id,
                        Site = c.Site,
                        Client = c.Client,
                        Status = c.Status,
                        EndDate = c.EndDate,
                        StartDate = c.StartDate,
                    })
                    .ToArray()
            };

        private ContractPerStatusDetails[] GetContractsPerStatus(IEnumerable<MaintenanceContract> contracts)
            => contracts
                .GroupBy(contract => contract.Status)
                .Select(grouping => new ContractPerStatusDetails(grouping.Key)
                {
                    ContractDetails = grouping.Select(contract => new ContractDetails
                    {
                        Id = contract.Id,
                        Site = contract.Site,
                        Client = contract.Client,
                        Status = contract.Status,
                        EndDate = contract.EndDate,
                        StartDate = contract.StartDate,
                    })
                    .ToArray(),
                })
                .ToArray();
    }

    /// <summary>
    /// partial part for <see cref="AnalyticsService"/>
    /// </summary>
    public partial class AnalyticsService : BaseService, IAnalyticsService
    {
        protected override string EntityName => "Analytics";

        private readonly IChartOfAccountsService _chartOfAccountsService;
        private readonly IAnalyticsDataAccess _dataAccess;
        private readonly ILabelDataAccess _labelDataAccess;
        private readonly IDateService _dateService;

        public AnalyticsService(
            IChartOfAccountsService chartOfAccountsService,
            IAnalyticsDataAccess analyticsDataAccess,
            ILabelDataAccess labelDataAccess,
            IDateService dateService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService configurationService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(loggedInUserService, configurationService, translationService, loggerFactory, mapper)
        {
            _chartOfAccountsService = chartOfAccountsService;
            _dataAccess = analyticsDataAccess;
            _labelDataAccess = labelDataAccess;
            _dateService = dateService;
        }

        private static IEnumerable<ExternalPartnerTransactionDetails> GetTopClients(IEnumerable<Invoice> invoices, IEnumerable<CreditNote> creditNotes, int totalTake = 10)
        {
            return invoices
                .GroupBy(e => e.ClientId)
                .Select(e =>
                {
                    var client = e.First().Client;
                    return new ExternalPartnerTransactionDetails
                    {
                        Id = client.Id,
                        Name = client.FullName,
                        Reference = client.Reference,
                        Total = e.Sum(i => i.OrderDetails.TotalTTC)
                    };
                })
                .Concat(creditNotes
                .GroupBy(e => e.ClientId)
                .Select(e =>
                {
                    var client = e.First().Client;
                    return new ExternalPartnerTransactionDetails
                    {
                        Id = client.Id,
                        Name = client.FullName,
                        Reference = client.Reference,
                        Total = e.Sum(i => i.OrderDetails.TotalTTC)
                    };
                }))
                .OrderBy(e => e.Total)
                .Take(totalTake);
        }

        private async Task<SalesProductAnalyticsResult> GetProductsDetailsAsync(IEnumerable<Invoice> invoices, int totalTake = 10)
        {
            var labels = await _labelDataAccess.GetAllAsync(null);

            // top clients
            var productDetails = invoices
                .SelectMany(e => e.OrderDetails
                    .GetAllProduct()
                    .Select(p => new
                    {
                        p.Product.Id,
                        p.Product.Name,
                        p.Product.Reference,
                        p.Product.Category,
                        Labels = labels.Where(l => l.Products.Any(pl => pl.ProductId == p.Product.Id))
                            .Select(l => new LabelModel
                            {
                                Id = l.Id,
                                Value = l.Value,
                            }),
                        Client = new ExternalPartnerProductsAnalytics
                        {
                            Id = e.Client.Id,
                            Quantity = p.Quantity,
                            Name = e.Client.FullName,
                            Price = p.Product.TotalTTC,
                            Reference = e.Client.Reference,
                        },
                        Document = new MinimalDocumentModel
                        {
                            Id = e.Id,
                            Status = e.Status,
                            Reference = e.Reference,
                            TotalHT = e.GetTotalHT(),
                            TotalTTC = e.GetTotalTTC(),
                            DocumentType = e.DocumentType,
                        }
                    }))
                .GroupBy(e => e.Id)
                .Select(g =>
                {
                    var product = g.First();
                    return new ProductAnalyticsResult
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Reference = product.Reference,
                        Classification = product.Category,
                        Labels = g.SelectMany(e => e.Labels).Distinct(),
                        Suppliers = g.Select(e => e.Client),
                    };
                });

            //
            return new SalesProductAnalyticsResult
            {
                TopProductsByTotalSales = productDetails
                    .OrderByDescending(e => e.TotalOrders)
                    .Take(totalTake),

                TopLabelsByTotalSales = productDetails
                    .SelectMany(p => p.Labels.Select(l => new
                    {
                        Label = l,
                        Product = p,
                    }))
                    .GroupBy(e => e.Label)
                    .Select(e => new LabelAnalyticsResult
                    {
                        Label = e.Key,
                        Total = e.Sum(p => p.Product.TotalOrders)
                    })
                    .OrderByDescending(e => e.Total)
                    .Take(totalTake),
            };
        }

        private async Task<SalesTurnoverDetails> GetTurnoverAnalyticsAsync(int periodYear, IEnumerable<Invoice> invoices, IEnumerable<CreditNote> creditNotes, IEnumerable<Expense> expenses)
        {
            var turnoverGoalsResult = await _configuration.GetTurnoverGoalsAsync();
            var turnoverGoal = turnoverGoalsResult.Value?.FirstOrDefault(e => e.Id == periodYear);
            var monthlyGolas = turnoverGoal?.MonthlyGoals?.ToLookup(e => e.Month);

            var goalDetail = new TurnoverPerPeriodGoalDetail(turnoverGoal?.Goal ?? 0);
            var increaseDetails = new TurnoverPerPeriodIncreaseDetails
            {
                TotalIncome =
                    invoices
                        .Where(i =>
                            i.Status != InvoiceStatus.Draft &&
                            i.Status != InvoiceStatus.Canceled)
                        .Sum(e => e.GetTotalHT())
                    +
                    creditNotes
                        .Where(i =>
                            i.Status != CreditNoteStatus.Draft)
                        .Sum(e => e.GetTotalHT()),

                TotalExpense = expenses
                    .Where(e =>
                        e.Status != ExpenseStatus.Canceled &&
                        e.Status != ExpenseStatus.Draft)
                    .Sum(e => e.GetTotalHT())

            };

            for (int month = 1; month <= 12; month++)
            {
                var totalExpense_current = expenses.Where(e => e.Status != ExpenseStatus.Canceled && e.Status != ExpenseStatus.Draft && e.CreationDate.Month == month)
                        .Sum(e => e.GetTotalHT());

                var totalInvoices_curent = invoices.Where(i => i.Status != InvoiceStatus.Draft && i.Status != InvoiceStatus.Canceled && i.CreationDate.Month == month)
                         .Sum(e => e.GetTotalHT());

                var totalCreditNotes = creditNotes.Where(i => i.Status != CreditNoteStatus.Draft && i.CreationDate.Month == month)
                        .Sum(e => e.GetTotalHT());

                var totalIncome_current = totalInvoices_curent + totalCreditNotes;

                increaseDetails.MonthlyIncreaseDetails.Add(
                    new TurnoverMonthlyIncreaseDetails(month, totalIncome_current, totalExpense_current));

                var currentPeriodGoal = 0F;
                if (monthlyGolas != null)
                {
                    currentPeriodGoal = monthlyGolas[month]?.FirstOrDefault()?.Goal ?? 0;
                }

                goalDetail.MonthlyDetails.Add(new MonthlyTurnoverGoalAchievement(currentPeriodGoal)
                {
                    Month = month,
                    Total = totalIncome_current,
                });
            }

            return new SalesTurnoverDetails
            {
                // turnover increase details
                TurnoverIncreaseDetails = increaseDetails,

                // Turnover goal details
                TurnoverAchievementDetails = goalDetail,
            };
        }

        public static WorkshopAnalytics GetMarginDetails(IEnumerable<ConstructionWorkshop> workshops, ChartAccountItem chartAccountItem)
        {
            var invoices = workshops.SelectMany(e => e.Invoices);
            var creditNotes = workshops.SelectMany(e => e.CreditNotes);
            var expenses = workshops.SelectMany(e => e.Expenses);
            var quotes = workshops.SelectMany(e => e.Quotes).ToLookup(e => e.Id);

            var totalMaterialPurchase = expenses
                .Sum(expense => expense
                    .OrderDetails
                    .GetAllProduct()
                    .Where(product =>
                    {
                        if (product.Product.Category is null || product.Product.Category.ChartAccountItem is null)

                            return false;

                        var chart = chartAccountItem[product.Product.Category.ChartAccountItem.Id];
                        if (chart is null)
                            return false;

                        return chart.CategoryType == CategoryType.Purchases;
                    })
                    .Sum(product => product.Quantity * product.Product.GetPrice(expense.SupplierId)));

            var subcontracting = expenses
                .Sum(expense => expense
                    .OrderDetails
                    .GetAllProduct()
                    .Where(product =>
                    {
                        if (product.Product.Category is null || product.Product.Category.ChartAccountItem is null)

                            return false;

                        var chart = chartAccountItem[product.Product.Category.ChartAccountItem.Id];
                        if (chart is null)
                            return false;

                        return chart.CategoryType == CategoryType.Purchases;
                    })
                    .Sum(product => product.Quantity * product.Product.GetPrice(expense.SupplierId)));

            var totalMaterialCost = invoices
                .Sum(e =>
                {
                    if (e.TypeInvoice == InvoiceType.General)
                        return e.OrderDetails.GetAllProduct()
                            .Sum(p => p.Quantity * p.Product.MaterialCost);

                    var quoteItem = quotes[e.QuoteId]?.FirstOrDefault();
                    if (quoteItem is null)
                        return 0;

                    return quoteItem.OrderDetails
                        .GetAllProduct()
                        .Sum(p => p.Quantity * p.Product.MaterialCost) * e.Situation / 100;
                });

            var totalSubContracting = invoices
                .Sum(e =>
                {
                    if (e.TypeInvoice == InvoiceType.General)
                        return e.OrderDetails.GetAllProduct()
                            .Sum(p => p.Quantity * p.Product.TotalHours * p.Product.HourlyCost);

                    var quoteItem = quotes[e.QuoteId]?.FirstOrDefault();
                    if (quoteItem is null)
                        return 0;

                    return quoteItem.OrderDetails
                        .GetAllProduct()
                        .Sum(p => p.Quantity * p.Product.TotalHours * p.Product.HourlyCost) * e.Situation / 100;
                });

            return new WorkshopAnalytics
            {
                Subcontracting = totalMaterialCost - totalMaterialPurchase,
                WorkforceMargin = totalSubContracting - subcontracting,
            };
        }

        public static float GetTurnover(Invoice[] invoices, CreditNote[] creditNotes)
            => invoices
                .Where(i =>
                    i.Status != InvoiceStatus.Canceled &&
                    i.Status != InvoiceStatus.Draft)
                .Sum(i => i.GetTotalHT())
            + creditNotes
                .Where(i =>
                    i.Status != CreditNoteStatus.Draft &&
                    i.Status != CreditNoteStatus.Expired)
                .Sum(i => i.GetTotalHT());

        internal static ClientTotalTurnOver GetClientTurnOverDetails(Invoice[] invoices, CreditNote[] creditNotes)
        {
            var totalTunOver = GetTurnover(invoices, creditNotes);
            var totalPaiement = invoices
                .Where(i =>
                    i.Status != InvoiceStatus.Canceled &&
                    i.Status != InvoiceStatus.Draft)
                .Sum(e => e.GetTotalPaid());

            return new ClientTotalTurnOver
            {
                TotalTurnOver = totalTunOver,
                TotalPayments = (float)totalPaiement,
            };
        }

        private double GetTotalWorkingHours(DateTime startDate, DateTime endDate, DefaultValue defaultValues)
            => _dateService.GetTotalWorkingHours(startDate, endDate, defaultValues.StartingHour, defaultValues.EndingHour);

        private (TurnoverIncreaseDetails increaseDetails, TurnoverGoalDetail goalDetail) GetTurnoverIncreaseDetails
            (IEnumerable<Invoice> invoices, IEnumerable<CreditNote> creditNotes, IEnumerable<Expense> expenses, IEnumerable<TurnoverGoalModel> turnoverGoals)
        {
            // set the periods
            var currentPeriod = DateTime.Today;
            var preCurrentPeriod = currentPeriod.AddYears(-1);

            // get turnover goals
            var currentPeriodTurnoverGoal = turnoverGoals.FirstOrDefault(e => e.Id == currentPeriod.Year);
            var currentPeriodTurnoverMonthlyGolas = turnoverGoals.FirstOrDefault(e => e.Id == currentPeriod.Year)?.MonthlyGoals?.ToLookup(e => e.Month);
            var perCurrentPeriodTurnover = turnoverGoals.FirstOrDefault(e => e.Id == preCurrentPeriod.Year);
            var perCurrentPeriodTurnoverMonthlyGolas = turnoverGoals.FirstOrDefault(e => e.Id == preCurrentPeriod.Year)?.MonthlyGoals?.ToLookup(e => e.Month);

            var result = new TurnoverIncreaseDetails()
            {
                CurrentPeriod = new TurnoverPerPeriodIncreaseDetails
                {
                    TotalIncome =
                        invoices
                            .Where(i =>
                                i.Status != InvoiceStatus.Draft &&
                                i.Status != InvoiceStatus.Canceled &&
                                i.CreationDate.Year == currentPeriod.Year)
                            .Sum(e => e.GetTotalHT())
                        +
                        creditNotes
                            .Where(i =>
                                i.Status != CreditNoteStatus.Draft &&
                                i.CreationDate.Year == currentPeriod.Year)
                            .Sum(e => e.GetTotalHT()),

                    TotalExpense = expenses
                        .Where(e =>
                            e.Status != ExpenseStatus.Canceled &&
                            e.Status != ExpenseStatus.Draft &&
                            e.CreationDate.Year == currentPeriod.Year)
                        .Sum(e => e.GetTotalHT())

                },
                PreCurrentPeriod = new TurnoverPerPeriodIncreaseDetails
                {
                    TotalIncome =
                        invoices
                            .Where(i =>
                                i.Status != InvoiceStatus.Draft &&
                                i.Status != InvoiceStatus.Canceled &&
                                i.CreationDate.Year == preCurrentPeriod.Year)
                            .Sum(e => e.GetTotalHT())
                        +
                        creditNotes
                            .Where(i =>
                                i.Status != CreditNoteStatus.Draft &&
                                i.CreationDate.Year == preCurrentPeriod.Year)
                            .Sum(e => e.GetTotalHT()),

                    TotalExpense = expenses
                        .Where(e =>
                            e.Status != ExpenseStatus.Canceled &&
                            e.Status != ExpenseStatus.Draft &&
                            e.CreationDate.Year == preCurrentPeriod.Year)
                        .Sum(e => e.GetTotalHT())
                },
            };
            var resultTurnoverGoal = new TurnoverGoalDetail(currentPeriodTurnoverGoal, perCurrentPeriodTurnover);

            for (int month = 1; month <= 12; month++)
            {
                #region total calculation

                var totalExpense_current = expenses.Where(e => e.Status != ExpenseStatus.Canceled && e.Status != ExpenseStatus.Draft && e.CreationDate.Month == month && e.CreationDate.Year == currentPeriod.Year)
                        .Sum(e => e.GetTotalHT());

                var totalInvoices_curent = invoices.Where(i => i.Status != InvoiceStatus.Draft && i.Status != InvoiceStatus.Canceled && i.CreationDate.Month == month && i.CreationDate.Year == currentPeriod.Year)
                         .Sum(e => e.GetTotalHT());

                var totalCreditNotes = creditNotes.Where(i => i.Status != CreditNoteStatus.Draft && i.CreationDate.Month == month && i.CreationDate.Year == currentPeriod.Year)
                        .Sum(e => e.GetTotalHT());

                var totalIncome_current = totalInvoices_curent + totalCreditNotes;

                var totalExpense_preCurrent = expenses.Where(e => e.Status != ExpenseStatus.Canceled && e.Status != ExpenseStatus.Draft && e.CreationDate.Month == month && e.CreationDate.Year == preCurrentPeriod.Year)
                        .Sum(e => e.GetTotalHT());

                var totalInvoices_preCurrent = invoices.Where(i => i.Status != InvoiceStatus.Draft && i.Status != InvoiceStatus.Canceled && i.CreationDate.Month == month && i.CreationDate.Year == preCurrentPeriod.Year)
                         .Sum(e => e.GetTotalHT());

                var totalCreditNotes_preCurrent = creditNotes.Where(i => i.Status != CreditNoteStatus.Draft && i.CreationDate.Month == month && i.CreationDate.Year == preCurrentPeriod.Year)
                        .Sum(e => e.GetTotalHT());

                var totalIncome_preCurrent = totalInvoices_preCurrent + totalCreditNotes_preCurrent;

                #endregion
                var currentPeriodGoal = 0F;
                var preCurrentPeriodGoal = 0F;
                if (currentPeriodTurnoverMonthlyGolas != null)
                {
                    currentPeriodGoal = currentPeriodTurnoverMonthlyGolas[month]?.FirstOrDefault()?.Goal ?? 0;
                }
                if (perCurrentPeriodTurnoverMonthlyGolas != null)
                {
                    preCurrentPeriodGoal = perCurrentPeriodTurnoverMonthlyGolas[month]?.FirstOrDefault()?.Goal ?? 0;
                }
                resultTurnoverGoal.CurrentPeriod.MonthlyDetails.Add(
                    new MonthlyTurnoverGoalAchievement(currentPeriodGoal)
                    {
                        Month = month,
                        Total = totalIncome_current,
                    });

                resultTurnoverGoal.PreCurrentPeriod.MonthlyDetails.Add(
                    new MonthlyTurnoverGoalAchievement(preCurrentPeriodGoal)
                    {
                        Month = month,
                        Total = totalIncome_preCurrent,
                    });

                result.CurrentPeriod.MonthlyIncreaseDetails.Add(
                    new TurnoverMonthlyIncreaseDetails(month, totalIncome_current, totalExpense_current));

                result.PreCurrentPeriod.MonthlyIncreaseDetails.Add(
                    new TurnoverMonthlyIncreaseDetails(month, totalIncome_preCurrent, totalExpense_preCurrent));
            }

            return (result, resultTurnoverGoal);
        }

        public static QuoteAnalyticsResult GetQuoteDetails(Quote[] quotes)
        {
            // quotes details
            var quotesDetailed = quotes
                .Select(q => new MinimalDocumentModel
                {
                    Id = q.Id,
                    Status = q.Status,
                    Reference = q.Reference,
                    DocumentType = q.DocumentType,
                    TotalHT = q.GetTotalHT(),
                    TotalTTC = q.GetTotalTTC(),
                });

            var Total = quotesDetailed.Sum(e => e.TotalHT);

            return new QuoteAnalyticsResult
            {
                // Total quote turnover
                Total = Total,

                // count of quotes
                Count = quotesDetailed.Count(),

                // quotes by status
                DocumentByStatus = quotesDetailed
                    .GroupBy(e => e.Status)
                    .Select(e => new DocumentByStatus(Total)
                    {
                        Status = e.Key,
                        Documents = e.Select(q => q),
                        Total = e.Sum(q => q.TotalHT),
                    }),
            };
        }

        public static DocumentAnalyticsResult GetInvoiceDetails(Invoice[] invoices)
        {
            // quotes details
            var documentDetailed = invoices
                .Select(q => new MinimalDocumentModel
                {
                    Id = q.Id,
                    Status = q.Status,
                    Reference = q.Reference,
                    DocumentType = q.DocumentType,
                    TotalHT = q.GetTotalHT(),
                    TotalTTC = q.GetTotalTTC(),
                });

            var Total = documentDetailed.Sum(e => e.TotalHT);

            return new DocumentAnalyticsResult
            {
                // Total quote turnover
                Total = Total,

                // count of quotes
                Count = documentDetailed.Count(),

                // quotes by status
                DocumentByStatus = documentDetailed
                    .GroupBy(e => e.Status)
                    .Select(e => new DocumentByStatus(Total)
                    {
                        Status = e.Key,
                        Documents = e.Select(q => q),
                        Total = e.Sum(q => q.TotalHT),
                    }),
            };
        }

        public static WorkshopDocumentAnalyticsResult GetWorkshopDetails(ConstructionWorkshop[] workshops, ChartAccountItem ExpensesChartAccountItem)
        {
            // quotes details
            var documentDetailed = workshops
                .Select(q => new MinimalDocumentModel
                {
                    Id = q.Id,
                    TotalHT = 0,
                    Reference = "",
                    Status = q.Status,
                    TotalTTC = q.Amount,
                    DocumentType = q.DocumentType,
                });

            var total = workshops.Sum(e => e.Amount);

            return new WorkshopDocumentAnalyticsResult
            {
                // Total quote turnover
                Total = total,

                // count of quotes
                Count = workshops.Length,

                // quotes by status
                DocumentByStatus = documentDetailed
                    .GroupBy(e => e.Status)
                    .Select(e => new DocumentByStatus(total)
                    {
                        Status = e.Key,
                        Documents = e.Select(q => q),
                        Total = e.Sum(q => q.TotalTTC),
                    }),

                MarginDetails = GetMarginDetails(workshops, ExpensesChartAccountItem),
            };
        }

        public static DocumentAnalyticsResult GetContractDetails(MaintenanceContract[] contracts)
        {
            // quotes details
            var documentDetailed = contracts
                .Select(q => new MinimalDocumentModel
                {
                    Id = q.Id,
                    Status = q.Status,
                    Reference = q.Reference,
                    DocumentType = q.DocumentType,
                    TotalHT = q.GetTotalHT(),
                    TotalTTC = q.GetTotalTTC(),
                });

            var total = documentDetailed.Sum(e => e.TotalHT);

            return new DocumentAnalyticsResult
            {
                // Total quote turnover
                Total = total,

                // count of quotes
                Count = contracts.Length,

                // quotes by status
                DocumentByStatus = documentDetailed
                    .GroupBy(e => e.Status)
                    .Select(e => new DocumentByStatus(total)
                    {
                        Status = e.Key,
                        Documents = e.Select(q => q),
                        Total = e.Sum(q => q.TotalTTC),
                    }),
            };
        }

        internal static DocumentAnalyticsResult GetOperationSheetDetails(OperationSheet[] operationSheet, MaintenanceOperationSheet[] operationSheetMaintence)
        {
            var documentDetailed = operationSheet
                .Select(q => new MinimalDocumentModel
                {
                    Id = q.Id,
                    Status = q.Status,
                    Reference = q.Reference,
                    DocumentType = q.DocumentType,
                    TotalHT = q.GetTotalHT(),
                    TotalTTC = q.GetTotalTTC(),
                })
                .Concat(operationSheetMaintence
                .Select(q => new MinimalDocumentModel
                {
                    Id = q.Id,
                    Status = q.Status,
                    Reference = q.Reference,
                    DocumentType = q.DocumentType,
                    TotalHT = q.GetTotalHT(),
                    TotalTTC = q.GetTotalTTC(),
                }));

            var total = documentDetailed.Sum(e => e.TotalHT);

            return new DocumentAnalyticsResult
            {
                // Total quote turnover
                Total = total,

                // count of quotes
                Count = documentDetailed.Count(),

                // quotes by status
                DocumentByStatus = documentDetailed
                    .GroupBy(e => e.Status)
                    .Select(e => new DocumentByStatus(total)
                    {
                        Status = e.Key,
                        Documents = e.Select(q => q),
                        Total = e.Sum(q => q.TotalTTC),
                    }),
            };

            throw new NotImplementedException();
        }
    }
}
