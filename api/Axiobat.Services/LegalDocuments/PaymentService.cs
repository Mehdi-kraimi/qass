﻿namespace Axiobat.Services.Documents
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Documents;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="Payment"/>
    /// </summary>
    public partial class PaymentService : DataService<Payment, string>, IPaymentService
    {
        /// <summary>
        /// create a transfer operation between two accounts
        /// </summary>
        /// <param name="model">the model user for creating the transfer operation</param>
        /// <returns>the operation result</returns>
        public async Task<Result> CreateTransferOperationAsync(PaymentCreatetransferModel model)
        {
            // first create the payments
            var paymentForCreditedAccount = new Payment
            {
                Description = T("Mouvement compte à compte"),
                DatePayment = DateTime.Now,
                Type = PaymentType.Transfer,
                AccountId = model.CreditedAccountId,
                TransferAmount = model.TransferAmount,
                PaymentMethodId = model.PaymentMethodId,
                Operation = PaymentOperation.Transfer_To,
            };

            var paymentForDebiteddAccount = new Payment
            {
                Description = T("Mouvement compte à compte"),
                DatePayment = DateTime.Now,
                Type = PaymentType.Transfer,
                AccountId = model.DebitedAccountId,
                TransferAmount = -model.TransferAmount,
                PaymentMethodId = model.PaymentMethodId,
                Operation = PaymentOperation.Transfer_From,
            };

            paymentForDebiteddAccount.Id += "5";
            _history.Recored(paymentForCreditedAccount, ChangesHistoryType.Added);
            _history.Recored(paymentForDebiteddAccount, ChangesHistoryType.Added);

            // insert the payments
            var result = await _dataAccess.AddRangeAsync(new Payment[] { paymentForCreditedAccount, paymentForDebiteddAccount });
            if (!result.IsSuccess)
                return Result.Failed("Failed to add the payments");

            return Result.Success();
        }

        /// <summary>
        /// get the payment total balance
        /// </summary>
        /// <returns>the payment balance</returns>
        public async Task<Result<PaymentBalance>> GetPaymentsBalanceAsync()
        {
            // get the payments
            var payments = await _dataAccess.GetAllInvoiceExpensePaymentsAsync();

            // return result
            return new PaymentBalance
            {
                TotalPaymentExpense =(float) payments.Where(e => e.Type == PaymentType.Expense).Sum(e => e.Amount),
                TotalPaymentInvoice = (float) payments.Where(e => e.Type == PaymentType.Invoice).Sum(e => e.Amount),
            };
        }
    }

    /// <summary>
    /// partial part for <see cref="PaymentService"/>
    /// </summary>
    public partial class PaymentService : DataService<Payment, string>, IPaymentService
    {
        private readonly IInvoiceDataAccess _invoiceDataAccess;
        private readonly IExpenseDataAccess _expenseDataAccess;
        private readonly ICreditNoteService _creditNoteService;

        private new IPaymentDataAccess _dataAccess => base._dataAccess as IPaymentDataAccess;

        public PaymentService(
            IPaymentDataAccess dataAccess,
            IInvoiceDataAccess invoiceDataAccess,
            IExpenseDataAccess expenseDataAccess,
            ICreditNoteService creditNoteService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _invoiceDataAccess = invoiceDataAccess;
            _expenseDataAccess = expenseDataAccess;
            _creditNoteService = creditNoteService;
        }

        protected override Task InGet_AfterMappingAsync<TOut>(Payment entity, TOut mappedEntity)
        {
            if (mappedEntity is PaymentModel quoteModel)
            {
                var documents = entity.Invoices.Select(e => new AssociatedDocument
                {
                    Id = e.InvoiceId,
                    Status = e.Invoice.Status,
                    Reference = e.Invoice.Reference,
                    CreationDate = entity.DatePayment,
                    TypeInvoice = e.Invoice.TypeInvoice,
                    DocumentType = DocumentType.Invoice,
                })
                .Concat(entity.Expenses.Select(e => new AssociatedDocument
                {
                    Id = e.ExpenseId,
                    Status = e.Expense.Status,
                    CreationDate = e.Expense.CreationDate,
                    DocumentType = e.Expense.DocumentType,
                    Reference = e.Expense.Reference,
                }));

                foreach (var document in documents)
                    quoteModel.AssociatedDocuments.Add(document);
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }

        protected override async Task InCreate_BeforInsertAsync<TCreateModel>(Payment payment, TCreateModel createModel)
        {
            if (!(createModel is PaymentPutModel model))
                throw new ModelTypeIsNotValidException(nameof(createModel), typeof(PaymentPutModel));

            // add the technicians
            payment.Invoices = model.Invoices
                .Select(e => new Invoices_Payments { PaymentId = payment.Id, InvoiceId = e.DocumentId, Amount = e.Amount })
                .ToList();

            payment.Expenses = model.Expenses
                .Select(e => new Expenses_Payments { PaymentId = payment.Id, ExpenseId = e.DocumentId, Amount = e.Amount })
                .ToList();

            if (!payment.CreditNoteId.IsValid() && await HasCreditNotePaymentMethodIdAsync(payment))
            {
                var invoicePayment = payment.Invoices.FirstOrDefault();
                if (!(invoicePayment is null))
                {
                    // get the invoice
                    var invoice = await _invoiceDataAccess.GetByIdAsync(invoicePayment.InvoiceId);

                    // create a credit note
                    var creditNoteResult = await _creditNoteService.CreateForPaymentAsync<CreditNoteModel>(invoice, payment.Amount);
                    if (!creditNoteResult.IsSuccess)
                    {
                        _logger.LogError("Failed to credit note for the payment with id: {paymentId}", payment.Id);
                        return;
                    }

                    // attach the credit note the payment
                    payment.CreditNoteId = creditNoteResult.Value.Id;
                }
            }
            else
            {
                payment.CreditNoteId = null;
            }
        }

        protected override async Task InCreate_AfterInsertAsync<TCreateModel>(Payment payment, TCreateModel createModel)
        {
            if (!(createModel is PaymentPutModel model))
                throw new ModelTypeIsNotValidException(nameof(createModel), typeof(PaymentPutModel));

            if (model.CreditNoteId.IsValid())
            {
                await _creditNoteService.UpdateStatusAsync(model.CreditNoteId, CreditNoteStatus.Used);
            }

            // update status
            if (payment.Type == PaymentType.Expense)
            {
                await UpdateExpenseStatus(model.Expenses.Select(e => e.DocumentId).ToArray());
                return;
            }
            else if (payment.Type == PaymentType.Invoice)
            {
                await UpdateInvoicesStatusAsync(model.Invoices.Select(e => e.DocumentId).ToArray());
                return;
            }
        }

        protected override async Task InUpdate_BeforUpdateAsync<TUpdateModel>(Payment payment, TUpdateModel createModel)
        {
            if (!(createModel is PaymentPutModel model))
                throw new ModelTypeIsNotValidException(nameof(createModel), typeof(PaymentPutModel));

            payment.Account = null;
            payment.PaymentMethod = null;

            await UpdateInvoicesPaymentsAsync(payment, model);
            await UpdateExpensePaymentsAsync(payment, model);
        }

        protected override async Task InUpdate_AfterUpdateAsync<TUpdateModel>(Payment payment, TUpdateModel updateModel)
        {
            if (!(updateModel is PaymentPutModel model))
                throw new ModelTypeIsNotValidException(nameof(updateModel), typeof(PaymentPutModel));

            // update status
            if (payment.Type == PaymentType.Expense)
            {
                await UpdateExpenseStatus(model.Expenses.Select(e => e.DocumentId).ToArray());
                return;
            }
            else if (payment.Type == PaymentType.Invoice)
            {
                await UpdateInvoicesStatusAsync(model.Expenses.Select(e => e.DocumentId).ToArray());
                return;
            }
        }

        protected override async Task InDelete_AfterDeleteAsync(Payment payment)
        {
            // update status
            if (payment.Type == PaymentType.Expense)
            {
                await UpdateExpenseStatus(payment.Expenses.Select(e => e.ExpenseId).ToArray());
                return;
            }
            else if (payment.Type == PaymentType.Invoice)
            {
                await UpdateInvoicesStatusAsync(payment.Expenses.Select(e => e.ExpenseId).ToArray());
                return;
            }

            // update the credit note status if any
            if (payment.CreditNoteId.IsValid())
            {
                await _creditNoteService.UpdateStatusAsync(payment.CreditNoteId, CreditNoteStatus.InProgress);
            }
        }

        private async Task UpdateExpenseStatus(params string[] expensesIds)
        {
            // get the expenses
            var expenses = await _expenseDataAccess.GetByIdAsync(expensesIds);

            // check every expense payment
            foreach (var expense in expenses)
            {
                // check the payments
                var restToPay = expense.GetRestToPay();

                // check the rest to pay amount
                if (restToPay == 0)
                    expense.Status = InvoiceStatus.Closed;

                if (restToPay > 0)
                    expense.Status = InvoiceStatus.InProgress;

                // update the expense
                await _expenseDataAccess.UpdateAsync(expense);
            }
        }

        private async Task UpdateInvoicesStatusAsync(params string[] invoicesIds)
        {
            // get the invoices
            var invoices = await _invoiceDataAccess.GetByIdAsync(invoicesIds);

            // check every invoice payment
            foreach (var invoice in invoices)
            {
                // check the payments
                var restToPay = invoice.GetRestToPay();

                // check the rest to pay amount
                if (restToPay == 0)
                    invoice.Status = InvoiceStatus.Closed;

                if (restToPay > 0)
                    invoice.Status = InvoiceStatus.InProgress;

                // update the invoice
                await _invoiceDataAccess.UpdateAsync(invoice);
            }
        }

        private async Task<bool> HasCreditNotePaymentMethodIdAsync(Payment payment)
        {
            var creditNotePaymentMethod = await _configuration.GetCreditNotePaymentMethodAsyn();
            if (!creditNotePaymentMethod.IsSuccess)
            {
                _logger.LogError("there is no payment method of type [{methodType}]", PaymentMethodType.CreditNote);
                return false;
            }

            return payment.PaymentMethodId == creditNotePaymentMethod.Value.Id;
        }

        private async Task UpdateInvoicesPaymentsAsync(Payment payment, PaymentPutModel model)
        {
            // list of invoices
            var invoices_Payments = model.Invoices.Select(e => new Invoices_Payments
            {
                PaymentId = payment.Id,
                InvoiceId = e.DocumentId,
                Amount = e.Amount
            });

            if (invoices_Payments.Count() <= 0)
                return;

            // get the intersection
            var intersection = payment.Invoices.Intersection(invoices_Payments);

            // add and delete the payments
            await _dataAccess.RemovePaymentinvoicesAsync(intersection.Removed);
            await _dataAccess.AddPaymentinvoicesAsync(intersection.Added);
            await _dataAccess.UpdatePaymentinvoicesAsync(intersection.Common);

            // clear payments
            payment.Invoices.Clear();
        }

        private async Task UpdateExpensePaymentsAsync(Payment payment, PaymentPutModel model)
        {
            // list of invoices
            var expenses_Payments = model.Expenses.Select(e => new Expenses_Payments
            {
                PaymentId = payment.Id,
                ExpenseId = e.DocumentId,
                Amount = e.Amount
            });

            if (expenses_Payments.Count() <= 0)
                return;

            // get the intersection
            var intersection = payment.Expenses.Intersection(expenses_Payments);

            // add and delete the payments
            await _dataAccess.RemoveExpensePaymentAsync(intersection.Removed);
            await _dataAccess.AddExpensePaymentAsync(intersection.Added);
            await _dataAccess.UpdateExpensePaymentAsync(intersection.Common);

            // clear payments
            payment.Expenses.Clear();
        }
    }
}
