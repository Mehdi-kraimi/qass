﻿namespace Axiobat.Services.Documents
{
    using App.Common;
    using Application.Data;
    using Application.Enums;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.Documents;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.MailService;
    using Application.Services.PushNotification;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using Domain.Exceptions;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// data service implantation for <see cref="IOperationSheetService"/>
    /// </summary>
    public partial class OperationSheetService
    {
        /// <summary>
        /// check if the given reference is unique
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsRefrenceUniqueAsync(string reference)
            => !await _dataAccess.IsExistAsync(e => e.Reference == reference);

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateStatusAsync(string operationSheetId, DocumentUpdateStatusModel model)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(operationSheetId);
            if (document is null)
                throw new NotFoundException("there is no operation Sheet with the given id");

            // update the devis status
            _history.Recored(document, ChangesHistoryType.Updated, fields: new[] { new ChangedField("status", document.Status, model.Status) });
            document.Status = model.Status;
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateModeStatusAsync(string operationSheetId, ModeStatut model)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(operationSheetId);
            if (document is null)
                throw new NotFoundException("there is no operation Sheet with the given id");
            if (document.ModeStatut is null)
                document.ModeStatut = new List<ModeStatut>();

            // update the devis status
            document.ModeStatut.Add(model);
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// update the signature of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the signature</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateSignatureAsync(string operationSheetId, DocumentUpdateSignatureModel model)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(operationSheetId);
            if (document is null)
                throw new NotFoundException("there is no operation Sheet with the given id");

            // update the devis status
            _history.Recored(document, ChangesHistoryType.Updated, fields: new[] { new ChangedField("status", document.Status, MaintenanceOperationSheetStatus.Realized) });
            document.Status = MaintenanceOperationSheetStatus.Realized;
            document.ClientSignature = model.ClientSignature;
            document.TechnicianSignature = model.TechnicianSignature;
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// retrieve the list of operation sheet of the given client
        /// </summary>
        /// <param name="clientId">the id of the client</param>
        /// <returns>list of operation sheets</returns>
        public async Task<ListResult<TOut>> GetClientOperationSheetsAsync<TOut>(string clientId)
        {
            var result = await _dataAccess.GetOperationSheetsByClientIdAsync(clientId);
            return Result.ListSuccess(Map<IEnumerable<TOut>>(result));
        }

        /// <summary>
        /// send the operationSheet with the given id with an email using the given options
        /// </summary>
        /// <param name="operationSheetId">the id of the document</param>
        /// <param name="emailOptions">the email options</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SendAsync(string operationSheetId, SendEmailOptions emailOptions)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(operationSheetId);
            if (document is null)
                throw new NotFoundException("there is no Operation sheet with the given id");

            // generate the document Pdf
            //var filePDFResult = await ExportDataAsync(operationSheetId, new DataExportOptions { ExportType = ExportType.PDF });
            //if (!filePDFResult.IsSuccess)
            //    return filePDFResult;

            // generate an id to the email
            emailOptions.Id = $"{operationSheetId}-".AppendTimeStamp();

            // add the PDF file to the email attachments
            //emailOptions.Attachments.Add(new AttachmentModel
            //{
            //    FileType = "application/pdf",
            //    Content = Convert.ToBase64String(filePDFResult),
            //    FileName = $"{DocumentType.Quote}-{DateHelper.Timestamp}.pdf",
            //});

            // send the email
            var result = await _emailService.SendEmailAsync(emailOptions);
            if (!result.IsSuccess)
                return result;

            // add the email document
            document.Emails.Add(new DocumentEmail
            {
                To = emailOptions.To,
                SentOn = DateTime.Now,
                MailId = emailOptions.Id,
                Content = emailOptions.Body,
                Subject = emailOptions.Subject,
                User = _loggedInUserService.GetMinimalUser(),
            });

            // update the document
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
                User = new MinimalUser(_loggedInUserService.User.UserId, _loggedInUserService.User.UserName),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => entity.Memos.Add(memo));
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            _logger.LogDebug(LogEvent.SavingMemo, "a new memo with id: [{memoId}] has been saved", memo.Id);
            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memos.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToArray();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memos.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memos.Remove(memo);

            var deleteResult = await _dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// get the operation sheet by reference
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <param name="operationSheetReference">the reference of the operation sheet</param>
        /// <returns>the output type</returns>
        public async Task<Result<TOut>> GetByReferenceAsync<TOut>(string operationSheetReference)
        {
            var operationSheet = await _dataAccess.GetSingleAsync(options => options.AddPredicate(e => e.Reference == operationSheetReference));
            if (operationSheet is null)
                return Result.Failed<TOut>("there is no operation sheet with the given reference");

            return Map<TOut>(operationSheet);
        }

        /// <summary>
        /// duplicate the operation sheet
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="operationSheetId">the operation sheet Id</param>
        /// <returns>the out put model</returns>
        public async Task<Result<TOut>> DuplicateAsync<TOut>(string operationSheetId)
        {
            // retrieve the operation sheet
            var operationSheet = await _dataAccess.GetByIdAsync(operationSheetId);
            if (operationSheet is null)
                throw new NotFoundException("there is no operation Sheet with the given id");

            // generate a new reference
            var reference = await GenerateReferenceAsync();

            // empty the operation sheet
            if (operationSheet.Status != OperationSheetStatus.Draft)
                operationSheet.Status = OperationSheetStatus.Planned;

            operationSheet.Id = operationSheet.GenerateId();
            operationSheet.Reference = reference;
            operationSheet.ChangesHistory.Clear();
            operationSheet.Technicians.Clear();
            operationSheet.TechnicianSignature = null;
            operationSheet.ClientSignature = null;
            operationSheet.InvoiceId = null;
            operationSheet.Workshop = null;
            operationSheet.Client = null;
            operationSheet.Quote = null;

            var technicians = operationSheet.Technicians.Select(e => e.TechnicianId);
            operationSheet.Technicians = technicians.Select(e => new OperationSheets_Technicians
            {
                TechnicianId = e,
                OperationSheetId = operationSheet.Id
            })
            .ToList();

            // record changes history
            _history.Recored(operationSheet, ChangesHistoryType.Added);

            // insert the new operation sheet
            var addResult = await _dataAccess.AddAsync(operationSheet);
            if (!addResult.IsSuccess)
                return Result.From<TOut>(addResult);

            // increment reference
            await _configuration.IncrementReferenceAsync(DocumentType.OperationSheet);

            // return result
            return Map<TOut>(operationSheet);
        }

        /// <summary>
        /// bill the operation sheets with the given ids, this function will retrieve the operation sheets with the given ids
        /// and update their status to <see cref="Domain.Constants.OperationSheetStatus.Billed"/>
        /// </summary>
        /// <param name="invoiceId">the id of the invoice</param>
        /// <param name="operationSheetId">the id of the operation sheet</param>
        public async Task BillOperationSheetsAsync(string invoiceId, params string[] operationSheetId)
        {
            // retrieve the operation sheets
            var operationSheets = await _dataAccess.GetByIdAsync(operationSheetId);

            foreach (var operationSheet in operationSheets)
            {
                _history.Recored(operationSheet, ChangesHistoryType.Updated, fields: new[] { new ChangedField("status", operationSheet.Status, OperationSheetStatus.Billed) });
                operationSheet.Status = OperationSheetStatus.Billed;
                operationSheet.InvoiceId = invoiceId;

                await _dataAccess.UpdateAsync(operationSheet);
            }
        }

        public Task UpdateInvoiceOperationSheetStatusAsync(string invoiceId, DocumentUpdateStatusModel model)
        {
            return _dataAccess.UpdateInvoiceOperationSheetStatusAsync(invoiceId, model.Status);
        }
    }

    /// <summary>
    /// partial part for <see cref="OperationSheetService"/>
    /// </summary>
    public partial class OperationSheetService : SynchronizeDataService<OperationSheet, string>, IOperationSheetService
    {
        private readonly IGoogleCalendarService _googleCalendarService;
        private readonly IPushNotificationService _pushNotificationService;
        private readonly IEmailService _emailService;

        private new IOperationSheetDataAccess _dataAccess => base._dataAccess as IOperationSheetDataAccess;

        public OperationSheetService(
            IOperationSheetDataAccess dataAccess,
            ISynchronizationResolverService synchronizationResolverService,
            IGoogleCalendarService googleCalendarService,
            IPushNotificationService pushNotificationService,
            IEmailService emailService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolverService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _googleCalendarService = googleCalendarService;
            _pushNotificationService = pushNotificationService;
            _emailService = emailService;
        }

        private async Task<string> GenerateReferenceAsync()
        {
            var reference = await _configuration.GenerateRefereceAsync(DocumentType.OperationSheet);
            if (!await IsRefrenceUniqueAsync(reference))
            {
                await _configuration.IncrementReferenceAsync(DocumentType.OperationSheet);
                return await GenerateReferenceAsync();
            }

            return reference;
        }

        /// <summary>
        /// after getting the entity by the id
        /// </summary>
        /// <typeparam name="TOut">the output type</typeparam>
        /// <param name="entity">the entity instant</param>
        /// <param name="mappedEntity">the mapped entity instant</param>
        protected override Task InGet_AfterMappingAsync<TOut>(OperationSheet entity, TOut mappedEntity)
        {
            if (mappedEntity is OperationSheetModel expenseModel)
            {
                if (!(entity.Invoice is null))
                    expenseModel.AssociatedDocuments.Add(new AssociatedDocument
                    {
                        Id = entity.Invoice.Id,
                        Status = entity.Invoice.Status,
                        DocumentType = DocumentType.Invoice,
                        Reference = entity.Invoice.Reference,
                        TypeInvoice = entity.Invoice.TypeInvoice,
                        CreationDate = entity.Invoice.CreationDate,
                    });

                if (!(entity.Quote is null))
                    expenseModel.AssociatedDocuments.Add(new AssociatedDocument
                    {
                        Id = entity.Quote.Id,
                        Status = entity.Quote.Status,
                        DocumentType = DocumentType.Quote,
                        Reference = entity.Quote.Reference,
                        CreationDate = entity.Quote.CreatedOn.DateTime,
                    });
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }

        /// <summary>
        /// before inserting the entity to the database
        /// </summary>
        /// <typeparam name="TCreateModel">the create model type</typeparam>
        /// <param name="operationsheet">the operation sheet entity instant</param>
        /// <param name="createModel">the create model entity instant</param>
        protected override async Task InCreate_BeforInsertAsync<TCreateModel>(OperationSheet operationsheet, TCreateModel createModel)
        {
            if (!(createModel is OperationSheetPutModel model))
                throw new ModelTypeIsNotValidException(nameof(createModel), typeof(OperationSheetPutModel));

            // set the Google calendar id
            if (operationsheet.Status == OperationSheetStatus.Planned)
            {
                operationsheet.GoogleCalendarId = await _googleCalendarService.RegisterEventAsync(
                    documentType: T(DocumentType.OperationSheet.ToString()),
                    reference: operationsheet.Reference,
                    startDate: operationsheet.StartDate,
                    endDate: operationsheet.EndDate);
            }

            // add the technicians
            operationsheet.Technicians = model.Technicians
                .Select(e => new OperationSheets_Technicians { OperationSheetId = operationsheet.Id, TechnicianId = e })
                .ToList();

        }

        /// <summary>
        /// after inserting the entity to the database
        /// </summary>
        /// <typeparam name="TCreateModel">the create model type</typeparam>
        /// <param name="operationsheet">the operation sheet entity instant</param>
        /// <param name="createModel">the create model entity instant</param>
        protected override async Task InCreate_AfterInsertAsync<TCreateModel>(OperationSheet entity, TCreateModel model)
        {
            // send a push notification to the technicians
            await _pushNotificationService.SendNotificationAsync(
                usersIds: entity.Technicians.Select(e => e.TechnicianId.ToString()),
                messageTemplteId: TemplateMessages.OperationSheetAssignment,
                placeHolders: new PlaceHolder[]
                {
                    new PlaceHolder(PlaceHolders.reference, entity.Reference),
                    new PlaceHolder(PlaceHolders.ClientName, entity.Client.FullName),
                    new PlaceHolder(PlaceHolders.StartDate, entity.StartDate.ToString(DateHelper.DateFormat)),
                });
        }

        /// <summary>
        /// before updating the entity to the database
        /// </summary>
        /// <typeparam name="TUpdateModel">the update model type</typeparam>
        /// <param name="operationSheet">the operation sheet entity instant</param>
        /// <param name="updateModel">the update model entity instant</param>
        protected override async Task InUpdate_BeforUpdateAsync<TUpdateModel>(OperationSheet operationSheet, TUpdateModel updateModel)
        {
            if (!(updateModel is OperationSheetPutModel model))
                throw new ModelTypeIsNotValidException(nameof(updateModel), typeof(OperationSheetPutModel));

            operationSheet.Workshop = null;

            // set the Google calendar
            if (operationSheet.Status == OperationSheetStatus.Planned && !operationSheet.GoogleCalendarId.IsValid())
            {
                operationSheet.GoogleCalendarId = await _googleCalendarService.RegisterEventAsync(
                    documentType: T(DocumentType.OperationSheet.ToString()),
                    reference: operationSheet.Reference,
                    startDate: operationSheet.StartDate,
                    endDate: operationSheet.EndDate);
            }
            else if (operationSheet.Status == OperationSheetStatus.Planned && operationSheet.GoogleCalendarId.IsValid())
            {
                operationSheet.GoogleCalendarId = await _googleCalendarService.UpdateEventAsync(
                    eventId: operationSheet.GoogleCalendarId,
                    documentType: T(DocumentType.OperationSheet.ToString()),
                    reference: operationSheet.Reference,
                    startDate: operationSheet.StartDate,
                    endDate: operationSheet.EndDate);
            }
            else if (operationSheet.Status != OperationSheetStatus.Planned && operationSheet.GoogleCalendarId.IsValid())
            {
                await _googleCalendarService.DeleteEventAsync(operationSheet.GoogleCalendarId);
            }

            // list of technicians
            var technicians = model.Technicians.Select(e => new OperationSheets_Technicians
            {
                OperationSheetId = operationSheet.Id,
                TechnicianId = e,
            });

            var intersection = operationSheet.Technicians.Intersection(technicians);

            operationSheet.AddressIntervention = operationSheet.Client.BillingAddress;
            // add the new technicians
            await _dataAccess.RemoveOperationSheetTechniciansAsync(intersection.Removed);
            await _dataAccess.AddOperationSheetTechniciansAsync(intersection.Added);
        }

        /// <summary>
        /// before deleting the operation sheet
        /// </summary>
        /// <param name="operationSheet">the operation sheet entity instant</param>
        protected override async Task InDelete_BeforDeleteAsync(OperationSheet operationSheet)
        {
            // delete the Google calendar event
            if (operationSheet.GoogleCalendarId.IsValid())
                await _googleCalendarService.DeleteEventAsync(operationSheet.GoogleCalendarId);
        }

        /// <summary>
        /// re-writing the delete logic of the operation sheet
        /// </summary>
        /// <param name="operationsheet">the operation sheet entity instant</param>
        /// <returns>the operation result</returns>
        protected override async Task<Result> DeleteEntityAsync(OperationSheet operationsheet)
        {
            operationsheet.IsDeleted = true;
            var updateResult = await _dataAccess.UpdateAsync(operationsheet);
            if (!updateResult.IsSuccess)
                return Result.Success("failed to delete the operation sheet");

            return Result.Success("the operation sheet has been deleted successfully");
        }

        public async Task updateForCalendarIdAsync()
        {
            var operationSheets = await _dataAccess.GetAllAsync(null);
            foreach (var operationSheet in operationSheets)
            {
                var model = _mapper.Map<OperationSheetPutModel>(operationSheet);
                await UpdateAsync<OperationSheet, OperationSheetPutModel>(operationSheet.Id, model);
            }
        }

        public override Task<OperationSheet[]> GetFirstsynchronizationDataAsync()
        {
            return _dataAccess.GetFirstsynchronizationDataAsync();
        }

        public override Task<OperationSheet[]> GetNewestObjectsAsync(DateTimeOffset? lastSyncDate, string[] syncObjectsIds)
        {
            return _dataAccess.GetNewestObjectsAsync(lastSyncDate, syncObjectsIds);
        }
    }
}
