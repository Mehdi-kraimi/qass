﻿namespace Axiobat.Services.Documents
{
    using Application.Data;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Documents;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Axiobat.Domain.Constants;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the data service implementation for <see cref="IDocumentService{TEntity}"/>
    /// </summary>
    /// <typeparam name="TDocument">the type of the entity</typeparam>
    public partial class DocumentService<TDocument>
    {
        /// <summary>
        /// check if the given reference is unique
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsRefrenceUniqueAsync(string reference)
            => !await _dataAccess.IsExistAsync(e => e.Reference == reference && e.Status != InvoiceStatus.Draft );

        /// <summary>
        /// get the count of documents the given workshop owns
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns>the count of documents</returns>
        public Task<int> GetWorkShopDocumentsCountAsync(string workshopId)
            => _dataAccess.GetCountAsync(e => e.WorkshopId == workshopId);
    }

    /// <summary>
    /// partial part for <see cref="DocumentService{TDocument}"/>
    /// </summary>
    /// <typeparam name="TDocument"></typeparam>
    public partial class DocumentService<TDocument> : SynchronizeDataService<TDocument, string>, IDocumentService<TDocument>
        where TDocument : Document
    {
        public DocumentService(
            IDocumentDataAccess<TDocument> dataAccess,
            ISynchronizationResolverService synchronizationResolverService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolverService, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }

        /// <summary>
        /// increment the reference of the document
        /// </summary>
        protected Task IncrementRefenereceAsync()
            => _configuration.IncrementReferenceAsync<TDocument>();
    }
}
