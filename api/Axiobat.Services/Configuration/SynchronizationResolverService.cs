﻿namespace Axiobat.Services.Configuration
{
    using Application.Services.Configuration;
    using Axiobat.Domain.Entities;
    using Domain.Interfaces;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the service implementation for <see cref="ISynchronizationResolverService"/>
    /// </summary>
    public partial class SynchronizationResolverService
    {
        /// <summary>
        /// this function is meant to resolve the conflict between two entities, it will compare the two entries looking for changes,
        /// and use the changes history list to get the information about the newest changes on a property, and tack the newest value
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity</typeparam>
        /// <param name="databaseEntity">the entity in the database</param>
        /// <param name="synchronizeWith">the entity that we synchronize with it</param>
        /// <returns>if there is any changes to the entity true will be returned, otherwise false</returns>
        public bool Resolve<TEntity>(TEntity databaseEntity, TEntity synchronizeWith)
            where TEntity : IRecordable
        {
            var changedFields = new List<ChangedField>();

            foreach (var property in typeof(TEntity).GetProperties())
            {
                var databaseEntityPropValue = property.GetValue(databaseEntity);
                var synchronizeWithPropValue = property.GetValue(synchronizeWith);

                // check for nulls
                if (databaseEntityPropValue is null && synchronizeWithPropValue is null)
                    continue;

                // check if the original entity doesn't have a value, give it the value from the synchronize entity
                if (databaseEntityPropValue is null && !(synchronizeWithPropValue is null))
                {
                    property.SetValue(databaseEntity, synchronizeWithPropValue);
                    changedFields.Add(new ChangedField
                    {
                        Champ = "",
                        PropName = property.Name,
                        ValeurInitial = "",
                        ValeurFinal = ""
                    });

                    continue;
                }

                // check if both props are equals
                if (Equals(databaseEntityPropValue, synchronizeWithPropValue))
                    continue;

                // get the name of the property
                var LastUpdateInDb = databaseEntity.LastTimeUpdated(property.Name);
                var LastUpdateInSync = synchronizeWith.LastTimeUpdated(property.Name);

                // check if we are dealing with a complex type
                if (property.PropertyType.IsClass && !(property.PropertyType == typeof(string)))
                {
                    // need special logic
                    continue;
                }

                // the changes from the
                if (LastUpdateInDb < LastUpdateInSync)
                {
                    property.SetValue(databaseEntity, synchronizeWithPropValue);
                    changedFields.Add(new ChangedField
                    {
                        Champ = "",
                        PropName = property.Name,
                        ValeurInitial = "",
                        ValeurFinal = ""
                    });

                    continue;
                }
            }

            if (changedFields.Count > 0)
            {
                databaseEntity.ChangesHistory.Insert(0, new ChangesHistory
                {
                    Action = Domain.Enums.ChangesHistoryType.Updated,
                    User = MinimalUser.Empty,
                    DateAction = DateTime.Now,
                    Information = "resolved by the synchronization",
                    Fields = changedFields,
                });
            }

            return changedFields.Count > 0;
        }
    }

    /// <summary>
    /// partial part for <see cref="SynchronizationResolverService"/>
    /// </summary>
    public partial class SynchronizationResolverService : ISynchronizationResolverService
    {
        private readonly IHistoryService _historyService;

        /// <summary>
        /// create an instant of <see cref=""/>
        /// </summary>
        public SynchronizationResolverService(
            IHistoryService historyService)
        {
            _historyService = historyService;
        }
    }
}
