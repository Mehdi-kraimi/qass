﻿namespace Axiobat.Services.Configuration
{
    using App.Common;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.Configuration;
    using AutoMapper;
    using Domain.Entities;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implantation for
    /// </summary>
    public partial class ClassificationService
    {
        private async Task ValidatePutModelAsync(ClassificationPutModel[] model)
        {
            var parentCategoriesIds = model
                .Where(category => category.ParentId.HasValue)
                .Select(category => (int)category.ParentId)
                .Distinct()
                .ToArray();

            if (!await _dataAccess.IsAllkeysExistAsync(parentCategoriesIds))
                throw new ValidationException("one of the given classification has a parent ids of a classification that not exist", MessageCode.ParentCategoryNotFound);

            var categoriesToUpdateIds = model
                .Where(category => category.Id.HasValue)
                .Select(category => (int)category.Id)
                .Distinct()
                .ToArray();

            if (!await _dataAccess.IsAllkeysExistAsync(categoriesToUpdateIds))
                throw new ValidationException("one of the classification to update not exist", MessageCode.NotFound);

            var chartOfAccountsIds = model
                .Where(category => category.ChartAccountItemId.IsValid())
                .Select(category => category.ChartAccountItemId)
                .Distinct()
                .ToArray();

            if (!await _chartOfAccountsService.IsAllExistAsync(chartOfAccountsIds))
                throw new ValidationException("one of the given classification has an id of a chart of account that not exist", MessageCode.NotFound);
        }

        public async Task PutAsync(ClassificationPutModel[] model)
        {
            await ValidatePutModelAsync(model);

            var classificationToAdd = _mapper.Map<Classification[]>(model.Where(category => !category.Id.HasValue));
            if (classificationToAdd.Any())
            {
                var assetsClassification = classificationToAdd.Where(e => e.ParentId == Classification.AssetsPurchases);
                if (assetsClassification.Any())
                {
                    var charts = await _chartOfAccountsService.GetChartOfAccountsAsync();
                    var assetsCharts = charts[ChartOfAccounts.Assets].SubCategoriesIds();

                    if (assetsClassification.Any(e => !assetsCharts.Contains(e.ChartAccountItemId)))
                        throw new ValidationException("assets classification must reference chart account of assets", MessageCode.InvalidClassificationChartAccount);
                }

                var insertResult = await _dataAccess.AddRangeAsync(classificationToAdd);
            }

            var classificationToUpdateAsModel = model.Where(category => category.Id.HasValue).ToDictionary(category => category.Id);
            if (classificationToUpdateAsModel.Any())
            {
                var classificationIoUpdate = await _dataAccess.GetByIdAsync(classificationToUpdateAsModel.Select(e => (int)e.Key).ToArray());

                foreach (var classification in classificationIoUpdate)
                {
                    var categoryModel = classificationToUpdateAsModel[classification.Id];
                    categoryModel.Update(classification);
                }

                var updateResult = await _dataAccess.UpdateRangeAsync(classificationIoUpdate);
            }
        }

        public async Task<TOut[]> GetCategoriesByTypeAsync<TOut>(ClassificationType categoryType)
        {
            var classifications = await _dataAccess.GetAllAsync(options =>
            {
                options.AddPredicate(e => e.Type == categoryType && e.ParentId == null);
            });

            return _mapper.Map<TOut[]>(classifications);
        }

        public async Task<ListResult<TOut>> GetAllAsync<TOut>()
        {
            var classification = await _dataAccess.GetAllAsync(options =>
            {
                options.AddPredicate(e => e.ParentId == null);
            });
            return Result.ListSuccess(_mapper.Map<IEnumerable<TOut>>(classification));
        }

        public async Task<Result> DeleteAsync(int categoryId)
        {
            var entity = await _dataAccess.GetByIdAsync(categoryId);
            if (entity is null)
                throw new NotFoundException($"failed to delete the chart of account not found");

            if (entity.Id == Classification.AssetsPurchases)
                throw new ValidationException("you can't delete this classification it required", MessageCode.RequiredValue);

            return await _dataAccess.DeleteAsync(entity);
        }

        public async Task<Result<TOut>> GetByIdAsync<TOut>(int categoryId)
        {
            var entity = await _dataAccess.GetByIdAsync(categoryId);
            if (entity is null)
                throw new NotFoundException($"the chart of account not exist");

            var mappedEntity = _mapper.Map<TOut>(entity);

            return mappedEntity;
        }

        public Task<Classification> GetDefaultClassificationAsync()
        {
            var classification = _dataAccess.GetSingleAsync(options =>
            {
                // ToDo: Set a logic for retrieving the default product classification
                options.AddPredicate(c => c.Id == 42);
            });
            // to do
            return classification;
        }

        public async Task<dynamic> SynchronizeAsync(DateTimeOffset? LastSyncDate, IEnumerable<SyncObject> ObjectsToSync)
        {
            // if the first synchronization rerun everything
            if (!LastSyncDate.HasValue)
                return new
                {
                    docType = typeof(Classification).DocType(),
                    objects = await _dataAccess.GetAllAsync(options => options.AddDefaultIncludes(false)),
                };

            var objectsToReturn = await _dataAccess.GetAllAsync(options =>
            {
                options.AddDefaultIncludes(false);
                options.AddPredicate(e => e.LastModifiedOn > LastSyncDate);
            });

            // return the data
            return new
            {
                docType = typeof(Classification).DocType(),
                objects = objectsToReturn,
            };
        }
    }

    public partial class ClassificationService : IClassificationService, ISynchronize
    {
        private readonly IChartOfAccountsService _chartOfAccountsService;
        private readonly IClassificationDataAccess _dataAccess;
        private readonly IMapper _mapper;

        public ClassificationService(
            IChartOfAccountsService chartOfAccountsService,
            IClassificationDataAccess dataAccess,
            IMapper mapper)
        {
            _chartOfAccountsService = chartOfAccountsService;
            _dataAccess = dataAccess;
            _mapper = mapper;
        }


    }
}
