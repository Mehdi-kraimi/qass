﻿namespace Axiobat.Services.Configuration
{
    using App.Common;
    using App.Common.Exceptions;
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Entities.Configuration;
    using Domain.Enums;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the <see cref="IApplicationConfigurationService"/> implementation
    /// </summary>
    public partial class ApplicationConfigurationService
    {
        /// <summary>
        /// get the email address
        /// </summary>
        /// <param name="emailId">the id of the email to retrieve, one of <see cref="Emails"/></param>
        /// <returns>the email address</returns>
        public async Task<string> GetEmailAsync(string emailId)
        {
            var emails = await GetAsync<Dictionary<string, string>>(ApplicationConfigurationType.StaticEmails);
            if (emails.Count <= 0)
                throw new NotFoundException("the emails list is empty");

            if (!emails.TryGetValue(emailId, out string email))
                throw new NotFoundException("email not exist");

            return email;

        }

        /// <summary>
        /// get the email template id
        /// </summary>
        /// <param name="emailTemplateName">the name of the template, one of <see cref="EmailTemplates"/> values</param>
        /// <returns>the id of the email template</returns>
        public async Task<string> GetEmailTemplateIdAsync(string emailTemplateName)
        {
            var templates = await GetAsync<Dictionary<string, string>>(ApplicationConfigurationType.EmailTemplates);
            if (templates.Count <= 0)
                throw new NotFoundException("the email templates list is empty");

            if (!templates.TryGetValue(emailTemplateName, out string template))
                throw new NotFoundException("email templates not exist");

            return template;
        }

        /// <summary>
        /// get the setting of the given type
        /// </summary>
        /// <param name="configurationType">the type of the setting to be retrieved, must be one of <see cref="ApplicationConfiguration"/> values</param>
        /// <returns>the setting value as a string</returns>
        public async Task<string> GetAsync(string configurationType)
        {
            if (!configurationType.IsValid())
                throw new ArgumentException("the given setting type is null or empty", nameof(configurationType));

            if (!ApplicationConfigurationType.IsValid(configurationType))
                throw new AppException("the given setting type is not valid", ((int)MessageCode.InvalidApplicationSettingType).ToString());

            var configuration = await _dataAccess.GetConfigurationAsync(configurationType);
            if (configuration is null)
                throw new NotFoundException("the setting of type [{settingType}] doesn't exist");

            return configuration.Value;
        }

        /// <summary>
        /// get the setting of the given type, and deserialize the setting value to the given type
        /// </summary>
        /// <typeparam name="TOut">the type of the settings</typeparam>
        /// <param name="configurationType">the type of the setting to be retrieved, must be one of <see cref="ApplicationConfiguration"/> values</param>
        /// <returns>the setting value as an object of the given type</returns>
        public async Task<TOut> GetAsync<TOut>(string configurationType)
            => (await GetAsync(configurationType)).FromJson<TOut>();

        /// <summary>
        /// retrieve the document configuration of the given document type
        /// </summary>
        /// <param name="documentType">the type of the document to retrieve the configuration for it</param>
        /// <returns>the configuration of  the document</returns>
        public async Task<DocumentConfiguration> GetDocumentConfigAsync(DocumentType documentType)
        {
            var configurations = await GetAsync<IEnumerable<DocumentConfiguration>>(ApplicationConfigurationType.DocumentConfiguration);
            if (configurations.Count() <= 0)
                throw new NotFoundException("there is no document configuration, please set the one in the database");

            return configurations.First(e => e.DocumentType == documentType);
        }

        /// <summary>
        /// get  pdf options
        /// </summary>
        /// 
        /// <returns>the configuration of  the document</returns>
        //public async Task<DocumentConfiguration> GetDocumentConfigAsync(DocumentType documentType)
        //{
        //    var configurations = await GetAsync<IEnumerable<DocumentConfiguration>>(ApplicationConfigurationType.DocumentConfiguration);
        //    if (configurations.Count() <= 0)
        //        throw new NotFoundException("there is no document configuration, please set the one in the database");

        //    return configurations.First(e => e.DocumentType == documentType);
        //}

        /// <summary>
        /// update the value of the configuration based on the given configuration type
        /// </summary>
        /// <param name="configurationType">the configuration type</param>
        /// <param name="value">the updated value of the configuration</param>
        public async Task UpdateAsync(string configurationType, string value)
        {
            var configuration = await _dataAccess.GetConfigurationAsync(configurationType);
            if (configuration is null)
                throw new NotFoundException("the setting of type [{settingType}] doesn't exist");

            // ToDo: we should implement a validation logic

            configuration.Value = value;
            configuration.LastModifiedOn = DateTimeOffset.Now;

            await _dataAccess.UpdateConfigurationAsync(configuration);
        }

        /// <summary>
        /// generate a unique reference for the given document
        /// </summary>
        /// <param name="docType">the document type</param>
        /// <returns>the string value</returns>
        public async Task<string> GenerateRefereceAsync(DocumentType docType)
        {
            // get the counters
            var counters = await GetAsync<IEnumerable<Numerator>>(ApplicationConfigurationType.Counter);

            // retrieve the document counter
            var documentCounter = counters.FirstOrDefault(c => c.DocumentType == docType);
            if (documentCounter is null)
                throw new NotFoundException($"the {docType} has no counter configured");

            return documentCounter.Generate();
        }

        /// <summary>
        /// use this method to increment the reference counter by one
        /// </summary>
        /// <param name="docType">the document to increment the counter for it</param>
        /// <returns>the operation result</returns>
        public async Task IncrementReferenceAsync(DocumentType docType)
        {
            // check the document type
            if (docType == DocumentType.Undefined)
                return;

            // get the counter configuration
            var configuration = await _dataAccess.GetConfigurationAsync(ApplicationConfigurationType.Counter);
            if (configuration is null)
                throw new NotFoundException("the configuration of type [counter] doesn't exist");

            // get the counters
            var counters = configuration.Value.FromJson<List<Numerator>>();

            // retrieve document
            var documentCounter = counters.FirstOrDefault(c => c.DocumentType == docType);
            if (documentCounter is null)
            {
                _logger.LogWarning("there is no counter configured for the docType [{docType}]", docType);
                return;
            }

            documentCounter.Increment();
            configuration.Value = counters.ToJson();

            await _dataAccess.UpdateConfigurationAsync(configuration);
        }

        /// <summary>
        /// use this method to increment the reference counter by one
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity to increment the reference for it</typeparam>
        /// <returns>returns>
        public Task IncrementReferenceAsync<TEntity>()
            => IncrementReferenceAsync(typeof(TEntity).DocType());

        /// <summary>
        /// get list of all categories
        /// </summary>
        /// <returns>the list of categories</returns>
        public async Task<ListResult<ChartAccountItemModel>> GetAllCategoriesAsync()
        {
            var data = await _dataAccess.GetAllCategoriesAsync();
            return Result.ListSuccess(_mapper.Map<IEnumerable<ChartAccountItemModel>>(data));
        }

        /// <summary>
        /// get list of all entities
        /// </summary>
        /// <typeparam name="TEntity">the entity type</typeparam>
        /// <typeparam name="TModel">the model type</typeparam>
        /// <returns>the list result</returns>
        public async Task<ListResult<TModel>> GetAllAsync<TEntity, TModel>() where TEntity : Entity
            => Result.ListSuccess(_mapper.Map<IEnumerable<TModel>>(await _dataAccess.GetAllAsync<TEntity>()));

        /// <summary>
        /// retrieve list of all taxes
        /// </summary>
        /// <returns>the list of taxes</returns>
        public async Task<PagedResult<TModel>> GetAllAsync<TEntity, TModel>(FilterOptions filter) where TEntity : Entity
        {
            var unites = await _dataAccess.GetAllAsync<TEntity>(filter);
            if (!unites.IsSuccess)
                return Result.ListFrom<TModel, TEntity>(unites);

            return Result.PagedSuccess(_mapper.Map<IEnumerable<TModel>>(unites.Value), unites);
        }

        /// <summary>
        /// retrieve list of all taxes
        /// </summary>
        /// <returns>the list of taxes</returns>
        public async Task<Result<TModel>> GetByIdAsync<TKey, TEntity, TModel>(TKey entityId) where TEntity : Entity<TKey>
        {
            var unit = await _dataAccess.GetbyIdAsync<TEntity, TKey>(entityId);

            if (unit is null)
                return Result.Failed<TModel>($"there is no {typeof(TEntity).Name} with the given id", MessageCode.NotFound);

            return _mapper.Map<TModel>(unit);
        }

        public async Task<Result<TModel>> PutAsync<TModel, TEntity, TKey>(TModel model)
            where TModel : IModel<TEntity, TKey>, IUpdateModel<TEntity>
            where TEntity : Entity<TKey>
        {
            var entity = await _dataAccess.GetbyIdAsync<TEntity, TKey>(model.Id);

            // the entity not exist, inserting it as a new one
            if (entity is null || ReferenceEquals(model.Id, null) || (model.Id.Equals(0) || model.Id.Equals("") || model.Id.Equals("0")))
            {
                var entityFromModel = _mapper.Map<TEntity>(model);
                var addResult = await _dataAccess.AddAsync<TEntity, TKey>(entityFromModel);
                if (!addResult.IsSuccess)
                    return Result.From<TModel>(addResult);

                return _mapper.Map<TModel>(addResult.Value);
            }

            // the entity not exist
            if (entity is null && !model.Equals(0))
                return Result.Failed<TModel>($"Failed to retrieve the {nameof(TEntity)} with id: {model.Id}");

            // update the entity
            model.Update(entity);
            var updateResult = await _dataAccess.UpdateAsync<TEntity, TKey>(entity);
            if (!updateResult.IsSuccess)
                return Result.From<TModel>(updateResult);

            // return the result
            return _mapper.Map<TModel>(updateResult.Value);
        }

        /// <summary>
        /// get list of all countries
        /// </summary>
        /// <returns>the list of all countries</returns>
        public async Task<ListResult<Country>> GetAllCountriesAsync()
        {
            var data = await _countryDataAccess.GetAllAsync();
            return Result.ListSuccess(data);
        }

        /// <summary>
        /// get the country with the given code
        /// </summary>
        /// <param name="countryCode">the country code</param>
        /// <returns>the country</returns>
        public async Task<Result<Country>> GetCountryByCodeAsync(string countryCode)
        {
            return await _countryDataAccess.GetCountryByCodeAsync(countryCode);
        }

        public async Task<Result> DeleteAsync<TEntity, TKey>(TKey entityId) where TEntity : Entity<TKey>
        {
            var entity = await _dataAccess.GetbyIdAsync<TEntity, TKey>(entityId);
            if (entity is null)
                throw new ValidationException($"there is no {typeof(TEntity).Name} with the given id", MessageCode.NotFound);

            // if we are deleting a payment method first we validate if it has any payment associated with
            if (entity is PaymentMethod paymentMethod)
            {
                int result = await _dataAccess.GetPaymentMethodPaymentsCountAsync(paymentMethod.Id);
                if (result > 0)
                    throw new ValidationException(T("you can't delete the payment method with id {0}, it has {1} payments associated with", entityId, result), MessageCode.ValidationFailed);
            }

            return await _dataAccess.DeleteAsync(entity);
        }

        public Task<WorkshopDocumentType> GetSpecialDocumentTypeAsync()
            => _dataAccess.GetSpecialDocumentTypeAsync();

        public async Task<Result<PaymentMethodModel>> GetCreditNotePaymentMethodAsyn()
        {
            var method = await _dataAccess.GetCreditNotePaymentMethodAsync();
            return _mapper.Map<PaymentMethodModel>(method);
        }

        public async Task<ListResult<TurnoverGoalModel>> GetTurnoverGoalsAsync()
        {
            var result = await _dataAccess.GetTurnoverGoalsAsync();
            return _mapper.Map<List<TurnoverGoalModel>>(result);
        }

        public async Task<dynamic> SynchronizeAsync(DateTimeOffset? LastSyncDate, IEnumerable<SyncObject> ObjectsToSync)
        {
            var objects = await _dataAccess.GetConfigurationAsync(new[] {
                    ApplicationConfigurationType.Counter,
                    ApplicationConfigurationType.DefaultValues,
                    ApplicationConfigurationType.DocumentConfiguration,
                }, LastSyncDate);
            return new
            {
                docType = typeof(ApplicationConfiguration).DocType(),
                objects = await _dataAccess.GetConfigurationAsync(new[] {
                    ApplicationConfigurationType.Counter,
                    ApplicationConfigurationType.DefaultValues,
                    ApplicationConfigurationType.DocumentConfiguration,
                }, LastSyncDate),
            };
        }
    }

    /// <summary>
    /// partial part for <see cref="ApplicationConfigurationService"/>
    /// </summary>
    public partial class ApplicationConfigurationService : IApplicationConfigurationService
    {
        private readonly IApplicationConfigurationDataAccess _dataAccess;
        private readonly ILoggedInUserService _loggedInUserService;
        private readonly ICountryDataAccess _countryDataAccess;
        private readonly ITranslationService _translator;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        /// <summary>
        /// create an instant of <see cref="ApplicationConfigurationService"/>
        /// </summary>
        /// <param name="appConfigDataAccess">the <see cref="IApplicationConfigurationDataAccess"/> instant</param>
        /// <param name="mapper">the <see cref="IMapper"/> instant</param>
        public ApplicationConfigurationService(
            IApplicationConfigurationDataAccess appConfigDataAccess,
            ILoggedInUserService loggedInUserService,
            ICountryDataAccess countryDataAccess,
            ITranslationService translationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
        {
            _logger = loggerFactory.CreateLogger<ApplicationConfigurationService>();
            _countryDataAccess = countryDataAccess;
            _translator = translationService;
            _dataAccess = appConfigDataAccess;
            _loggedInUserService = loggedInUserService;
            _mapper = mapper;
        }

        /// <summary>
        /// get the localized string version of the given string
        /// </summary>
        /// <param name="stringToLocalize">the string value to localize</param>
        /// <returns>the localized version</returns>
        protected string T(string stringToLocalize, params object[] args)
            => _translator.Get(stringToLocalize, args);
    }
}
