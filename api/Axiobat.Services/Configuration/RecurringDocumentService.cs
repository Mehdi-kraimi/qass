﻿namespace Axiobat.Services.Configuration
{
    using Application.Data;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Domain.Entities;
    using Domain.Constants;
    using Domain.Enums;
    using Domain.Exceptions;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implantation for <see cref="IRecurringDocumentService"/>
    /// </summary>
    public partial class RecurringDocumentService
    {
        public async Task UpdateStatusAsync(string recurringId, DocumentUpdateStatusModel model)
        {
            var recurring = await _dataAccess.GetByIdAsync(recurringId);
            if (recurring is null)
                throw new NotFoundException("there is no recurring with the given id");

            recurring.Status = model.Status;

            await _dataAccess.UpdateAsync(recurring);
        }

        public async Task UpdateRecurringDocumentInfoAsync<TDocument>(string recurringId, TDocument document)
        {
            var recurring = await _dataAccess.GetByIdAsync(recurringId);
            if (recurring is null)
                throw new NotFoundException("there is no recurring with the given id");

            recurring.Document = JObject.FromObject(document);
            recurring.DocType = typeof(TDocument).DocType();

            await _dataAccess.UpdateAsync(recurring);
        }
    }

    public partial class RecurringDocumentService : SynchronizeDataService<RecurringDocument, string>, IRecurringDocumentService
    {
        public RecurringDocumentService(
            IRecurringDocumentDataAccess dataAccess,
            ISynchronizationResolverService synchronizationResolver,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolver, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }

        protected override Task InCreate_BeforInsertAsync<TCreateModel>(RecurringDocument entity, TCreateModel createModel)
        {
            entity.NextOccurring = entity.StartingDate;
            return base.InCreate_BeforInsertAsync(entity, createModel);
        }

        protected override Task InUpdate_ValidateEntityAsync(RecurringDocument entity, IUpdateModel<RecurringDocument> updateModel)
        {
            if (updateModel is RecurringDocumentPutModel model)
            {
                if (model.StartingDate != entity.StartingDate)
                {
                    if (model.StartingDate <= DateTime.Now)
                        throw new ValidationException("the starting date must be value in the future", MessageCode.InvalidData);
                }
            }

            return base.InUpdate_ValidateEntityAsync(entity, updateModel);
        }
    }
}
