﻿namespace Axiobat.Services.Maintenance
{
    using App.Common;
    using Application.Data;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.Maintenance;
    using AutoMapper;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation for <see cref="IEquipmentMaintenanceService"/>
    /// </summary>
    public partial class EquipmentMaintenanceService
    {
        /// <summary>
        /// check if the given equipment name is unique
        /// </summary>
        /// <param name="equipmentName">the name of the equipment</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsNameUniqueAsync(string equipmentName)
            => ! await _dataAccess.IsExistAsync(e => e.EquipmentName == equipmentName);
    }

    /// <summary>
    /// partial part for <see cref="EquipmentMaintenanceService"/>
    /// </summary>
    public partial class EquipmentMaintenanceService : SynchronizeDataService<EquipmentMaintenance, string>, IEquipmentMaintenanceService
    {
        protected new IEquipmentMaintenanceDataAccess _dataAccess => base._dataAccess as IEquipmentMaintenanceDataAccess;

        public EquipmentMaintenanceService(
            IEquipmentMaintenanceDataAccess dataAccess,
            ISynchronizationResolverService synchronizationResolver,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolver, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
        }

        protected override Task InCreate_BeforInsertAsync<TCreateModel>(EquipmentMaintenance entity, TCreateModel createModel)
        {
            foreach (var operation in entity.MaintenanceOperations)
            {
                if (!operation.Id.IsValid())
                    operation.Id = Guid.NewGuid().ToString();

                foreach (var subOperation in operation.SubOperations)
                {
                    if (!subOperation.Id.IsValid())
                        subOperation.Id = Guid.NewGuid().ToString();
                }
            }

            return base.InCreate_BeforInsertAsync(entity, createModel);
        }
    }
}
