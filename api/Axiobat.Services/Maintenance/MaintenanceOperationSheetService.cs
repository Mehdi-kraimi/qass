﻿namespace Axiobat.Services.Maintenance
{
    using App.Common;
    using Application.Data;
    using Application.Enums;
    using Application.Exceptions;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Application.Services.MailService;
    using Application.Services.Maintenance;
    using Application.Services.PushNotification;
    using AutoMapper;
    using Axiobat.Domain.Constants;
    using Axiobat.Domain.Enums;
    using Axiobat.Domain.Exceptions;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation for <see cref="IMaintenanceOperationSheetService"/>
    /// </summary>
    public partial class MaintenanceOperationSheetService
    {
        /// <summary>
        /// check if the given reference is unique
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        public async Task<bool> IsRefrenceUniqueAsync(string reference)
            => !await _dataAccess.IsExistAsync(e => e.Reference == reference);

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateStatusAsync(string operationSheetId, DocumentUpdateStatusModel model)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(operationSheetId);
            if (document is null)
                throw new NotFoundException("there is no operation Sheet with the given id");

            // update the devis status
            document.Status = model.Status;
            var updateResult = await _dataAccess.UpdateAsync(document);
            if (!updateResult.IsSuccess)
                return Result.From(updateResult);

            if (document.Status == MaintenanceOperationSheetStatus.Realized && document.MaintenanceVisitId.IsValid())
            {
                await _maintenanceVisitService.UpdateStatusAsync(document.MaintenanceVisitId, MaintenanceVisitStatus.Realized);
            }

            return Result.Success();
        }

        /// <summary>
        /// update the signature of the operation Sheet
        /// </summary>
        /// <param name="operationSheetId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the signature</param>
        /// <returns>the operation result</returns>
        public async Task<Result> UpdateSignatureAsync(string operationSheetId, DocumentUpdateSignatureModel model)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(operationSheetId);
            if (document is null)
                throw new NotFoundException("there is no operation Sheet with the given id");

            // update the devis status
            document.Status = MaintenanceOperationSheetStatus.Realized;
            document.ClientSignature = model.ClientSignature;
            document.TechnicianSignature = model.TechnicianSignature;
            var updateResult = await _dataAccess.UpdateAsync(document);
            if (!updateResult.IsSuccess)
                return Result.From(updateResult);

            if (document.MaintenanceVisitId.IsValid())
                await _maintenanceVisitService.UpdateStatusAsync(document.MaintenanceVisitId, MaintenanceVisitStatus.Realized);

            return Result.Success();
        }

        /// <summary>
        /// send the operationSheet with the given id with an email using the given options
        /// </summary>
        /// <param name="operationSheetId">the id of the document</param>
        /// <param name="emailOptions">the email options</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SendAsync(string operationSheetId, SendEmailOptions emailOptions)
        {
            // retrieve the document
            var document = await _dataAccess.GetByIdAsync(operationSheetId);
            if (document is null)
                throw new NotFoundException("there is no Maintenance Operation sheet with the given id");

            // generate the document Pdf
            //var filePDFResult = await ExportDataAsync(operationSheetId, new DataExportOptions { ExportType = ExportType.PDF });
            //if (!filePDFResult.IsSuccess)
            //    return filePDFResult;

            // generate an id to the email
            emailOptions.Id = $"{operationSheetId}-".AppendTimeStamp();

            // add the PDF file to the email attachments
            //emailOptions.Attachments.Add(new AttachmentModel
            //{
            //    FileType = "application/pdf",
            //    Content = Convert.ToBase64String(filePDFResult),
            //    FileName = $"{DocumentType.MaintenanceOperationSheet}-{DateHelper.Timestamp}.pdf",
            //});

            // send the email
            var result = await _emailService.SendEmailAsync(emailOptions);
            if (!result.IsSuccess)
                return result;

            // add the email document
            document.Emails.Add(new DocumentEmail
            {
                To = emailOptions.To,
                SentOn = DateTime.Now,
                MailId = emailOptions.Id,
                Content = emailOptions.Body,
                Subject = emailOptions.Subject,
                User = _loggedInUserService.GetMinimalUser(),
            });

            // update the document
            return await _dataAccess.UpdateAsync(document);
        }

        /// <summary>
        /// save the memo to the client with the given id
        /// </summary>
        /// <param name="entityId">the id of the client to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        public async Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel model)
        {
            var memo = new Memo
            {
                Id = model.Id,
                Comment = model.Comment,
                CreatedOn = DateTime.Now,
                Attachments = await _fileService.SaveAsync(model.Attachments.ToArray()),
                User = new MinimalUser(_loggedInUserService.User.UserId, _loggedInUserService.User.UserName),
            };

            var updateResult = await _dataAccess.UpdateAsync(entityId, entity => entity.Memos.Add(memo));
            if (!updateResult.HasValue)
                return Result.From<Memo>(updateResult);

            _logger.LogDebug(LogEvent.SavingMemo, "a new memo with id: [{memoId}] has been saved", memo.Id);
            return memo;
        }

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memoModel">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        public async Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memoModel)
        {
            // retrieve entity
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no entity with the given id");

            // retrieve memo
            var memo = entity.Memos.FirstOrDefault(e => e.Id == memoId);
            if (memo is null)
                return Result.Failed<Memo>("Failed to update the memo, there is no memo with the given id");

            // update the memo
            memo.Comment = memoModel.Comment;

            // set the attachment deferences
            var intersection = memo.Attachments.Select(e => new AttachmentModel
            {
                FileId = e.FileId,
                FileName = e.FileName,
                FileType = e.FileType
            })
            .Intersection(memoModel.Attachments);

            // delete attachments from the server and memo
            var removed = intersection.Removed.Select(e => new Attachment(e.FileId, e.FileName, e.FileType)).ToArray();
            var Added = memoModel.Attachments.Where(e => intersection.Added.Select(a => a.FileId).Contains(e.FileId));
            var attachementToRemove = memo.Attachments.Where(e => removed.Select(a => a.FileId).Contains(e.FileId)).ToArray();

            await _fileService.DeleteAsync(removed);

            foreach (var attachment in attachementToRemove)
                memo.Attachments.Remove(attachment);

            // add the new attachments
            var newAttachments = await _fileService.SaveAsync(Added.ToArray());

            foreach (var attachment in newAttachments)
                memo.Attachments.Add(attachment);

            // update the entity
            var updateResult = await _dataAccess.UpdateAsync(entity);
            if (!updateResult.IsSuccess)
                return Result.Failed<Memo>("failed to update the entity");

            return memo;
        }

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model)
        {
            var entity = await _dataAccess.GetByIdAsync(entityId);
            if (entity is null)
                throw new NotFoundException("entity not found");

            var memosToDelete = entity.Memos.Where(e => model.MemosIds.Contains(e.Id)).ToList();
            var attachments = memosToDelete.SelectMany(e => e.Attachments).ToArray();

            foreach (var memo in memosToDelete)
                entity.Memos.Remove(memo);

            var deleteResult = await _dataAccess.UpdateAsync(entity);
            if (!deleteResult.IsSuccess)
            {
                _logger.LogCritical(LogEvent.DeleteMemos, "Failed to delete the memos check previous logs");
                Result.Failed("Failed to delete memos");
            }

            var deleted = await _fileService.DeleteAsync(attachments);
            _logger.LogInformation(LogEvent.DeleteMemos, "[{deletedFiles}] file has been deleted out of [{totalFiles}]", deleted, attachments.Length);

            return Result.Success();
        }

        /// <summary>
        /// get the operation sheet by reference
        /// </summary>
        /// <typeparam name="TOut">the type of the output</typeparam>
        /// <param name="operationSheetReference">the reference of the operation sheet</param>
        /// <returns>the output type</returns>
        public async Task<Result<TOut>> GetByReferenceAsync<TOut>(string operationSheetReference)
        {
            var operationSheet = await _dataAccess.GetSingleAsync(options => options.AddPredicate(e => e.Reference == operationSheetReference));
            if (operationSheet is null)
                return Result.Failed<TOut>("there is no operation sheet with the given reference");

            return Map<TOut>(operationSheet);
        }


        public async Task<Result> SetStatusToUndoneAsync(string operationSheetId, MaintenanceOperationSheetMotif model)
        {
            var operationSheet = await _dataAccess.GetSingleAsync(option =>
            {
                option.AddPredicate(e => e.Id == operationSheetId);
            });

            if (operationSheet is null)
                throw new NotFoundException("there is no operation sheet with the given id");

            _history.Recored(operationSheet, ChangesHistoryType.Updated, fields: new[]
            {
                new ChangedField("status", operationSheet.Status, MaintenanceOperationSheetStatus.UnDone)
            });

            operationSheet.Status = MaintenanceOperationSheetStatus.UnDone;

            if (operationSheet.UnDoneMotif is null)
                operationSheet.UnDoneMotif = new HashSet<MaintenanceOperationSheetMotif>();

            operationSheet.UnDoneMotif.Add(new MaintenanceOperationSheetMotif
            {
                Description = model.Description,
                User = _loggedInUserService.GetMinimalUser(),
            });

            var updateResult = await _dataAccess.UpdateAsync(operationSheet);
            return Result.From(updateResult);
        }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceOperationSheetService"/>
    /// </summary>
    public partial class MaintenanceOperationSheetService : SynchronizeDataService<MaintenanceOperationSheet, string>, IMaintenanceOperationSheetService
    {
        private readonly IMaintenanceVisitService _maintenanceVisitService;
        private readonly IClientService _clientService;
        private readonly IPushNotificationService _pushNotificationService;
        private readonly IEmailService _emailService;

        public MaintenanceOperationSheetService(
            IMaintenanceVisitService maintenanceVisitService,
            ISynchronizationResolverService synchronizationResolver,
            IClientService clientservice,
            IPushNotificationService pushNotificationService,
            IMaintenanceOperationSheetDataAccess dataAccess,
            IEmailService emailService,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, synchronizationResolver, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _maintenanceVisitService = maintenanceVisitService;
            _clientService = clientservice;
            _pushNotificationService = pushNotificationService;
            _emailService = emailService;
        }

        protected override async Task InCreate_BeforInsertAsync<TCreateModel>(MaintenanceOperationSheet entity, TCreateModel createModel)
        {
            // this creation is intended for a operation sheet of type maintenance
            if (createModel is MaintenanceOperationSheetCreateModel model)
            {
                var maintenanceVisit = await _maintenanceVisitService.CreateMaintenanceVisitAsync<MaintenanceVisit>(model.MaintenanceContractId, model.Year, model.Month, model.Client);
                if (!(maintenanceVisit is null))
                {
                    // associate the MaintenanceVisit with the Maintenance Operation Sheet
                    entity.MaintenanceVisitId = maintenanceVisit.Id;
                }

                // set the equipment form the maintenanceVisit
                entity.EquipmentDetails = maintenanceVisit.EquipmentDetails;
            }
            else if (createModel is AfterSalesServiceOperationSheetCreateModel afterSalesModel)
            {

            }
            else
            {
                throw new ModelTypeIsNotValidException(nameof(createModel), typeof(MaintenanceOperationSheetBaseCreateModel));
            }
        }

        protected override async Task InCreate_AfterInsertAsync<TCreateModel>(MaintenanceOperationSheet entity, TCreateModel createModel)
        {
            // send a push notification to the technicians
            await _pushNotificationService.SendNotificationAsync(
                usersIds: new string[] { entity.TechnicianId.ToString() },
                messageTemplteId: TemplateMessages.OperationSheetAssignment,
                placeHolders: new PlaceHolder[]
                {
                    new PlaceHolder(PlaceHolders.reference, entity.Reference),
                    new PlaceHolder(PlaceHolders.ClientName, entity.Client.FullName),
                    new PlaceHolder(PlaceHolders.StartDate, entity.StartDate.ToString(DateHelper.DateFormat)),
                });
        }

        protected override Task InUpdate_BeforUpdateAsync<TUpdateModel>(MaintenanceOperationSheet operationSheet, TUpdateModel updateModel)
        {
            operationSheet.Technician = null;
            return Task.CompletedTask;
        }

        protected override Task InGet_AfterMappingAsync<TOut>(MaintenanceOperationSheet entity, TOut mappedEntity)
        {
            if (mappedEntity is InvoiceModel model)
            {
                model.AssociatedDocuments.Add(Map<AssociatedDocument>(entity.Invoice));
            }

            return base.InGet_AfterMappingAsync(entity, mappedEntity);
        }
    }
}
