﻿namespace Axiobat.Services
{
    using Application;
    using Application.Data;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using AutoMapper;
    using Axiobat.Domain.Enums;
    using Domain.Interfaces;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public abstract partial class SynchronizeDataService<TEntity, TKey>
    {
        /// <summary>
        /// this function should hold the logic for Processing Synchronization logic
        /// </summary>
        /// <param name="lastSyncDate">the date of the last synchronization, if null that means it the first synchronization</param>
        /// <param name="objectsToSync">the list of objects to synchronization</param>
        /// <returns>the list of synchronized data</returns>
        public virtual async Task<dynamic> SynchronizeAsync(DateTimeOffset? lastSyncDate, IEnumerable<SyncObject> objectsToSync)
        {
            // if the first synchronization rerun everything
            if (!lastSyncDate.HasValue)
                return new
                {
                    docType = typeof(TEntity).DocType(),
                    objects = await GetFirstsynchronizationDataAsync(),
                };

            var entitiesToAdd = objectsToSync.EntitiesToAdd<TEntity>();
            if (entitiesToAdd.Any())
            {
                // ToDo: we should check if we have a reference to something that has been removed from the database, this will case an exception
                var addResult = await _dataAccess.AddRangeAsync(entitiesToAdd, false);
            }

            // set the list of the objects to be returned
            var objectsToReturn = new List<TEntity>();

            // get the entities to be updated
            var entitiesToUpdate = objectsToSync.EntitiesToUpdate<TEntity>();
            if (entitiesToUpdate.Any())
            {
                // get the entities that has been updated
                var entitiesInDb = (await _dataAccess
                    .GetByIdAsync(entitiesToUpdate.Select(e => e.Id).ToArray()))
                    .ToLookup(entity => entity.Id);

                var entitiesToUpdateInDb = new LinkedList<TEntity>();
                foreach (var entity in entitiesToUpdate)
                {
                    // get the entity in db
                    var entityInDB = entitiesInDb[entity.Id].FirstOrDefault();
                    if (entityInDB is null)
                    {
                        // this will be weird the entity not exist in the database !!
                        // it may be deleted !!
                        continue;
                    }

                    // resolve the conflict between the entities, (using just the date comparison)
                    if (entityInDB.LastModifiedOn > entity.LastModifiedOn)
                    {
                        // the entity in the database is the most up to date
                        objectsToReturn.Add(entityInDB);
                        continue;
                    }

                    entitiesToUpdateInDb.AddLast(entity);

                    // resolve the conflict between the entities, (using a resolver)
                    // _synchronizationResolver.Resolve(entityInDB, entity);
                }

                var updateResult = await _dataAccess.UpdateRangeAsync(entitiesToUpdateInDb, false);
            }

            // get the newest object
            var syncObjectsIds = objectsToSync.Select(e => e.Object.Id<TKey>()).ToArray();
            objectsToReturn.AddRange(await GetNewestObjectsAsync(lastSyncDate, syncObjectsIds));

            // return the data
            return new
            {
                docType = typeof(TEntity).DocType(),
                objects = objectsToReturn,
            };
        }

        public virtual Task<TEntity[]> GetFirstsynchronizationDataAsync()
        {
            var addDefaultIncludes = typeof(TEntity).DocType() == DocumentType.Lot || typeof(TEntity).DocType() == DocumentType.Invoice;
            return _dataAccess.GetAllAsync(options => options.AddDefaultIncludes(addDefaultIncludes));
        }

        public virtual Task<TEntity[]> GetNewestObjectsAsync(DateTimeOffset? lastSyncDate, TKey[] syncObjectsIds)
        {
            return _dataAccess.GetAllAsync(options =>
            {
                options.AddDefaultIncludes(false);
                options.AddPredicate(e => e.LastModifiedOn > lastSyncDate && !syncObjectsIds.Contains(e.Id));
            });
        }
    }

    public abstract partial class SynchronizeDataService<TEntity, TKey> : DataService<TEntity, TKey>, ISynchronize
        where TEntity : class, IEntity<TKey>
    {
        protected readonly ISynchronizationResolverService _synchronizationResolver;

        protected SynchronizeDataService(
            IDataAccess<TEntity, TKey> dataAccess,
            ISynchronizationResolverService synchronizationResolver,
            IFileService fileService,
            IValidationService validation,
            IHistoryService historyService,
            ILoggedInUserService loggedInUserService,
            IApplicationConfigurationService appSetting,
            ITranslationService transalationService,
            ILoggerFactory loggerFactory,
            IMapper mapper)
            : base(dataAccess, fileService, validation, historyService, loggedInUserService, appSetting, transalationService, loggerFactory, mapper)
        {
            _synchronizationResolver = synchronizationResolver;
        }
    }
}
