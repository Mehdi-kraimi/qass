﻿namespace Axiobat.Services
{
    using AccountManagement;
    using Application.Services;
    using Application.Services.Accounting;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Application.Services.Contacts;
    using Application.Services.Documents;
    using Application.Services.Maintenance;
    using Application.Services.Products;
    using Application.Services.Mission;
    using Configuration;
    using Contacts;
    using Microsoft.Extensions.DependencyInjection;
    using Services.Accounting;
    using Services.Documents;
    using Services.Maintenance;
    using Services.Products;

    /// <summary>
    /// the Configurations class for registering the layer services
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public static class Configurations
    {
        /// <summary>
        /// add the services from the services layer
        /// </summary>
        /// <param name="builder">the extensions builder instant</param>
        /// <returns><see cref="ConfigurationsBuilder"/> instant</returns>
        public static ConfigurationsBuilder AddServices(this ConfigurationsBuilder builder)
        {
            RegiseterApplicationServices(builder.Services);
            return builder;
        }

        /// <summary>
        /// we use this method to register all Services
        /// </summary>
        /// <param name="services">the DI Service collection</param>
        private static void RegiseterApplicationServices(IServiceCollection services)
        {
            // subscription services
            services.AddScoped<IValidationService, ValidationService>();

            // register services
            services.AddScoped<IRoleManagementService, RoleManagementService>();
            services.AddScoped<IUsersManagementService, UsersManagementService>();

            // configuration
            services.AddScoped<IDateService, DateService>();
            services.AddScoped<IHistoryService, HistoryService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IClassificationService, ClassificationService>();
            services.AddScoped<IAccountingPeriodService, AccountingPeriodService>();
            services.AddScoped<IChartOfAccountsService, ChartOfAccountsService>();
            services.AddScoped<ISynchronizationResolverService, SynchronizationResolverService>();
            services.AddScoped<IApplicationConfigurationService, ApplicationConfigurationService>();

            services.AddScoped<IPublishingService, PublishingService>();
            services.AddScoped<IRecurringDocumentService, RecurringDocumentService>();

            // accounting
            services.AddScoped<IAccountingService, AccountingService>();
            services.AddScoped<IAnalyticsService, AnalyticsService>();

            // documents services
            services.AddScoped<IGroupService, GroupService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<ISupplierService, SupplierService>();

            // workshop management
            services.AddScoped<IConstructionWorkshopService, ConstructionWorkshopService>();
            services.AddScoped<IOperationSheetService, OperationSheetService>();

            // products services
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ILotService, LotService>();

            // legal document
            services.AddScoped<IQuoteService, QuoteService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<IExpenseService, ExpenseService>();
            services.AddScoped<ISupplierOrderService, SupplierOrderService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<ICreditNoteService, CreditNoteService>();
            services.AddScoped<IDashboardService, DashboardService>();

            // Maintenance
            services.AddScoped<IEquipmentMaintenanceService, EquipmentMaintenanceService>();
            services.AddScoped<IMaintenanceContractService, MaintenanceContractService>();
            services.AddScoped<IMaintenanceOperationSheetService, MaintenanceOperationSheetService>();
            services.AddScoped<IMaintenanceVisitService, MaintenanceVisitService>();

            // Mission
            services.AddScoped<IMissionService, MissionService>();
        }
    }
}
