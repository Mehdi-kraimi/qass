FROM mcr.microsoft.com/dotnet/core/sdk:2.1 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY api/*.sln .

# copy layers to new files 
COPY api/Axiobat.Domain/*.csproj ./Axiobat.Domain/
COPY api/Axiobat.Application/*.csproj ./Axiobat.Application/
COPY api/Axiobat.Presentation/*.csproj ./Axiobat.Presentation/
COPY api/Axiobat.Infrastructure/*.csproj ./Axiobat.Infrastructure/
COPY api/Axiobat.Persistence/*.csproj ./Axiobat.Persistence/
COPY api/Axiobat.Services/*.csproj ./Axiobat.Services/
COPY api/Axiobat.Test/*.csproj ./Axiobat.Test/

RUN dotnet restore  -s https://api.nuget.org/v3/index.json -s http://demo.artinove.net/packages/nuget

# copy everything else and build app
COPY api/Axiobat.Presentation/. ./Axiobat.Presentation/
COPY api/Axiobat.Infrastructure/. ./Axiobat.Infrastructure/
COPY api/Axiobat.Persistence/. ./Axiobat.Persistence/
COPY api/Axiobat.Services/. ./Axiobat.Services/
COPY api/Axiobat.Domain/. ./Axiobat.Domain/
COPY api/Axiobat.Application/. ./Axiobat.Application/
COPY api/Axiobat.Test/. ./Axiobat.Test/

WORKDIR /app
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:2.1 AS runtime
WORKDIR /app

COPY --from=build /app/Axiobat.Presentation/out ./


ENTRYPOINT ["dotnet", "Axiobat.Presentation.dll"]
