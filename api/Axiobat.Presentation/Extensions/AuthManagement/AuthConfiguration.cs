﻿namespace Axiobat.Presentation.AuthManagement
{
    using App.Common;
    using Application.Services.AccountManagement;
    using Application.Services.Configuration;
    using Domain.Entities;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using Microsoft.IdentityModel.Tokens;
    using Persistence.DataAccess;
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// this class is used o configure Authentication
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    internal static class AuthConfigurations
    {
        /// <summary>
        /// register and add the authentication services
        /// </summary>
        /// <param name="builder">the <see cref="ConfigurationsBuilder"/> instant</param>
        /// <returns>the <see cref="ConfigurationsBuilder"/> instant</returns>
        internal static ConfigurationsBuilder AddAuthConfig(this ConfigurationsBuilder builder, Action<AuthenticationOptions> options)
        {
            if (options is null)
                throw new ArgumentNullException(nameof(options));

            var authOptions = new AuthenticationOptions();
            options(authOptions);

            #region Add Identity

            builder.Services.AddIdentityCore<User>(config =>
            {
                config.Password = new Microsoft.AspNetCore.Identity.PasswordOptions
                {
                    RequireDigit = false,
                    RequireLowercase = false,
                    RequireNonAlphanumeric = false,
                    RequireUppercase = false,
                    RequiredLength = 3,
                };

                config.ClaimsIdentity = new ClaimsIdentityOptions
                {
                    RoleClaimType = AppClaimTypes.RoleName,
                    UserIdClaimType = AppClaimTypes.UserId,
                    UserNameClaimType = AppClaimTypes.UserName,
                    SecurityStampClaimType = AppClaimTypes.SecurityStamp,
                };

                config.Tokens.ProviderMap.Add(TokenOptions.DefaultProvider, new TokenProviderDescriptor(typeof(DataProtectorTokenProvider<User>)));
                config.Tokens.ProviderMap.Add(TokenOptions.DefaultEmailProvider, new TokenProviderDescriptor(typeof(EmailTokenProvider<User>)));
                config.Tokens.ProviderMap.Add(TokenOptions.DefaultPhoneProvider, new TokenProviderDescriptor(typeof(PhoneNumberTokenProvider<User>)));
                config.Tokens.ProviderMap.Add(TokenOptions.DefaultAuthenticatorProvider, new TokenProviderDescriptor(typeof(AuthenticatorTokenProvider<User>)));
                config.Tokens.ProviderMap.Add(CustomPasswordRestTokenProvider.DefaultName, new TokenProviderDescriptor(typeof(CustomPasswordRestTokenProvider)));

                config.Tokens.PasswordResetTokenProvider = CustomPasswordRestTokenProvider.DefaultName;
            });
            builder.Services.TryAddScoped(typeof(IUserStore<User>), typeof(UserDataAccess));
            builder.Services.TryAddScoped(typeof(IRoleStore<User>), typeof(RoleDataAccess));
            builder.Services.AddTransient<CustomPasswordRestTokenProvider>();
            builder.Services.AddTransient<DataProtectorTokenProvider<User>>();
            builder.Services.AddTransient<EmailTokenProvider<User>>();
            builder.Services.AddTransient<PhoneNumberTokenProvider<User>>();
            builder.Services.AddTransient<AuthenticatorTokenProvider<User>>();
            builder.Services.Configure<DataProtectionTokenProviderOptions>(DataProtectionOptions =>
            {
                DataProtectionOptions.TokenLifespan = TimeSpan.FromDays(2);
            });

            #endregion

            builder.Services.AddAuthentication(config =>
            {
                config.DefaultAuthenticateScheme = config.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(config =>
            {
                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,

                    ClockSkew = TimeSpan.Zero,
                    ValidIssuer = authOptions.Issuer,
                    ValidAudience = authOptions.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(authOptions.SignInKey)),
                };
            });

            builder.Services.AddAuthorization();
            builder.Services.AddScoped<IUserClaimsPrincipalFactory<User>, AppClaimsPrincipalFactory>();
            builder.Services.AddScoped<ILoggedInUserService, LoggedInUserService>();

            return builder;
        }

        /// <summary>
        /// sign the user in by generating a valid token for his session
        /// </summary>
        /// <param name="context">the HTTP context</param>
        /// <param name="user">the user claims to use for generating the token</param>
        /// <param name="authenticationMethod">the authentication Method</param>
        /// <param name="customTokenExpiration">a custom expiration token value, in minutes</param>
        /// <returns>the token</returns>
        internal static async Task<string> SignUserInAsync(this HttpContext context, User user, string authenticationMethod = null, int? customTokenExpiration = null)
        {
            var claimsFacory = context.RequestServices.GetService<IUserClaimsPrincipalFactory<User>>();
            var claimPrinciple = await claimsFacory.CreateAsync(user);

            return await SignUserInAsync(context, claimPrinciple, authenticationMethod, customTokenExpiration);
        }

        /// <summary>
        /// sign the user in by generating a valid token for his session
        /// </summary>
        /// <param name="context">the HTTP context</param>
        /// <param name="user">the user claims to use for generating the token</param>
        /// <param name="authenticationMethod">the authentication Method</param>
        /// <param name="customTokenExpiration">a custom expiration token value, in minutes</param>
        /// <returns>the token</returns>
        internal static async Task<string> SignUserInAsync(this HttpContext context, ClaimsPrincipal user, string authenticationMethod = null, int? customTokenExpiration = null)
        {
            var globalConfig = context.RequestServices.GetService<IApplicationSettingsAccessor>();
            var secrets = context.RequestServices.GetService<IApplicationSecretAccessor>();

            var now = DateTime.UtcNow;

            authenticationMethod = authenticationMethod ?? JwtBearerDefaults.AuthenticationScheme;
            var tokenExpiration = customTokenExpiration ?? globalConfig.GetTokenExpiration();

            var claims = new List<Claim>(user.Claims)
            {
                new Claim(ClaimTypes.AuthenticationMethod, authenticationMethod),
                new Claim(JwtRegisteredClaimNames.Sub, user.FindFirst(AppClaimTypes.UserName)?.Value ?? "demo"),
                new Claim(JwtRegisteredClaimNames.Jti, await AuthenticationOptions.NonceGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64),
            };

            var signInKey = await secrets.GetSignInKeyAsync();
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(signInKey));

            // Create the JWT and write it to a string
            var jwt = new JwtSecurityToken(
                issuer: globalConfig.GetTokenIssuer(),
                audience: globalConfig.GetTokenAudience(),
                claims: claims,
                notBefore: now,
                expires: now.AddMinutes(tokenExpiration),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            context.User = new ClaimsPrincipal(
                new ClaimsIdentity(claims, JwtBearerDefaults.AuthenticationScheme));

            return encodedJwt;
        }
    }
}
