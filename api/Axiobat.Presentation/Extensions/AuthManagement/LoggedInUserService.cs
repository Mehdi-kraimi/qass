﻿namespace Axiobat.Presentation.AuthManagement
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.AspNetCore.Http;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the service implementation of <see cref="ILoggedInUserService"/>
    /// </summary>
    public partial class LoggedInUserService : ILoggedInUserService
    {
        /// <summary>
        /// check if the current request is for an authenticated user
        /// </summary>
        public bool IsAuthenticated => _httpContext?.User?.Identity?.IsAuthenticated ?? false;

        /// <summary>
        /// check if the current logged in user is a super admin,
        /// basically we check if the role type is <see cref="RoleType.SuperAdminRole"/> and
        /// company type is <see cref="CompanyType.Foliatech"/>
        /// </summary>
        public bool IsSuperAdmin => User.RoleType == RoleType.SuperAdminRole;

        /// <summary>
        /// check if the current logged in user is an admin,
        /// basically we only check if the role type is <see cref="RoleType.AdminRole"/>
        /// </summary>
        public bool IsAdmin => User.RoleType == RoleType.SuperAdminRole;

        /// <summary>
        /// the current logged in user information
        /// </summary>
        public ILoggedInUser User
        {
            get
            {
                if (_loggedInUserInfo is null)
                    _loggedInUserInfo = GetLoggedInUserInfo();

                return _loggedInUserInfo;
            }
        }

        /// <summary>
        /// get the list of the user permissions
        /// </summary>
        /// <returns>the user permissions</returns>
        public IEnumerable<Permission> GetUserPermissions()
            => Permission.ToPermission(User?.Permissions);

        /// <summary>
        /// get the current user Role
        /// </summary>
        /// <returns>role of the user</returns>
        public async Task<Role> GetUserRoleAsync()
        {
            if (_userRole is null)
                _userRole = await _roleDataAccess.GetByIdAsync(User.RoleId);

            return _userRole;
        }

        /// <summary>
        /// get the userAgent information
        /// </summary>
        /// <returns>an instant of <see cref="UserAgentInfo"/>, or null if the request don't own any User-agent header</returns>
        public UserAgentInfo GetUserAgentInfo()
        {
            // check if there is aUser-Agent header
            if (!_httpContext.Request.Headers.ContainsKey("User-Agent"))
                return null;

            // get the user agent value and validate if there is any value
            var userAgent = _httpContext.Request.Headers["User-Agent"].ToString();
            if (!userAgent.IsValid())
                return null;

            // parse the user agent value
            var userAgentParser = UserAgentParser.GetDefault();
            return userAgentParser.Parse(userAgent);
        }

        /// <summary>
        /// get the IP address of the user
        /// </summary>
        /// <returns>the IP address of the user</returns>
        public string GetUserIPAddress()
            => _httpContext.Connection.RemoteIpAddress.ToString();

        /// <summary>
        /// get the <see cref="MinimalUser"/> instant
        /// </summary>
        /// <returns>the <see cref="MinimalUser"/> instant</returns>
        public MinimalUser GetMinimalUser() => new MinimalUser(User?.UserId ?? Guid.Empty, User?.UserName);

        /// <summary>
        /// check if the user has the given permissions
        /// </summary>
        /// <param name="permissions">the permissions to check if the user has</param>
        /// <returns>true if the user has the permissions, false if not</returns>
        public bool HasPermission(params Permissions[] permissions)
            => Permission.HasPermission(User.Permissions, permissions);

        /// <summary>
        /// get the user instant of the current logged in user
        /// </summary>
        /// <returns>the user instant</returns>
        public async Task<User> GetUserAsync()
        {
            if (_user is null)
                _user = await _userDataAccess.GetByIdAsync(User.UserId);

            return _user;
        }

        public Task<User> GetCurrentUserOrAdminAsync()
        {
            if (IsAuthenticated)
                return GetUserAsync();

            return GetUserAsync();
        }
    }

    /// <summary>
    /// partial part for <see cref="LoggedInUserService"/>
    /// </summary>
    public partial class LoggedInUserService
    {
        /// <summary>
        /// a class that holds information on the current logged in user
        /// </summary>
        class LoggedInUserInfo : ILoggedInUser
        {
            /// <summary>
            /// default constructor
            /// </summary>
            public LoggedInUserInfo()
            {
                Permissions = new HashSet<Permissions>();
            }

            /// <summary>
            /// the id of the current logged in user
            /// </summary>
            public Guid UserId { get; set; }

            /// <summary>
            /// the user name of the user
            /// </summary>
            public string UserName { get; set; }

            /// <summary>
            /// the email of the user
            /// </summary>
            public string userEmail { get; set; }

            /// <summary>
            /// the role name
            /// </summary>
            public string RoleName { get; set; }

            /// <summary>
            /// Role Id
            /// </summary>
            public Guid RoleId { get; set; }

            /// <summary>
            /// the type of the role this user has
            /// </summary>
            public RoleType RoleType { get; set; }

            /// <summary>
            /// the list of permissions this user owns
            /// </summary>
            public IEnumerable<Permissions> Permissions { get; set; }

            /// <summary>
            /// build the string representation of the object
            /// </summary>
            /// <returns>the string value of the object</returns>
            public override string ToString()
                => $"Id: {UserId}, Role: {RoleName}, with {Permissions.Count()} Permissions";
        }

        private User _user;
        private Role _userRole;
        private ILoggedInUser _loggedInUserInfo;
        private readonly HttpContext _httpContext;
        private readonly IUserDataAccess _userDataAccess;
        private readonly IRoleDataAccess _roleDataAccess;

        /// <summary>
        /// create an instant of <see cref="LoggedInUserService"/>
        /// </summary>
        /// <param name="companyDataAccess"></param>
        /// <param name="subscriptionDataAccess"></param>
        /// <param name="settingDataAccess"></param>
        /// <param name="userDataAccess"></param>
        /// <param name="roleDataAccess"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="httpContextAccessor">the IHttpContextAccessor instant</param>
        public LoggedInUserService(
            IUserDataAccess userDataAccess,
            IRoleDataAccess roleDataAccess,
            IHttpContextAccessor httpContextAccessor)
        {
            _httpContext = httpContextAccessor.HttpContext;
            _userDataAccess = userDataAccess;
            _roleDataAccess = roleDataAccess;

            if (!(_httpContext is null))
                _loggedInUserInfo = GetLoggedInUserInfo();
        }

        /// <summary>
        /// extract the current logged in user info
        /// </summary>
        /// <returns>a <see cref="LoggedInUserInfo"/> instant</returns>
        private ILoggedInUser GetLoggedInUserInfo()
        {
            if (_httpContext is null)
                return null;

            if (!IsAuthenticated)
                return null;

            var user = new LoggedInUserInfo()
            {
                RoleId = Role.DemoRole,
                UserId = Domain.Entities.User.DemoUserId,
                userEmail = _httpContext.User.FindFirst(AppClaimTypes.UserEmail)?.Value,
                UserName = _httpContext.User.FindFirst(AppClaimTypes.UserName)?.Value,
                RoleName = _httpContext.User.FindFirst(AppClaimTypes.RoleName)?.Value,
                Permissions = Permission.Unpack(_httpContext.User.FindFirst(AppClaimTypes.UserPermissions)?.Value),
            };

            if (Enum.TryParse(_httpContext.User.FindFirst(AppClaimTypes.RoleType)?.Value, true, out RoleType roleType))
                user.RoleType = roleType;

            if (Guid.TryParse(_httpContext.User.FindFirst(AppClaimTypes.UserId)?.Value, out Guid userId))
                user.UserId = userId;

            if (Guid.TryParse(_httpContext.User.FindFirst(AppClaimTypes.RoleId)?.Value, out Guid roleId))
                user.RoleId = roleId;

            return user;
        }
    }
}
