﻿namespace Axiobat.Presentation.AuthManagement
{
    using Domain.Entities;
    using Microsoft.AspNetCore.DataProtection;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Options;
    using System;

    /// <summary>
    /// a custom token provider for password rest
    /// </summary>
    public class CustomPasswordRestTokenProvider : DataProtectorTokenProvider<User>
    {
        /// <summary>
        /// default name of the provider
        /// </summary>
        public const string DefaultName = "PasswordDataProtectorTokenProvider";

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="dataProtectionProvider"></param>
        /// <param name="options"></param>
        public CustomPasswordRestTokenProvider(IDataProtectionProvider dataProtectionProvider,
            IOptions<PasswodRestTokenProviderOptions> options)
            : base(dataProtectionProvider, options)
        {

        }
    }

    /// <summary>
    /// the options for toke provider for password rest
    /// </summary>
    public class PasswodRestTokenProviderOptions : DataProtectionTokenProviderOptions
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public PasswodRestTokenProviderOptions()
        {
            Name = CustomPasswordRestTokenProvider.DefaultName;
            TokenLifespan = TimeSpan.FromMinutes(60);
        }
    }
}
