﻿namespace Axiobat.Presentation
{
    using App.Common;
    using Domain.Entities;
    using Domain.Enums;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using System;

    /// <summary>
    /// this attribute is for Permissions Authorization
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    sealed class HasPermissionAttribute : Attribute, IActionFilter, IFilterMetadata
    {
        private readonly Permissions[] _permissions;

        /// <summary>
        /// construct a new instant of <see cref="HasPermissionAttribute"/>
        /// </summary>
        public HasPermissionAttribute(params Permissions[] permissions)
        {
            if (permissions.Length <= 0)
                throw new ArgumentException("you must supply the permissions or remove the attribute");

            _permissions = permissions;
        }

        /// <summary>
        ///  Called before the action executes, after model binding is complete.
        /// </summary>
        /// <param name="context">the action context</param>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var permissions = context.HttpContext.User.FindFirst(AppClaimTypes.UserPermissions);
            if (!Permission.HasPermission(permissions?.Value, _permissions))
            {
                context.Result = new ForbidResult();
                return;
            }
        }

        /// <summary>
        /// Called after the action executes, before the action result.
        /// </summary>
        /// <param name="context">the action context</param>
        public void OnActionExecuted(ActionExecutedContext context) { }
    }
}
