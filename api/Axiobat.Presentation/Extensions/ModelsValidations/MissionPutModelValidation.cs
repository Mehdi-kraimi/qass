﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common;
    using Application.Models;
    using Axiobat.Application.Services;
    using Axiobat.Application.Services.AccountManagement;
    using Axiobat.Application.Services.Contacts;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    public class MissionPutModelValidation : BaseValidator<MissionPutModel>
    {
        public MissionPutModelValidation(
            IClientService clientService,
            IUsersManagementService usersManagementService,
            IConstructionWorkshopService workshopService,
            ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            RuleSet("validateStartDate", () =>
            {
                When(e => e.EndingDate.HasValue, () =>
                {
                    RuleFor(e => e.EndingDate)
                        .Must((model, endingDate) => endingDate > model.StartingDate)
                            .WithMessage("the value of the ending date must be greater than the starting date");
                });
            });

            RuleSet("default", () =>
            {
                RuleFor(e => e.MissionKind)
                    .NotEmpty()
                        .WithMessage("the Mission Kind is required")
                    .NotNull()
                        .WithMessage("the Mission Kind is required")
                    .Must(kind => MissionKind.IsValid(kind))
                        .WithMessage($"the given Mission kind is not supported, you must provide one of {MissionKind.ALL}");

                RuleFor(e => e.Title)
                    .NotEmpty()
                        .WithMessage("the title is required")
                    .NotNull()
                        .WithMessage("the title is required");

                When(e => e.WorkshopId.IsValid(), () =>
                {
                    RuleFor(e => e.WorkshopId)
                        .MustAsync(async (id, token) => await workshopService.IsExistAsync(id))
                        .WithMessage("the given workshop id not exist");
                });

                RuleFor(e => e.TechnicianId)
                    .MustAsync(async (technicianId, cancelToken) => await usersManagementService.IsExistAsync(technicianId))
                        .WithMessage("the given Technician not exist")
                        .WithErrorCode(MessageCode.TechnicianNotExist);
            });
        }
    }
}
