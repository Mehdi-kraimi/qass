﻿namespace Axiobat.Presentation.Models.Validations
{
    using Axiobat.Application.Models;
    using Axiobat.Domain.Constants;
    using Axiobat.Domain.Entities;
    using Axiobat.Domain.Enums;
    using FluentValidation;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Linq;

    public class RecurringDocumentPutModelValidation : BaseValidator<RecurringDocumentPutModel>
    {
        public RecurringDocumentPutModelValidation(ILoggerFactory loggerFactory) : base(loggerFactory)
        {
            RuleSet("validateStartDate", () =>
            {
                RuleFor(e => e.StartingDate)
                    .Must(e => e >= DateTime.Now)
                        .WithMessage("the starting date must be value in the future");
            });

            RuleSet("default", () =>
            {
                When(e => e.PeriodicityType == PeriodicityType.Custom, () =>
                {
                    RuleFor(e => e.PeriodicityOptions)
                        .NotNull()
                            .WithMessage("you must supply a valid periodicity option when selecting the custom option for PeriodicityType");

                    When(e => !(e.PeriodicityOptions is null), () =>
                    {
                        RuleFor(e => e.PeriodicityOptions)
                            .SetValidator(new PeriodicityOptionsValidation(loggerFactory));
                    });
                });

                When(e => e.EndingType == PeriodicityEndingType.SpecificDate, () =>
                {
                    RuleFor(e => e.EndingDate)
                        .NotNull()
                            .WithMessage("you must supply a value for the ending date");

                    When(e => e.EndingDate.HasValue, () =>
                    {
                        RuleFor(e => e.EndingDate)
                            .Must((recurring, endingDate) => endingDate.Value > recurring.StartingDate)
                                .WithMessage("the value of the ending date must be greater than the starting date");
                    });
                });

                When(e => e.EndingType == PeriodicityEndingType.SpecificNumberOfTime, () =>
                {
                    RuleFor(e => e.OccurringCount)
                        .Must(e => e >= 1)
                            .WithMessage("the value must be greater than One");

                    When(e => e.EndingDate.HasValue, () =>
                    {
                        RuleFor(e => e.EndingDate)
                            .Must((recurring, endingDate) => endingDate.Value > recurring.StartingDate)
                                .WithMessage("the value of the ending date must be greater than the starting date");
                    });
                });

                RuleFor(e => e.Document)
                    .NotNull()
                        .WithMessage("you must supply a document");

                RuleFor(e => e.Status)
                    .Must(e => DocumentStatus.ValidStatus(e, DocumentType.RecurringDocument));

                RuleFor(e => e.DocumentType)
                    .Must(e => e == DocumentType.Expenses || e == DocumentType.Invoice)
                        .WithMessage("the only supported documents are the expenses and invoices");
            });
        }
    }

    public class PeriodicityOptionsValidation : BaseValidator<PeriodicityOptions>
    {
        public PeriodicityOptionsValidation(ILoggerFactory loggerFactory) : base(loggerFactory)
        {
            When(e => e.RecurringType == PeriodicityRecurringType.EveryMonth, () =>
            {
                RuleFor(e => e.DayOfMonth)
                    .Must(e => e >= 1 && e <= 31)
                        .WithMessage("Day Of Month value must be between 1 and 31");
            });

            When(e => e.RecurringType == PeriodicityRecurringType.EveryWeek, () =>
            {
                RuleFor(e => e.DaysOfWeek)
                    .Must(e => e.Any())
                        .WithMessage("you must sully one or more Days Of Week");
            });

            RuleFor(e => e.RepeatEvery)
                .Must(e => e >= 1)
                    .WithMessage("the value must be greater than One");
        }
    }
}
