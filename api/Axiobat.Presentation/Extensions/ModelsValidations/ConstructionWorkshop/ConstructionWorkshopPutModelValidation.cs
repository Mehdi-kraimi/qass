﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// module validation for <see cref="ConstructionWorkshopPutModel"/>
    /// </summary>
    public class ConstructionWorkshopPutModelValidation : BaseValidator<ConstructionWorkshopPutModel>
    {
        /// <summary>
        /// create an instant of <see cref="ConstructionWorkshopPutModelValidation"/>
        /// </summary>
        /// <param name="loggerFactory">the logger factory instant</param>
        public ConstructionWorkshopPutModelValidation(
            ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            RuleFor(e => e.Name)
                .NotEmpty()
                    .WithErrorCode(MessageCode.NameIsRequired)
                    .WithMessage("the name is required")
                .NotNull()
                    .WithErrorCode(MessageCode.NameIsRequired)
                    .WithMessage("the name is required");

            RuleFor(e => e.ClientId)
                .NotEmpty()
                    .WithErrorCode(MessageCode.RequiredValue)
                    .WithMessage("the client id is required")
                .NotNull()
                    .WithErrorCode(MessageCode.RequiredValue)
                    .WithMessage("the client id is required");

        }
    }
}
