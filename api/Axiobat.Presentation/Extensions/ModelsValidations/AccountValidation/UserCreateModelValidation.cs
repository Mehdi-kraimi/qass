﻿namespace Axiobat.Presentation.Models.Validations
{
    using App.Common;
    using Application.Models;
    using Application.Services.AccountManagement;
    using FluentValidation;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// the account models validation
    /// </summary>
    public class UserCreateModelValidation : BaseValidator<UserCreateModel>
    {
        private readonly IUsersManagementService _userService;
        private readonly IRoleManagementService _roleService;

        /// <summary>
        /// default constructor
        /// </summary>
        /// <param name="userService">account service</param>
        /// <param name="roleService"></param>
        /// <param name="companyService">company Service</param>
        /// <param name="logger"></param>
        public UserCreateModelValidation(
            IUsersManagementService userService,
            IRoleManagementService roleService,
            ILoggerFactory logger)
            : base(logger)
        {
            _userService = userService;
            _roleService = roleService;

            RuleFor(e => e.RoleId)
                .MustAsync(RoleExistAsync)
                    .WithMessage("the specified role does not exist!")
                    .WithErrorCode(MessageCode.RoleNotExist);

            RuleFor(e => e.UserName)
                .NotNull()
                    .WithMessage("user name is required")
                    .WithErrorCode(MessageCode.RequiredValue)
                .NotEmpty()
                    .WithMessage("user name is required")
                    .WithErrorCode(MessageCode.RequiredValue)
                .MustAsync(UniqueUserNameAsync)
                    .WithMessage("the UserName is already taken")
                    .WithErrorCode(MessageCode.DuplicateEntry);

            When(e => e.Email.IsValid(), () =>
            {
                RuleFor(e => e.Email)
                    .IsValidEmail()
                    .MustAsync(UniqueEmailAsync)
                        .WithMessage("the email is already taken")
                        .WithErrorCode(MessageCode.DuplicateEntry);
            });
        }

        private async Task<bool> UniqueUserNameAsync(string userEmail, CancellationToken cancellationToken)
            => !await _userService.IsUserNameExistAsync(userEmail);

        private Task<bool> RoleExistAsync(Guid roleId, CancellationToken cancellationToken)
            => _roleService.IsRoleExistAsync(roleId);

        private async Task<bool> UniqueEmailAsync(string userEmail, CancellationToken cancellationToken)
            => !await _userService.IsUserEmailExistAsync(userEmail);
    }
}

