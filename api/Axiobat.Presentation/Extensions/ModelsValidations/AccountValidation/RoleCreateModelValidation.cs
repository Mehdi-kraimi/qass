﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Role Validation Model
    /// </summary>
    public class RoleUpdateModelValidation : BaseValidator<RoleUpdateModel>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        /// <param name="accountService">the account service</param>
        /// <param name="logger"></param>
        public RoleUpdateModelValidation(ILoggerFactory logger) : base(logger)
        {
            RuleFor(e => e.Name)
                   .NotNull()
                        .WithErrorCode(MessageCode.RoleNameIsRequired)
                        .WithMessage("Role name is required")
                   .NotEmpty()
                        .WithErrorCode(MessageCode.RoleNameIsRequired)
                        .WithMessage("Role name is required")
                   .MaximumLength(250)
                        .WithErrorCode(MessageCode.PropertyValueTooLong)
                        .WithMessage("only 250 characters are allowed");
        }
    }
}
