﻿namespace Axiobat.Presentation.Models.Validations
{
    using Application.Models;
    using FluentValidation;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// the filter options Validation
    /// </summary>
    public class FilterOptionsModelValidation : BaseValidator<FilterOptions>
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public FilterOptionsModelValidation(ILoggerFactory logger)
            : base(logger)
        {
            RuleFor(e => e.Page)
                .GreaterThan(0)
                    .WithErrorCode(MessageCode.PageIndexMustBeGreaterThanZero)
                    .WithMessage("the page index value must be Greater than 0");

            RuleFor(e => e.PageSize)
                .GreaterThan(0)
                    .WithErrorCode(MessageCode.PageSizeMustBeGreaterThanZero)
                    .WithMessage("the page size value must be Greater than 0");
        }
    }
}
