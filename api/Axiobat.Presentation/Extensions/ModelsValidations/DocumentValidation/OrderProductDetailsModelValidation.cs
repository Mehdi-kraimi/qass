﻿namespace Axiobat.Presentation.Models.Validations
{
    using Domain.Entities;
    using Microsoft.Extensions.Logging;

    public class OrderProductDetailsModelValidation : BaseValidator<OrderProductDetails>
    {
        public OrderProductDetailsModelValidation(ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            When(e => e.Type == Domain.Enums.ProductType.Product, () =>
            {
                RuleFor(e => e.Product);
            });

        }
    }
}
