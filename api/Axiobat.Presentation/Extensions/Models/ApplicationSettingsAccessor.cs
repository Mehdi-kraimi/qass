﻿namespace Axiobat.Presentation.Models
{
    using Application.Services.Configuration;
    using Axiobat.Application.Exceptions;
    using Microsoft.Extensions.Options;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the application configuration accessor
    /// </summary>
    public partial class ApplicationSettingsAccessor
    {
        /// <summary>
        /// get the if of the project
        /// </summary>
        public string ProjectId => _applicationSettings.ProjectId;

        /// <summary>
        /// check if background services are allowed to run
        /// </summary>
        /// <returns>true if allowed false if not</returns>
        public bool BackgroundServicesAllowed() => _applicationSettings.AllowBackgroundServices;

        /// <summary>
        /// get the file output path
        /// </summary>
        /// <returns></returns>
        public string GetFileOutputPath() => _applicationSettings.FileOutputPath;

        /// <summary>
        /// retrieve token audience value
        /// </summary>
        /// <returns>token audience value</returns>
        public string GetTokenAudience() => _applicationSettings.AuthenticationOptions.Audience;

        /// <summary>
        /// get token expiration value
        /// </summary>
        /// <returns>token expiration value</returns>
        public int GetTokenExpiration() => _applicationSettings.AuthenticationOptions.AuthenticationTokenExpiration;

        /// <summary>
        /// get token issuer value
        /// </summary>
        /// <returns>token issuer value</returns>
        public string GetTokenIssuer() => _applicationSettings.AuthenticationOptions.Issuer;

        /// <summary>
        /// get options related to one signal
        /// </summary>
        /// <returns>the OneSignal options</returns>
        public OneSignalOptions GetOneSignalOptions() => _applicationSettings.OneSignal;

        /// <summary>
        /// get the URL value
        /// </summary>
        /// <param name="urlName">the name of the URL to retrieve, one of <see cref="ApplicationsURLs"/></param>
        /// <returns></returns>
        public string GetUrl(string urlName)
        {
            if (!_applicationSettings.ApplicationsUrls.TryGetValue(urlName, out string url))
                throw new NotFoundException($"URL '{urlName}' not exist");

            return url;
        }

        public string GetPublishContractFilesOutput() => _applicationSettings.Publishing.Files;
    }

    /// <summary>
    /// partial part for <see cref="ApplicationSettingsAccessor"/>
    /// </summary>
    public partial class ApplicationSettingsAccessor : IApplicationSettingsAccessor
    {
        private readonly IDisposable _secretsChangedDisposable;
        private ApplicationSettings _applicationSettings;

        /// <summary>
        /// create an instant of <see cref="ApplicationSettingsAccessor"/>
        /// </summary>
        /// <param name="options"></param>
        public ApplicationSettingsAccessor(IOptionsMonitor<ApplicationSettings> options)
        {
            _applicationSettings = options.CurrentValue;
            _secretsChangedDisposable = options.OnChange(e => _applicationSettings = e);
        }

        /// <summary>
        /// the object destructor
        /// </summary>
        ~ApplicationSettingsAccessor()
        {
            _secretsChangedDisposable?.Dispose();
        }
    }
}
