﻿namespace Axiobat.Presentation.Models
{
    using Application.Services.Localization;

    /// <summary>
    /// the translation service implementation of <see cref="ITranslationService"/>
    /// </summary>
    public partial class TranslationService : ITranslationService
    {
        /// <summary>
        /// retrieve the translation version of the text if not found the text it self will be returned
        /// </summary>
        /// <param name="text">the text to translate</param>
        /// <param name="args">the list of arguments</param>
        /// <returns>the translation version of the text</returns>
        public string Get(string text, params object[] args)
        {
            return string.Format(text, args);
        }
    }

    /// <summary>
    /// partial part for <see cref="TranslationService"/>
    /// </summary>
    public partial class TranslationService
    {
        /// <summary>
        /// create an instant of <see cref="TranslationService"/>
        /// </summary>
        public TranslationService()
        {

        }
    }
}
