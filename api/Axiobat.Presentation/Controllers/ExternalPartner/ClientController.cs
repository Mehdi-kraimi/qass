﻿namespace Axiobat.Presentation.Controllers.ExternalPartner
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Contacts;
    using Application.Services.Localization;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the client management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class ClientController : BaseController<Client>
    {
        /// <summary>
        /// get list of all clients
        /// </summary>
        /// <returns>list of all clients</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<ClientModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<ClientModel>());

        /// <summary>
        /// get a paged result of the clients list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of clients as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<ClientModel>>> Get([FromBody] FilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<ClientModel, FilterOptions>(filter));

        /// <summary>
        /// retrieve Client with the given id
        /// </summary>
        /// <param name="clientId">the id of the Client to be retrieved</param>
        /// <returns>the Client</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ClientModel>>> Get([FromRoute(Name = "id")] string clientId)
            => ActionResultForAsync(_service.GetByIdAsync<ClientModel>(clientId));

        /// <summary>
        /// create a new Client record
        /// </summary>
        /// <param name="ClientModel">the model to create the Client from it</param>
        /// <returns>the newly created Client</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ClientModel>>> Create(
            [FromBody] ClientPutModel ClientModel)
        {
            var result = await _service.CreateAsync<ClientModel, ClientPutModel>(ClientModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Client informations
        /// </summary>
        /// <param name="ClientModel">the model to use for updating the client</param>
        /// <param name="clientId">the id of the Client to be updated</param>
        /// <returns>the updated Client</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ClientModel>>> Update([FromRoute(Name = "id")] string clientId, [FromBody] ClientPutModel ClientModel) =>
            ActionResultForAsync(_service.UpdateAsync<ClientModel, ClientPutModel>(clientId, ClientModel));

        /// <summary>
        /// delete the Client with the given id
        /// </summary>
        /// <param name="clientId">the id of the client to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string clientId)
            => ActionResultForAsync(_service.DeleteAsync(clientId));

        /// <summary>
        /// get the client documents details
        /// </summary>
        /// <param name="clientId">the id of the client to get the document details for it</param>
        /// <returns>the details</returns>
        [HttpGet("{id}/documentDetails")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ClientDocumentDetails>>> GetClientDocumentsDetails(
            [FromRoute(Name = "id")] string clientId)
        {
            var result = await _service.GetClientDocumentsDetailsAsync(clientId);
            return Result.Success(result);
        }

        /// <summary>
        /// get the client documents details
        /// </summary>
        /// <param name="clientId">the id of the client to get the document details for it</param>
        /// <returns>the details</returns>
        [HttpGet("{id}/TurnOver/{year:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ClientTurnoverDetail>>> GetClientTurnOverDetails(
            [FromRoute(Name = "id")] string clientId,
            [FromRoute(Name = "year")] int year)
        {
            var result = await _service.GetClientTurnOverDetailsAsync(clientId, year);
            return Result.Success(result);
        }

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        /// <summary>
        /// check if the given client code is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="code">the code to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Code/{code}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckCodeIfUnique([FromRoute(Name = "code")] string code)
            => await _service.IsCodeUniqueAsync(code);
    }

    /// <summary>
    /// partial part for <see cref="SupplierController"/>
    /// </summary>
    public partial class ClientController
    {
        private readonly IClientService _service;

        /// <summary>
        /// create an instant of <see cref="SupplierController"/>
        /// </summary>
        /// <param name="service">the <see cref="IClientService"/> instant</param>
        /// <param name="loggedInUserService">the <see cref="ILoggedInUserService"/> instant</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> instant</param>
        /// <param name="loggerFactory">the <see cref="ILoggerFactory"/> instant</param>
        public ClientController(IClientService service, ILoggedInUserService loggedInUserService, ITranslationService translationService, ILoggerFactory loggerFactory) : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = service;
        }
    }
}