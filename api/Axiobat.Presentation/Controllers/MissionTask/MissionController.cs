﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Mission;
    using Domain.Entities;
    using FluentValidation.AspNetCore;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the Mission management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class MissionController : BaseController<Mission>
    {
        /// <summary>
        /// get list of all Missions
        /// </summary>
        /// <returns>list of all Missions</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<MissionModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<MissionModel>());

        /// <summary>
        /// get a paged result of the Missions list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Missions as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<MissionModel>>> Get([FromBody] MissionFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<MissionModel, MissionFilterOptions>(filter));

        /// <summary>
        /// retrieve Mission with the given id
        /// </summary>
        /// <param name="MissionId">the id of the Mission to be retrieved</param>
        /// <returns>the Mission</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MissionModel>>> Get([FromRoute(Name = "id")] string MissionId)
            => ActionResultForAsync(_service.GetByIdAsync<MissionModel>(MissionId));

        /// <summary>
        /// create a new Mission record
        /// </summary>
        /// <param name="MissionModel">the model to create the Mission from it</param>
        /// <returns>the newly created Mission</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<MissionModel>>> Create(
            [FromBody, CustomizeValidator(RuleSet = "default,validateStartDate")] MissionPutModel MissionModel)
        {
            var result = await _service.CreateAsync<MissionModel, MissionPutModel>(MissionModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Mission informations
        /// </summary>
        /// <param name="MissionModel">the model to use for updating the Mission</param>
        /// <param name="MissionId">the id of the Mission to be updated</param>
        /// <returns>the updated Mission</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MissionModel>>> Update(
            [FromRoute(Name = "id")] string MissionId,
            [FromBody, CustomizeValidator(RuleSet = "default")] MissionPutModel MissionModel) =>
            ActionResultForAsync(_service.UpdateAsync<MissionModel, MissionPutModel>(MissionId, MissionModel));

        /// <summary>
        /// delete the Mission with the given id
        /// </summary>
        /// <param name="MissionId">the id of the Mission to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string MissionId)
            => ActionResultForAsync(_service.DeleteAsync(MissionId));

        /// <summary>
        /// get the list of types
        /// </summary>
        /// <returns>the operation result</returns>
        [HttpPost("Type")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<MissionTypeModel>>> GetMissionTypes([FromBody] MissionTypeFilterOptions filter)
            => ActionResultForAsync(_service.GetMissionTypesAsync(filter));

        /// <summary>
        /// create or update a mission type, based on the given model
        /// </summary>
        /// <param name="model">the model to be used when creating or updating a mission type</param>
        /// <returns>the updated/created mission type</returns>
        [HttpPut("Type")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MissionTypeModel>>> PutMissionType(
            [FromBody] MissionTypeModel model)
            => ActionResultForAsync(_service.PutMissionTypeAsync(model));

        /// <summary>
        /// delete the Mission Type with the given id
        /// </summary>
        /// <param name="MissionTypeId">the id of the Mission Type to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("Type/{id:int}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> DeleteMissionType([FromRoute(Name = "id")] int MissionTypeId)
        {
            await _service.DeleteMissionTypeAsync(MissionTypeId);
            return Ok(Result.Success());
        }
    }

    /// <summary>
    /// partial part for <see cref="SupplierController"/>
    /// </summary>
    public partial class MissionController
    {
        private readonly IMissionService _service;

        /// <summary>
        /// create an instant of <see cref="SupplierController"/>
        /// </summary>
        /// <param name="service">the <see cref="IMissionService"/> instant</param>
        /// <param name="loggedInUserService">the <see cref="ILoggedInUserService"/> instant</param>
        /// <param name="translationService">the <see cref="ITranslationService"/> instant</param>
        /// <param name="loggerFactory">the <see cref="ILoggerFactory"/> instant</param>
        public MissionController(IMissionService service, ILoggedInUserService loggedInUserService, ITranslationService translationService, ILoggerFactory loggerFactory) : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = service;
        }
    }
}
