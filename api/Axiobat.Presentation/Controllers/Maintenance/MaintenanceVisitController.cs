﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Maintenance;
    using Application.Models;
    using Domain.Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// controller for managing <see cref="MaintenanceContract"/>
    /// </summary>
    [Route("api/[controller]")]
    public partial class MaintenanceVisitController : BaseController<MaintenanceVisit>
    {
        /// <summary>
        /// get Maintenance Visit with the given id
        /// </summary>
        /// <param name="maintenanceVisitId">the id of the MaintenanceVisit to be retrieved</param>
        /// <returns>Maintenance Visit with the given id</returns>
        [HttpGet("{maintenanceVisitId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceVisitModel>>> GetById([FromRoute(Name = "maintenanceVisitId")] string maintenanceVisitId)
            => ActionResultForAsync(_service.GetByIdAsync<MaintenanceVisitModel>(maintenanceVisitId));

        /// <summary>
        /// get list of all maintenanceVisit associated with this maintenanceVisit
        /// </summary>
        /// <param name="contractId">the id of the contract</param>
        /// <returns>maintenanceVisitId</returns>
        [HttpGet("contract/{contractId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<MaintenanceVisitModel>>> GetContractMaintenanceVisit(
            [FromRoute(Name = "contractId")] string contractId)
            => ActionResultForAsync(_service.GetAllContractMaintenanceVisitAsync(contractId));

        /// <summary>
        /// get list of all maintenanceVisit associated with this maintenanceVisit
        /// </summary>
        /// <param name="contractId">the id of the contract</param>
        /// <returns>maintenanceVisitId</returns>
        [HttpGet("contract/{contractId}/{month}/{year}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceVisitModel>>> GetContractMaintenanceVisitByMonthAndYear(
            [FromRoute(Name = "month")] int month,
            [FromRoute(Name = "year")] int year,
            [FromRoute(Name = "contractId")] string contractId)
            => ActionResultForAsync(_service.GetMaintenanceVisitAsync(new MaintenanceVisitRetrieveOptions
            {
                ContractId = contractId,
                Month = month,
                Year = year,
            }));

        /// <summary>
        /// get a paged result of the MaintenanceContracts list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of MaintenanceContracts as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<MaintenanceVisitListModel>>> Get([FromBody] MaintenanceVisitFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<MaintenanceVisitListModel, MaintenanceVisitFilterOptions>(filter));
    }

    /// <summary>
    /// the partial part for <see cref="MaintenanceVisitController"/>
    /// </summary>
    public partial class MaintenanceVisitController : BaseController<MaintenanceVisit>
    {
        private readonly IMaintenanceVisitService _service;

        public MaintenanceVisitController(
            IMaintenanceVisitService maintenanceVisitService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = maintenanceVisitService;
        }
    }
}