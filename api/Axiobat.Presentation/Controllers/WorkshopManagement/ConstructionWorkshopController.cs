﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services;
    using Application.Services.Accounting;
    using Application.Services.AccountManagement;
    using Application.Services.Documents;
    using Application.Services.Localization;
    using Domain.Constants;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the Construction Workshop
    /// </summary>
    [Route("api/Workshop")]
    public partial class ConstructionWorkshopController
    {
        /// <summary>
        /// get list of all Constructions Workshops
        /// </summary>
        /// <returns>list of all Constructions Workshops</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<ConstructionWorkshopModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<ConstructionWorkshopModel>());

        /// <summary>
        /// get a paged result of the ConstructionWorkshops list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of ConstructionWorkshops as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<ConstructionWorkshopModel>>> Get([FromBody] WorkshopFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<ConstructionWorkshopModel, WorkshopFilterOptions>(filter));

        /// <summary>
        /// create a new ConstructionWorkshop record
        /// </summary>
        /// <param name="ConstructionWorkshopModel">the model to create the ConstructionWorkshop from it</param>
        /// <returns>the newly created ConstructionWorkshop</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ConstructionWorkshopModel>>> Create(
            [FromBody] ConstructionWorkshopPutModel ConstructionWorkshopModel)
        {
            var result = await _service.CreateAsync<ConstructionWorkshopModel, ConstructionWorkshopPutModel>(ConstructionWorkshopModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// retrieve ConstructionWorkshop with the given id
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the ConstructionWorkshop to be retrieved</param>
        /// <returns>the ConstructionWorkshop</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<ConstructionWorkshopModel>>> Get(
            [FromRoute(Name = "id")] string ConstructionWorkshopId)
        {
            return ActionResultFor(await _service.GetByIdAsync<ConstructionWorkshopModel>(ConstructionWorkshopId));
        }

        /// <summary>
        /// get the workshop suppliers orders that belongs to the given supplier
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to get the document details for it</param>
        /// <returns>the Construction Workshop document details</returns>
        [HttpGet("{id}/supplier/{supplierId}/orders")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<SupplierOrderModel>>> GetWorkshopSupplierOrders(
            [FromServices] ISupplierOrderService supplierOrderService,
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromRoute(Name = "supplierId")] string supplierId,
            [FromQuery(Name = "status")] string status = SupplierOrderStatus.InProgress)
            => ActionResultForAsync(supplierOrderService.GetWorkshopSupplierOrdersBySupplierIdAsync(ConstructionWorkshopId, supplierId, status));

        /// <summary>
        /// update the ConstructionWorkshop informations
        /// </summary>
        /// <param name="ConstructionWorkshopModel">the model to use for updating the ConstructionWorkshop</param>
        /// <param name="ConstructionWorkshopId">the id of the ConstructionWorkshop to be updated</param>
        /// <returns>the updated ConstructionWorkshop</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ConstructionWorkshopModel>>> Update(
            [FromBody] ConstructionWorkshopPutModel ConstructionWorkshopModel,
            [FromRoute(Name = "id")] string ConstructionWorkshopId)
            => ActionResultForAsync(_service.UpdateAsync<ConstructionWorkshopModel, ConstructionWorkshopPutModel>(ConstructionWorkshopId, ConstructionWorkshopModel));

        /// <summary>
        /// update the status of a construction workshop
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the ConstructionWorkshop to be updated</param>
        /// <param name="model">the model used to update the status</param>
        /// <returns>the operation result</returns>
        [HttpPut("{id}/Update/Status")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ConstructionWorkshopModel>>> UpdateWorkshopStatus(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromBody] WorkshopUpdateStatusModel model)
            => ActionResultForAsync(_service.UpdateStatusAsync(ConstructionWorkshopId, model));

        /// <summary>
        /// update Progress Rate of a construction workshop
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the ConstructionWorkshop to be updated</param>
        /// <param name="model">the model used to update the Progress Rate</param>
        /// <returns>the operation result</returns>
        [HttpPut("{id}/Update/ProgressRate")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ConstructionWorkshopModel>>> UpdateWorkshopProgressRate(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromBody] WorkshopUpdateProgressRateModel model)
            => ActionResultForAsync(_service.UpdateProgressRateAsync(ConstructionWorkshopId, model));

        /// <summary>
        /// delete the ConstructionWorkshop with the given id
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the ConstructionWorkshop to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string ConstructionWorkshopId)
            => ActionResultForAsync(_service.DeleteAsync(ConstructionWorkshopId));

        /// <summary>
        /// check if the given name is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="workshopName">the name of the workshop to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Name/{name}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "name")] string workshopName)
            => await _service.IsNameUniqueAsync(workshopName);

        /// <summary>
        /// retrieve the Financial Summary of the workshop with the given id
        /// </summary>
        /// <param name="accountingService">the accounting Service</param>
        /// <param name="ConstructionWorkshopId">the Construction Workshop Id</param>
        /// <returns>the Financial Summary instant</returns>
        [HttpGet("{Id}/FinancialSummary")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopFinancialSummary>>> GetFinancialSummary(
            [FromServices] IAccountingService accountingService,
            [FromRoute(Name = "Id")] string ConstructionWorkshopId)
            => ActionResultForAsync(accountingService.GetFinancialSummaryAsync(ConstructionWorkshopId));

        /// <summary>
        /// get the documents counts of the workshop with the given id
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the workshop</param>
        /// <returns>the documents counts</returns>
        [HttpGet("{Id}/Documents/Count")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopDocumentsCount>>> GetFinancialSummary(
            [FromRoute(Name = "Id")] string ConstructionWorkshopId)
            => ActionResultForAsync(_service.GetWorkshopDocumentsCountAsync(ConstructionWorkshopId));

        #region Documents Management

        /// <summary>
        /// get list of rubrics with their documents count details
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to get the document details for it</param>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>list of paged result</returns>
        [HttpPost("{id}/Rubric")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<WorkshopDocumentsDetails>>> GetDocumentDetails(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromBody] RubricFilterOptions filterOptions)
            => ActionResultForAsync(_service.GetDocumentsDetailsAsync(ConstructionWorkshopId, filterOptions));

        /// <summary>
        /// Create or Update a workshop base on the given model
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to associate the Rubrics for it</param>
        /// <param name="model">the model to be used for creating the rubric</param>
        /// <returns>list of Rubrics</returns>
        [HttpPut("{id}/Rubric")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopDocumentRubricModel>>> PutWorkshopRubrics(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromBody] WorkshopDocumentRubricModel model)
            => ActionResultForAsync(_service.PutWorkshopDocumentsRubricsAsync(ConstructionWorkshopId, model));

        /// <summary>
        /// delete the rubric with the given id
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to associate the Rubrics for it</param>
        /// <param name="rubricId">the id of the rubric</param>
        /// <returns>list of Rubrics</returns>
        [HttpDelete("{id}/Rubric/{rubricId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeleteWorkshopRubrics(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromRoute(Name = "rubricId")] string rubricId)
            => ActionResultForAsync(_service.DeleteWorkshopDocumentsRubricsAsync(ConstructionWorkshopId, rubricId));

        /// <summary>
        /// create a new document for the workshop
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to add the document for it</param>
        /// <returns>the Construction Workshop</returns>
        [HttpPost("{id}/Documents")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopDocumentModel>>> AddANewDocument(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromBody] WorkshopDocumentPutModel model)
            => ActionResultForAsync(_service.CreateDocumentAsync(ConstructionWorkshopId, model));

        /// <summary>
        /// Get list of documents that belongs to the given Rubrics
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to retrieve the Rubrics for it</param>
        /// <param name="rubricId">the id of the rubric</param>
        /// <returns>list of Rubrics</returns>
        [HttpGet("{id}/Rubric/{rubricId}/Documents")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<WorkshopDocumentModel>>> GetWorkshopRubrics(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromRoute(Name = "rubricId")] string rubricId)
            => ActionResultForAsync(_service.GetWorkshopRubricsDocumentsAsync(ConstructionWorkshopId, rubricId));

        /// <summary>
        /// get the document details
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to get the document details for it</param>
        /// <returns>the Construction Workshop document details</returns>
        [HttpPost("{id}/Documents/Rubrics/Details")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<WorkshopDocumentModel>>> GetDocumentsByRubric(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromBody] WorkshopDocumentFilterOptions filterOptions)
        {
            filterOptions.WorkshopId = ConstructionWorkshopId;
            return ActionResultForAsync(_service.GetRubricDocumentsAsync(filterOptions));
        }

        /// <summary>
        /// retrieve Construction Workshop documents
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to retrieve document for it</param>
        /// <returns>the Construction Workshop documents</returns>
        [HttpGet("{id}/Documents")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<WorkshopDocumentModel>>> GetWorkShopDocuments(
            [FromRoute(Name = "id")] string ConstructionWorkshopId)
            => ActionResultForAsync(_service.GetDocumentsAsync(ConstructionWorkshopId));

        /// <summary>
        /// get the document details with the given id
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the Construction Workshop to add the document for it</param>
        /// <param name="documentId">the id of the document to retrieve</param>
        /// <returns>the Construction Workshop</returns>
        [HttpGet("{id}/Documents/{documentId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopDocumentModel>>> GetWorkShopDocumentById(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromRoute(Name = "documentId")] string documentId)
            => ActionResultForAsync(_service.GetDocumentByIdAsync(ConstructionWorkshopId, documentId));

        /// <summary>
        /// update the documentation of a construction workshop
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the ConstructionWorkshop to be updated</param>
        /// <param name="model">the model used to update the documentation</param>
        /// <returns>the operation result</returns>
        [HttpPut("{id}/Update/Documents/{documentId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopDocumentModel>>> UpdateWorkshopDocumentation(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromRoute(Name = "documentId")] string documentId,
            [FromBody] WorkshopDocumentPutModel model)
            => ActionResultForAsync(_service.UpdateDocumentAsync(ConstructionWorkshopId, documentId, model));

        /// <summary>
        /// delete the Construction Workshop document with the given id
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the ConstructionWorkshop to be deleted it document</param>
        /// <param name="documentId">the id of the document to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Documents/{documentId}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> DeleteDocument(
            [FromRoute(Name = "id")] string ConstructionWorkshopId,
            [FromRoute(Name = "documentId")] string documentId)
            => ActionResultForAsync(_service.DeleteDocumentAsync(ConstructionWorkshopId, documentId));

        #endregion
    }

    /// <summary>
    /// partial part for <see cref="ConstructionWorkshopController"/>
    /// </summary>
    public partial class ConstructionWorkshopController : BaseController<ConstructionWorkshop>
    {
        private readonly IConstructionWorkshopService _service;

        public ConstructionWorkshopController(
            IConstructionWorkshopService constructionWorkshopService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = constructionWorkshopService;
        }
    }
}