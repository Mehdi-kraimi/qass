﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Documents;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the Payment API Controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class PaymentController
    {
        /// <summary>
        /// get a paged result of the Payments list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Payments as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<PaymentListModel>>> Get([FromBody] PaymentFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<PaymentListModel, PaymentFilterOptions>(filter));

        /// <summary>
        /// retrieve Payment with the given id
        /// </summary>
        /// <param name="PaymentId">the id of the Payment to be retrieved</param>
        /// <returns>the Payment</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<PaymentModel>>> Get(
            [FromRoute(Name = "id")] string PaymentId)
        {
            return ActionResultFor(await _service.GetByIdAsync<PaymentModel>(PaymentId));
        }

        /// <summary>
        /// create a new Payment record
        /// </summary>
        /// <param name="PaymentModel">the model to create the Payment from it</param>
        /// <returns>the newly created Payment</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<PaymentModel>>> Create(
            [FromBody] PaymentPutModel PaymentModel)
        {
            var result = await _service.CreateAsync<PaymentModel, PaymentPutModel>(PaymentModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// create a new Payment record
        /// </summary>
        /// <param name="PaymentModel">the model to create the Payment from it</param>
        /// <returns>the newly created Payment</returns>
        [HttpPost("Create/Transfer")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> CreateTransfer(
            [FromBody] PaymentCreatetransferModel model)
            => ActionResultForAsync(_service.CreateTransferOperationAsync(model));

        /// <summary>
        /// update the Payment informations
        /// </summary>
        /// <param name="PaymentModel">the model to use for updating the Payment</param>
        /// <param name="PaymentId">the id of the Payment to be updated</param>
        /// <returns>the updated Payment</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<PaymentModel>>> Update(
            [FromBody] PaymentPutModel PaymentModel,
            [FromRoute(Name = "id")] string PaymentId)
            => ActionResultForAsync(_service.UpdateAsync<PaymentModel, PaymentPutModel>(PaymentId, PaymentModel));

        /// <summary>
        /// delete the Payment with the given id
        /// </summary>
        /// <param name="PaymentId">the id of the Payment to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string PaymentId)
        {
            // try to delete the Payment, and return the right action result
            return ActionResultFor(await _service.DeleteAsync(PaymentId));
        }

        /// <summary>
        /// get the payments balance
        /// </summary>
        /// <returns>the total balance</returns>
        [HttpGet("Balance")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<PaymentBalance>>> GetPaymentBalance()
        {
            // try to delete the Payment, and return the right action result
            return ActionResultFor(await _service.GetPaymentsBalanceAsync());
        }
    }

    /// <summary>
    /// partial part for <see cref="Payment"/>
    /// </summary>
    public partial class PaymentController : BaseController<Payment>
    {
        private readonly IPaymentService _service;

        public PaymentController(
            IPaymentService paymentService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = paymentService;
        }
    }
}