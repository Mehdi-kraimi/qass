﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Documents;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the Quote API Controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class QuoteController : BaseController<Quote>
    {
        /// <summary>
        /// get a paged result of the Quotes list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Quotes as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<QuoteListModel>>> Get([FromBody] DocumentFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<QuoteListModel, DocumentFilterOptions>(filter));

        /// <summary>
        /// retrieve Quote with the given id
        /// </summary>
        /// <param name="QuoteId">the id of the Quote to be retrieved</param>
        /// <returns>the Quote</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<QuoteModel>>> Get([FromRoute(Name = "id")] string QuoteId)
            => ActionResultForAsync(_service.GetByIdAsync<QuoteModel>(QuoteId));

        /// <summary>
        /// create a new Quote record
        /// </summary>
        /// <param name="QuoteModel">the model to create the Quote from it</param>
        /// <returns>the newly created Quote</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<QuoteModel>>> Create([FromBody] QuotePutModel QuoteModel)
        {
            var result = await _service.CreateAsync<QuoteModel, QuotePutModel>(QuoteModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Quote informations
        /// </summary>
        /// <param name="QuoteModel">the model to use for updating the Quote</param>
        /// <param name="QuoteId">the id of the Quote to be updated</param>
        /// <returns>the updated Quote</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<QuoteModel>>> Update([FromBody] QuotePutModel QuoteModel, [FromRoute(Name = "id")] string QuoteId)
            => ActionResultForAsync(_service.UpdateAsync<QuoteModel, QuotePutModel>(QuoteId, QuoteModel));

        /// <summary>
        /// update the Quote status
        /// </summary>
        /// <param name="model">the model to use for updating the Quote status</param>
        /// <param name="QuoteId">the id of the Quote to be updated</param>
        /// <returns>the updated Quote</returns>
        [HttpPut("{id}/Update/Status")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateStatus([FromBody] DocumentUpdateStatusModel model, [FromRoute(Name = "id")] string QuoteId)
            => ActionResultForAsync(_service.UpdateStatusAsync(QuoteId, model));

        /// <summary>
        /// delete the Quote with the given id
        /// </summary>
        /// <param name="QuoteId">the id of the Quote to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string QuoteId)
            => ActionResultForAsync(_service.DeleteAsync(QuoteId));

        /// <summary>
        /// generate suppliers products orders
        /// </summary>
        /// <param name="QuoteId">the id of the Quote to be generate the orders from it</param>
        /// <returns>operation result</returns>
        [HttpGet("Generate/SuppliersOrders/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<MinimalDocumentModel>>> GenerateSupplierOrder([FromRoute(Name = "id")] string QuoteId)
            => ActionResultForAsync(_service.GenerateSupplierOrderAsync(QuoteId));

        /// <summary>
        /// generate Invoice from the quote with the given id
        /// </summary>
        /// <param name="QuoteId">the id of the Quote to be generate the Invoice from it</param>
        /// <returns>operation result</returns>
        [HttpPost("{id}/Generate/Invoice")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<InvoiceModel>>> GenerateInvoice([FromRoute(Name = "id")] string QuoteId)
            => ActionResultForAsync(_service.GenerateInvoiceFromQuoteAsync(QuoteId));

        /// <summary>
        /// update the signature Sheet status
        /// </summary>
        /// <param name="model">the model to use for updating the Operation Sheet signature</param>
        /// <param name="operationSheetId">the id of the Operation Sheet to be updated</param>
        /// <returns>the updated Operation Sheet</returns>
        [HttpPut("{id}/Update/Signature")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateSignature([FromBody] DocumentUpdateSignatureModel model,
            [FromRoute(Name = "id")] string operationSheetId)
            => ActionResultForAsync(_service.UpdateSignatureAsync(operationSheetId, model));

        /// <summary>
        /// retrieve Quote with the given id
        /// </summary>
        /// <param name="QuoteId">the id of the Quote to be retrieved</param>
        /// <returns>the Quote</returns>
        [HttpPost("{id}/Export")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<byte[]>>> Export([FromRoute(Name = "id")] string QuoteId, [FromBody] DataExportOptions exportOptions)
            => ActionResultForAsync(_service.ExportDataAsync(QuoteId, exportOptions));

        /// <summary>
        /// retrieve Quote with the given id
        /// </summary>
        /// <param name="QuoteId">the id of the Quote to be retrieved</param>
        /// <returns>the Quote</returns>
        [HttpPost("{id}/Email")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> EmailDocument([FromRoute(Name = "id")] string QuoteId, [FromBody] SendEmailOptions emailOptions)
            => ActionResultForAsync(_service.SendAsync(QuoteId, emailOptions));

        /// <summary>
        /// duplicate the Quote with the given id
        /// </summary>
        /// <param name="QuoteId">the id of the Quote to be duplicated</param>
        /// <returns>the generated Quote</returns>
        [HttpGet("Duplicate/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<QuoteModel>>> EmailDocument([FromRoute(Name = "id")] string QuoteId)
            => ActionResultForAsync(_service.DuplicateAsync<QuoteModel>(QuoteId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);
    }

    /// <summary>
    /// partial part for <see cref="QuoteController"/>
    /// </summary>
    public partial class QuoteController : BaseController<Quote>
    {
        private readonly IQuoteService _service;

        /// <summary>
        /// create an instant of <see cref="QuoteController"/>
        /// </summary>
        /// <param name="loggedInUserService">the logged in user service</param>
        /// <param name="translationService">the translation service</param>
        /// <param name="loggerFactory">the logger factory</param>
        public QuoteController(
            IQuoteService quoteService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = quoteService;
        }
    }
}