﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Documents;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the invoice API Controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class InvoiceController : BaseController<Invoice>
    {
        /// <summary>
        /// get a paged result of the Invoices list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Invoices as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<InvoiceListModel>>> Get([FromBody] DocumentFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<InvoiceListModel, DocumentFilterOptions>(filter));

        /// <summary>
        /// retrieve Invoice with the given id
        /// </summary>
        /// <param name="InvoiceId">the id of the Invoice to be retrieved</param>
        /// <returns>the Invoice</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<InvoiceModel>>> Get([FromRoute(Name = "id")] string InvoiceId)
            => ActionResultForAsync(_service.GetByIdAsync<InvoiceModel>(InvoiceId));

        /// <summary>
        /// retrieve Invoice with the given id
        /// </summary>
        /// <param name="InvoiceId">the id of the Quote to be retrieved</param>
        /// <returns>the Quote</returns>
        [HttpPost("{id}/Email")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> EmailDocument([FromRoute(Name = "id")] string InvoiceId, [FromBody] SendEmailOptions emailOptions)
            => ActionResultForAsync(_service.SendAsync(InvoiceId, emailOptions));

        /// <summary>
        /// create a new Invoice record
        /// </summary>
        /// <param name="InvoiceModel">the model to create the Invoice from it</param>
        /// <returns>the newly created Invoice</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<InvoiceModel>>> Create(
            [FromBody] InvoicePutModel InvoiceModel)
        {
            var result = await _service.CreateAsync<InvoiceModel, InvoicePutModel>(InvoiceModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Invoice informations
        /// </summary>
        /// <param name="InvoiceModel">the model to use for updating the Invoice</param>
        /// <param name="InvoiceId">the id of the Invoice to be updated</param>
        /// <returns>the updated Invoice</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<InvoiceModel>>> Update(
            [FromBody] InvoicePutModel InvoiceModel,
            [FromRoute(Name = "id")] string InvoiceId)
            => ActionResultForAsync(_service.UpdateAsync<InvoiceModel, InvoicePutModel>(InvoiceId, InvoiceModel));

        /// <summary>
        /// update the Invoice Holdback informations
        /// </summary>
        /// <param name="model">the Holdback Details Update Model</param>
        /// <param name="InvoiceId">the id of the Invoice to be updated the holdback for it</param>
        /// <returns>the updated Invoice</returns>
        [HttpPut("{id}/Update/Holdback")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> UpdateHoldback(
            [FromBody] HoldbackDetailsUpdateModel model,
            [FromRoute(Name = "id")] string InvoiceId)
            => ActionResultForAsync(_service.UpdateHoldbackDetailsAsync(InvoiceId, model));

        /// <summary>
        /// get the documents counts of the workshop with the given id
        /// </summary>
        /// <param name="ConstructionWorkshopId">the id of the workshop</param>
        /// <returns>the documents counts</returns>
        [HttpGet("Workshop/{workshopId}/Holdback")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<HoldbackDetailsModel>>> GetHoldbacks(
            [FromRoute(Name = "workshopId")] string ConstructionWorkshopId)
            => ActionResultForAsync(_service.GetHoldbackDetailsAsync(ConstructionWorkshopId));

        /// <summary>
        /// cancel the invoice with the given id
        /// </summary>
        /// <param name="InvoiceId">the id of the Invoice to be canceled</param>
        /// <param name="creditNote">the credit not to be used for canceling the invoice</param>
        /// <returns>the operation result</returns>
        [HttpGet("{id}/Cancel")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<CreditNoteModel>>> CancelInvoice([FromRoute(Name = "id")] string InvoiceId)
            => ActionResultForAsync(_service.CancelInvoiceAsync(InvoiceId));

        /// <summary>
        /// delete the Invoice with the given id
        /// </summary>
        /// <param name="InvoiceId">the id of the Invoice to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string InvoiceId)
            => ActionResultForAsync(_service.DeleteAsync(InvoiceId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
            => await _service.IsRefrenceUniqueAsync(reference);

        /// <summary>
        /// retrieve Invoice with the given id
        /// </summary>
        /// <param name="InvoiceID">the id of the Invoice to be retrieved</param>
        /// <returns>the Quote</returns>
        [HttpPost("{id}/Export")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<byte[]>>> Export([FromRoute(Name = "id")] string InvoiceID, [FromBody] DataExportOptions exportOptions)
            => ActionResultForAsync(_service.ExportDataAsync(InvoiceID, exportOptions));

        /// <summary>
        /// retrieve Invoice with the given id
        /// </summary>
        /// <param name="InvoiceID">the id of the Invoice to be retrieved</param>
        /// <returns>the Quote</returns>
        [HttpPost("ExportReleveByPeriod")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<ExportByte>>> ExportReleve([FromBody] ExportByPeriodByte exportOptions)
            => ActionResultForAsync(_service.UpdateExportReleve(exportOptions));
    }

    /// <summary>
    /// partial part for <see cref="Invoice"/>
    /// </summary>
    public partial class InvoiceController : BaseController<Invoice>
    {
        private readonly IInvoiceService _service;

        public InvoiceController(
            IInvoiceService invoiceService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = invoiceService;
        }
    }
}