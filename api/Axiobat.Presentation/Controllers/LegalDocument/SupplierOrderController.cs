﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Documents;
    using Domain.Entities;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the Supplier Order API Controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class SupplierOrderController : BaseController<SupplierOrder>
    {
        /// <summary>
        /// get a paged result of the SupplierOrders list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of SupplierOrders as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<SupplierOrderModel>>> Get([FromBody] DocumentFilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<SupplierOrderModel, DocumentFilterOptions>(filter));

        /// <summary>
        /// retrieve SupplierOrder with the given id
        /// </summary>
        /// <param name="SupplierOrderId">the id of the SupplierOrder to be retrieved</param>
        /// <returns>the SupplierOrder</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<SupplierOrderModel>>> Get([FromRoute(Name = "id")] string SupplierOrderId)
            => ActionResultForAsync(_service.GetByIdAsync<SupplierOrderModel>(SupplierOrderId));

        /// <summary>
        /// create a new SupplierOrder record
        /// </summary>
        /// <param name="SupplierOrderModel">the model to create the SupplierOrder from it</param>
        /// <returns>the newly created SupplierOrder</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<SupplierOrderModel>>> Create([FromBody] SupplierOrderPutModel SupplierOrderModel)
        {
            var result = await _service.CreateAsync<SupplierOrderModel, SupplierOrderPutModel>(SupplierOrderModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the SupplierOrder informations
        /// </summary>
        /// <param name="SupplierOrderModel">the model to use for updating the SupplierOrder</param>
        /// <param name="SupplierOrderId">the id of the SupplierOrder to be updated</param>
        /// <returns>the updated SupplierOrder</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<SupplierOrderModel>>> Update([FromBody] SupplierOrderPutModel SupplierOrderModel, [FromRoute(Name = "id")] string SupplierOrderId)
            => ActionResultForAsync(_service.UpdateAsync<SupplierOrderModel, SupplierOrderPutModel>(SupplierOrderId, SupplierOrderModel));

        /// <summary>
        /// delete the SupplierOrder with the given id
        /// </summary>
        /// <param name="SupplierOrderId">the id of the SupplierOrder to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string SupplierOrderId)
            => ActionResultForAsync(_service.DeleteAsync(SupplierOrderId));

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Reference/{reference}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckReferenceIfUnique([FromRoute(Name = "reference")] string reference)
        {
            // check if the reference, and return the right action result
            return await _service.IsRefrenceUniqueAsync(reference);
        }
    }

    /// <summary>
    /// partial part for <see cref="SupplierOrderController"/>
    /// </summary>
    public partial class SupplierOrderController : BaseController<SupplierOrder>
    {
        private readonly ISupplierOrderService _service;

        public SupplierOrderController(
            ISupplierOrderService supplierOrderService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = supplierOrderService;
        }
    }
}