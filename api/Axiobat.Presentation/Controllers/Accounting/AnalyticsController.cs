﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Services.Accounting;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    public partial class AnalyticsController : BaseController
    {
        /// <summary>
        /// get the workshop Analytics
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>a paged result</returns>
        [HttpPost("Workshop")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<WorkshopAnalyticsResult>>> GetWorkshopAnalytics([FromBody] WorkshopAnalyticsFilterOtions filterModel)
            => ActionResultForAsync(_service.GetWorkshopAnalyticsAsync(filterModel));

        /// <summary>
        /// get the Financial Analytics
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>a paged result</returns>
        [HttpPost("Financial")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<FinancialAnalyticsResult>>> GetFinancialAnalytics([FromBody] ExternalPartnerFilterOptions filterModel)
            => ActionResultForAsync(_service.GetFinancialAnalyticsAsync(filterModel));

        /// <summary>
        /// get the Sales Analytics
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>a paged result</returns>
        [HttpPost("Sales")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<SalesAnalyticsResult>>> GetSalesAnalytics([FromBody] TransactionAnalyticsFilterOptions filterModel)
            => ActionResultForAsync(_service.GetSalesAnalyticsAsync(filterModel));

        /// <summary>
        /// get the Purchase Analytics
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>a paged result</returns>
        [HttpPost("Purchase")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<PurchaseAnalyticsResult>>> GetPurchaseAnalytics([FromBody] TransactionAnalyticsFilterOptions filterModel)
            => ActionResultForAsync(_service.GetPurchaseAnalyticsAsync(filterModel));

        /// <summary>
        /// get the Purchase Analytics
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>a paged result</returns>
        [HttpPost("Maintenance")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<MaintenanceAnalyticsResult>>> GetMaintenanceAnalytics([FromBody] MaintenanceAnalyticsFilterOptions filterModel)
            => ActionResultForAsync(_service.GetMaintenanceAnalyticsAsync(filterModel));

    }

    public partial class AnalyticsController : BaseController
    {
        private readonly IAnalyticsService _service;

        public AnalyticsController(
            IAnalyticsService analyticsService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = analyticsService;
        }
    }
}