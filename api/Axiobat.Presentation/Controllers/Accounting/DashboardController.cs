﻿namespace Axiobat.Presentation.Controllers.Accounting
{
    using Application.Services.Accounting;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// the dashboard controller
    /// </summary>
    public partial class DashboardController
    {
        /// <summary>
        /// Get document details
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>the documents details result</returns>
        [HttpPost("DocumentDetails")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<DashboardDocumentsDetailsModel>>> GetDashboardDocumentsDetails(
            [FromBody] DateRangeFilterOptions filterModel)
            => ActionResultForAsync(_service.GetDocumentsDetailsAsync(filterModel));

        /// <summary>
        /// Get TurnOver details by year
        /// </summary>
        /// <param name="year">the year to get the details for it</param>
        /// <returns>the turnOver details result</returns>
        [HttpGet("TurnOver/{year:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<DashboardTurnOverDetailsModel>>> GetDashboardTurnOverDetails(
            [FromRoute(Name = "year")] int year)
            => ActionResultForAsync(_service.GetTurnOverDetailsAsync(year));

        /// <summary>
        /// Get expenses details by year
        /// </summary>
        /// <param name="year">the year to get the details for it</param>
        /// <returns>the expenses details result</returns>
        [HttpGet("Expenses/{year:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<DashboardExpensesDetailsModel>>> GetDashboardExpensesDetails(
            [FromRoute(Name = "year")] int year)
            => ActionResultForAsync(_service.GetExpensesDetailsAsync(year));

        /// <summary>
        /// Get tops details
        /// </summary>
        /// <param name="topValue">the tops Value</param>
        /// <returns>the tops details result</returns>
        [HttpGet("Tops/{topValue:int}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<DashboardTopsDetailsModel>>> GetDashboardTopsDetails(
            [FromRoute(Name = "topValue")] int topValue)
            => ActionResultForAsync(_service.GetTopsDetailsAsync(topValue));

        /// <summary>
        /// Get cash summary details
        /// </summary>
        /// <param name="topValue">the tops Value</param>
        /// <returns>the tops details result</returns>
        [HttpGet("CashSummary")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<DashboardCashSummaryDetailsModel>>> GetDashboardCashSummaryDetails()
            => ActionResultForAsync(_service.GetCashSummaryDetailsAsync());
    }

    /// <summary>
    /// the partial part for <see cref="DashboardController"/>
    /// </summary>
    public partial class DashboardController : BaseController
    {
        private readonly IDashboardService _service;

        public DashboardController(
            IDashboardService service,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            this._service = service;
        }
    }
}
