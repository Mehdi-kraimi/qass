﻿namespace Axiobat.Presentation.Controllers.Lots
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Services.Products;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;

    /// <summary>
    /// Lot management API controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class LotController
    {
        /// <summary>
        /// get list of all Lots
        /// </summary>
        /// <returns>list of all Lots</returns>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<LotModel>>> GetAll()
            => ActionResultForAsync(_service.GetAllAsync<LotModel>());

        /// <summary>
        /// get a paged result of the Lots list using the given <see cref="FilterOptions"/>
        /// </summary>
        /// <param name="filter">the filter options model</param>
        /// <returns>list of Lots as paged result</returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<PagedResult<LotModel>>> Get([FromBody] FilterOptions filter)
            => ActionResultForAsync(_service.GetAsPagedResultAsync<LotModel, FilterOptions>(filter));

        /// <summary>
        /// retrieve Lot with the given id
        /// </summary>
        /// <param name="LotId">the id of the Lot to be retrieved</param>
        /// <returns>the Lot</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<LotModel>>> Get([FromRoute(Name = "id")] string LotId)
            => ActionResultForAsync(_service.GetByIdAsync<LotModel>(LotId));

        /// <summary>
        /// create a new Lot record
        /// </summary>
        /// <param name="LotModel">the model to create the Lot from it</param>
        /// <returns>the newly created Lot</returns>
        [HttpPost("Create")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<LotModel>>> Create(
            [FromBody] LotPutModel LotModel)
        {
            var result = await _service.CreateAsync<LotModel, LotPutModel>(LotModel);
            if (result.Status == ResultStatus.Failed)
            {
                // something went wrong (exception)
                if (result.HasError)
                    return StatusCode(500, result);

                // result not found
                if (!result.HasValue || result.MessageCode == MessageCode.NotFound)
                    return NotFound(result);

                // user is not authorized
                if (result.MessageCode.Equals(MessageCode.Unauthorized))
                    return StatusCode(StatusCodes.Status403Forbidden, result);

                //if nothing bad request
                return BadRequest(result);
            }

            return CreatedAtAction(nameof(Get), new { result.Value.Id }, result);
        }

        /// <summary>
        /// update the Lot informations
        /// </summary>
        /// <param name="LotModel">the model to use for updating the Lot</param>
        /// <param name="LotId">the id of the Lot to be updated</param>
        /// <returns>the updated Lot</returns>
        [HttpPut("{id}/Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result<LotModel>>> Update([FromRoute(Name = "id")] string LotId, [FromBody] LotPutModel LotModel) =>
            ActionResultForAsync(_service.UpdateAsync<LotModel, LotPutModel>(LotId, LotModel));

        /// <summary>
        /// delete the Lot with the given id
        /// </summary>
        /// <param name="LotId">the id of the Lot to be deleted</param>
        /// <returns>the operation result</returns>
        [HttpDelete("{id}/Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<Result>> Delete([FromRoute(Name = "id")] string LotId)
            => ActionResultForAsync(_service.DeleteAsync(LotId));

        /// <summary>
        /// check if the given name is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="name">the name to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("Check/Name/{name}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<bool>> CheckNameIfUnique([FromRoute(Name = "name")] string name)
            => await _service.IsNameUniqueAsync(name);

        /// <summary>
        /// check if the given reference is unique, returns true if unique, false if not
        /// </summary>
        /// <param name="reference">the reference to be checked</param>
        /// <returns>true if unique, false if not</returns>
        [HttpGet("{id}/Products")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<ListResult<ProductModel>>> GetLotLots([FromRoute(Name = "id")] string lotId)
        {
            // check if the reference, and return the right action result
            return ActionResultFor(await _service.GetLotProductsAsync(lotId));
        }
    }

    /// <summary>
    /// partial part for <see cref="LotController"/>
    /// </summary>
    public partial class LotController : BaseController
    {
        private readonly ILotService _service;

        /// <summary>
        /// create an instant of <see cref="LotController"/>
        /// </summary>
        /// <param name="loggedInUserService">the logged in user service</param>
        /// <param name="translationService">the translation service</param>
        /// <param name="loggerFactory">the logger factory</param>
        public LotController(
            ILotService lotService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = lotService;
        }
    }
}