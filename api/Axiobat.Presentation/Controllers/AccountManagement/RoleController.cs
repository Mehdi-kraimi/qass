﻿namespace Axiobat.Presentation.Controllers
{
    using Application.Services.AccountManagement;
    using Application.Services.Localization;
    using Application.Models;
    using Domain.Entities;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the account controller
    /// </summary>
    [Route("api/[controller]")]
    public partial class RoleController : BaseController<Role>
    {
        /// <summary>
        /// get the list of roles
        /// </summary>
        /// <param name="filter">the filter options to get the paged result</param>
        /// <returns>list of roles</returns>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<PagedResult<RoleModel>>> GetRoles(
            [FromBody] FilterOptions filter)
        {
            return ActionResultFor(await _service.GetAsPagedResultAsync<RoleModel, FilterOptions>(filter));
        }

        /// <summary>
        /// get list of all roles
        /// </summary>
        /// <returns>the list of roles</returns>
        [HttpGet]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<ListResult<RoleModel>>> GetAllRoles()
        {
            return ActionResultFor(await _service.GetAllAsync<RoleModel>());
        }

        /// <summary>
        /// get the user with the given Id
        /// </summary>
        /// <param name="service">the user management service</param>
        /// <param name="id">the id of the user</param>
        /// <returns>return the user</returns>
        [HttpPut("Users")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public Task<ActionResult<ListResult<UserModel>>> GetbyRoles(
            [FromServices] IUsersManagementService usersManagementService,
            [FromBody] UserRetrievealByRoleType model)
            => ActionResultForAsync(usersManagementService.GetByRoleTypeAsync<UserModel>(model));

        /// <summary>
        /// get the role with the given id
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>the role with the given id</returns>
        [HttpGet("{id:Guid}")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<RoleModel>>> GetRoleById(
            [FromRoute(Name = "id")] Guid roleId)
        {
            return ActionResultFor(await _service.GetByIdAsync<RoleModel>(roleId));
        }

        /// <summary>
        /// create a new Role
        /// </summary>
        /// <param name="roleModel">the role model</param>
        /// <returns>the newly created role</returns>
        [HttpPost("Create")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<RoleModel>>> CreateRole(
            [FromBody] RoleUpdateModel roleModel)
        {
            return ActionResultFor(await _service.CreateAsync<RoleModel, RoleUpdateModel>(roleModel));
        }

        /// <summary>
        /// delete the role with the given id
        /// </summary>
        /// <param name="userId">the id of the role to be deleted</param>
        /// <returns>an operation result</returns>
        [HttpDelete("{id:Guid}/Delete")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> DeleteRole(
            [FromRoute(Name = "id")] Guid userId)
        {
            // Try to delete the role and return the proper action result
            return ActionResultFor(await _service.DeleteAsync(userId));
        }

        /// <summary>
        /// update the role with given id using the pass in model
        /// </summary>
        /// <param name="roleId">the id of the role to be updated</param>
        /// <param name="roleModel">the role model</param>
        /// <returns>the updated role</returns>
        [HttpPut("{id:Guid}/Update")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result<RoleModel>>> UpdateRole(
            [FromRoute(Name = "id")] Guid roleId,
            [FromBody] RoleUpdateModel roleModel)
        {
            // try to update the role and return the proper action result
            return ActionResultFor(await _service.UpdateAsync<RoleModel, RoleUpdateModel>(roleId, roleModel));
        }

        /// <summary>
        /// update the role permissions
        /// </summary>
        /// <param name="model">the role model</param>
        /// <param name="roleId">the id of the role to be update the Access for it</param>
        /// <returns>the updated role</returns>
        [HttpPut("{id:Guid}/Update/Permissions")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Result>> UpdateRoleAccess(
            [FromRoute(Name = "id")] Guid roleId,
            [FromBody] RolePermissionsModel model)
        {
            // try to update the role permission
            await _service.UpdateRolePermissionsAsync(roleId, model);

            // all done
            return Ok();
        }
    }

    /// <summary>
    /// partial part for <see cref="RoleController"/>
    /// </summary>
    public partial class RoleController : BaseController<Role>
    {
        private readonly IRoleManagementService _service;

        /// <summary>
        /// create an instant of <see cref="RoleController"/>
        /// </summary>
        /// <param name="loggedInUserService"><see cref="ILoggedInUserService"/> instant</param>
        /// <param name="translationService"><see cref="ITranslationService"/> instant</param>
        /// <param name="loggerFactory"><see cref="ILoggerFactory"/> instant</param>
        public RoleController(
            IRoleManagementService roleService,
            ILoggedInUserService loggedInUserService,
            ITranslationService translationService,
            ILoggerFactory loggerFactory)
            : base(loggedInUserService, translationService, loggerFactory)
        {
            _service = roleService;
        }
    }
}