﻿namespace Axiobat.Presentation.BackgroundServices
{
    using Application.Data;
    using Application.Services.Configuration;
    using Axiobat.Domain.Entities;
    using Domain.Constants;
    using Domain.Enums;
    using Domain.Exceptions;
    using Microsoft.EntityFrameworkCore.Internal;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// this class is used to process Recurring Documents
    /// </summary>
    public partial class RecurringDocumentBackgroundService
    {
        protected override async Task RunAsync(IServiceScope scope)
        {
            var configurationService = scope.GetService<IApplicationConfigurationService>();
            var recurringDataAccess = scope.GetService<IRecurringDocumentDataAccess>();
            var invoiceDataAccess = scope.GetService<IInvoiceDataAccess>();
            var expenseDataAccess = scope.GetService<IExpenseDataAccess>();

            var currentRecurrings = await recurringDataAccess.GetAllAsync(options =>
            {
                options.AddPredicate(recurring =>
                    recurring.Status == RecurringStatus.InProgress &&
                    recurring.NextOccurring == DateTime.Today);
            });

            if (!currentRecurrings.Any())
                return;

            var invoices = new LinkedList<Invoice>();
            var expenses = new LinkedList<Expense>();

            foreach (var recurring in currentRecurrings)
            {
                switch (recurring.DocType)
                {
                    case DocumentType.Invoice:
                        invoices.AddLast(await ProcessRecurringInvoiceAsync(configurationService, recurring.Document.As<Invoice>()));
                        break;
                    case DocumentType.Expenses:
                        expenses.AddLast(await ProcessRecurringExpensesAsync(configurationService, recurring.Document.As<Expense>()));
                        break;
                    default:
                        throw new AxiobatException("unsupported recurring document type, only ['invoices', 'expenses'] are supported");
                }

                recurring.Counter += 1;

                if (recurring.HasFinished)
                {
                    recurring.Status = RecurringStatus.Finished;
                    continue;
                }

                recurring.SetNextOccurring();
            }

            if (expenses.Any())
                await expenseDataAccess.AddRangeAsync(expenses);

            if (invoices.Any())
                await invoiceDataAccess.AddRangeAsync(invoices);

            await recurringDataAccess.UpdateRangeAsync(currentRecurrings);
        }
    }

    /// <summary>
    /// partial part for <see cref="RecurringDocumentBackgroundService"/>
    /// </summary>
    public partial class RecurringDocumentBackgroundService : BaseBackgroundService
    {
        public RecurringDocumentBackgroundService(IServiceProvider services)
            : base(services)
        {
        }

        private async Task<Expense> ProcessRecurringExpensesAsync(IApplicationConfigurationService configurationService, Expense expense)
        {
            expense.Reference = await configurationService.GenerateRefereceAsync(DocumentType.Expenses);
            return expense;
        }

        private async Task<Invoice> ProcessRecurringInvoiceAsync(IApplicationConfigurationService configurationService, Invoice invoice)
        {
            invoice.Reference = await configurationService.GenerateRefereceAsync(DocumentType.Invoice);

            var documentConfig = await configurationService.GetDocumentConfigAsync(DocumentType.Invoice);
            invoice.PaymentCondition = documentConfig.PaymentCondition;
            invoice.Note = documentConfig.Note;
            invoice.Status = InvoiceStatus.Draft;

            return invoice;
        }
    }
}
