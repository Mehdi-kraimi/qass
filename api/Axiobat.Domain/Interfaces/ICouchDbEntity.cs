﻿namespace Axiobat.Domain.Interfaces
{
    public interface ICouchDbEntity : IEntity<string>
    {
        /// <summary>
        /// document Rev for couchDb
        /// </summary>
        string _rev { get; set; }

        /// <summary>
        /// the type of the document, used for CouchDb
        /// </summary>
        string DocType { get; }

        /// <summary>
        /// this method is used to generate a unique id based on the format specified
        /// </summary>
        void GenerateId(string societeId);
    }
}