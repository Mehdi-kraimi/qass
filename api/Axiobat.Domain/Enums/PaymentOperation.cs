﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the <see cref="Entities.Payment"/> operation
    /// </summary>
    public enum PaymentOperation
    {
        /// <summary>
        /// a payment
        /// </summary>
        Payment,

        /// <summary>
        /// a grouped payment
        /// </summary>
        GroupedPayment,

        /// <summary>
        /// a transfer from an account
        /// </summary>
        Transfer_From,

        /// <summary>
        /// a transfer to an account
        /// </summary>
        Transfer_To,

        /// <summary>
        /// a payment with a CreditNote
        /// </summary>
        CreditNotePayment
    }
}
