﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the payment method
    /// </summary>
    public enum PaymentMethodType
    {
        /// <summary>
        /// the payment method is a custom method defines by the user
        /// </summary>
        Custom,

        /// <summary>
        /// the payment method is a credit note
        /// </summary>
        CreditNote,
    }
}
