﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the permission
    /// </summary>
    public enum PermissionType
    {
        /// <summary>
        /// permission to Access
        /// </summary>
        Access,

        /// <summary>
        /// permission to Create
        /// </summary>
        Create,

        /// <summary>
        /// permission to Update
        /// </summary>
        Update,

        /// <summary>
        /// permission to Delete
        /// </summary>
        Delete,

        /// <summary>
        /// the permission is for an operation
        /// </summary>
        Operation
    }
}
