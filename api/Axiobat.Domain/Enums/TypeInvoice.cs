﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the type of the invoice
    /// </summary>
    public enum InvoiceType
    {
        /// <summary>
        /// the invoice don't have any type
        /// </summary>
        General = 0,

        /// <summary>
        /// the invoice it a situation
        /// </summary>
        Situation = 1,

        /// <summary>
        /// the invoice it an acompte
        /// </summary>
        Acompte = 2,

        /// <summary>
        /// the invoice has been closed
        /// </summary>
        Cloture = 3
    }
}
