﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the date format
    /// </summary>
    public enum DateFormat
    {
        /// <summary>
        /// don't include any date
        /// </summary>
        None = 0,

        /// <summary>
        /// include the current year
        /// </summary>
        Year = 1,

        /// <summary>
        /// include the current year and month
        /// </summary>
        Year_Month = 2,
    }
}
