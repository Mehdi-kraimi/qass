﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// the modules defines in the application
    /// </summary>
    public enum AppModules
    {
        /// <summary>
        /// the access to deferents apps (like web, and mobile)
        /// </summary>
        AppAccess,
    }
}
