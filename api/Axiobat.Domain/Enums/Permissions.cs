﻿namespace Axiobat.Domain.Enums
{
    using Attributes;

    /// <summary>
    /// this enum defines the names for the Policies
    /// </summary>
    public enum Permissions
    {
        /// <summary>
        /// give access to the web application
        /// </summary>
        [PermissionDescriptor(1, "Access", PermissionType.Access, "a permission to Access the web Application", AppModules.AppAccess)]
        WebAppAccess = 1,

        /// <summary>
        /// give access to the mobile application
        /// </summary>
        [PermissionDescriptor(2, "Access", PermissionType.Access, "a permission to Access the mobile Application", AppModules.AppAccess)]
        MobileAppAccess = 2,
    }
}
