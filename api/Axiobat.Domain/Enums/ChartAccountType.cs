﻿namespace Axiobat.Domain.Enums
{
    /// <summary>
    /// Chart Account Category Types
    /// </summary>
    public enum ChartAccountType
    {
        /// <summary>
        /// the Category repressing a default value
        /// </summary>
        Default = 0,

        /// <summary>
        /// expenses Category
        /// </summary>
        Expense = 1,

        /// <summary>
        /// sales Category
        /// </summary>
        Sales = 2,

        /// <summary>
        /// VAT Category
        /// </summary>
        VAT = 3,

        /// <summary>
        /// assets chart of accounts
        /// </summary>
        Assets = 4,

        /// <summary>
        /// the chart account is a subCategory
        /// </summary>
        SubCategory = 5
    }
}
