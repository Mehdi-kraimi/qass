﻿namespace Axiobat.Domain.Constants
{
    /// <summary>
    /// this class holds information about system emails
    /// </summary>
    public static partial class Emails
    {
        /// <summary>
        /// the email user to send the account management relates emails
        /// </summary>
        public const string AccountManagement = "account_management";

        /// <summary>
        /// the email used to send the emails to users for general use, the default one
        /// </summary>
        public const string Default = "default";
    }
}
