﻿namespace Axiobat.Domain.Constants
{
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// contains list of document status
    /// </summary>
    public static partial class DocumentStatus
    {
        /// <summary>
        /// list of all document status
        /// </summary>
        public static List<string> ALL => Enumerable.Empty<string>()
                .Concat(QuoteStatus.ALL)
                .Concat(InvoiceStatus.ALL)
                .Concat(ExpenseStatus.ALL)
                .Concat(WorkshopStatus.ALL)
                .Concat(HoldbackStatus.ALL)
                .Concat(RecurringStatus.ALL)
                .Concat(CreditNoteStatus.ALL)
                .Concat(SupplierOrderStatus.ALL)
                .Concat(OperationSheetStatus.ALL)
                .Concat(MaintenanceVisitStatus.ALL)
                .Concat(MaintenanceContractStatus.ALL)
                .Concat(MaintenanceOperationSheetStatus.ALL)
                .Distinct()
                .ToList();

        /// <summary>
        /// validate the status of the document
        /// </summary>
        /// <param name="status">the status to validate</param>
        /// <param name="documentType">the document type</param>
        /// <returns>true if valid, false if not</returns>
        public static bool ValidStatus(string status)
            => ALL.Contains(status);

        /// <summary>
        /// validate the status of the document
        /// </summary>
        /// <param name="status">the status to validate</param>
        /// <param name="documentType">the document type</param>
        /// <returns>true if valid, false if not</returns>
        public static bool ValidStatus(string status, DocumentType documentType)
            => GetDocumentStatus(documentType).Contains(status);

        /// <summary>
        /// get the list of status of the given document type, if not exist an empty array will be returned
        /// </summary>
        /// <param name="documentType">the document type</param>
        /// <returns>list of status</returns>
        public static string[] GetDocumentStatus(DocumentType documentType)
        {
            switch (documentType)
            {
                case DocumentType.Quote: return QuoteStatus.ALL;
                case DocumentType.Invoice: return InvoiceStatus.ALL;
                case DocumentType.Expenses: return ExpenseStatus.ALL;
                case DocumentType.Workshop: return WorkshopStatus.ALL;
                case DocumentType.CreditNote: return CreditNoteStatus.ALL;
                case DocumentType.RecurringDocument: return RecurringStatus.ALL;
                case DocumentType.SupplierOrder: return SupplierOrderStatus.ALL;
                case DocumentType.OperationSheet: return OperationSheetStatus.ALL;
                case DocumentType.MaintenanceVisit: return MaintenanceVisitStatus.ALL;
                case DocumentType.MaintenanceContract: return MaintenanceContractStatus.ALL;
                case DocumentType.MaintenanceOperationSheet: return MaintenanceOperationSheetStatus.ALL;
                default: return Array.Empty<string>();
            }
        }
    }

    /// <summary>
    /// the Maintenance Operation Sheet statuses
    /// </summary>
    public static class MaintenanceOperationSheetStatus
    {
        /// <summary>
        /// list of all status
        /// </summary>
        public static string[] ALL = new string[] { Draft, Planned, Realized, Canceled, Billed, UnDone };

        /// <summary>
        /// the Maintenance Operation Sheet has been drafted
        /// </summary>
        public const string Draft = "draft";

        /// <summary>
        /// the Maintenance Operation Sheet has been planned for it.
        /// </summary>
        public const string Planned = "planned";

        /// <summary>
        /// the Maintenance Operation Sheet been realized
        /// </summary>
        public const string Realized = "realized";

        /// <summary>
        /// the Maintenance Operation Sheet has been canceled
        /// </summary>
        public const string Canceled = "canceled";

        /// <summary>
        /// the Maintenance Operation Sheet has been billed
        /// </summary>
        public const string Billed = "billed";

        /// <summary>
        /// the Maintenance Operation Sheet has been undone
        /// </summary>
        public const string UnDone = "un_done";

        /// <summary>
        /// the Maintenance Operation Sheet is late
        /// </summary>
        public const string Late = "late";
    }

    /// <summary>
    /// the Maintenance Visit status
    /// </summary>
    public static class MaintenanceVisitStatus
    {
        /// <summary>
        /// list of all status
        /// </summary>
        public static string[] ALL = new string[] { Planned, Unplanned, };

        /// <summary>
        /// the Maintenance Visit has been planned for it.
        /// </summary>
        public const string Planned = "planned";

        /// <summary>
        /// the Maintenance Visit has been not been planned for it yet.
        /// </summary>
        public const string Unplanned = "Unplanned";

        /// <summary>
        /// the Maintenance Visit has been canceled
        /// </summary>
        public const string Canceled = "canceled";

        /// <summary>
        /// the Maintenance Visit been realized
        /// </summary>
        public const string Realized = "realized";
    }

    /// <summary>
    /// the Maintenance Contract status
    /// </summary>
    public static class MaintenanceContractStatus
    {
        /// <summary>
        /// list of all status
        /// </summary>
        public static string[] ALL = new string[] { InProgress, Draft, Finished, Canceled };

        /// <summary>
        /// the Contract still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the Contract has been drafted
        /// </summary>
        public const string Draft = "draft";

        /// <summary>
        /// the Contract has been finished
        /// </summary>
        public const string Finished = "finished";

        /// <summary>
        /// the Contract has been canceled
        /// </summary>
        public const string Canceled = "canceled";

        /// <summary>
        /// the Contract has been waiting
        /// </summary>
        public const string Waiting = "waiting";
    }

    /// <summary>
    /// the holdback status
    /// </summary>
    public static class HoldbackStatus
    {
        /// <summary>
        /// list of all status
        /// </summary>
        public static string[] ALL = new string[] { InProgress, Late, Recover, NotRecovered };

        /// <summary>
        /// the Holdback still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the Holdback is late
        /// </summary>
        public const string Late = "late";

        /// <summary>
        /// the hold back is recovered
        /// </summary>
        public const string Recover = "recovered";

        /// <summary>
        /// the hold back is not recovered
        /// </summary>
        public const string NotRecovered = "not_recovered";
    }

    /// <summary>
    /// the workshop status
    /// </summary>
    public static class WorkshopStatus
    {
        /// <summary>
        /// list of all status
        /// </summary>
        public static string[] ALL = new string[] { Study, Accepted, Finished, NotAccepted };

        /// <summary>
        /// the project still in the study phase
        /// </summary>
        public const string Study = "study";

        /// <summary>
        /// the work has been accepted
        /// </summary>
        public const string Accepted = "accepted";

        /// <summary>
        /// the project has been finished
        /// </summary>
        public const string Finished = "finished";

        /// <summary>
        /// the project has not been accepted
        /// </summary>
        public const string NotAccepted = "not_accepted";
    }

    /// <summary>
    /// the invoice status
    /// </summary>
    public static class InvoiceStatus
    {
        /// <summary>
        /// list of all document status
        /// </summary>
        public static string[] ALL = new string[] { Draft, InProgress, Late, Closed, Canceled };

        /// <summary>
        /// the Invoice has been drafted
        /// </summary>
        public const string Draft = "draft";

        /// <summary>
        /// the Invoice still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the Invoice is late
        /// </summary>
        public const string Late = "late";

        /// <summary>
        /// the Invoice has been closed
        /// </summary>
        public const string Closed = "closed";

        /// <summary>
        /// the Invoice has been canceled
        /// </summary>
        public const string Canceled = "canceled";
    }

    /// <summary>
    /// the Credit Note status
    /// </summary>
    public static class CreditNoteStatus
    {
        /// <summary>
        /// list of all document status
        /// </summary>
        public static string[] ALL = new string[] { Draft, InProgress, Used, Expired };

        /// <summary>
        /// the Credit Note has been drafted
        /// </summary>
        public const string Draft = "draft";

        /// <summary>
        /// the Credit Note still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the credit note has been used
        /// </summary>
        public const string Used = "used";

        /// <summary>
        /// the Credit Note is expired
        /// </summary>
        public const string Expired = "expired";
    }

    /// <summary>
    /// the Expense status
    /// </summary>
    public static class ExpenseStatus
    {
        /// <summary>
        /// list of all document status
        /// </summary>
        public static string[] ALL = new string[] { Draft, InProgress, Late, Closed, Canceled };

        /// <summary>
        /// the InvoExpenseice has been drafted
        /// </summary>
        public const string Draft = "draft";

        /// <summary>
        /// the Expense still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the Expense is late
        /// </summary>
        public const string Late = "late";

        /// <summary>
        /// the Expense has been closed
        /// </summary>
        public const string Closed = "closed";

        /// <summary>
        /// the Expense has been canceled
        /// </summary>
        public const string Canceled = "canceled";
    }

    /// <summary>
    /// status of  the quote
    /// </summary>
    public static class QuoteStatus
    {

        /// <summary>
        /// the Contract has been drafted
        /// </summary>
        public const string Draft = "draft";
        /// <summary>
        /// list of all document status
        /// </summary>
        public static string[] ALL = new string[] { InProgress, Accepted, Refused, Canceled, Billed };

        /// <summary>
        /// the quote still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the quote is accepted
        /// </summary>
        public const string Accepted = "accepted";

        /// <summary>
        /// the quote has been refused
        /// </summary>
        public const string Refused = "refused";

        /// <summary>
        /// the quote has been canceled
        /// </summary>
        public const string Canceled = "canceled";

        /// <summary>
        /// the quote has been billed
        /// </summary>
        public const string Billed = "billed";

        /// <summary>
        /// the quote has been billed
        /// </summary>
        public const string Signed = "signed";

        /// <summary>
        /// the quote is late
        /// </summary>
        public const string Late = "late";
    }

    /// <summary>
    /// Supplier Order Status
    /// </summary>
    public static class SupplierOrderStatus
    {
        /// <summary>
        /// list of all document status
        /// </summary>
        public static string[] ALL = new string[] { Draft, InProgress };

        /// <summary>
        /// the Supplier is drafted
        /// </summary>
        public const string Draft = "draft";

        /// <summary>
        /// the Supplier  been canceled
        /// </summary>

        public const string Canceled = "canceled";

        /// <summary>
        /// the Supplier Order still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the Supplier Order has been billed
        /// </summary>
        public const string Billed = "billed";
    }

    /// <summary>
    /// Operation Sheet Status
    /// </summary>
    public static class OperationSheetStatus
    {
        /// <summary>
        /// list of all document status
        /// </summary>
        public static string[] ALL = new string[] { Draft, Billed, Planned, Canceled, Realized };

        /// <summary>
        /// the Operation Sheet is drafted
        /// </summary>
        public const string Draft = "draft";

        /// <summary>
        /// the Operation Sheet has been billed
        /// </summary>
        public const string Billed = "billed";

        /// <summary>
        /// the operation sheet has been planed
        /// </summary>
        public const string Planned = "planned";

        /// <summary>
        /// the operation sheet been canceled
        /// </summary>
        public const string Canceled = "canceled";

        /// <summary>
        /// the operation sheet been realized
        /// </summary>
        public const string Realized = "realized";

        /// <summary>
        /// the operation sheet late
        /// </summary>
        public const string Late = "late";
    }

    /// <summary>
    /// status of the recurring
    /// </summary>
    public static class RecurringStatus
    {
        /// <summary>
        /// list of all status
        /// </summary>
        public static string[] ALL = new string[] { Draft, Canceled, InProgress, Finished };

        /// <summary>
        /// the recurring has been drafted
        /// </summary>
        public const string Draft = "draft";

        /// <summary>
        /// the recurring has been canceled
        /// </summary>
        public const string Canceled = "canceled";

        /// <summary>
        /// the recurring still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the recurring has finished running
        /// </summary>
        public const string Finished = "finished";
    }

    /// <summary>
    /// the Technician Task statuses
    /// </summary>
    public static class MissionStatus
    {
        /// <summary>
        /// list of all status
        /// </summary>
        public static string[] ALL = new string[] { InProgress, Finished };

        /// <summary>
        /// the Technician Task still in progress
        /// </summary>
        public const string InProgress = "in_progress";

        /// <summary>
        /// the Task is late
        /// </summary>
        public const string Late = "late";

        /// <summary>
        /// the Technician has finished the Task
        /// </summary>
        public const string Finished = "finished";
    }

    public static class StatutOperation
    {
        public const string NotFait = "NotFait";

        public const string Fait = "Fait";
    }
}
