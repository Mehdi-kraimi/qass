﻿namespace Axiobat.Domain.Constants
{
    using System.Linq;

    /*
     * Notes:
     *
     * - all values should be in lower case
     * - all properties should be constant
     * - every type should be added to the list of All
     */

    /// <summary>
    ///  type of the application setting,
    /// </summary>
    public static partial class ApplicationConfigurationType
    {
        /// <summary>
        /// the application URLs
        /// </summary>
        public const string ApplicationsURLs = "applications_urls";

        /// <summary>
        /// the email templates settings
        /// </summary>
        public const string EmailTemplates = "email_templates";

        /// <summary>
        /// list of emails used when sending an system email, used in "FROM" value
        /// </summary>
        public const string StaticEmails = "static_emails";

        /// <summary>
        /// the counter settings
        /// </summary>
        public const string Counter = "counter";

        /// <summary>
        /// the messaging settings
        /// </summary>
        public const string Messaging = "messaging";

        /// <summary>
        /// the default values configuration
        /// </summary>
        public const string DefaultValues = "default_values";

        /// <summary>
        /// the document configuration
        /// </summary>
        public const string DocumentConfiguration = "documents_configuration";

        /// <summary>
        /// the Google configuration
        /// </summary>
        public const string GoogleConfiguration = "google_configuration";

        /// <summary>
        /// the pdf options
        /// </summary>
        public const string PdfOptions = "pdf_options";

        /// <summary>
        /// the tasks types configuration
        /// </summary>
        public const string MissionTechnicianTaskTypes = "mission_technician_task_types";

        /// <summary>
        /// the calls types configuration
        /// </summary>
        public const string MissionCallsTypes = "mission_call_types";

        /// <summary>
        /// the appointment types configurations
        /// </summary>
        public const string MissionAppointmentTypes = "mission_appointment_types";
    }

    /// <summary>
    /// partial part for <see cref="ApplicationConfigurationType"/>
    /// </summary>
    public static partial class ApplicationConfigurationType
    {
        /// <summary>
        /// the list of all application types
        /// </summary>
        public static readonly string[] All = new string[]
        {
            Counter, Messaging, ApplicationsURLs, EmailTemplates, StaticEmails,
            DefaultValues, DocumentConfiguration, GoogleConfiguration, PdfOptions,
            MissionTechnicianTaskTypes, MissionCallsTypes,MissionAppointmentTypes
        };

        /// <summary>
        /// check if the given type is a valid type
        /// </summary>
        /// <param name="settingType">the setting type to be checked</param>
        /// <returns>true if valid, false if not</returns>
        public static bool IsValid(string settingType)
            => All.Contains(settingType);
    }
}
