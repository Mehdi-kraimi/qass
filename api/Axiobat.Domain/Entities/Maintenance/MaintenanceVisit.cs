﻿namespace Axiobat.Domain.Entities
{
    using Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the maintenance contract
    /// </summary>
    [DocType(DocumentType.MaintenanceVisit)]
    public partial class MaintenanceVisit : Entity<string>
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.MaintenanceVisit;

        /// <summary>
        /// status of the Visit, one of the values of <see cref="Constants.MaintenanceVisitStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the date of the visit, this will be the start of the month
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// the site where the maintenance will be performed
        /// </summary>
        public Address Site { get; set; }

        /// <summary>
        /// the Maintenance Contract associated with this visit
        /// </summary>
        public string MaintenanceContractId { get; set; }

        /// <summary>
        /// the id of the client associated with the maintenance visit
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the client associated with the maintenance visit
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the Maintenance Contract associated with this visit
        /// </summary>
        public MaintenanceContract MaintenanceContract { get; set; }

        /// <summary>
        /// the Maintenance Operation Sheet associated with this Maintenance visit
        /// </summary>
        public MaintenanceOperationSheet MaintenanceOperationSheet { get; set; }

        /// <summary>
        /// the equipments associated with this visit
        /// </summary>
        public ICollection<MaintenanceContractEquipmentDetails> EquipmentDetails { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceVisit"/>
    /// </summary>
    public partial class MaintenanceVisit
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceVisit"/>
        /// </summary>
        public MaintenanceVisit()
        {
            Id = GenerateId();
            EquipmentDetails = new HashSet<MaintenanceContractEquipmentDetails>();
        }

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Site}";

        /// <summary>
        /// get the string representation for the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"status: {Status}, in {Date.ToShortDateString()}, has {EquipmentDetails.Count} Equipments";
    }
}
