﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class defines the equipment maintenance operation
    /// </summary>
    public partial class EquipmentMaintenanceOperation
    {
        /// <summary>
        /// the id of the operation
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// name of the operation
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the maintenance operation Periodicity
        /// </summary>
        public ICollection<Month> Periodicity { get; set; }

        /// <summary>
        /// the list of sub operations associated with this maintenance operation
        /// </summary>
        public ICollection<EquipmentMaintenanceOperation> SubOperations { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="EquipmentMaintenanceOperation"/>
    /// </summary>
    public partial class EquipmentMaintenanceOperation
    {
        /// <summary>
        /// create an instant of <see cref="EquipmentMaintenanceOperation"/>
        /// </summary>
        public EquipmentMaintenanceOperation()
        {
            Id = Guid.NewGuid().ToString();
            Periodicity = new HashSet<Month>();
            SubOperations = new HashSet<EquipmentMaintenanceOperation>();
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Id: {Id}, Name: {Name}, has " + (SubOperations.Count > 0 ? $"{SubOperations.Count} sub operations" : $"{Periodicity.Count} Periodicity");

        /// <summary>
        /// check if the Equipment has any operation in the given month
        /// </summary>
        /// <param name="mount">the month to be checked</param>
        /// <returns>true if exist false if not</returns>
        public bool HasOperationIn(int mount)
        {
            // the operation doesn't have a sub operation
            if (Periodicity.Count > 0)
                return Periodicity.Any(p => ((int)p) == mount);

            // check the sub operation
            return SubOperations.Any(e => e.HasOperationIn(mount));
        }
    }
}
