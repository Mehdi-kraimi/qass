﻿namespace Axiobat.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines the Technician Observation on a equipment when
    /// performing a maintenance visit
    /// </summary>
    public partial class TechnicianObservation
    {
        /// <summary>
        /// the id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the Observation
        /// </summary>
        public string Observation { get; set; }

        /// <summary>
        /// the status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the attachment related to this observation
        /// </summary>
        public ObservationAttachment Attachment { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="TechnicianObservation"/>
    /// </summary>
    public partial class TechnicianObservation : IEquatable<TechnicianObservation>
    {
        /// <summary>
        /// create an instant of <see cref="TechnicianObservation"/>
        /// </summary>
        public TechnicianObservation()
        {

        }

        /// <summary>
        /// check if the given object equals current the instant
        /// </summary>
        /// <param name="obj">the object to compare</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj)
            => Equals(obj as TechnicianObservation);

        /// <summary>
        /// check if the given object equals current the instant
        /// </summary>
        /// <param name="other">the object to compare</param>
        /// <returns>true if equals, false if not</returns>
        public bool Equals(TechnicianObservation other)
            => other != null &&
                   Id == other.Id &&
                   Name == other.Name &&
                   Observation == other.Observation &&
                   Status == other.Status;

        /// <summary>
        /// get the hash code value of the object
        /// </summary>
        /// <returns>the hash value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = -1687721462;
                hashCode = hashCode * -1521134295 + Id.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Observation);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Status);
                return hashCode;
            }
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"{Id}, name: {Name}, status: {Status}";

        public static bool operator ==(TechnicianObservation left, TechnicianObservation right) => EqualityComparer<TechnicianObservation>.Default.Equals(left, right);
        public static bool operator !=(TechnicianObservation left, TechnicianObservation right) => !(left == right);
    }
}
