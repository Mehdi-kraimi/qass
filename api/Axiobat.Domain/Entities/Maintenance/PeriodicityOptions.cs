﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the Periodicity options for custom period selection
    /// </summary>
    public partial class PeriodicityOptions
    {
        /// <summary>
        /// the day of the week
        /// </summary>
        public IEnumerable<DayOfWeek> DaysOfWeek { get; set; }

        /// <summary>
        /// the day of the month
        /// </summary>
        public byte DayOfMonth { get; set; }

        /// <summary>
        /// Type of the Recurring
        /// </summary>
        public PeriodicityRecurringType RecurringType { get; set; }

        /// <summary>
        /// Repeat every (example Repeat every 2 weeks)
        /// </summary>
        public uint RepeatEvery { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (RecurringType == PeriodicityRecurringType.EveryDay)
            {
                return $"Run every day";
            }

            if (RecurringType == PeriodicityRecurringType.EveryMonth)
            {
                return $"Run every {RepeatEvery} months at {DayOfMonth}";
            }

            if (RecurringType == PeriodicityRecurringType.EveryWeek)
            {
                return $"Run every {RepeatEvery} week in [{DaysOfWeek?.ToJson()}]";
            }

            return "Nothing";
        }
    }
}
