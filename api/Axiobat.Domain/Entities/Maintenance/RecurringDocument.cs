﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Constants;
    using Axiobat.Domain.Exceptions;
    using Enums;
    using Newtonsoft.Json.Linq;
    using System;

    /// <summary>
    /// this class holds the information about Recurring Invoice parameters
    /// </summary>
    [DocType(DocumentType.RecurringDocument)]
    public partial class RecurringDocument
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType => DocumentType.RecurringDocument;

        /// <summary>
        /// if <see cref="PeriodicityType"/> is set to <see cref="PeriodicityType.Custom"/>
        /// this object will hold the information for defining the custom Periodicity
        /// </summary>
        public PeriodicityOptions PeriodicityOptions { get; set; }

        /// <summary>
        /// the type of the Periodicity
        /// </summary>
        public PeriodicityType PeriodicityType { get; set; }

        /// <summary>
        /// the type of the ending
        /// </summary>
        public PeriodicityEndingType EndingType { get; set; }

        /// <summary>
        /// the number of the times the Recurring should run before terminating
        /// </summary>
        public uint OccurringCount { get; set; }

        /// <summary>
        /// the ending of the recurring
        /// </summary>
        public DateTime? EndingDate { get; set; }

        /// <summary>
        /// the date of the next occurring
        /// </summary>
        public DateTime NextOccurring { get; set; }

        /// <summary>
        /// the date that the Recurring will tack effect
        /// </summary>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// how many time the recurring has occurred
        /// </summary>
        public uint Counter { get; set; }

        /// <summary>
        /// the status of the Recurring one of <see cref="Constants.RecurringStatus"/>
        /// </summary>
        public string Status { get; set; } = RecurringStatus.Draft;

        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocType { get; set; }

        /// <summary>
        /// the document that will be generated
        /// </summary>
        public JObject Document { get; set; }

        /// <summary>
        /// the error associated with this recurring if any
        /// </summary>
        public JObject Error { get; set; }

        /// <summary>
        /// MaintenanceContract associated with this recurring if any
        /// </summary>
        public MaintenanceContract MaintenanceContract { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="RecurringDocument"/>
    /// </summary>
    public partial class RecurringDocument : Entity<string>
    {
        /// <summary>
        /// create an instant of <see cref="RecurringDocument"/>
        /// </summary>
        public RecurringDocument() : base()
        {
        }

        /// <summary>
        /// check if the recurring has finished
        /// </summary>
        public bool HasFinished => CheckIfFinished();

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Status}";

        /// <summary>
        /// set the value of the <see cref="NextOccurring"/> by executing <see cref="GetNextOccurring()"/>
        /// </summary>
        public void SetNextOccurring()
            => NextOccurring = GetNextOccurring();

        /// <summary>
        /// get the next Occurring date
        /// </summary>
        /// <returns>generate the next Occurring date</returns>
        public DateTime GetNextOccurring()
        {
            switch (PeriodicityType)
            {
                case PeriodicityType.Custom:
                    return GenereteNextOccurring(PeriodicityOptions, NextOccurring);
                case PeriodicityType.BiMonthly:
                    return NextOccurring.AddMonths(2);
                case PeriodicityType.Quarterly:
                    return NextOccurring.AddMonths(3);
                case PeriodicityType.EveryFourMonths:
                    return NextOccurring.AddMonths(4);
                case PeriodicityType.Biannual:
                    return NextOccurring.AddMonths(6);
                case PeriodicityType.Annually:
                    return NextOccurring.AddYears(1);
                case PeriodicityType.Monthly:
                default:
                    return NextOccurring.AddMonths(1);
            }
        }

        private DateTime GenereteNextOccurring(PeriodicityOptions periodicityOptions, DateTime oldOccurring)
        {
            switch (periodicityOptions.RecurringType)
            {
                case PeriodicityRecurringType.EveryDay:
                    return oldOccurring.AddDays(periodicityOptions.RepeatEvery);
                case PeriodicityRecurringType.EveryMonth:
                    var date = oldOccurring.AddMonths((int)periodicityOptions.RepeatEvery);
                    int dayOfMonth = GetDayOfMonth(periodicityOptions, date);
                    return new DateTime(date.Year, date.Month, dayOfMonth, date.Hour, date.Minute, date.Second);
                case PeriodicityRecurringType.EveryWeek:
                    foreach (var dayOfWeek in periodicityOptions.DaysOfWeek)
                    {
                        var dayOfWeekDate = oldOccurring.DayOfWeekDate(dayOfWeek);
                        if (dayOfWeekDate >= oldOccurring)
                            return new DateTime(dayOfWeekDate.Year, dayOfWeekDate.Month, dayOfWeekDate.Day, oldOccurring.Hour, oldOccurring.Minute, oldOccurring.Second);
                    }

                    var nextWeekOfOldOccurring = oldOccurring.NextWeek();
                    foreach (var dayOfWeek in periodicityOptions.DaysOfWeek)
                    {
                        var dayOfWeekDate = oldOccurring.DayOfWeekDate(dayOfWeek);
                        if (dayOfWeekDate >= nextWeekOfOldOccurring)
                            return new DateTime(dayOfWeekDate.Year, dayOfWeekDate.Month, dayOfWeekDate.Day, nextWeekOfOldOccurring.Hour, nextWeekOfOldOccurring.Minute, nextWeekOfOldOccurring.Second);
                    }

                    throw new AxiobatException($"Failed to get the date of the next Occurring for RecurringDocument with Id: {Id}");
                default:
                    throw new NotSupportedException($"the given RecurringType [{periodicityOptions.RecurringType}] is not supported");
            }
        }

        private static int GetDayOfMonth(PeriodicityOptions periodicityOptions, DateTime date)
        {
            var daysOfMonth = DateTime.DaysInMonth(date.Year, date.Month);
            if (periodicityOptions.DayOfMonth > daysOfMonth)
                return daysOfMonth;

            return periodicityOptions.DayOfMonth;
        }

        private bool CheckIfFinished()
        {
            switch (EndingType)
            {
                case PeriodicityEndingType.SpecificDate: return NextOccurring >= EndingDate;
                case PeriodicityEndingType.SpecificNumberOfTime: return Counter >= OccurringCount;
                case PeriodicityEndingType.Undifined:
                default:
                    return false;
            }
        }
    }
}
