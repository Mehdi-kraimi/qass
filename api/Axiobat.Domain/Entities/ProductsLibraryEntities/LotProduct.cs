﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// defines the relationship between the <see cref="Lot"/> and <see cref="Product"/>
    /// </summary>
    public partial class LotProduct
    {
        /// <summary>
        /// the id of the product
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// the id of the lot
        /// </summary>
        public string LotId { get; set; }

        /// <summary>
        /// the quantity of the products in the Lot
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// the lot navigation property
        /// </summary>
        public Lot Lot { get; set; }

        /// <summary>
        /// the product navigation property
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Lot: {LotId}, has {Quantity} product";
    }
}
