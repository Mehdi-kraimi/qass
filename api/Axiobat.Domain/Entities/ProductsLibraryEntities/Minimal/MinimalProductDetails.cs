﻿namespace Axiobat.Domain.Entities
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// the minimal information about a <see cref="Product"/>
    /// </summary>
    public partial class MinimalProductDetails
    {
        /// <summary>
        /// the id of the product
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the product reference, should be unique
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the Designation
        /// </summary>
        public string Designation { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// name of the product
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the discount
        /// </summary>
        public Discount Discount { get; set; }

        /// <summary>
        /// the total estimated hours for the product
        /// </summary>
        public int TotalHours { get; set; }

        /// <summary>
        /// the estimated material cost that the product will consume
        /// </summary>
        public float MaterialCost { get; set; }

        /// <summary>
        /// the cost of selling the product, the price is Hourly based.
        /// </summary>
        public float HourlyCost { get; set; }

        /// <summary>
        /// the Value added tax
        /// </summary>
        public float VAT { get; set; }

        /// <summary>
        /// the total HT of the product
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the total TTC of the product
        /// </summary>
        public float TotalTTC { get; set; }

        /// <summary>
        /// the product unite
        /// </summary>
        public string Unite { get; set; }

        /// <summary>
        /// the product category type
        /// </summary>
        public ProductCategoryType ProductCategoryType { get; set; }

        /// <summary>
        /// the category of the product
        /// </summary>
        public Classification Category { get; set; }

        /// <summary>
        /// list of the product suppliers
        /// </summary>
        public ICollection<ProductSupplier> ProductSuppliers { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MinimalProductDetails"/>
    /// </summary>
    public partial class MinimalProductDetails
    {
        /// <summary>
        /// create an instant of <see cref="MinimalProductDetails"/>
        /// </summary>
        public MinimalProductDetails()
        {
            ProductSuppliers = new List<ProductSupplier>();
        }

        /// <summary>
        /// the cost of the product based on the total hours * hourly cost
        /// </summary>
        public float Cost => TotalHours * HourlyCost;

        /// <summary>
        /// Calculate the Total HT of the product
        /// </summary>
        public void CalculateTotalHT() => TotalHT = Cost + MaterialCost;

        /// <summary>
        /// the total Amount Including the tax
        /// </summary>
        public float CalculateTotalTTC() => TotalHT + TaxValue;

        /// <summary>
        /// the total VAT Tax value
        /// </summary>
        public float TaxValue => TotalHT * VAT / 100;

        /// <summary>
        /// the product cost details
        /// </summary>
        public ProductCostDetails CostDetails => new ProductCostDetails
        {
            TotalCost = Cost,
            TotalHours = TotalHours,
            TotalMaterialCost = MaterialCost,
        };

        /// <summary>
        /// get the default suppler
        /// </summary>
        public ProductSupplier DefaultSupplier => ProductSuppliers.FirstOrDefault(e => e.IsDefault);

        /// <summary>
        /// get the product price of the supplier with the given id
        /// </summary>
        /// <param name="supplierId">the id of the supplier</param>
        /// <returns>the price</returns>
        public float GetPrice(string supplierId)
            => ProductSuppliers.FirstOrDefault(e => e.SupplierId == supplierId)?.Price ?? DefaultSupplier.Price;

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"{Name}, ref: {Reference}";
    }
}
