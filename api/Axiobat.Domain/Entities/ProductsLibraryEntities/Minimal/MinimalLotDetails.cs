﻿namespace Axiobat.Domain.Entities
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// the minimal information about a <see cref="Lot"/>
    /// </summary>
    public partial class MinimalLotDetails
    {
        /// <summary>
        /// the id of the Lot
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// name of the product
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the list of products included in this Lot
        /// </summary>
        public ICollection<MinimalLotProductDetails> Products { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MinimalLotDetails"/>
    /// </summary>
    public partial class MinimalLotDetails
    {
        /// <summary>
        /// create an instant of <see cref="MinimalLotProductDetails"/>
        /// </summary>
        public MinimalLotDetails()
        {
            Products = new HashSet<MinimalLotProductDetails>();
        }

        /// <summary>
        /// the cost of the product based on the total hours * hourly cost
        /// </summary>
        public float TotalCost => Products.Sum(e => (e.ProductDetails?.Cost ?? 0) * e.Quantity);

        /// <summary>
        /// the total products Material Cost
        /// </summary>
        public float TotalMaterialCost => Products.Sum(e => (e.ProductDetails?.MaterialCost ?? 0) * e.Quantity);

        /// <summary>
        /// the total products Hours
        /// </summary>
        public int TotalHours => Products.Sum(e => (e.ProductDetails?.TotalHours ?? 0));

        /// <summary>
        /// the total Tax value of the products
        /// </summary>
        public float TaxValue => Products.Sum(e => (e.ProductDetails?.TaxValue ?? 0) * e.Quantity);

        /// <summary>
        /// the total price of the products (HT)
        /// </summary>
        public float TotalHT => TotalCost + TotalMaterialCost;

        /// <summary>
        /// the total Amount Including the tax
        /// </summary>
        public float TotalTTC => TotalHT + TaxValue;

        /// <summary>
        /// the products cost details
        /// </summary>
        public ProductCostDetails CostDetails => new ProductCostDetails
        {
            TotalCost = TotalCost,
            TotalHours = TotalHours,
            TotalMaterialCost = TotalMaterialCost,
        };
    }
}
