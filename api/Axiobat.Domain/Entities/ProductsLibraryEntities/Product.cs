﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Enums;
    using Interfaces;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class that describe an invoice
    /// </summary>
    [DocType(DocumentType.Product)]
    public partial class Product
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.Product;

        /// <summary>
        /// the product reference, should be unique
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the total estimated hours for the product
        /// </summary>
        public float TotalHours { get; set; }

        /// <summary>
        /// the estimated material cost that the product will consume
        /// </summary>
        public float MaterialCost { get; set; }

        /// <summary>
        /// the cost of selling the product, the price is Hourly based.
        /// </summary>
        public float HourlyCost { get; set; }

        /// <summary>
        /// the Value added tax
        /// </summary>
        public float VAT { get; set; }

        /// <summary>
        /// the product unite
        /// </summary>
        public string Unite { get; set; }

        /// <summary>
        /// the id of the product category type
        /// </summary>
        public string ProductCategoryTypeId { get; set; }

        /// <summary>
        /// the classification of the product
        /// </summary>
        public int ClassificationId { get; set; }

        /// <summary>
        /// the category of the product
        /// </summary>
        public Classification Classification { get; set; }

        /// <summary>
        /// the product category type
        /// </summary>
        public ProductCategoryType ProductCategoryType { get; set; }

        /// <summary>
        /// the Data sheets associated with the product
        /// </summary>
        public ICollection<Memo> Memo { get; set; }

        /// <summary>
        /// list of labels
        /// </summary>
        public ICollection<ProductLabel> Labels { get; set; }

        /// <summary>
        /// list of Lots
        /// </summary>
        public ICollection<LotProduct> Lots { get; set; }

        /// <summary>
        /// list of the product suppliers
        /// </summary>
        public ICollection<ProductSupplier> Suppliers { get; set; }
    }

    /// <summary>
    /// the partial part of <see cref="Product"/>
    /// </summary>
    public partial class Product : ProductsBase, IReferenceable<Product>
    {
        /// <summary>
        /// create a new instant of <see cref="Product"/> entity
        /// </summary>
        public Product() : base()
        {
            Memo = new HashSet<Memo>();
            Lots = new HashSet<LotProduct>();
            Labels = new HashSet<ProductLabel>();
            Suppliers = new HashSet<ProductSupplier>();
        }

        /// <summary>
        /// the cost of the product based on the total hours * hourly cost
        /// </summary>
        public float Cost => TotalHours * HourlyCost;

        /// <summary>
        /// the total price of the product, HT
        /// </summary>
        public float TotalHT => Cost + MaterialCost;

        /// <summary>
        /// the total price of the product, TAX
        /// </summary>
        public float TaxValue => TotalHT * VAT / 100;

        /// <summary>
        /// the total price of the product, TTC
        /// </summary>
        public float TotalTTC => TotalHT + TaxValue;

        /// <summary>
        /// build the object search terms
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Reference} {Designation}";

        /// <summary>
        /// return string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"{Designation}, ref: {Reference}";

        public bool CanIncrementOnCreate() => true;

        public bool CanIncrementOnUpdate(Product oldState) => false;
    }
}
