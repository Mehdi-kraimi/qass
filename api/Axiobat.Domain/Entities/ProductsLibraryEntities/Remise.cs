﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the discount class
    /// </summary>
    public partial class Remise
    {
        /// <summary>
        /// type of discount
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// the value of the discount
        /// </summary>
        public float Valeur { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Remise"/>
    /// </summary>
    public partial class Remise
    {
        /// <summary>
        /// represent the default value of the discount, with type = "$", and a value = 0
        /// </summary>
        public static Remise Default => new Remise("$", 0);

        /// <summary>
        /// create an instant of <see cref="Remise"/>
        /// </summary>
        public Remise() { }

        /// <summary>
        /// create an instant of <see cref="Remise"/>
        /// </summary>
        /// <param name="type">the type of the discount, one of "$" or "%"</param>
        /// <param name="valeur">the value of the discount</param>
        public Remise(string type, float valeur) : this()
        {
            Type = type;
            Valeur = valeur;
        }
    }
}
