﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the Google configuration
    /// </summary>
    public partial class GoogleConfiguration
    {
        /// <summary>
        /// the calendar Id
        /// </summary>
        public string CalendarId { get; set; }

      
    }
}
