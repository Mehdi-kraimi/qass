﻿using App.Common;

namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// A class that defines the messaging parameters
    /// </summary>
    public partial class MessagingSettings
    {
        /// <summary>
        /// user name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// server
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// port
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        /// SSL
        /// </summary>
        public bool SSL { get; set; }

        /// <summary>
        /// check if the messaging are empty, by checking all values if are empty or not
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public bool IsEmpty => !UserName.IsValid() && !Server.IsValid() && !Password.IsValid() && !Port.IsValid();
    }
}
