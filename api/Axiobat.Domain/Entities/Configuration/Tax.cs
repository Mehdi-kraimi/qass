﻿namespace Axiobat.Domain.Entities
{
    using App.Common;

    /// <summary>
    /// this class defines the Tax object
    /// </summary>
    public partial class Tax : Entity<string>
    {
        /// <summary>
        /// the id of the <see cref="Tax"/> entity
        /// </summary>
        public override string Id
        {
            get => _id;
            set => _id = value.IsValid() ? value : Generator.GenerateRandomId();
        }

        /// <summary>
        /// name of the Tax
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the value of the Tax
        /// </summary>
        public float Value { get; set; }

        /// <summary>
        /// the accounting code of the category
        /// </summary>
        public string AccountingCode { get; set; }

        /// <summary>
        /// if this instant is the default one
        /// </summary>
        public bool IsDefault { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Tax"/>
    /// </summary>
    public partial class Tax
    {
        private string _id;

        /// <summary>
        /// create an instant of <see cref="Tax"/>
        /// </summary>
        public Tax()
        {
            Value = 0;
            Name = string.Empty;
        }

        /// <summary>
        /// create an instant of <see cref="Tax"/>
        /// </summary>
        /// <param name="value">the value of the tax</param>
        public Tax(float value) : this()
        {
            Value = value;
        }

        /// <summary>
        /// create an instant of <see cref="Tax"/>
        /// </summary>
        /// <param name="name">the full name of the Tax</param>
        /// <param name="value">the tax value</param>
        public Tax(string name, float value) : this(value)
        {
            Name = name;
        }

        /// <summary>
        /// this function will calculate the amount of the Tax base on the given HT amount,
        /// and return the calculated value,
        /// </summary>
        /// <param name="amountHT">the HT Amount</param>
        public float Calculate(float amountHT)
            => (amountHT * Value / 100).Round();

        /// <summary>
        /// build the search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Name} {Value}";

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"name: {Name}, value {Value}%";

        /// <summary>
        /// check if the given tax equals the current instant,
        /// two <see cref="Tax"/> are equals if they have the same <see cref="Value"/> and <see cref="Name"/>
        /// </summary>
        /// <param name="other">the <see cref="Tax"/> to compare</param>
        /// <returns>true if equals false if not</returns>
        public bool Equals(Tax other)
            => other is null ? false : Value.Equals(other.Value) && Name.Equals(other.Name) ? true : false;

        /// <summary>
        /// check if the given object equals the current instant
        /// </summary>
        /// <param name="obj">the object to check</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Tax)) return false;
            if (ReferenceEquals(obj, this)) return true;
            return Equals(obj as Tax);
        }

        /// <summary>
        /// get the hasCode of the current instant
        /// </summary>
        /// <returns>the has value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ (int)Value;
                return (hashCode * 397) ^ (Name.IsValid() ? Name.GetHashCode() : 0);
            }
        }
    }
}
