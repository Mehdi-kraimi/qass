﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class that defines the Financial accounts
    /// </summary>
    public partial class FinancialAccount
    {
        /// <summary>
        /// type of the account
        /// </summary>
        public FinancialAccountsType Type { get; set; }

        /// <summary>
        /// the account label
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// the accounting code associated with the financial account
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// is this account the default one
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// the list of the payments associated with this account
        /// </summary>
        public ICollection<Payment> Payments { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="FinancialAccount"/>
    /// </summary>
    public partial class FinancialAccount : Entity<int>
    {
        public FinancialAccount()
        {
            Payments = new HashSet<Payment>();
        }

        /// <summary>
        /// build the search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Label} {Code}";

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Label: {Label}, code: {Code}, type: {Type}";
    }
}
