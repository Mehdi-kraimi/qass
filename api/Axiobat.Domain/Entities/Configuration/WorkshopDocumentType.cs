﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using Enums;
    using System.Collections.Generic;

    /// <summary>
    /// a class that defines the <see cref="ConstructionWorkshop"/> document types
    /// </summary>
    public partial class WorkshopDocumentType
    {
        /// <summary>
        /// the id of the <see cref="Label"/> entity
        /// </summary>
        public override string Id
        {
            get => _id;
            set => _id = value.IsValid() ? value : Generator.GenerateRandomId();
        }

        /// <summary>
        /// the document type label
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// a flag to set this type as default
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// some of the document types have a special affect on the workshop status,
        /// this field define what type of the status that will be affected by this type, if null it will be neutral
        /// </summary>
        public string AffectedStatus { get; set; }

        /// <summary>
        /// the new status that the workshop will get id this type is attached to the workshop document
        /// </summary>
        public string NewStatus { get; set; }

        /// <summary>
        /// list of document associated with this type
        /// </summary>
        public IEnumerable<WorkshopDocumentsTypes> Documents { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="WorkshopDocumentType"/>
    /// </summary>
    public partial class WorkshopDocumentType : Entity<string>
    {
        private string _id;

        /// <summary>
        /// create an instant of <see cref="WorkshopDocumentType"/>
        /// </summary>
        public WorkshopDocumentType()
        {

        }

        /// <summary>
        /// build the search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Value}";

        /// <summary>
        /// build the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"id: {Id}, label: {Value}, isDefault: {IsDefault}";
    }
}
