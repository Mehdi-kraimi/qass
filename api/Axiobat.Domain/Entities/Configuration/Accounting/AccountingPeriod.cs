﻿namespace Axiobat.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class the defines the accounting periods
    /// </summary>
    public partial class AccountingPeriod
    {
        /// <summary>
        /// the starting date of the accounting period
        /// </summary>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// the ending date of the accounting period
        /// </summary>
        public DateTime? EndingDate { get; set; }

        /// <summary>
        /// the accounting period
        /// </summary>
        public int Period { get; set; }

        /// <summary>
        /// the id of the user who set the closing date
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// the user who set the closing date
        /// </summary>
        public User User { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="AccountingPeriod"/>
    /// </summary>
    public partial class AccountingPeriod : Entity<int>, IEquatable<AccountingPeriod>
    {
        /// <summary>
        /// this function will check if the given date is in this accounting period
        /// </summary>
        /// <param name="date">the date to be checked</param>
        /// <returns>true if in the accounting period, false if not</returns>
        public bool InAccountingPeriod(DateTime date)
        {
            var endDate = CalculateEndingDate();
            if (EndingDate.HasValue)
                endDate = (DateTime)EndingDate;

            return StartingDate <= date && date < endDate;
        }

        /// <summary>
        /// calculate the ending date
        /// </summary>
        /// <returns>the ending date value</returns>
        public DateTime CalculateEndingDate() => StartingDate.AddMonths(Period);

        /// <summary>
        /// build the search terms of the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{StartingDate.ToShortDateString()} {Period}";

        /// <summary>
        /// get the string representation of the entity
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"id: {Id}, start at: {StartingDate.ToShortDateString()}, last for {Period}" + (EndingDate.HasValue ? $" ended at {EndingDate?.ToShortDateString()}" : "");

        /// <summary>
        /// check if the given object equals the current instant
        /// </summary>
        /// <param name="obj">the object instant</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (!(obj is AccountingPeriod accountingPeriod)) return false;
            return Equals(accountingPeriod);
        }

        /// <summary>
        /// check if the given object equals the current instant
        /// </summary>
        /// <param name="other">the object instant</param>
        /// <returns>true if equals, false if not</returns>
        public bool Equals(AccountingPeriod other)
        {
            return
                !(other is null) &&
                Id.Equals(other.Id) &&
                StartingDate.Equals(other.StartingDate) &&
                EndingDate.Equals(other.EndingDate) &&
                Period.Equals(other.Period);
        }

        /// <summary>
        /// get has code of the object
        /// </summary>
        /// <returns>the generated hash code</returns>
        public override int GetHashCode()
        {
            var hashCode = 1481384219;
            hashCode = hashCode * -1521134295 + StartingDate.GetHashCode();
            hashCode = hashCode * -1521134295 + EndingDate.GetHashCode();
            hashCode = hashCode * -1521134295 + Period.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(AccountingPeriod left, AccountingPeriod right)
            => EqualityComparer<AccountingPeriod>.Default.Equals(left, right);
        public static bool operator !=(AccountingPeriod left, AccountingPeriod right)
            => !(left == right);
    }
}
