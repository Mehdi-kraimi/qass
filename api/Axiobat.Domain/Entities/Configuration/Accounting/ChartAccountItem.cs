﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent a category of a chart account
    /// </summary>
    public partial class ChartAccountItem
    {
        /// <summary>
        /// the id of the <see cref="ChartAccountItem"/> entity
        /// </summary>
        public override string Id
        {
            get => _id;
            set => _id = value.IsValid() ? value : Generator.GenerateRandomGuidId();
        }

        /// <summary>
        /// the type that defines the type of the chart of account
        /// </summary>
        public ChartAccountType Type { get; set; }

        /// <summary>
        /// the type of the chart of account item
        /// </summary>
        public CategoryType CategoryType { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// the accounting code associated with this category
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// a description of the category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the value of the tax
        /// </summary>
        public float VATValue { get; set; }

        /// <summary>
        /// the id of the parent category
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// the list of sub categories owned by this category
        /// </summary>
        public ICollection<ChartAccountItem> SubCategories { get; set; }

        /// <summary>
        /// list of classification associated with this chart of account
        /// </summary>
        public IEnumerable<Classification> Classifications { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ChartAccountItem"/>
    /// </summary>
    public partial class ChartAccountItem : Entity<string>, System.IEquatable<ChartAccountItem>
    {
        private IDictionary<string, ChartAccountItem> _chartOfAccounts;

        private string _id;

        /// <summary>
        /// create an instant of <see cref="ChartAccountItem"/>
        /// </summary>
        public ChartAccountItem() : base()
        {
            SubCategories = new HashSet<ChartAccountItem>();
            Classifications = new HashSet<Classification>();
        }

        /// <summary>
        /// get the plan account with the given id
        /// </summary>
        /// <param name="planId">the id of the plan</param>
        /// <returns>the <see cref="AccountingPlanAccount"/> instant</returns>
        public ChartAccountItem this[string subAccountId]
        {
            get
            {
                if (_chartOfAccounts is null)
                    _chartOfAccounts = SubCategories.ToDictionary(e => e.Id);

                return _chartOfAccounts[subAccountId];
            }
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"id: {Id}, label: {Label}, type: {Type}";

        /// <summary>
        /// build the search term of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Label} {Code} {VATValue}";

        /// <summary>
        /// check if the given object equals the c
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(ChartAccountItem other)
            => !(other is null) && other.Id == other.Id &&
                    other.Label == Label &&
                    other.Code == Code &&
                    other.ParentId == ParentId;

        /// <summary>
        /// check if the given object equals the c
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(ChartAccountItem)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as ChartAccountItem);
        }

        /// <summary>
        /// get the hash code value of the entity
        /// </summary>
        /// <returns>the has value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = -1880813789;
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Id);
                hashCode = hashCode * -1521134295 + Type.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Label);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Code);
                hashCode = hashCode * -1521134295 + VATValue.GetHashCode();
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ParentId);
                return hashCode;
            }
        }

        /// <summary>
        /// get the list of subcategories ids
        /// </summary>
        /// <returns>list of ids</returns>
        public string[] SubCategoriesIds()
            => SubCategories.Select(e => e.Id).ToArray();

        public static bool operator ==(ChartAccountItem left, ChartAccountItem right)
            => EqualityComparer<ChartAccountItem>.Default.Equals(left, right);

        public static bool operator !=(ChartAccountItem left, ChartAccountItem right)
            => !(left == right);
    }

    /// <summary>
    /// default values
    /// </summary>
    public partial class ChartAccountItem
    {
        /// <summary>
        /// the default account code
        /// </summary>
        public const string Default = "17";

        /// <summary>
        /// the account code for clients
        /// </summary>
        public const string Clients = "11";

        /// <summary>
        /// the account code for avoir
        /// </summary>
        public const string CreditNote = "13";

        /// <summary>
        /// the account code for suppliers
        /// </summary>
        public const string Suppliers = "12";

        /// <summary>
        /// the id of the internal transfers
        /// </summary>
        public const string InternalTransfers = "16";

        /// <summary>
        /// the id of the Bank code
        /// </summary>
        public const string Bank = "15";

        /// <summary>
        /// the id of the Cash code
        /// </summary>
        public const string Cash = "14";

        /// <summary>
        /// the account code for default TAX
        /// </summary>
        public const string DefaultTax = "110";

        /// <summary>
        /// the account code for default TAX
        /// </summary>
        public const string DeductibleTax = "111";

        /// <summary>
        /// the chart account id for assets tax
        /// </summary>
        public const string AssetsTax = "112";
    }
}
