﻿namespace Axiobat.Domain.Entities
{
    using Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the roles for users, a user can have multiple roles, and a role can be assigned to many users
    /// </summary>
    public partial class Role
    {
        /// <summary>
        /// Gets or sets the name for this role.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the normalized name for this role.
        /// </summary>
        public virtual string NormalizedName { get; set; }

        /// <summary>
        /// A random value that should change whenever a role is persisted to the store
        /// </summary>
        public virtual string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

        /// <summary>
        /// type of the role
        /// </summary>
        public RoleType Type { get; set; }

        /// <summary>
        /// list of permissions associated with this role, the list is of type int[]
        /// </summary>
        public ICollection<Permissions> Permissions { get; set; }

        /// <summary>
        /// the users they own this role
        /// </summary>
        public ICollection<User> Users { get; set; }

        /// <summary>
        /// is Role a system defined role
        /// </summary>
        /// <returns></returns>
        public bool IsSystemRole()
            => Type == RoleType.SuperAdminRole ||
               Type == RoleType.Technician ||
               Type == RoleType.Manager ||
               Type == RoleType.Client ||
               Type == RoleType.MaintenanceTechnician ||
               Type == RoleType.WorkshopTechnician;
    }

    /// <summary>
    /// a partial class for the role class
    /// </summary>
    public partial class Role : Entity<Guid>
    {
        /// <summary>
        /// the id of the demo role
        /// </summary>
        public static Guid DemoRole => new Guid("274A9C13-7D5E-40BF-90A8-141F3850D1A1");

        /// <summary>
        /// the id of the supper admin role
        /// </summary>
        public static Guid SuperAdminRole => new Guid("E0F66E8A-0DFA-45FF-A713-406A40DC881D");

        /// <summary>
        /// Initializes a new instance of <see cref="IdentityRole{TKey}"/>.
        /// </summary>
        public Role()
        {
            Id = Guid.NewGuid();
            Permissions = new HashSet<Permissions>();
            Users = new HashSet<User>();
        }

        /// <summary>
        /// Initializes a new instance of <see cref="IdentityRole{TKey}"/>.
        /// </summary>
        /// <param name="roleName">The role name.</param>
        public Role(string roleName) : this()
        {
            Name = roleName;
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Name}";

        /// <summary>
        /// build the search term for querying
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Name}".ToLower();
    }
}
