﻿namespace Axiobat.Domain.Entities
{
    using System;

    /// <summary>
    /// Represents a login and its associated provider for a user.
    /// </summary>
    public class UserLogin
    {
        /// <summary>
        /// Gets or sets the login provider for the login (e.g. facebook, google)
        /// </summary>
        public virtual string LoginProvider { get; set; }

        /// <summary>
        /// Gets or sets the unique provider identifier for this login.
        /// </summary>
        public virtual string ProviderKey { get; set; }

        /// <summary>
        /// Gets or sets the friendly name used in a UI for this login.
        /// </summary>
        public virtual string ProviderDisplayName { get; set; }

        /// <summary>
        /// Gets or sets the primary key of the user associated with this login.
        /// </summary>
        public virtual Guid UserId { get; set; }

        /// <summary>
        /// the user who own this Login
        /// </summary>
        public User User { get; set; }
    }
}
