﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using Attributes;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent the Role Access
    /// each role will have a an access to certain functionalities
    /// </summary>
    public partial class Permission
    {
        /// <summary>
        /// permission id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// name of the role access
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// a short description to define the Permission
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the id of the module
        /// </summary>
        public AppModules Module { get; set; }

        /// <summary>
        /// type of the permission
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public PermissionType Type { get; set; }

        /// <summary>
        /// the permission instant
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public Permissions Instant => (Permissions)Id;
    }

    /// <summary>
    /// partial class for <see cref="Permission"/>
    /// </summary>
    public partial class Permission
    {
        /// <summary>
        /// an empty list of Permissions
        /// </summary>
        public static string Empty = "[]";

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value of the object</returns>
        public override string ToString()
            => $"id: {Id}, Name: {Name}";

        /// <summary>
        /// get the <see cref="Permission"/> instant for the given permission
        /// </summary>
        /// <param name="permissions">the permission</param>
        /// <returns>the permission instant</returns>
        public static Permission ToPermission(Permissions permissions)
            => permissions.GetAttribute<PermissionDescriptorAttribute>()?.Permission;

        /// <summary>
        /// get the <see cref="Permission"/> instant for the given permission
        /// </summary>
        /// <param name="permissions">the permission</param>
        /// <returns>the permission instant</returns>
        public static IEnumerable<Permission> ToPermission(IEnumerable<Permissions> permissions)
            => permissions is null
                ? Enumerable.Empty<Permission>()
                : permissions.Select(p => ToPermission(p)).Where(p => !(p is null));

        /// <summary>
        /// pack list of permissions into a string
        /// </summary>
        /// <param name="permissions">the list of permissions</param>
        /// <returns>the packed list</returns>
        public static string Pack(IEnumerable<Permissions> permissions)
            => permissions.Select(p => (int)p).ToList().ToJson();

        /// <summary>
        /// unpack list of permissions from the given packed permissions string
        /// </summary>
        /// <param name="packedPermissions">the packed permissions</param>
        /// <returns>list of <see cref="Permissions"/></returns>
        public static IEnumerable<Permissions> Unpack(string packedPermissions)
            => !packedPermissions.IsValid() ? Enumerable.Empty<Permissions>()
               : packedPermissions.FromJson<ICollection<int>>().Select(p => (Permissions)p);

        /// <summary>
        /// unpack list of permissions from the given packed permissions string
        /// </summary>
        /// <param name="packedPermissions">the packed permissions</param>
        /// <returns>list of <see cref="Permission"/></returns>
        public static IEnumerable<Permission> UnpackToPermission(string packedPermissions)
            => ToPermission(packedPermissions.FromJson<ICollection<int>>().Select(p => (Permissions)p));

        /// <summary>
        /// find permission by name
        /// </summary>
        /// <param name="permissionName">name of the permission</param>
        /// <returns>the <see cref="Permissions"/> if any</returns>
        public static Permissions? FindbyName(string permissionName)
            => Enum.TryParse(permissionName, out Permissions permission)
                ? (Permissions?)permission
                : null;

        /// <summary>
        /// find permission by value
        /// </summary>
        /// <param name="permissionValue">value of the permission</param>
        /// <returns>the <see cref="Permissions"/> if any</returns>
        public static Permissions? FindByValue(int permissionValue)
            => Enum.GetValues(typeof(Permissions))
                .Cast<Permissions>().ToList().Select(p => (int)p)
                .Contains(permissionValue)
                    ? (Permissions?)permissionValue
                    : null;

        /// <summary>
        /// check if the packed permissions contains the given permission
        /// </summary>
        /// <param name="packedPermissions">the packed permissions</param>
        /// <param name="permissions">the permission to check</param>
        /// <returns>true if exist, false if not</returns>
        public static bool HasPermission(string packedPermissions, params Permissions[] permissions)
            => HasPermission(Unpack(packedPermissions), permissions);

        /// <summary>
        /// check if the packed permissions contains the given permission
        /// </summary>
        /// <param name="packedPermissions">the packed permissions</param>
        /// <param name="permissions">the permission to check</param>
        /// <returns>true if exist, false if not</returns>
        public static bool HasPermission(IEnumerable<Permissions> userPermissions, params Permissions[] permissions)
        {
            if (userPermissions.Count() <= 0)
                return false;

            return permissions.Intersect(userPermissions).Count() == permissions.Length;
        }
    }
}
