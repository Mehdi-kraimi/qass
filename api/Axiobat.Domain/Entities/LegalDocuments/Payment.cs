﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// this class represent a Payment
    /// </summary>
    [DocType(DocumentType.Payment)]
    public partial class Payment
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.Payment;

        /// <summary>
        /// a description associated wit the payment
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the type of the payment operation
        /// </summary>
        public PaymentOperation Operation { get; set; }

        /// <summary>
        /// the type of this payment
        /// </summary>
        public PaymentType Type { get; set; }

        /// <summary>
        /// the date of the payment
        /// </summary>
        public DateTime DatePayment { get; set; }

        /// <summary>
        /// the payment amount
        /// </summary>
        public double Amount => GetAmount();

        /// <summary>
        /// the transfer amount, only if the type is <see cref="PaymentType.Transfer"/>
        /// </summary>
        public double TransferAmount { get; set; }

        /// <summary>
        /// the id of the account
        /// </summary>
        public int? AccountId { get; set; }

        /// <summary>
        /// the id of the payment method
        /// </summary>
        public int? PaymentMethodId { get; set; }

        /// <summary>
        /// the if of the credit note
        /// </summary>
        public string CreditNoteId { get; set; }

        /// <summary>
        /// mark an entity as deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// the account that the payment has been made with it
        /// </summary>
        public FinancialAccount Account { get; set; }

        /// <summary>
        /// the payment method associated with payment
        /// </summary>
        public PaymentMethod PaymentMethod { get; set; }

        /// <summary>
        /// the credit note associated with this payment
        /// </summary>
        public CreditNote CreditNote { get; set; }

        /// <summary>
        /// the list of changes history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// list of invoices associated with this payment
        /// </summary>
        public ICollection<Invoices_Payments> Invoices { get; set; }

        /// <summary>
        /// list of expenses associated with this payment
        /// </summary>
        public ICollection<Expenses_Payments> Expenses { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Payment"/>
    /// </summary>
    public partial class Payment : Entity<string>, IRecordable, IDeletable
    {
        /// <summary>
        /// create an instant of <see cref="Payment"/>
        /// </summary>
        public Payment()
        {
            Id = GenerateId();
            Invoices = new HashSet<Invoices_Payments>();
            Expenses = new HashSet<Expenses_Payments>();
            ChangesHistory = new List<ChangesHistory>();
        }

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);

        /// <summary>
        /// create search terms of the object
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Description}";

        /// <summary>
        /// calculate the amount and return the value
        /// </summary>
        /// <returns>the amount value</returns>
        private double GetAmount()
        {
            switch (Type)
            {
                case PaymentType.Expense:
                    return Expenses.Sum(e => e.Amount);
                case PaymentType.Invoice:
                    return Invoices.Sum(e => e.Amount);
                case PaymentType.Transfer:
                    return TransferAmount;
                default:
                    return 0;
            }
        }
    }
}
