﻿namespace Axiobat.Domain.Entities
{
    using System;

    /// <summary>
    /// this class used to get the informations about the products
    /// </summary>
    public partial class ProductsDetails
    {
        /// <summary>
        /// the quantity of the product in the order, if the product is inside a Lot the Quantity will be the Lot Quantity * order Quantity
        /// </summary>
        public float Quantity { get; set; } = 1;

        /// <summary>
        /// the product instant
        /// </summary>
        public MinimalProductDetails Product { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="ProductsDetails"/>
    /// </summary>
    public partial class ProductsDetails
    {
        /// <summary>
        /// create an instant of <see cref="ProductsDetails"/>
        /// </summary>
        /// <param name="quantity">the product quantity</param>
        /// <param name="product">the product instant</param>
        public ProductsDetails(float quantity, MinimalProductDetails product)
        {
            Quantity = quantity == 0 ? 1 : quantity;
            Product = product ?? throw new ArgumentNullException(nameof(product));
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"product: {Product.Reference}, Quantity: {Quantity}";
    }
}
