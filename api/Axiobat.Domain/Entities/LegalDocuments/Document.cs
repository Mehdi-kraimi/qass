﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Constants;
    using Enums;
    using Interfaces;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines a document
    /// </summary>
    public partial class Document
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.Undefined;

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the document
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// a note about the quote
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the conditions of the payment
        /// </summary>
        public string PaymentCondition { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// mark an entity as deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// the work shop associated with this Quote if any
        /// </summary>
        public ConstructionWorkshop Workshop { get; set; }

        /// <summary>
        /// entity changes history
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Document"/>
    /// </summary>
    public partial class Document : Entity<string>, IRecordable, IDeletable
    {
        /// <summary>
        /// create an instant of <see cref="Document"/>
        /// </summary>
        public Document()
        {
            Id = GenerateId();
            ChangesHistory = new List<ChangesHistory>();
        }

        /// <summary>
        /// build the search terms for the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Purpose} {Reference}".ToLower();

        /// <summary>
        /// build the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"reference: {Reference}";

        /// <summary>
        /// generate an id for the document
        /// </summary>
        /// <returns>the new generated id</returns>
        public string GenerateId() => EntityId.Generate(DocumentType);

        public bool CanIncrementOnCreate() => Status != QuoteStatus.Draft;

        public bool CanIncrementOnUpdate(Document oldState) => oldState.Status == QuoteStatus.Draft && Status == QuoteStatus.InProgress;
    }
}
