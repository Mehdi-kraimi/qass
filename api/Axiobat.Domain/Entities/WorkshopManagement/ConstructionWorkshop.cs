﻿namespace Axiobat.Domain.Entities
{
    using Enums;
    using Interfaces;
    using System.Collections.Generic;

    /// <summary>
    /// this class defines a construction workshop
    /// </summary>
    [DocType(DocumentType.Workshop)]
    public partial class ConstructionWorkshop : Entity<string>
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public virtual DocumentType DocumentType => DocumentType.Workshop;

        /// <summary>
        /// mark an entity as deleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// name of the construction workshop
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// a description of the construction workshop
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// a Comment about the construction workshop
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the construction workshop status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the total hours spent on this workshop
        /// </summary>
        public int TotalHours { get; set; }

        /// <summary>
        /// the total amount spent on the workshop
        /// </summary>
        public float Amount { get; set; }

        /// <summary>
        /// the Progress Rate on the workshop
        /// </summary>
        public int ProgressRate { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the client that owns this workshop
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// list of history changes on the entity
        /// </summary>
        public List<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// the Construction Workshop Documentation
        /// </summary>
        public ICollection<WorkshopDocument> Documents { get; set; }

        /// <summary>
        /// list of <see cref="Quote"/> owned by this workshop
        /// </summary>
        public ICollection<Quote> Quotes { get; set; }

        /// <summary>
        /// the list of operation sheets associated with this workshop
        /// </summary>
        public ICollection<OperationSheet> OperationSheets { get; set; }

        /// <summary>
        /// the list of invoices associated with this workshop
        /// </summary>
        public ICollection<Invoice> Invoices { get; set; }

        /// <summary>
        /// the list of expenses associated with this workshop
        /// </summary>
        public ICollection<Expense> Expenses { get; set; }

        /// <summary>
        /// the list of supplier orders
        /// </summary>
        public ICollection<SupplierOrder> SupplierOrders { get; set; }

        /// <summary>
        /// list of credit notes associated with this workshop
        /// </summary>
        public ICollection<CreditNote> CreditNotes { get; set; }

        /// <summary>
        /// the list of Rubrics associated to this workshop
        /// </summary>
        public ICollection<WorkshopDocumentRubric> Rubrics { get; set; }

        /// <summary>
        /// the list of missions associated with the workshop
        /// </summary>
        public ICollection<Mission> Missions { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ConstructionWorkshop"/>
    /// </summary>
    public partial class ConstructionWorkshop : IRecordable, IDeletable
    {
        /// <summary>
        /// create an instant of <see cref="ConstructionWorkshop"/>
        /// </summary>
        public ConstructionWorkshop() : base()
        {
            Quotes = new HashSet<Quote>();
            Invoices = new HashSet<Invoice>();
            Missions = new HashSet<Mission>();
            CreditNotes = new HashSet<CreditNote>();
            ChangesHistory = new List<ChangesHistory>();
            Documents = new HashSet<WorkshopDocument>();
            SupplierOrders = new HashSet<SupplierOrder>();
            OperationSheets = new HashSet<OperationSheet>();
            Rubrics = new HashSet<WorkshopDocumentRubric>();
            Expenses = new HashSet<Expense>();
        }

        /// <summary>
        /// create the search query for the entity
        /// </summary>
        public override void BuildSearchTerms()
            => SearchTerms = $"{Name}";
    }
}
