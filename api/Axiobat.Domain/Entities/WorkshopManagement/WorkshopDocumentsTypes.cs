﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// this class defines the relationship between a <see cref="WorkshopDocument"/> and a <see cref="WorkshopDocumentsTypes"/>
    /// </summary>
    public partial class WorkshopDocumentsTypes
    {
        /// <summary>
        /// the id of the workshop document
        /// </summary>
        public string TypeId { get; set; }

        /// <summary>
        /// the id of the workshop document
        /// </summary>
        public string DocumentId { get; set; }

        /// <summary>
        /// the workshop document instant
        /// </summary>
        public WorkshopDocument Document { get; set; }

        /// <summary>
        /// the workshop document type instant
        /// </summary>
        public WorkshopDocumentType Type { get; set; }
    }
}
