﻿namespace Axiobat.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// a class that describe a memo
    /// </summary>
    public partial class Memo
    {
        /// <summary>
        /// the id of the <see cref="Memo"/>
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the comment associated with the Memo
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the date of the memo
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// the user that has added this memo
        /// </summary>
        public MinimalUser User { get; set; }

        /// <summary>
        /// the list of Attachments
        /// </summary>
        public ICollection<Attachment> Attachments { get; set; }
    }

    /// <summary>
    /// the partial part of <see cref="Memo"/>
    /// </summary>
    public partial class Memo
    {
        /// <summary>
        /// the default constructor
        /// </summary>
        public Memo()
        {
            Attachments = new HashSet<Attachment>();
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"user: {User.ToString()}, Attachments count: {Attachments.Count}";
    }
}
