﻿namespace Axiobat.Domain.Entities
{
    using App.Common;
    using Newtonsoft.Json;

    /// <summary>
    /// a class that defines an Attachment
    /// </summary>
    public partial class Attachment
    {
        /// <summary>
        /// the id of the file
        /// </summary>
        [JsonProperty]
        public string FileId { get; private set; }

        /// <summary>
        /// the original name of the file including the extension
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// the mime-type of the file
        /// </summary>
        public string FileType { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Attachment"/>
    /// </summary>
    public partial class Attachment
    {
        /// <summary>
        /// create an instant of <see cref="Attachment"/>
        /// </summary>
        [JsonConstructor]
        protected Attachment() {}

        /// <summary>
        /// create an instant of <see cref="Attachment"/>
        /// </summary>
        /// <param name="fileId">the id of the file</param>
        /// <param name="fileName">the name of the file</param>
        /// <param name="fileType">the type of the file</param>
        public Attachment(string fileId, string fileName, string fileType) : this()
        {
            FileId = fileId;
            FileName = fileName;
            FileType = fileType;
        }

        /// <summary>
        /// create an instant of <see cref="Attachment"/>
        /// </summary>
        /// <param name="fileName">the name of the file</param>
        /// <param name="fileType">the type of the file</param>
        public Attachment(string fileName, string fileType)
            : this(Generator.GenerateRandomGuidId(), fileName, fileType) { }

        /// <summary>
        /// create an instant of <see cref="Attachment"/>
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"fileId: {FileId}, name: {FileName}, type: {FileType}";
    }
}
