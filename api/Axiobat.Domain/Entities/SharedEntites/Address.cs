﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// a class that defines an address
    /// </summary>
    public partial class Address
    {
        /// <summary>
        /// the address designation
        /// </summary>
        public string Designation { get; set; }

        /// <summary>
        /// the address department
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// the street address
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// the complement address
        /// </summary>
        public string Complement { get; set; }

        /// <summary>
        /// the city
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// the Zip code
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// the code of country
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// is this address the default one
        /// </summary>
        public bool IsDefault { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="Address"/>
    /// </summary>
    public partial class Address
    {
        /// <summary>
        /// create an instant of <see cref="Address"/>
        /// </summary>
        public Address()
        {

        }

        /// <summary>
        /// create an instant of <see cref="Address"/>
        /// </summary>
        /// <param name="street"></param>
        /// <param name="complement"></param>
        /// <param name="city"></param>
        /// <param name="postalCode"></param>
        /// <param name="country"></param>
        public Address(string street, string complement, string city, string postalCode, string country)
        {
            City = city;
            Street = street;
            CountryCode = country;
            Complement = complement;
            PostalCode = postalCode;
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{CountryCode} {City} {Street} {Complement} {PostalCode}";
    }
}
