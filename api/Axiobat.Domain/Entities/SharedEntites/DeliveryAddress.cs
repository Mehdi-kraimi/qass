﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the Shipping address
    /// </summary>
    public partial class DeliveryAddress : Address
    {
        /// <summary>
        /// the name of the address
        /// </summary>
        public string Nom { get; set; }

        /// <summary>
        /// a flag to determent if the address is the default address
        /// </summary>
        public bool Default { get; set; }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"{Nom}, is default address : {Default}";
    }
}
