﻿namespace Axiobat.Domain.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// the email data details of the sent email
    /// </summary>
    public class DocumentEmailData
    {
        /// <summary>
        /// the email who this email has been sent to it
        /// </summary>
        public ICollection<string> To { get; set; }

        /// <summary>
        /// email object
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// email content
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// the attachment associated with this email
        /// </summary>
        public string Attachments { get; set; }
    }
}
