﻿namespace Axiobat.Domain.Entities
{
    using Domain.Enums;

    /// <summary>
    /// this class represent a Discount
    /// </summary>
    public partial class Discount
    {
        /// <summary>
        /// the value of the discount
        /// </summary>
        public float Value { get; set; }

        /// <summary>
        /// the type of the discount
        /// </summary>
        public TypeValue Type { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="Discount"/>
    /// </summary>
    public partial class Discount : System.IEquatable<Discount>
    {
        /// <summary>
        /// create an instant of <see cref="Discount"/>
        /// </summary>
        public Discount() { }

        /// <summary>
        /// create an instant of <see cref="Discount"/>
        /// </summary>
        /// <param name="value">Discount value</param>
        /// <param name="type">Discount type</param>
        public Discount(float value, TypeValue type)
        {
            Value = value;
            Type = type;
        }

        /// <summary>
        /// this function will calculate the amount of the Discount base on the given amount,
        /// and return the calculated value,
        /// </summary>
        /// <param name="amountHT">the HT Amount</param>
        public float Calculate(float amount)
        {
            if (Type == TypeValue.None)
                return 0;

            if (Type == TypeValue.Amount)
                return Value;

            return amount * Value / 100;
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"value {Value}{(Type == TypeValue.Amount ? "$" : "%")}";

        /// <summary>
        /// check if the given tax equals the current instant,
        /// two <see cref="Discount"/> are equals if they have the same <see cref="Value"/> and <see cref="Name"/>
        /// </summary>
        /// <param name="other">the <see cref="Discount"/> to compare</param>
        /// <returns>true if equals false if not</returns>
        public bool Equals(Discount other)
            => other is null ? false : Value.Equals(other.Value) && Type.Equals(other.Type) ? true : false;

        /// <summary>
        /// check if the given object equals the current instant
        /// </summary>
        /// <param name="obj">the object to check</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Discount)) return false;
            if (ReferenceEquals(obj, this)) return true;
            return Equals(obj as Discount);
        }

        /// <summary>
        /// get the hasCode of the current instant
        /// </summary>
        /// <returns>the has value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ (int)Value;
                return (hashCode * 397) ^ Type.GetHashCode();
            }
        }

        /// <summary>
        /// extract the value of the discount base on it type of it
        /// from the given amount, if the discount is null or the amount is less than or equals to 0
        /// the amount will be returned
        /// </summary>
        /// <param name="discount">the discount object</param>
        /// <param name="amount">the amount to extract the discount from it</param>
        /// <returns>the value</returns>
        public static float operator -(float amount, Discount discount)
        {
            if (amount <= 0 || discount is null)
                return amount;

            return amount - discount.Calculate(amount);
        }
    }
}
