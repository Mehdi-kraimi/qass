﻿namespace Axiobat.Domain.Entities
{
    using Domain.Enums;

    /// <summary>
    /// this class represent a commission
    /// </summary>
    public partial class Commission
    {
        /// <summary>
        /// the commission value to be paid for the partner
        /// </summary>
        public float Value { get; set; }

        /// <summary>
        /// the type of the Commission
        /// </summary>
        public TypeValue Type { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="Commission"/>
    /// </summary>
    public partial class Commission : System.IEquatable<Commission>
    {
        /// <summary>
        /// create an instant of <see cref="Commission"/>
        /// </summary>
        public Commission() { }

        /// <summary>
        /// create an instant of <see cref="Commission"/>
        /// </summary>
        /// <param name="value">Commission value</param>
        /// <param name="type">Commission type</param>
        public Commission(float value, TypeValue type)
        {
            Value = value;
            Type = type;
        }

        /// <summary>
        /// this function will calculate the amount of the Commission base on the given amount,
        /// and return the calculated value,
        /// </summary>
        /// <param name="amountHT">the HT Amount</param>
        public float Calculate(float amount)
        {
            if (Type == TypeValue.None)
                return 0;

            if (Type == TypeValue.Amount)
                return Value;

            return amount * Value / 100;
        }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"value {Value}{(Type == TypeValue.Amount ? "$" : "%")}";

        /// <summary>
        /// check if the given tax equals the current instant,
        /// two <see cref="Commission"/> are equals if they have the same <see cref="Value"/> and <see cref="Name"/>
        /// </summary>
        /// <param name="other">the <see cref="Commission"/> to compare</param>
        /// <returns>true if equals false if not</returns>
        public bool Equals(Commission other)
            => other is null ? false : Value.Equals(other.Value) && Type.Equals(other.Type) ? true : false;

        /// <summary>
        /// check if the given object equals the current instant
        /// </summary>
        /// <param name="obj">the object to check</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Commission)) return false;
            if (ReferenceEquals(obj, this)) return true;
            return Equals(obj as Commission);
        }

        /// <summary>
        /// get the hasCode of the current instant
        /// </summary>
        /// <returns>the has value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ (int)Value;
                return (hashCode * 397) ^ Type.GetHashCode();
            }
        }

        /// <summary>
        /// add the value of the commission base on it type of it
        /// to the given amount, if the commission is null or the amount is less than or equals to 0
        /// the amount will be returned
        /// </summary>
        /// <param name="commission">the commission object</param>
        /// <param name="amount">the amount to add the commission to it</param>
        /// <returns>the value</returns>
        public static float operator +(Commission commission, float amount)
        {
            if (amount <= 0 || commission is null)
                return amount;

            return amount + commission.Calculate(amount);
        }

        /// <summary>
        /// add the value of the commission base on it type of it
        /// to the given amount, if the commission is null or the amount is less than or equals to 0
        /// the amount will be returned
        /// </summary>
        /// <param name="commission">the commission object</param>
        /// <param name="amount">the amount to add the commission to it</param>
        /// <returns>the value</returns>
        public static float operator +(float amount, Commission commission)
            => commission + amount;

        /// <summary>
        /// extract the value of the commission base on it type of it
        /// from the given amount, if the commission is null or the amount is less than or equals to 0
        /// the amount will be returned
        /// </summary>
        /// <param name="commission">the commission object</param>
        /// <param name="amount">the amount to extract the commission from it</param>
        /// <returns>the value</returns>
        public static float operator -(Commission commission, float amount)
        {
            if (amount <= 0 || commission is null)
                return amount;

            return amount - commission.Calculate(amount);
        }

        /// <summary>
        /// extract the value of the commission base on it type of it
        /// from the given amount, if the commission is null or the amount is less than or equals to 0
        /// the amount will be returned
        /// </summary>
        /// <param name="commission">the commission object</param>
        /// <param name="amount">the amount to extract the commission from it</param>
        /// <returns>the value</returns>
        public static float operator -(float amount, Commission commission)
            => commission - amount;
    }
}
