﻿namespace Axiobat.Domain.Entities
{
    using App.Common;

    /// <summary>
    /// a class that defines the Currency
    /// </summary>
    public partial class Currency
    {
        /// <summary>
        /// name of the currency
        /// </summary>
        public string Nom { get; set; }

        /// <summary>
        /// symbol
        /// </summary>
        public string Symbole { get; set; }

        /// <summary>
        /// code
        /// </summary>
        public int Code { get; set; }
    }

    /// <summary>
    /// partial part of <see cref="Devise"/>
    /// </summary>
    public partial class Currency : System.IEquatable<Currency>
    {
        /// <summary>
        /// get the default currency (EUR)
        /// </summary>
        public static Currency Default => new Currency("EUR", "€", 978);

        /// <summary>
        /// default constructor
        /// </summary>
        public Currency() { }

        /// <summary>
        /// construct a devise with a name and a symbol
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="symbole"></param>
        public Currency(string nom, string symbole) : this()
        {
            Nom = nom;
            Symbole = symbole;
        }

        /// <summary>
        /// construct a devise with a name and a symbol
        /// </summary>
        /// <param name="nom">the name of the devise</param>
        /// <param name="symbole">the symbol</param>
        /// <param name="code">the code of the symbol</param>
        public Currency(string nom, string symbole, int code) : this(nom, symbole)
        {
            Code = code;
        }

        /// <summary>
        /// check if this currency is the default currency
        /// </summary>
        /// <returns></returns>
        public bool IsDefault() => Equals(Default);

        /// <summary>
        /// string value of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"{Nom}, symbol: {Symbole}, code: {Code}";

        /// <summary>
        /// check if the given tax equals the current instant,
        /// two <see cref="Currency"/> are equals if they have the same <see cref="Value"/> and <see cref="Name"/>
        /// </summary>
        /// <param name="other">the <see cref="Currency"/> to compare</param>
        /// <returns>true if equals false if not</returns>
        public bool Equals(Currency other)
            => other is null ? false : Nom.Equals(other.Nom) && Symbole.Equals(other.Symbole) && Code.Equals(other.Code) ? true : false;

        /// <summary>
        /// check if the given object equals the current instant
        /// </summary>
        /// <param name="obj">the object to check</param>
        /// <returns>true if equals, false if not</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (obj.GetType() != typeof(Currency)) return false;
            if (ReferenceEquals(obj, this)) return true;
            return Equals(obj as Currency);
        }

        /// <summary>
        /// get the hasCode of the current instant
        /// </summary>
        /// <returns>the has value</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 13;
                hashCode = (hashCode * 397) ^ Code.GetHashCode();
                hashCode = (hashCode * 397) ^ (Symbole.IsValid() ? Symbole.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Nom.IsValid() ? Nom.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
