﻿namespace Axiobat.Domain.Entities
{
    /// <summary>
    /// the contact class
    /// </summary>
    public partial class ContactInformation
    {
        /// <summary>
        /// the civility
        /// </summary>
        public string Civility { get; set; }

        /// <summary>
        /// the last name of the contact
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// the first name of the contact
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// the Function of the contact
        /// </summary>
        public string Function { get; set; }

        /// <summary>
        /// the email of the contact
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// the phone number
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// the Land-line number
        /// </summary>
        public string Landline { get; set; }

        /// <summary>
        /// a comment on this contact information
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"contact Name: {Civility}. {LastName} {FirstName}, email: {Email}";
    }
}
