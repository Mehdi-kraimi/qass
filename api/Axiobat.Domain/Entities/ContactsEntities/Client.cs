﻿namespace Axiobat.Domain.Entities
{
    using Axiobat.Domain.Interfaces;
    using Enums;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// a class that describe a client
    /// </summary>
    [DocType(DocumentType.Client)]
    public partial class Client
    {
        /// <summary>
        /// the type of the document
        /// </summary>
        public override DocumentType DocumentType => DocumentType.Client;

        /// <summary>
        /// the client code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// the client type one of <see cref="Constants.ClientType"/>
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// the id of the groupe that this client belongs to it
        /// </summary>
        public int? GroupeId { get; set; }

        /// <summary>
        /// the groupe that the client belongs to it
        /// </summary>
        public Group Groupe { get; set; }

        /// <summary>
        /// list of Addresses of the client
        /// </summary>
        public ICollection<Address> Addresses { get; set; }

        /// <summary>
        /// list of Addresses of the client
        /// </summary>
        public ICollection<Material> Materials { get; set; }

        /// <summary>
        /// list of Construction Workshop of this client
        /// </summary>
        public ICollection<ConstructionWorkshop> ConstructionWorkshops { get; set; }

        /// <summary>
        /// list of Quotes associated with this client
        /// </summary>
        public ICollection<Quote> Quotes { get; set; }

        /// <summary>
        /// the list of invoices associate with the client
        /// </summary>
        public ICollection<Invoice> Invoices { get; set; }

        /// <summary>
        /// the list of Credit Notes associate with the client
        /// </summary>
        public ICollection<CreditNote> CreditNotes { get; set; }

        /// <summary>
        /// list of operation sheets associated with this client
        /// </summary>
        public ICollection<OperationSheet> OperationSheets { get; set; }

        /// <summary>
        /// list of contracts associated with this client
        /// </summary>
        public ICollection<MaintenanceContract> MaintenanceContracts { get; set; }

        /// <summary>
        /// list of MaintenanceOperationSheet associated with this client
        /// </summary>
        public ICollection<MaintenanceOperationSheet> MaintenanceOperationSheets { get; set; }

        /// <summary>
        /// list of MaintenanceVisit associated with this client
        /// </summary>
        public ICollection<MaintenanceVisit> MaintenanceVisits { get; set; }

        /// <summary>
        /// list of Tasks associated with this client
        /// </summary>
        public ICollection<Mission> Missions { get; set; }
    }

    /// <summary>
    /// the partial part for <see cref="Client"/>
    /// </summary>
    public partial class Client : ExternalPartner, IReferenceable<Client>
    {
        /// <summary>
        /// the default constructor
        /// </summary>
        public Client() : base()
        {
            Quotes = new HashSet<Quote>();
            Invoices = new HashSet<Invoice>();
            Addresses = new HashSet<Address>();
            Materials = new HashSet<Material>();
            Missions = new HashSet<Mission>();
            CreditNotes = new HashSet<CreditNote>();
            OperationSheets = new HashSet<OperationSheet>();
            MaintenanceVisits = new HashSet<MaintenanceVisit>();
            MaintenanceContracts = new HashSet<MaintenanceContract>();
            ConstructionWorkshops = new HashSet<ConstructionWorkshop>();
            MaintenanceOperationSheets = new HashSet<MaintenanceOperationSheet>();
        }

        public bool CanIncrementOnUpdate(Client oldState) => base.CanIncrementOnUpdate(oldState);

        /// <summary>
        /// get the default address of the client
        /// </summary>
        /// <returns>the client address</returns>
        public Address DefaultAddress()
            => Addresses.FirstOrDefault(e => e.IsDefault) ?? Addresses.FirstOrDefault();
    }
}
