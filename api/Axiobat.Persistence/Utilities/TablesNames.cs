﻿namespace Axiobat.Persistence
{
    /// <summary>
    /// the name of the tables in database
    /// </summary>
    internal static class TablesNames
    {
        public const string ApplicationConfigurations = "ApplicationConfigurations";
        public const string WorkshopDocumentRubrics = "WorkshopDocumentsRubrics";
        public const string WorkshopDocumentsTypes = "WorkshopDocuments_Types";
        public const string ChartAccountCategories = "ChartAccountCategories";
        public const string WorkshopDocumentTypes = "WorkshopDocumentTypes";
        public const string ProductCategoryType = "ProductCategoryType";
        public const string RecurringDocuments = "RecurringDocuments";
        public const string ProductsSuppliers = "Products_Suppliers";
        public const string AccountingPeriods = "AccountingPeriods";
        public const string MaintenanceVisits = "MaintenanceVisits";
        public const string FinancialAccount = "FinancialAccounts";
        public const string OperationSheets = "OperationSheets";
        public const string ProductsLabels = "Products_Labels";
        public const string Classification = "Classifications";
        public const string SupplierOrders = "SuppliersOrders";
        public const string PaymentMethods = "PaymentMethod";
        public const string TurnoverGoals = "TurnoverGoals";
        public const string GlobalHistory = "GlobalHistory";
        public const string LotProducts = "Lots_Products";
        public const string CreditNote = "CreditNotes";
        public const string UserLogins = "UserLogins";
        public const string UserTokens = "UserTokens";
        public const string Countries = "Countries";
        public const string Expenses = "Expenses";
        public const string Roles = "Roles";
        public const string Users = "Users";
        public const string Units = "Units";

    }
}
