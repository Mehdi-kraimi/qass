﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    /// <summary>
    /// the data access implantation for <see cref="ILabelDataAccess"/>
    /// </summary>
    public partial class LabelDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="LabelDataAccess"/>
    /// </summary>
    public partial class LabelDataAccess : DataAccess<Label, string>, ILabelDataAccess
    {
        public LabelDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<Label> SetDefaultIncludsForListRetrieve(IQueryable<Label> query)
            => query.Include(e => e.Products);
    }
}
