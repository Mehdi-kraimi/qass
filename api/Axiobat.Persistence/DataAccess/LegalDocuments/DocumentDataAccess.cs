﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    /// <summary>
    /// the data access implantation for <see cref="Document"/>
    /// </summary>
    /// <typeparam name="TDocument">the documents</typeparam>
    public partial class DocumentDataAccess<TDocument>
    {
    }

    /// <summary>
    /// partial part for <see cref="DocumentDataAccess{TDocument}"/>
    /// </summary>
    /// <typeparam name="TDocument"></typeparam>
    public partial class DocumentDataAccess<TDocument> : DataAccess<TDocument, string>, IDocumentDataAccess<TDocument>
        where TDocument : Document
    {
        public DocumentDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        /// <summary>
        /// set the default include of the document
        /// </summary>
        /// <param name="query">the query instant</param>
        /// <returns>the new query result</returns>
        protected override IQueryable<TDocument> SetDefaultIncludsForListRetrieve(IQueryable<TDocument> query)
            => query.Include(e => e.Workshop);

        protected override IQueryable<TDocument> SetDefaultIncludsForSingleRetrieve(IQueryable<TDocument> query)
            => query.Include(e => e.Workshop);
    }
}
