﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using App.Common.Enums;
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implementation for <see cref="ICreditNoteDataAccess"/>
    /// </summary>
    public partial class SupplierOrderDataAccess
    {
        /// <summary>
        /// update the status of the given suppliers orders
        /// </summary>
        /// <param name="status">the new status</param>
        /// <param name="supplierOrdersIds">the id of the suppliers</param>
        /// <returns>the affected lines</returns>
        public Task<int> UpdateStatusAsync(string status, string[] supplierOrdersIds)
        {
            if (supplierOrdersIds.Length <= 0)
                return Task.FromResult(0);

            var Identifiers = supplierOrdersIds.SelectIdentifier(e => e, ListBounds.parentheses);
            var sql = $"UPDATE {TablesNames.SupplierOrders} SET {nameof(SupplierOrder.Status)} = '{status}' WHERE {nameof(SupplierOrder.Id)} IN {Identifiers}";
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return _context.Database.ExecuteSqlCommandAsync(sql);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }

        /// <summary>
        /// get the list of supplier orders
        /// </summary>
        /// <param name="workshopId"></param>
        /// <param name="supplierId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public Task<IEnumerable<SupplierOrder>> GetByWorkshopIdSupplierIdStatusAsync(string workshopId, string supplierId, params string[] status)
            => GetList(null).Where(e => e.SupplierId == supplierId && status.Contains(e.Status))
                .ToIEnumerableAsync();
    }

    /// <summary>
    /// partial part <see cref="SupplierDataAccess"/>
    /// </summary>
    public partial class SupplierOrderDataAccess : DocumentDataAccess<SupplierOrder>, ISupplierOrderDataAccess
    {
        /// <summary>
        /// create an instant of <see cref="SupplierOrderDataAccess"/>
        /// </summary>
        /// <param name="context">the data db context</param>
        /// <param name="loggerFactory">the logger factory instant</param>
        public SupplierOrderDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<SupplierOrder> SetPagedResultFilterOptions<IFilter>(IQueryable<SupplierOrder> query, IFilter filterOption)
        {
            if (filterOption is DocumentFilterOptions filter)
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreationDate >= filter.DateStart && e.DueDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreationDate < filter.DateEnd && e.DueDate < filter.DateEnd);

                if (filter.WorkshopId.IsValid())
                    query = query.Where(e => e.WorkshopId == filter.WorkshopId);

                if (filter.ExternalPartnerId.IsValid())
                    query = query.Where(e => e.SupplierId == filter.ExternalPartnerId);

                if (filter.Status.Any())
                    query = query.Where(e => filter.Status.Contains(e.Status));
            }

            return query;
        }

        protected override IQueryable<SupplierOrder> SetDefaultIncludsForListRetrieve(IQueryable<SupplierOrder> query)
            => query
                .Include(e => e.Workshop)
                .Include(e => e.Expenses)
                    .ThenInclude(e => e.Expense);

        protected override IQueryable<SupplierOrder> SetDefaultIncludsForSingleRetrieve(IQueryable<SupplierOrder> query)
            => query
                .Include(e => e.Workshop)
                .Include(e => e.Expenses)
                    .ThenInclude(e => e.Expense);
    }
}
