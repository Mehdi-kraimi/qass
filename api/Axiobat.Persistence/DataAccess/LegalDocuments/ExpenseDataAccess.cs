﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implementation for <see cref="IExpenseDataAccess"/>
    /// </summary>
    public partial class ExpenseDataAccess
    {
        /// <summary>
        /// check if the expense has any payments associated with it
        /// </summary>
        /// <param name="expenseId">the id of the expense</param>
        /// <returns>true if any payments exist, false if not</returns>
        public Task<bool> HasPaymentsAsync(string expenseId)
            => _context.Expenses_Payments.AsNoTracking().AnyAsync(e => e.ExpenseId == expenseId);
    }

    /// <summary>
    /// partial part for <see cref="ExpenseDataAccess"/>
    /// </summary>
    public partial class ExpenseDataAccess : DocumentDataAccess<Expense>, IExpenseDataAccess
    {
        public ExpenseDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<Expense> SetPagedResultFilterOptions<IFilter>(IQueryable<Expense> query, IFilter filterOption)
        {
            if (filterOption is DocumentFilterOptions filter)
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreationDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreationDate < filter.DateEnd);

                if (filter.WorkshopId.IsValid())
                    query = query.Where(e => e.WorkshopId == filter.WorkshopId);

                if (filter.ExternalPartnerId.IsValid())
                    query = query.Where(e => e.SupplierId == filter.ExternalPartnerId);

                if (filter.Status.Count() > 0)
                    query = query.Where(e => filter.Status.Contains(e.Status));
            }

            return query;
        }

        protected override IQueryable<Expense> SetDefaultIncludsForListRetrieve(IQueryable<Expense> query)
            => query.Include(e => e.Workshop)
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment);

        protected override IQueryable<Expense> SetDefaultIncludsForSingleRetrieve(IQueryable<Expense> query)
            => query.Include(e => e.Workshop)
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment)
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment)
                        .ThenInclude(e => e.Account)
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment)
                        .ThenInclude(e => e.PaymentMethod)
                .Include(e => e.SupplierOrders)
                    .ThenInclude(e => e.SupplierOrder);
    }
}
