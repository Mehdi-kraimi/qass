﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Axiobat.Domain.Constants;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    /// <summary>
    /// the data access implementation for <see cref="Quote"/>
    /// </summary>
    public partial class QuoteDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="QuoteDataAccess"/>
    /// </summary>
    public partial class QuoteDataAccess : DocumentDataAccess<Quote>, IQuoteDataAccess
    {
        /// <summary>
        /// create an instant of <see cref="Quote"/>
        /// </summary>
        /// <param name="context">the data db context</param>
        /// <param name="loggerFactory">the logger factory instant</param>
        public QuoteDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory) { }

        protected override IQueryable<Quote> SetDefaultIncludsForListRetrieve(IQueryable<Quote> query)
             => query.Include(e => e.SupplierOrders)
                .Include(e => e.Workshop)
                .Include(e => e.Invoices)
                .Include(e => e.OperationSheets)
                .Include(e => e.SupplierOrders);

        protected override IQueryable<Quote> SetDefaultIncludsForSingleRetrieve(IQueryable<Quote> query)
            => query.Include(e => e.Invoices)
                .Include(e => e.OperationSheets)
                .Include(e => e.SupplierOrders)
                .Include(e => e.Workshop);

        protected override IQueryable<Quote> SetPagedResultFilterOptions<IFilter>(IQueryable<Quote> query, IFilter filterOption)
        {
            if (filterOption is DocumentFilterOptions filter)
            {
                if (filter.ExternalPartnerId.IsValid())
                    query = query.Where(e => e.ClientId == filter.ExternalPartnerId);

                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreatedOn >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreatedOn < filter.DateEnd);

                if (filter.WorkshopId.IsValid())
                    query = query.Where(e => e.WorkshopId == filter.WorkshopId);

                if (filter.Status.Any())
                    query = query.Where(GetStatusPredicate(filter.Status));
            }

            return query;
        }

        protected static Expression<Func<Quote, bool>> GetStatusPredicate(IEnumerable<string> status)
        {
            var newStatus = new HashSet<string>(status);
            var predicate = PredicateBuilder.True<Quote>();
            var isFirst = true;

            if (status.Contains(QuoteStatus.Late))
            {
                predicate = predicate.And(e => e.Status == QuoteStatus.InProgress && e.DueDate.Date < DateTime.Today);
                newStatus.Remove(QuoteStatus.Late);
                isFirst = false;
            }

            if (status.Contains(QuoteStatus.InProgress))
            {
                if (isFirst)
                    predicate = predicate.And(e => e.Status == QuoteStatus.InProgress && e.DueDate.Date > DateTime.Today);
                else
                    predicate = predicate.Or(e => e.Status == QuoteStatus.InProgress && e.DueDate.Date > DateTime.Today);

                newStatus.Remove(QuoteStatus.InProgress);
                isFirst = false;
            }

            if (newStatus.Any())
            {
                if (isFirst)
                    predicate = predicate.And(e => newStatus.Contains(e.Status));
                else
                    predicate = predicate.Or(e => newStatus.Contains(e.Status));
            }

            return predicate;
        }
    }
}
