﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Axiobat.Domain.Constants;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implementation for <see cref="Invoice"/>
    /// </summary>
    public partial class InvoiceDataAccess
    {
        public Task<IEnumerable<Invoice>> GetHoldbackDetailsModelAsync(string workshopId)
            => _context.Invoices.AsNoTracking()
                .Where(e => e.WorkshopId == workshopId)
                .ToIEnumerableAsync();
    }

    /// <summary>
    /// partial part for <see cref="InvoiceDataAccess"/>
    /// </summary>
    public partial class InvoiceDataAccess : DocumentDataAccess<Invoice>, IInvoiceDataAccess
    {
        /// <summary>
        /// create an instant of <see cref="InvoiceDataAccess"/>
        /// </summary>
        /// <param name="context">the data db context</param>
        /// <param name="loggerFactory">the logger factory instant</param>
        public InvoiceDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory) { }

        protected override IQueryable<Invoice> SetDefaultIncludsForListRetrieve(IQueryable<Invoice> query)
            => query.Include(e => e.Workshop)
                .Include(e => e.Contract)
                .Include(e => e.CreditNotes)
                .Include(e => e.OperationSheetMaintenance)
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment);

        protected override IQueryable<Invoice> SetDefaultIncludsForSingleRetrieve(IQueryable<Invoice> query)
            => query.Include(e => e.OperationSheets)
                .Include(e => e.Contract)
                .Include(e => e.OperationSheetMaintenance)
                .Include(e => e.Workshop)
                .Include(e => e.CreditNotes)
                .Include(e => e.Quote)
                .Include(e => e.OperationSheets)
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment)
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment)
                        .ThenInclude(e => e.Account)
                .Include(e => e.Payments)
                    .ThenInclude(e => e.Payment)
                        .ThenInclude(e => e.PaymentMethod);

        protected override IQueryable<Invoice> SetPagedResultFilterOptions<IFilter>(IQueryable<Invoice> query, IFilter filterOption)
        {
            if (filterOption is DocumentFilterOptions filter)
            {
                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.CreationDate >= filter.DateStart);

                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.CreationDate < filter.DateEnd);

                if (filter.ExternalPartnerId.IsValid())
                    query = query.Where(e => e.ClientId == filter.ExternalPartnerId);

                if (filter.WorkshopId.IsValid())
                    query = query.Where(e => e.WorkshopId == filter.WorkshopId);

                if (filter.Status.Any())
                {
                    query = query.Where(GetStatusPredicate(filter.Status));
                }
            }

            return query;
        }

        protected static Expression<Func<Invoice, bool>> GetStatusPredicate(IEnumerable<string> status)
        {
            var newStatus = new HashSet<string>(status);
            var predicate = PredicateBuilder.True<Invoice>();
            var isFirst = true;

            if (status.Contains(InvoiceStatus.Late))
            {
                predicate = predicate.And(e => e.Status == InvoiceStatus.InProgress && e.DueDate.Date < DateTime.Today);
                newStatus.Remove(InvoiceStatus.Late);
                isFirst = false;
            }

            if (status.Contains(InvoiceStatus.InProgress))
            {
                if (isFirst)
                    predicate = predicate.And(e => e.Status == InvoiceStatus.InProgress && e.DueDate.Date > DateTime.Today);
                else
                    predicate = predicate.Or(e => e.Status == InvoiceStatus.InProgress && e.DueDate.Date > DateTime.Today);

                newStatus.Remove(InvoiceStatus.InProgress);
                isFirst = false;
            }

            if (newStatus.Any())
            {
                if (isFirst)
                    predicate = predicate.And(e => newStatus.Contains(e.Status));
                else
                    predicate = predicate.Or(e => newStatus.Contains(e.Status));
            }

            return predicate;
        }
    }
}
