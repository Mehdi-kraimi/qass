﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Domain.Entities;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Persistence.DataContext;
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// a class that implement <see cref="IRoleDataAccess"/>
    /// to describe the data access layer for the <see cref="Role"/> entity
    /// </summary>
    public partial class RoleDataAccess
    {
        /// <summary>
        /// get the user role
        /// </summary>
        /// <param name="userId">the id of the user</param>
        /// <returns>the role instant</returns>
        public Task<Role> GetUserRoleAsync(Guid userId)
            => _context.Roles.AsNoTracking()
                .Join(_context.Users, r => r.Id, u => u.RoleId, (r, u) => new { role = r, Users = u })
                .Where(q => q.Users.Id == userId)
                .Select(q => q.role)
                .FirstOrDefaultAsync();
    }

    /// <summary>
    /// partial part of <see cref="RoleDataAccess"/>
    /// </summary>
    public partial class RoleDataAccess : DataAccess<Role, Guid>, IRoleDataAccess
    {
        private bool _disposed;

        /// <summary>
        /// create a new instant of <see cref="RoleDataAccess"/>
        /// </summary>
        /// <param name="context">the <see cref="ApplicationDbContext"/> db context</param>
        /// <param name="loggerFactory">the logger factory</param>
        public RoleDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory) { }

        /// <summary>
        /// Creates a new role in a store as an asynchronous operation.
        /// </summary>
        /// <param name="role">The role to create in the store.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A <see cref="Task{TResult}"/> that represents the <see cref="IdentityResult"/> of the asynchronous query.</returns>
        public async virtual Task<IdentityResult> CreateAsync(Role role, CancellationToken cancellationToken = default)
        {
            Validate(role, cancellationToken);
            var result = await base.AddAsync(role);

            if (!result.IsSuccess)
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = result.MessageCode.ToString(),
                    Description = result.Message
                });

            return IdentityResult.Success;
        }

        /// <summary>
        /// Updates a role in a store as an asynchronous operation.
        /// </summary>
        /// <param name="role">The role to update in the store.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A <see cref="Task{TResult}"/> that represents the <see cref="IdentityResult"/> of the asynchronous query.</returns>
        public async virtual Task<IdentityResult> UpdateAsync(Role role, CancellationToken cancellationToken = default)
        {
            Validate(role, cancellationToken);
            var result = await base.UpdateAsync(role);

            if (!result.IsSuccess)
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = result.MessageCode.ToString(),
                    Description = result.Message
                });

            return IdentityResult.Success;
        }

        /// <summary>
        /// Deletes a role from the store as an asynchronous operation.
        /// </summary>
        /// <param name="role">The role to delete from the store.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A <see cref="Task{TResult}"/> that represents the <see cref="IdentityResult"/> of the asynchronous query.</returns>
        public async virtual Task<IdentityResult> DeleteAsync(Role role, CancellationToken cancellationToken = default)
        {
            Validate(role, cancellationToken);
            var result = await base.DeleteAsync(role);

            if (!result.IsSuccess)
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = result.MessageCode.ToString(),
                    Description = result.Message
                });

            return IdentityResult.Success;
        }

        /// <summary>
        /// Gets the ID for a role from the store as an asynchronous operation.
        /// </summary>
        /// <param name="role">The role whose ID should be returned.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A <see cref="Task{TResult}"/> that contains the ID of the role.</returns>
        public virtual Task<Guid> GeRoleIdAsync(Role role, CancellationToken cancellationToken = default)
        {
            Validate(role, cancellationToken);
            return Task.FromResult(role.Id);
        }

        /// <summary>
        /// Gets the name of a role from the store as an asynchronous operation.
        /// </summary>
        /// <param name="role">The role whose name should be returned.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A <see cref="Task{TResult}"/> that contains the name of the role.</returns>
        public virtual Task<string> GeRoleNameAsync(Role role, CancellationToken cancellationToken = default)
        {
            Validate(role, cancellationToken);
            return Task.FromResult(role.Name);
        }

        /// <summary>
        /// Sets the name of a role in the store as an asynchronous operation.
        /// </summary>
        /// <param name="role">The role whose name should be set.</param>
        /// <param name="roleName">The name of the role.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>The <see cref="Task"/> that represents the asynchronous operation.</returns>
        public virtual Task SeRoleNameAsync(Role role, string roleName, CancellationToken cancellationToken = default)
        {
            Validate(role, cancellationToken);
            role.Name = roleName;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Converts the provided <paramref name="id"/> to a strongly typed key object.
        /// </summary>
        /// <param name="id">The id to convert.</param>
        /// <returns>An instance of <typeparamref name="int"/> representing the provided <paramref name="id"/>.</returns>
        public virtual int ConvertIdFromString(string id)
        {
            if (id == null)
            {
                return default;
            }
            return (int)TypeDescriptor.GetConverter(typeof(int)).ConvertFromInvariantString(id);
        }

        /// <summary>
        /// Converts the provided <paramref name="id"/> to its string representation.
        /// </summary>
        /// <param name="id">The id to convert.</param>
        /// <returns>An <see cref="string"/> representation of the provided <paramref name="id"/>.</returns>
        public virtual string ConvertIdToString(int id)
        {
            if (id.Equals(default))
            {
                return null;
            }
            return id.ToString();
        }

        /// <summary>
        /// Finds the role who has the specified ID as an asynchronous operation.
        /// </summary>
        /// <param name="id">The role ID to look for.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A <see cref="Task{TResult}"/> that result of the look up.</returns>
        public virtual Task<Role> FindByIdAsync(string id, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            var roleId = ConvertIdFromString(id);
            return _context.Roles.FirstOrDefaultAsync(u => u.Id.Equals(roleId), cancellationToken);
        }

        /// <summary>
        /// Finds the role who has the specified normalized name as an asynchronous operation.
        /// </summary>
        /// <param name="normalizedName">The normalized role name to look for.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A <see cref="Task{TResult}"/> that result of the look up.</returns>
        public virtual Task<Role> FindByNameAsync(string normalizedName, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return _context.Roles.FirstOrDefaultAsync(r => r.NormalizedName == normalizedName, cancellationToken);
        }

        /// <summary>
        /// Get a role's normalized name as an asynchronous operation.
        /// </summary>
        /// <param name="role">The role whose normalized name should be retrieved.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A <see cref="Task{TResult}"/> that contains the name of the role.</returns>
        public virtual Task<string> GetNormalizedRoleNameAsync(Role role, CancellationToken cancellationToken = default)
        {
            Validate(role, cancellationToken);
            return Task.FromResult(role.NormalizedName);
        }

        /// <summary>
        /// Set a role's normalized name as an asynchronous operation.
        /// </summary>
        /// <param name="role">The role whose normalized name should be set.</param>
        /// <param name="normalizedName">The normalized name to set</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>The <see cref="Task"/> that represents the asynchronous operation.</returns>
        public virtual Task SetNormalizedRoleNameAsync(Role role, string normalizedName, CancellationToken cancellationToken = default)
        {
            Validate(role, cancellationToken);
            role.NormalizedName = normalizedName;
            return Task.CompletedTask;
        }

        /// <summary>
        /// get the id value of the given role
        /// </summary>
        /// <param name="role">the role to get the id for it</param>
        /// <param name="cancellationToken">the cancellation token</param>
        /// <returns>the id value</returns>
        public Task<string> GetRoleIdAsync(Role role, CancellationToken cancellationToken)
        {
            Validate(role, cancellationToken);
            return Task.FromResult(role.Id.ToString());
        }

        /// <summary>
        /// get the Name value of the given role
        /// </summary>
        /// <param name="role">the role to get the Name for it</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>the Name value</returns>
        public Task<string> GetRoleNameAsync(Role role, CancellationToken cancellationToken)
        {
            Validate(role, cancellationToken);
            return Task.FromResult(role.Name);
        }

        /// <summary>
        /// set the value of the role name to the given value
        /// </summary>
        /// <param name="role">the role to update his name</param>
        /// <param name="roleName">the new name value</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>The <see cref="Task"/> that represents the asynchronous operation.</returns>
        public Task SetRoleNameAsync(Role role, string roleName, CancellationToken cancellationToken)
        {
            Validate(role, cancellationToken);
            role.Name = roleName;
            return Task.CompletedTask;
        }

        /// <summary>
        /// validate the role and cancellationToken
        /// </summary>
        /// <param name="role">the role to validate</param>
        /// <param name="cancellationToken">the cancellation token</param>
        private void Validate(Role role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }
        }

        /// <summary>
        /// Throws if this class has been disposed.
        /// </summary>
        protected void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        /// <summary>
        /// Dispose the store
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// dispose the object an
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            _disposed = disposing;
            if (disposing)
            {

            }
        }
    }
}
