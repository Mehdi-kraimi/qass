﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Axiobat.Application.Enums;
    using DataContext;
    using Domain.Constants;
    using Domain.Entities;
    using FluentValidation.Validators;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for the accounting service
    /// </summary>
    public partial class AccountingDataAccess : IAccountingDataAccess
    {
        /// <summary>
        /// get the list of credit notes
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the list of credit notes</returns>
        public Task<PagedResult<CreditNote>> GetCreditNoteAsync(JournalFilterOptions filterOption)
        {
            var query = _context.CreditNotes
                .Where(e => e.Status != CreditNoteStatus.Draft);

            if (filterOption.SearchQuery.IsValid())
                query = query.Where(e => e.SearchTerms.Contains(filterOption.SearchQuery));

            if (filterOption.DateStart.HasValue)
                query = query.Where(e => e.CreationDate >= filterOption.DateStart);

            if (filterOption.DateEnd.HasValue)
                query = query.Where(e => e.CreationDate < filterOption.DateEnd);

            return query
                .DynamicOrderBy(filterOption.OrderBy, filterOption.SortDirection)
                .AsPagedResultAsync(filterOption.Page, filterOption.PageSize, filterOption.IgnorePagination, _logger);
        }

        /// <summary>
        /// get the list of invoices
        /// </summary>
        /// <param name="filterOptions">the list of invoices</param>
        /// <returns>the list of invoices</returns>
        public Task<PagedResult<Invoice>> GetInvoicesAsync(JournalFilterOptions filterOption)
        {
            var query = _context.Invoices
                .Include(e => e.Quote)
                .Where(e => e.Status != CreditNoteStatus.Draft);

            if (filterOption.SearchQuery.IsValid())
                query = query.Where(e => e.SearchTerms.Contains(filterOption.SearchQuery));

            if (filterOption.DateStart.HasValue)
                query = query.Where(e => e.CreationDate >= filterOption.DateStart);

            if (filterOption.DateEnd.HasValue)
                query = query.Where(e => e.CreationDate < filterOption.DateEnd);

            return query
                .DynamicOrderBy(filterOption.OrderBy, filterOption.SortDirection)
                .AsPagedResultAsync(filterOption.Page, filterOption.PageSize, filterOption.IgnorePagination, _logger);
        }

        /// <summary>
        /// get the list of expenses
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the list of expenses</returns>
        public Task<PagedResult<Expense>> GetExpensesAsync(DateRangeFilterOptions filterOption)
        {
            var query = _context.Expenses
                //.Include(e => e.Supplier)
                .OrderBy(e => e.CreationDate)
                .IgnoreQueryFilters();

            if (filterOption.SearchQuery.IsValid())
                query = query.Where(e => e.SearchTerms.Contains(filterOption.SearchQuery));

            if (filterOption.DateStart.HasValue)
                query = query.Where(e => e.CreationDate >= filterOption.DateStart);

            if (filterOption.DateEnd.HasValue)
                query = query.Where(e => e.CreationDate < filterOption.DateEnd);

            return query
                .DynamicOrderBy(filterOption.OrderBy, filterOption.SortDirection)
                .AsPagedResultAsync(filterOption.Page, filterOption.PageSize, filterOption.IgnorePagination, _logger);
        }

        /// <summary>
        /// get the list of payments
        /// </summary>
        /// <param name="filterOptions">the date filter options</param>
        /// <param name="journalType">the type of the journal</param>
        /// <returns>the list of payment</returns>
        public Task<PagedResult<Payment>> GetPaymentsAsync(DateRangeFilterOptions filterOption, Domain.Enums.FinancialAccountsType accountType)
        {
            var query = _context.Payments
                .Include(e => e.Invoices)
                    .ThenInclude(e => e.Invoice)
                .Include(e => e.Expenses)
                    .ThenInclude(e => e.Expense)
                .Include(e => e.Account)
                .Include(e => e.PaymentMethod)
                .Join(_context.FinancialAccounts, p => p.AccountId, a => a.Id, (p, a) => new { Payment = p, Account = a })
                    .Where(q => q.Account.Type == accountType)
                .Select(q => q.Payment);

            if (filterOption.SearchQuery.IsValid())
                query = query.Where(e => e.SearchTerms.Contains(filterOption.SearchQuery));

            if (filterOption.DateStart.HasValue)
                query = query.Where(e => e.DatePayment >= filterOption.DateStart);

            if (filterOption.DateEnd.HasValue)
                query = query.Where(e => e.DatePayment < filterOption.DateEnd);

            return query
                .DynamicOrderBy(filterOption.OrderBy, filterOption.SortDirection)
                .AsPagedResultAsync(filterOption.Page, filterOption.PageSize, filterOption.IgnorePagination, _logger);
        }

        /// <summary>
        /// get the workshop with the given id for financial summary report, this will
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the construction workshop</param>
        /// <returns>the workshop model</returns>
        public Task<ConstructionWorkshop> GetWorkshopsByIdAsync(string constructionWorkshopId)
            => _context.Workshops
                .Include(e => e.Invoices)
                    .ThenInclude(e => e.Payments)
                        .ThenInclude(e => e.Payment)
                .Include(e => e.Quotes)
                .Include(e => e.Expenses)
                    .ThenInclude(e => e.Payments)
                        .ThenInclude(e => e.Payment)
                .Include(e => e.Expenses)
                .Include(e => e.OperationSheets)
                .FirstOrDefaultAsync(e => e.Id == constructionWorkshopId);
    }

    /// <summary>
    /// partial part for <see cref="AccountingDataAccess"/>
    /// </summary>
    public partial class AccountingDataAccess
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;

        public AccountingDataAccess(ApplicationDbContext dbContext, ILoggerFactory loggerFactory)
        {
            _context = dbContext;
            _logger = loggerFactory.CreateLogger<AccountingDataAccess>();
        }
    }
}
