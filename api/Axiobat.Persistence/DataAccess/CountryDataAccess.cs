﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="Country"/> entity, implementing <see cref="ICountryDataAccess"/>
    /// </summary>
    public partial class CountryDataAccess
    {
        /// <summary>
        /// get the country by code
        /// </summary>
        /// <param name="countryCode">the code of the country to be retrieved</param>
        /// <returns>the country</returns>
        public Task<Country> GetCountryByCodeAsync(string countryCode)
            => _context.Countries.AsNoTracking().SingleOrDefaultAsync(c => c.CountryCode == countryCode);

        /// <summary>
        /// get the country by name
        /// </summary>
        /// <param name="countryName">name of the country to be retrieved</param>
        /// <returns>the country</returns>
        public Task<Country> GetCountryByNameAsync(string countryName)
            => _context.Countries.AsNoTracking().SingleOrDefaultAsync(c => c.Name == countryName);

        /// <summary>
        /// get list of all countries
        /// </summary>
        /// <returns>the list of countries</returns>
        public Task<IEnumerable<Country>> GetAllAsync()
            => _context.Countries.AsNoTracking().ToIEnumerableAsync();

        /// <summary>
        /// get the paged result using the given filter type
        /// </summary>
        /// <typeparam name="IFilter">the type of the filter to use</typeparam>
        /// <param name="filterOption">the filter instant</param>
        /// <returns>paged result</returns>
        public virtual Task<PagedResult<Country>> GetPagedResultAsync<IFilter>(IFilter filterOption)
            where IFilter : IFilterOptions
            => _context.Countries.AsNoTracking()
                .DynamicOrderBy(filterOption.OrderBy, filterOption.SortDirection)
                .AsPagedResultAsync(filterOption.Page, filterOption.PageSize, filterOption.IgnorePagination, _logger);

        /// <summary>
        /// check if the given country code is exist
        /// </summary>
        /// <param name="countryCode">the code of the country</param>
        /// <returns>true if exist, false if not</returns>
        public Task<bool> IsCountryExistAsync(string countryCode)
            => _context.Countries.AsNoTracking().AnyAsync(c => c.CountryCode.Equals(countryCode.ToUpper()));
    }

    /// <summary>
    /// partial part for <see cref="CountryDataAccess"/>
    /// </summary>
    public partial class CountryDataAccess : ICountryDataAccess
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;

        /// <summary>
        /// construct a new <see cref="CountryDataAccess"/>
        /// </summary>
        /// <param name="context">the <see cref="ApplicationDbContext"/> dbContext</param>
        /// <param name="loggerFactory">the logger factory</param>
        public CountryDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<CountryDataAccess>();
            _context = context;
        }
    }
}
