﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Persistence.DataContext;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    public partial class ClassificationDataAccess
    {

    }

    public partial class ClassificationDataAccess : DataAccess<Classification, int>, IClassificationDataAccess
    {
        public ClassificationDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<Classification> SetDefaultIncludsForSingleRetrieve(IQueryable<Classification> query)
            => query.Include(classification => classification.ChartAccountItem)
                .Include(classification => classification.SubClassification)
                    .ThenInclude(classification => classification.ChartAccountItem);

        protected override IQueryable<Classification> SetDefaultIncludsForListRetrieve(IQueryable<Classification> query)
            => query.Include(classification => classification.ChartAccountItem)
                .Include(classification => classification.SubClassification)
                    .ThenInclude(classification => classification.ChartAccountItem);
    }
}
