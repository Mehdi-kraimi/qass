﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="ConstructionWorkshop"/> implementing <see cref="IConstructionWorkshopDataAccess"/>
    /// </summary>
    public partial class ConstructionWorkshopDataAccess
    {
        /// <summary>
        /// get the client of the workshop
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns>the client instant</returns>
        public Task<Client> GetWorkshopClientAsync(string workshopId)
        {
            return _context.Clients
                .Join(_context.Workshops, c => c.Id, w => w.ClientId, (c, w) => new { workshop = w, client = c })
                .Where(q => q.workshop.Id == workshopId)
                .Select(q => q.client)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// get the workshop with the given id for financial summary report
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the construction workshop</param>
        /// <returns>the workshop model</returns>
        public Task<ConstructionWorkshop> GetByIdForFinancialSummaryAsync(string constructionWorkshopId)
            => _context.Workshops
                .Include(e => e.Invoices)
                    .ThenInclude(e => e.Payments)
                        .ThenInclude(e => e.Payment)
                .Include(e => e.Quotes)
                .Include(e => e.Expenses)
                    .ThenInclude(e => e.Payments)
                        .ThenInclude(e => e.Payment)
                .Include(e => e.Expenses)
                .Include(e => e.OperationSheets)
                .FirstOrDefaultAsync(e => e.Id == constructionWorkshopId);

        /// <summary>
        /// retrieve the workshop documents counts informations
        /// </summary>
        /// <param name="constructionWorkshopId">the construction workshop id</param>
        /// <returns>the workshop documents count</returns>
        public Task<WorkshopDocumentsCount> GetWorkshopDocumentsCountAsync(string constructionWorkshopId)
            => _context.Workshops
                .Where(e => e.Id == constructionWorkshopId)
                .Select(e => new WorkshopDocumentsCount
                {
                    QuotesCount = e.Quotes.Count,
                    ExpensesCount = e.Expenses.Count,
                    InvoicesCount = e.Invoices.Count,
                    SupplierOrdersCount = e.SupplierOrders.Count,
                    OperationSheetsCount = e.OperationSheets.Count,
                })
                .FirstOrDefaultAsync();

        public Task<int> GetQuotesDocumentsCountAsync(string workshopId)
            => _context.Quotes.Where(e => e.WorkshopId == workshopId).CountAsync();

        public Task<int> GetInvoiceDocumentsCountAsync(string workshopId)
            => _context.Invoices.Where(e => e.WorkshopId == workshopId).CountAsync();

        public Task<int> GetSupplierOrderDocumentsCountAsync(string workshopId)
            => _context.SupplierOrders.Where(e => e.WorkshopId == workshopId).CountAsync();
    }

    /// <summary>
    /// partial part for <see cref="ConstructionWorkshopDataAccess"/>
    /// </summary>
    public partial class ConstructionWorkshopDataAccess : DataAccess<ConstructionWorkshop, string>, IConstructionWorkshopDataAccess
    {
        /// <summary>
        /// create an instant of <see cref="ConstructionWorkshopDataAccess"/>
        /// </summary>
        /// <param name="context">the <see cref="ApplicationDbContext"/> instant</param>
        /// <param name="loggerFactory">the <see cref="ILoggerFactory"/> instant</param>
        public ConstructionWorkshopDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<ConstructionWorkshop> SetPagedResultFilterOptions<IFilter>(IQueryable<ConstructionWorkshop> query, IFilter filterOption)
        {
            if (filterOption is WorkshopFilterOptions filter)
            {
                if (filter.ClientId.IsValid())
                    query = query.Where(e => e.ClientId == filter.ClientId);

                if (filter.Status.Count > 0)
                    query = query.Where(e => filter.Status.Contains(e.Status));
            }

            return query;
        }

        protected override IQueryable<ConstructionWorkshop> SetDefaultIncludsForListRetrieve(IQueryable<ConstructionWorkshop> query)
            => query.Include(e => e.Invoices);
    }
}
