﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using Application.Models;
    using Domain.Entities;
    using DataContext;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access implementation for <see cref="IMaintenanceVisitDataAccess"/>
    /// </summary>
    public partial class MaintenanceVisitDataAccess
    {
        public Task UpdateStatusAsync(string maintenanceVisitId, string status)
        {
            var SQL = $"UPDATE {TablesNames.MaintenanceVisits} SET {nameof(MaintenanceVisit.Status)} = '{status}' WHERE Id = '{maintenanceVisitId}'";
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return _context.Database.ExecuteSqlCommandAsync(SQL);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceVisitDataAccess"/>
    /// </summary>
    public partial class MaintenanceVisitDataAccess : DataAccess<MaintenanceVisit, string>, IMaintenanceVisitDataAccess
    {
        public MaintenanceVisitDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<MaintenanceVisit> SetPagedResultFilterOptions<IFilter>(IQueryable<MaintenanceVisit> query, IFilter filterOption)
        {
            if (filterOption is MaintenanceVisitFilterOptions filter)
            {
                if (filter.Year.HasValue)
                    query = query.Where(e => e.Date.Year == filter.Year);

                if (filter.Months.Count() > 0)
                    query = query.Where(e => filter.Months.Contains(e.Date.Month));

                if (!(filter.ExternalPartnerId is null) && filter.ExternalPartnerId.Count() > 0)
                    query = query.Where(e => filter.ExternalPartnerId.Contains(e.ClientId));
            }

            return query;
        }

        protected override IQueryable<MaintenanceVisit> SetDefaultIncludsForListRetrieve(IQueryable<MaintenanceVisit> query)
            => query
                .Include(e => e.MaintenanceOperationSheet)
                    .ThenInclude(e => e.Technician);

        protected override IQueryable<MaintenanceVisit> SetDefaultIncludsForSingleRetrieve(IQueryable<MaintenanceVisit> query)
            => query
                .Include(e => e.MaintenanceContract)
                .Include(e => e.MaintenanceOperationSheet)
                    .ThenInclude(e => e.Technician);
    }
}
