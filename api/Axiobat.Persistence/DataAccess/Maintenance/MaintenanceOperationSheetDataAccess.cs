﻿namespace Axiobat.Persistence.DataAccess
{
    using App.Common;
    using Application.Data;
    using Application.Models;
    using Axiobat.Domain.Constants;
    using DataContext;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// the data access implantation for <see cref="IMaintenanceOperationSheetDataAccess"/>
    /// </summary>
    public partial class MaintenanceOperationSheetDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="MaintenanceOperationSheetDataAccess"/>
    /// </summary>
    public partial class MaintenanceOperationSheetDataAccess : DataAccess<MaintenanceOperationSheet, string>, IMaintenanceOperationSheetDataAccess
    {
        public MaintenanceOperationSheetDataAccess(
            ApplicationDbContext context,
            ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }

        protected override IQueryable<MaintenanceOperationSheet> SetPagedResultFilterOptions<IFilter>(IQueryable<MaintenanceOperationSheet> query, IFilter filterOption)
        {
            if (filterOption is MaintenanceOperationSheetFilterOptions filter)
            {
                if (filter.DateEnd.HasValue)
                    query = query.Where(e => e.StartDate.Date <= filter.DateEnd);

                if (filter.DateStart.HasValue)
                    query = query.Where(e => e.StartDate.Date >= filter.DateStart);

                if (filter.TechniciansId.Any())
                    query = query.Where(e => e.TechnicianId == null || (e.TechnicianId != null && filter.TechniciansId.Contains((Guid)e.TechnicianId)));

                if (filter.ExternalPartnerId.IsValid())
                    query = query.Where(e => e.ClientId == filter.ExternalPartnerId);

                if (filter.Status.Any())
                {
                    var status = new HashSet<string>(filter.Status);

                    var isFirst = true;
                    var predicate = PredicateBuilder.True<MaintenanceOperationSheet>();
                    if (filter.Status.Contains(MaintenanceOperationSheetStatus.Late))
                    {
                        predicate = predicate.And(e => e.Status == MaintenanceOperationSheetStatus.Planned && e.EndDate.Date < DateTime.Today);
                        status.Remove(MaintenanceOperationSheetStatus.Late);
                        status.Remove(MaintenanceOperationSheetStatus.Planned);
                        isFirst = false;
                    }
                    if (filter.Status.Contains(MaintenanceOperationSheetStatus.Planned))
                    {
                        if (isFirst)
                            predicate = predicate.And(e => e.Status == MaintenanceOperationSheetStatus.Planned && e.EndDate.Date >= DateTime.Today);
                        else
                            predicate = predicate.Or(e => e.Status == MaintenanceOperationSheetStatus.Planned && e.EndDate.Date >= DateTime.Today);

                        status.Remove(MaintenanceOperationSheetStatus.Planned);
                        isFirst = false;
                    }
                    if (status.Any())
                    {
                        if (isFirst)
                            predicate = predicate.And(e => status.Contains(e.Status));
                        else
                            predicate = predicate.Or(e => status.Contains(e.Status));
                    }

                    query = query.Where(predicate);
                }

                if (filter.HasQuoteRequest.HasValue)
                    query = query.Where(e => e.QuoteRequest != null && e.QuoteRequest != "");

                if (filter.Type.HasValue)
                    query = query.Where(e => e.Type == filter.Type);
            }

            return query;
        }

        protected override IQueryable<MaintenanceOperationSheet> SetDefaultIncludsForListRetrieve(IQueryable<MaintenanceOperationSheet> query)
            => query
                .Include(e => e.Technician);

        protected override IQueryable<MaintenanceOperationSheet> SetDefaultIncludsForSingleRetrieve(IQueryable<MaintenanceOperationSheet> query)
            => query
                .Include(e => e.Invoice)
                .Include(e => e.Technician);
    }
}
