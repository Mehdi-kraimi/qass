﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using DataContext;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// data access implementation for <see cref="ClientDataAccess"/>
    /// </summary>
    public partial class ClientDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="ClientDataAccess"/>
    /// </summary>
    public partial class ClientDataAccess : ExternalPartnersDataAccess<Client>, IClientDataAccess
    {
        public ClientDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }
    }
}
