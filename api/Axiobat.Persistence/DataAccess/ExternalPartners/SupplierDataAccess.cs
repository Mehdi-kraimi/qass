﻿namespace Axiobat.Persistence.DataAccess
{
    using Application.Data;
    using DataContext;
    using Domain.Entities;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// the supplier data access implementation for <see cref="ISupplierDataAccess"/>
    /// </summary>
    public partial class SupplierDataAccess
    {

    }

    /// <summary>
    /// partial part for <see cref="SupplierDataAccess"/>
    /// </summary>
    public partial class SupplierDataAccess : ExternalPartnersDataAccess<Supplier>, ISupplierDataAccess
    {
        public SupplierDataAccess(ApplicationDbContext context, ILoggerFactory loggerFactory)
            : base(context, loggerFactory)
        {
        }
    }
}
