﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class RemoveIsDeletedFlagFromUSer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Users",
                nullable: false,
                defaultValue: false);
        }
    }
}
