﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class UpdateAdressIntervention : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdresseIntervention",
                table: "Invoices");

            migrationBuilder.AddColumn<string>(
                name: "AddressIntervention",
                table: "Invoices",
                type: "LONGTEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressIntervention",
                table: "Invoices");

            migrationBuilder.AddColumn<string>(
                name: "AdresseIntervention",
                table: "Invoices",
                type: "LONGTEXT",
                nullable: true);
        }
    }
}
