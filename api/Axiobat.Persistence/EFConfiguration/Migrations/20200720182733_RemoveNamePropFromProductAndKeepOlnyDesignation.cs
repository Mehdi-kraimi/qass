﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class RemoveNamePropFromProductAndKeepOlnyDesignation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("UPDATE Products AS p SET p.Designation = p.Name");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Lots",
                newName: "Designation");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Designation",
                table: "Lots",
                newName: "Name");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Products",
                maxLength: 256,
                nullable: true);
        }
    }
}
