﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class AddEquipmentMaintenancesEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EquipmentMaintenances",
                columns: table => new
                {
                    CreatedOn = table.Column<DateTimeOffset>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    LastModifiedOn = table.Column<DateTimeOffset>(nullable: true, defaultValueSql: "CURRENT_TIMESTAMP"),
                    SearchTerms = table.Column<string>(maxLength: 500, nullable: true),
                    Id = table.Column<string>(maxLength: 256, nullable: false),
                    EquipmentName = table.Column<string>(maxLength: 256, nullable: true),
                    MaintenanceOperations = table.Column<string>(type: "LONGTEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentMaintenances", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentMaintenances_EquipmentName",
                table: "EquipmentMaintenances",
                column: "EquipmentName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentMaintenances_SearchTerms",
                table: "EquipmentMaintenances",
                column: "SearchTerms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EquipmentMaintenances");
        }
    }
}
