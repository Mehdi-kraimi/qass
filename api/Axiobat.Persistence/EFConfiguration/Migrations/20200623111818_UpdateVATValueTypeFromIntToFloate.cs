﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Axiobat.Persistence.Migrations
{
    public partial class UpdateVATValueTypeFromIntToFloate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "VATValue",
                table: "ChartAccountCategories",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "VATValue",
                table: "ChartAccountCategories",
                nullable: false,
                oldClrType: typeof(float));
        }
    }
}
