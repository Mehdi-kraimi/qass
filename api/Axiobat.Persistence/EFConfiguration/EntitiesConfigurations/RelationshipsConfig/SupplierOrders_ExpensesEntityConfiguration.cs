﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal class SupplierOrders_ExpensesEntityConfiguration : IEntityTypeConfiguration<SupplierOrders_Expenses>
    {
        public void Configure(EntityTypeBuilder<SupplierOrders_Expenses> builder)
        {
            // table name
            builder.ToTable("SupplierOrders_Expenses");

            // primary key
            builder.HasKey(e => new { e.SupplierOrderId, e.ExpenseId });

            // relationships
            builder.HasOne(e => e.Expense)
                .WithMany(e => e.SupplierOrders)
                .HasForeignKey(e => e.ExpenseId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.SupplierOrder)
                .WithMany(e => e.Expenses)
                .HasForeignKey(e => e.SupplierOrderId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
