﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ProductLabelEntityConfiguration : IEntityTypeConfiguration<ProductLabel>
    {
        public void Configure(EntityTypeBuilder<ProductLabel> builder)
        {
            // table name
            builder.ToTable("Products_Labels");

            // key
            builder.HasKey(e => new { e.LabelId, e.ProductId });

            // relationships
            builder.HasOne(e => e.Product)
                .WithMany(p => p.Labels)
                .HasForeignKey(e => e.ProductId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Label)
                .WithMany(p => p.Products)
                .HasForeignKey(e => e.LabelId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
