﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class SupplierProductsEntityConfiguration : IEntityTypeConfiguration<ProductSupplier>
    {
        public void Configure(EntityTypeBuilder<ProductSupplier> builder)
        {
            builder.ToTable("Products_Suppliers");

            // properties configurations
            builder.HasKey(e => new { e.ProductId, e.SupplierId });

            // relationships
            builder.HasOne(e => e.Product)
                .WithMany(e => e.Suppliers)
                .HasForeignKey(e => e.ProductId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Supplier)
                .WithMany(e => e.Products)
                .HasForeignKey(e => e.SupplierId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
