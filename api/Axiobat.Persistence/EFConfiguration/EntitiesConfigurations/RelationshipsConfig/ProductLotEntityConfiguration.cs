﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ProductLotEntityConfiguration : IEntityTypeConfiguration<LotProduct>
    {
        public void Configure(EntityTypeBuilder<LotProduct> builder)
        {
            builder.ToTable("Lots_Products");

            builder.HasKey(e => new { e.LotId, e.ProductId });

            // properties configuration

            // relationShips
            builder.HasOne(e => e.Lot)
                .WithMany(e => e.Products)
                .HasForeignKey(e => e.LotId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Product)
                .WithMany(e => e.Lots)
                .HasForeignKey(e => e.ProductId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
