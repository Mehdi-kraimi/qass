﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class PublishingContractEntityConfigurations : IEntityTypeConfiguration<PublishingContract>
    {
        public void Configure(EntityTypeBuilder<PublishingContract> builder)
        {
            builder.ToTable("PublishingContract");

            builder.Property(e => e.Tags)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Dictionary<string, string>>())
                 .HasColumnType("LONGTEXT");

            builder.HasOne(e => e.User)
                .WithMany(e => e.PublishedContracts)
                .HasForeignKey(e => e.UserId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}