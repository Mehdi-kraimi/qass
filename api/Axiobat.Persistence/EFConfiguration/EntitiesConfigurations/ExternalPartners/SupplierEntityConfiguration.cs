﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class SupplierEntityConfiguration : ExternalPartnerEntityConfiguration<Supplier>, IEntityTypeConfiguration<Supplier>
    {
        public override void Configure(EntityTypeBuilder<Supplier> builder)
        {
            base.Configure(builder);

            // entity table name
            builder.ToTable("Suppliers");

            // conversions
            builder.Property(e => e.Address)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Address>())
                 .HasColumnType("LONGTEXT");
        }
    }
}
