﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Axiobat.Domain.Enums;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    /// <summary>
    /// the entity <see cref="User"/> database Configuration
    /// </summary>
    public class RoleEntityConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable("Roles");

            // properties configuration
            builder.Property(e => e.Name)
                .HasMaxLength(250)
                .IsRequired();

            builder.Property(e => e.Permissions)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<ICollection<Permissions>>())
                .HasColumnType("LONGTEXT");

            // index configuration
            builder.HasIndex(e => e.Name);

            builder
                .HasIndex(r => r.NormalizedName)
                .HasName("RoleNameIndex");

            builder
                .HasMany(r => r.Users)
                .WithOne(e => e.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
