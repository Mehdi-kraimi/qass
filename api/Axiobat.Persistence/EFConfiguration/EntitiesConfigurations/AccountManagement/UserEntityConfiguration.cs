﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    /// <summary>
    /// the entity <see cref="User"/> database Configuration
    /// </summary>
    internal class UserEntityConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.Property(e => e.UserName)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(e => e.Email)
                .HasMaxLength(50);

            builder.Property(e => e.Reference)
                .HasMaxLength(50);

            builder.Property(e => e.ChangesHistory)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<List<ChangesHistory>>())
                .HasColumnType("LONGTEXT");

            builder
                .Property(u => u.ConcurrencyStamp)
                .IsConcurrencyToken();

            // index
            builder.HasIndex(e => e.UserName)
                .IsUnique();

            builder.HasIndex(u => u.NormalizedUserName)
                .HasName("UserNameIndex").IsUnique();

            builder
                .HasIndex(u => u.Email)
                .IsUnique();

            builder
                .HasIndex(u => u.NormalizedEmail)
                .HasName("EmailIndex")
                 .IsUnique();

            // relationShips
        }

        /// <summary>
        /// get the assembly for registering the configuration
        /// </summary>
        /// <returns><see cref="System.Reflection.Assembly"/></returns>
        internal static System.Reflection.Assembly GetAssembly()
            => typeof(UserEntityConfiguration).Assembly;
    }
}
