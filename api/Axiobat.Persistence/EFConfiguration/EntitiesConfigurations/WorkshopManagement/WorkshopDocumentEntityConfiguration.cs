﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Axiobat.Domain.Enums;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class WorkshopDocumentRubricConfiguration : IEntityTypeConfiguration<WorkshopDocumentRubric>
    {
        public void Configure(EntityTypeBuilder<WorkshopDocumentRubric> builder)
        {
            builder.Property(e => e.Type)
                .HasDefaultValue(DocumentRubricType.Custom);

            builder.HasOne(e => e.Workshop)
                .WithMany(e => e.Rubrics)
                .HasForeignKey(e => e.WorkshopId)
                .OnDelete(DeleteBehavior.SetNull)
                .IsRequired(false);
        }
    }

    internal class WorkshopDocumentEntityConfiguration : IEntityTypeConfiguration<WorkshopDocument>
    {
        public void Configure(EntityTypeBuilder<WorkshopDocument> builder)
        {
            // table name
            builder.ToTable("WorkshopDocuments");

            // properties configuration
            builder.Property(e => e.Comment)
                .HasMaxLength(500);

            // converters
            builder.Property(e => e.ChangesHistory)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ChangesHistory>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Attachments)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Attachment>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.User)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<MinimalUser>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne(e => e.Rubric)
                .WithMany(e => e.Documents)
                .HasForeignKey(e => e.RubricId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.Workshop)
               .WithMany(e => e.Documents)
               .HasForeignKey(e => e.WorkshopId)
               .IsRequired(true)
               .OnDelete(DeleteBehavior.Cascade);
        }
    }
}