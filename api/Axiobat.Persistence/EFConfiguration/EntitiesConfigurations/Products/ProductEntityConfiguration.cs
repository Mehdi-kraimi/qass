﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    public class ProductEntityConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products");

            // indexes
            builder.HasIndex(e => e.Reference);

            // properties configuration
            builder.Property(e => e.ChangesHistory)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<List<ChangesHistory>>())
                .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memo)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<ICollection<Memo>>())
                .HasColumnType("LONGTEXT");

            builder.HasQueryFilter(e => !e.IsDeleted);

            // relationships
            builder.HasOne(e => e.Classification)
                .WithMany(e => e.Products)
                .HasForeignKey(e => e.ClassificationId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.ProductCategoryType)
                .WithMany(e => e.Products)
                .HasForeignKey(e => e.ProductCategoryTypeId)
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
