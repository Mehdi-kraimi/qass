﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;
    using System.Security.Policy;

    public class MaintenanceContractEntityConfiguration : IEntityTypeConfiguration<MaintenanceContract>
    {
        public void Configure(EntityTypeBuilder<MaintenanceContract> builder)
        {
            // the table name
            builder.ToTable("MaintenanceContracts");

            // properties;
            builder.Property(e => e.Status)
                .HasMaxLength(20);

            builder.Property(e => e.Reference)
                .HasMaxLength(50);

            // query filter
            builder.HasQueryFilter(e => !e.IsDeleted);

            // converters
            builder.Property(e => e.OrderDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<WorkshopOrderDetails>())
                 .HasColumnType("LONGTEXT");

            // conversions
            builder.Property(e => e.Site)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<Address>())
                .HasColumnType("LONGTEXT");

            builder.Property(e => e.PublishingContracts)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<PublishingContractMinimal>>() ?? new HashSet<PublishingContractMinimal>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Memo>>() ?? new HashSet<Memo>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.ChangesHistory)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ChangesHistory>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Attachments)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Attachment>>() ?? new HashSet<Attachment>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.EquipmentMaintenance)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<MaintenanceContractEquipmentDetails>>() ?? new HashSet<MaintenanceContractEquipmentDetails>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Client)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ClientDocument>())
                 .HasColumnType("LONGTEXT");

            // indexes
            builder.HasIndex(e => e.Status);
            builder.HasIndex(e => e.Reference);

            // relationships
            builder.HasOne(e => e.RecurringDocument)
                .WithOne(e => e.MaintenanceContract)
                .HasForeignKey<MaintenanceContract>(e => e.RecurringDocumentId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne<Client>()
                .WithMany(e => e.MaintenanceContracts)
                .HasForeignKey(e => e.ClientId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade); // when deleting a client his contract will be deleted
        }
    }
}
