﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Axiobat.Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class OperationSheetEntityConfiguration : IEntityTypeConfiguration<OperationSheet>
    {
        public void Configure(EntityTypeBuilder<OperationSheet> builder)
        {
            // table name
            builder.ToTable("OperationSheets");

            // properties
            builder.Ignore(e => e.DocumentType);

            builder.Property(e => e.Reference)
               .HasMaxLength(50);

            builder.Property(e => e.Purpose)
                .HasMaxLength(500);

            builder.Property(e => e.Status)
                .HasMaxLength(50);

            builder.Property(e => e.Report)
                .HasColumnType("LONGTEXT");

            builder.Property(e => e.GoogleCalendarId)
                .HasMaxLength(500);

            // default query filter
            builder.HasQueryFilter(e => !e.IsDeleted);

            // indexes
            builder.HasIndex(e => e.Reference);

            // converters
            builder.Property(e => e.ChangesHistory)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ChangesHistory>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.OrderDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<OrderDetails>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.ModeStatut)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<List<ModeStatut>>() ?? new List<ModeStatut>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.AddressIntervention)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Address>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.ClientSignature)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Signature>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.TechnicianSignature)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Signature>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Emails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<DocumentEmail>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<Memo>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Client)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<ClientDocument>())
                .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne(e => e.Invoice)
                .WithMany(e => e.OperationSheets)
                .HasForeignKey(e => e.InvoiceId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.Quote)
                .WithMany(e => e.OperationSheets)
                .HasForeignKey(e => e.QuoteId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.Workshop)
                .WithMany(e => e.OperationSheets)
                .HasForeignKey(e => e.WorkshopId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne<Client>()
                .WithMany(e => e.OperationSheets)
                .HasForeignKey(e => e.ClientId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
