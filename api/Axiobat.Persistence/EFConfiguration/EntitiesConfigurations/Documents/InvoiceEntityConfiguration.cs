﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class InvoiceEntityConfiguration : DocumentEntityConfiguration<Invoice>, IEntityTypeConfiguration<Invoice>
    {
        public override void Configure(EntityTypeBuilder<Invoice> builder)
        {
            // set base configuration
            base.Configure(builder);

            // table name
            builder.ToTable("Invoices");

            builder.Property(e => e.AddressIntervention)
              .HasConversion(
                  entity => entity.ToJson(false, false),
                  json => json.FromJson<Address>())
              .HasColumnType("LONGTEXT");

            // converters
            builder.Property(e => e.Emails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<DocumentEmail>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.OrderDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<WorkshopOrderDetails>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Client)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ClientDocument>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
                .HasConversion(
                    entity => entity.ToJson(false, false),
                    json => json.FromJson<ICollection<Memo>>())
                .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne(e => e.Quote)
                .WithMany(e => e.Invoices)
                .HasForeignKey(e => e.QuoteId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne<Client>()
                .WithMany(e => e.Invoices)
                .HasForeignKey(e => e.ClientId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.Workshop)
                .WithMany(e => e.Invoices)
                .HasForeignKey(e => e.WorkshopId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.Contract)
                .WithMany(e => e.Invoices)
                .HasForeignKey(e => e.ContractId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(e => e.OperationSheetMaintenance)
                .WithOne(e => e.Invoice)
                .HasForeignKey<Invoice>(e => e.OperationSheetMaintenanceId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
