﻿namespace Axiobat.Persistence.DataContext.EntitiesConfigurations
{
    using App.Common;
    using Domain.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using System.Collections.Generic;

    internal class QuoteEntityConfiguration : DocumentEntityConfiguration<Quote>, IEntityTypeConfiguration<Quote>
    {
        public override void Configure(EntityTypeBuilder<Quote> builder)
        {
            // set base configuration
            base.Configure(builder);

            // table name
            builder.ToTable("Quotes");

            // converters
            builder.Property(e => e.AddressIntervention)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Address>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Emails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<DocumentEmail>>() ?? new HashSet<DocumentEmail>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Memos)
             .HasConversion(
                 entity => entity.ToJson(false, false),
                 json => json.FromJson<ICollection<Memo>>() ?? new HashSet<Memo>())
             .HasColumnType("LONGTEXT");

            builder.Property(e => e.Situations)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ICollection<QuoteSituations>>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.OrderDetails)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<WorkshopOrderDetails>())
                 .HasColumnType("LONGTEXT");

            // converters
            builder.Property(e => e.ClientSignature)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<Signature>())
                 .HasColumnType("LONGTEXT");

            builder.Property(e => e.Client)
                 .HasConversion(
                     entity => entity.ToJson(false, false),
                     json => json.FromJson<ClientDocument>())
                 .HasColumnType("LONGTEXT");

            // relationships
            builder.HasOne(e => e.Workshop)
                .WithMany(e => e.Quotes)
                .HasForeignKey(e => e.WorkshopId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne<Client>()
                .WithMany(e => e.Quotes)
                .HasForeignKey(e => e.ClientId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
