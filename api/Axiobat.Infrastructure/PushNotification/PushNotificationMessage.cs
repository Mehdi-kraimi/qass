﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines a PushNotification Message
    /// </summary>
    public partial class PushNotificationMessage
    {
        /// <summary>
        /// the id of the push Notification message template, must be on of <see cref="TemplateMessages"/>
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// list of Title content
        /// </summary>
        public IEnumerable<LanguageContent> TitleContent { get; set; }

        /// <summary>
        /// list of Body Content
        /// </summary>
        public IEnumerable<LanguageContent> BodyContent { get; set; }

        /// <summary>
        /// set the place holders values to the <see cref="BodyContent"/> content
        /// </summary>
        /// <param name="placeHolders">the list of place holders</param>
        public void SetPlaceHolders(PlaceHolder[] placeHolders)
        {
            foreach (var content in BodyContent)
            {
                content.Content = PlaceHolder.Set(content.Content, placeHolders);
            }
        }

        /// <summary>
        /// return the Body content as an object example : { "fr" : "c'est un contenu", "en" : "this is a content" }
        /// </summary>
        /// <returns>the object instant</returns>
        public object GetContentAsObject()
        {
            var content = new Newtonsoft.Json.Linq.JObject();
            foreach (var item in BodyContent)
                content.Add(item.LanguageCode, item.Content);

            return content;
        }

        /// <summary>
        /// return the titles content as an object example : { "fr" : "c'est un titre", "en" : "this is a title" }
        /// </summary>
        /// <returns>the object instant</returns>
        public object GetTitlesAsObject()
        {
            var content = new Newtonsoft.Json.Linq.JObject();
            foreach (var item in TitleContent)
                content.Add(item.LanguageCode, item.Content);

            return content;
        }
    }

    /// <summary>
    /// partial part for <see cref="PushNotificationMessage"/>
    /// </summary>
    public partial class PushNotificationMessage
    {
        /// <summary>
        /// create an instant of <see cref="PushNotificationMessage"/>
        /// </summary>
        public PushNotificationMessage()
        {
            TitleContent = new HashSet<LanguageContent>();
            BodyContent = new HashSet<LanguageContent>();
        }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => $"id: {Id}, has {TitleContent.Count()} Titles, and {BodyContent.Count()} Body Content";
    }
}
