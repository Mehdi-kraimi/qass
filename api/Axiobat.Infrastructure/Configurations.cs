﻿namespace Axiobat.Infrastructure
{
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.MailService;
    using Application.Services.PushNotification;
    using Axiobat.Infrastructure.ImportService;
    using FileService.Internal;
    using FileServices;
    using FileServices.Helpers;
    using GoogleService;
    using MailService;
    using Microsoft.Extensions.DependencyInjection;
    using PushNotification;
    using SharedServices.EmailServices.Extension;
    using SharedServices.FileServices.Extension;
    using System;

    /// <summary>
    /// the extensions class for registering the layer services
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public static class Configurations
    {
        /// <summary>
        /// add the infrastructure layer services
        /// </summary>
        /// <param name="builder">the extension builder</param>
        public static ConfigurationsBuilder AddInfrastructure(this ConfigurationsBuilder builder, Action<InfrastructureOptions> options)
        {
            // validate the options
            if (options is null)
                throw new ArgumentNullException("the given options are null");

            // create the options instant
            var option = InfrastructureOptions.Initialize(options);

            // Services
            builder.Services.AddScoped<ExcelService>();
            builder.Services.AddScoped<FilesManager>();
            builder.Services.AddScoped<IFileService, FileService>();
            builder.Services.AddScoped<IEmailService, EmailService>();
            builder.Services.AddScoped<IDocumentService, DocumentService>();
            builder.Services.AddScoped<IDataImportService, DataImportService>();
            builder.Services.AddScoped<IGoogleCalendarService, GoogleCalendarService>();
            builder.Services.AddScoped<IPushNotificationService, PushNotificationService>();

            // shared services configurations
            builder.Services.AddFileService(config =>
            {
                config.ProjectId = builder.ProjectId;
                config.ServiceURL = option.FileServiceURL;
            });

            builder.Services.AddEmailService(config =>
            {
                config.ProjectId = builder.ProjectId;
                config.ServiceURL = option.EmailServiceURL;
            });

            return builder;
        }
    }

    /// <summary>
    /// the options used to configure the Infrastructure layer
    /// </summary>
    public class InfrastructureOptions
    {
        /// <summary>
        /// the file service URL
        /// </summary>
        public string FileServiceURL { get; set; }

        /// <summary>
        /// the email service URL
        /// </summary>
        public string EmailServiceURL { get; set; }

        /// <summary>
        /// create and validate a new instant of <see cref="InfrastructureOptions"/> generated from the given options builder
        /// </summary>
        /// <param name="options">the options</param>
        /// <returns>a new instant of <see cref="InfrastructureOptions"/></returns>
        internal static InfrastructureOptions Initialize(Action<InfrastructureOptions> options)
        {
            var option = new InfrastructureOptions();
            options(option);
            Validate(option);
            return option;
        }

        /// <summary>
        /// validate  the given options
        /// </summary>
        /// <param name="option">the options to validate</param>
        private static void Validate(InfrastructureOptions option)
        {
            if (option is null)
                throw new ArgumentNullException();
        }
    }
}
