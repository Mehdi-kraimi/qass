﻿namespace Axiobat.Infrastructure.FileServices.Helpers
{
    using App.Common;
    using Axiobat.Application.Models;
    using Axiobat.Domain.Constants;
    using Axiobat.Domain.Entities.Configuration;
    using Axiobat.Domain.Enums;
    using Domain.Entities;
    using iTextSharp.text;
    using iTextSharp.text.html.simpleparser;
    using iTextSharp.text.pdf;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public partial class MaintenanceOperationSheetPDFService
    {
        /// <summary>
        /// generate a PDF for the quote
        /// </summary>
        /// <param name="operationSheet">the operationSheet document instant</param>
        /// <returns>the PDF document as Byte[]</returns>
        internal byte[] Generate()
        {
            using (var memoryStream = new MemoryStream())
            {
                // create document and PDF writer
                var document = new iTextSharp.text.Document(PageSize.A4, 30, 30, 5, 60);
                var writer = PdfWriter.GetInstance(document, memoryStream);
                writer.PageEvent = new Footer(_pdfOptions);

                // open the document
                document.Open();

                // add document header section
                document.Add(CreateHeaderSection(_configuration, "Fiche maintenance d'intervention", _operationSheet.Reference, _operationSheet.Client.Reference, _operationSheet.Client.FullName, _operationSheet.CreatedOn.DateTime, _operationSheet.EndDate, _pdfOptions));

                // add document Address informations section
                document.Add(CreateDocumentAddressSection(_operationSheet.AddressIntervention , _operationSheet.StartDate, _operationSheet.EndDate , _operationSheet.VisitsCount, _operationSheet.TotalBasketConsumption));


                if (_operationSheet.Type == MaintenanceOperationSheetType.AfterSalesService)
                {
                    // Add order details section
                    CreateOrderDetailsSection(document, _operationSheet.Purpose, _operationSheet.OrderDetails);

                }
                if (_operationSheet.Type == MaintenanceOperationSheetType.Maintenance)
                {
                    // Add order details section
                    BodyEquipement(document, _operationSheet.Purpose, _operationSheet);
                }

                // add rapport
                document.Add(CreateRapport(_operationSheet.Report));

                // add technicien

                document.Add(CreateTechnicien(_operationSheet));

                // close document
                document.Close();

                // export the file as byte array
                return memoryStream.ToArray();
            }
        }
    }


    public partial class MaintenanceOperationSheetPDFService
    {
        private readonly MaintenanceOperationSheet _operationSheet;
        private readonly DocumentConfiguration _configuration;
        private readonly PdfOptions _pdfOptions;

        public MaintenanceOperationSheetPDFService(MaintenanceOperationSheet operationSheet , DocumentConfiguration configuration,PdfOptions pdfOptions)
        {
            _operationSheet = operationSheet;
            _pdfOptions = pdfOptions;
            _configuration = configuration;
        }

        private static IElement CreateRapport(string Rapport)
        {
            PdfPTable rapport = new PdfPTable(1)
            { SpacingBefore = 5f, WidthPercentage = 100 };

              rapport.DefaultCell.Border = Rectangle.NO_BORDER;
              rapport.WidthPercentage = 100;
              rapport.AddCell(new PdfPCell() { FixedHeight = 10f, BorderWidth = 0 });

              rapport.AddCell(new PdfPCell(new Paragraph("Rapport", PDFFonts.H10)) { PaddingLeft = 50f, FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, });
              rapport.AddCell(new PdfPCell(new Paragraph(Rapport == null ? "" : Rapport, PDFFonts.H10)) { BorderWidth = 0, PaddingLeft = 15f, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_JUSTIFIED_ALL });

              return rapport;
        }

        private static IElement CreateTechnicien(MaintenanceOperationSheet ficheIntervention)
        {

            PdfPTable signaturevisa = new PdfPTable(1) { SpacingBefore = 5f, WidthPercentage = 100 };
            signaturevisa.SpacingAfter = 15f;
            signaturevisa.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPTable visa = new PdfPTable(1) { WidthPercentage = 100 };
            visa.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPTable signature = new PdfPTable(2) { WidthPercentage = 100 };

            signature.DefaultCell.Border = Rectangle.NO_BORDER;
            // signature.SetWidths(new float[] { 350f, 70f, 550f });
            visa.AddCell(new PdfPCell(new Paragraph(new Chunk("Visa", PDFFonts.H10))) { PaddingLeft = 50f, FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE });
            signaturevisa.AddCell(new PdfPCell(visa) { BorderWidth = 0, PaddingTop = 10 });
            PdfPTable signature_technicien = new PdfPTable(1);
            signature_technicien.DefaultCell.Border = Rectangle.NO_BORDER;
            signature_technicien.WidthPercentage = 100;
            PdfPTable signature_client = new PdfPTable(1);

            // SignatureModel signetureClient= JsonConvert.DeserializeObject<SignatureModel>(ficheIntervention.SignatureClient);

            //Signature tECHNICIEN

            signature_technicien.AddCell(new PdfPCell(new Paragraph(new Chunk("Technicien", PDFFonts.H12Bold))) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE });
            Phrase PhrasesignetureTechnom = new Phrase();
            Phrase PhrasesignetureTechdate = new Phrase();
            Phrase PhrasesignetureTechsignature = new Phrase();
            Signature signatureTech = null;


            if (ficheIntervention.TechnicianSignature != null)
            {

                signatureTech = ficheIntervention.TechnicianSignature;

            }


            //  idTechnicien
            if (signatureTech != null)
            {
                PhrasesignetureTechnom.Add(new Chunk("Nom : ", PDFFonts.H12Bold));

                PhrasesignetureTechnom.Add(new Chunk((signatureTech == null) ? " " : signatureTech.Name, PDFFonts.H10));
                PhrasesignetureTechnom.Add(new Chunk(signatureTech.Name, PDFFonts.H10));
                PhrasesignetureTechdate.Add(new Chunk("Date : ", PDFFonts.H12Bold));

                var SignaturedateTech = (signatureTech.SignatureDate == null) ? "" : signatureTech.SignatureDate.ToString("dd/MM/yyyy");

                PhrasesignetureTechdate.Add(new Chunk(SignaturedateTech, PDFFonts.H10));
            }


            PhrasesignetureTechsignature.Add(new Chunk("Signature : ", PDFFonts.H12Bold));
            PdfPTable signatureTable = new PdfPTable(1);
            if (signatureTech != null)
            {
                if (!string.IsNullOrEmpty(signatureTech.ImageContent))
                {
                    ConvertBase64ToImage convert = new ConvertBase64ToImage();
                    string content = convert.Replace(signatureTech.ImageContent);
                    if (convert.IsBase64(content))
                    {
                        byte[] imageBytes = Convert.FromBase64String(content);
                        System.Drawing.Image result = convert.ByteArrayToImage(imageBytes);
                        Image Signatu = Image.GetInstance(result, null, false);
                        Signatu.ScaleAbsolute(160f, 160f);

                        signatureTable.AddCell(new PdfPCell(Signatu) { BorderWidth = Rectangle.NO_BORDER });
                    }
                    else { signatureTable.AddCell(new PdfPCell(new Phrase(" ")) { BorderWidth = Rectangle.NO_BORDER }); }

                }
            }
            else { signatureTable.AddCell(new PdfPCell(new Phrase(" ")) { BorderWidth = Rectangle.NO_BORDER }); }

            signature_technicien.AddCell(new PdfPCell(PhrasesignetureTechnom) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            signature_technicien.AddCell(new PdfPCell(PhrasesignetureTechdate) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            signature_technicien.AddCell(new PdfPCell(PhrasesignetureTechsignature) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            signature_technicien.AddCell(new PdfPCell(signatureTable) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderTop, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            signature.AddCell(new PdfPCell(signature_technicien) { BorderWidth = 0 });

            //Signature CLIENT


            Phrase phrasesignatureClientnom = new Phrase();
            Phrase phrasesignatureClientdate = new Phrase();
            Phrase phrasesignatureClientsignature = new Phrase();

            signature_client.AddCell(new PdfPCell(new Paragraph(new Chunk("Client", PDFFonts.H12Bold))) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE });

            Signature SignatureClient = null;


            if (ficheIntervention.ClientSignature != null)
            {
                SignatureClient = ficheIntervention.ClientSignature;

            }

            if (SignatureClient != null)
            {
                phrasesignatureClientnom.Add(new Chunk("Nom : ", PDFFonts.H12Bold));
                phrasesignatureClientnom.Add(new Chunk((SignatureClient == null) ? " " : SignatureClient.Name, PDFFonts.H10));
                phrasesignatureClientdate.Add(new Chunk("Date : ", PDFFonts.H12Bold));
                var Signaturedateclient = (SignatureClient.SignatureDate == null) ? "" : SignatureClient.SignatureDate.ToString("dd/MM/yyyy");

                phrasesignatureClientdate.Add(new Chunk(Signaturedateclient, PDFFonts.H10));

            }


            phrasesignatureClientsignature.Add(new Chunk("Signature : ", PDFFonts.H12Bold));
            PdfPTable signatureClientTable = new PdfPTable(1);

            if (SignatureClient != null)
            {
                if (!string.IsNullOrEmpty(SignatureClient.ImageContent))
                {
                    ConvertBase64ToImage convert = new ConvertBase64ToImage();
                    string content = convert.Replace(SignatureClient.ImageContent);
                    if (convert.IsBase64(content))
                    {
                        byte[] imageBytes = Convert.FromBase64String(content);
                        System.Drawing.Image result = convert.ByteArrayToImage(imageBytes);
                        Image Signatu = Image.GetInstance(result, null, false);
                        Signatu.ScaleAbsolute(160f, 160f);

                        signatureClientTable.AddCell(new PdfPCell(Signatu) { BorderWidth = Rectangle.NO_BORDER });
                    }
                    else
                        signatureClientTable.AddCell(new PdfPCell(new Phrase(" ")) { BorderWidth = Rectangle.NO_BORDER });
                }
            }
            else
            {
                signatureClientTable.AddCell(new PdfPCell(new Phrase(" ")) { BorderWidth = Rectangle.NO_BORDER });
            }

            signature_client.AddCell(new PdfPCell(phrasesignatureClientnom) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            signature_client.AddCell(new PdfPCell(phrasesignatureClientdate) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            signature_client.AddCell(new PdfPCell(phrasesignatureClientsignature) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            signature_client.AddCell(new PdfPCell(signatureClientTable) { BorderWidth = 0.75f, Padding = 5f, BorderColor = PDFColors.BorderTop, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            signature.AddCell(new PdfPCell(signature_client) { BorderWidth = 0 });

            signature.AddCell(new PdfPCell() { BorderWidth = 0 });
            signaturevisa.AddCell(new PdfPCell(signature) { BorderWidth = 0, PaddingTop = 10 });

            return signaturevisa;

        }

        private static void CreateOrderDetailsSection(iTextSharp.text.Document PDFDocument, string purpose, OrderDetails orderDetails)
        {
            PdfPTable prestations = new PdfPTable(1)
            {
                SpacingBefore = 10f,
                WidthPercentage = 100
            };
            prestations.DefaultCell.Border = Rectangle.NO_BORDER;
            prestations.AddCell(new PdfPCell() { FixedHeight = 7f, BorderWidth = 0 });

            PdfPTable articlestop = new PdfPTable(1)
            {
                WidthPercentage = 100
            };
            articlestop.DefaultCell.Border = Rectangle.NO_BORDER;
            if (purpose.IsValid())
            {
                articlestop.AddCell(new PdfPCell(new Paragraph(new Chunk(purpose, PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                prestations.AddCell(new PdfPCell(articlestop) { BorderWidth = 0, PaddingBottom = 10 });
                PDFDocument.Add(prestations);
            }

            PdfPTable articlestab = new PdfPTable(11)
            { SpacingBefore = 10f, WidthPercentage = 100 };
            articlestab.DefaultCell.Border = Rectangle.NO_BORDER;

            // Header table
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Article", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Désignation", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Catégorie", PDFFonts.H10))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
            articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Quantité", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });


            foreach (var productDetails in orderDetails.LineItems)
            {
                if (productDetails.Type == Domain.Enums.ProductType.Product)
                {
                    var product = (productDetails.Product as JObject).ToObject<MinimalProductDetails>();
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(product.Reference, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(product.Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(product.Category != null ? product.Category.Label : " ", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0}", productDetails.Quantity), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                }

                if (productDetails.Type == Domain.Enums.ProductType.Lot)
                {
                    var Lot = (productDetails.Product as JObject).ToObject<MinimalLotDetails>();
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(Lot.Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(" Sous Total HT: " + string.Format("{0:0.00}", Lot.TotalHT), PDFFonts.HItalic))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });

                    foreach (var lotProductDetails in Lot.Products)
                    {
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("     " + lotProductDetails.ProductDetails.Reference, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(lotProductDetails.ProductDetails.Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(lotProductDetails.ProductDetails.Category != null ? lotProductDetails.ProductDetails.Category.Label : " ", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(string.Format("{0}", lotProductDetails.Quantity), PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                    }
                }

                PDFDocument.Add(articlestab);
                articlestab.DeleteBodyRows();
            }

            if (orderDetails.LineItems.Count < 4)
            {
                for (int i = 0; i < 3; i++)
                {
                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15)))
                    {
                        BorderWidthBottom = 0,
                        BorderWidthLeft = 0,
                        BorderWidth = 0,
                        Padding = 5f,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Colspan = 12
                    });

                    PDFDocument.Add(articlestab);
                    articlestab.DeleteBodyRows();
                }
            }
            else
            {
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15)))
                {
                    BorderWidthBottom = 0,
                    BorderWidthLeft = 0,
                    BorderWidth = 0,
                    Padding = 5f,
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    Colspan = 12
                });

                PDFDocument.Add(articlestab);
                articlestab.DeleteBodyRows();
            }
            prestations.DeleteBodyRows();
        }


        private static void BodyEquipement(iTextSharp.text.Document PDFDocument, string purpose, MaintenanceOperationSheet operationSheet)
        {

                PdfPTable prestations = new PdfPTable(1);
                prestations.WidthPercentage = 100;
                prestations.DefaultCell.Border = Rectangle.NO_BORDER;
                prestations.AddCell(new PdfPCell() { FixedHeight = 7f, BorderWidth = 0 });

                PdfPTable articlestop = new PdfPTable(1);
                articlestop.WidthPercentage = 100;
                articlestop.DefaultCell.Border = Rectangle.NO_BORDER;

                if (purpose != null && purpose != "")
                {
                    articlestop.AddCell(new PdfPCell(new Paragraph(new Chunk(purpose, PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_CENTER, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                    prestations.AddCell(new PdfPCell(articlestop) { BorderWidth = 0, PaddingBottom = 10 });
                    PDFDocument.Add(prestations);
                }

                PdfPTable articlestab = new PdfPTable(11)
                { SpacingBefore = 5f, WidthPercentage = 100 };
                articlestab.DefaultCell.Border = Rectangle.NO_BORDER;

                // Header table
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Gamme maintenance d'équipement", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 6 });
                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("Observations", PDFFonts.H10B))) { FixedHeight = 20f, BorderWidth = 0, BackgroundColor = PDFColors.TableHeader, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 5 });


                List<MaintenanceContractEquipmentDetails> equipmentMaintenance = operationSheet.EquipmentDetails.ToList();
                if (equipmentMaintenance != null && equipmentMaintenance.Count > 0)
                {

                var ListObservation = operationSheet.Observations;


                var length = equipmentMaintenance.Count;

                    for (int i = 0; i < length; i++)
                    {
                        var dataEquipement = equipmentMaintenance[i];
                        List<EquipmentMaintenanceOperation> LibelleLot = dataEquipement.MaintenanceOperations.ToList();


                    var taille = LibelleLot.Count;
                        articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(equipmentMaintenance[i].EquipmentName, PDFFonts.H12Bold))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BackgroundColor = PDFColors.BorderBotom, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 11 });


                            for (int j = 0; j < taille; j++)
                            {
                                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("    " + LibelleLot[j].Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 11 });

                                List<EquipmentMaintenanceOperation> operationsLot = LibelleLot[j].SubOperations.ToList();
                                var lengthoperation = operationsLot.Count;

                            for (int k = 0; k < lengthoperation; k++)
                            {
                                    articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("       " + operationsLot[k].Name, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 6 });
                            var observation = ListObservation.Where(x => x.Id == operationsLot[k].Id).FirstOrDefault();
                            if (observation != null)
                            {
                                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(observation.Observation, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                                var text = "";
                                if (observation.Status == StatutOperation.NotFait)
                                {
                                    text = "Non fait";
                                }
                                if (observation.Status == StatutOperation.Fait)
                                {
                                    text = "Fait";
                                }
                                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk(text, PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

                            }
                            else
                            {
                                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 3 });
                                articlestab.AddCell(new PdfPCell(new Paragraph(new Chunk("", PDFFonts.H15))) { BorderWidthBottom = 0.3f, BorderWidthLeft = 0, BorderWidth = 0, Padding = 5f, BorderColorBottom = PDFColors.BorderBotom, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
                            }
                            }
                         }

                    PDFDocument.Add(articlestab);
                    articlestab.DeleteBodyRows();
                }

                prestations.DeleteBodyRows();

            }
        }

        private static IElement CreatePaymentConditionSection(string paymentCondition)
        {
            PdfPTable conditiontab = new PdfPTable(1)
            {
                SpacingBefore = 5f,
                WidthPercentage = 100,
                SpacingAfter = 10f,
            };
            if (paymentCondition != "")
            {
                conditiontab.AddCell(new PdfPCell(new Paragraph(new Chunk("Conditions de réglement :", PDFFonts.H10B)))
                { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });
            }

            Phrase phrase = new Phrase
            {
                CreateHTMLParagraph(paymentCondition)
            };

            conditiontab.AddCell(new PdfPCell(phrase) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });
            return conditiontab;
        }

        private static IElement CreateFooterSection(string documentNote)
        {
            PdfPTable piedtab = new PdfPTable(1)
            {
                SpacingBefore = 5f,
                WidthPercentage = 100,
                SpacingAfter = 10f
            };
            if (documentNote != "")
            {
                piedtab.AddCell(new PdfPCell(new Paragraph(new Chunk("Notes :", PDFFonts.H10B)))
                { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingLeft = 0 });
            }

            PdfPTable pdfTab = new PdfPTable(1);
            pdfTab.AddCell(new PdfPCell(CreateHTMLParagraph(documentNote)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            piedtab.AddCell(new PdfPCell(pdfTab) { BorderWidth = 0 });

            return piedtab;
        }


        private static IElement CreateDocumentAddressSection(Address AddressIntervention , DateTime StartDate , DateTime EndDate , int visitsCount , int totalBasketConsumption)
        {


            PdfPTable table_header = new PdfPTable(2)
            {
                WidthPercentage = 100
            };


            // Left header
            PdfPTable leftRow = new PdfPTable(1);

            //adresse INTERVENTION
            leftRow.DefaultCell.Border = Rectangle.NO_BORDER;

            leftRow.AddCell(new Paragraph("ADRESSE INTERVENTION :", PDFFonts.H10B));
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk((AddressIntervention == null) ? " " : AddressIntervention.Street, PDFFonts.H10))) { Padding = 1f, BorderWidth = 0 });
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk((AddressIntervention == null) ? " " : AddressIntervention.Complement, PDFFonts.H10))) { Padding = 1f, BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT });
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk((AddressIntervention == null) ? " " : AddressIntervention.PostalCode + " " + AddressIntervention.City, PDFFonts.H10))) { Padding = 1f, BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT });

            table_header.AddCell(new PdfPCell(leftRow) { BorderWidth = 0, PaddingTop = 10 });

            PdfPTable rightRow = new PdfPTable(1);
            rightRow.WidthPercentage = 80;
            rightRow.DefaultCell.Border = Rectangle.NO_BORDER;
            //Date

            Phrase phraseDateDebut = new Phrase();

            var datedebut = (StartDate == null) ? "" : StartDate.ToString("dd/MM/yyyy");

            phraseDateDebut.Add(new Chunk("Date début : ", PDFFonts.H12Bold));
            phraseDateDebut.Add(new Chunk(datedebut, PDFFonts.H15));
            rightRow.AddCell(new PdfPCell(phraseDateDebut) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE });

            Phrase phraseDateFin = new Phrase();

            var dateFin = (EndDate == null) ? "" : EndDate.ToString("dd/MM/yyyy");

            phraseDateFin.Add(new Chunk("Date Fin : ", PDFFonts.H12Bold));
            phraseDateFin.Add(new Chunk(dateFin, PDFFonts.H15));
            rightRow.AddCell(new PdfPCell(phraseDateFin) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE });


            Phrase phraseNbrDeplacement = new Phrase();
            var nbrdep = visitsCount.ToString();
            phraseNbrDeplacement.Add(new Chunk("Nombre Déplacement : ", PDFFonts.H12Bold));
            phraseNbrDeplacement.Add(new Chunk(nbrdep, PDFFonts.H15));
            rightRow.AddCell(new PdfPCell(phraseNbrDeplacement) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE });

            //Phrase phraseNbrPanier = new Phrase();
            //var nbrpanier = totalBasketConsumption.ToString();
            //phraseNbrPanier.Add(new Chunk("Nombre Panier : ", PDFFonts.H12Bold));
            //phraseNbrPanier.Add(new Chunk(nbrpanier, PDFFonts.H15));
            //rightRow.AddCell(new PdfPCell(phraseNbrPanier) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE });



            table_header.AddCell(new PdfPCell(rightRow) { BorderWidth = 0, PaddingTop = 10 });
            return table_header;

        }

        private static IElement CreateHeaderSection(DocumentConfiguration configuration, string documentType, string documentReference, string clientReference, string nameClient, DateTime creationDate, DateTime endDate, PdfOptions pdfOptions)
        {
            // logo
            // var logo = Image.GetInstance(FileHelper.GetByteArray(configuration.PDFConfiguration.Logo));

            PdfPTable table_header = new PdfPTable(2)
            {
                WidthPercentage = 100
            };

            // Left header
            PdfPTable leftRow = new PdfPTable(1);
            leftRow.DefaultCell.Border = Rectangle.NO_BORDER;

            // logo.ScaleAbsolute(130f, 50f);
            // leftRow.AddCell(new PdfPCell(logo) { FixedHeight = 50f, BorderWidth = 0, PaddingTop = 10, HorizontalAlignment = Element.ALIGN_LEFT });
            // logo
            ConvertBase64ToImage convert = new ConvertBase64ToImage();
            if (pdfOptions.Images.Logo != null && pdfOptions.Images.Logo != "")
            {
                string content = convert.Replace(pdfOptions.Images.Logo);


                var logo = Image.GetInstance(FileHelper.GetByteArray(content));

                logo.ScaleAbsolute(130f, 50f);
                leftRow.AddCell(new PdfPCell(logo) { FixedHeight = 50f, BorderWidth = 0, PaddingTop = 10, PaddingLeft = 0, HorizontalAlignment = Element.ALIGN_LEFT });

            }
            leftRow.AddCell(new PdfPCell() { FixedHeight = 7f, BorderWidth = 0 });

            //info societe
            var Email = pdfOptions.Header.Email;
            var Address = pdfOptions.Header.Address;

            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk((pdfOptions.Header.CompanyName == null) ? "" : pdfOptions.Header.CompanyName, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });


            if (Address != null)
            {
                leftRow.AddCell(new PdfPCell(new Paragraph((Address.Street == null) ? "" : Address.Street, PDFFonts.H10B)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });
                var ville = (Address.City == null) ? "" : Address.City;
                var PostalCode = (Address.PostalCode == null) ? "" : Address.PostalCode;

                leftRow.AddCell(new PdfPCell(new Paragraph(PostalCode + " " + ville, PDFFonts.H10B)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });

            }
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(string.IsNullOrEmpty(pdfOptions.Header.PhoneNumber) ? "" : "Tél: " + pdfOptions.Header.PhoneNumber, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });
            leftRow.AddCell(new PdfPCell(new Paragraph(new Chunk(string.IsNullOrEmpty(pdfOptions.Header.Email) ? "" : "Email: " + pdfOptions.Header.Email, PDFFonts.H10B))) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_MIDDLE });



            PdfPTable leftRowseco = new PdfPTable(1);
            //info societe
            //leftRowseco.AddCell(new PdfPCell(CreateHTMLParagraph(configuration.Header.EnsureValue())) { BorderWidth = 0, PaddingLeft = 15f, HorizontalAlignment = Element.ALIGN_LEFT, PaddingRight = 80f, VerticalAlignment = Element.ALIGN_LEFT });


            leftRow.AddCell(new PdfPCell(leftRowseco) { BorderWidth = 0 });

            leftRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });
            table_header.AddCell(new PdfPCell(leftRow) { BorderWidth = 0, PaddingTop = 10 });

            // Right table
            PdfPTable rightRow = new PdfPTable(1)
            {
                WidthPercentage = 80,
            };

            rightRow.DefaultCell.Border = Rectangle.NO_BORDER;

            rightRow.AddCell(new PdfPCell() { FixedHeight = 20f, BorderWidth = 0 });
            rightRow.AddCell(new PdfPCell(new Paragraph("FICHE INTERVENTION MAINTENANCE", PDFFonts.H10B)) { PaddingLeft = 50f, BorderWidth = 0 });
            rightRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });

            PdfPTable infoClient = new PdfPTable(6);
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("FICHE INTERVENTION MAINTENANCE n°  ", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Date", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk("Client", PDFFonts.H10B))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, BackgroundColor = PDFColors.TableHeader, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(documentReference, PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            //infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(creationDate.ToString(configuration.PDFConfiguration.DateFormat), PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });
            Phrase phraseDateCreation = new Phrase();
            Phrase phraseDateExpiration = new Phrase();
            PdfPTable tabDateDocument = new PdfPTable(1);

            //   doc.Add(p);
            phraseDateCreation.Add(new Chunk("Date création : ", PDFFonts.H10B));
            phraseDateCreation.Add(new Chunk(creationDate.ToString("dd/MM/yyyy"), PDFFonts.H15));
            phraseDateExpiration.Add(new Chunk("Date Fin : ", PDFFonts.H10B));
            phraseDateExpiration.Add(new Chunk(endDate.ToString("dd/MM/yyyy"), PDFFonts.H15));
            tabDateDocument.AddCell(new PdfPCell(phraseDateCreation) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            tabDateDocument.AddCell(new PdfPCell(phraseDateExpiration) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            infoClient.AddCell(new PdfPCell(tabDateDocument) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });



            //infoClient.AddCell(new PdfPCell(new Paragraph(new Chunk(clientReference, PDFFonts.H15))) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            Phrase phraseClientnom = new Phrase();
            Phrase phraseclientReference = new Phrase();
            PdfPTable clientInfo = new PdfPTable(1);

            //   doc.Add(p);
            phraseClientnom.Add(new Chunk("Nom : ", PDFFonts.H10B));
            phraseClientnom.Add(new Chunk((nameClient == null) ? " " : nameClient, PDFFonts.H15));
            phraseclientReference.Add(new Chunk("Réference : ", PDFFonts.H10B));
            phraseclientReference.Add(new Chunk((clientReference == null) ? " " : clientReference, PDFFonts.H15));
            clientInfo.AddCell(new PdfPCell(phraseClientnom) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });
            clientInfo.AddCell(new PdfPCell(phraseclientReference) { BorderWidth = 0, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_LEFT });

            infoClient.AddCell(new PdfPCell(clientInfo) { BorderWidth = 0.75f, BorderColor = PDFColors.BorderBotom, Padding = 5f, HorizontalAlignment = Element.ALIGN_LEFT, VerticalAlignment = Element.ALIGN_MIDDLE, Colspan = 2 });

            rightRow.AddCell(new PdfPCell(infoClient) { BorderWidth = 0, PaddingTop = 0 });

            rightRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });

            rightRow.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });
            table_header.AddCell(new PdfPCell(rightRow) { BorderWidth = 0, PaddingTop = 0 });

            return table_header;
        }

        private static Paragraph CreateHTMLParagraph(string text)
        {
            Paragraph p = new Paragraph();
            using (StringReader sr = new StringReader(text))
            {
                foreach (IElement e in HtmlWorker.ParseToList(sr, null))
                {
                    p.Add(e);
                }
            }
            return p;
        }

    }
    public partial class Footer : PdfPageEventHelper
    {
        //public override void OnEndPage(PdfWriter wri, iTextSharp.text.Document doc)
        //{
        //    base.OnEndPage(wri, doc);
        //    BaseColor borderTopColor = new BaseColor(224, 224, 224);

        //    Font fontH13 = FontFactory.GetFont("Arial", 14, Font.NORMAL, new BaseColor(161, 165, 169));
        //    Font fontH14 = FontFactory.GetFont("Arial", 7, Font.NORMAL, BaseColor.Black);
        //    int courantPageNumber = wri.CurrentPageNumber;

        //    String pageText = "Page " + courantPageNumber.ToString();
        //    PdfPTable footerTab = new PdfPTable(3);
        //    PdfPTable pdfTab = new PdfPTable(1);
        //    pdfTab.HorizontalAlignment = Element.ALIGN_BOTTOM;
        //    pdfTab.TotalWidth = 550;
        //    pdfTab.AddCell(new PdfPCell() { BorderWidth = 0, FixedHeight = 5f });

        //    PdfPTable footerContenu = new PdfPTable(2);
        //    footerContenu.SetWidths(new float[] { 65f, 35f });
        //    footerContenu.AddCell(new PdfPCell(new Paragraph()) { BorderWidth = 0, PaddingLeft = 15, PaddingTop = 0, HorizontalAlignment = Element.ALIGN_LEFT });
        //    footerContenu.AddCell(new PdfPCell(new Paragraph(pageText, fontH14)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_RIGHT, VerticalAlignment = Element.ALIGN_BOTTOM, PaddingRight = 15 });

        //    pdfTab.AddCell(new PdfPCell(footerContenu) { BorderWidth = 0 });
        //    pdfTab.AddCell(new PdfPCell() { FixedHeight = 5f, BorderWidth = 0 });


        //    PdfPTable footerPied = new PdfPTable(1);

        //    footerPied.AddCell(new PdfPCell() { FixedHeight = 1f, Padding = 3f, BorderWidthBottom = 0, BorderWidthTop = 0.5f, BorderWidth = 0, BorderColorTop = borderTopColor, PaddingTop = 10 });

        //    pdfTab.AddCell(new PdfPCell(footerPied) { BorderWidth = 0, PaddingLeft = 40, PaddingRight = 40 });
        //    pdfTab.AddCell(new PdfPCell(new Paragraph("www.couleur-avenir.fr", fontH13)) { BorderWidth = 0, HorizontalAlignment = Element.ALIGN_CENTER });
        //    pdfTab.WriteSelectedRows(0, -2, 18, 60, wri.DirectContent);


        //    footerTab.AddCell(new PdfPCell(pdfTab) { BorderWidth = 0 });
        //}
    }
}
