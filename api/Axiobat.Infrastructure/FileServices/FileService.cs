﻿namespace Axiobat.Infrastructure.FileServices
{
    using Application.Enums;
    using Application.Models;
    using Application.Services.Configuration;
    using Application.Services.FileService;
    using Application.Services.Localization;
    using Axiobat.Domain.Constants;
    using Axiobat.Domain.Entities.Configuration;
    using Domain.Entities;
    using Domain.Interfaces;
    using FileServices.Helpers;
    using global::FileService.Internal;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// the implementation of <see cref="IFileService"/>
    /// </summary>
    public partial class FileService
    {
        #region File manipulation logic

        /// <summary>
        /// delete the file with the given name
        /// </summary>
        /// <param name="fileId">name of the file to delete</param>
        /// <param name="loggedInUserInfo">the current logged in user information</param>
        /// <returns>the operation result</returns>
        public async Task<Result> DeleteAsync(string fileId)
        {
            var result = await _filesManager.DeleteAsync(fileId, _applicationSettings.ProjectId);
            if (!result.Success)
                return Result.Failed(result.Reason);

            //var result = await _filesService.DeleteAsync(fileId);
            //if (result.Successed <= 0)
            //    return Result.Failed(result.Result.FirstOrDefault()?.Reason ?? "failed to delete the file");

            return Result.Success();
        }

        /// <summary>
        /// get the file with the given name
        /// </summary>
        /// <param name="fileId">name of the file to retrieve</param>
        /// <param name="loggedInUserInfo">the current logged in user information</param>
        /// <returns>the file</returns>
        public async Task<Result<string>> GetAsync(string fileId)
        {
            var result = await _filesManager.GetAsync(fileId, GetPath());

            //var result = await _filesService.GetAsync(fileId);
            if (!result.Success)
                return Result.Failed<string>(result.Reason);

            return Result.Success<string>(result.Content);
        }

        /// <summary>
        /// save the given file
        /// </summary>
        /// <param name="file">the file to be saved</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SaveAsync(FileModel file)
        {
            var result = await _filesManager.SaveAsync(file.Id, file.Base64, GetPath());

            //var result = await _filesService.SaveAsync(new SharedServices.FileServices.Models.FileModel
            //{
            //    Id = file.Id,
            //    Content = file.Base64,
            //});

            //if (result.Successed <= 0)
            //    return Result.Failed(result.Result.FirstOrDefault()?.Reason ?? "failed to save the file");

            if (!result.Success)
                return Result.Failed(result.Reason);

            return Result.Success();
        }

        /// <summary>
        /// save the list of files
        /// </summary>
        /// <param name="files">list of files to save</param>
        /// <param name="loggedInUserInfo">the current logged in user informations</param>
        /// <returns>the operation result</returns>
        public async Task<Result> SaveAsync(List<FileModel> files)
        {
            //var filesData = files.Select(f => new SharedServices.FileServices.Models.FileModel
            //{
            //    Id = f.Id,
            //    Content = f.Base64,
            //})
            //.ToArray();

            //var result = await _filesService.SaveAsync(filesData);
            //if (result.Failed > 0)
            //    return Result.Failed(result.Result.FirstOrDefault()?.Reason ?? "failed to save the files");

            var path = GetPath();

            foreach (var file in files)
                await _filesManager.SaveAsync(file.Id, file.Base64, path);

            return Result.Success();
        }

        /// <summary>
        /// save the list of the attachment, and return list of the saved attachments
        /// </summary>
        /// <param name="attachments">list of attachment models to be saved</param>
        /// <returns>list of saved attachments</returns>
        public async Task<List<Attachment>> SaveAsync(params AttachmentModel[] attachments)
        {
            var attachements = new List<Attachment>();
            foreach (var attachementModel in attachments)
            {
                var attachment = new Attachment(attachementModel.FileName, attachementModel.FileType);
                var saveResult = await SaveAsync(new FileModel
                {
                    Id = attachment.FileId,
                    Base64 = attachementModel.Content,
                });

                if (!saveResult.IsSuccess)
                {
                    _logger.LogCritical(LogEvent.SavingMemo, "Failed to save the attachment [{@attachment}]", attachementModel);
                    continue;
                }

                attachements.Add(attachment);
            }

            return attachements;
        }

        /// <summary>
        /// delete the list of attachment
        /// </summary>
        /// <param name="attachments">the list of attachment to be deleted</param>
        /// <returns>the count the attachment deleted successful</returns>
        public async Task<int> DeleteAsync(params Attachment[] attachments)
        {
            var path = GetPath();
            var count = 0;
            foreach (var attachement in attachments)
            {
                var result = await _filesManager.DeleteAsync(attachement.FileId, path);
                if (result.Success)
                    count++;
            }

            return count;


            //var filesIds = attachments.Select(e => e.FileId).ToArray();
            //var deleteResult = await _filesService.DeleteAsync(filesIds);
            //return deleteResult.Successed;
        }

        #endregion

        /// <summary>
        /// this is used to generate Excel file for the given model list
        /// </summary>
        /// <typeparam name="TModel">the type of model to export</typeparam>
        /// <typeparam name="TExportOptions">the type of data export options</typeparam>
        /// <param name="data">the list of entities to export</param>
        /// <param name="exportOptions">the data export options</param>
        /// <returns>the excel file as byte array</returns>
        public byte[] GenerateFile<TModel, TExportOptions>(IEnumerable<TModel> data, TExportOptions exportOptions)
            where TExportOptions : DataExportOptions
            where TModel : IEntity
        {
            return Array.Empty<byte>();
        }

        /// <summary>
        /// this is used to generate Excel file for the given model list
        /// </summary>
        /// <typeparam name="TModel">the type of model to export</typeparam>
        /// <typeparam name="TExportOptions">the type of data export options</typeparam>
        /// <param name="entity">the entity to export</param>
        /// <param name="exportOptions">the data export options</param>
        /// <returns>the excel file as byte array</returns>
        public byte[] GenerateFile<TModel, TExportOptions>(TModel entity, TExportOptions exportOptions)
            where TExportOptions : DataExportOptions
            where TModel : IEntity
        {
            var pdfOption = _configuration.GetAsync<PdfOptions>(ApplicationConfigurationType.PdfOptions).GetAwaiter().GetResult();
            if (entity is Quote quote)
            {
                switch (exportOptions.ExportType)
                {
                    case ExportType.Excel:
                        break;
                    case ExportType.CSV:
                        break;
                    case ExportType.XML:
                        break;
                    case ExportType.PDF:
                        var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.Quote).GetAwaiter().GetResult();
                        return new QuotePDFService(quote, configuration, pdfOption).Generate();
                    default:
                        break;
                }
            }
            else
            if (entity is Invoice invoice)
            {
                switch (exportOptions.ExportType)
                {
                    case ExportType.Excel:
                        break;
                    case ExportType.CSV:
                        break;
                    case ExportType.XML:
                        break;
                    case ExportType.PDF:
                        var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.Invoice).GetAwaiter().GetResult();
                        List<Document> list = new List<Document>();
                        return new Invoice_CreditNotePDFService(invoice, configuration, list , "invoice", pdfOption).Generate();
                    default:
                        break;
                }
            }
            else
            if (entity is CreditNote creditNote)
            {
                //IEnumerable<CreditNote> creditNotes = new IEnumerable<CreditNote>();
                List<CreditNote> creditNotes = new List<CreditNote>();
                creditNotes.Add(creditNote);

                switch (exportOptions.ExportType)
                {
                    case ExportType.Excel:
                        break;
                    case ExportType.CSV:
                        break;
                    case ExportType.XML:
                        break;
                    case ExportType.PDF:
                        var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.CreditNote).GetAwaiter().GetResult();
                        List<Document> list = new List<Document>();
                        return new Invoice_CreditNotePDFService(creditNote, configuration , list , "credit", pdfOption).Generate();
                    default:
                        break;
                }
            }
            else
            if (entity is OperationSheet operationSheet)
            {
                switch (exportOptions.ExportType)
                {
                    case ExportType.Excel:
                        break;
                    case ExportType.CSV:
                        break;
                    case ExportType.XML:
                        break;
                    case ExportType.PDF:
                        var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.OperationSheet).GetAwaiter().GetResult();
                        return new OperationSheetPDFService(operationSheet, configuration, pdfOption).Generate();
                    default:
                        break;
                }
            }
            else
            if (entity is MaintenanceContract maintenanceContract)
            {
                switch (exportOptions.ExportType)
                {
                    case ExportType.Excel:
                        break;
                    case ExportType.CSV:
                        break;
                    case ExportType.XML:
                        break;
                    case ExportType.PDF:
                        var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.OperationSheet).GetAwaiter().GetResult();
                        return new MaintenanceContractPDFService(maintenanceContract, configuration, pdfOption).Generate();
                    default:
                        break;
                }
            }
            if (entity is MaintenanceOperationSheet operationSheetMain)
            {
                switch (exportOptions.ExportType)
                {
                    case ExportType.Excel:
                        break;
                    case ExportType.CSV:
                        break;
                    case ExportType.XML:
                        break;
                    case ExportType.PDF:
                        var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.OperationSheet).GetAwaiter().GetResult();
                        return new MaintenanceOperationSheetPDFService(operationSheetMain, configuration, pdfOption).Generate();
                    default:
                        break;
                }
            }

            return Array.Empty<byte>();
        }

        /// <summary>
        /// export the journal data as a byte array
        /// </summary>
        /// <param name="journalData">the journal data</param>
        /// <returns>the file as a byte array</returns>
        public byte[] ExportJournal(IEnumerable<JournalEntry> journalData, JournalType journalType)
            => _excelService.GenerateJournalExcelFile(journalData, journalType);

        /// <summary>
        /// export the Maintenance contrat
        /// </summary>
        /// <param name="">the journal data</param>
        /// <returns>the file as a byte array</returns>
        public byte[] ExportContrat(MaintenanceContract contratData)
            => _excelService.GenerateContratExcelFile(contratData);


        public byte[] generateInvoicebyFilter(ExportByPeriodByte model, IEnumerable<DocumentSharedModel> document)
        {
            var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.Invoice).GetAwaiter().GetResult();
            var pdfOption = _configuration.GetAsync<PdfOptions>(ApplicationConfigurationType.PdfOptions).GetAwaiter().GetResult();
            return new Invoice_FilterPDFService(configuration, model , document, pdfOption).Generate();
        }

        public byte[] generateInvoicebyFilterClient(IEnumerable<Invoice> document)
        {
            var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.Invoice).GetAwaiter().GetResult();
            var pdfOption = _configuration.GetAsync<PdfOptions>(ApplicationConfigurationType.PdfOptions).GetAwaiter().GetResult();
            return new Invoice_CreditNotePDFService(new Document(), configuration , document , "invoice", pdfOption).GenerateFilter();
        }

        public byte[] generateCreditNotebyFilterClient(IEnumerable<CreditNote> document)
        {
            var configuration = _configuration.GetDocumentConfigAsync(Domain.Enums.DocumentType.CreditNote).GetAwaiter().GetResult();
            var pdfOption = _configuration.GetAsync<PdfOptions>(ApplicationConfigurationType.PdfOptions).GetAwaiter().GetResult();
            return new Invoice_CreditNotePDFService(new Document(), configuration , document , "credit", pdfOption).GenerateFilter();
        }

    }

    /// <summary>
    /// the partial part for <see cref="FileService"/>
    /// </summary>
    public partial class FileService : IFileService
    {
        private readonly ExcelService _excelService;
        private readonly ITranslationService _translationService;
        private readonly FilesManager _filesManager;
        private readonly IApplicationSettingsAccessor _applicationSettings;

        //private readonly SharedServices.FileServices.IFileService _filesService;
        private readonly IApplicationConfigurationService _configuration;
        private readonly ILogger _logger;

        /// <summary>
        /// default constructor with the file manager
        /// </summary>
        /// <param name="filesManager">the file manager instant</param>
        public FileService(
            ILoggerFactory logger,
            ExcelService excelService,
            ITranslationService translationService,
            //SharedServices.FileServices.IFileService filesManager,
            FilesManager filesManager,
            IApplicationSettingsAccessor applicationSettings,
            IApplicationConfigurationService configurationService)
        {
            _excelService = excelService;
            _translationService = translationService;
            _filesManager = filesManager;
            _applicationSettings = applicationSettings;
            _configuration = configurationService;
            _logger = logger.CreateLogger<FileService>();
        }

        /// <summary>
        /// build the path
        /// </summary>
        /// <param name="projectId">the project id</param>
        /// <returns>the path</returns>
        private string GetPath()
            => Path.Combine(_applicationSettings.GetFileOutputPath(), _applicationSettings.ProjectId);
    }
}
