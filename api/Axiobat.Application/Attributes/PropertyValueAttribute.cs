﻿namespace Axiobat.Application
{
    using App.Common;

    /// <summary>
    /// this attribute is for defining where to get the value of an Id Property
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.All, Inherited = true, AllowMultiple = false)]
    public sealed class PropertyValueAttribute : System.Attribute
    {
        /// <summary>
        /// create an instant of the <see cref="PropertyValueAttribute"/>
        /// </summary>
        /// <param name="navigationProprety">the name of the property to get the value from it, example "Client.FullName"</param>
        public PropertyValueAttribute(string navigationProprety)
        {
            if (!NavigationProprety.IsValid())

            NavigationProprety = navigationProprety;
        }

        public string NavigationProprety { get; }
    }
}
