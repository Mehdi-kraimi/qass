﻿namespace Axiobat.Application.Exceptions
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// this exception get throw when a required data is messing
    /// </summary>
    [Serializable]
    public class NotFoundException : ValidationException
    {
        private static readonly string _message = "the required data couldn't be found";

        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundException"/> class with a default error message
        /// message.
        /// </summary>
        public NotFoundException() : base(_message, Axiobat.MessageCode.NotFound)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundException"/> class with a specified error
        /// message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public NotFoundException(string message) : base(message, Axiobat.MessageCode.NotFound)
        {
        }

        /// <summary>
        /// Initializes a new instance of the System.Exception class with serialized data.
        /// </summary>
        /// <param name="serializationInfo">The System.Runtime.Serialization.SerializationInfo that holds the serialized object data about the exception being thrown.</param>
        /// <param name="streamingContext">The System.Runtime.Serialization.StreamingContext that contains contextual information about the source or destination.</param>
        protected NotFoundException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}
