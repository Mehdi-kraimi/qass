﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// the type of the journal
    /// </summary>
    public enum JournalType
    {
        /// <summary>
        /// the sales journal
        /// </summary>
        Sales,

        /// <summary>
        /// the expense journal
        /// </summary>
        Expense,

        /// <summary>
        /// the bank journal
        /// </summary>
        Bank,

        /// <summary>
        /// the cash journal
        /// </summary>
        Cash
    }
}
