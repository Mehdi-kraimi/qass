﻿namespace Axiobat.Application.Enums
{
    /// <summary>
    /// describe the status of an operation result
    /// </summary>
    public enum ResultStatus
    {
        /// <summary>
        /// operation has been failed
        /// </summary>
        Failed,

        /// <summary>
        /// the operation has been succeed
        /// </summary>
        Succeed,
    }
}
