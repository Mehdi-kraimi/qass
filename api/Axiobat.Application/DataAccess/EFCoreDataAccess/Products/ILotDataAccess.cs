﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="Lot"/>
    /// </summary>
    public interface ILotDataAccess : IProductsBaseDataAccess<Lot>
    {
        /// <summary>
        /// get the list of the products that the Lot owns
        /// </summary>
        /// <param name="lotId">the id of the lots</param>
        /// <returns>the list of products</returns>
        Task<IEnumerable<Product>> GetLotProducts(string lotId);

        /// <summary>
        /// add a range of lot products
        /// </summary>
        Task<Result> AddLotProductRangeAsync(IEnumerable<LotProduct> lotProducts);

        /// <summary>
        /// delete all lot products
        /// </summary>
        /// <param name="id">the id of the lot to delete the products for it</param>
        /// <returns>the operation result</returns>
        Task DeleteLotProductsAsync(string id);
    }
}
