﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the base data access for <see cref="Client"/> and <see cref="Supplier"/>
    /// </summary>
    public interface IExternalPartnersDataAccess<TEntity> : IDataAccess<TEntity, string>
        where TEntity : ExternalPartner
    {

    }
}
