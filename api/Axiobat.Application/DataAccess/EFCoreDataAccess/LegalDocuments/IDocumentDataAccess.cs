﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;

    /// <summary>
    /// the data access for all document based entities
    /// </summary>
    /// <typeparam name="TDocument">the type of the document, must be a <see cref="Document"/></typeparam>
    public interface IDocumentDataAccess<TDocument> : IDataAccess<TDocument, string>
        where TDocument : Document
    {

    }
}
