﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="EquipmentMaintenance"/>
    /// </summary>
    public interface IEquipmentMaintenanceDataAccess : IDataAccess<EquipmentMaintenance, string>
    {

    }

    /// <summary>
    /// the data access for <see cref="MaintenanceContract"/>
    /// </summary>
    public interface IMaintenanceContractDataAccess : IDataAccess<MaintenanceContract, string>
    {

    }

    /// <summary>
    /// the data access for <see cref="MaintenanceContract"/>
    /// </summary>
    public interface IMaintenanceOperationSheetDataAccess : IDataAccess<MaintenanceOperationSheet, string>
    {

    }

    /// <summary>
    /// the data access for the MaintenanceVisit
    /// </summary>
    public interface IMaintenanceVisitDataAccess : IDataAccess<MaintenanceVisit, string>
    {
        Task UpdateStatusAsync(string maintenanceVisitId, string status);
    }
}
