﻿namespace Axiobat.Application.Data
{
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// the <see cref="AccountingPeriod"/> DataAccess
    /// </summary>
    public interface IAccountingPeriodDataAccess : IDataAccess<AccountingPeriod, int>
    {
        /// <summary>
        /// get the current accounting period
        /// </summary>
        /// <returns></returns>
        Task<AccountingPeriod> CurrentAccountingPeriodAsync();
    }
}
