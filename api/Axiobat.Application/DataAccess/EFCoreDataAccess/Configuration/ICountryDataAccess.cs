﻿namespace Axiobat.Application.Data
{
    using Application.Models;
    using Domain.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the data access for <see cref="Country"/>
    /// </summary>
    public interface ICountryDataAccess
    {
        /// <summary>
        /// get the country by code
        /// </summary>
        /// <param name="countryCode">the code of the country to be retrieved</param>
        /// <returns>the country</returns>
        Task<Country> GetCountryByCodeAsync(string countryCode);

        /// <summary>
        /// get the country by name
        /// </summary>
        /// <param name="countryName">name of the country to be retrieved</param>
        /// <returns>the country</returns>
        Task<Country> GetCountryByNameAsync(string countryName);

        /// <summary>
        /// get list of all countries
        /// </summary>
        /// <returns>the list of countries</returns>
        Task<IEnumerable<Country>> GetAllAsync();

        /// <summary>
        /// check if the given country code is exist
        /// </summary>
        /// <param name="countryCode">the code of the country</param>
        /// <returns>true if exist, false if not</returns>
        Task<bool> IsCountryExistAsync(string countryCode);

        /// <summary>
        /// get the paged result using the given filter type
        /// </summary>
        /// <typeparam name="IFilter">the type of the filter to use</typeparam>
        /// <param name="filterOption">the filter instant</param>
        /// <returns>paged result</returns>
        Task<PagedResult<Country>> GetPagedResultAsync<IFilter>(IFilter filterOption)
            where IFilter : IFilterOptions;
    }
}
