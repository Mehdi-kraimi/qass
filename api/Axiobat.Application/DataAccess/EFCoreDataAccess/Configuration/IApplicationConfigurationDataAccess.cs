﻿namespace Axiobat.Application.Data
{
    using Domain.Enums;
    using Domain.Entities;
    using Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System;

    /// <summary>
    /// the <see cref="ApplicationConfiguration"/> DataAccess
    /// </summary>
    public interface IApplicationConfigurationDataAccess : IQueryableDataAccess
    {
        /// <summary>
        /// get the <see cref="global::Axiobat.Domain.Entities.ApplicationConfiguration"/> instant associated with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved, must be one of <see cref="global::Axiobat.Domain.Entities.ApplicationConfiguration"/> values</param>
        /// <returns>the <see cref="global::Axiobat.Domain.Entities.ApplicationConfiguration"/> instant</returns>
        Task<ApplicationConfiguration> GetConfigurationAsync(string configurationType);

        /// <summary>
        /// get the configuration of the given types
        /// </summary>
        /// <param name="configurationType">the type of the configuration to retrieve</param>
        /// <param name="LastModifiedOn">the time of the last modified on should be greater than</param>
        /// <returns>list of configuration</returns>
        Task<ApplicationConfiguration[]> GetConfigurationAsync(string[] configurationType, DateTimeOffset? LastModifiedOn);


        /// <summary>
        /// check if is there any application configuration with the given type
        /// </summary>
        /// <param name="configurationType">the type of the configuration to be retrieved, must be one of <see cref="ApplicationConfiguration"/> values</param>
        /// <returns>true if exist, false if not</returns>
        Task<bool> IsConfigurationExistAsync(string configurationType);

        /// <summary>
        /// update the given configuration
        /// </summary>
        /// <param name="configuration">the configuration instant</param>
        /// <returns>the updated application configuration</returns>
        Task<Result<ApplicationConfiguration>> UpdateConfigurationAsync(ApplicationConfiguration configuration);

        /// <summary>
        /// get paged result of the entity using the given filter options
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity</typeparam>
        /// <param name="filter">the filter instant</param>
        /// <returns>the paged result</returns>
        Task<PagedResult<TEntity>> GetAllAsync<TEntity>(FilterOptions filter) where TEntity : Entity;

        /// <summary>
        /// get list of all entities
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity to be retrieved</typeparam>
        /// <returns>the list of entity</returns>
        Task<IEnumerable<TEntity>> GetAllAsync<TEntity>() where TEntity : Entity;

        /// <summary>
        /// get list of all categories
        /// </summary>
        /// <returns>the list of categories</returns>
        Task<IEnumerable<ChartAccountItem>> GetAllCategoriesAsync();

        /// <summary>
        /// get the entity by id
        /// </summary>
        /// <typeparam name="TEntity">the type of the entity</typeparam>
        /// <typeparam name="Tkey">the type of the key</typeparam>
        /// <param name="entityId">the id of the entity</param>
        /// <returns>the entity</returns>
        Task<TEntity> GetbyIdAsync<TEntity, Tkey>(Tkey entityId) where TEntity : Entity<Tkey>;

        /// <summary>
        /// add the given entity to the database
        /// </summary>
        /// <typeparam name="TEntity">type of the entity</typeparam>
        /// <typeparam name="TKey">the type of the entity key</typeparam>
        /// <param name="entity">the entity to be added</param>
        /// <returns>the newly added entity</returns>
        Task<Result<TEntity>> AddAsync<TEntity, TKey>(TEntity entity) where TEntity : Entity<TKey>;

        /// <summary>
        /// update the entity
        /// </summary>
        /// <typeparam name="TEntity">the type of ht</typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<Result<TEntity>> UpdateAsync<TEntity, TKey>(TEntity entity) where TEntity : Entity<TKey>;

        /// <summary>
        /// get list of all categories with the given type
        /// </summary>
        /// <param name="categoryType">the type of the categories to retrieve</param>
        /// <returns>list of <see cref="global::Axiobat.Domain.Entities.ChartAccountItem"/></returns>
        Task<IEnumerable<ChartAccountItem>> GetAllCategoriesByTypeAsync(ChartAccountType categoryType);

        /// <summary>
        /// delete the entity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<Result> DeleteAsync<TEntity>(TEntity entity) where TEntity : Entity;
        Task<WorkshopDocumentType> GetSpecialDocumentTypeAsync();

        Task<PaymentMethod> GetCreditNotePaymentMethodAsync();

        Task<IEnumerable<TurnoverGoal>> GetTurnoverGoalsAsync();

        /// <summary>
        /// get the count of payment associated with the given payment method
        /// </summary>
        /// <param name="paymentMethodId">the id of the payment method</param>
        /// <returns>the count of payments</returns>
        Task<int> GetPaymentMethodPaymentsCountAsync(int paymentMethodId);
    }
}
