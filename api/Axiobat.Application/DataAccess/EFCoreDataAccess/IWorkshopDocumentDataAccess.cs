﻿namespace Axiobat.Application.Data
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;
    using System.Collections.Generic;

    /// <summary>
    /// the data access for <see cref="WorkshopDocument"/>
    /// </summary>
    public interface IWorkshopDocumentDataAccess : IDataAccess<WorkshopDocument, string>
    {
        /// <summary>
        /// delete all documents types
        /// </summary>
        /// <param name="id">the id of the document to delete it types</param>
        Task DeleteDocumentTypesAsync(string id);

        /// <summary>
        /// add the Documents Types
        /// </summary>
        /// <param name="documentsTypes">the documents Types</param>
        /// <returns>the operations result</returns>
        Task<Result> AddDocumentsTypesAsync(params WorkshopDocumentsTypes[] documentsTypes);

        /// <summary>
        /// get the document  of the given workshop
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns>the list of documents</returns>
        Task<IEnumerable<WorkshopDocument>> GetWorkshopDocumentsAsync(string workshopId);

        /// <summary>
        /// get the list of document of the given rubric
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <param name="rubricId">the id of the rubric</param>
        /// <returns>list of documents</returns>
        Task<IEnumerable<WorkshopDocument>> GetRubricDocumentsAsync(string workshopId, string rubricId);
    }

    /// <summary>
    /// the data access for <see cref="WorkshopDocumentRubric"/>
    /// </summary>
    public interface IWorkshopDocumentRubricDataAccess : IDataAccess<WorkshopDocumentRubric, string>
    {

    }
}
