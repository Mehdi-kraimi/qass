﻿namespace Axiobat.Application.Services
{
    using Configuration;
    using Domain.Entities;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// service for <see cref="ConstructionWorkshop"/>
    /// </summary>
    public interface IConstructionWorkshopService : IDataService<ConstructionWorkshop, string>, ISynchronize
    {
        /// <summary>
        /// check if the given Construction Workshop name unique or not
        /// </summary>
        /// <param name="name">the name of Construction Workshop</param>
        /// <returns>true if unique, false if not</returns>
        Task<bool> IsNameUniqueAsync(string name);

        /// <summary>
        /// update the status of the workshop using the given model
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the construction workshop to be updated</param>
        /// <param name="model">the status update model</param>
        /// <returns>the operation result</returns>
        Task<Result<ConstructionWorkshopModel>> UpdateStatusAsync(string constructionWorkshopId, WorkshopUpdateStatusModel model);

        /// <summary>
        /// update Progress Rate of the given workshop
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <param name="model">the update model</param>
        /// <returns>the updated version of the workshop</returns>
        Task<Result<ConstructionWorkshopModel>> UpdateProgressRateAsync(string constructionWorkshopId, WorkshopUpdateProgressRateModel model);

        /// <summary>
        /// update the workshop documentation using the given model
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the work shop to update it document</param>
        /// <param name="documentId">the id of the document to update</param>
        /// <param name="model">the update model</param>
        /// <returns>the updated version of the workshop</returns>
        Task<Result<WorkshopDocumentModel>> UpdateDocumentAsync(string constructionWorkshopId, string documentId, WorkshopDocumentPutModel model);

        /// <summary>
        /// retrieve the workshop documents details
        /// </summary>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns></returns>
        Task<PagedResult<WorkshopDocumentsDetails>> GetDocumentsDetailsAsync(string workshopId, RubricFilterOptions filterOptions);

        /// <summary>
        /// create a new document
        /// </summary>
        /// <param name="constructionWorkshopId">the construction workshop document id</param>
        /// <param name="model">the create model</param>
        /// <returns>the create document</returns>
        Task<Result<WorkshopDocumentModel>> CreateDocumentAsync(string constructionWorkshopId, WorkshopDocumentPutModel model);

        /// <summary>
        /// get the workshop document with the given id
        /// </summary>
        /// <param name="constructionWorkshopId">the construction workshop id</param>
        /// <param name="documentId">the document id</param>
        /// <returns>the document</returns>
        Task<Result<WorkshopDocumentModel>> GetDocumentByIdAsync(string constructionWorkshopId, string documentId);

        /// <summary>
        /// return list of document of the given workshop
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <returns>the list of documents</returns>
        Task<ListResult<WorkshopDocumentModel>> GetDocumentsAsync(string constructionWorkshopId);

        /// <summary>
        /// delete the document with the given id
        /// </summary>
        /// <param name="constructionWorkshopId">the construction workshop id</param>
        /// <param name="documentId">the document id</param>
        /// <returns>the operation result</returns>
        Task<Result> DeleteDocumentAsync(string constructionWorkshopId, string documentId);

        /// <summary>
        /// retrieve list of documents using the filter option as a paged result
        /// </summary>
        /// <param name="workshopId">the id the workshop</param>
        /// <param name="filterOptions">the Workshop Document FilterOptions</param>
        /// <returns>the paged of documents</returns>
        Task<PagedResult<WorkshopDocumentModel>> GetRubricDocumentsAsync(WorkshopDocumentFilterOptions filterOptions);

        /// <summary>
        /// get the workshop client
        /// </summary>
        /// <typeparam name="TOut">the type of the output, should be registered with automapper</typeparam>
        /// <param name="workshopId">the id of the workshop</param>
        /// <returns>the client with desired output</returns>
        Task<Result<TOut>> GetWorkshopClientAsync<TOut>(string workshopId);

        /// <summary>
        /// retrieve the workshop documents counts informations
        /// </summary>
        /// <param name="constructionWorkshopId">the construction workshop id</param>
        /// <returns>the workshop documents count</returns>
        Task<Result<WorkshopDocumentsCount>> GetWorkshopDocumentsCountAsync(string constructionWorkshopId);

        /// <summary>
        /// retrieve list of documents Rubrics associated with the given workshop
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <returns>list result of <see cref="WorkshopDocumentRubricModel"/></returns>
        Task<ListResult<WorkshopDocumentRubricModel>> GetWorkshopDocumentsRubricsAsync(string constructionWorkshopId);

        /// <summary>
        /// Create or Update a workshop base on the given model
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the Construction Workshop to associate the Rubrics for it</param>
        /// <param name="model">the model to be used for creating the rubric</param>
        /// <returns>the created rubric</returns>
        Task<Result<WorkshopDocumentRubricModel>> PutWorkshopDocumentsRubricsAsync(string constructionWorkshopId, WorkshopDocumentRubricModel model);

        /// <summary>
        /// delete the workshop rubric with the give id
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <param name="rubricId">the id of the rubric</param>
        /// <returns>the operation result</returns>
        Task<Result> DeleteWorkshopDocumentsRubricsAsync(string constructionWorkshopId, string rubricId);

        /// <summary>
        /// Get list of documents that belongs to the given Rubrics
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <param name="rubricId">the id of the rubric</param>
        /// <returns>list of <see cref="WorkshopDocumentModel"/></returns>
        Task<ListResult<WorkshopDocumentModel>> GetWorkshopRubricsDocumentsAsync(string constructionWorkshopId, string rubricId);
    }
}
