﻿namespace Axiobat.Application.Services.Products
{
    using Configuration;
    using Domain.Entities;

    /// <summary>
    /// the base service for all products including <see cref="Product"/> and <see cref="Lot"/>
    /// </summary>
    public interface IProductBaseService<TEntity> : IDataService<TEntity, string>, ISynchronize
        where TEntity : ProductsBase
    {

    }
}
