﻿namespace Axiobat.Application.Services.Maintenance
{
    using Documents;
    using Models;
    using Configuration;
    using Domain.Entities;
    using FileService;
    using System.Threading.Tasks;
    using System.Collections.Generic;

    /// <summary>
    /// service for <see cref="MaintenanceContract"/>
    /// </summary>
    public interface IMaintenanceContractService : IDataService<MaintenanceContract, string>, ISynchronize,
        IMemoService,
        IReferenceService,
        IDuplicableService
    {
        Task<Result<byte[]>> ExportContratAsync(string contratId);

        /// <summary>
        /// update the status of the operation Sheet
        /// </summary>
        /// <param name="contratId">the id of the operation Sheet to update</param>
        /// <param name="model">the model used for updating the status</param>
        /// <returns>the operation result</returns>
        Task<Result> UpdateStatusAsync(string contratId, DocumentUpdateStatusModel model);

        /// <summary>
        /// update the Contract PublishingDocument with the given list
        /// </summary>
        /// <param name="maintenanceContractId">the id of the Contract</param>
        /// <param name="model">list of PublishingDocument</param>
        /// <returns>operation result</returns>
        Task<ListResult<PublishingContractMinimal>> UpdateContractPublishingDocumentAsync(string maintenanceContractId, ICollection<PublishingContractMinimal> model);
    }
}
