﻿namespace Axiobat.Application.Services.Maintenance
{
    using Axiobat.Application.Services.Configuration;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// service for <see cref="EquipmentMaintenance"/>
    /// </summary>
    public interface IEquipmentMaintenanceService : IDataService<EquipmentMaintenance, string>, ISynchronize
    {
        /// <summary>
        /// check if the given equipment name is unique
        /// </summary>
        /// <param name="equipmentName">the name of the equipment</param>
        /// <returns>true if unique, false if not</returns>
        Task<bool> IsNameUniqueAsync(string equipmentName);
    }
}
