﻿namespace Axiobat.Application.Services.Documents
{
    using FileService;
    using Domain.Entities;
    using System.Threading.Tasks;
    using Application.Models;

    /// <summary>
    /// the service for <see cref="Expense"/>
    /// </summary>
    public interface IExpenseService : IDocumentService<Expense>, IMemoService
    {
        /// <summary>
        /// cancel the expense with the given id
        /// </summary>
        /// <param name="expenseId">the id of the expense to be canceled</param>
        /// <param name="cancelationDocument">the cancellation document</param>
        /// <returns>the operation result</returns>
        Task<Result<MemoModel>> CancelExpenseAsync(string expenseId, MemoModel cancelationDocument);
    }
}
