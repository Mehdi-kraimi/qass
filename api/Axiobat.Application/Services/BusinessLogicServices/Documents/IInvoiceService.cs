﻿namespace Axiobat.Application.Services.Documents
{
    using Domain.Entities;
    using FileService;
    using Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// the service for <see cref="Invoice"/>
    /// </summary>
    public interface IInvoiceService : IDocumentService<Invoice>, IEmailDocument<string>, IMemoService
    {
        /// <summary>
        /// create a new invoice form the given quote
        /// </summary>
        /// <param name="document">the quote document instant</param>
        /// <returns>the generated invoice document</returns>
        Task<Result<InvoiceModel>> GenerateFromQuoteAsync(Quote document);

        /// <summary>
        /// cancel the invoice with the given id using the given credit note
        /// </summary>
        /// <param name="invoiceId">the id of the invoice</param>
        /// <param name="creditNote">the credit note to be used for the cancellation</param>
        /// <returns>the operation result</returns>
        Task<Result<CreditNoteModel>> CancelInvoiceAsync(string invoiceId);

        /// <summary>
        /// get the Holdback Details
        /// </summary>
        /// <param name="workshopId">the id of the workshop id</param>
        /// <returns>the list of workshop details</returns>
        Task<ListResult<HoldbackDetailsModel>> GetHoldbackDetailsAsync(string workshopId);

        /// <summary>
        /// update the holdback details of the invoice
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Result> UpdateHoldbackDetailsAsync(string invoiceId, HoldbackDetailsUpdateModel model);

        /// <summary>
        /// the exportReleve  of the invoice
        /// </summary>
        /// <returns></returns>
        Task<Result<ExportByte>> UpdateExportReleve(ExportByPeriodByte model);
    }
}
