﻿namespace Axiobat.Application.Services.Contacts
{
    using Models;
    using Domain.Entities;
    using System.Threading.Tasks;

    /// <summary>
    /// service for <see cref="Client"/>
    /// </summary>
    public interface IClientService : IExternalPartnersService<Client>
    {
        /// <summary>
        /// check if the given code is unique
        /// </summary>
        /// <param name="code">the code to be checked</param>
        /// <returns>true if unique, false if not</returns>
        Task<bool> IsCodeUniqueAsync(string code);

        /// <summary>
        /// get the client documents details
        /// </summary>
        /// <param name="clientId">the id of the client to get the document details for it</param>
        /// <returns>the documents details result</returns>
        Task<ClientDocumentDetails> GetClientDocumentsDetailsAsync(string clientId);

        /// <summary>
        /// get the client turn details for the given month
        /// </summary>
        /// <param name="clientId">the id of the client</param>
        /// <param name="year">the year to get the turnOver for</param>
        /// <returns>the client turnOver details</returns>
        Task<ClientTurnoverDetail> GetClientTurnOverDetailsAsync(string clientId, int year);
    }
}
