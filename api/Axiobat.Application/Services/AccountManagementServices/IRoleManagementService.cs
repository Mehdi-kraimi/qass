﻿namespace Axiobat.Application.Services.AccountManagement
{
    using Axiobat.Application.Models;
    using Axiobat.Domain.Entities;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// the role management service
    /// </summary>
    public interface IRoleManagementService : IDataService<Role, Guid>
    {
        /// <summary>
        /// check if a role with the given id is exist
        /// </summary>
        /// <param name="roleName">the id of the role</param>
        /// <returns>true if exist, false if not</returns>
        Task<bool> IsRoleExistAsync(Guid roleId);

        /// <summary>
        /// check if a role with the given name exist
        /// </summary>
        /// <param name="roleName">the name of the role</param>
        /// <returns>true if exist, false if not</returns>
        Task<bool> IsRoleExistAsync(string roleName);

        /// <summary>
        /// update the role permissions
        /// </summary>
        /// <param name="roleId">the id of the role</param>
        /// <param name="model">the role permissions model</param>
        /// <returns></returns>
        Task UpdateRolePermissionsAsync(Guid roleId, RolePermissionsModel model);
    }
}