﻿namespace Axiobat.Application.Services.AccountManagement
{
    using Domain.Enums;
    using Domain.Entities;
    using Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using App.Common;
    using System;

    /// <summary>
    /// a service to get all sort of information about the current logged in user
    /// </summary>
    public interface ILoggedInUserService
    {
        /// <summary>
        /// the current authenticated user information
        /// </summary>
        ILoggedInUser User { get; }

        /// <summary>
        /// check if the current request is for an authenticated user
        /// </summary>
        bool IsAuthenticated { get; }

        /// <summary>
        /// check if the current logged in user is a super admin,
        /// basically we check if the role type is <see cref="RoleType.SuperAdminRole"/> and
        /// company type is <see cref="CompanyType.Foliatech"/>
        /// </summary>
        bool IsSuperAdmin { get; }

        /// <summary>
        /// check if the current logged in user is an admin,
        /// basically we only check if the role type is <see cref="RoleType.AdminRole"/>
        /// </summary>
        bool IsAdmin { get; }

        /// <summary>
        /// check if the current logged in user has the given permission
        /// </summary>
        /// <param name="permissions">the permissions to check if the user have</param>
        /// <returns>true if the user has the permission, false if not</returns>
        bool HasPermission(params Permissions[] permissions);

        /// <summary>
        /// get the list of permissions of the current authenticated in user
        /// </summary>
        /// <returns>the user permissions</returns>
        IEnumerable<Permission> GetUserPermissions();

        /// <summary>
        /// get the user instant of the current logged in user
        /// </summary>
        /// <returns>the user instant</returns>
        Task<User> GetUserAsync();

        /// <summary>
        /// get the role of the current authenticated user
        /// </summary>
        /// <returns>role of the user</returns>
        Task<Role> GetUserRoleAsync();

        /// <summary>
        /// get current logged in user or the admin if the user is not logged in
        /// </summary>
        /// <returns>the user (logged in or admin)</returns>
        Task<User> GetCurrentUserOrAdminAsync();

        /// <summary>
        /// get the <see cref="MinimalUser"/> instant
        /// </summary>
        /// <returns>the <see cref="MinimalUser"/> instant</returns>
        MinimalUser GetMinimalUser();

        /// <summary>
        /// get the userAgent information
        /// </summary>
        /// <returns>an instant of <see cref="UserAgentInfo"/>, or null if the request don't own any User-agent header</returns>
        UserAgentInfo GetUserAgentInfo();

        /// <summary>
        /// get the IP address of the user request
        /// </summary>
        /// <returns>the IP address of the user</returns>
        string GetUserIPAddress();
    }
}