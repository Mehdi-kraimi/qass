﻿namespace Axiobat.Application.Services.Configuration
{
    using Application.Models;
    using Domain.Entities;
    using Domain.Enums;
    using System.Threading.Tasks;

    /// <summary>
    /// service for managing chart of accounts
    /// </summary>
    public interface IChartOfAccountsService
    {
        Task PutAsync(ChartAccountItemPutModel[] model);

        Task<ListResult<TOut>> GetAllAsync<TOut>();

        Task<Result<TOut>> GetByIdAsync<TOut>(string categoryId);

        Task<TOut[]> GetCategoriesByTypeAsync<TOut>(params ChartAccountType[] categoryTypes);

        Task<Result> DeleteAsync(string categoryId);

        Task<ChartOfAccounts> GetChartOfAccountsAsync();
        Task<bool> IsAllExistAsync(string[] chartOfAccountsIds);
    }
}
