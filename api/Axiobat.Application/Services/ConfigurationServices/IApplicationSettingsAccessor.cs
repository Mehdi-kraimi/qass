﻿namespace Axiobat.Application.Services.Configuration
{
    /// <summary>
    /// the application configuration accessor
    /// </summary>
    public interface IApplicationSettingsAccessor
    {
        /// <summary>
        /// get the if of the project
        /// </summary>
        string ProjectId { get; }

        /// <summary>
        /// get the file output path
        /// </summary>
        /// <returns></returns>
        string GetFileOutputPath();

        /// <summary>
        /// get the expiration token duration
        /// </summary>
        /// <returns>the expiration token</returns>
        int GetTokenExpiration();

        /// <summary>
        /// get the token audience
        /// </summary>
        /// <returns>the token audience</returns>
        string GetTokenAudience();

        /// <summary>
        /// get the token issuer
        /// </summary>
        /// <returns>the token issuer</returns>
        string GetTokenIssuer();

        /// <summary>
        /// get the path of the output where the file contracts are saved
        /// </summary>
        /// <returns>the file output path</returns>
        string GetPublishContractFilesOutput();

        /// <summary>
        /// get the URL value
        /// </summary>
        /// <param name="urlName">the name of the URL to retrieve, one of <see cref="Domain.Constants.ApplicationsURLs"/></param>
        /// <returns></returns>
        string GetUrl(string urlName);

        /// <summary>
        /// get options related to one signal
        /// </summary>
        /// <returns>the OneSignal options</returns>
        OneSignalOptions GetOneSignalOptions();

        /// <summary>
        /// check if background services are allowed to run
        /// </summary>
        /// <returns>true if allowed false if not</returns>
        bool BackgroundServicesAllowed();
    }
}
