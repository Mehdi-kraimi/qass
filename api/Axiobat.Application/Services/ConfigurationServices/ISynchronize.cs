﻿namespace Axiobat.Application.Services.Configuration
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// this interface is used to mark the service to process Synchronization of the underling entity
    /// </summary>
    public interface ISynchronize
    {
        /// <summary>
        /// this function should hold the logic for Processing Synchronization logic
        /// </summary>
        /// <param name="LastSyncDate">the date of the last synchronization, if null that means it the first synchronization</param>
        /// <param name="ObjectsToSync">the list of objects to synchronization</param>
        /// <returns>the list of synchronized data</returns>
        Task<dynamic> SynchronizeAsync(DateTimeOffset? LastSyncDate, IEnumerable<SyncObject> ObjectsToSync);
    }
}