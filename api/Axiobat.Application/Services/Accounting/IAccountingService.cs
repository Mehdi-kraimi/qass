﻿namespace Axiobat.Application.Services.Accounting
{
    using Application.Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the accounting service
    /// </summary>
    public interface IAccountingService
    {
        /// <summary>
        /// get the Financial Summary of the workshop with the given id
        /// </summary>
        /// <param name="constructionWorkshopId">the id of the workshop</param>
        /// <returns>the workshop Financial Summary instant</returns>
        Task<Result<WorkshopFinancialSummary>> GetFinancialSummaryAsync(string constructionWorkshopId);

        /// <summary>
        /// get the journal details
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the list of journal details</returns>
        Task<PagedResult<JournalEntry>> GetJournalAsync(JournalFilterOptions filterOptions);

        /// <summary>
        /// export the journal using the given options
        /// </summary>
        /// <param name="filterOptions">the filter options</param>
        /// <returns>the exported file as a byte array</returns>
        Task<Result<byte[]>> ExportJournalAsync(JournalFilterOptions filterOptions);
    }
}