﻿namespace Axiobat.Application.Services.Accounting
{
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the dashboard service
    /// </summary>
    public interface IDashboardService
    {
        /// <summary>
        /// get the documents details
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>the documents details</returns>
        Task<Result<DashboardDocumentsDetailsModel>> GetDocumentsDetailsAsync(DateRangeFilterOptions filterModel);

        /// <summary>
        /// Get TurnOver details by year
        /// </summary>
        /// <param name="year">the year to get the details for it</param>
        /// <returns>the turnOver details result</returns>
        Task<Result<DashboardTurnOverDetailsModel>> GetTurnOverDetailsAsync(int year);

        /// <summary>
        /// Get expenses details by year
        /// </summary>
        /// <param name="year">the year to get the details for it</param>
        /// <returns>the expenses details result</returns>
        Task<Result<DashboardExpensesDetailsModel>> GetExpensesDetailsAsync(int year);

        /// <summary>
        /// Get tops details
        /// </summary>
        /// <param name="topValue">the tops Value</param>
        /// <returns>the tops details result</returns>
        Task<Result<DashboardTopsDetailsModel>> GetTopsDetailsAsync(int topValue);

        /// <summary>
        /// Get cash summary details
        /// </summary>
        /// <param name="topValue">the tops Value</param>
        /// <returns>the tops details result</returns>
        Task<Result<DashboardCashSummaryDetailsModel>> GetCashSummaryDetailsAsync();
    }
}