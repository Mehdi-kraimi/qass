﻿namespace Axiobat.Application.Services.Accounting
{
    using Application.Models;
    using System.Threading.Tasks;

    /// <summary>
    /// the analytics service
    /// </summary>
    public interface IAnalyticsService
    {
        /// <summary>
        /// get the workshop analytics result
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>the result</returns>
        Task<Result<WorkshopAnalyticsResult>> GetWorkshopAnalyticsAsync(WorkshopAnalyticsFilterOtions filterModel);

        /// <summary>
        /// the financial analytics result
        /// </summary>
        /// <param name="filterModel">the filter options</param>
        /// <returns>the result</returns>
        Task<Result<FinancialAnalyticsResult>> GetFinancialAnalyticsAsync(ExternalPartnerFilterOptions filterModel);

        /// <summary>
        /// the sales analytics result
        /// </summary>
        /// <param name="filterModel">the filter model</param>
        /// <returns>the sales analytics result</returns>
        Task<Result<SalesAnalyticsResult>> GetSalesAnalyticsAsync(TransactionAnalyticsFilterOptions filterModel);

        /// <summary>
        /// get the purchase analytics result
        /// </summary>
        /// <param name="filterModel">the filer model</param>
        /// <returns>the purchase analytics result</returns>
        Task<Result<PurchaseAnalyticsResult>> GetPurchaseAnalyticsAsync(TransactionAnalyticsFilterOptions filterModel);

        /// <summary>
        /// get the Maintenance Analytics
        /// </summary>
        /// <param name="filterModel">the filer model</param>
        /// <returns>the Maintenance Analytics</returns>
        Task<Result<MaintenanceAnalyticsResult>> GetMaintenanceAnalyticsAsync(MaintenanceAnalyticsFilterOptions filterModel);
    }
}