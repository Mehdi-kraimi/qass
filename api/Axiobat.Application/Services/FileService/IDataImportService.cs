﻿namespace Axiobat.Application.Services.FileService
{
    using Application.Models.DataImport;
    using Axiobat.Application.Models;
    using Domain.Entities;
    using Domain.Entities.Configuration;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// this interface defines the data import operation
    /// </summary>
    public interface IDataImportService
    {
        IEnumerable<Client> GetClientsFromFile(
            Stream file,
            int startingRow,
            Numerator clientsNumerator,
            string defaultClientCode,
            ClientColumnsDefinitions columnsDefinitions,
            string worksheetName);

        IEnumerable<Supplier> GetSuppliersFromFile(
            Stream file,
            int startingRow,
            SupplierColumnsDefinitions columnsDefinitions,
            string worksheetName);

        IEnumerable<Product> GetProductsFromFile(
            Stream file,
            int startingRow,
            User superAdmin,
            ProductColumnsDefinitions columnsDefinitions, 
            ExternalPartnerMinimalInfo[] suppliers,
            string worksheetName);

        IEnumerable<ProductPricing> GetProductPricingsFromFile(
            Stream file,
            int startingRow,
            ProductPriceColumnsDefinitions columnsDefinitions,
            string worksheetName);
    }
}
