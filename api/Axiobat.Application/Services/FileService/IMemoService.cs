﻿namespace Axiobat.Application.Services.FileService
{
    using Domain.Entities;
    using Models;
    using System.Threading.Tasks;

    /// <summary>
    /// this interface defines the memo related operation
    /// </summary>
    public interface IMemoService
    {
        /// <summary>
        /// save the memo to the <see cref="TEntity"/> with the given id
        /// </summary>
        /// <param name="entityId">the id of the <see cref="TEntity"/> to add memo to it</param>
        /// <param name="memos">the memo to add</param>
        /// <returns>an operation result</returns>
        Task<Result<Memo>> SaveMemoAsync(string entityId, MemoModel memos);

        /// <summary>
        /// update the memo with the given id
        /// </summary>
        /// <param name="entityId">the id of the entity to be updated</param>
        /// <param name="memoId">the id of the memo to be updated</param>
        /// <param name="memos">the new memo</param>
        /// <returns>the updated version of the memo</returns>
        Task<Result<Memo>> UpdateMemoAsync(string entityId, string memoId, MemoModel memos);

        /// <summary>
        /// delete <see cref="Memo"/> form the <see cref="TEntity"/> using the given <see cref="DeleteMemoModel"/>
        /// </summary>
        /// <param name="model">the <see cref="DeleteMemoModel"/></param>
        /// <returns>the operation result</returns>
        Task<Result> DeleteMemosAsync(string entityId, DeleteMemoModel model);
    }
}
