﻿namespace Axiobat.Application.Models.DataImport
{
    public class SupplierColumnsDefinitions
    {
        public int Reference { get; set; }
        public int AccountingCode { get; set; }
        public int FirstName { get; set; }
        public int LastName { get; set; }
        public int PhoneNumber { get; set; }
        public int Email { get; set; }
        public int Website { get; set; }
        public int Siret { get; set; }
        public int IntraCommunityVAT { get; set; }
        public int Address_Designation { get; set; }
        public int Address_Street { get; set; }
        public int Address_City { get; set; }
        public int Address_PostalCode { get; set; }
        public int Address_Country { get; set; }
    }
}