﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Entities;

    /// <summary>
    /// this model is used to define forget password requirements
    /// </summary>
    [ModelFor(typeof(User))]
    public class UserForgetPasswordModel
    {
        /// <summary>
        /// the email of the user
        /// </summary>
        public string Email { get; set; }
    }
}
