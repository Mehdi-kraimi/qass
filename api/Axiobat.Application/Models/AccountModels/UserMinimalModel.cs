﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// a minimal model of the user
    /// </summary>
    [ModelFor(typeof(User))]
    public partial class UserMinimalModel
    {
        /// <summary>
        /// the id of the user
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Last name of the user
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// first name of the user
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the user name for this user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// the role of the user
        /// </summary>
        public RoleModel Role { get; set; }
    }
}
