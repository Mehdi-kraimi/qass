﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Enums;

    /// <summary>
    /// this object holds information about user and company
    /// </summary>
    public class LoginEntityModel
    {
        /// <summary>
        /// create an instant of <see cref="LoginEntityModel"/>
        /// </summary>
        public LoginEntityModel() { }

        /// <summary>
        /// create an instant of <see cref="LoginEntityModel"/>
        /// </summary>
        /// <param name="id">the id of the user / company</param>
        /// <param name="name">the name of the company / full name of the user</param>
        /// <param name="email">the email of the user / company</param>
        /// <param name="type">the type of the company</param>
        public LoginEntityModel(string id, string name, string email)
        {
            Id = id;
            Name = name;
            Email = email;
        }

        /// <summary>
        /// the id of the user / company
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the name of the company / full name of the user
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// email of the company / user
        /// </summary>
        public string Email { get; set; }
    }
}
