﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Entities;

    [ModelFor(typeof(WorkshopDocumentType))]
    public partial class WorkshopDocumentTypeModel : IModel<WorkshopDocumentType, string>, IUpdateModel<WorkshopDocumentType>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document type label
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// a flag to set this type as default
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// update the entity from the given model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public void Update(WorkshopDocumentType entity)
        {
            entity.Value = Value;
        }
    }
}
