﻿namespace Axiobat.Application.Models
{
    using Domain.Constants;
    using Domain.Enums;
    using Domain.Entities;
    using Newtonsoft.Json.Linq;
    using System;

    /// <summary>
    /// put model for <see cref="RecurringDocument"/>
    /// </summary>
    [ModelFor(typeof(RecurringDocument))]
    public class RecurringDocumentPutModel : IUpdateModel<RecurringDocument>
    {
        /// <summary>
        /// if <see cref="PeriodicityType"/> is set to <see cref="PeriodicityType.Custom"/>
        /// this object will hold the information for defining the custom Periodicity
        /// </summary>
        public PeriodicityOptions PeriodicityOptions { get; set; }

        /// <summary>
        /// the type of the Periodicity
        /// </summary>
        public PeriodicityType PeriodicityType { get; set; }

        /// <summary>
        /// the type of the ending
        /// </summary>
        public PeriodicityEndingType EndingType { get; set; }

        /// <summary>
        /// the number of the times the Recurring should run before terminating
        /// </summary>
        public uint OccurringCount { get; set; }

        /// <summary>
        /// the ending of the recurring
        /// </summary>
        public DateTime? EndingDate { get; set; }

        /// <summary>
        /// the date that the Recurring will tack effect
        /// </summary>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// the status of the Recurring one of <see cref="Constants.RecurringStatus"/>
        /// </summary>
        public string Status { get; set; } = RecurringStatus.Draft;

        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the document that will be generated
        /// </summary>
        public JObject Document { get; set; }

        public void Update(RecurringDocument entity)
        {
            entity.PeriodicityOptions = PeriodicityOptions;
            entity.PeriodicityType = PeriodicityType;
            entity.OccurringCount = OccurringCount;
            entity.StartingDate = StartingDate;
            entity.DocType = DocumentType;
            entity.EndingDate = EndingDate;
            entity.EndingType = EndingType;
            entity.Document = Document;
            entity.Status = Status;
        }
    }
}
