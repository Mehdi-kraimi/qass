﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;
    using Domain.Constants;
    using Newtonsoft.Json.Linq;
    using System;

    /// <summary>
    /// model for <see cref="RecurringDocument"/>
    /// </summary>
    [ModelFor(typeof(RecurringDocument))]
    public class RecurringDocumentModel : IModel<RecurringDocument, string>
    {
        /// <summary>
        /// the id of the recurring
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// if <see cref="PeriodicityType"/> is set to <see cref="PeriodicityType.Custom"/>
        /// this object will hold the information for defining the custom Periodicity
        /// </summary>
        public PeriodicityOptions PeriodicityOptions { get; set; }

        /// <summary>
        /// the type of the Periodicity
        /// </summary>
        public PeriodicityType PeriodicityType { get; set; }

        /// <summary>
        /// the type of the ending
        /// </summary>
        public PeriodicityEndingType EndingType { get; set; }

        /// <summary>
        /// the number of the times the Recurring should run before terminating
        /// </summary>
        public uint OccurringCount { get; set; }

        /// <summary>
        /// the ending of the recurring
        /// </summary>
        public DateTime? EndingDate { get; set; }

        /// <summary>
        /// the date of the next occurring
        /// </summary>
        public DateTime NextOccurring { get; set; }

        /// <summary>
        /// the date that the Recurring will tack effect
        /// </summary>
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// how many time the recurring has occurred
        /// </summary>
        public uint Counter { get; set; }

        /// <summary>
        /// the status of the Recurring one of <see cref="Constants.RecurringStatus"/>
        /// </summary>
        public string Status { get; set; } = RecurringStatus.Draft;

        /// <summary>
        /// the type of the document
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the document that will be generated
        /// </summary>
        public JObject Document { get; set; }

        /// <summary>
        /// MaintenanceContract associated with this recurring if any
        /// </summary>
        public MaintenanceContractMinimalModel MaintenanceContract { get; set; }
    }
}
