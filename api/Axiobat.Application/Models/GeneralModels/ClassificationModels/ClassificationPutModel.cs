﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;

    /// <summary>
    /// the put model for <see cref="Classification"/>
    /// </summary>
    public class ClassificationPutModel : IUpdateModel<Classification>
    {
        /// <summary>
        /// the id of the classification
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// the type of the classification
        /// </summary>
        public ClassificationType Type { get; set; }

        /// <summary>
        /// the label of the category
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// a description of the category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// the id of the chart of account associated with this classification
        /// </summary>
        public string ChartAccountItemId { get; set; }

        /// <summary>
        /// the id of the parent classification
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// update the given entity from the current model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public void Update(Classification entity)
        {
            entity.Type = Type;
            entity.Label = Label;
            entity.ParentId = ParentId;
            entity.Description = Description;
            entity.ChartAccountItemId = ChartAccountItemId;
            entity.ChartAccountItem = null;
        }
    }
}
