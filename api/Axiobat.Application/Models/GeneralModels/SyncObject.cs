﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Enums;
    using Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// this class represent the Synchronization options
    /// </summary>
    public partial class SynchronizationOption
    {
        /// <summary>
        /// list of docTypes to Synchronize
        /// </summary>
        public static readonly DocumentType[] SynchronizedDocumentTypes = new[]
        {
            DocumentType.Configuration,
            DocumentType.Classification,
            DocumentType.Client,
            DocumentType.Supplier,
            DocumentType.Lot,
            DocumentType.Product,
            DocumentType.Workshop,
            DocumentType.Quote,
            DocumentType.Invoice,
            DocumentType.MaintenanceContract,
            DocumentType.OperationSheet,
            DocumentType.MaintenanceOperationSheet,
            DocumentType.RecurringDocument,
            DocumentType.EquipmentMaintenance,

        };

        /// <summary>
        /// create an instant of <see cref="SynchronizationOption"/>
        /// </summary>
        public SynchronizationOption()
        {
            Objects = new HashSet<SyncObject>();
        }

        /// <summary>
        /// the time of the last Synchronization
        /// </summary>
        public DateTime? LastSyncDate { get; set; }

        /// <summary>
        /// list of objects to Synchronize
        /// </summary>
        public IEnumerable<SyncObject> Objects { get; set; }

        /// <summary>
        /// check if requesting the first Synchronization
        /// </summary>
        /// <returns>true if first Synchronization, false if not</returns>
        [Newtonsoft.Json.JsonIgnore]
        public bool IsFistSynchronization => !LastSyncDate.HasValue;
    }

    /// <summary>
    /// this class defines the object used for the synchronization
    /// </summary>
    public partial class SyncObject
    {
        /// <summary>
        /// the status of the object
        /// </summary>
        public SynchronizationStatus Status { get; set; }

        /// <summary>
        /// the type of the document to Synchronize
        /// </summary>
        public DocumentType? DocType { get; set; }

        /// <summary>
        /// the object instant
        /// </summary>
        public Newtonsoft.Json.Linq.JObject Object { get; set; }
    }
}
