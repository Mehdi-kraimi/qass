﻿namespace Axiobat
{
    using Domain.Enums;

    /// <summary>
    /// this model is used to check if the reference is unique or not
    /// </summary>
    public class CheckReferenceModel
    {
        /// <summary>
        /// the type of the document to check the reference for it
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// the reference to be checked
        /// </summary>
        public string Reference { get; set; }
    }
}
