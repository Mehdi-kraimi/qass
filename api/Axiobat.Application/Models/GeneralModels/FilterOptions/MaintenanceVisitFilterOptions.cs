﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// the MaintenanceVisit filter options
    /// </summary>
    public partial class MaintenanceVisitFilterOptions : FilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceVisitFilterOptions "/>
        /// </summary>
        public MaintenanceVisitFilterOptions() : base()
        {
            ExternalPartnerId = new HashSet<string>();
            Months = new HashSet<int>();
        }

        /// <summary>
        /// the id of the External Partner, client or supplier
        /// </summary>
        public IEnumerable<string> ExternalPartnerId { get; set; }

        /// <summary>
        /// the status of the document
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the year
        /// </summary>
        public int? Year { get; set; }

        /// <summary>
        /// the month
        /// </summary>
        [Newtonsoft.Json.JsonProperty("Month")]
        public IEnumerable<int> Months { get; set; }
    }

    /// <summary>
    /// options for retrieving single MaintenanceVisit with year and month
    /// </summary>
    public partial class MaintenanceVisitRetrieveOptions
    {
        /// <summary>
        /// the id of the contract
        /// </summary>
        public string ContractId { get; set; }

        /// <summary>
        /// the year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// the month
        /// </summary>
        public int Month { get; set; }
    }
}
