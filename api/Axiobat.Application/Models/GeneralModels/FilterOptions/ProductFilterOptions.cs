﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// the Product filter options
    /// </summary>
    public partial class ProductFilterOptions : FilterOptions
    {
        /// <summary>
        /// the product that own this category
        /// </summary>
        public int? ClassificationPrentId { get; set; }

        /// <summary>
        /// the id of the supplier
        /// </summary>
        public string SupplierId { get; set; }

        /// <summary>
        /// the ids of labels that the products may owns
        /// </summary>
        public ICollection<string> Label { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="ProductFilterOptions"/>
    /// </summary>
    public partial class ProductFilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="ProductFilterOptions"/>
        /// </summary>
        public ProductFilterOptions() : base()
        {
            Label = new HashSet<string>();
        }
    }
}
