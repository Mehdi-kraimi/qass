﻿namespace Axiobat.Application.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the filter options of the user
    /// </summary>
    public partial class UserFilterOptions : FilterOptions
    {
        /// <summary>
        /// create an instant of <see cref="UserFilterOptions"/>
        /// </summary>
        public UserFilterOptions()
        {
            RoleIds = new HashSet<Guid>();
        }

        /// <summary>
        /// the id of the role of the user
        /// </summary>
        public IEnumerable<Guid> RoleIds { get; set; }
    }

    /// <summary>
    /// filter options for retrieving list of rubrics
    /// </summary>
    public partial class RubricFilterOptions : FilterOptions
    {
        /// <summary>
        /// the id of the workshop
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public string WorkshopId { get; set; }
    }
}
