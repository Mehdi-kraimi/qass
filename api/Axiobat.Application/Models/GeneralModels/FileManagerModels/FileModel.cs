﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// the file model
    /// </summary>
    public class FileModel
    {
        /// <summary>
        /// the default constructor
        /// </summary>
        public FileModel() {}

        /// <summary>
        /// construct a new file model with an id and a base64
        /// </summary>
        /// <param name="id">the id of the file</param>
        /// <param name="base64">the base64 file</param>
        public FileModel(string id, string base64)
        {
            Id = id;
            Base64 = base64;
        }

        /// <summary>
        /// a unique id that identifies this file, this id should be stored or you can't access the file anymore
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the file in a base64 format
        /// </summary>
        public string Base64 { get; set; }
    }
}
