﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// the object to send back to SocketLabs for event processing
    /// </summary>
    public partial class SocketLabsEventResponse
    {
        /// <summary>
        /// the validation key associated with our server
        /// </summary>
        public string ValidationKey { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="SocketLabsEventResponse"/>
    /// </summary>
    public partial class SocketLabsEventResponse
    {
        /// <summary>
        /// create an instant of <see cref="SocketLabsEventResponse"/>
        /// </summary>
        public SocketLabsEventResponse()
        {

        }

        /// <summary>
        /// create an instant of <see cref="SocketLabsEventResponse"/> with a validation key
        /// </summary>
        /// <param name="validationKey">the validation key value</param>
        public SocketLabsEventResponse(string validationKey)
        {
            ValidationKey = validationKey;
        }
    }
}
