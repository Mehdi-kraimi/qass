﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using System.Collections.Generic;

    /// <summary>
    /// this class is a model that describe the requirement of sending an email
    /// </summary>
    public partial class SendEmailOptions
    {
        /// <summary>
        /// a unique id that defines this email
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the email that the message has been send from
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// the subject of the email
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// the message body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// the list of emails who we will send the emails to them
        /// </summary>
        public ICollection<string> To { get; set; }

        /// <summary>
        /// Bcc stands for blind carbon copy which is similar to that of Cc except that the Email address
        /// of the recipients specified in this field do not appear in the received message header and the
        /// recipients in the To or Cc fields will not know that a copy sent to these address.
        /// </summary>
        public ICollection<string> Bcc { get; set; }

        /// <summary>
        /// Cc: (Carbon Copy) - Put the email address(es) here if you are sending a copy for their information
        /// (and you want everyone to explicitly see this) Bcc: (Blind Carbon Copy) - Put the email address here
        /// if you are sending them a Copy and you do not want the other recipients to see that you sent it to this contact.
        /// </summary>
        public ICollection<string> Cc { get; set; }

        /// <summary>
        /// list of attachments
        /// </summary>
        public ICollection<AttachmentModel> Attachments { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="SendEmailOptions"/>
    /// </summary>
    public partial class SendEmailOptions
    {
        /// <summary>
        /// create an instant of <see cref="SendEmailOptions"/>
        /// </summary>
        public SendEmailOptions()
        {
            To = new HashSet<string>();
            Bcc = new HashSet<string>();
            Cc = new HashSet<string>();
            Attachments = new HashSet<AttachmentModel>();
        }
    }
}
