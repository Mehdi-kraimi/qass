﻿namespace Axiobat.Application.Models
{
    using Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// An extended Dictionary to store values
    /// </summary>
    public class SocketLabsEvent
    {
        /// <summary>
        /// create an instant of <see cref="SocketLabsEvent"/>
        /// </summary>
        public SocketLabsEvent()
        {
            DateTime = DateTime.UtcNow;
            Type = SocketLabsEventType.Unknown;
        }

        /// <summary>
        /// type of the event
        /// </summary>
        public SocketLabsEventType Type { get; set; }

        /// <summary>
        /// server id who has issued this event
        /// </summary>
        public long ServerId { get; set; }

        /// <summary>
        /// the address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// the date of the event
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// the mailing id
        /// </summary>
        public string MailingId { get; set; }

        /// <summary>
        /// message id
        /// </summary>
        public string MessageId { get; set; }

        /// <summary>
        /// secret key
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// additional data
        /// </summary>
        public Dictionary<string, string> ExtraData { get; set; }

        /// <summary>
        /// get the value of the given parameter name
        /// </summary>
        /// <typeparam name="TOut">type of the output</typeparam>
        /// <param name="propName">the name of the property to be retrieved</param>
        /// <returns>the value, or default</returns>
        public string Get(string propName)
        {
            if (!ExtraData.TryGetValue(propName, out string value))
                return default;

            return value;
        }

        /// <summary>
        /// get the value of the given parameter name
        /// </summary>
        /// <typeparam name="TOut">type of the output</typeparam>
        /// <param name="propName">the name of the property to be retrieved</param>
        /// <returns>the value, or default</returns>
        public TOut Get<TOut>(string propName)
        {
            try
            {
                if (!ExtraData.TryGetValue(propName, out string value))
                    return default;

                return (TOut)Convert.ChangeType(value, typeof(TOut));
            }
            catch (Exception)
            {
                return default;
            }
        }
    }
}
