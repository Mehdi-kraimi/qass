﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="TurnoverGoal"/>
    /// </summary>
    [ModelFor(typeof(TurnoverGoal))]
    public partial class TurnoverGoalModel : IModel<TurnoverGoal, int>, IUpdateModel<TurnoverGoal>
    {
        /// <summary>
        /// create an instant of <see cref="TurnoverGoalModel"/>
        /// </summary>
        public TurnoverGoalModel()
        {
            MonthlyGoals = new HashSet<TurnoverMonthlyGoal>();
        }

        /// <summary>
        /// the year
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the goal of the year
        /// </summary>
        public float Goal { get; set; }

        /// <summary>
        /// the predicted goal to be achieved, per month
        /// </summary>
        public ICollection<TurnoverMonthlyGoal> MonthlyGoals { get; set; }

        public void Update(TurnoverGoal entity)
        {
            entity.Id = Id;
            entity.Goal = Goal;
            entity.MonthlyGoals = MonthlyGoals;
        }
    }
}
