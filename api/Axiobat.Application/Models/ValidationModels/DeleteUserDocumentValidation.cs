﻿namespace Axiobat.Application.Models.ValidationModels
{
    using Axiobat.Domain.Enums;
    using System.Collections.Generic;

    /// <summary>
    /// the user validation result
    /// </summary>
    public class DeleteUserDocumentValidation
    {
        public DeleteUserDocumentValidation()
        {
            Documents = new HashSet<AssociatedDocumentValidationItem>();
        }

        /// <summary>
        /// if the validation has Success
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// the documents
        /// </summary>
        public ICollection<AssociatedDocumentValidationItem> Documents { get; set; }
    }

    /// <summary>
    /// the details for the documents validation
    /// </summary>
    public class AssociatedDocumentValidationItem
    {
        /// <summary>
        /// the document type
        /// </summary>
        public DocumentType DocType { get; set; }

        /// <summary>
        /// the id of the document
        /// </summary>
        public string DocumentId { get; set; }
        public string Reference { get; set; }
    }
}
