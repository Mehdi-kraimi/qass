﻿namespace Axiobat.Application.Models.MappingProfiles.MappingResolver
{
    using AutoMapper;
    using Domain.Constants;
    using Domain.Entities;
    using Domain.Enums;
    using System;
    using System.Linq;

    public class QuoteToListModelConverter : ITypeConverter<Quote, QuoteListModel>
    {
        public QuoteListModel Convert(Quote source, QuoteListModel destination, ResolutionContext context)
        {
            return new QuoteListModel
            {
                Id = source.Id,
                Status = (source.Status == QuoteStatus.InProgress && source.DueDate < DateTime.Now) ? QuoteStatus.Late : source.Status,
                Workshop = source.Workshop?.Name,
                TotalHT = source.GetTotalHT(),
                TotalTTC = source.GetTotalTTC(),
                CanGenerateSupplierOrder = source.Status != QuoteStatus.Canceled && source.OrderDetails.ProductsDetailsType == OrderProductsDetailsType.List && source.SupplierOrders.Count() == 0,
                CanGenerateInvoice = source.Status != InvoiceStatus.Canceled && source.Situations.Count <= 0 && source.OrderDetails.ProductsDetailsType == OrderProductsDetailsType.List,

                CanGenerateDownPaymentInvoice = source.Status != QuoteStatus.Canceled &&
                !(source.Status == QuoteStatus.Billed &&
                source.Situations.Count == 00 &&

                source.SupplierOrders.Count == 0) &&
                 !source.Situations.Any(d => d.TypeInvoice == InvoiceType.General) &&
                 !source.Situations.Any(d => d.TypeInvoice == InvoiceType.Cloture) &&
                 source.Situations.Where(e => (e.TypeInvoice == InvoiceType.Acompte || e.TypeInvoice == InvoiceType.Situation) && e.Status != InvoiceStatus.Canceled).Sum(t => t.Situation) < 90,
                //&& !source.Situations.Any(d => d.TypeInvoice == InvoiceType.Situation),

                CanGenerateSituationInvoice =
                source.Status != QuoteStatus.Canceled &&

                !(source.Status == QuoteStatus.Billed
                && source.Situations.Count == 00
                && source.SupplierOrders.Count == 0)
                && source.Situations.Where(e => (e.TypeInvoice == InvoiceType.Acompte || e.TypeInvoice == InvoiceType.Situation) && e.Status != InvoiceStatus.Canceled).Sum(t => t.Situation) < 90
                && !source.Situations.Any(e => e.TypeInvoice == InvoiceType.Cloture)
                && !source.Situations.Any(e => e.TypeInvoice == InvoiceType.General)

                ,

                CanGenerateOperationSheet = (source.Status != QuoteStatus.Canceled) && source.OperationSheets.Count <= 0,
                CanDelete = source.Invoices.Count <= 0 && source.SupplierOrders.Count <= 0 && source.OperationSheets.Count <= 0 && source.Status != QuoteStatus.Billed,
                CanUpdate = source.Invoices.Count <= 0 && source.SupplierOrders.Count <= 0 && source.OperationSheets.Count <= 0 && source.Status != QuoteStatus.Billed && source.Status != QuoteStatus.Canceled,
                CreationDate = source.CreationDate,
                Client = source.Client.FirstName + ' ' + source.Client.LastName,
                DueDate = source.DueDate,
                Purpose = source.Purpose,
                Reference = source.Reference,
                Situation = GetInvoicingSutiation(source),
            };
        }

        private float GetInvoicingSutiation(Quote quote)
        {
            if (quote.Invoices.Any(i => i.TypeInvoice == InvoiceType.Cloture || i.TypeInvoice == InvoiceType.General)) // the quote has invoice cloture or normal
                return 100;

            return quote.Situations
                .Where(i => i.TypeInvoice != InvoiceType.Cloture && i.Status != InvoiceStatus.Canceled)
                .Sum(i => i.Situation);
        }
    }
}
