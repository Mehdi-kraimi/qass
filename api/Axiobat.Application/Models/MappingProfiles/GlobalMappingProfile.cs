﻿namespace Axiobat.Application.Models.MappingProfiles
{
    using App.Common;
    using AutoMapper;
    using Domain.Constants;
    using Domain.Enums;
    using Domain.Entities;
    using System;
    using System.Linq;
    using MappingResolver;

    /// <summary>
    /// a global mapping profile
    /// </summary>
    public class GlobalMappingProfile : Profile
    {
        /// <summary>
        /// create an instant of <see cref="GlobalMappingProfile"/>
        /// </summary>
        public GlobalMappingProfile()
        {
            GeneralModelsMapping();
            ProductsModelsMapping();
            AuthenticationsModelsMapping();
            ExternalPartnersModelsMapping();
            WorkshopModelMapping();
            WorkshopDocumentsRelatedEntitiesMapping();
            QuoteMapping();
            ExpenseMapping();
            InvoiceMapping();
            CreditNoteMapping();
            SupplierOrderMapping();
            PaymentMapping();
            EquipmentMaintenanceMapping();
            MaintenanceContractMapping();
            MaintenanceVisitMapping();
            MissionMapping();

            OperationSheetMapping();
            MaintenanceOperationSheetMapping();
        }

        /// <summary>
        /// the general model for mapping entities
        /// </summary>
        private void GeneralModelsMapping()
        {
            CreateMap<Label, LabelModel>()
                .ReverseMap();

            CreateMap<ProductLabel, LabelModel>()
                .ForMember(e => e.Id, r => r.MapFrom(e => e.LabelId))
                .ForMember(e => e.Value, r => r.MapFrom(e => e.Label.Value.EnsureValue("")))
                .ReverseMap();

            CreateMap<Unit, UnitModel>()
                .ReverseMap();

            CreateMap<ChartAccountItemPutModel, ChartAccountItem>();
            CreateMap<ChartAccountItem, ChartAccountSubItemModel>()
                .ReverseMap();

            CreateMap<ChartAccountItem, ChartAccountItemModel>()
                .ReverseMap();

            CreateMap<ClassificationPutModel, Classification>();
            CreateMap<Classification, ClassificationModel>()
                .ReverseMap();

            CreateMap<Tax, TaxModel>()
                .ReverseMap();

            CreateMap<PaymentMethod, PaymentMethodModel>()
                .ReverseMap();

            CreateMap<PaymentMethod, PaymentMethodModel>()
                .ReverseMap();

            CreateMap<FinancialAccount, FinancialAccountModel>()
                .ReverseMap();

            CreateMap<WorkshopDocumentType, WorkshopDocumentTypeModel>()
               .ReverseMap();

            CreateMap<AccountingPeriod, AccountingPeriodModel>()
               .ReverseMap();

            CreateMap<WorkshopDocumentRubric, WorkshopDocumentRubricModel>()
                .ForMember(e => e.TotalDocuments, r => r.MapFrom(e => e.Documents.Count))
                .ReverseMap();

            CreateMap<Memo, MemoModel>()
               .ReverseMap();

            CreateMap<ProductCategoryType, ProductCategoryTypeModel>();

            CreateMap<TurnoverGoal, TurnoverGoalModel>()
               .ReverseMap();

            CreateMap<PublishingContract, PublishingContractModel>();
            CreateMap<PublishingContractCreateModel, PublishingContract>();

            CreateMap<RecurringDocumentPutModel, RecurringDocument>()
                .ForMember(e => e.DocType, r => r.MapFrom(e => e.DocumentType));

            CreateMap<RecurringDocument, RecurringDocumentModel>()
                .ForMember(e => e.DocumentType, r => r.MapFrom(e => e.DocType))
                .ReverseMap();
        }

        /// <summary>
        /// the external partners models mapping profiles
        /// </summary>
        private void ExternalPartnersModelsMapping()
        {
            // --------------- Group --------------- //

            CreateMap<Group, GroupModel>()
                .ReverseMap();

            CreateMap<Group, GroupPutModel>()
                .ReverseMap();

            // --------------- Client --------------- //

            CreateMap<Client, ClientPutModel>()
                .ReverseMap();

            CreateMap<Client, ClientModel>()
                .ReverseMap();

            CreateMap<Client, ClientDocument>()
                .ReverseMap();

            CreateMap<ClientModel, ClientDocument>()
                .ReverseMap();

            CreateMap<Client, ExternalPartnerMinimalInfo>()
                .ForMember(e => e.Address, r => r.MapFrom(e => e.Addresses.FirstOrDefault(x => x.IsDefault == true)))
                .ForMember(e => e.Name, r => r.MapFrom(e => e.FullName));

            // --------------- supplier --------------- //

            CreateMap<Supplier, SupplierModel>()
                .ReverseMap();

            CreateMap<Supplier, SupplierPutModel>()
                .ReverseMap();

            CreateMap<Supplier, SupplierDocuement>()
                .ReverseMap();

            CreateMap<SupplierModel, SupplierDocuement>()
                .ReverseMap();

            CreateMap<Supplier, ExternalPartnerMinimalInfo>()
                .ForMember(e => e.Name, r => r.MapFrom(e => e.FullName));
        }

        /// <summary>
        /// authentication related models mapping profiles
        /// </summary>
        private void AuthenticationsModelsMapping()
        {
            CreateMap<User, UserCreateModel>()
                .ForMember(e => e.Password, e => e.MapFrom(r => r.PasswordHash))
                .ReverseMap();

            CreateMap<User, UserUpdateModel>()
                .ReverseMap();

            CreateMap<User, UserModel>()
                .ReverseMap();

            CreateMap<User, UserMinimalModel>()
                .ReverseMap();

            CreateMap<Role, RoleModel>()
                //.ForMember(e => e.Permissions, r => r.MapFrom(e => Permission.ToPermission(e.Permissions)))
                .ForMember(e => e.Permissions, r => r.Ignore())
                .ReverseMap();

            CreateMap<Role, RoleMinimalModel>()
                .ReverseMap();

            CreateMap<Role, RoleUpdateModel>()
               .ReverseMap();

            CreateMap<Permission, PermissionModel>()
                .ReverseMap();
        }

        /// <summary>
        /// products mapping profiles
        /// </summary>
        private void ProductsModelsMapping()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(e => e.Category, r => r.MapFrom(e => e.Classification))
                .ForMember(e => e.ProductSuppliers, r => r.MapFrom(e => e.Suppliers))
                .ReverseMap();

            CreateMap<LotProduct, LotProductDetails>()
                .ForMember(e => e.ProductDetails, r => r.MapFrom(e => e.Product))
                .ReverseMap();

            CreateMap<Lot, LotModel>()
                .ReverseMap();

            CreateMap<Product, ProductPutModel>()
                .ForMember(e => e.CategoryId, r => r.MapFrom(e => e.ClassificationId))
                .ForMember(e => e.ProductSuppliers, r => r.Ignore())
                .ForMember(e => e.Labels, r => r.Ignore())
                .ReverseMap()
                .ForMember(e => e.ClassificationId, r => r.MapFrom(e => e.CategoryId))
                .ForMember(e => e.Suppliers, r => r.Ignore())
                .ForMember(e => e.Labels, r => r.Ignore());

            CreateMap<ProductSupplier, ProductSupplierInfo>()
                .ReverseMap();

            CreateMap<Lot, LotPutModel>()
                .ForMember(e => e.Name, r => r.MapFrom(e => e.Designation))
                .ReverseMap()
                .ForMember(e => e.Designation, r => r.MapFrom(e => e.Designation.IsValid() ? e.Designation : e.Name));
        }

        /// <summary>
        /// the mapping for <see cref="ConstructionWorkshop"/> entity
        /// </summary>
        private void WorkshopModelMapping()
        {
            CreateMap<ConstructionWorkshop, ConstructionWorkshopModel>()
                .ForMember(e => e.Turnover, r => r.MapFrom(e => e.Invoices.Sum(x => x.OrderDetails.TotalTTC)))
                .ReverseMap();

            CreateMap<ConstructionWorkshop, ConstructionWorkshopMinimalModel>()
                .ReverseMap();

            CreateMap<ConstructionWorkshopPutModel, ConstructionWorkshop>()
                .ForMember(e => e.Rubrics, r => r.Ignore())
                .ReverseMap();

            CreateMap<WorkshopDocument, WorkshopDocumentModel>()
                .ReverseMap();

            CreateMap<Attachment, AttachmentModel>()
                .ForMember(e => e.Content, r => r.Ignore())
                .ReverseMap()
                .ForMember(e => e.FileId, r => r.Ignore());

            CreateMap<WorkshopDocument, WorkshopDocumentPutModel>()
                .ForMember(e => e.Attachments, r => r.Ignore())
                .ForMember(e => e.Types, r => r.Ignore())
                .ReverseMap()
                .ForMember(e => e.Attachments, r => r.Ignore())
                .ForMember(e => e.Types, r => r.Ignore());
        }

        /// <summary>
        /// the general mapping for document related entities
        /// </summary>
        private void WorkshopDocumentsRelatedEntitiesMapping()
        {
            CreateMap<WorkshopDocument, WorkshopDocumentModel>()
               .ForMember(e => e.Types, r => r.MapFrom(e => e.Types.Select(t => t.Type)))
               .ReverseMap();
        }

        private void QuoteMapping()
        {
            CreateMap<Quote, Invoice>()
                .ForMember(e => e.QuoteId, r => r.MapFrom(e => e.Id))
                .ForMember(e => e.DueDate, r => r.Ignore())
                .ForMember(e => e.Quote, r => r.Ignore())
                .ForMember(e => e.CreditNotes, r => r.Ignore())
                .ForMember(e => e.Reference, r => r.Ignore())
                .ReverseMap()
                .ForMember(e => e.Reference, r => r.Ignore())
                .ForMember(e => e.AddressIntervention, r => r.Ignore())
                .ForMember(e => e.Invoices, r => r.Ignore());

            CreateMap<Quote, MinimalDocumentModel>()
                .ForMember(e => e.DocumentType, e => e.MapFrom(r => DocumentType.Quote));

            CreateMap<Quote, QuoteListModel>()
                .ConvertUsing<QuoteToListModelConverter>();
            //
            //CanDelete = source.Invoices.Count <= 0 && source.SupplierOrders.Count <= 0 && source.OperationSheets.Count <= 0 && source.Status != QuoteStatus.Billed,
            //    CanUpdate = source.Invoices.Count <= 0 && source.SupplierOrders.Count <= 0 && source.OperationSheets.Count <= 0 && source.Status != QuoteStatus.Billed,
            CreateMap<Quote, QuoteModel>()
                   .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == QuoteStatus.InProgress && e.DueDate < DateTime.Now) ? ExpenseStatus.Late : e.Status))
                   .ForMember(e => e.CanCancel, r => r.MapFrom(e => e.Status != QuoteStatus.Accepted && e.Status != QuoteStatus.Refused && e.Status != QuoteStatus.InProgress && e.Status != QuoteStatus.Billed && e.OperationSheets.Count == 0 && e.SupplierOrders.Count == 0))
                   .ForMember(e => e.CanUpdate, r => r.MapFrom(e => (e.Invoices.Count <= 0 && e.SupplierOrders.Count <= 0  && e.Status != QuoteStatus.Billed && e.Status != QuoteStatus.Canceled)))
                   .ForMember(e => e.CanDelete, r => r.MapFrom(e => (e.Invoices.Count <= 0 && e.SupplierOrders.Count <= 0 && e.Status != QuoteStatus.Billed)))
                   .ForMember(e => e.CanGenerateOperationSheet, r => r.MapFrom(e => ((e.Status != QuoteStatus.Canceled) && e.OperationSheets.Count <= 0)))
                  .ForMember(e => e.CanGenerateInvoice, r => r.MapFrom(e => (e.Status != InvoiceStatus.Canceled ) && e.Situations.Count <= 0 && e.OrderDetails.ProductsDetailsType == OrderProductsDetailsType.List))
                  .ForMember(e => e.CanGenerateDownPaymentInvoice, r => r.MapFrom(e => (e.Status != QuoteStatus.Canceled &&
                  !(e.Status == QuoteStatus.Billed && e.Situations.Count == 00 && e.SupplierOrders.Count == 0)&&
            e.Situations.Where(d=> (d.TypeInvoice == InvoiceType.Acompte || d.TypeInvoice == InvoiceType.Situation)  && d.Status != InvoiceStatus.Canceled).Sum(t => t.Situation) < 90
        &&
                    !e.Situations.Any(d => d.TypeInvoice == InvoiceType.General) &&
                  !e.Situations.Any(d => d.TypeInvoice == InvoiceType.Cloture)
                  )))
                  .ForMember(e => e.CanGenerateSituationInvoice, r => r.MapFrom(e => e.Status != QuoteStatus.Canceled &&
                  !(e.Status == QuoteStatus.Billed && e.Situations.Count == 00
                  && e.SupplierOrders.Count == 0) &&
                e.Situations.Where(d => (d.TypeInvoice == InvoiceType.Acompte || d.TypeInvoice == InvoiceType.Situation) && d.Status != InvoiceStatus.Canceled).Sum(t => t.Situation) < 90 &&

                  !e.Situations.Any(d => d.TypeInvoice == InvoiceType.Cloture) &&
                    !e.Situations.Any(d => d.TypeInvoice == InvoiceType.General)
                  ))
                  .ForMember(e => e.CanGenerateSupplierOrder, r => r.MapFrom(e => e.Status != QuoteStatus.Canceled &&
                  !(e.Status == QuoteStatus.Billed
                  && e.SupplierOrders.Count == 0 && e.OrderDetails.ProductsDetailsType == OrderProductsDetailsType.List)
                  ))
                  .ReverseMap();

            CreateMap<Quote, QuotePutModel>()
                .ReverseMap();

            CreateMap<Quote, AssociatedDocument>()
                .ForMember(e => e.CreationDate, p => p.MapFrom(e => e.CreatedOn.DateTime))
                .ReverseMap();
        }

        private void ExpenseMapping()
        {
            CreateMap<Expense, ExpenseModel>()
                .ForMember(e => e.Payments, r => r.MapFrom(e => e.Payments.Select(p => p.Payment)))
                 .ForMember(e => e.CanUpdate, r => r.MapFrom(e => e.Payments.Count <= 0 && e.Status != InvoiceStatus.Closed))
                .ForMember(e => e.CanCancel, r => r.MapFrom(e => e.Status != InvoiceStatus.Closed  && e.Payments.Count <= 0))

                .ForMember(e => e.CanDelete, r => r.MapFrom(e => e.Payments.Count <= 0))
                .ForMember(e => e.Status, r => r.MapFrom(
                    e => (e.Status == ExpenseStatus.InProgress && e.DueDate < DateTime.Now) ? ExpenseStatus.Late : e.Status))
                .ReverseMap();

            CreateMap<ExpenseModel, SupplierOrders_Expenses>()
                .ForMember(e => e.Expense, r => r.MapFrom(e => e))
                .ReverseMap();

            CreateMap<Expense, ExpensePutModel>()
                .ForMember(e => e.SupplierOrdersId, r => r.Ignore())
                .ReverseMap()
                .ForMember(e => e.SupplierOrders, r => r.Ignore())
                .ForMember(e => e.Payments, r => r.Ignore());

            CreateMap<Expense, AssociatedDocument>();

            CreateMap<Expense, ExpenseListModel>()
                .ForMember(e => e.TotalTTC, r => r.MapFrom(e => e.GetTotalTTC()))
                .ForMember(e => e.TotalHT, r => r.MapFrom(e => e.GetTotalHT()))
                .ForMember(e => e.Workshop, r => r.MapFrom(e => e.Workshop.Name))
                .ForMember(e => e.Supplier, r => r.MapFrom(e => e.Supplier.FullName))
                .ForMember(e => e.CanUpdate, r => r.MapFrom(e => e.Payments.Count <= 0 && e.Status != InvoiceStatus.Closed))
                .ForMember(e => e.CanCancel, r => r.MapFrom(e => e.Status != InvoiceStatus.Closed && e.Payments.Count <= 0))
                                .ForMember(e => e.CanDelete, r => r.MapFrom(e => e.Payments.Count <= 0))

                // .ForMember(e => e.CanUpdate, r => r.MapFrom(e =>  e.Payments.Count <= 0))
                //.ForMember(e => e.CanDelete, r => r.MapFrom(e => e.Payments.Count <= 0))
                //.ForMember(e => e.CanCancel, r => r.MapFrom(e => e.Payments.Count <= 0))

                .ForMember(e => e.Status, r => r.MapFrom(
                    e => (e.Status == ExpenseStatus.InProgress && e.DueDate < DateTime.Now) ? ExpenseStatus.Late : e.Status));

            CreateMap<Expense, MinimalDocumentModel>()
               .ForMember(e => e.DocumentType, e => e.MapFrom(r => DocumentType.CreditNote));
        }

        private void InvoiceMapping()
        {
            CreateMap<Invoice, InvoiceModel>()
                    .ForMember(e => e.Payments, r => r.MapFrom(e => e.Payments.Select(p => p.Payment)))
                    .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == InvoiceStatus.InProgress && e.DueDate < DateTime.Now) ? InvoiceStatus.Late : e.Status))
                    .ForMember(e => e.CanDelete, r => r.MapFrom(e => e.TypeInvoice == InvoiceType.General && e.Payments.Count <= 0 && e.CreditNotes.Count <= 0))
                    .ForMember(e => e.CanUpdate, r => r.MapFrom(e => e.TypeInvoice == InvoiceType.General && e.Status != InvoiceStatus.Closed && e.CreditNotes.Count <= 0 && e.Payments.Count <= 0))
                    .ForMember(e => e.CanCancel, r => r.MapFrom(e =>
                    e.TypeInvoice == InvoiceType.General || e.TypeInvoice == InvoiceType.Cloture || (e.TypeInvoice == InvoiceType.Acompte && e.Quote.Situations.Any(q => q.TypeInvoice == InvoiceType.Cloture)) ||
                    (e.TypeInvoice == InvoiceType.Situation && e.Quote.Situations.Any(q => q.TypeInvoice == InvoiceType.Cloture)
                    && e.Quote.Situations.Any(q => q.TypeInvoice == InvoiceType.Cloture) && e.Payments.Count <= 0 && e.Status != InvoiceStatus.Closed && e.Status != InvoiceStatus.Draft
                    )))
                    .ReverseMap();
            CreateMap<Invoice, InvoicePutModel>()
                .ForMember(e => e.OperationSheetsIds, r => r.Ignore())
                .ReverseMap();

            CreateMap<Invoice, AssociatedDocument>();

            CreateMap<Invoice, InvoiceListModel>()
                .ForMember(e => e.RestToPay, r => r.MapFrom(e => e.GetRestToPay()))
                .ForMember(e => e.TotalTTC, r => r.MapFrom(e => e.GetTotalTTC()))
                .ForMember(e => e.TotalHT, r => r.MapFrom(e => e.GetTotalHT()))
                .ForMember(e => e.Workshop, r => r.MapFrom(e => e.Workshop.Name))
                .ForMember(e => e.Client, r => r.MapFrom(e => e.Client.FullName))
                .ForMember(e => e.ContractReference, r => r.MapFrom(e => e.Contract.Reference))
                .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == InvoiceStatus.InProgress && e.DueDate < DateTime.Now) ? InvoiceStatus.Late : e.Status))
                .ForMember(e => e.CanDelete, r => r.MapFrom(e => e.TypeInvoice == InvoiceType.General && e.Payments.Count <= 0 && e.CreditNotes.Count <= 0))
                .ForMember(e => e.CanUpdate, r => r.MapFrom(e => e.TypeInvoice == InvoiceType.General && e.Status != InvoiceStatus.Closed && e.CreditNotes.Count <= 0 && e.Payments.Count <= 0))
               .ForMember(e => e.CanCancel, r => r.MapFrom(e => e.TypeInvoice == InvoiceType.General && e.Status != InvoiceStatus.Closed && e.CreditNotes.Count <= 0 && e.Payments.Count <= 0))

                .ForMember(e => e.OperationSheetMaintenanceReference, r => r.MapFrom(e => e.OperationSheetMaintenance.Reference));


            CreateMap<Invoice, DocumentSharedModel>()
                .ForMember(e => e.Payments, r => r.MapFrom(e => e.Payments.Select(p => p.Payment)))
                .ForMember(e => e.DocumentType, e => e.MapFrom(r => DocumentType.Invoice))
                .ReverseMap();

            CreateMap<Invoice, MinimalDocumentModel>()
                .ForMember(e => e.DocumentType, e => e.MapFrom(r => DocumentType.Invoice));
        }

        private void CreditNoteMapping()
        {
            CreateMap<CreditNote, MinimalDocumentModel>()
                .ForMember(e => e.DocumentType, e => e.MapFrom(r => DocumentType.CreditNote));

            CreateMap<CreditNote, CreditNoteModel>()
                .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == CreditNoteStatus.InProgress && e.DueDate < DateTime.Now) ? CreditNoteStatus.Expired : e.Status))
               .ReverseMap();

            CreateMap<CreditNote, CreditNotePutModel>()
                .ReverseMap();

            CreateMap<CreditNote, DocumentSharedModel>()
                .ForMember(e => e.DocumentType, e => e.MapFrom(r => DocumentType.CreditNote))
                .ReverseMap();

            CreateMap<CreditNote, CreditNoteListModel>()
                .ForMember(e => e.TotalTTC, r => r.MapFrom(e => e.GetTotalTTC()))
                .ForMember(e => e.TotalHT, r => r.MapFrom(e => e.GetTotalHT()))
                .ForMember(e => e.Client, r => r.MapFrom(e => e.Client.FullName))
                .ForMember(e => e.Workshop, r => r.MapFrom(e => e.Workshop.Name))
                .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == CreditNoteStatus.InProgress && e.DueDate < DateTime.Now) ? CreditNoteStatus.Expired : e.Status)
                );

            CreateMap<CreditNote, AssociatedDocument>();
        }

        private void SupplierOrderMapping()
        {
            CreateMap<SupplierOrder, MinimalDocumentModel>()
                .ForMember(e => e.DocumentType, e => e.MapFrom(r => DocumentType.SupplierOrder));

            CreateMap<SupplierOrder, SupplierOrderModel>()
                    .ForMember(e => e.Expenses, r => r.MapFrom(e => e.Expenses))
                    .ReverseMap();

            CreateMap<SupplierOrder, SupplierOrderPutModel>()
                .ReverseMap();

            CreateMap<SupplierOrder, AssociatedDocument>();

            CreateMap<SupplierOrder, SupplierOrderListModel>()
                .ForMember(e => e.TotalTTC, r => r.MapFrom(e => e.GetTotalTTC()))
                .ForMember(e => e.TotalHT, r => r.MapFrom(e => e.GetTotalHT()))
                .ForMember(e => e.Supplier, r => r.MapFrom(e => e.Supplier.FullName));
        }

        private void PaymentMapping()
        {
            CreateMap<Payment, PaymentModel>()
                    .ReverseMap();

            CreateMap<Payment, PaymentPutModel>()
                .ForMember(e => e.Invoices, r => r.Ignore())
                .ForMember(e => e.Expenses, r => r.Ignore())
                .ReverseMap()
                .ForMember(e => e.Invoices, r => r.Ignore())
                .ForMember(e => e.Expenses, r => r.Ignore());

            CreateMap<Payment, PaymentListModel>()
                .ForMember(e => e.Account, r => r.MapFrom(e => e.Account.Label));

            CreateMap<Payment, AssociatedDocument>()
                .ForMember(e => e.CreationDate, p => p.MapFrom(e => e.CreatedOn.DateTime))
                .ReverseMap();
        }

        private void OperationSheetMapping()
        {
            CreateMap<OperationSheet, OperationSheetModel>()
                    .ForMember(e => e.Technicians, r => r.MapFrom(e => e.Technicians.Select(t => t.Technician)))
                    .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == OperationSheetStatus.Planned && e.EndDate.Date < DateTime.Now.Date) ? OperationSheetStatus.Late : e.Status))
                    .ReverseMap();

            CreateMap<OperationSheet, OperationSheetPutModel>()
                .ForMember(e => e.Technicians, r => r.MapFrom(e => e.Technicians.Select(t => t.TechnicianId)))
                .ReverseMap()
                .ForMember(e => e.Technicians, r => r.Ignore());

            CreateMap<OperationSheet, AssociatedDocument>()
                .ForMember(e => e.CreationDate, r => r.MapFrom(e => e.CreatedOn.DateTime));

            CreateMap<OperationSheet, CombinedOperationSheetListModel>()
                 .ForMember(e => e.CanDelete, r => r.MapFrom(e => e.Status != OperationSheetStatus.Realized && e.Status != OperationSheetStatus.Billed))
                 .ForMember(e => e.CanUpdate, r => r.MapFrom(e => e.Status != OperationSheetStatus.Realized && e.Status != OperationSheetStatus.Billed))
                 .ForMember(e => e.Workshop, r => r.MapFrom(e => e.Workshop.Name))
                 .ForMember(e => e.Client, r => r.MapFrom(e => e.Client.FullName))
                 .ForMember(e => e.Type, r => r.MapFrom(e => 3))
                 .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == OperationSheetStatus.Planned && e.EndDate.Date < DateTime.Now.Date) ? OperationSheetStatus.Late : e.Status))
                ;

            CreateMap<OperationSheet, OperationSheetListModel>()
                .ForMember(e => e.Technicians, r => r.MapFrom(e => e.Technicians.Select(t => t.Technician)))
                .ForMember(e => e.Client, r => r.MapFrom(e => e.Client.FullName))
                .ForMember(e => e.ClientCity, r => r.MapFrom(e => e.Client.Addresses.FirstOrDefault(x => x.IsDefault == true).City))
                .ForMember(e => e.Workshop, r => r.MapFrom(e => e.Workshop.Name))
                .ForMember(e => e.ClientId, r => r.MapFrom(e => e.Client.Id))
                .ForMember(e => e.WorkshopId, r => r.MapFrom(e => e.Workshop.Id))
                .ForMember(e => e.Adresses, r => r.MapFrom(e => new[] { e.AddressIntervention }))
                .ForMember(e => e.SmileSatisfication, r => r.MapFrom(e => e.ClientSignature == null ? 0 : e.ClientSignature.SmileSatisfication))
                .ForMember(e => e.CanDelete, r => r.MapFrom(e => e.Status != OperationSheetStatus.Realized && e.Status != OperationSheetStatus.Billed))
                .ForMember(e => e.CanUpdate, r => r.MapFrom(e => e.Status != OperationSheetStatus.Realized && e.Status != OperationSheetStatus.Billed))
                .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == OperationSheetStatus.Planned && e.EndDate.Date < DateTime.Now.Date) ? OperationSheetStatus.Late : e.Status))
                .ForMember(e => e.QuoteEtablir, r => r.MapFrom(e => (e.ModeStatut.Any(a => a.type == TypeStatus.DevisAEtablir))));

            CreateMap<OperationSheet, MinimalDocumentModel>()
                .ForMember(e => e.DocumentType, e => e.MapFrom(r => DocumentType.OperationSheet));
        }

        /// <summary>
        /// mapping configuration for <see cref="EquipmentMaintenance"/>
        /// </summary>
        private void EquipmentMaintenanceMapping()
        {
            CreateMap<EquipmentMaintenance, EquipmentMaintenanceModel>()
                .ReverseMap();

            CreateMap<EquipmentMaintenance, EquipmentMaintenanceListModel>();
        }

        private void MaintenanceContractMapping()
        {
            CreateMap<MaintenanceContract, MaintenanceContractListModel>()
                .ForMember(e => e.Status, r => r.MapFrom(e =>
                         (e.Status == MaintenanceContractStatus.Waiting && e.StartDate.Date == DateTime.Today.Date)
                            ? MaintenanceContractStatus.InProgress
                            : (e.Status == MaintenanceContractStatus.Waiting && e.EndDate.Date <= DateTime.Today.Date)
                                 ? MaintenanceContractStatus.Finished
                                 : e.Status))
                //.ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == MaintenanceContractStatus.Waiting && e.EndDate.Date <= DateTime.Today.Date) ? MaintenanceContractStatus.Finished : e.Status))
                .ForMember(e => e.Alerted, r => r.MapFrom(e => e.ExpirationAlertEnabled && e.EndDate.AddDays(-e.ExpirationAlertPeriod) <= DateTime.Today.Date && e.EndDate.Date >= DateTime.Today.Date));

            CreateMap<MaintenanceContract, MaintenanceContractMinimalModel>();
            CreateMap<MaintenanceContract, MaintenanceContractModel>()
                             .ForMember(e => e.Status, r => r.MapFrom(e =>
                         (e.Status == MaintenanceContractStatus.Waiting && e.StartDate.Date == DateTime.Today.Date)
                            ? MaintenanceContractStatus.InProgress
                            : (e.Status == MaintenanceContractStatus.Waiting && e.EndDate.Date <= DateTime.Today.Date)
                                 ? MaintenanceContractStatus.Finished
                                 : e.Status))
                .ForMember(e => e.ExpirationAlertPeriod, r => r.MapFrom(e => e.ExpirationAlertPeriod / (int)e.ExpirationAlertPeriodType));

            CreateMap<MaintenanceContractPutModel, MaintenanceContract>()
                .ForMember(e => e.ExpirationAlertPeriod, r => r.MapFrom(e => e.ExpirationAlertPeriod * (int)e.ExpirationAlertPeriodType))
                .ForMember(e => e.Attachments, r => r.Ignore())
                .ReverseMap();
        }

        private void MaintenanceVisitMapping()
        {
            CreateMap<MaintenanceVisit, MaintenanceVisitListModel>()
                .ForMember(e => e.MaintenanceOperationSheetReference, r => r.MapFrom(e => e.MaintenanceOperationSheet.Reference))
                .ForMember(e => e.ContractId, r => r.MapFrom(e => e.MaintenanceContractId))
                .ForMember(e => e.Year, r => r.MapFrom(e => e.Date.Year))
                .ForMember(e => e.Month, r => r.MapFrom(e => e.Date.Month))
                .ForMember(e => e.Client, r => r.MapFrom(e => e.Client.FullName))
                .ForMember(e => e.ClientId, r => r.MapFrom(e => e.Client.Id));

            CreateMap<MaintenanceVisit, MaintenanceVisitMinimalModel>()
                .ForMember(e => e.Year, r => r.MapFrom(e => e.Date.Year))
                .ForMember(e => e.Month, r => r.MapFrom(e => e.Date.Month))
                .ForMember(e => e.Technician, r => r.MapFrom(e => e.MaintenanceOperationSheet.Technician.FullName))
                .ForMember(e => e.ContractId, r => r.MapFrom(e => e.MaintenanceContractId))
                .ForMember(e => e.MaintenanceOperationSheetId, r => r.MapFrom(e => (e.MaintenanceOperationSheet == null) ? "" : e.MaintenanceOperationSheet.Id))
                .ForMember(e => e.MaintenanceOperationSheetReference, r => r.MapFrom(e => (e.MaintenanceOperationSheet == null) ? "" : e.MaintenanceOperationSheet.Reference));


            CreateMap<MaintenanceVisitModel, MaintenanceVisitMinimalModel>();

            CreateMap<MaintenanceVisit, MaintenanceVisitModel>()
                .ForMember(e => e.Year, r => r.MapFrom(e => e.Date.Year))
                .ForMember(e => e.Month, r => r.MapFrom(e => e.Date.Month))
                .ForMember(e => e.ContractId, r => r.MapFrom(e => e.MaintenanceContractId))
                .ForMember(e => e.Technician, r => r.MapFrom(e => (e.MaintenanceOperationSheet == null) ? "" : e.MaintenanceOperationSheet.Technician.FullName))
                .ForMember(e => e.MaintenanceOperationSheetId, r => r.MapFrom(e => (e.MaintenanceOperationSheet == null) ? "" : e.MaintenanceOperationSheet.Id))
                .ForMember(e => e.MaintenanceOperationSheetReference, r => r.MapFrom(e => (e.MaintenanceOperationSheet == null) ? "" : e.MaintenanceOperationSheet.Reference));
        }

        private void MaintenanceOperationSheetMapping()
        {
            CreateMap<MaintenanceOperationSheet, CombinedOperationSheetListModel>()
                .ForMember(e => e.CanDelete, r => r.MapFrom(e => e.Status != MaintenanceOperationSheetStatus.Realized && e.Status != MaintenanceOperationSheetStatus.Billed))
                .ForMember(e => e.CanUpdate, r => r.MapFrom(e => e.Status != MaintenanceOperationSheetStatus.Realized && e.Status != MaintenanceOperationSheetStatus.Billed))
                .ForMember(e => e.Workshop, r => r.MapFrom(e => " "))
                .ForMember(e => e.Client, r => r.MapFrom(e => e.Client.FullName))
                .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == MaintenanceOperationSheetStatus.Planned && e.EndDate.Date < DateTime.Now.Date) ? MaintenanceOperationSheetStatus.Late : e.Status))
                ;

            CreateMap<MaintenanceOperationSheet, MaintenanceOperationSheetListModel>()
                .ForMember(e => e.ClientId, r => r.MapFrom(e => e.Client.Id))
                .ForMember(e => e.Client, r => r.MapFrom(e => e.Client.FullName))
                .ForMember(e => e.TechnicianId, r => r.MapFrom(e => e.Technician.Id))
                .ForMember(e => e.Technician, r => r.MapFrom(e => e.Technician.FullName))
                .ForMember(e => e.ClientCity, r => r.MapFrom(e => e.Client.Addresses.FirstOrDefault(x => x.IsDefault == true).City))
                .ForMember(e => e.SmileSatisfication, r => r.MapFrom(e => e.ClientSignature == null ? 0 : e.ClientSignature.SmileSatisfication))
                .ForMember(e => e.CanDelete, r => r.MapFrom(e => e.Status != MaintenanceOperationSheetStatus.Realized && e.Status != MaintenanceOperationSheetStatus.Billed))
                .ForMember(e => e.CanUpdate, r => r.MapFrom(e => e.Status != MaintenanceOperationSheetStatus.Realized && e.Status != MaintenanceOperationSheetStatus.Billed))
                .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == MaintenanceOperationSheetStatus.Planned && e.EndDate.Date < DateTime.Now.Date) ? MaintenanceOperationSheetStatus.Late : e.Status))
                .ForMember(e => e.HasQuoteRequest, r => r.MapFrom(e => e.QuoteRequest.IsValid()));

            CreateMap<MaintenanceOperationSheet, MaintenanceOperationSheetModel>()
                .ForMember(e => e.Status, r => r.MapFrom(e => (e.Status == MaintenanceOperationSheetStatus.Planned && e.EndDate.Date < DateTime.Now.Date) ? MaintenanceOperationSheetStatus.Late : e.Status))
                .ReverseMap();

            CreateMap<MaintenanceOperationSheet, AssociatedDocument>()
                .ForMember(e => e.CreationDate, r => r.MapFrom(e => e.CreatedOn.Date));

            CreateMap<MaintenanceOperationSheetUpdateModel, MaintenanceOperationSheet>()
                .ReverseMap();

            CreateMap<MaintenanceOperationSheetCreateModel, MaintenanceOperationSheet>()
                .ForMember(e => e.Type, r => r.MapFrom(e => MaintenanceOperationSheetType.Maintenance));

            CreateMap<AfterSalesServiceOperationSheetCreateModel, MaintenanceOperationSheet>()
                .ForMember(e => e.Type, r => r.MapFrom(e => MaintenanceOperationSheetType.AfterSalesService));
        }

        private void MissionMapping()
        {
            CreateMap<Mission, MissionListModel>()
                .ForMember(e => e.ClientName, r => r.MapFrom(e => e.Client.FullName))
                .ForMember(e => e.TechnicianName, r => r.MapFrom(e => e.Technician.FullName));

            CreateMap<Mission, MissionModel>();

            CreateMap<Mission, MissionPutModel>()
                .ReverseMap();

            CreateMap<MissionType, MissionTypeModel>()
                .ReverseMap();
        }
    }
}
