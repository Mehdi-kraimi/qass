﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// the model for the type of the mission
    /// </summary>
    [ModelFor(typeof(MissionType))]
    public partial class MissionTypeModel : IModel<MissionType, int>, IUpdateModel<MissionType>
    {
        /// <summary>
        /// the id of the mission type
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// the mission Type
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// the kind of mission this type belongs to it
        /// </summary>
        public string MissionKind { get; set; }

        /// <summary>
        /// update the given entity
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public void Update(MissionType entity)
        {
            entity.Value = Value;
        }
    }
}
