﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// a model used for updating the supplier entity
    /// </summary>
    [ModelFor(typeof(Supplier))]
    public partial class SupplierPutModel : ExternalPartnerPutModel<Supplier>
    {
        /// <summary>
        /// the address of the supplier
        /// </summary>
        /// <remarks>
        /// this is a JSON Object
        /// </remarks>
        public Address Address { get; set; }

        public override void Update(Supplier entity)
        {
            base.Update(entity);
            entity.Address = Address;
        }
    }
}
