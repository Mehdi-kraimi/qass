﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// a minimal info about an external partner
    /// </summary>
    [ModelFor(typeof(ExternalPartner))]
    public partial class ExternalPartnerMinimalInfo
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the supplier reference, should be unique in every company database only
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the name of the Supplier
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// the address of the client
        /// </summary>
        public Address Address { get; set; }
    }
}
