﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="MaintenanceVisit"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceVisit))]
    public partial class MaintenanceVisitModel : IModel<MaintenanceVisit>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// status of the Visit, one of the values of <see cref="Constants.MaintenanceVisitStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the site where the maintenance will be performed
        /// </summary>
        public Address Site { get; set; }

        /// <summary>
        /// the year
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// the month
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the client associated with the maintenance visit
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the id of the contract
        /// </summary>
        public string ContractId { get; set; }

        /// <summary>
        /// the Technician associated with this Operation Sheet Maintenance
        /// </summary>
        public string Technician { get; set; }

        /// <summary>
        /// the id of the MaintenanceOperationSheet associated with this MaintenanceVisitModel
        /// </summary>
        public string MaintenanceOperationSheetId { get; set; }

        /// <summary>
        /// reference of the Maintenance OperationSheet associated with this visit
        /// </summary>
        public string MaintenanceOperationSheetReference { get; set; }

        /// <summary>
        /// the equipments associated with this visit
        /// </summary>
        public ICollection<MaintenanceContractEquipmentDetails> EquipmentDetails { get; set; }

        /// <summary>
        /// the list of Previous MaintenanceVisits
        /// </summary>
        public ICollection<MaintenanceVisitMinimalModel> PreviousMaintenanceVisits { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="MaintenanceVisitModel"/>
    /// </summary>
    public partial class MaintenanceVisitModel
    {
        /// <summary>
        /// create an instant of <see cref="MaintenanceVisitModel"/>
        /// </summary>
        public MaintenanceVisitModel()
        {
            EquipmentDetails = new HashSet<MaintenanceContractEquipmentDetails>();
            PreviousMaintenanceVisits = new HashSet<MaintenanceVisitMinimalModel>();
        }
    }
}
