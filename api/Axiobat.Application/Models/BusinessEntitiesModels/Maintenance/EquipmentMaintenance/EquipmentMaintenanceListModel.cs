﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="EquipmentMaintenance"/>
    /// </summary>
    [ModelFor(typeof(EquipmentMaintenance))]
    public partial class EquipmentMaintenanceListModel
    {
        /// <summary>
        /// the if of the model
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the name of the Equipment
        /// </summary>
        public string EquipmentName { get; set; }

        public ICollection<EquipmentMaintenanceOperation> MaintenanceOperations { get; set; }
    }
}
