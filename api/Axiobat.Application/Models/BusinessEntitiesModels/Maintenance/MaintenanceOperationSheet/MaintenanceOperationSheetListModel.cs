﻿namespace Axiobat.Application.Models
{
    using Domain.Enums;
    using Domain.Entities;
    using System;

    /// <summary>
    /// list model for <see cref="MaintenanceOperationSheet"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceOperationSheet))]
    public partial class MaintenanceOperationSheetListModel : IModel<MaintenanceOperationSheet>
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the Maintenance Operation Sheet, one of the values of <see cref="Domain.Constants.MaintenanceOperationSheetStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the type of the Maintenance OperationSheet
        /// </summary>
        public MaintenanceOperationSheetType Type { get; set; }

        /// <summary>
        /// the client associated with this operation sheet
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// the id of the client associated with this operation sheet
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the Technician associated with this operation sheet
        /// </summary>
        public string Technician { get; set; }

        /// <summary>
        /// the Id of the Technician associated with this operation sheet
        /// </summary>
        public string TechnicianId { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// a boolean to check if this Maintenance OperationSheet has a quote request or not
        /// </summary>
        public bool HasQuoteRequest { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }


        /// <summary>
        /// a boolean check if you can delete this Maintenance operation Sheet
        /// </summary>
        public bool CanDelete { get; set; }


        /// <summary>
        /// a boolean check if you can delete this Maintenance operation Sheet
        /// </summary>
        public bool CanUpdate{ get; set; }


        public string ClientCity { get; set; }

        /// <summary>
        /// Smile
        /// </summary>
        public SmileSatisfication SmileSatisfication { get; set; }
    }
}
