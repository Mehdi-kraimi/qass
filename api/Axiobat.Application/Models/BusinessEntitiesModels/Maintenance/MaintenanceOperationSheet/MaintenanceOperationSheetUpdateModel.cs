﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// update model for <see cref="MaintenanceOperationSheet"/>
    /// </summary>
    [ModelFor(typeof(MaintenanceOperationSheet))]
    public partial class MaintenanceOperationSheetUpdateModel : IUpdateModel<MaintenanceOperationSheet>
    {
        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }
        /// <summary>
        /// the id of the Technician
        /// </summary>
        public Guid TechnicianId { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the Report associated with the operation sheet
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the count of the Visits the technician has made to the client
        /// </summary>
        public int VisitsCount { get; set; }

        /// <summary>
        /// the total Basket consumption the technician has made
        /// </summary>
        public int TotalBasketConsumption { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the order details associated with this Maintenance Operation Sheet, only check for this if the <see cref="Type"/> is <see cref="MaintenanceOperationSheetType.AfterSalesService"/>
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// list of Technician Observation
        /// </summary>
        public ICollection<TechnicianObservation> Observations { get; set; }

        /// <summary>
        /// update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity</param>
        public void Update(MaintenanceOperationSheet entity)
        {
            entity.Report = Report;
            entity.Status = Status;
            entity.EndDate = EndDate;
            entity.Purpose = Purpose;
            entity.ClientId = ClientId;
            entity.StartDate = StartDate;
            entity.VisitsCount = VisitsCount;
            entity.TechnicianId = TechnicianId;
            entity.OrderDetails = OrderDetails;
            entity.AddressIntervention = AddressIntervention;
            entity.TotalBasketConsumption = TotalBasketConsumption;
            entity.Observations = Observations;
           entity.Reference = Reference;

        }
    }
}
