﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Entities;
    using Axiobat.Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for updating the documentation
    /// </summary>
    public partial class WorkshopUpdateDocumentationModel
    {
        /// <summary>
        /// list of documentations to be updated
        /// </summary>
        public ICollection<WorkshopDocument> Documentations { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString() => $"{Documentations.Count} Documentations";
    }

    /// <summary>
    /// the workshop documents count details of the workshop
    /// </summary>
    public partial class WorkshopDocumentsCountDetails
    {
        /// <summary>
        /// the count of invoices
        /// </summary>
        public int InvoicesCount { get; set; }

        /// <summary>
        /// the count of Orders
        /// </summary>
        public int OrdersCount { get; set; }

        /// <summary>
        /// the count of the Quotes
        /// </summary>
        public int QuotesCount { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString() => $"{InvoicesCount} Invoices, {OrdersCount} Orders, {QuotesCount} Quotes";
    }

    /// <summary>
    /// this class represent a details informations about the workshop associated documents
    /// </summary>
    public partial class WorkshopDocumentsDetails
    {
        /// <summary>
        /// the rubric id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the value of the rubric
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// the type of the rubric
        /// </summary>
        public DocumentRubricType Type { get; set; }

        /// <summary>
        /// the id of the workshop that owns this rubric
        /// </summary>
        public string WorkshopId { get; set; }

        /// <summary>
        /// the total count of documents associated with this Rubric
        /// </summary>
        public int TotalDocuments { get; set; }

        /// <summary>
        /// count of associated commercial document
        /// </summary>
        public int AssociatedDocumentsCount { get; set; }

        /// <summary>
        /// the last time the model has been modified
        /// </summary>
        public DateTimeOffset? LastModifiedOn { get; set; }
    }
}
