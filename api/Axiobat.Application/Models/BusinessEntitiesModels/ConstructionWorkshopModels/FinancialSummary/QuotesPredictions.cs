﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// the Total quotes
    /// </summary>
    public partial class QuotesPredictions
    {
        /// <summary>
        /// the total sales of the materials
        /// </summary>
        public float MaterialSales { get; set; }

        /// <summary>
        /// the total sales of the Workforce
        /// </summary>
        public float WorkforceSales { get; set; }

        /// <summary>
        /// the total work Hours of the Workforce
        /// </summary>
        public float TotalWorkforceWorkHours { get; set; }

        /// <summary>
        /// the total of the quotes
        /// </summary>
        public float Total => MaterialSales + WorkforceSales;

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"MaterialSales: {MaterialSales}, WorkforceSales: {WorkforceSales}, total: {Total}";
    }
}
