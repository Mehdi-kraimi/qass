﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// this class describe the Financial Summary of the Workshop
    /// </summary>
    [ModelFor(typeof(ConstructionWorkshop))]
    public partial class WorkshopFinancialSummary
    {
        /// <summary>
        /// create an instant of <see cref="WorkshopFinancialSummary"/>
        /// </summary>
        public WorkshopFinancialSummary()
        {
            Predictions = new Predictions();
            BillingTreasury = new BillingTreasury();
        }

        /// <summary>
        /// the workshop Financial predictions Summary
        /// </summary>
        public Predictions Predictions { get; set; }

        /// <summary>
        /// the workshop Billing and Treasury informations
        /// </summary>
        public BillingTreasury BillingTreasury { get; set; }

        public float TotalWorkshopTreasury { get; set; }
    }
}
