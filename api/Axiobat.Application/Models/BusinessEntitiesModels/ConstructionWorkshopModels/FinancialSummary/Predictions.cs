﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// this class holds the predictions related to the financial summary
    /// </summary>
    public partial class Predictions
    {
        /// <summary>
        /// create an instant of <see cref="Predictions"/>
        /// </summary>
        public Predictions()
        {
            Quotes = new QuotesPredictions();
            Expenses = new ExpensesPredictions();
        }

        /// <summary>
        /// the Quotes predictions
        /// </summary>
        public QuotesPredictions Quotes { get; set; }

        /// <summary>
        /// the Expenses predictions
        /// </summary>
        public ExpensesPredictions Expenses { get; set; }

        /// <summary>
        /// the Margin Predictions
        /// </summary>
        public MarginPredictions Margin { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString()
            => $"Total Quotes: {Quotes.Total}, Total Expense: {Expenses.Total}, Margin total: {Margin.Total}";
    }
}
