﻿namespace Axiobat.Application.Models
{
    using Axiobat.Domain.Entities;

    /// <summary>
    /// the count of documents
    /// </summary>
    [ModelFor(typeof(ConstructionWorkshop))]
    public partial class WorkshopDocumentsCount
    {
        /// <summary>
        /// the total count of invoices
        /// </summary>
        public int InvoicesCount { get; set; }

        /// <summary>
        /// total count of expenses
        /// </summary>
        public int ExpensesCount { get; set; }

        /// <summary>
        /// total count of operation sheets
        /// </summary>
        public int OperationSheetsCount { get; set; }

        /// <summary>
        /// total count of quotes
        /// </summary>
        public int QuotesCount { get; set; }

        /// <summary>
        /// the supplier order count
        /// </summary>
        public int SupplierOrdersCount { get; set; }
    }
}
