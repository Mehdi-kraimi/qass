﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// a model used to update the Progress Rate of a Workshop
    /// </summary>
    public partial class WorkshopUpdateProgressRateModel
    {
        /// <summary>
        /// the Progress Rate value to be affected to a workshop
        /// </summary>
        public int ProgressRate { get; set; }

        /// <summary>
        /// get the string representation of the object
        /// </summary>
        /// <returns>the string value</returns>
        public override string ToString() => $"ProgressRate: {ProgressRate}";
    }
}
