﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public partial class MaintenanceAnalyticsResult
    {
        public ContractPerDateDetails ContractExpireSoon { get; set; }
        public IEnumerable<ContractPerStatusDetails> ContractsPerStatus { get; set; }
        public IEnumerable<TechniciansWorkingHoursDetails> TechniciansWorkingHours { get; set; }
        public IEnumerable<ContractByTurnover> TopContractByTurnover { get; set; }

        public IEnumerable<OperationSheetsByType> OperationSheetByType { get; set; }
    }

    public class OperationSheetsByType
    {
        public int Count => OperationSheets.Count();

        public MaintenanceOperationSheetType Type { get; set; }

        public IEnumerable<OperationSheetsMinimalInfo> OperationSheets { get; set; }
    }

    public partial class ContractByTurnover
    {
        public double Turnover { get; set; }

        public ContractDetails Contract { get; set; }
    }

    public partial class TechniciansWorkingHoursDetails
    {
        public double TotalWorkingHours { get; set; }

        public UserMinimalModel Technician { get; set; }
        public IEnumerable<string> Contracts { get; set; }
        public IEnumerable<string> OperationSheets { get; set; }
    }

    public partial class ContractPerDateDetails
    {
        public int ContractsCount => ContractDetails.Count();

        public IEnumerable<ContractDetails> ContractDetails { get; set; }
    }

    public partial class ContractPerStatusDetails
    {
        public ContractPerStatusDetails(string status)
        {
            Status = status;
        }

        public string Status { get; }

        public int ContractsCount => ContractDetails.Count();

        public IEnumerable<ContractDetails> ContractDetails { get; set; }
    }

    public partial class ContractDetails
    {
        public string Id { get; set; }

        public string Status { get; set; }

        public ClientDocument Client { get; set; }

        /// <summary>
        /// the site of the maintenance
        /// </summary>
        public Address Site { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }
    }
}
