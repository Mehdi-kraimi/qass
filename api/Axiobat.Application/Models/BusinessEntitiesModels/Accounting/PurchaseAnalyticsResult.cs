﻿namespace Axiobat.Application.Models
{
    using System.Collections.Generic;
    using System.Linq;

    public partial class PurchaseAnalyticsResult
    {
        public IEnumerable<SuppliersAnalyticsResult> TopSuppliers { get; set; }
        public IEnumerable<ProductAnalyticsResult> TopProductsByQuantity { get; set; }
        public IEnumerable<ProductAnalyticsResult> TopProductsBySupplierPrice { get; set; }

        public IEnumerable<ExternalPartnerProductsAnalytics> SelectedProductPurchaseDetails { get; set; }
    }

    public partial class SuppliersAnalyticsResult
    {
        public SuppliersAnalyticsResult()
        {
            ExpensesDetails = new HashSet<MinimalDocumentModel>();
        }

        public ExternalPartnerMinimalInfo Supplier { get; set; }

        public int CountTransaction => ExpensesDetails.Count();

        public float TotalTransaction => ExpensesDetails.Sum(e => e.TotalTTC);

        public IEnumerable<MinimalDocumentModel> ExpensesDetails { get; set; }
    }

    public partial class ProductAnalyticsResult
    {
        private float? _totalOrders;
        private float? _countOrders;

        public string Id { get; set; }

        public string Name { get; set; }

        public string Reference { get; set; }

        public float TotalOrders
        {
            get
            {
                if (_totalOrders is null)
                    return (_totalOrders = Suppliers.Sum(e => e.Quantity * e.Price)).GetValueOrDefault();

                return _totalOrders.GetValueOrDefault();
            }
        }

        public float CountOrders
        {
            get
            {
                if (_countOrders is null)
                    return (_countOrders = Suppliers.Sum(e => e.Quantity)).GetValueOrDefault();

                return _countOrders.GetValueOrDefault();
            }
        }

        public ClassificationMinimalModel Classification { get; set; }

        public IEnumerable<ExternalPartnerProductsAnalytics> Suppliers { get; set; }
        public IEnumerable<LabelModel> Labels { get; set; }
    }

    public partial class ExternalPartnerProductsAnalytics
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public float Price { get; set; }
        public float Quantity { get; set; }
    }

    public partial class ExternalPartnerTransactionDetails
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public float Total { get; set; }
    }
}
