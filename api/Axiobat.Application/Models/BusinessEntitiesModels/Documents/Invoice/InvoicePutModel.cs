﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Entities;
    using Domain.Enums;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Invoice"/>
    /// </summary>
    [ModelFor(typeof(Invoice))]
    public partial class InvoicePutModel
    {
        /// <summary>
        /// the date the invoice should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// invoice creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// invoice AdresseIntervention
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// whether to include the Lot details or not
        /// </summary>
        public bool LotDetails { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        [PropertyName("Client")]
        [PropertyValue("Client.FullName")]
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the id of the Quote
        /// </summary>
        public string QuoteId { get; set; }

        /// <summary>
        /// the type of invoice
        /// </summary>
        public InvoiceType TypeInvoice { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the list of operation sheets associated with this invoice
        /// </summary>
        public ICollection<string> OperationSheetsIds { get; set; }

        /// <summary>
        /// the situation that this invoice representing, only check this value if the invoice type is deferent from <see cref="InvoiceType.General"/>
        /// </summary>
        public float Situation { get; set; }

        /// <summary>
        /// the id of the workshop
        /// </summary>
        public string ContractId { get; set; }

        /// <summary>
        /// the id of the operation sheet maintenance
        /// </summary>
        public string OperationSheetMaintenanceId { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="InvoicePutModel"/>
    /// </summary>
    public partial class InvoicePutModel : DocumentPutModel<Invoice>
    {
        /// <summary>
        /// create an instant of <see cref="InvoicePutModel"/>
        /// </summary>
        public InvoicePutModel()
        {
            OperationSheetsIds = new HashSet<string>();
        }

        /// <summary>
        /// use this function to update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public override void Update(Invoice entity)
        {
            base.Update(entity);
            entity.OrderDetails = OrderDetails;
            entity.DueDate = DueDate;
            entity.ClientId = ClientId;
            entity.Client = Client;
            entity.LotDetails = LotDetails;
            entity.ContractId = ContractId;
            entity.CreationDate = CreationDate;
            entity.AddressIntervention = AddressIntervention;
            entity.OperationSheetMaintenanceId = OperationSheetMaintenanceId;
        }
    }
}
