﻿namespace Axiobat.Application.Models
{
    /// <summary>
    /// a model that hold the payment balance
    /// </summary>
    public class PaymentBalance
    {
        /// <summary>
        /// the total balance
        /// </summary>
        public float Total => TotalPaymentInvoice - TotalPaymentExpense;

        /// <summary>
        /// the total payment of invoices
        /// </summary>
        public float TotalPaymentInvoice { get; set; }

        /// <summary>
        /// the total payment of the expenses
        /// </summary>
        public float TotalPaymentExpense { get; set; }
    }
}
