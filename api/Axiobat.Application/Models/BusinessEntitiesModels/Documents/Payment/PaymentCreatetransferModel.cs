﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;

    /// <summary>
    /// a model for creating a transfer operation
    /// </summary>
    [ModelFor(typeof(Payment))]
    public partial class PaymentCreatetransferModel
    {
        /// <summary>
        /// the transfer amount
        /// </summary>
        public float TransferAmount { get; set; }

        /// <summary>
        /// the account that will be credited
        /// </summary>
        public int CreditedAccountId { get; set; }

        /// <summary>
        /// the account that will be debited
        /// </summary>
        public int DebitedAccountId { get; set; }

        /// <summary>
        /// the id of the payment method
        /// </summary>
        public int PaymentMethodId { get; set; }
    }
}
