﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// model for <see cref="OperationSheet"/>
    /// </summary>
    [ModelFor(typeof(OperationSheet))]
    public partial class OperationSheetModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the status of the operation sheet, one of <see cref="Constants.OperationSheetStatus"/>
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the order details associated with this operation sheet
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// the starting date of the operation
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// the ending date of the operation
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// the Report associated with the operation sheet
        /// </summary>
        public string Report { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the count of the Visits the technician has made to the client
        /// </summary>
        public int VisitsCount { get; set; }

        /// <summary>
        /// the total Basket consumption the technician has made
        /// </summary>
        public int TotalBasketConsumption { get; set; }

        /// <summary>
        /// the client associated with the worksheet
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the Signature of the client
        /// </summary>
        public Signature ClientSignature { get; set; }

        /// <summary>
        /// the technicien signature
        /// </summary>
        public Signature TechnicianSignature { get; set; }

        /// <summary>
        /// the construction workshop
        /// </summary>
        public ConstructionWorkshopModel Workshop { get; set; }

        /// <summary>
        /// the Data sheets associated with the product
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of Technicians associated with the operation sheet
        /// </summary>
        public ICollection<UserMinimalModel> Technicians { get; set; }

        /// <summary>
        /// entity changes history
        /// </summary>
        public ICollection<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// entity changes mode statut
        /// </summary>
        public List<ModeStatut> ModeStatut { get; set; }

        /// <summary>
        /// list of associated documents
        /// </summary>
        public ICollection<AssociatedDocument> AssociatedDocuments { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="OperationSheetModel"/>
    /// </summary>
    public partial class OperationSheetModel : IModel<OperationSheet, string>
    {
        /// <summary>
        /// create an instant of <see cref="OperationSheetModel"/>
        /// </summary>
        public OperationSheetModel()
        {
            Memos = new HashSet<Memo>();
            ChangesHistory = new List<ChangesHistory>();
            Technicians = new HashSet<UserMinimalModel>();
            ModeStatut = new List<ModeStatut>();
            AssociatedDocuments = new HashSet<AssociatedDocument>();
        }
    }
}
