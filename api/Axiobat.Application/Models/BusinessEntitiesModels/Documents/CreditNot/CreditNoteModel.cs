﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="CreditNote"/>
    /// </summary>
    [ModelFor(typeof(CreditNote))]
    public partial class CreditNoteModel
    {
        /// <summary>
        /// the date the invoice should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Credit Note creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the client associated with the CreditNote
        /// </summary>
        public ClientDocument Client { get; set; }

        /// <summary>
        /// the CreditNote order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the memos associated with the CreditNote
        /// </summary>
        public ICollection<Memo> Memos { get; set; }

        /// <summary>
        /// list of emails that has been sent for this documents
        /// </summary>
        public ICollection<DocumentEmail> Emails { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="CreditNoteModel"/>
    /// </summary>
    public partial class CreditNoteModel : DocumentModel<CreditNote>
    {
        /// <summary>
        /// create an instant of <see cref="CreditNoteModel"/>
        /// </summary>
        public CreditNoteModel()
        {
            Emails = new HashSet<DocumentEmail>();
            Memos = new HashSet<Memo>();
        }
    }
}
