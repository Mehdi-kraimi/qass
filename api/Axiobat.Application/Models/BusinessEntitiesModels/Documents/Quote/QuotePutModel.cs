﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Entities;
    using System;

    /// <summary>
    /// the model for <see cref="Quote"/>
    /// </summary>
    [ModelFor(typeof(Quote))]
    public partial class QuotePutModel
    {
        /// <summary>
        /// Quote creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Quote due date
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// the devis order details
        /// </summary>
        public WorkshopOrderDetails OrderDetails { get; set; }

        /// <summary>
        /// the Intervention Address
        /// </summary>
        public Address AddressIntervention { get; set; }

        /// <summary>
        /// whether to include the Lot details or not
        /// </summary>
        public bool LotDetails { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        [PropertyName("Client")]
        [PropertyValue("Client.FullName")]
        public string ClientId { get; set; }

        /// <summary>
        /// the id of the client
        /// </summary>
        public ClientDocument Client { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="QuotePutModel"/>
    /// </summary>
    public partial class QuotePutModel : DocumentPutModel<Quote>, IUpdateModel<Quote>
    {
        /// <summary>
        /// use this function to update the entity from the current model
        /// </summary>
        /// <param name="entity">the entity to be updated</param>
        public override void Update(Quote entity)
        {
            base.Update(entity);
            entity.LotDetails = LotDetails;
            entity.OrderDetails = OrderDetails;
            entity.AddressIntervention = AddressIntervention;
            entity.ClientId = ClientId;
            entity.Client = Client;
            entity.CreationDate = CreationDate;
            entity.DueDate = DueDate;
        }
    }
}
