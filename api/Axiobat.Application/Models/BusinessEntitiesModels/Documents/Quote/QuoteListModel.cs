﻿using System;

namespace Axiobat.Application.Models
{
    /// <summary>
    /// the list model for all documents
    /// </summary>
    public class QuoteListModel
    {
        /// <summary>
        /// the id of the document
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// the status of the document
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// this workshop associated with this document
        /// </summary>
        public string Workshop { get; set; }

        /// <summary>
        /// the total TTC of the document
        /// </summary>
        public float TotalTTC { get; set; }

        /// <summary>
        /// the total HT of the document
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the purpose of the document
        /// </summary>
        public string Purpose { get; set; }


        /// <summary>
        /// the name of the document
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// the date creation
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Quote due date
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// a boolean check if you can generate an invoice for this quote
        /// </summary>
        public bool CanGenerateInvoice { get; set; }

        /// <summary>
        /// a boolean check if you can generate an DownPayment Invoice for this quote
        /// </summary>
        public bool CanGenerateDownPaymentInvoice { get; set; }

        /// <summary>
        /// a boolean check if you can generate a Situation Invoice for this quote
        /// </summary>
        public bool CanGenerateSituationInvoice { get; set; }

        /// <summary>
        /// a boolean check if you can generate a Supplier Order for this quote
        /// </summary>
        public bool CanGenerateSupplierOrder { get; set; }

        /// <summary>
        /// a boolean check if you can delete this quote
        /// </summary>
        public bool CanGenerateOperationSheet { get; set; }

        /// <summary>
        /// a boolean check if you can delete this quote
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// a boolean check if you can delete this quote
        /// </summary>
        public bool CanUpdate { get; set; }

        /// <summary>
        /// percentage facture
        /// </summary>
        public float Situation { get; set; }
    }
}
