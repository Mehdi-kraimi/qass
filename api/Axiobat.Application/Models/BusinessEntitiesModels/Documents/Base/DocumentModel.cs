﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Quote"/>
    /// </summary>
    [ModelFor(typeof(Document))]
    public partial class DocumentModel<TEntity> : IModel<TEntity> where TEntity : Entity<string>
    {
        /// <summary>
        /// create an instant of <see cref="DocumentModel{TEntity}"/>
        /// </summary>
        public DocumentModel()
        {
            AssociatedDocuments = new HashSet<AssociatedDocument>();
        }

        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the quote
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// a note about the quote
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// the purpose of the quote
        /// </summary>
        public string Purpose { get; set; }

        /// <summary>
        /// the conditions of the payment
        /// </summary>
        public string PaymentCondition { get; set; }

        /// <summary>
        /// the work shop associated with this Quote if any
        /// </summary>
        public ConstructionWorkshopMinimalModel Workshop { get; set; }

        /// <summary>
        /// entity changes history
        /// </summary>
        public ICollection<ChangesHistory> ChangesHistory { get; set; }

        /// <summary>
        /// list of associated documents
        /// </summary>
        public ICollection<AssociatedDocument> AssociatedDocuments { get; set; }
    }
}
