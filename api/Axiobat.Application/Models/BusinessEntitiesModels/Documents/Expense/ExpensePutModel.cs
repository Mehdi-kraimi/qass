﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Entities;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// the model for <see cref="Expense"/>
    /// </summary>
    [ModelFor(typeof(Expense))]
    public partial class ExpensePutModel
    {
        /// <summary>
        /// the date the expense should be payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Expense creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the id of the supplier
        /// </summary>
        [PropertyName("Supplier")]
        [PropertyValue("Supplier.FullName")]
        public string SupplierId { get; set; }

        /// <summary>
        /// the id of the supplier
        /// </summary>
        public SupplierDocuement Supplier { get; set; }

        /// <summary>
        /// the id of the Classification associated with the expense
        /// </summary>
        public int ExpenseCategoryId { get; set; }

        /// <summary>
        /// the order details
        /// </summary>
        public OrderDetails OrderDetails { get; set; }

        /// <summary>
        /// list of the suppliers orders associated with this expense
        /// </summary>
        public ICollection<string> SupplierOrdersId { get; set; }

        /// <summary>
        /// list of attachments
        /// </summary>
        public ICollection<AttachmentModel> Attachments { get; set; }
    }

    /// <summary>
    /// partial part <see cref="ExpensePutModel"/>
    /// </summary>
    public partial class ExpensePutModel : DocumentPutModel<Expense>
    {
        /// <summary>
        /// create an instant of <see cref="ExpensePutModel"/>
        /// </summary>
        public ExpensePutModel()
        {
            Attachments = new HashSet<AttachmentModel>();
            SupplierOrdersId = new HashSet<string>();
        }

        /// <summary>
        /// update the given entity for the current model
        /// </summary>
        /// <param name="entity">the entity instant</param>
        public override void Update(Expense entity)
        {
            base.Update(entity);
            entity.DueDate = DueDate;
            entity.Supplier = Supplier;
            entity.SupplierId = SupplierId;
            entity.CreationDate = CreationDate;
            entity.OrderDetails = OrderDetails;
        }
    }
}
