﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System;

    /// <summary>
    /// the list model for <see cref="Expense"/>
    /// </summary>
    [ModelFor(typeof(Expense))]
    public partial class ExpenseListModel
    {
        /// <summary>
        /// the id of the entity
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the document reference
        /// </summary>
        public string Reference { get; set; }

        /// <summary>
        /// status of the Expense
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// the Supplier associated with this Expense
        /// </summary>
        public string Supplier { get; set; }

        /// <summary>
        /// the Workshop associated with this Expense
        /// </summary>
        public string Workshop { get; set; }

        /// <summary>
        /// the category associated with this expense
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// the date the Expense should be payed or has been payed
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Expense creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// the total HT of the Expense
        /// </summary>
        public float TotalHT { get; set; }

        /// <summary>
        /// the total TTC of the Expense
        /// </summary>
        public float TotalTTC { get; set; }

        /// <summary>
        /// a boolean check if you can cancel this invoice
        /// </summary>
        public bool CanCancel { get; set; }

        /// <summary>
        /// a boolean check if you can delete this invoice
        /// </summary>
        public bool CanDelete { get; set; }
        /// <summary>
        /// a boolean check if you can delete this invoice
        /// </summary>
        public bool CanUpdate { get; set; }
    }
}
