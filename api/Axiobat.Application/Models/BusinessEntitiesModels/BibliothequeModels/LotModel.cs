﻿namespace Axiobat.Application.Models
{
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the <see cref="Lot"/> model
    /// </summary>
    [ModelFor(typeof(Lot))]
    public partial class LotModel : ProductsBaseModel<Lot>
    {
        /// <summary>
        /// name of the lot
        /// </summary>
        public string Name => Designation;

        /// <summary>
        /// list of lot products details
        /// </summary>
        public ICollection<LotProductDetails> Products { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="LotModel"/>
    /// </summary>
    public partial class LotModel : ProductsBaseModel<Lot>
    {
        /// <summary>
        /// create an instant of <see cref="LotModel"/>
        /// </summary>
        public LotModel()
        {
            Products = new HashSet<LotProductDetails>();
        }
    }
}
