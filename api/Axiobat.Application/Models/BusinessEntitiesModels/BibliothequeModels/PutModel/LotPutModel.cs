﻿namespace Axiobat.Application.Models
{
    using App.Common;
    using Domain.Entities;
    using System.Collections.Generic;

    /// <summary>
    /// the model used for updating or create the Lot
    /// </summary>
    [ModelFor(typeof(Lot))]
    public partial class LotPutModel
    {
        /// <summary>
        /// name of the lot
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// list of lot products details
        /// </summary>
        public ICollection<LotProductDetails> Products { get; set; }
    }

    /// <summary>
    /// partial part for <see cref="LotPutModel"/>
    /// </summary>
    public partial class LotPutModel : ProductsBasePutModel<Lot>
    {
        /// <summary>
        /// create an instant of <see cref="LotPutModel"/>
        /// </summary>
        public LotPutModel()
        {
            Products = new HashSet<LotProductDetails>();
        }

        public override void Update(Lot entity)
        {
            base.Update(entity);

            if (!Designation.IsValid())
                entity.Designation = Name;
        }
    }
}
