﻿using Axiobat.Application.Models;

namespace Axiobat
{
    /// <summary>
    /// this class will hold a reference
    /// to the secretes used in the application
    /// </summary>
    public class ApplicationSecrets
    {
        /// <summary>
        /// construct a new instant of <see cref="ApplicationSecrets"/>
        /// </summary>
        public ApplicationSecrets()
        {

        }

        /// <summary>
        /// Connection strings
        /// </summary>
        public ConnectionStrings ConnectionStrings { get; set; }

        /// <summary>
        /// Authentication secrets
        /// </summary>
        public AuthenticationSecretes Authentication { get; set; }

        /// <summary>
        /// Socketlabs Secrets
        /// </summary>
        public SocketlabsSecrets Socketlabs { get; set; }

        /// <summary>
        /// GoogleCalendar secrets
        /// </summary>
        public GoogleCalendarOptions GoogleCalendar { get; set; }
    }

    /// <summary>
    /// Authentication secretes
    /// </summary>
    public class AuthenticationSecretes
    {
        /// <summary>
        /// the secrets key for authentication
        /// </summary>
        public string SecretKey { get; set; }
    }

    /// <summary>
    /// database connection strings
    /// </summary>
    public class ConnectionStrings
    {
        /// <summary>
        /// application database connection string
        /// </summary>
        public string ApplicationDatabase { get; set; }
        public string Serilog { get; set; }
    }

    /// <summary>
    /// Socketlabs Secrets
    /// </summary>
    public class SocketlabsSecrets
    {
        /// <summary>
        /// the socketlabs API Key
        /// </summary>
        public string APIKey { get; set; }

        /// <summary>
        /// the id of the server
        /// </summary>
        public int ServerId { get; set; }

        /// <summary>
        /// the secret key sent with the Socketlabs events
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// the validation Key sent with the event
        /// </summary>
        public string ValidationKey { get; set; }
    }
}
